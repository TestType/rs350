package com.clou.rs350;

import android.app.Application;
import android.content.SharedPreferences;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.util.Log;

import com.clou.rs350.handler.CrashHandler;
import com.clou.rs350.manager.MessageManager;
import com.clou.rs350.manager.StatusBarManager;
import com.clou.rs350.model.ClouData;
import com.clou.rs350.utils.LanguageUtil;

public class CLApplication extends Application {
    private static final String NAME = "CLApplication";
    public static String BACKUPPATH = "/Backup";
    public static String DOWNLOADPATH = "/Download";
    public static String EXPORTPATH = "/ExportData";
    public static String IMAGEPATH = "/Image";
    public static String IMPORTPATH = "/ImportData";
    public static String LOGPATH = "/log";
    public static String LTMEXPORTPATH = "/LTMExportData";
    public static String REPORTPATH = "/Report";
    public static String ROOTPATH = "/RS350";
    public static String SIMULATORPATH = "/Simulator";
    public static String TEMPLATELOGOPATH = "/Template/Logo";
    public static String TEMPLETXMLPATH = "/Template/XML";
    public static CLApplication app;
    public static SharedPreferences mSharedPreferences;
    private int ContinueOrStart;
    private StatusBarManager statusBarManager;

    public void onCreate() {
        super.onCreate();
        app = this;
        mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        LanguageUtil.changeLanguage(this);
        ClouData.getInstance(this);
        initPath();
        this.statusBarManager = StatusBarManager.getInstance();
        this.statusBarManager.setContext(this);
        MessageManager.getInstance(this);
        CrashHandler.getInstance().init(this);
    }

    public void initPath() {
        String ROOT;
        if (Environment.getExternalStorageState().equals("mounted")) {
            ROOT = Environment.getExternalStorageDirectory().getPath();
        } else {
            Log.v("MKDIR", "No SD card!!!");
            ROOT = "/data/data/" + getPackageName();
        }
        if (ROOTPATH.equals("/RS350")) {
            ROOTPATH = ROOT + ROOTPATH;
            DOWNLOADPATH = ROOTPATH + DOWNLOADPATH;
            SIMULATORPATH = ROOTPATH + SIMULATORPATH;
            IMAGEPATH = ROOTPATH + IMAGEPATH;
            TEMPLETXMLPATH = ROOTPATH + TEMPLETXMLPATH;
            TEMPLATELOGOPATH = ROOTPATH + TEMPLATELOGOPATH;
            REPORTPATH = ROOTPATH + REPORTPATH;
            IMPORTPATH = ROOTPATH + IMPORTPATH;
            EXPORTPATH = ROOTPATH + EXPORTPATH;
            LTMEXPORTPATH = ROOTPATH + LTMEXPORTPATH;
            BACKUPPATH = ROOTPATH + BACKUPPATH;
            LOGPATH = ROOTPATH + LOGPATH;
        }

        refreshPath(ROOTPATH);
    }

    public void refreshPath(String path) {
        MediaScannerConnection.scanFile(this, new String[]{path}, null, new MediaScannerConnection.OnScanCompletedListener() {
            /* class com.clou.rs350.CLApplication.AnonymousClass1 */

            public void onScanCompleted(String path, Uri uri) {
            }
        });
    }

    public String getRootPath() {
        return ROOTPATH;
    }

    public String getDownloadPath() {
        return DOWNLOADPATH;
    }

    public String getSimulatorPath() {
        return SIMULATORPATH;
    }

    public String getImagePath() {
        return IMAGEPATH;
    }

    public String getTemplateXmlPath() {
        return TEMPLETXMLPATH;
    }

    public String getTemplateLogoPath() {
        return TEMPLATELOGOPATH;
    }

    public String getReportPath() {
        return REPORTPATH;
    }

    public String getImportPath() {
        return IMPORTPATH;
    }

    public String getExportPath() {
        return EXPORTPATH;
    }

    public String getLTMExportPath() {
        return LTMEXPORTPATH;
    }

    public String getBackupPath() {
        return BACKUPPATH;
    }

    public String getLogPath() {
        return LOGPATH;
    }

    public int getContinueOrStart() {
        return this.ContinueOrStart;
    }

    public void setContinueOrStart(int continueOrStart) {
        this.ContinueOrStart = continueOrStart;
    }
}
