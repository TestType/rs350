package com.clou.rs350.device.model;

import com.clou.rs350.db.model.DBDataModel;

public class Baudrate {
    public DBDataModel baudrate = new DBDataModel(-1L, "Auto");
    public DBDataModel dataBits = new DBDataModel(1L, "8");
    public DBDataModel parityBits = new DBDataModel();
    public DBDataModel stopBits = new DBDataModel(1L, "1");
}
