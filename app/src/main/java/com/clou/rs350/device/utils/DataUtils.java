package com.clou.rs350.device.utils;

import android.content.res.Resources;
import android.text.TextUtils;
import android.widget.TextView;

import com.clou.rs350.CLApplication;
import com.clou.rs350.R;
import com.clou.rs350.device.constants.CLTConstant;
import com.clou.rs350.device.model.DataModel;
import com.clou.rs350.device.model.ExplainedModel;
import com.clou.rs350.manager.MessageManager;
import com.itextpdf.text.pdf.PdfObject;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.text.pdf.codec.JBIG2SegmentReader;
import com.itextpdf.text.xml.xmp.XmpWriter;
import com.itextpdf.xmp.XMPError;

import java.io.UnsupportedEncodingException;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;

public class DataUtils {
    private static long time = 0;

    public static String getString(int resId) {
        return CLApplication.app.getResources().getString(resId);
    }

    public static String toHexString(byte[] bytes) {
        if (bytes == null) {
            return null;
        }
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < bytes.length; i++) {
            sb.append(String.valueOf(Integer.toHexString(bytes[i])) + " ");
        }
        return sb.toString();
    }

    public static String bytes2HexString(byte[] frame, String sep) {
        StringBuffer sb = new StringBuffer();
        for (byte b : frame) {
            String val = Integer.toHexString(b & 255);
            if (val.length() < 2) {
                val = "0" + val;
            }
            sb.append(val);
            sb.append(sep);
        }
        String f = sb.toString();
        if (f.length() <= 0 || f.lastIndexOf(sep) != f.length() - 1) {
            return f;
        }
        return f.substring(0, f.lastIndexOf(sep));
    }

    public static int getnumberCount(String strValue) {
        int count = strValue.length() - 1;
        while (count >= 0 && strValue.charAt(count) != '0' && strValue.charAt(count) != '1' && strValue.charAt(count) != '2' && strValue.charAt(count) != '3' && strValue.charAt(count) != '4' && strValue.charAt(count) != '5' && strValue.charAt(count) != '6' && strValue.charAt(count) != '7' && strValue.charAt(count) != '8' && strValue.charAt(count) != '9') {
            count--;
        }
        return count + 1;
    }

    public static String[] doubleArrayToStringArray(double[] array) {
        if (array == null || array.length == 0) {
            return null;
        }
        String[] strArray = new String[array.length];
        for (int i = 0; i < array.length; i++) {
            strArray[i] = String.valueOf(array[i]);
        }
        return strArray;
    }

    public static int parseInt(String value) {
        try {
            return Integer.parseInt(value);
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    public static int parseInt(String value, int defaultValue) {
        try {
            return Integer.parseInt(value);
        } catch (Exception e) {
            e.printStackTrace();
            return defaultValue;
        }
    }

    public static double parseDouble(String value) {
        try {
            return Double.parseDouble(value);
        } catch (Exception e) {
            e.printStackTrace();
            return 0.0d;
        }
    }

    public static double parseDouble(TextView valueTv) {
        try {
            return Double.parseDouble(valueTv.getText().toString().trim());
        } catch (Exception e) {
            e.printStackTrace();
            return 0.0d;
        }
    }

    public static double parseDouble(String value, String unit) {
        if (TextUtils.isEmpty(value) || "---".equals(value)) {
            return 0.0d;
        }
        return parseDouble(value.replace(unit, PdfObject.NOTHING));
    }

    public static long parseLong(String value) {
        try {
            return Long.parseLong(value);
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    public static byte[] parseObjectArray(Object[] data) {
        if (data == null || data.length == 0) {
            return null;
        }
        byte[] array = new byte[data.length];
        for (int i = 0; i < data.length; i++) {
            try {
                array[i] = Byte.parseByte(data[i].toString());
            } catch (Exception e) {
                return null;
            }
        }
        return array;
    }

    public static String parseString(byte[] data) {
        try {
            return new String(data, XmpWriter.UTF8);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static int convertToInt(Object data) {
        try {
            return ((Number) data).intValue();
        } catch (Exception e) {
            return 0;
        }
    }

    public static int convertToUint(Object data) {
        try {
            return ((Number) data).intValue() & 1073741823;
        } catch (Exception e) {
            return 0;
        }
    }

    public static double convertToDouble(Object data) {
        try {
            return ((Number) data).doubleValue();
        } catch (Exception e) {
            return 0.0d;
        }
    }

    public static long convertToLong(Object data) {
        try {
            return ((Number) data).longValue();
        } catch (Exception e) {
            return 0;
        }
    }

    public static int[] parseIntArray(Object[] array) {
        if (array == null) {
            return null;
        }
        int[] intArray = new int[array.length];
        for (int i = 0; i < array.length; i++) {
            intArray[i] = convertToInt(array[i]);
        }
        return intArray;
    }

    public static Double[] parseDoubleArray(Object[] array) {
        if (array == null) {
            return null;
        }
        Double[] doubleArray = new Double[array.length];
        for (int i = 0; i < array.length; i++) {
            doubleArray[i] = Double.valueOf(convertToDouble(array[i]));
        }
        return doubleArray;
    }

    public static String add_unit(Double data, String str_unit) {
        String unit;
        String result;
        double result_value;
        String unit2 = PdfObject.NOTHING;
        if (data == null) {
            return "0";
        }
        if (str_unit.contains("°") || str_unit.contains("%") || str_unit.contains("'")) {
            unit = str_unit;
            double result_value2 = data.doubleValue();
            DecimalFormat df = new DecimalFormat("0.000");
            DecimalFormatSymbols symbols = new DecimalFormatSymbols();
            symbols.setDecimalSeparator('.');
            df.setDecimalFormatSymbols(symbols);
            result = df.format(result_value2);
        } else {
            if (Math.abs(data.doubleValue()) > 1000000.0d) {
                result_value = data.doubleValue() / 1000000.0d;
                unit2 = "M";
            } else if (Math.abs(data.doubleValue()) > 1000.0d) {
                result_value = data.doubleValue() / 1000.0d;
                unit2 = "k";
            } else if (Math.abs(data.doubleValue()) < 1.0d) {
                result_value = data.doubleValue() * 1000.0d;
                unit2 = "m";
            } else {
                result_value = data.doubleValue();
            }
            unit = String.valueOf(unit2) + str_unit;
            result = RoundValue(Double.valueOf(result_value));
        }
        return String.valueOf(result) + unit;
    }

    public static String RoundValue(Double Value) {
        DecimalFormat df = null;
        int dian = new StringBuilder(String.valueOf(Math.abs(Value.doubleValue()))).toString().indexOf(".");
        if (dian == 1) {
            df = new DecimalFormat("0.00000");
        } else if (dian == 2) {
            df = new DecimalFormat("0.0000");
        } else if (dian == 3) {
            df = new DecimalFormat("0.000");
        } else if (dian == 4) {
            df = new DecimalFormat("0.00");
        } else if (dian == 5) {
            df = new DecimalFormat("0.0");
        } else if (dian == 6) {
            df = new DecimalFormat("000000");
        }
        DecimalFormatSymbols symbols = new DecimalFormatSymbols();
        symbols.setDecimalSeparator('.');
        df.setDecimalFormatSymbols(symbols);
        return df.format(Value);
    }

    public static Double RoundValueDouble(Double Value, int pointIndex) {
        DecimalFormat df = null;
        if (pointIndex == 1) {
            df = new DecimalFormat("0.0");
        } else if (pointIndex == 2) {
            df = new DecimalFormat("0.00");
        } else if (pointIndex == 3) {
            df = new DecimalFormat("0.000");
        } else if (pointIndex == 4) {
            df = new DecimalFormat("0.0000");
        } else if (pointIndex == 5) {
            df = new DecimalFormat("0.00000");
        } else if (pointIndex == 6) {
            df = new DecimalFormat("0.000000");
        }
        DecimalFormatSymbols symbols = new DecimalFormatSymbols();
        symbols.setDecimalSeparator('.');
        df.setDecimalFormatSymbols(symbols);
        return Double.valueOf(Double.parseDouble(df.format(Value)));
    }

    public static String RoundValueString(Double Value, int pointIndex) {
        DecimalFormat df = null;
        if (pointIndex == 1) {
            df = new DecimalFormat("0.0");
        } else if (pointIndex == 2) {
            df = new DecimalFormat("0.00");
        } else if (pointIndex == 3) {
            df = new DecimalFormat("0.000");
        } else if (pointIndex == 4) {
            df = new DecimalFormat("0.0000");
        } else if (pointIndex == 5) {
            df = new DecimalFormat("0.00000");
        } else if (pointIndex == 6) {
            df = new DecimalFormat("0.000000");
        }
        DecimalFormatSymbols symbols = new DecimalFormatSymbols();
        symbols.setDecimalSeparator('.');
        df.setDecimalFormatSymbols(symbols);
        return df.format(Value);
    }

    public static ExplainedModel[] parseDoubleArray(Object[] array, String util) {
        if (array == null || array.length < 1) {
            return null;
        }
        ExplainedModel[] doubleArray = new ExplainedModel[array.length];
        double[] tmpValue = new double[array.length];
        for (int i = 0; i < array.length; i++) {
            tmpValue[i] = convertToDouble(array[i]);
            doubleArray[i] = parse(Double.valueOf(tmpValue[i]), util);
        }
        return doubleArray;
    }

    public static ExplainedModel[] parseDoubleArrayRemove0(Object[] array, String util) {
        if (array == null || array.length < 2) {
            return null;
        }
        ExplainedModel[] doubleArray = new ExplainedModel[(array.length - 1)];
        double[] tmpValue = new double[(array.length - 1)];
        for (int i = 1; i < array.length; i++) {
            tmpValue[i - 1] = convertToDouble(array[i]);
            doubleArray[i - 1] = parse(Double.valueOf(tmpValue[i - 1]), util);
        }
        return doubleArray;
    }

    public static Double[] parseDoubleArrayRemove0(Object[] array, int dot) {
        if (array == null || array.length < 2) {
            return null;
        }
        Double[] doubleArray = new Double[(array.length - 1)];
        for (int i = 1; i < array.length; i++) {
            doubleArray[i - 1] = Double.valueOf(convertToDouble(array[i]));
            doubleArray[i - 1] = RoundValueDouble(doubleArray[i - 1], dot);
        }
        return doubleArray;
    }

    public static Double[] parseDoubleArray(Object[] array, int dot, int AngleDefinition) {
        if (array == null || array.length < 2) {
            return null;
        }
        Double[] doubleArray = new Double[array.length];
        for (int i = 0; i < array.length; i++) {
            doubleArray[i] = Double.valueOf(convertToDouble(array[i]));
            if (1 == AngleDefinition) {
                doubleArray[i] = Double.valueOf((360.0d - doubleArray[i].doubleValue()) % 360.0d);
            }
        }
        return doubleArray;
    }

    public static Double[] parseDoubleArrayRemove0(Object[] array, int dot, int AngleDefinition) {
        if (array == null || array.length < 2) {
            return null;
        }
        Double[] doubleArray = new Double[(array.length - 1)];
        for (int i = 1; i < array.length; i++) {
            doubleArray[i - 1] = Double.valueOf(convertToDouble(array[i]));
            if (1 == AngleDefinition) {
                doubleArray[i - 1] = Double.valueOf((360.0d - doubleArray[i - 1].doubleValue()) % 360.0d);
            }
        }
        return doubleArray;
    }

    public static boolean is3P(int wiring) {
        return ((wiring >> 6) & 1) != 0;
    }

    public static String decimalFormat(float value, int length) {
        DecimalFormat df = new DecimalFormat("0.00");
        DecimalFormatSymbols symbols = new DecimalFormatSymbols();
        symbols.setDecimalSeparator('.');
        df.setDecimalFormatSymbols(symbols);
        return df.format((double) value);
    }

    public static ExplainedModel parse(Object value, String unit) {
        ExplainedModel model = new ExplainedModel();
        try {
            double dValue = convertToDouble(value);
            return new ExplainedModel(Double.valueOf(dValue), add_unit(Double.valueOf(dValue), unit));
        } catch (Exception e) {
            return model;
        }
    }

    public static ExplainedModel parseAngle(Object value, int AngleDefinition) {
        try {
            double dValue = convertToDouble(value);
            if (1 == AngleDefinition) {
                dValue = (360.0d - dValue) % 360.0d;
            }
            return new ExplainedModel(Double.valueOf(dValue), add_unit(Double.valueOf(dValue), "°"));
        } catch (Exception e) {
            return new ExplainedModel();
        }
    }

    public static ExplainedModel parseRound(Object value) {
        ExplainedModel model = new ExplainedModel();
        try {
            double dValue = convertToDouble(value);
            return new ExplainedModel(Double.valueOf(dValue), RoundValue(Double.valueOf(dValue)));
        } catch (Exception e) {
            return model;
        }
    }

    public static ExplainedModel parseint(Object value) {
        ExplainedModel model = new ExplainedModel();
        try {
            int dValue = convertToInt(value);
            return new ExplainedModel(Integer.valueOf(dValue), new StringBuilder(String.valueOf(dValue)).toString());
        } catch (Exception e) {
            return model;
        }
    }

    public static ExplainedModel parseRound(Object value, int pointIndex) {
        ExplainedModel model = new ExplainedModel();
        try {
            double dValue = convertToDouble(value);
            return new ExplainedModel(Double.valueOf(dValue), RoundValueString(Double.valueOf(dValue), pointIndex).toString());
        } catch (Exception e) {
            return model;
        }
    }

    public static ExplainedModel parseRound(Object value, int pointIndex, String unit) {
        ExplainedModel model = new ExplainedModel();
        try {
            double dValue = Double.parseDouble(RoundValueString(Double.valueOf(convertToDouble(value)), pointIndex));
            return new ExplainedModel(Double.valueOf(dValue), String.valueOf(RoundValueString(Double.valueOf(dValue), pointIndex)) + unit);
        } catch (Exception e) {
            return model;
        }
    }

    public static String getTime(long seconds) {
        int hou = (int) (seconds / 3600);
        int min = (int) ((seconds / 60) % 60);
        int sec = (int) (seconds % 60);
        String houStr = hou > 9 ? new StringBuilder(String.valueOf(hou)).toString() : "0" + hou;
        return String.valueOf(houStr) + ":" + (min > 9 ? new StringBuilder(String.valueOf(min)).toString() : "0" + min) + ":" + (sec > 9 ? new StringBuilder(String.valueOf(sec)).toString() : "0" + sec);
    }

    public static String parseUnitByWiring(int pqFlag, String unit) {
        String str = PdfObject.NOTHING;
        switch (MessageManager.getInstance().getMeter().getPQFlagValue()) {
            case 0:
                str = "W";
                break;
            case 1:
                str = "var";
                break;
            case 2:
                str = "VA";
                break;
        }
        return unit.replace("W", str);
    }

    public static String parseData(DataModel data) {
        return RoundValue(Double.valueOf(((Number) data.Data[0]).doubleValue()));
    }

    public static String parseAngle(DataModel data) {
        return RoundValueString(Double.valueOf(((Number) data.Data[0]).doubleValue()), 3);
    }

    public static int parseDataInt(DataModel data) {
        return ((Number) data.Data[0]).intValue();
    }

    public static String parsePhaseSequence(int UaPhaseSequence, int UbPhaseSequence, int UcPhaseSequence) {
        return String.valueOf(String.valueOf(String.valueOf(PdfObject.NOTHING) + parsePhaseSequence(UaPhaseSequence)) + " | " + parsePhaseSequence(UbPhaseSequence)) + " | " + parsePhaseSequence(UcPhaseSequence);
    }

    private static String parsePhaseSequence(int phaseSequenceValue) {
        Resources res = CLApplication.app.getResources();
        switch (phaseSequenceValue) {
            case 1:
            case 8:
                return res.getString(R.string.text_1);
            case 2:
            case 16:
                return res.getString(R.string.text_2);
            case 4:
            case 32:
                return res.getString(R.string.text_3);
            default:
                return " ";
        }
    }

    public static int parseHexInt(String sHex) {
        if (TextUtils.isEmpty(sHex)) {
            return -1;
        }
        int iHex = 0;
        int i = 0;
        while (true) {
            if (i >= 8 && i >= sHex.length()) {
                return iHex;
            }
            iHex = (iHex << 4) + parseHexCharToInt(sHex.charAt(i));
            i++;
        }
    }

    public static String parseHexString(int intHex) {
        char[] cHex = {'0', '1', PdfWriter.VERSION_1_2, PdfWriter.VERSION_1_3, PdfWriter.VERSION_1_4, PdfWriter.VERSION_1_5, PdfWriter.VERSION_1_6, PdfWriter.VERSION_1_7, '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};
        String sHex = PdfObject.NOTHING;
        for (int i = 0; i < 8; i++) {
            sHex = String.valueOf(cHex[intHex & 15]) + sHex;
            intHex >>= 4;
        }
        return sHex;
    }

    private static int parseHexCharToInt(char charHex) {
        switch (charHex) {
            case JBIG2SegmentReader.PAGE_INFORMATION /*{ENCODED_INT: 48}*/:
                return 0;
            case '1':
                return 1;
            case '2':
                return 2;
            case JBIG2SegmentReader.END_OF_FILE /*{ENCODED_INT: 51}*/:
                return 3;
            case JBIG2SegmentReader.PROFILES /*{ENCODED_INT: 52}*/:
                return 4;
            case '5':
                return 5;
            case '6':
                return 6;
            case '7':
                return 7;
            case '8':
                return 8;
            case '9':
                return 9;
            case CLTConstant.Func_Mstate_41:
            case 'a':
                return 10;
            case CLTConstant.Func_Mstate_42:
            case 'b':
                return 11;
            case CLTConstant.Func_Mstate_43:
            case 'c':
                return 12;
            case 'D':
            case 'd':
                return 13;
            case 'E':
            case 'e':
                return 14;
            case 'F':
            case XMPError.BADXPATH /*{ENCODED_INT: 102}*/:
                return 15;
            default:
                return 0;
        }
    }

    public static boolean isDoubleClick() {
        if (System.currentTimeMillis() - time > 1500) {
            time = System.currentTimeMillis();
            return false;
        }
        time = System.currentTimeMillis();
        return true;
    }
}
