package com.clou.rs350.device;

import android.content.Context;
import android.os.AsyncTask;

import com.clou.rs350.R;
import com.clou.rs350.db.model.DigitalMeterDataType;
import com.clou.rs350.db.model.StandardInfo;
import com.clou.rs350.device.model.ConnectByteModel;
import com.clou.rs350.device.model.DataModel;
import com.clou.rs350.device.model.ExplainedModel;
import com.clou.rs350.device.utils.DataUtils;
import com.clou.rs350.task.ISleepCallback;
import com.clou.rs350.task.SleepTask;
import com.clou.rs350.task.TimerThread;
import com.clou.rs350.utils.AngleUtil;
import com.clou.rs350.utils.OtherUtils;
import com.itextpdf.text.pdf.ColumnText;
import com.itextpdf.text.pdf.PdfObject;

import java.lang.reflect.Array;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

public class EmulatorDevice extends MeterBaseDevice {
    private static final double angleToRadian = 0.017453292519943295d;
    private Double[][] m_ActivePower = ((Double[][]) Array.newInstance(Double.class, 3, 63));
    private Double[][] m_ApparentPower = ((Double[][]) Array.newInstance(Double.class, 3, 63));
    private Double[] m_BasicValue = {Double.valueOf(230.0d), Double.valueOf(230.0d), Double.valueOf(230.0d), Double.valueOf(5.0d), Double.valueOf(5.0d), Double.valueOf(5.0d)};
    private Double[] m_CFValue = new Double[6];
    private Double[] m_CosValue = new Double[4];
    private Double[] m_CurrentAngle = {Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d)};
    private DemandTest m_DemandTest = new DemandTest(this, null);
    private EnergyTest m_EnergyTest = new EnergyTest(this, null);
    private ErrorTest[] m_ErrorTest = {new ErrorTest(this, null), new ErrorTest(this, null), new ErrorTest(this, null)};
    private int m_FirstStart = 0;
    private float m_Frequency = 50.0f;
    private int m_Function = 0;
    private Double[][] m_HarmonicAngle = {new Double[]{Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d)}, new Double[]{Double.valueOf(240.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d)}, new Double[]{Double.valueOf(120.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d)}, new Double[]{Double.valueOf(330.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d)}, new Double[]{Double.valueOf(210.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d)}, new Double[]{Double.valueOf(90.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d)}};
    private Double[][] m_HarmonicAngleCal = {new Double[]{Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d)}, new Double[]{Double.valueOf(240.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d)}, new Double[]{Double.valueOf(120.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d)}, new Double[]{Double.valueOf(330.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d)}, new Double[]{Double.valueOf(210.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d)}, new Double[]{Double.valueOf(90.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d)}};
    private Double[][] m_HarmonicContent = {new Double[]{Double.valueOf(1.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d)}, new Double[]{Double.valueOf(1.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d)}, new Double[]{Double.valueOf(1.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d)}, new Double[]{Double.valueOf(1.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d)}, new Double[]{Double.valueOf(1.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d)}, new Double[]{Double.valueOf(1.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d)}};
    private Double[][] m_HarmonicValue = {new Double[]{Double.valueOf(230.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d)}, new Double[]{Double.valueOf(230.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d)}, new Double[]{Double.valueOf(230.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d)}, new Double[]{Double.valueOf(5.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d)}, new Double[]{Double.valueOf(5.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d)}, new Double[]{Double.valueOf(5.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d)}};
    private int m_IntegrationFlag = 0;
    private int m_IntegrationInterval = 0;
    private int m_IntegrationIntervalCal = 0;
    private Double[] m_LLVoltageAngle = {Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d)};
    private Double[] m_LLVoltageCurrentAngle = new Double[4];
    private Double[] m_LLVoltageValue = new Double[3];
    private Double[] m_LNVoltageAngle = {Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d)};
    private Double[] m_LNVoltageCurrentAngle = new Double[3];
    private Double[] m_NeutralMeasurement = {Double.valueOf(0.0d), Double.valueOf(0.0d)};
    private Double[] m_PQPPValue = new Double[12];
    private Double[][] m_PQWave = ((Double[][]) Array.newInstance(Double.class, 6, 192));
    private Double[] m_PowerValue = new Double[12];
    private Double[][] m_ReactivePower = ((Double[][]) Array.newInstance(Double.class, 3, 63));
    private int m_StartFlag = 0;
    private float[] m_THDValue = new float[6];
    private Double[] m_UIPPValue = new Double[12];
    private Double[] m_UIValue = new Double[6];
    private Double[][] m_UIWave = ((Double[][]) Array.newInstance(Double.class, 6, 192));
    private int m_Wiring = 0;

    /* access modifiers changed from: private */
    public class ErrorTest {
        private float Constant;
        private int ErrorCount;
        private Double[] Errors;
        private int HasErrorCount;
        private int IsTest;
        private int Pulse;
        private TimerThread ReceivePulse;
        private Double ReferenceError;
        private int RemainPulse;

        private ErrorTest() {
            this.IsTest = 0;
            this.Constant = ColumnText.GLOBAL_SPACE_CHAR_RATIO;
            this.Pulse = 0;
            this.ErrorCount = 0;
            this.HasErrorCount = 0;
            this.RemainPulse = 0;
            this.ReferenceError = Double.valueOf(0.0d);
            this.Errors = new Double[]{Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d)};
        }

        /* synthetic */ ErrorTest(EmulatorDevice emulatorDevice, ErrorTest errorTest) {
            this();
        }

        private void Init() {
            this.RemainPulse = this.Pulse;
            this.HasErrorCount = 0;
            this.Errors = new Double[]{Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d)};
            this.ReferenceError = Double.valueOf(Math.random() - 0.5d);
        }

        /* access modifiers changed from: private */
        /* access modifiers changed from: public */
        private void Start() {
            if (1 == this.IsTest) {
                Init();
                final int PulseTime = (int) ((3.6E9d / EmulatorDevice.this.m_PowerValue[3].doubleValue()) / ((double) this.Constant));
                new SleepTask((long) PulseTime, new ISleepCallback() {
                    /* class com.clou.rs350.device.EmulatorDevice.ErrorTest.AnonymousClass1 */

                    @Override // com.clou.rs350.task.ISleepCallback
                    public void onSleepAfterToDo() {
                        ErrorTest.this.ReceivePulse = new TimerThread((long) PulseTime, new ISleepCallback() {
                            /* class com.clou.rs350.device.EmulatorDevice.ErrorTest.AnonymousClass1.AnonymousClass1 */

                            @Override // com.clou.rs350.task.ISleepCallback
                            public void onSleepAfterToDo() {
                                ErrorTest errorTest = ErrorTest.this;
                                errorTest.RemainPulse--;
                                if (ErrorTest.this.RemainPulse == 0) {
                                    ErrorTest.this.RemainPulse = ErrorTest.this.Pulse;
                                    ErrorTest.this.HasErrorCount++;
                                    for (int i = 8; i >= 0; i--) {
                                        ErrorTest.this.Errors[i + 1] = ErrorTest.this.Errors[i];
                                    }
                                    ErrorTest.this.Errors[0] = Double.valueOf(ErrorTest.this.ReferenceError.doubleValue() + ((Math.random() - 0.5d) * 0.1d));
                                    if (ErrorTest.this.ErrorCount <= ErrorTest.this.HasErrorCount) {
                                        ErrorTest.this.Errors[10] = Double.valueOf(0.0d);
                                        ErrorTest.this.Errors[11] = Double.valueOf(0.0d);
                                        for (int i2 = 0; i2 < ErrorTest.this.ErrorCount; i2++) {
                                            Double[] dArr = ErrorTest.this.Errors;
                                            dArr[10] = Double.valueOf(dArr[10].doubleValue() + ErrorTest.this.Errors[i2].doubleValue());
                                        }
                                        Double[] dArr2 = ErrorTest.this.Errors;
                                        dArr2[10] = Double.valueOf(dArr2[10].doubleValue() / ((double) ErrorTest.this.ErrorCount));
                                        for (int i3 = 0; i3 < ErrorTest.this.ErrorCount; i3++) {
                                            Double[] dArr3 = ErrorTest.this.Errors;
                                            dArr3[11] = Double.valueOf(dArr3[11].doubleValue() + Math.pow(ErrorTest.this.Errors[i3].doubleValue() - ErrorTest.this.Errors[10].doubleValue(), 2.0d));
                                        }
                                        ErrorTest.this.Errors[11] = Double.valueOf(Math.sqrt(ErrorTest.this.Errors[11].doubleValue() / 5.0d));
                                    }
                                }
                            }
                        }, "Emulator get pulse");
                        ErrorTest.this.ReceivePulse.start();
                    }
                }).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, 0);
            }
        }

        /* access modifiers changed from: private */
        /* access modifiers changed from: public */
        private void Stop() {
            if (this.ReceivePulse != null) {
                this.ReceivePulse.stopTimer();
                this.ReceivePulse = null;
            }
        }
    }

    /* access modifiers changed from: private */
    public class EnergyTest {
        private long Count;
        private Double[] Energys;

        private EnergyTest() {
            this.Energys = new Double[]{Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d)};
            this.Count = 0;
        }

        /* synthetic */ EnergyTest(EmulatorDevice emulatorDevice, EnergyTest energyTest) {
            this();
        }

        /* access modifiers changed from: private */
        /* access modifiers changed from: public */
        private void Init() {
            this.Energys = new Double[]{Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d)};
            this.Count = 0;
        }

        /* access modifiers changed from: private */
        /* access modifiers changed from: public */
        private void CalEnergy() {
            this.Count++;
            for (int i = 0; i < 3; i++) {
                Double[] dArr = this.Energys;
                dArr[i] = Double.valueOf(dArr[i].doubleValue() + (EmulatorDevice.this.m_PowerValue[(i * 4) + 3].doubleValue() / 3600000.0d));
            }
        }
    }

    /* access modifiers changed from: private */
    public class DemandTest {
        private long Count;
        private Double Energy;

        private DemandTest() {
            this.Energy = Double.valueOf(0.0d);
            this.Count = 0;
        }

        /* synthetic */ DemandTest(EmulatorDevice emulatorDevice, DemandTest demandTest) {
            this();
        }

        /* access modifiers changed from: private */
        /* access modifiers changed from: public */
        private void Init() {
            this.Energy = Double.valueOf(0.0d);
            this.Count = 0;
        }

        /* access modifiers changed from: private */
        /* access modifiers changed from: public */
        private void CalEnergy() {
            this.Count++;
            this.Energy = Double.valueOf(this.Energy.doubleValue() + EmulatorDevice.this.m_PowerValue[3].doubleValue());
        }
    }

    public EmulatorDevice() {
        initRange();
        initData();
    }

    /* access modifiers changed from: protected */
    @Override // com.clou.rs350.device.BaseDevice, com.clou.rs350.device.MeterBaseDevice
    public void initRange() {
        this.S_ConnectType_All = new String[]{String.valueOf(OtherUtils.getString(R.string.text_U_Direct)) + "," + OtherUtils.getString(R.string.text_I_Direct), String.valueOf(OtherUtils.getString(R.string.text_U_Direct)) + "," + OtherUtils.getString(R.string.text_I_Clamp10A), String.valueOf(OtherUtils.getString(R.string.text_U_Direct)) + "," + OtherUtils.getString(R.string.text_I_Clamp100A), String.valueOf(OtherUtils.getString(R.string.text_U_Direct)) + "," + OtherUtils.getString(R.string.text_I_Clamp500A), String.valueOf(OtherUtils.getString(R.string.text_U_Direct)) + "," + OtherUtils.getString(R.string.text_I_Clamp1000A), String.valueOf(OtherUtils.getString(R.string.text_Pri_Clamp100A)) + "," + OtherUtils.getString(R.string.text_Sec_Clamp10A), String.valueOf(OtherUtils.getString(R.string.text_Pri_Clamp500A)) + "," + OtherUtils.getString(R.string.text_Sec_Clamp10A), String.valueOf(OtherUtils.getString(R.string.text_Pri_Clamp1000A)) + "," + OtherUtils.getString(R.string.text_Sec_Clamp10A)};
        this.S_ConnectType_Voltage = new String[]{OtherUtils.getString(R.string.text_U_Direct)};
        this.S_ConnectType_Current = new String[]{OtherUtils.getString(R.string.text_I_Direct), OtherUtils.getString(R.string.text_I_Clamp10A), OtherUtils.getString(R.string.text_I_Clamp100A), OtherUtils.getString(R.string.text_I_Clamp500A), OtherUtils.getString(R.string.text_I_Clamp1000A), OtherUtils.getString(R.string.text_I_flexct2000A), OtherUtils.getString(R.string.text_I_Clamp2000A), OtherUtils.getString(R.string.text_I_flexct3000A)};
        this.VoltageRange_All = new String[]{"60", "120", "240", "480"};
        this.CurrentRange_All = new String[]{"0.01", "0.02", "0.05", "0.1", "0.2", "0.5", "1", "2", "5", "10", "20", "50", "100", "200", "500", "1000", "1500", "2000", "3000"};
        int[] iArr = new int[8];
        iArr[1] = 1;
        iArr[2] = 2;
        iArr[3] = 3;
        iArr[4] = 4;
        iArr[5] = 5;
        iArr[6] = 6;
        iArr[7] = 7;
        this.I_ConnectType_All_Index = iArr;
        this.S_ConnectType_Realtime = new String[]{String.valueOf(OtherUtils.getString(R.string.text_U_Direct)) + "," + OtherUtils.getString(R.string.text_I_Direct), String.valueOf(OtherUtils.getString(R.string.text_U_Direct)) + "," + OtherUtils.getString(R.string.text_I_Clamp10A), String.valueOf(OtherUtils.getString(R.string.text_U_Direct)) + "," + OtherUtils.getString(R.string.text_I_Clamp100A), String.valueOf(OtherUtils.getString(R.string.text_U_Direct)) + "," + OtherUtils.getString(R.string.text_I_Clamp500A), String.valueOf(OtherUtils.getString(R.string.text_U_Direct)) + "," + OtherUtils.getString(R.string.text_I_Clamp1000A)};
        int[] iArr2 = new int[8];
        iArr2[1] = 1;
        iArr2[2] = 2;
        iArr2[3] = 3;
        iArr2[4] = 4;
        iArr2[5] = 5;
        iArr2[6] = 6;
        iArr2[7] = 7;
        this.I_ConnectType_Realtime_Index = iArr2;
        this.S_ConnectType_CT = new String[]{String.valueOf(OtherUtils.getString(R.string.text_Pri_Clamp100A)) + "," + OtherUtils.getString(R.string.text_Sec_Clamp10A), String.valueOf(OtherUtils.getString(R.string.text_Pri_Clamp500A)) + "," + OtherUtils.getString(R.string.text_Sec_Clamp10A), String.valueOf(OtherUtils.getString(R.string.text_Pri_Clamp1000A)) + "," + OtherUtils.getString(R.string.text_Sec_Clamp10A)};
        this.I_ConnectType_CT_Index = new int[]{5, 6, 7};
        this.VoltageRange = new ArrayList();
        this.VoltageRange.add(new String[]{"60V", "120V", "240V", "480V"});
        this.VoltageRange.add(new String[]{"0.1A", "10A", "100A"});
        this.VoltageRange.add(new String[]{"50A", "500A"});
        this.VoltageRange.add(new String[]{"100A", "1000A"});
        int[] iArr3 = new int[4];
        iArr3[1] = 18;
        iArr3[2] = 24;
        iArr3[3] = 30;
        this.VoltageRangeIndex = iArr3;
        this.CurrentRange = new ArrayList();
        this.CurrentRange.add(new String[]{"0.02A", "0.05A", "0.1A", "0.2A", "0.5A", "1A", "2A", "5A", "10A", "20A"});
        this.CurrentRange.add(new String[]{"0.1A", "0.2A", "0.5A", "1A", "2A", "5A"});
        this.CurrentRange.add(new String[]{"0.2A", "1A", "5A", "20A", "100A"});
        this.CurrentRange.add(new String[]{"5A", "20A", "100A", "500A"});
        this.CurrentRange.add(new String[]{"10A", "20A", "50A", "200A", "1000A"});
        this.CurrentRange.add(new String[]{"200A", "500A", "1000A", "2000A"});
        this.CurrentRange.add(new String[]{"20A", "100A", "500A", "2000A"});
        this.CurrentRange.add(new String[]{"200A", "500A", "1500A", "3000A"});
        this.CurrentRangeIndex = new int[]{1, 13, 20, 27, 33, 40, 46, 52};
        this.S_Neutral_Voltage_Range = new String[]{"480V", "60V"};
        this.S_Neutral_Current_Range = new String[]{"100A", "10A"};
    }

    private void initData() {
        for (int i = 0; i < 6; i++) {
            for (int j = 1; j < 63; j++) {
                this.m_HarmonicContent[i][j] = Double.valueOf(Math.random() * 5.0E-4d);
                this.m_HarmonicAngleCal[i][j] = Double.valueOf(AngleUtil.adjustAngle((Math.random() - 0.5d) * 360.0d, 1));
            }
        }
    }

    private void CalLLVoltage(int index) {
        double radian = this.m_HarmonicAngle[index][0].doubleValue() * angleToRadian;
        double x = this.m_UIValue[index].doubleValue() * Math.cos(radian);
        double y = this.m_UIValue[index].doubleValue() * Math.sin(radian);
        double radian2 = this.m_HarmonicAngle[(index + 1) % 3][0].doubleValue() * angleToRadian;
        double x2 = x - (this.m_UIValue[(index + 1) % 3].doubleValue() * Math.cos(radian2));
        double y2 = y - (this.m_UIValue[(index + 1) % 3].doubleValue() * Math.sin(radian2));
        this.m_LLVoltageValue[index] = Double.valueOf(Math.sqrt((x2 * x2) + (y2 * y2)));
        this.m_LLVoltageAngle[index] = Double.valueOf(AngleUtil.adjustAngle(Math.atan2(y2, x2) / angleToRadian));
        this.m_LLVoltageCurrentAngle[index] = Double.valueOf(AngleUtil.adjustAngle(this.m_LLVoltageAngle[index].doubleValue() - this.m_HarmonicAngle[index + 3][0].doubleValue()));
    }

    private void getAngles() {
        double[] angles = {this.m_HarmonicAngle[0][0].doubleValue(), this.m_HarmonicAngle[1][0].doubleValue(), this.m_HarmonicAngle[2][0].doubleValue(), this.m_LLVoltageAngle[0].doubleValue(), this.m_LLVoltageAngle[1].doubleValue(), this.m_LLVoltageAngle[2].doubleValue(), this.m_LLVoltageAngle[3].doubleValue(), this.m_HarmonicAngle[3][0].doubleValue(), this.m_HarmonicAngle[4][0].doubleValue(), this.m_HarmonicAngle[5][0].doubleValue()};
        if (1 == this.m_VectorDefinition) {
            double referenceAngle = 0.0d;
            if (0.0d != this.m_UIValue[3].doubleValue()) {
                referenceAngle = angles[7];
            } else if (0.0d != this.m_UIValue[4].doubleValue()) {
                referenceAngle = angles[8] - 120.0d;
            } else if (0.0d != this.m_UIValue[5].doubleValue()) {
                referenceAngle = angles[9] - 240.0d;
            }
            for (int i = 0; i < angles.length; i++) {
                angles[i] = angles[i] - referenceAngle;
                angles[i] = AngleUtil.adjustAngle(angles[i]);
            }
        }
        if (1 == this.m_AngleDefinition) {
            for (int i2 = 0; i2 < angles.length; i2++) {
                angles[i2] = (360.0d - angles[i2]) % 360.0d;
            }
        }
        this.m_LNVoltageAngle[0] = Double.valueOf(angles[0]);
        this.m_LNVoltageAngle[1] = Double.valueOf(angles[1]);
        this.m_LNVoltageAngle[2] = Double.valueOf(angles[2]);
        this.m_LLVoltageAngle[0] = Double.valueOf(angles[3]);
        this.m_LLVoltageAngle[1] = Double.valueOf(angles[4]);
        this.m_LLVoltageAngle[2] = Double.valueOf(angles[5]);
        this.m_LLVoltageAngle[3] = Double.valueOf(angles[6]);
        this.m_CurrentAngle[0] = Double.valueOf(angles[7]);
        this.m_CurrentAngle[1] = Double.valueOf(angles[8]);
        this.m_CurrentAngle[2] = Double.valueOf(angles[9]);
    }

    private void CalMeasureValue() {
        this.m_UIWave = (Double[][]) Array.newInstance(Double.class, 6, 192);
        double sqrt2 = Math.sqrt(2.0d);
        for (int i = 0; i < 6; i++) {
            Double[] dArr = this.m_HarmonicValue[i];
            dArr[0] = Double.valueOf(dArr[0].doubleValue() + (this.m_BasicValue[i].doubleValue() * (Math.random() - 0.5d) * 2.0E-4d));
            this.m_HarmonicAngle[i][0] = Double.valueOf(AngleUtil.adjustAngle(this.m_HarmonicAngle[i][0].doubleValue() + ((Math.random() - 0.5d) * 0.02d)));
            this.m_UIValue[i] = Double.valueOf(this.m_HarmonicValue[i][0].doubleValue() * this.m_HarmonicValue[i][0].doubleValue());
            this.m_THDValue[i] = 0.0f;
            this.m_HarmonicAngle[0][0] = Double.valueOf(0.0d);
            double radian = (360.0d - this.m_HarmonicAngle[i][0].doubleValue()) * angleToRadian;
            for (int k = 0; k < 192; k++) {
                this.m_UIWave[i][k] = Double.valueOf(Math.sin((((double) k) * 0.09817477042468103d) - radian) * this.m_HarmonicValue[i][0].doubleValue() * sqrt2);
            }
            for (int j = 1; j < 63; j++) {
                this.m_HarmonicContent[i][j] = Double.valueOf(Math.abs(((Math.random() - 0.5d) * 1.0E-4d) + this.m_HarmonicContent[i][j].doubleValue()));
                this.m_HarmonicValue[i][j] = Double.valueOf(this.m_HarmonicValue[i][0].doubleValue() * this.m_HarmonicContent[i][j].doubleValue());
                float[] fArr = this.m_THDValue;
                fArr[i] = (float) (((double) fArr[i]) + (this.m_HarmonicContent[i][j].doubleValue() * this.m_HarmonicContent[i][j].doubleValue()));
                this.m_HarmonicAngleCal[i][j] = Double.valueOf(AngleUtil.adjustAngle(this.m_HarmonicAngleCal[i][j].doubleValue() + ((Math.random() - 0.5d) * 0.02d), 1));
                this.m_HarmonicAngle[i][j] = Double.valueOf(this.m_HarmonicAngleCal[i][j].doubleValue() / ((double) (j + 1)));
                this.m_UIPPValue[i * 2] = Double.valueOf(0.0d);
                this.m_UIPPValue[(i * 2) + 1] = Double.valueOf(0.0d);
                double radian2 = (((360.0d - this.m_HarmonicAngle[i][0].doubleValue()) * ((double) j)) + this.m_HarmonicAngle[i][j].doubleValue()) * angleToRadian;
                for (int k2 = 0; k2 < 192; k2++) {
                    Double[] dArr2 = this.m_UIWave[i];
                    dArr2[k2] = Double.valueOf(dArr2[k2].doubleValue() + (Math.sin(((((double) k2) * 0.09817477042468103d) * ((double) j)) - radian2) * sqrt2 * this.m_HarmonicValue[i][j].doubleValue()));
                    if (this.m_UIWave[i][k2].doubleValue() > this.m_UIPPValue[i * 2].doubleValue()) {
                        this.m_UIPPValue[i * 2] = this.m_UIWave[i][k2];
                    }
                    if (this.m_UIWave[i][k2].doubleValue() < this.m_UIPPValue[(i * 2) + 1].doubleValue()) {
                        this.m_UIPPValue[(i * 2) + 1] = this.m_UIWave[i][k2];
                    }
                }
            }
            this.m_UIValue[i] = Double.valueOf(Math.sqrt((double) (1.0f + this.m_THDValue[i])) * this.m_HarmonicValue[i][0].doubleValue());
            this.m_CFValue[i] = Double.valueOf((Math.abs(this.m_UIPPValue[i * 2].doubleValue()) > Math.abs(this.m_UIPPValue[(i * 2) + 1].doubleValue()) ? Math.abs(this.m_UIPPValue[i * 2].doubleValue()) : Math.abs(this.m_UIPPValue[(i * 2) + 1].doubleValue())) / this.m_UIValue[i].doubleValue());
            this.m_THDValue[i] = (float) Math.sqrt((double) this.m_THDValue[i]);
        }
        this.m_PowerValue[3] = Double.valueOf(0.0d);
        this.m_PowerValue[7] = Double.valueOf(0.0d);
        this.m_PowerValue[11] = Double.valueOf(0.0d);
        for (int i2 = 0; i2 < 3; i2++) {
            CalLLVoltage(i2);
            this.m_LNVoltageCurrentAngle[i2] = Double.valueOf(AngleUtil.adjustAngle(this.m_HarmonicAngle[i2][0].doubleValue() - this.m_HarmonicAngle[i2 + 3][0].doubleValue()));
            double radian3 = this.m_LNVoltageCurrentAngle[i2].doubleValue() * angleToRadian;
            this.m_PowerValue[i2 + 8] = Double.valueOf(this.m_UIValue[i2].doubleValue() * this.m_UIValue[i2 + 3].doubleValue());
            this.m_PowerValue[i2] = Double.valueOf(this.m_PowerValue[i2 + 8].doubleValue() * Math.cos(radian3));
            Double[] dArr3 = this.m_PowerValue;
            dArr3[3] = Double.valueOf(dArr3[3].doubleValue() + this.m_PowerValue[i2].doubleValue());
            this.m_PowerValue[i2 + 4] = Double.valueOf(this.m_PowerValue[i2 + 8].doubleValue() * Math.sin(radian3));
            Double[] dArr4 = this.m_PowerValue;
            dArr4[7] = Double.valueOf(dArr4[7].doubleValue() + this.m_PowerValue[i2 + 4].doubleValue());
            this.m_CosValue[i2] = Double.valueOf(this.m_PowerValue[i2].doubleValue() / this.m_PowerValue[i2 + 8].doubleValue());
            this.m_ApparentPower[i2][0] = Double.valueOf(this.m_HarmonicValue[i2][0].doubleValue() * this.m_HarmonicValue[i2 + 3][0].doubleValue());
            this.m_ActivePower[i2][0] = Double.valueOf(this.m_ApparentPower[i2][0].doubleValue() * Math.cos(radian3));
            this.m_ReactivePower[i2][0] = Double.valueOf(this.m_ApparentPower[i2][0].doubleValue() * Math.sin(radian3));
            for (int j2 = 1; j2 < 63; j2++) {
                this.m_ApparentPower[i2][j2] = Double.valueOf(this.m_HarmonicValue[i2][j2].doubleValue() * this.m_HarmonicValue[i2 + 3][j2].doubleValue());
                double radian4 = (((this.m_LNVoltageCurrentAngle[i2].doubleValue() * ((double) j2)) + this.m_HarmonicAngle[i2][j2].doubleValue()) - this.m_HarmonicAngle[i2 + 3][j2].doubleValue()) * angleToRadian;
                this.m_ActivePower[i2][j2] = Double.valueOf(this.m_ApparentPower[i2][j2].doubleValue() * Math.cos(radian4));
                this.m_ReactivePower[i2][j2] = Double.valueOf(this.m_ApparentPower[i2][j2].doubleValue() * Math.sin(radian4));
            }
            this.m_PQPPValue[i2 * 2] = Double.valueOf(0.0d);
            this.m_PQPPValue[(i2 * 2) + 1] = Double.valueOf(0.0d);
            this.m_PQPPValue[(i2 * 2) + 6] = Double.valueOf(0.0d);
            this.m_PQPPValue[(i2 * 2) + 7] = Double.valueOf(0.0d);
            for (int k3 = 0; k3 < 192; k3++) {
                this.m_PQWave[i2][k3] = Double.valueOf(this.m_UIWave[i2][k3].doubleValue() * this.m_UIWave[i2 + 3][k3].doubleValue());
                if (this.m_PQWave[i2][k3].doubleValue() > this.m_PQPPValue[i2 * 2].doubleValue()) {
                    this.m_PQPPValue[i2 * 2] = this.m_PQWave[i2][k3];
                }
                if (this.m_PQWave[i2][k3].doubleValue() < this.m_PQPPValue[(i2 * 2) + 1].doubleValue()) {
                    this.m_PQPPValue[(i2 * 2) + 1] = this.m_PQWave[i2][k3];
                }
                this.m_PQWave[i2 + 3][k3] = Double.valueOf(this.m_UIWave[i2][k3].doubleValue() * this.m_UIWave[i2 + 3][(k3 + 16) % 192].doubleValue());
                if (this.m_PQWave[i2 + 3][k3].doubleValue() > this.m_PQPPValue[(i2 * 2) + 6].doubleValue()) {
                    this.m_PQPPValue[(i2 * 2) + 6] = this.m_PQWave[i2 + 3][k3];
                }
                if (this.m_PQWave[i2 + 3][k3].doubleValue() < this.m_PQPPValue[(i2 * 2) + 7].doubleValue()) {
                    this.m_PQPPValue[(i2 * 2) + 7] = this.m_PQWave[i2 + 3][k3];
                }
            }
        }
        this.m_LLVoltageAngle[3] = Double.valueOf(AngleUtil.adjustAngle(this.m_LLVoltageAngle[1].doubleValue() - 180.0d));
        this.m_PowerValue[11] = Double.valueOf(Math.sqrt((this.m_PowerValue[3].doubleValue() * this.m_PowerValue[3].doubleValue()) + (this.m_PowerValue[7].doubleValue() * this.m_PowerValue[7].doubleValue())));
        this.m_CosValue[3] = Double.valueOf(this.m_PowerValue[3].doubleValue() / this.m_PowerValue[11].doubleValue());
        this.m_Frequency = (float) (((double) this.m_Frequency) + ((Math.random() - 0.5d) * 0.02d));
        this.m_NeutralMeasurement[0] = Double.valueOf(Math.abs(this.m_NeutralMeasurement[0].doubleValue() + (this.m_BasicValue[0].doubleValue() * (Math.random() - 0.5d) * 2.0E-4d)));
        this.m_NeutralMeasurement[1] = Double.valueOf(Math.abs(this.m_NeutralMeasurement[1].doubleValue() + (this.m_BasicValue[3].doubleValue() * (Math.random() - 0.5d) * 2.0E-4d)));
        getAngles();
    }

    private void startFunction() {
        if (1 == this.m_StartFlag && 67 == this.m_Function) {
            if (this.m_FirstStart == 0) {
                this.m_FirstStart = 1;
                this.m_IntegrationIntervalCal = this.m_IntegrationInterval;
                this.m_IntegrationFlag = 0;
            } else {
                this.m_IntegrationIntervalCal--;
                if (this.m_IntegrationIntervalCal == 0) {
                    this.m_IntegrationIntervalCal = this.m_IntegrationInterval;
                    this.m_IntegrationFlag = 1;
                }
            }
        }
        if (1 == this.m_StartFlag && 33 == this.m_Function && this.m_FirstStart == 0) {
            for (int i = 0; i < 3; i++) {
                this.m_ErrorTest[i].Start();
            }
            this.m_FirstStart = 1;
        }
        if (1 == this.m_StartFlag && 35 == this.m_Function) {
            if (this.m_FirstStart == 0) {
                this.m_EnergyTest.Init();
                this.m_FirstStart = 1;
            } else {
                this.m_EnergyTest.CalEnergy();
            }
        }
        if (1 == this.m_StartFlag && 39 == this.m_Function) {
            if (this.m_FirstStart == 0) {
                this.m_DemandTest.Init();
                this.m_FirstStart = 1;
            } else {
                this.m_DemandTest.CalEnergy();
            }
        }
        if (this.m_StartFlag == 0) {
            this.m_FirstStart = 0;
            for (int i2 = 0; i2 < 3; i2++) {
                this.m_ErrorTest[i2].Stop();
            }
        }
    }

    @Override // com.clou.rs350.device.BaseDevice, com.clou.rs350.device.MeterBaseDevice
    public int UnPacket(byte[] ReceiveBuffer) {
        try {
            CalMeasureValue();
            startFunction();
            return 0;
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    @Override // com.clou.rs350.device.clinterface.IReadMeter, com.clou.rs350.device.MeterBaseDevice
    public List<byte[]> sendBuffer(int comPort, List<byte[]> buffer) {
        return buffer;
    }

    @Override // com.clou.rs350.device.clinterface.IReadMeter, com.clou.rs350.device.MeterBaseDevice
    public ConnectByteModel getReceiveModel() {
        return new ConnectByteModel();
    }

    @Override // com.clou.rs350.device.MeterBaseDevice, com.clou.rs350.device.clinterface.ITest
    public List<byte[]> setCheckKFlag(int flag) {
        return new ArrayList();
    }

    @Override // com.clou.rs350.device.MeterBaseDevice, com.clou.rs350.device.clinterface.ITest
    public ExplainedModel[] getEnergyValues() {
        return new ExplainedModel[]{DataUtils.parseRound(this.m_EnergyTest.Energys[0], 4), DataUtils.parseRound(this.m_EnergyTest.Energys[0], 4), DataUtils.parseRound(this.m_EnergyTest.Energys[1], 4), DataUtils.parseRound(this.m_EnergyTest.Energys[2], 4)};
    }

    @Override // com.clou.rs350.device.MeterBaseDevice, com.clou.rs350.device.clinterface.ITest
    public ExplainedModel[] getDemandValues() {
        return new ExplainedModel[]{DataUtils.parse(Double.valueOf(this.m_DemandTest.Energy.doubleValue() / ((double) this.m_DemandTest.Count)), "W")};
    }

    @Override // com.clou.rs350.device.MeterBaseDevice, com.clou.rs350.device.clinterface.ITest
    public List<byte[]> setErrorParameters(float[] constants, int[] pulses) {
        for (int i = 0; i < 3; i++) {
            this.m_ErrorTest[i].Constant = constants[i];
            this.m_ErrorTest[i].Pulse = pulses[i];
        }
        return new ArrayList();
    }

    @Override // com.clou.rs350.device.MeterBaseDevice, com.clou.rs350.device.clinterface.ITest
    public List<byte[]> setErrorParameters(int[] measurementType) {
        for (int i = 0; i < 3; i++) {
            this.m_ErrorTest[i].IsTest = measurementType[i];
        }
        return new ArrayList();
    }

    @Override // com.clou.rs350.device.MeterBaseDevice, com.clou.rs350.device.clinterface.ITest
    public List<byte[]> setErrorParameters(int[] measurementType, float[] constants, int[] pulses) {
        for (int i = 0; i < 3; i++) {
            this.m_ErrorTest[i].IsTest = measurementType[i];
            this.m_ErrorTest[i].Constant = constants[i];
            this.m_ErrorTest[i].Pulse = pulses[i];
        }
        return new ArrayList();
    }

    @Override // com.clou.rs350.device.MeterBaseDevice, com.clou.rs350.device.clinterface.ITest
    public List<byte[]> setErrorParameters(int[] errorCount, int[] measurementType, float[] constants, int[] pulses) {
        for (int i = 0; i < 3; i++) {
            this.m_ErrorTest[i].IsTest = measurementType[i];
            this.m_ErrorTest[i].Constant = constants[i];
            this.m_ErrorTest[i].Pulse = pulses[i];
            this.m_ErrorTest[i].ErrorCount = errorCount[i];
        }
        return new ArrayList();
    }

    @Override // com.clou.rs350.device.MeterBaseDevice, com.clou.rs350.device.clinterface.ITest
    public List<byte[]> setErrorParameters(double ct, int[] errorCount, int[] measurementType, float[] constants, int[] pulses) {
        for (int i = 0; i < 3; i++) {
            this.m_ErrorTest[i].IsTest = measurementType[i];
            this.m_ErrorTest[i].Constant = constants[i];
            this.m_ErrorTest[i].Pulse = pulses[i];
            this.m_ErrorTest[i].ErrorCount = errorCount[i];
        }
        return new ArrayList();
    }

    @Override // com.clou.rs350.device.clinterface.IParameterSetting, com.clou.rs350.device.MeterBaseDevice
    public List<byte[]> cleanOverload() {
        return new ArrayList();
    }

    @Override // com.clou.rs350.device.MeterBaseDevice, com.clou.rs350.device.clinterface.ITest
    public List<byte[]> setDailyParameters(int[] measurementType, int[] constants, int[] pulses) {
        return new ArrayList();
    }

    @Override // com.clou.rs350.device.clinterface.IParameterSetting, com.clou.rs350.device.MeterBaseDevice
    public List<byte[]> setWiringData(int Wiring, int Pulse) {
        this.m_Wiring = Wiring;
        return new ArrayList();
    }

    @Override // com.clou.rs350.device.clinterface.IParameterSetting, com.clou.rs350.device.MeterBaseDevice
    public List<byte[]> setPTCTRatio(double VT, double CR, int RatioApply) {
        return new ArrayList();
    }

    @Override // com.clou.rs350.device.clinterface.IParameterSetting, com.clou.rs350.device.MeterBaseDevice
    public List<byte[]> setRange(int AutoConnectType, int connectType, int AutoRange, double UaRange, double UbRange, double UcRange, double IaRange, double IbRange, double IcRange) {
        return new ArrayList();
    }

    @Override // com.clou.rs350.device.clinterface.IParameterSetting, com.clou.rs350.device.MeterBaseDevice
    public List<byte[]> setConnectType(int ConnectType) {
        return new ArrayList();
    }

    @Override // com.clou.rs350.device.clinterface.IParameterSetting, com.clou.rs350.device.MeterBaseDevice
    public List<byte[]> setParameter(int Wiring, int Pluse, int Auto, int UaRange, int UbRange, int UcRange, int IaRange, int IbRange, int IcRange) {
        return new ArrayList();
    }

    @Override // com.clou.rs350.device.clinterface.IParameterSetting, com.clou.rs350.device.MeterBaseDevice
    public List<byte[]> setStandardInfo(StandardInfo info) {
        return new ArrayList();
    }

    @Override // com.clou.rs350.device.clinterface.IParameterSetting, com.clou.rs350.device.MeterBaseDevice
    public StandardInfo getStandardInfoValue(StandardInfo info) {
        info.CalibrateDate = new Date(System.currentTimeMillis());
        info.Calib_Valid_Date = new Date(System.currentTimeMillis() + 31536000000L);
        info.CertificateID = PdfObject.NOTHING;
        info.Calibrated_By = PdfObject.NOTHING;
        info.MainBoardVersion = PdfObject.NOTHING;
        info.MeasureBoardVersion = PdfObject.NOTHING;
        info.NeutralBoardVersion = PdfObject.NOTHING;
        return info;
    }

    @Override // com.clou.rs350.device.clinterface.IParameterSetting, com.clou.rs350.device.MeterBaseDevice
    public List<byte[]> setConstant(long constant) {
        return new ArrayList();
    }

    @Override // com.clou.rs350.device.clinterface.IParameterSetting, com.clou.rs350.device.MeterBaseDevice
    public List<byte[]> setStart(int start) {
        this.m_StartFlag = start;
        return new ArrayList();
    }

    @Override // com.clou.rs350.device.clinterface.IParameterSetting, com.clou.rs350.device.MeterBaseDevice
    public List<byte[]> setFunc_Mstate(int state) {
        this.m_Function = state;
        return new ArrayList();
    }

    @Override // com.clou.rs350.device.clinterface.IParameterSetting, com.clou.rs350.device.MeterBaseDevice
    public List<byte[]> setFunc_Mstate_Start(int state, int start) {
        this.m_Function = state;
        this.m_StartFlag = start;
        return new ArrayList();
    }

    @Override // com.clou.rs350.device.MeterBaseDevice, com.clou.rs350.device.clinterface.IAdjust
    public List<byte[]> setAdjust(double UaValue, double UbValue, double UcValue, double IaValue, double IbValue, double IcValue, double UaAngle, double UbAngle, double UcAngle, double IaAngle, double IbAngle, double IcAngle) {
        double[] adjustValue = {UaValue, UbValue, UcValue, IaValue, IbValue, IcValue, UaAngle, UbAngle, UcAngle, IaAngle, IbAngle, IcAngle};
        for (int i = 0; i < 6; i++) {
            if (-1.0d != adjustValue[i]) {
                this.m_BasicValue[i] = Double.valueOf(adjustValue[i]);
                this.m_HarmonicValue[i][0] = Double.valueOf(adjustValue[i]);
            }
            if (-1.0d != adjustValue[i + 6]) {
                if (this.m_AngleDefinition == 0) {
                    this.m_HarmonicAngle[i][0] = Double.valueOf(AngleUtil.adjustAngle(adjustValue[i + 6]));
                } else {
                    this.m_HarmonicAngle[i][0] = Double.valueOf(AngleUtil.adjustAngle(360.0d - adjustValue[i + 6]));
                }
            }
        }
        return new ArrayList();
    }

    @Override // com.clou.rs350.device.MeterBaseDevice, com.clou.rs350.device.clinterface.IAdjust
    public List<byte[]> setAdjustNeutral(double UnValue, double InValue) {
        this.m_NeutralMeasurement[0] = Double.valueOf(UnValue);
        this.m_NeutralMeasurement[1] = Double.valueOf(InValue);
        return new ArrayList();
    }

    @Override // com.clou.rs350.device.MeterBaseDevice, com.clou.rs350.device.clinterface.IAdjust
    public List<byte[]> setAdjustValue(double UaValue, double UbValue, double UcValue, double IaValue, double IbValue, double IcValue, double UaAngle, double UbAngle, double UcAngle, double IaAngle, double IbAngle, double IcAngle) {
        return setAdjust(UaValue, UbValue, UcValue, IaValue, IbValue, IcValue, UaAngle, UbAngle, UcAngle, IaAngle, IbAngle, IcAngle);
    }

    @Override // com.clou.rs350.device.MeterBaseDevice, com.clou.rs350.device.clinterface.IAdjust
    public List<byte[]> setStopAdjust() {
        return new ArrayList();
    }

    @Override // com.clou.rs350.device.MeterBaseDevice, com.clou.rs350.device.clinterface.IAdjust
    public List<byte[]> setAdjustFlag(int flag) {
        return new ArrayList();
    }

    @Override // com.clou.rs350.device.clinterface.IMeter, com.clou.rs350.device.MeterBaseDevice
    public ExplainedModel[] getUIPPValue() {
        return new ExplainedModel[]{DataUtils.parse(this.m_UIPPValue[0], "V"), DataUtils.parse(this.m_UIPPValue[1], "V"), DataUtils.parse(this.m_UIPPValue[6], "A"), DataUtils.parse(this.m_UIPPValue[7], "A"), DataUtils.parse(this.m_UIPPValue[2], "V"), DataUtils.parse(this.m_UIPPValue[3], "V"), DataUtils.parse(this.m_UIPPValue[8], "A"), DataUtils.parse(this.m_UIPPValue[9], "A"), DataUtils.parse(this.m_UIPPValue[4], "V"), DataUtils.parse(this.m_UIPPValue[5], "V"), DataUtils.parse(this.m_UIPPValue[10], "A"), DataUtils.parse(this.m_UIPPValue[11], "A")};
    }

    @Override // com.clou.rs350.device.clinterface.IMeter, com.clou.rs350.device.MeterBaseDevice
    public ExplainedModel[] getPQPPValue() {
        return new ExplainedModel[]{DataUtils.parse(this.m_PQPPValue[0], "W"), DataUtils.parse(this.m_PQPPValue[1], "W"), DataUtils.parse(this.m_PQPPValue[6], "var"), DataUtils.parse(this.m_PQPPValue[7], "var"), DataUtils.parse(this.m_PQPPValue[2], "W"), DataUtils.parse(this.m_PQPPValue[3], "W"), DataUtils.parse(this.m_PQPPValue[8], "var"), DataUtils.parse(this.m_PQPPValue[9], "var"), DataUtils.parse(this.m_PQPPValue[4], "W"), DataUtils.parse(this.m_PQPPValue[5], "W"), DataUtils.parse(this.m_PQPPValue[10], "var"), DataUtils.parse(this.m_PQPPValue[11], "var")};
    }

    @Override // com.clou.rs350.device.clinterface.IParameterSetting, com.clou.rs350.device.MeterBaseDevice
    public List<byte[]> setCalculateType(int apparentPower, int harmonicDefinition) {
        return new ArrayList();
    }

    @Override // com.clou.rs350.device.MeterBaseDevice, com.clou.rs350.device.clinterface.IIntegration
    public List<byte[]> setIntegrationParameter(int Period, int Interval) {
        this.m_IntegrationInterval = Interval;
        return new ArrayList();
    }

    @Override // com.clou.rs350.device.clinterface.IMeter, com.clou.rs350.device.MeterBaseDevice
    public ExplainedModel[] getLNVoltageValue() {
        return new ExplainedModel[]{DataUtils.parse(this.m_UIValue[0], "V"), DataUtils.parse(this.m_UIValue[1], "V"), DataUtils.parse(this.m_UIValue[2], "V")};
    }

    @Override // com.clou.rs350.device.clinterface.IMeter, com.clou.rs350.device.MeterBaseDevice
    public ExplainedModel[] getLLVoltageValue() {
        return new ExplainedModel[]{DataUtils.parse(this.m_LLVoltageValue[0], "V"), DataUtils.parse(this.m_LLVoltageValue[1], "V"), DataUtils.parse(this.m_LLVoltageValue[2], "V")};
    }

    @Override // com.clou.rs350.device.clinterface.IMeter, com.clou.rs350.device.MeterBaseDevice
    public ExplainedModel[] getCurrentValue() {
        return new ExplainedModel[]{DataUtils.parse(this.m_UIValue[3], "A"), DataUtils.parse(this.m_UIValue[4], "A"), DataUtils.parse(this.m_UIValue[5], "A")};
    }

    @Override // com.clou.rs350.device.clinterface.IMeter, com.clou.rs350.device.MeterBaseDevice
    public ExplainedModel[] getPValue() {
        return new ExplainedModel[]{DataUtils.parse(this.m_PowerValue[0], "W"), DataUtils.parse(this.m_PowerValue[1], "W"), DataUtils.parse(this.m_PowerValue[2], "W"), DataUtils.parse(this.m_PowerValue[3], "W")};
    }

    @Override // com.clou.rs350.device.clinterface.IMeter, com.clou.rs350.device.MeterBaseDevice
    public ExplainedModel[] getQValue() {
        return new ExplainedModel[]{DataUtils.parse(this.m_PowerValue[4], "var"), DataUtils.parse(this.m_PowerValue[5], "var"), DataUtils.parse(this.m_PowerValue[6], "var"), DataUtils.parse(this.m_PowerValue[7], "var")};
    }

    @Override // com.clou.rs350.device.clinterface.IMeter, com.clou.rs350.device.MeterBaseDevice
    public ExplainedModel[] getSValue() {
        return new ExplainedModel[]{DataUtils.parse(this.m_PowerValue[8], "VA"), DataUtils.parse(this.m_PowerValue[9], "VA"), DataUtils.parse(this.m_PowerValue[10], "VA"), DataUtils.parse(this.m_PowerValue[11], "VA")};
    }

    @Override // com.clou.rs350.device.clinterface.IMeter, com.clou.rs350.device.MeterBaseDevice
    public ExplainedModel[] getCosValue() {
        return new ExplainedModel[]{DataUtils.parseRound(this.m_CosValue[0], 4), DataUtils.parseRound(this.m_CosValue[1], 4), DataUtils.parseRound(this.m_CosValue[2], 4), DataUtils.parseRound(this.m_CosValue[3], 4)};
    }

    @Override // com.clou.rs350.device.clinterface.IMeter, com.clou.rs350.device.MeterBaseDevice
    public ExplainedModel[] getLNVoltageAngleValue() {
        return new ExplainedModel[]{DataUtils.parse(this.m_LNVoltageAngle[0], "°"), DataUtils.parse(this.m_LNVoltageAngle[1], "°"), DataUtils.parse(this.m_LNVoltageAngle[2], "°")};
    }

    @Override // com.clou.rs350.device.clinterface.IMeter, com.clou.rs350.device.MeterBaseDevice
    public ExplainedModel[] getLLVoltageAngleValue() {
        return new ExplainedModel[]{DataUtils.parse(this.m_LLVoltageAngle[0], "°"), DataUtils.parse(this.m_LLVoltageAngle[1], "°"), DataUtils.parse(this.m_LLVoltageAngle[2], "°"), DataUtils.parse(this.m_LLVoltageAngle[3], "°")};
    }

    @Override // com.clou.rs350.device.clinterface.IMeter, com.clou.rs350.device.MeterBaseDevice
    public ExplainedModel[] getFrequencyValue() {
        return new ExplainedModel[]{DataUtils.parse(Float.valueOf(this.m_Frequency), "Hz")};
    }

    @Override // com.clou.rs350.device.clinterface.IMeter, com.clou.rs350.device.MeterBaseDevice
    public ExplainedModel[] getCurrentAngleValue() {
        return new ExplainedModel[]{DataUtils.parse(this.m_CurrentAngle[0], "°"), DataUtils.parse(this.m_CurrentAngle[1], "°"), DataUtils.parse(this.m_CurrentAngle[2], "°")};
    }

    @Override // com.clou.rs350.device.clinterface.IMeter, com.clou.rs350.device.MeterBaseDevice
    public ExplainedModel[] getNeutralValue() {
        return new ExplainedModel[]{DataUtils.parse(this.m_NeutralMeasurement[0], "V"), DataUtils.parse(this.m_NeutralMeasurement[1], "A")};
    }

    @Override // com.clou.rs350.device.MeterBaseDevice, com.clou.rs350.device.clinterface.IAdjust
    public boolean getAdjustResult() {
        return true;
    }

    @Override // com.clou.rs350.device.clinterface.IMeter, com.clou.rs350.device.MeterBaseDevice
    public ExplainedModel[] getHarmonicValuesValue(int Phase, int Flag) {
        Object[] Data;
        String Util;
        Object[] objArr = new Object[0];
        switch (Flag) {
            case 1:
                Data = this.m_HarmonicValue[Phase + 3];
                Util = "A";
                break;
            case 2:
                Data = this.m_ActivePower[Phase];
                Util = "W";
                break;
            case 3:
                Data = this.m_ReactivePower[Phase];
                Util = "var";
                break;
            case 4:
                Data = this.m_ApparentPower[Phase];
                Util = "VA";
                break;
            default:
                Data = this.m_HarmonicValue[Phase];
                Util = "V";
                break;
        }
        return DataUtils.parseDoubleArray(Data, Util);
    }

    @Override // com.clou.rs350.device.clinterface.IMeter, com.clou.rs350.device.MeterBaseDevice
    public Double[] getHarmonicContantsValue(int Phase) {
        return this.m_HarmonicValue[Phase];
    }

    @Override // com.clou.rs350.device.clinterface.IMeter, com.clou.rs350.device.MeterBaseDevice
    public Double[] getHarmonicAnglesValue(int Phase) {
        return DataUtils.parseDoubleArray(this.m_HarmonicAngle[Phase], 4, this.m_AngleDefinition);
    }

    @Override // com.clou.rs350.device.clinterface.IMeter, com.clou.rs350.device.MeterBaseDevice
    public ExplainedModel[] getTHDValue() {
        return new ExplainedModel[]{DataUtils.parseRound(Float.valueOf(this.m_THDValue[0] * 100.0f), 2, "%"), DataUtils.parseRound(Float.valueOf(this.m_THDValue[1] * 100.0f), 2, "%"), DataUtils.parseRound(Float.valueOf(this.m_THDValue[2] * 100.0f), 2, "%"), DataUtils.parseRound(Float.valueOf(this.m_THDValue[3] * 100.0f), 2, "%"), DataUtils.parseRound(Float.valueOf(this.m_THDValue[4] * 100.0f), 2, "%"), DataUtils.parseRound(Float.valueOf(this.m_THDValue[5] * 100.0f), 2, "%")};
    }

    @Override // com.clou.rs350.device.clinterface.IMeter, com.clou.rs350.device.MeterBaseDevice
    public Double[] getUIWaveValue(int Phase) {
        return this.m_UIWave[Phase];
    }

    @Override // com.clou.rs350.device.clinterface.IMeter, com.clou.rs350.device.MeterBaseDevice
    public Double[] getPQWaveValue(int Phase) {
        return this.m_PQWave[Phase];
    }

    @Override // com.clou.rs350.device.clinterface.IMeter, com.clou.rs350.device.MeterBaseDevice
    public ExplainedModel[] getVoltageCrestFactorValue() {
        return new ExplainedModel[]{DataUtils.parseRound(this.m_CFValue[0], 3), DataUtils.parseRound(this.m_CFValue[1], 3), DataUtils.parseRound(this.m_CFValue[2], 3)};
    }

    @Override // com.clou.rs350.device.clinterface.IMeter, com.clou.rs350.device.MeterBaseDevice
    public ExplainedModel[] getCurrentCrestFactorValue() {
        return new ExplainedModel[]{DataUtils.parseRound(this.m_CFValue[3], 3), DataUtils.parseRound(this.m_CFValue[4], 3), DataUtils.parseRound(this.m_CFValue[5], 3)};
    }

    @Override // com.clou.rs350.device.clinterface.IParameterSetting, com.clou.rs350.device.MeterBaseDevice
    public int getBatteryValue() {
        return 100;
    }

    @Override // com.clou.rs350.device.clinterface.IParameterSetting, com.clou.rs350.device.MeterBaseDevice
    public String getVersionValue() {
        return PdfObject.NOTHING;
    }

    @Override // com.clou.rs350.device.clinterface.IParameterSetting, com.clou.rs350.device.MeterBaseDevice
    public int getStartStateValue() {
        return this.m_StartFlag;
    }

    @Override // com.clou.rs350.device.clinterface.IParameterSetting, com.clou.rs350.device.MeterBaseDevice
    public int getFunctionValue() {
        return this.m_Function;
    }

    @Override // com.clou.rs350.device.clinterface.IParameterSetting, com.clou.rs350.device.MeterBaseDevice
    public ExplainedModel[] getRangeValue() {
        ExplainedModel[] tmpValue = getConnectTypeValue();
        try {
            return new ExplainedModel[]{new ExplainedModel(2, ((String[]) this.VoltageRange.get(tmpValue[0].getIntValue()))[2 - this.VoltageRangeIndex[tmpValue[0].getIntValue()]]), new ExplainedModel(2, ((String[]) this.VoltageRange.get(tmpValue[1].getIntValue()))[2 - this.VoltageRangeIndex[tmpValue[1].getIntValue()]]), new ExplainedModel(2, ((String[]) this.VoltageRange.get(tmpValue[2].getIntValue()))[2 - this.VoltageRangeIndex[tmpValue[2].getIntValue()]]), new ExplainedModel(22, ((String[]) this.CurrentRange.get(tmpValue[3].getIntValue()))[22 - this.CurrentRangeIndex[tmpValue[3].getIntValue()]]), new ExplainedModel(22, ((String[]) this.CurrentRange.get(tmpValue[4].getIntValue()))[22 - this.CurrentRangeIndex[tmpValue[4].getIntValue()]]), new ExplainedModel(22, ((String[]) this.CurrentRange.get(tmpValue[5].getIntValue()))[22 - this.CurrentRangeIndex[tmpValue[5].getIntValue()]])};
        } catch (Exception e) {
            return new ExplainedModel[]{new ExplainedModel(((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.ReadRange[0]))).Data[0], PdfObject.NOTHING), new ExplainedModel(((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.ReadRange[1]))).Data[0], PdfObject.NOTHING), new ExplainedModel(((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.ReadRange[2]))).Data[0], PdfObject.NOTHING), new ExplainedModel(((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.ReadRange[3]))).Data[0], PdfObject.NOTHING), new ExplainedModel(((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.ReadRange[4]))).Data[0], PdfObject.NOTHING), new ExplainedModel(((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.ReadRange[5]))).Data[0], PdfObject.NOTHING)};
        }
    }

    @Override // com.clou.rs350.device.clinterface.IParameterSetting, com.clou.rs350.device.MeterBaseDevice
    public ExplainedModel[] getConnectTypeValue() {
        try {
            return new ExplainedModel[]{new ExplainedModel(0, this.S_ConnectType_Voltage[0]), new ExplainedModel(0, this.S_ConnectType_Voltage[0]), new ExplainedModel(0, this.S_ConnectType_Voltage[0]), new ExplainedModel(2, this.S_ConnectType_Current[2]), new ExplainedModel(2, this.S_ConnectType_Current[2]), new ExplainedModel(2, this.S_ConnectType_Current[2])};
        } catch (Exception e) {
            return new ExplainedModel[]{new ExplainedModel(0, PdfObject.NOTHING), new ExplainedModel(0, PdfObject.NOTHING), new ExplainedModel(0, PdfObject.NOTHING), new ExplainedModel(2, PdfObject.NOTHING), new ExplainedModel(2, PdfObject.NOTHING), new ExplainedModel(2, PdfObject.NOTHING)};
        }
    }

    @Override // com.clou.rs350.device.clinterface.IParameterSetting, com.clou.rs350.device.MeterBaseDevice
    public long getConstantValue() {
        return 0;
    }

    @Override // com.clou.rs350.device.MeterBaseDevice, com.clou.rs350.device.clinterface.ITest
    public int getCheckKFlagValue() {
        return 0;
    }

    @Override // com.clou.rs350.device.MeterBaseDevice, com.clou.rs350.device.clinterface.ITest
    public ExplainedModel getMeterConstantValues(int index) {
        return new ExplainedModel();
    }

    @Override // com.clou.rs350.device.clinterface.IParameterSetting, com.clou.rs350.device.MeterBaseDevice
    public boolean[] getOverloadValue() {
        return new boolean[6];
    }

    @Override // com.clou.rs350.device.MeterBaseDevice, com.clou.rs350.device.clinterface.IIntegration
    public int getIntegrationFlagValue() {
        int flag = this.m_IntegrationFlag;
        if (1 == this.m_IntegrationFlag) {
            this.m_IntegrationFlag = 0;
        }
        return flag;
    }

    @Override // com.clou.rs350.device.MeterBaseDevice, com.clou.rs350.device.clinterface.IIntegration
    public ExplainedModel[] getLNVoltageIntegrationValue() {
        return new ExplainedModel[]{DataUtils.parse(this.m_UIValue[0], "V"), DataUtils.parse(this.m_UIValue[1], "V"), DataUtils.parse(this.m_UIValue[2], "V")};
    }

    @Override // com.clou.rs350.device.MeterBaseDevice, com.clou.rs350.device.clinterface.IIntegration
    public ExplainedModel[] getLLVoltageIntegrationValue() {
        return new ExplainedModel[]{DataUtils.parse(this.m_LLVoltageValue[0], "V"), DataUtils.parse(this.m_LLVoltageValue[1], "V"), DataUtils.parse(this.m_LLVoltageValue[2], "V")};
    }

    @Override // com.clou.rs350.device.MeterBaseDevice, com.clou.rs350.device.clinterface.IIntegration
    public ExplainedModel[] getCurrentIntegrationValue() {
        return new ExplainedModel[]{DataUtils.parse(this.m_UIValue[3], "A"), DataUtils.parse(this.m_UIValue[4], "A"), DataUtils.parse(this.m_UIValue[5], "A")};
    }

    @Override // com.clou.rs350.device.MeterBaseDevice, com.clou.rs350.device.clinterface.IIntegration
    public ExplainedModel[] getPIntegrationValue() {
        return new ExplainedModel[]{DataUtils.parse(this.m_PowerValue[0], "W"), DataUtils.parse(this.m_PowerValue[1], "W"), DataUtils.parse(this.m_PowerValue[2], "W"), DataUtils.parse(this.m_PowerValue[3], "W")};
    }

    @Override // com.clou.rs350.device.MeterBaseDevice, com.clou.rs350.device.clinterface.IIntegration
    public ExplainedModel[] getQIntegrationValue() {
        return new ExplainedModel[]{DataUtils.parse(this.m_PowerValue[4], "var"), DataUtils.parse(this.m_PowerValue[5], "var"), DataUtils.parse(this.m_PowerValue[6], "var"), DataUtils.parse(this.m_PowerValue[7], "var")};
    }

    @Override // com.clou.rs350.device.MeterBaseDevice, com.clou.rs350.device.clinterface.IIntegration
    public ExplainedModel[] getSIntegrationValue() {
        return new ExplainedModel[]{DataUtils.parse(this.m_PowerValue[8], "VA"), DataUtils.parse(this.m_PowerValue[9], "VA"), DataUtils.parse(this.m_PowerValue[10], "VA"), DataUtils.parse(this.m_PowerValue[11], "VA")};
    }

    @Override // com.clou.rs350.device.MeterBaseDevice, com.clou.rs350.device.clinterface.IIntegration
    public ExplainedModel[] getCosIntegrationValue() {
        return new ExplainedModel[]{DataUtils.parseRound(this.m_CosValue[0], 4), DataUtils.parseRound(this.m_CosValue[1], 4), DataUtils.parseRound(this.m_CosValue[2], 4), DataUtils.parseRound(this.m_CosValue[3], 4)};
    }

    @Override // com.clou.rs350.device.MeterBaseDevice, com.clou.rs350.device.clinterface.IIntegration
    public ExplainedModel[] getSinIntegrationValue() {
        return new ExplainedModel[]{new ExplainedModel(), new ExplainedModel(), new ExplainedModel(), new ExplainedModel()};
    }

    @Override // com.clou.rs350.device.MeterBaseDevice, com.clou.rs350.device.clinterface.IIntegration
    public ExplainedModel[] getLNVoltageAngleIntegrationValue() {
        return new ExplainedModel[]{DataUtils.parse(this.m_LNVoltageAngle[0], "°"), DataUtils.parse(this.m_LNVoltageAngle[1], "°"), DataUtils.parse(this.m_LNVoltageAngle[2], "°")};
    }

    @Override // com.clou.rs350.device.MeterBaseDevice, com.clou.rs350.device.clinterface.IIntegration
    public ExplainedModel[] getLLVoltageAngleIntegrationValue() {
        return new ExplainedModel[]{new ExplainedModel(), new ExplainedModel(), new ExplainedModel(), new ExplainedModel()};
    }

    @Override // com.clou.rs350.device.MeterBaseDevice, com.clou.rs350.device.clinterface.IIntegration
    public ExplainedModel[] getFrequencyIntegrationValue() {
        return new ExplainedModel[]{DataUtils.parse(Float.valueOf(this.m_Frequency), "Hz")};
    }

    @Override // com.clou.rs350.device.MeterBaseDevice, com.clou.rs350.device.clinterface.IIntegration
    public ExplainedModel[] getCurrentAngleIntegrationValue() {
        return new ExplainedModel[]{DataUtils.parse(this.m_CurrentAngle[0], "°"), DataUtils.parse(this.m_CurrentAngle[1], "°"), DataUtils.parse(this.m_CurrentAngle[2], "°")};
    }

    @Override // com.clou.rs350.device.MeterBaseDevice, com.clou.rs350.device.clinterface.IIntegration
    public ExplainedModel[] getNeutralIntegrationValue() {
        return new ExplainedModel[]{DataUtils.parse(this.m_NeutralMeasurement[0], "V"), DataUtils.parse(this.m_NeutralMeasurement[1], "A")};
    }

    @Override // com.clou.rs350.device.MeterBaseDevice, com.clou.rs350.device.clinterface.IIntegration
    public ExplainedModel[] getLNVoltageCurrentAngleIntegrationValue() {
        return new ExplainedModel[]{DataUtils.parse(this.m_LNVoltageCurrentAngle[0], "°"), DataUtils.parse(this.m_LNVoltageCurrentAngle[1], "°"), DataUtils.parse(this.m_LNVoltageCurrentAngle[2], "°")};
    }

    @Override // com.clou.rs350.device.MeterBaseDevice, com.clou.rs350.device.clinterface.IIntegration
    public ExplainedModel[] getLLVoltageCurrentAngleIntegrationValue() {
        return new ExplainedModel[]{new ExplainedModel(), new ExplainedModel(), new ExplainedModel(), new ExplainedModel()};
    }

    @Override // com.clou.rs350.device.MeterBaseDevice, com.clou.rs350.device.clinterface.IIntegration
    public ExplainedModel[] getTHDIntegrationValue() {
        return new ExplainedModel[]{DataUtils.parseRound(Float.valueOf(this.m_THDValue[0] * 100.0f), 2, "%"), DataUtils.parseRound(Float.valueOf(this.m_THDValue[1] * 100.0f), 2, "%"), DataUtils.parseRound(Float.valueOf(this.m_THDValue[2] * 100.0f), 2, "%"), DataUtils.parseRound(Float.valueOf(this.m_THDValue[3] * 100.0f), 2, "%"), DataUtils.parseRound(Float.valueOf(this.m_THDValue[4] * 100.0f), 2, "%"), DataUtils.parseRound(Float.valueOf(this.m_THDValue[5] * 100.0f), 2, "%")};
    }

    @Override // com.clou.rs350.device.MeterBaseDevice, com.clou.rs350.device.clinterface.IIntegration
    public ExplainedModel[] getVoltageCrestFactorIntegrationValue() {
        return new ExplainedModel[]{new ExplainedModel(), new ExplainedModel(), new ExplainedModel()};
    }

    @Override // com.clou.rs350.device.MeterBaseDevice, com.clou.rs350.device.clinterface.IIntegration
    public ExplainedModel[] getCurrentCrestFactorIntegrationValue() {
        return new ExplainedModel[]{new ExplainedModel(), new ExplainedModel(), new ExplainedModel()};
    }

    @Override // com.clou.rs350.device.MeterBaseDevice, com.clou.rs350.device.clinterface.IDigitalMetetTest
    public List<byte[]> setDigitalMeterParameters(int baudrate, int dataBits, int parity, int stopBits, int address, DigitalMeterDataType[] dataType, int pt, int ct) {
        return new ArrayList();
    }

    @Override // com.clou.rs350.device.MeterBaseDevice, com.clou.rs350.device.clinterface.IDigitalMetetTest
    public List<byte[]> setDigitalMeterTestStartFlag(int Flag) {
        return new ArrayList();
    }

    @Override // com.clou.rs350.device.clinterface.IMeter, com.clou.rs350.device.MeterBaseDevice
    public ExplainedModel[] getLNVoltageCurrentAngleValue() {
        return new ExplainedModel[]{DataUtils.parse(this.m_LNVoltageCurrentAngle[0], "°"), DataUtils.parse(this.m_LNVoltageCurrentAngle[1], "°"), DataUtils.parse(this.m_LNVoltageCurrentAngle[2], "°")};
    }

    @Override // com.clou.rs350.device.clinterface.IMeter, com.clou.rs350.device.MeterBaseDevice
    public ExplainedModel[] getLLVoltageCurrentAngleValue() {
        return new ExplainedModel[]{DataUtils.parse(this.m_LLVoltageCurrentAngle[0], "°"), DataUtils.parse(this.m_LLVoltageCurrentAngle[1], "°"), DataUtils.parse(this.m_LLVoltageCurrentAngle[2], "°"), DataUtils.parse(this.m_LLVoltageCurrentAngle[3], "°")};
    }

    @Override // com.clou.rs350.device.MeterBaseDevice, com.clou.rs350.device.clinterface.IAdjust
    public List<byte[]> setDCAdjustFlag(int flag) {
        return new ArrayList();
    }

    @Override // com.clou.rs350.device.MeterBaseDevice, com.clou.rs350.device.clinterface.IAdjust
    public List<byte[]> setResetAdjustmentFlag(int flag) {
        return new ArrayList();
    }

    @Override // com.clou.rs350.device.MeterBaseDevice, com.clou.rs350.device.clinterface.IAdjust
    public boolean getAllowAdjustFlagValue(Context context) {
        return true;
    }

    @Override // com.clou.rs350.device.MeterBaseDevice, com.clou.rs350.device.clinterface.IAdjust
    public List<byte[]> cleanAllowAdjustFlag() {
        return new ArrayList();
    }

    @Override // com.clou.rs350.device.clinterface.IParameterSetting, com.clou.rs350.device.MeterBaseDevice
    public int getWiringValue() {
        return this.m_Wiring;
    }

    @Override // com.clou.rs350.device.clinterface.IParameterSetting, com.clou.rs350.device.MeterBaseDevice
    public int getDefinitionValue() {
        return 0;
    }

    @Override // com.clou.rs350.device.clinterface.IParameterSetting, com.clou.rs350.device.MeterBaseDevice
    public int getPQFlagValue() {
        return 0;
    }

    @Override // com.clou.rs350.device.clinterface.IParameterSetting, com.clou.rs350.device.MeterBaseDevice
    public int getRangeAutoValue() {
        return 0;
    }

    @Override // com.clou.rs350.device.clinterface.IParameterSetting, com.clou.rs350.device.MeterBaseDevice
    public List<byte[]> setWiringData(int Wiring, int Pulse, int Definition) {
        this.m_Wiring = Wiring;
        return new ArrayList();
    }

    @Override // com.clou.rs350.device.clinterface.IParameterSetting, com.clou.rs350.device.MeterBaseDevice
    public String getSerialNumberValue() {
        return "Demo";
    }

    @Override // com.clou.rs350.device.MeterBaseDevice, com.clou.rs350.device.clinterface.ITest
    public ExplainedModel[] getErrorsValues(int index) {
        return new ExplainedModel[]{DataUtils.parse(this.m_ErrorTest[index].Errors[0], "%"), DataUtils.parse(this.m_ErrorTest[index].Errors[1], "%"), DataUtils.parse(this.m_ErrorTest[index].Errors[2], "%"), DataUtils.parse(this.m_ErrorTest[index].Errors[3], "%"), DataUtils.parse(this.m_ErrorTest[index].Errors[4], "%"), DataUtils.parse(this.m_ErrorTest[index].Errors[5], "%"), DataUtils.parse(this.m_ErrorTest[index].Errors[6], "%"), DataUtils.parse(this.m_ErrorTest[index].Errors[7], "%"), DataUtils.parse(this.m_ErrorTest[index].Errors[8], "%"), DataUtils.parse(this.m_ErrorTest[index].Errors[9], "%"), DataUtils.parse(this.m_ErrorTest[index].Errors[10], "%"), DataUtils.parse(this.m_ErrorTest[index].Errors[11], "%"), DataUtils.parseint(Integer.valueOf(this.m_ErrorTest[index].RemainPulse)), DataUtils.parseint(Integer.valueOf(this.m_ErrorTest[index].HasErrorCount))};
    }

    @Override // com.clou.rs350.device.clinterface.IWiringCheck, com.clou.rs350.device.MeterBaseDevice
    public int getWiringRemrakValue() {
        return 33619968;
    }

    @Override // com.clou.rs350.device.clinterface.IWiringCheck, com.clou.rs350.device.MeterBaseDevice
    public int[] getVoltagePhaseSequenceValue() {
        return new int[]{1, 2, 4};
    }

    @Override // com.clou.rs350.device.clinterface.IWiringCheck, com.clou.rs350.device.MeterBaseDevice
    public int[] getCurrentPhaseSequenceValue() {
        return new int[]{1, 2, 4};
    }

    @Override // com.clou.rs350.device.clinterface.IPTCTMeasurement, com.clou.rs350.device.MeterBaseDevice
    public ExplainedModel[] getCTErrorValue() {
        return new ExplainedModel[]{DataUtils.parse(this.m_UIValue[3], "A"), DataUtils.parse(this.m_UIValue[4], "A"), new ExplainedModel(), DataUtils.parse(Double.valueOf(Math.random() - 0.5d), "°")};
    }

    @Override // com.clou.rs350.device.clinterface.IPTCTMeasurement, com.clou.rs350.device.MeterBaseDevice
    public ExplainedModel[] getCTBurdenValue() {
        return new ExplainedModel[]{DataUtils.parse(Double.valueOf(this.m_UIValue[1].doubleValue() / 100.0d), "V"), DataUtils.parse(this.m_UIValue[4], "A"), DataUtils.parse(Double.valueOf(this.m_PowerValue[1].doubleValue() / 100.0d), "VA")};
    }

    @Override // com.clou.rs350.device.clinterface.IPTCTMeasurement, com.clou.rs350.device.MeterBaseDevice
    public ExplainedModel[] getPTBurdenValue() {
        return new ExplainedModel[]{DataUtils.parse(this.m_UIValue[0], "V"), DataUtils.parse(this.m_UIValue[1], "V"), DataUtils.parse(this.m_UIValue[2], "V"), DataUtils.parse(this.m_UIValue[3], "A"), DataUtils.parse(this.m_UIValue[4], "A"), DataUtils.parse(this.m_UIValue[5], "A"), DataUtils.parse(this.m_LNVoltageCurrentAngle[0], "°"), DataUtils.parse(this.m_LNVoltageCurrentAngle[1], "°"), DataUtils.parse(this.m_LNVoltageCurrentAngle[2], "°"), DataUtils.parse(this.m_PowerValue[0], "W"), DataUtils.parse(this.m_PowerValue[1], "W"), DataUtils.parse(this.m_PowerValue[2], "W"), DataUtils.parse(this.m_PowerValue[8], "VA"), DataUtils.parse(this.m_PowerValue[9], "VA"), DataUtils.parse(this.m_PowerValue[10], "VA")};
    }

    @Override // com.clou.rs350.device.clinterface.IAdjust
    public List<byte[]> getClampAdjustmentFlag() {
        return null;
    }

    @Override // com.clou.rs350.device.clinterface.IAdjust
    public int[] getClampAdjustmentFlagValue() {
        return new int[7];
    }
}
