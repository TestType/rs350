package com.clou.rs350.device.constants;

public class CLTArrays {
    public static LinkMapList buildPowerFactorList() {
        LinkMapList list = new LinkMapList();
        list.addData(1, "1~0.7L");
        list.addData(2, "0.75L~0");
        list.addData(3, "1~0.7C");
        list.addData(4, "0.75C~0");
        return list;
    }
}
