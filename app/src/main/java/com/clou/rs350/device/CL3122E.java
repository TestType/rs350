package com.clou.rs350.device;

import com.clou.rs350.R;
import com.clou.rs350.utils.OtherUtils;

import java.util.ArrayList;

public class CL3122E extends CL3122 {
    public CL3122E() {
        super(new CL3122EDictionary());
        initRange();
    }

    /* access modifiers changed from: protected */
    @Override // com.clou.rs350.device.BaseDevice, com.clou.rs350.device.CL3122, com.clou.rs350.device.MeterBaseDevice
    public void initRange() {
        this.S_ConnectType_All = new String[]{String.valueOf(OtherUtils.getString(R.string.text_U_Direct)) + "," + OtherUtils.getString(R.string.text_I_Direct), String.valueOf(OtherUtils.getString(R.string.text_U_Direct)) + "," + OtherUtils.getString(R.string.text_I_Clamp10A), String.valueOf(OtherUtils.getString(R.string.text_U_Direct)) + "," + OtherUtils.getString(R.string.text_I_Clamp100A), String.valueOf(OtherUtils.getString(R.string.text_U_Direct)) + "," + OtherUtils.getString(R.string.text_I_Clamp500A), String.valueOf(OtherUtils.getString(R.string.text_U_Direct)) + "," + OtherUtils.getString(R.string.text_I_Clamp1000A), String.valueOf(OtherUtils.getString(R.string.text_Pri_Clamp100A)) + "," + OtherUtils.getString(R.string.text_Sec_Clamp10A), String.valueOf(OtherUtils.getString(R.string.text_Pri_Clamp500A)) + "," + OtherUtils.getString(R.string.text_Sec_Clamp10A), String.valueOf(OtherUtils.getString(R.string.text_Pri_Clamp1000A)) + "," + OtherUtils.getString(R.string.text_Sec_Clamp10A)};
        this.S_ConnectType_Voltage = new String[]{OtherUtils.getString(R.string.text_U_Direct), OtherUtils.getString(R.string.text_U_Direct), OtherUtils.getString(R.string.text_U_Direct), OtherUtils.getString(R.string.text_U_Direct), OtherUtils.getString(R.string.text_U_Direct)};
        this.S_ConnectType_Current = new String[]{OtherUtils.getString(R.string.text_I_Direct), OtherUtils.getString(R.string.text_I_Clamp10A), OtherUtils.getString(R.string.text_I_Clamp100A), OtherUtils.getString(R.string.text_I_Clamp500A), OtherUtils.getString(R.string.text_I_Clamp1000A)};
        this.VoltageRange_All = new String[]{"5", "30", "60", "120", "240", "480"};
        this.CurrentRange_All = new String[]{"0.02", "0.05", "0.1", "0.2", "0.5", "1", "2", "5", "10", "20", "50", "100", "200", "500", "1000"};
        int[] iArr = new int[8];
        iArr[1] = 1;
        iArr[2] = 2;
        iArr[3] = 3;
        iArr[4] = 4;
        iArr[5] = 5;
        iArr[6] = 6;
        iArr[7] = 7;
        this.I_ConnectType_All_Index = iArr;
        this.S_ConnectType_Realtime = new String[]{String.valueOf(OtherUtils.getString(R.string.text_U_Direct)) + "," + OtherUtils.getString(R.string.text_I_Direct), String.valueOf(OtherUtils.getString(R.string.text_U_Direct)) + "," + OtherUtils.getString(R.string.text_I_Clamp10A), String.valueOf(OtherUtils.getString(R.string.text_U_Direct)) + "," + OtherUtils.getString(R.string.text_I_Clamp100A), String.valueOf(OtherUtils.getString(R.string.text_U_Direct)) + "," + OtherUtils.getString(R.string.text_I_Clamp500A), String.valueOf(OtherUtils.getString(R.string.text_U_Direct)) + "," + OtherUtils.getString(R.string.text_I_Clamp1000A)};
        int[] iArr2 = new int[5];
        iArr2[1] = 1;
        iArr2[2] = 2;
        iArr2[3] = 3;
        iArr2[4] = 4;
        this.I_ConnectType_Realtime_Index = iArr2;
        this.S_ConnectType_CT = new String[]{String.valueOf(OtherUtils.getString(R.string.text_Pri_Clamp100A)) + "," + OtherUtils.getString(R.string.text_Sec_Clamp10A), String.valueOf(OtherUtils.getString(R.string.text_Pri_Clamp500A)) + "," + OtherUtils.getString(R.string.text_Sec_Clamp10A), String.valueOf(OtherUtils.getString(R.string.text_Pri_Clamp1000A)) + "," + OtherUtils.getString(R.string.text_Sec_Clamp10A)};
        this.I_ConnectType_CT_Index = new int[]{5, 6, 7};
        this.VoltageRange = new ArrayList();
        this.VoltageRange.add(new String[]{"5V", "30V", "60V", "120V", "240V", "480V"});
        this.VoltageRange.add(new String[]{"5V", "30V", "60V", "120V", "240V", "480V"});
        this.VoltageRange.add(new String[]{"5V", "30V", "60V", "120V", "240V", "480V"});
        this.VoltageRange.add(new String[]{"5V", "30V", "60V", "120V", "240V", "480V"});
        this.VoltageRange.add(new String[]{"5V", "30V", "60V", "120V", "240V", "480V"});
        this.VoltageRange.add(new String[]{"2A", "5A", "10A", "20A", "50A", "100A"});
        this.VoltageRange.add(new String[]{"10A", "20A", "50A", "100A", "200A", "500A"});
        this.VoltageRange.add(new String[]{"50A", "100A", "200A", "500A", "1000A"});
        int[] iArr3 = new int[8];
        iArr3[5] = 18;
        iArr3[6] = 24;
        iArr3[7] = 30;
        this.VoltageRangeIndex = iArr3;
        this.CurrentRange = new ArrayList();
        this.CurrentRange.add(new String[]{"0.02A", "0.05A", "0.1A", "0.2A", "0.5A", "1A", "2A", "5A", "10A", "20A", "50A", "100A"});
        this.CurrentRange.add(new String[]{"0.1A", "0.2A", "0.5A", "1A", "2A", "5A"});
        this.CurrentRange.add(new String[]{"2A", "5A", "10A", "20A", "50A", "100A"});
        this.CurrentRange.add(new String[]{"10A", "20A", "50A", "100A", "200A", "500A"});
        this.CurrentRange.add(new String[]{"50A", "100A", "200A", "500A", "1000A"});
        this.CurrentRange.add(new String[]{"0.1A", "0.2A", "0.5A", "1A", "2A", "5A"});
        this.CurrentRange.add(new String[]{"0.1A", "0.2A", "0.5A", "1A", "2A", "5A"});
        this.CurrentRange.add(new String[]{"0.1A", "0.2A", "0.5A", "1A", "2A", "5A"});
        int[] iArr4 = new int[8];
        iArr4[1] = 12;
        iArr4[2] = 18;
        iArr4[3] = 24;
        iArr4[4] = 30;
        iArr4[5] = 12;
        iArr4[6] = 12;
        iArr4[7] = 12;
        this.CurrentRangeIndex = iArr4;
    }
}
