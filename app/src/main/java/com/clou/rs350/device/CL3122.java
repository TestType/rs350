package com.clou.rs350.device;

import android.content.Context;

import com.clou.rs350.R;
import com.clou.rs350.device.constants.CLTConstant;
import com.clou.rs350.device.model.ConnectByteModel;
import com.clou.rs350.device.model.DataModel;
import com.clou.rs350.device.model.ExplainedModel;
import com.clou.rs350.device.utils.DataUtils;
import com.clou.rs350.utils.AngleUtil;
import com.clou.rs350.utils.FieldSettingUtil;
import com.clou.rs350.utils.OtherUtils;
import com.itextpdf.text.pdf.PdfObject;
import com.itextpdf.text.pdf.codec.TIFFConstants;

import java.util.ArrayList;
import java.util.List;

public class CL3122 extends MeterBaseDevice {
    protected CLT m_CLT;
    private int m_Wiring;

    public CL3122() {
        this(new CL3122EDictionary());
        initRange();
    }

    public CL3122(BaseDictionary dictionary) {
        this.m_Wiring = 0;
        this.m_Dictionary = dictionary;
        this.m_Data = this.m_Dictionary.getDataMap();
        this.m_CLT = new CLT(33, 0, this.m_Data);
    }

    @Override // com.clou.rs350.device.clinterface.IMeter, com.clou.rs350.device.MeterBaseDevice
    public List<byte[]> getMeasureBasicData() {
        return this.m_CLT.AskData(new int[]{this.m_Dictionary.MeterUaValue, this.m_Dictionary.MeterUbValue, this.m_Dictionary.MeterUcValue, this.m_Dictionary.MeterUabValue, this.m_Dictionary.MeterUbcValue, this.m_Dictionary.MeterUcaValue, this.m_Dictionary.MeterIaValue, this.m_Dictionary.MeterIbValue, this.m_Dictionary.MeterIcValue, this.m_Dictionary.MeterUaAngle, this.m_Dictionary.MeterUbAngle, this.m_Dictionary.MeterUcAngle, this.m_Dictionary.MeterUabAngle, this.m_Dictionary.MeterUbcAngle, this.m_Dictionary.MeterUcaAngle, this.m_Dictionary.MeterUcbAngle, this.m_Dictionary.MeterIaAngle, this.m_Dictionary.MeterIbAngle, this.m_Dictionary.MeterIcAngle, this.m_Dictionary.MeterUIaAngle, this.m_Dictionary.MeterUIbAngle, this.m_Dictionary.MeterUIcAngle, this.m_Dictionary.MeterUabIaAngle, this.m_Dictionary.MeterUbcIbAngle, this.m_Dictionary.MeterUcaIcAngle, this.m_Dictionary.MeterUcbIcAngle, this.m_Dictionary.MeterPaValue, this.m_Dictionary.MeterPbValue, this.m_Dictionary.MeterPcValue, this.m_Dictionary.MeterPSumValue, this.m_Dictionary.MeterQaValue, this.m_Dictionary.MeterQbValue, this.m_Dictionary.MeterQcValue, this.m_Dictionary.MeterQSumValue, this.m_Dictionary.MeterSaValue, this.m_Dictionary.MeterSbValue, this.m_Dictionary.MeterScValue, this.m_Dictionary.MeterSSumValue, this.m_Dictionary.MeterCosaValue, this.m_Dictionary.MeterCosbValue, this.m_Dictionary.MeterCoscValue, this.m_Dictionary.MeterCosSumValue, this.m_Dictionary.MeterFrequency});
    }

    @Override // com.clou.rs350.device.clinterface.IMeter, com.clou.rs350.device.MeterBaseDevice
    public List<byte[]> getHarmonic(int type) {
        List<byte[]> buffersList = new ArrayList<>();
        int Phase = (type >> 8) - 1;
        if ((type & 1) == 1) {
            buffersList.addAll(this.m_CLT.AskArray(new int[]{this.m_Dictionary.MeterVoltageHarmonicContent[Phase], this.m_Dictionary.MeterVoltageHarmonicAngle[Phase]}));
        }
        if ((type & 2) == 2) {
            buffersList.addAll(this.m_CLT.AskArray(new int[]{this.m_Dictionary.MeterCurrentHarmonicContent[Phase], this.m_Dictionary.MeterCurrentHarmonicAngle[Phase]}));
        }
        return buffersList;
    }

    @Override // com.clou.rs350.device.clinterface.IMeter, com.clou.rs350.device.MeterBaseDevice
    public List<byte[]> getHarmonicContents(int type) {
        List<byte[]> buffersList = new ArrayList<>();
        int Phase = (type >> 8) - 1;
        if ((type & 1) == 1) {
            buffersList.addAll(this.m_CLT.AskArray(new int[]{this.m_Dictionary.MeterVoltageHarmonicContent[Phase]}));
        }
        if ((type & 2) == 2) {
            buffersList.addAll(this.m_CLT.AskArray(new int[]{this.m_Dictionary.MeterCurrentHarmonicContent[Phase]}));
        }
        return buffersList;
    }

    @Override // com.clou.rs350.device.clinterface.IMeter, com.clou.rs350.device.MeterBaseDevice
    public List<byte[]> getHarmonicAngles(int type) {
        List<byte[]> buffersList = new ArrayList<>();
        int Phase = (type >> 8) - 1;
        if ((type & 1) == 1) {
            buffersList.addAll(this.m_CLT.AskArray(new int[]{this.m_Dictionary.MeterVoltageHarmonicAngle[Phase]}));
        }
        if ((type & 2) == 2) {
            buffersList.addAll(this.m_CLT.AskArray(new int[]{this.m_Dictionary.MeterCurrentHarmonicAngle[Phase]}));
        }
        return buffersList;
    }

    @Override // com.clou.rs350.device.clinterface.IMeter, com.clou.rs350.device.MeterBaseDevice
    public List<byte[]> getHarmonic(String type) {
        return this.m_CLT.AskArray("Ua".equals(type) ? new int[]{this.m_Dictionary.MeterVoltageHarmonicContent[0], this.m_Dictionary.MeterVoltageHarmonicAngle[0]} : "Ub".equals(type) ? new int[]{this.m_Dictionary.MeterVoltageHarmonicContent[1], this.m_Dictionary.MeterVoltageHarmonicAngle[1]} : "Uc".equals(type) ? new int[]{this.m_Dictionary.MeterVoltageHarmonicContent[2], this.m_Dictionary.MeterVoltageHarmonicAngle[2]} : "Ia".equals(type) ? new int[]{this.m_Dictionary.MeterCurrentHarmonicContent[0], this.m_Dictionary.MeterCurrentHarmonicAngle[0]} : FieldSettingUtil.IB.equals(type) ? new int[]{this.m_Dictionary.MeterCurrentHarmonicContent[1], this.m_Dictionary.MeterCurrentHarmonicAngle[1]} : "Ic".equals(type) ? new int[]{this.m_Dictionary.MeterCurrentHarmonicContent[2], this.m_Dictionary.MeterCurrentHarmonicAngle[2]} : new int[]{this.m_Dictionary.MeterVoltageHarmonicContent[0], this.m_Dictionary.MeterVoltageHarmonicAngle[0]});
    }

    @Override // com.clou.rs350.device.clinterface.IMeter, com.clou.rs350.device.MeterBaseDevice
    public List<byte[]> getTHD() {
        return this.m_CLT.AskArray(new int[]{this.m_Dictionary.MeterTHD});
    }

    @Override // com.clou.rs350.device.clinterface.IMeter, com.clou.rs350.device.MeterBaseDevice
    public List<byte[]> getHarmonic() {
        return this.m_CLT.AskArray(new int[]{this.m_Dictionary.MeterVoltageHarmonicContent[0], this.m_Dictionary.MeterVoltageHarmonicAngle[0], this.m_Dictionary.MeterVoltageHarmonicContent[1], this.m_Dictionary.MeterVoltageHarmonicAngle[1], this.m_Dictionary.MeterVoltageHarmonicContent[2], this.m_Dictionary.MeterVoltageHarmonicAngle[2], this.m_Dictionary.MeterCurrentHarmonicContent[0], this.m_Dictionary.MeterCurrentHarmonicAngle[0], this.m_Dictionary.MeterCurrentHarmonicContent[1], this.m_Dictionary.MeterCurrentHarmonicAngle[1], this.m_Dictionary.MeterCurrentHarmonicContent[2], this.m_Dictionary.MeterCurrentHarmonicAngle[2], this.m_Dictionary.MeterTHD});
    }

    @Override // com.clou.rs350.device.clinterface.IMeter, com.clou.rs350.device.MeterBaseDevice
    public List<byte[]> getUIWave() {
        return this.m_CLT.AskArray(new int[]{this.m_Dictionary.MeterUaWave, this.m_Dictionary.MeterUbWave, this.m_Dictionary.MeterUcWave, this.m_Dictionary.MeterIaWave, this.m_Dictionary.MeterIbWave, this.m_Dictionary.MeterIcWave});
    }

    @Override // com.clou.rs350.device.clinterface.IParameterSetting, com.clou.rs350.device.MeterBaseDevice
    public List<byte[]> setCalculateType(int apparentPower, int harmonicDefinition) {
        ((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.MeterSSumFlag))).Data[0] = Integer.valueOf(apparentPower);
        return this.m_CLT.WriteData(new int[]{this.m_Dictionary.MeterSSumFlag});
    }

    @Override // com.clou.rs350.device.MeterBaseDevice, com.clou.rs350.device.clinterface.IAdjust
    public List<byte[]> setAdjust(double UaValue, double UbValue, double UcValue, double IaValue, double IbValue, double IcValue, double UaAngle, double UbAngle, double UcAngle, double IaAngle, double IbAngle, double IcAngle) {
        List<byte[]> listbyte = setFunc_Mstate(CLTConstant.Func_Mstate_91);
        listbyte.addAll(setStart(1));
        double[] adjustValue = {UaValue, IaValue, UbValue, IbValue, UcValue, IcValue, UaAngle, IaAngle, UbAngle, IbAngle, UcAngle, IcAngle};
        int[] key = {this.m_Dictionary.AdjustUaValue, this.m_Dictionary.AdjustIaValue, this.m_Dictionary.AdjustUbValue, this.m_Dictionary.AdjustIbValue, this.m_Dictionary.AdjustUcValue, this.m_Dictionary.AdjustIcValue, this.m_Dictionary.AdjustUaAngle, this.m_Dictionary.AdjustIaAngle, this.m_Dictionary.AdjustUbAngle, this.m_Dictionary.AdjustIbAngle, this.m_Dictionary.AdjustUcAngle, this.m_Dictionary.AdjustIcAngle};
        int valueState = 0;
        int angleState = 0;
        for (int i = 0; i < 6; i++) {
            ((DataModel) this.m_Data.get(Integer.valueOf(key[i]))).Data[0] = Double.valueOf(adjustValue[i]);
            if (-1.0d != adjustValue[i]) {
                valueState |= 1 << i;
            }
            if (this.m_AngleDefinition == 0) {
                ((DataModel) this.m_Data.get(Integer.valueOf(key[i + 6]))).Data[0] = Double.valueOf(AngleUtil.adjustAngle(adjustValue[i + 6]));
            } else {
                ((DataModel) this.m_Data.get(Integer.valueOf(key[i + 6]))).Data[0] = Double.valueOf(AngleUtil.adjustAngle(360.0d - adjustValue[i + 6]));
            }
            if (-1.0d != adjustValue[i + 6]) {
                angleState |= 1 << i;
            }
        }
        this.m_CLT.DataMap = this.m_Data;
        listbyte.addAll(this.m_CLT.WriteData(key));
        ((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.AdjustStateValue))).Data[0] = Integer.valueOf((valueState & 63) | 192);
        ((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.AdjustStateAngle))).Data[0] = Integer.valueOf(angleState & 63);
        ((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.AdjustStateResultValue))).Data[0] = 0;
        ((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.AdjustStateResultAngle))).Data[0] = 0;
        this.m_CLT.DataMap = this.m_Data;
        listbyte.addAll(this.m_CLT.WriteData(new int[]{this.m_Dictionary.AdjustStateValue, this.m_Dictionary.AdjustStateAngle, this.m_Dictionary.AdjustStateResultValue, this.m_Dictionary.AdjustStateResultAngle}));
        return listbyte;
    }

    @Override // com.clou.rs350.device.MeterBaseDevice, com.clou.rs350.device.clinterface.IAdjust
    public List<byte[]> setAdjustValue(double UaValue, double UbValue, double UcValue, double IaValue, double IbValue, double IcValue, double UaAngle, double UbAngle, double UcAngle, double IaAngle, double IbAngle, double IcAngle) {
        return null;
    }

    @Override // com.clou.rs350.device.MeterBaseDevice, com.clou.rs350.device.clinterface.IAdjust
    public List<byte[]> getAdjustState() {
        return this.m_CLT.AskData(new int[]{this.m_Dictionary.AdjustStateResultValue, this.m_Dictionary.AdjustStateResultAngle});
    }

    @Override // com.clou.rs350.device.MeterBaseDevice, com.clou.rs350.device.clinterface.IAdjust
    public List<byte[]> setStopAdjust() {
        ((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.AdjustStateValue))).Data[0] = 0;
        ((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.AdjustStateAngle))).Data[0] = 0;
        ((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.AdjustStateResultValue))).Data[0] = 0;
        ((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.AdjustStateResultAngle))).Data[0] = 0;
        List<byte[]> buffers = this.m_CLT.WriteData(new int[]{this.m_Dictionary.AdjustStateValue, this.m_Dictionary.AdjustStateAngle, this.m_Dictionary.AdjustStateResultValue, this.m_Dictionary.AdjustStateResultAngle});
        buffers.addAll(setStart(0));
        return buffers;
    }

    @Override // com.clou.rs350.device.MeterBaseDevice, com.clou.rs350.device.clinterface.IAdjust
    public List<byte[]> getAdjustFlag() {
        return this.m_CLT.AskData(new int[]{this.m_Dictionary.AdjustFlag});
    }

    @Override // com.clou.rs350.device.MeterBaseDevice, com.clou.rs350.device.clinterface.IAdjust
    public List<byte[]> setAdjustFlag(int flag) {
        ((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.AdjustFlag))).Data[0] = Integer.valueOf(flag);
        this.m_CLT.DataMap = this.m_Data;
        return this.m_CLT.WriteData(new int[]{this.m_Dictionary.AdjustFlag});
    }

    @Override // com.clou.rs350.device.MeterBaseDevice, com.clou.rs350.device.clinterface.IAdjust
    public List<byte[]> setDCAdjustFlag(int flag) {
        return null;
    }

    @Override // com.clou.rs350.device.MeterBaseDevice, com.clou.rs350.device.clinterface.IAdjust
    public List<byte[]> setResetAdjustmentFlag(int flag) {
        return null;
    }

    @Override // com.clou.rs350.device.clinterface.IParameterSetting, com.clou.rs350.device.MeterBaseDevice
    public List<byte[]> getWiringData() {
        return null;
    }

    @Override // com.clou.rs350.device.clinterface.IParameterSetting, com.clou.rs350.device.MeterBaseDevice
    public List<byte[]> setWiringData(int Wiring, int Pulse) {
        return setWiringData(Wiring, Pulse, 0);
    }

    @Override // com.clou.rs350.device.clinterface.IParameterSetting, com.clou.rs350.device.MeterBaseDevice
    public List<byte[]> getRange() {
        return null;
    }

    @Override // com.clou.rs350.device.clinterface.IParameterSetting, com.clou.rs350.device.MeterBaseDevice
    public List<byte[]> setRange(int AutoConnectType, int connectType, int AutoRange, double UaRange, double UbRange, double UcRange, double IaRange, double IbRange, double IcRange) {
        ((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.ConnectType))).Data[0] = Integer.valueOf(connectType);
        ((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.RangeAuto))).Data[0] = Integer.valueOf(AutoRange);
        ((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.RangeUa))).Data[0] = Double.valueOf(UaRange);
        ((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.RangeUb))).Data[0] = Double.valueOf(UbRange);
        ((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.RangeUc))).Data[0] = Double.valueOf(UcRange);
        ((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.RangeIa))).Data[0] = Double.valueOf(IaRange);
        ((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.RangeIb))).Data[0] = Double.valueOf(IbRange);
        ((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.RangeIc))).Data[0] = Double.valueOf(IcRange);
        this.m_CLT.DataMap = this.m_Data;
        return this.m_CLT.WriteData(new int[]{this.m_Dictionary.ConnectType, this.m_Dictionary.RangeAuto, this.m_Dictionary.RangeUa, this.m_Dictionary.RangeUb, this.m_Dictionary.RangeUc, this.m_Dictionary.RangeIa, this.m_Dictionary.RangeIb, this.m_Dictionary.RangeIc});
    }

    @Override // com.clou.rs350.device.clinterface.IParameterSetting, com.clou.rs350.device.MeterBaseDevice
    public List<byte[]> getConnectType() {
        return null;
    }

    @Override // com.clou.rs350.device.clinterface.IParameterSetting, com.clou.rs350.device.MeterBaseDevice
    public List<byte[]> setConnectType(int ConnectType) {
        return null;
    }

    @Override // com.clou.rs350.device.clinterface.IParameterSetting, com.clou.rs350.device.MeterBaseDevice
    public List<byte[]> getParameter() {
        return this.m_CLT.AskData(new int[]{this.m_Dictionary.PQPulseErrorFlag, this.m_Dictionary.Wiring, this.m_Dictionary.ConnectType, this.m_Dictionary.RangeAuto, this.m_Dictionary.ReadRange[0], this.m_Dictionary.ReadRange[1], this.m_Dictionary.ReadRange[2], this.m_Dictionary.ReadRange[3], this.m_Dictionary.ReadRange[4], this.m_Dictionary.ReadRange[5]});
    }

    @Override // com.clou.rs350.device.clinterface.IParameterSetting, com.clou.rs350.device.MeterBaseDevice
    public List<byte[]> setParameter(int Wiring, int Pluse, int Auto, int UaRange, int UbRange, int UcRange, int IaRange, int IbRange, int IcRange) {
        ((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.Wiring))).Data[0] = Integer.valueOf(Wiring);
        ((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.PQPulseErrorFlag))).Data[0] = Integer.valueOf(Pluse);
        ((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.RangeAuto))).Data[0] = Integer.valueOf(Auto);
        ((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.RangeUa))).Data[0] = Integer.valueOf(UaRange);
        ((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.RangeUb))).Data[0] = Integer.valueOf(UbRange);
        ((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.RangeUc))).Data[0] = Integer.valueOf(UcRange);
        ((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.RangeIa))).Data[0] = Integer.valueOf(IaRange);
        ((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.RangeIb))).Data[0] = Integer.valueOf(IbRange);
        ((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.RangeIc))).Data[0] = Integer.valueOf(IcRange);
        this.m_CLT.DataMap = this.m_Data;
        return this.m_CLT.WriteData(new int[]{this.m_Dictionary.Wiring, this.m_Dictionary.ConnectType, this.m_Dictionary.RangeAuto, this.m_Dictionary.RangeUa, this.m_Dictionary.RangeUb, this.m_Dictionary.RangeUc, this.m_Dictionary.RangeIa, this.m_Dictionary.RangeIb, this.m_Dictionary.RangeIc});
    }

    @Override // com.clou.rs350.device.clinterface.IParameterSetting, com.clou.rs350.device.MeterBaseDevice
    public String getVersionValue() {
        DataModel serioModel = (DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.Ver));
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < serioModel.Data.length; i++) {
            sb.append((char) ((Byte) serioModel.Data[i]).byteValue());
        }
        return sb.toString().trim();
    }

    @Override // com.clou.rs350.device.clinterface.IParameterSetting, com.clou.rs350.device.MeterBaseDevice
    public List<byte[]> getVersion() {
        return this.m_CLT.AskArray(new int[]{this.m_Dictionary.Ver});
    }

    @Override // com.clou.rs350.device.clinterface.IParameterSetting, com.clou.rs350.device.MeterBaseDevice
    public String getSerialNumberValue() {
        DataModel serioModel = (DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.SerialNo));
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < serioModel.Data.length; i++) {
            sb.append((char) ((Byte) serioModel.Data[i]).byteValue());
        }
        return sb.toString().trim();
    }

    @Override // com.clou.rs350.device.clinterface.IParameterSetting, com.clou.rs350.device.MeterBaseDevice
    public List<byte[]> getSerialNumber() {
        return this.m_CLT.AskArray(new int[]{this.m_Dictionary.SerialNo});
    }

    @Override // com.clou.rs350.device.clinterface.IParameterSetting, com.clou.rs350.device.MeterBaseDevice
    public List<byte[]> getHardwareVersion() {
        return this.m_CLT.AskArray(new int[]{this.m_Dictionary.HardwareVersion});
    }

    @Override // com.clou.rs350.device.clinterface.IParameterSetting, com.clou.rs350.device.MeterBaseDevice
    public List<byte[]> setConstant(long constant) {
        ((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.DeviceConstant))).Data[0] = Long.valueOf(constant);
        this.m_CLT.DataMap = this.m_Data;
        return this.m_CLT.WriteData(new int[]{this.m_Dictionary.DeviceConstant});
    }

    @Override // com.clou.rs350.device.clinterface.IParameterSetting, com.clou.rs350.device.MeterBaseDevice
    public List<byte[]> getConstant() {
        return this.m_CLT.AskData(new int[]{this.m_Dictionary.DeviceConstant});
    }

    @Override // com.clou.rs350.device.clinterface.IParameterSetting, com.clou.rs350.device.MeterBaseDevice
    public long getConstantValue() {
        return OtherUtils.convertToLong(((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.DeviceConstant))).Data[0]);
    }

    @Override // com.clou.rs350.device.BaseDevice, com.clou.rs350.device.MeterBaseDevice
    public int UnPacket(byte[] ReceiveBuffer) {
        int returnValue = this.m_CLT.UnPacket(ReceiveBuffer);
        if (132 == returnValue) {
            this.m_CLT.DataMap = this.m_Data;
            returnValue = this.m_CLT.UnPacket(ReceiveBuffer);
        }
        this.m_Data = this.m_CLT.DataMap;
        return returnValue;
    }

    @Override // com.clou.rs350.device.BaseDevice, com.clou.rs350.device.MeterBaseDevice
    public byte[] AnsOK() {
        return this.m_CLT.AnsOK();
    }

    @Override // com.clou.rs350.device.BaseDevice, com.clou.rs350.device.MeterBaseDevice
    public byte[] AnsErr() {
        return this.m_CLT.AnsErr();
    }

    @Override // com.clou.rs350.device.clinterface.IPowerQuality, com.clou.rs350.device.MeterBaseDevice
    public List<byte[]> getPowerQualityUnbalance() {
        List<byte[]> dataList = new ArrayList<>();
        List<byte[]> powerQualityUnbalance = this.m_CLT.AskData(new int[]{this.m_Dictionary.PowerQualityUnbalanceUzreo, this.m_Dictionary.PowerQualityUnbalanceUneg, this.m_Dictionary.PowerQualityUnbalanceIzreo, this.m_Dictionary.PowerQualityUnbalanceIneg});
        List<byte[]> getData = getMeasureBasicData();
        dataList.addAll(powerQualityUnbalance);
        dataList.addAll(getData);
        return dataList;
    }

    @Override // com.clou.rs350.device.clinterface.IPowerQuality, com.clou.rs350.device.MeterBaseDevice
    public List<byte[]> getPowerQualityStability() {
        List<byte[]> dataList = new ArrayList<>();
        List<byte[]> powerQualityStability = this.m_CLT.AskData(new int[]{this.m_Dictionary.PowerQualityPhaseStabU1, this.m_Dictionary.PowerQualityPhaseStabU2, this.m_Dictionary.PowerQualityPhaseStabU3, this.m_Dictionary.PowerQualityPhaseStabI1, this.m_Dictionary.PowerQualityPhaseStabI2, this.m_Dictionary.PowerQualityPhaseStabI3, this.m_Dictionary.PowerQualityPhaseStabP1, this.m_Dictionary.PowerQualityPhaseStabP2, this.m_Dictionary.PowerQualityPhaseStabP3, this.m_Dictionary.PowerQualityPhaseStabPSum, this.m_Dictionary.PowerQualityPhaseStabCountDown});
        List<byte[]> getData = getMeasureBasicData();
        dataList.addAll(powerQualityStability);
        dataList.addAll(getData);
        return dataList;
    }

    @Override // com.clou.rs350.device.clinterface.IPowerQuality, com.clou.rs350.device.MeterBaseDevice
    public List<byte[]> setPowerQualityStabilityParams(int StbMode, int PhaseStabTestTime, int PhaseStabSampleTime) {
        ((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.PowerQualityStbMode))).Data[0] = Integer.valueOf(StbMode);
        ((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.PowerQualityPhaseStabTestTime))).Data[0] = Integer.valueOf(PhaseStabTestTime);
        ((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.PowerQualityPhaseStabSampleTime))).Data[0] = Integer.valueOf(PhaseStabSampleTime);
        this.m_CLT.DataMap = this.m_Data;
        return this.m_CLT.WriteData(new int[]{this.m_Dictionary.PowerQualityStbMode, this.m_Dictionary.PowerQualityPhaseStabTestTime, this.m_Dictionary.PowerQualityPhaseStabSampleTime});
    }

    @Override // com.clou.rs350.device.clinterface.IParameterSetting, com.clou.rs350.device.MeterBaseDevice
    public List<byte[]> setStart(int start) {
        ((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.TaskStart))).Data[0] = Integer.valueOf(start);
        this.m_CLT.DataMap = this.m_Data;
        return this.m_CLT.WriteData(new int[]{this.m_Dictionary.TaskStart});
    }

    @Override // com.clou.rs350.device.clinterface.IParameterSetting, com.clou.rs350.device.MeterBaseDevice
    public List<byte[]> getStartState() {
        return this.m_CLT.AskData(new int[]{this.m_Dictionary.TaskStart});
    }

    @Override // com.clou.rs350.device.clinterface.IParameterSetting, com.clou.rs350.device.MeterBaseDevice
    public int getStartStateValue() {
        return ((Number) ((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.TaskStart))).Data[0]).intValue();
    }

    @Override // com.clou.rs350.device.clinterface.IParameterSetting, com.clou.rs350.device.MeterBaseDevice
    public List<byte[]> getStartStateAndFunc_Mstate() {
        return this.m_CLT.AskData(new int[]{this.m_Dictionary.TaskStart, this.m_Dictionary.FunctionState});
    }

    @Override // com.clou.rs350.device.clinterface.IParameterSetting, com.clou.rs350.device.MeterBaseDevice
    public List<byte[]> setFunc_Mstate(int state) {
        ((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.FunctionState))).Data[0] = Integer.valueOf(state);
        this.m_CLT.DataMap = this.m_Data;
        return this.m_CLT.WriteData(new int[]{this.m_Dictionary.FunctionState});
    }

    @Override // com.clou.rs350.device.clinterface.IParameterSetting, com.clou.rs350.device.MeterBaseDevice
    public List<byte[]> getFunc_Mstate() {
        return this.m_CLT.AskData(new int[]{this.m_Dictionary.FunctionState});
    }

    @Override // com.clou.rs350.device.clinterface.IParameterSetting, com.clou.rs350.device.MeterBaseDevice
    public List<byte[]> setFunc_Mstate_Start(int state, int start) {
        ((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.FunctionState))).Data[0] = Integer.valueOf(state);
        ((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.TaskStart))).Data[0] = Integer.valueOf(start);
        this.m_CLT.DataMap = this.m_Data;
        return this.m_CLT.WriteData(new int[]{this.m_Dictionary.FunctionState, this.m_Dictionary.TaskStart});
    }

    @Override // com.clou.rs350.device.MeterBaseDevice, com.clou.rs350.device.clinterface.ITest
    public List<byte[]> getErrors() {
        return this.m_CLT.AskData(new int[]{this.m_Dictionary.ErrorErrorCount[0], this.m_Dictionary.ErrorErrorCount[1], this.m_Dictionary.ErrorErrorCount[2], this.m_Dictionary.ErrorMeterError1[0], this.m_Dictionary.ErrorMeterError1[1], this.m_Dictionary.ErrorMeterError1[2], this.m_Dictionary.ErrorMeterError2[0], this.m_Dictionary.ErrorMeterError2[1], this.m_Dictionary.ErrorMeterError2[2], this.m_Dictionary.ErrorMeterError3[0], this.m_Dictionary.ErrorMeterError3[1], this.m_Dictionary.ErrorMeterError3[2], this.m_Dictionary.ErrorMeterError4[0], this.m_Dictionary.ErrorMeterError4[1], this.m_Dictionary.ErrorMeterError4[2], this.m_Dictionary.ErrorMeterError5[0], this.m_Dictionary.ErrorMeterError5[1], this.m_Dictionary.ErrorMeterError5[2], this.m_Dictionary.ErrorMeterErrorAvg[0], this.m_Dictionary.ErrorMeterErrorAvg[1], this.m_Dictionary.ErrorMeterErrorAvg[2], this.m_Dictionary.ErrorMeterErrorStd[0], this.m_Dictionary.ErrorMeterErrorStd[1], this.m_Dictionary.ErrorMeterErrorStd[2], this.m_Dictionary.ErrorMeterPulseNum[0], this.m_Dictionary.ErrorMeterPulseNum[1], this.m_Dictionary.ErrorMeterPulseNum[2]});
    }

    @Override // com.clou.rs350.device.MeterBaseDevice, com.clou.rs350.device.clinterface.ITest
    public List<byte[]> getEnergy() {
        return this.m_CLT.AskData(new int[]{this.m_Dictionary.EnergyValue});
    }

    @Override // com.clou.rs350.device.MeterBaseDevice, com.clou.rs350.device.clinterface.ITest
    public ExplainedModel[] getEnergyValues() {
        return new ExplainedModel[]{DataUtils.parseRound(((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.EnergyValue))).Data[0], 4), new ExplainedModel(), new ExplainedModel(), new ExplainedModel()};
    }

    @Override // com.clou.rs350.device.MeterBaseDevice, com.clou.rs350.device.clinterface.ITest
    public List<byte[]> setErrorParameters(float[] constants, int[] pulses) {
        ((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.ErrorMeterConstants[0]))).Data[0] = Integer.valueOf((int) constants[0]);
        ((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.ErrorMeterConstants[1]))).Data[0] = Integer.valueOf((int) constants[1]);
        ((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.ErrorMeterConstants[2]))).Data[0] = Integer.valueOf((int) constants[2]);
        ((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.ErrorPulseCount[0]))).Data[0] = Integer.valueOf(pulses[0]);
        ((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.ErrorPulseCount[1]))).Data[0] = Integer.valueOf(pulses[1]);
        ((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.ErrorPulseCount[2]))).Data[0] = Integer.valueOf(pulses[2]);
        ((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.ErrorErrorCount[0]))).Data[0] = 0;
        ((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.ErrorErrorCount[1]))).Data[0] = 0;
        ((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.ErrorErrorCount[2]))).Data[0] = 0;
        ((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.ErrorMeterPulseNum[0]))).Data[0] = Integer.valueOf(pulses[0]);
        ((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.ErrorMeterPulseNum[1]))).Data[0] = Integer.valueOf(pulses[1]);
        ((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.ErrorMeterPulseNum[2]))).Data[0] = Integer.valueOf(pulses[2]);
        this.m_CLT.DataMap = this.m_Data;
        return this.m_CLT.WriteData(new int[]{this.m_Dictionary.ErrorMeterConstants[0], this.m_Dictionary.ErrorMeterConstants[1], this.m_Dictionary.ErrorMeterConstants[2], this.m_Dictionary.ErrorPulseCount[0], this.m_Dictionary.ErrorPulseCount[1], this.m_Dictionary.ErrorPulseCount[2]});
    }

    @Override // com.clou.rs350.device.MeterBaseDevice, com.clou.rs350.device.clinterface.ITest
    public List<byte[]> setErrorParameters(int[] measurementType) {
        ((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.ErrorChannel))).Data[0] = Integer.valueOf(measurementType[0] + (measurementType[1] << 2) + (measurementType[2] << 4));
        this.m_CLT.DataMap = this.m_Data;
        return this.m_CLT.WriteData(new int[]{this.m_Dictionary.ErrorChannel});
    }

    @Override // com.clou.rs350.device.MeterBaseDevice, com.clou.rs350.device.clinterface.ITest
    public List<byte[]> setErrorParameters(int[] measurementType, float[] constants, int[] pulses) {
        return setErrorParameters(1.0d, new int[3], measurementType, constants, pulses);
    }

    @Override // com.clou.rs350.device.MeterBaseDevice, com.clou.rs350.device.clinterface.ITest
    public List<byte[]> setErrorParameters(int[] errorCount, int[] measurementType, float[] constants, int[] pulses) {
        return setErrorParameters(1.0d, errorCount, measurementType, constants, pulses);
    }

    @Override // com.clou.rs350.device.MeterBaseDevice, com.clou.rs350.device.clinterface.ITest
    public List<byte[]> setErrorParameters(double ct, int[] errorCount, int[] measurementType, float[] constants, int[] pulses) {
        ((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.ErrorChannel))).Data[0] = Integer.valueOf(measurementType[0] + (measurementType[1] << 2) + (measurementType[2] << 4));
        ((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.ErrorCT))).Data[0] = Double.valueOf(ct);
        ((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.ErrorMeterConstants[0]))).Data[0] = Integer.valueOf((int) constants[0]);
        ((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.ErrorMeterConstants[1]))).Data[0] = Integer.valueOf((int) constants[1]);
        ((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.ErrorMeterConstants[2]))).Data[0] = Integer.valueOf((int) constants[2]);
        ((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.ErrorPulseCount[0]))).Data[0] = Integer.valueOf(pulses[0]);
        ((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.ErrorPulseCount[1]))).Data[0] = Integer.valueOf(pulses[1]);
        ((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.ErrorPulseCount[2]))).Data[0] = Integer.valueOf(pulses[2]);
        ((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.ErrorErrorCount[0]))).Data[0] = 0;
        ((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.ErrorErrorCount[1]))).Data[0] = 0;
        ((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.ErrorErrorCount[2]))).Data[0] = 0;
        ((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.ErrorMeterPulseNum[0]))).Data[0] = Integer.valueOf(pulses[0]);
        ((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.ErrorMeterPulseNum[1]))).Data[0] = Integer.valueOf(pulses[1]);
        ((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.ErrorMeterPulseNum[2]))).Data[0] = Integer.valueOf(pulses[2]);
        this.m_CLT.DataMap = this.m_Data;
        return this.m_CLT.WriteData(new int[]{this.m_Dictionary.ErrorChannel, this.m_Dictionary.ErrorCT, this.m_Dictionary.ErrorMeterConstants[0], this.m_Dictionary.ErrorMeterConstants[1], this.m_Dictionary.ErrorMeterConstants[2], this.m_Dictionary.ErrorPulseCount[0], this.m_Dictionary.ErrorPulseCount[1], this.m_Dictionary.ErrorPulseCount[2]});
    }

    @Override // com.clou.rs350.device.MeterBaseDevice, com.clou.rs350.device.clinterface.ITest
    public List<byte[]> setDailyParameters(int[] measurementType, int[] constants, int[] pulses) {
        ((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.ErrorChannel))).Data[0] = Integer.valueOf(measurementType[0] + (measurementType[1] << 2) + (measurementType[2] << 4));
        ((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.ErrorMeterDailyFrequency[0]))).Data[0] = Integer.valueOf(constants[0]);
        ((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.ErrorMeterDailyFrequency[1]))).Data[0] = Integer.valueOf(constants[1]);
        ((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.ErrorMeterDailyFrequency[2]))).Data[0] = Integer.valueOf(constants[2]);
        ((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.ErrorPulseCount[0]))).Data[0] = Integer.valueOf(pulses[0]);
        ((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.ErrorPulseCount[1]))).Data[0] = Integer.valueOf(pulses[1]);
        ((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.ErrorPulseCount[2]))).Data[0] = Integer.valueOf(pulses[2]);
        ((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.ErrorErrorCount[0]))).Data[0] = 0;
        ((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.ErrorErrorCount[1]))).Data[0] = 0;
        ((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.ErrorErrorCount[2]))).Data[0] = 0;
        ((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.ErrorMeterPulseNum[0]))).Data[0] = Integer.valueOf(pulses[0]);
        ((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.ErrorMeterPulseNum[1]))).Data[0] = Integer.valueOf(pulses[1]);
        ((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.ErrorMeterPulseNum[2]))).Data[0] = Integer.valueOf(pulses[2]);
        this.m_CLT.DataMap = this.m_Data;
        return this.m_CLT.WriteData(new int[]{this.m_Dictionary.ErrorChannel, this.m_Dictionary.ErrorMeterDailyFrequency[0], this.m_Dictionary.ErrorMeterDailyFrequency[1], this.m_Dictionary.ErrorMeterDailyFrequency[2], this.m_Dictionary.ErrorPulseCount[0], this.m_Dictionary.ErrorPulseCount[1], this.m_Dictionary.ErrorPulseCount[2]});
    }

    @Override // com.clou.rs350.device.clinterface.IWiringCheck, com.clou.rs350.device.MeterBaseDevice
    public List<byte[]> getWiringCheck() {
        return this.m_CLT.AskData(new int[]{this.m_Dictionary.WiringCheckU1, this.m_Dictionary.WiringCheckU2, this.m_Dictionary.WiringCheckU3, this.m_Dictionary.WiringCheckI1, this.m_Dictionary.WiringCheckI2, this.m_Dictionary.WiringCheckI3, this.m_Dictionary.WiringCheckRemark});
    }

    @Override // com.clou.rs350.device.clinterface.IWiringCheck, com.clou.rs350.device.MeterBaseDevice
    public List<byte[]> setPowerFactor(int powerFactor) {
        ((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.WiringCheckCos))).Data[0] = Integer.valueOf(powerFactor + 1);
        this.m_CLT.DataMap = this.m_Data;
        return this.m_CLT.WriteData(new int[]{this.m_Dictionary.WiringCheckCos});
    }

    @Override // com.clou.rs350.device.clinterface.IWiringCheck, com.clou.rs350.device.MeterBaseDevice
    public List<byte[]> setSimulationValue(double ua, double ub, double uc, double uaAngle, double ubAngle, double ucAngle, double ia, double ib, double ic, double iaAngle, double ibAngle, double icAngle) {
        if (getWiringValue() == 1) {
            ((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.WiringCheckSimulationLineValueU1))).Data[0] = Double.valueOf(ua);
            ((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.WiringCheckSimulationLineValueU2))).Data[0] = Double.valueOf(ub);
            ((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.WiringCheckSimulationLineValueU2))).Data[0] = Double.valueOf(uc);
            ((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.WiringCheckSimulationLineAngleU1))).Data[0] = Double.valueOf(uaAngle);
            ((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.WiringCheckSimulationLineAngleU2))).Data[0] = Double.valueOf(ubAngle);
            ((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.WiringCheckSimulationLineAngleU2))).Data[0] = Double.valueOf(ucAngle);
            ((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.WiringCheckSimulationPhasesValueI1))).Data[0] = Double.valueOf(ia);
            ((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.WiringCheckSimulationPhasesValueI2))).Data[0] = Double.valueOf(ib);
            ((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.WiringCheckSimulationPhasesValueI3))).Data[0] = Double.valueOf(ic);
            ((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.WiringCheckSimulationPhasesAngleI1))).Data[0] = Double.valueOf(iaAngle);
            ((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.WiringCheckSimulationPhasesAngleI2))).Data[0] = Double.valueOf(ibAngle);
            ((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.WiringCheckSimulationPhasesAngleI3))).Data[0] = Double.valueOf(icAngle);
            this.m_CLT.DataMap = this.m_Data;
            return this.m_CLT.WriteData(new int[]{this.m_Dictionary.WiringCheckSimulationLineValueU1, this.m_Dictionary.WiringCheckSimulationLineValueU2, this.m_Dictionary.WiringCheckSimulationLineValueU2, this.m_Dictionary.WiringCheckSimulationLineAngleU1, this.m_Dictionary.WiringCheckSimulationLineAngleU2, this.m_Dictionary.WiringCheckSimulationLineAngleU2, this.m_Dictionary.WiringCheckSimulationPhasesValueI1, this.m_Dictionary.WiringCheckSimulationPhasesValueI2, this.m_Dictionary.WiringCheckSimulationPhasesValueI3, this.m_Dictionary.WiringCheckSimulationPhasesAngleI1, this.m_Dictionary.WiringCheckSimulationPhasesAngleI2, this.m_Dictionary.WiringCheckSimulationPhasesAngleI3});
        }
        ((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.WiringCheckSimulationPhasesValueU1))).Data[0] = Double.valueOf(ua);
        ((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.WiringCheckSimulationPhasesValueU2))).Data[0] = Double.valueOf(ub);
        ((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.WiringCheckSimulationPhasesValueU3))).Data[0] = Double.valueOf(uc);
        ((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.WiringCheckSimulationPhasesAngleU1))).Data[0] = Double.valueOf(ubAngle);
        ((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.WiringCheckSimulationPhasesAngleU2))).Data[0] = Double.valueOf(ubAngle);
        ((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.WiringCheckSimulationPhasesAngleU3))).Data[0] = Double.valueOf(ucAngle);
        ((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.WiringCheckSimulationPhasesValueI1))).Data[0] = Double.valueOf(ia);
        ((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.WiringCheckSimulationPhasesValueI2))).Data[0] = Double.valueOf(ib);
        ((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.WiringCheckSimulationPhasesValueI3))).Data[0] = Double.valueOf(ic);
        ((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.WiringCheckSimulationPhasesAngleI1))).Data[0] = Double.valueOf(iaAngle);
        ((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.WiringCheckSimulationPhasesAngleI2))).Data[0] = Double.valueOf(ibAngle);
        ((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.WiringCheckSimulationPhasesAngleI3))).Data[0] = Double.valueOf(icAngle);
        this.m_CLT.DataMap = this.m_Data;
        return this.m_CLT.WriteData(new int[]{this.m_Dictionary.WiringCheckSimulationPhasesValueU1, this.m_Dictionary.WiringCheckSimulationPhasesValueU2, this.m_Dictionary.WiringCheckSimulationPhasesValueU3, this.m_Dictionary.WiringCheckSimulationPhasesAngleU2, this.m_Dictionary.WiringCheckSimulationPhasesAngleU3, this.m_Dictionary.WiringCheckSimulationPhasesValueI1, this.m_Dictionary.WiringCheckSimulationPhasesValueI2, this.m_Dictionary.WiringCheckSimulationPhasesValueI3, this.m_Dictionary.WiringCheckSimulationPhasesAngleI1, this.m_Dictionary.WiringCheckSimulationPhasesAngleI2, this.m_Dictionary.WiringCheckSimulationPhasesAngleI3});
    }

    @Override // com.clou.rs350.device.clinterface.IWiringCheck, com.clou.rs350.device.MeterBaseDevice
    public List<byte[]> setTestFlag(int flag) {
        ((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.WiringCheckTestFlag))).Data[0] = Integer.valueOf(flag);
        this.m_CLT.DataMap = this.m_Data;
        return this.m_CLT.WriteData(new int[]{this.m_Dictionary.WiringCheckTestFlag});
    }

    @Override // com.clou.rs350.device.clinterface.IReadMeter, com.clou.rs350.device.MeterBaseDevice
    public List<byte[]> setBaudrate(int comPort, int baudrate, int wordlength, int parity, int stopbits) {
        return this.m_CLT.SetBaudrate(comPort, baudrate, wordlength, parity, stopbits);
    }

    @Override // com.clou.rs350.device.clinterface.IReadMeter, com.clou.rs350.device.MeterBaseDevice
    public List<byte[]> sendBuffer(int comPort, List<byte[]> buffer) {
        List<byte[]> sendBuffer = new ArrayList<>();
        for (int i = 0; i < buffer.size(); i++) {
            sendBuffer.add(this.m_CLT.GetSendBuffer(comPort, buffer.get(i)));
        }
        return sendBuffer;
    }

    @Override // com.clou.rs350.device.clinterface.IReadMeter, com.clou.rs350.device.MeterBaseDevice
    public byte[] sendBuffer(int comPort, byte[] buffer) {
        return this.m_CLT.GetSendBuffer(comPort, buffer);
    }

    @Override // com.clou.rs350.device.clinterface.IReadMeter, com.clou.rs350.device.MeterBaseDevice
    public ConnectByteModel getReceiveModel() {
        return this.m_CLT.dataBuffer;
    }

    @Override // com.clou.rs350.device.clinterface.IReadMeter, com.clou.rs350.device.MeterBaseDevice
    public void clearReceiveBuffer() {
        this.m_CLT.dataBuffer = new ConnectByteModel();
    }

    @Override // com.clou.rs350.device.clinterface.IPTCTMeasurement, com.clou.rs350.device.MeterBaseDevice
    public List<byte[]> setPTAddress(int location, int channel) {
        ((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.PTsetAddress))).Data[0] = Integer.valueOf(channel + (location << 7));
        this.m_CLT.DataMap = this.m_Data;
        return this.m_CLT.WriteData(new int[]{this.m_Dictionary.PTsetAddress});
    }

    @Override // com.clou.rs350.device.clinterface.IPTCTMeasurement, com.clou.rs350.device.MeterBaseDevice
    public List<byte[]> setCTChannel(int Channel) {
        ((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.CTChannel))).Data[0] = Integer.valueOf(Channel);
        this.m_CLT.DataMap = this.m_Data;
        return this.m_CLT.WriteData(new int[]{this.m_Dictionary.CTChannel});
    }

    @Override // com.clou.rs350.device.clinterface.IPTCTMeasurement, com.clou.rs350.device.MeterBaseDevice
    public List<byte[]> getCTValue() {
        return this.m_CLT.AskData(new int[]{this.m_Dictionary.CTCurrentValuePrimary, this.m_Dictionary.CTCurrentValueSecondary, this.m_Dictionary.CTCurrentAnglePrimary, this.m_Dictionary.CTCurrentAngleSecondary, this.m_Dictionary.CTCurrentValueRatio, this.m_Dictionary.CTCurrentValuePhase, this.m_Dictionary.CTCurrentValueError, this.m_Dictionary.CTResistance, this.m_Dictionary.CTReactance, this.m_Dictionary.CTImpedance, this.m_Dictionary.CTPowerFactor, this.m_Dictionary.CTBurden});
    }

    @Override // com.clou.rs350.device.clinterface.IPTCTMeasurement, com.clou.rs350.device.MeterBaseDevice
    public ExplainedModel[] getPTErrorValue() {
        return new ExplainedModel[]{DataUtils.parse(((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.PTLocation1Voltage[0]))).Data[0], "V"), DataUtils.parse(((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.PTLocation1Voltage[1]))).Data[0], "V"), DataUtils.parse(((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.PTLocation1Voltage[2]))).Data[0], "V"), DataUtils.parse(((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.PTLocation1Angle[0]))).Data[0], "°"), DataUtils.parse(((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.PTLocation1Angle[1]))).Data[0], "°"), DataUtils.parse(((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.PTLocation1Angle[2]))).Data[0], "°"), DataUtils.parse(((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.PTLocation2Voltage[0]))).Data[0], "V"), DataUtils.parse(((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.PTLocation2Voltage[1]))).Data[0], "V"), DataUtils.parse(((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.PTLocation2Voltage[2]))).Data[0], "V"), DataUtils.parse(((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.PTLocation2Angle[0]))).Data[0], "°"), DataUtils.parse(((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.PTLocation2Angle[1]))).Data[0], "°"), DataUtils.parse(((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.PTLocation2Angle[2]))).Data[0], "°"), DataUtils.parse(((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.PTVoltageDropError[0]))).Data[0], "°"), DataUtils.parse(((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.PTVoltageDropError[1]))).Data[0], "°"), DataUtils.parse(((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.PTVoltageDropError[2]))).Data[0], "°"), DataUtils.parseRound(((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.PTAngleError[0]))).Data[0], 3), DataUtils.parseRound(((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.PTAngleError[1]))).Data[0], 3), DataUtils.parseRound(((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.PTAngleError[2]))).Data[0], 3), DataUtils.parse(((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.PTRatioError[0]))).Data[0], "°"), DataUtils.parse(((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.PTRatioError[1]))).Data[0], "°"), DataUtils.parse(((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.PTRatioError[2]))).Data[0], "°"), new ExplainedModel()};
    }

    @Override // com.clou.rs350.device.clinterface.IPTCTMeasurement, com.clou.rs350.device.MeterBaseDevice
    public List<byte[]> getPTError() {
        return this.m_CLT.AskData(new int[]{this.m_Dictionary.PTLocation1Voltage[0], this.m_Dictionary.PTLocation1Voltage[1], this.m_Dictionary.PTLocation1Voltage[2], this.m_Dictionary.PTLocation1Angle[0], this.m_Dictionary.PTLocation1Angle[1], this.m_Dictionary.PTLocation1Angle[2], this.m_Dictionary.PTLocation2Voltage[0], this.m_Dictionary.PTLocation2Voltage[1], this.m_Dictionary.PTLocation2Voltage[2], this.m_Dictionary.PTLocation2Angle[0], this.m_Dictionary.PTLocation2Angle[1], this.m_Dictionary.PTLocation2Angle[2], this.m_Dictionary.PTRatioError[0], this.m_Dictionary.PTRatioError[1], this.m_Dictionary.PTRatioError[2], this.m_Dictionary.PTAngleError[0], this.m_Dictionary.PTAngleError[1], this.m_Dictionary.PTAngleError[2], this.m_Dictionary.PTVoltageDropError[0], this.m_Dictionary.PTVoltageDropError[1], this.m_Dictionary.PTVoltageDropError[2]});
    }

    @Override // com.clou.rs350.device.clinterface.IPTCTMeasurement, com.clou.rs350.device.MeterBaseDevice
    public List<byte[]> setPTConnect(int connect) {
        ((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.PTConnect))).Data[0] = Integer.valueOf(connect);
        this.m_CLT.DataMap = this.m_Data;
        return this.m_CLT.WriteData(new int[]{this.m_Dictionary.PTConnect});
    }

    @Override // com.clou.rs350.device.clinterface.IPTCTMeasurement, com.clou.rs350.device.MeterBaseDevice
    public int[] getPTConnectStateValue() {
        int tmp = ((Number) ((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.PTsetAddress))).Data[0]).intValue();
        return new int[]{tmp >> 7, tmp & 127, ((Number) ((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.PTIsConnect))).Data[0]).intValue()};
    }

    @Override // com.clou.rs350.device.clinterface.IPTCTMeasurement, com.clou.rs350.device.MeterBaseDevice
    public List<byte[]> getPTDeviceState() {
        return this.m_CLT.AskData(new int[]{this.m_Dictionary.PTIsConnect, this.m_Dictionary.DeviceBattery, this.m_Dictionary.PTsetAddress});
    }

    @Override // com.clou.rs350.device.clinterface.IParameterSetting, com.clou.rs350.device.MeterBaseDevice
    public List<byte[]> getBattery() {
        return this.m_CLT.AskData(new int[]{this.m_Dictionary.DeviceBattery});
    }

    @Override // com.clou.rs350.device.clinterface.IPTCTMeasurement, com.clou.rs350.device.MeterBaseDevice
    public List<byte[]> setAutomaticCheck(int check) {
        ((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.PTAutomaticCheck))).Data[0] = Integer.valueOf(check);
        this.m_CLT.DataMap = this.m_Data;
        return this.m_CLT.WriteData(new int[]{this.m_Dictionary.PTAutomaticCheck});
    }

    @Override // com.clou.rs350.device.clinterface.IPTCTMeasurement, com.clou.rs350.device.MeterBaseDevice
    public List<byte[]> getAutomaticCheckState() {
        return this.m_CLT.AskData(new int[]{this.m_Dictionary.PTAutomaticCheckState});
    }

    @Override // com.clou.rs350.device.clinterface.IPTCTMeasurement, com.clou.rs350.device.MeterBaseDevice
    public List<byte[]> cleanAutomaticCheckState() {
        ((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.PTAutomaticCheckState))).Data[0] = 0;
        return this.m_CLT.WriteData(new int[]{this.m_Dictionary.PTAutomaticCheckState});
    }

    @Override // com.clou.rs350.device.clinterface.IPTCTMeasurement, com.clou.rs350.device.MeterBaseDevice
    public List<byte[]> getGPS() {
        return this.m_CLT.AskData(new int[]{this.m_Dictionary.GPSYear, this.m_Dictionary.GPSMonth, this.m_Dictionary.GPSDay, this.m_Dictionary.GPSHour, this.m_Dictionary.GPSMinute, this.m_Dictionary.GPSSecond, this.m_Dictionary.GPSSvNum, this.m_Dictionary.GPSPossNum, this.m_Dictionary.GPSState, this.m_Dictionary.GPSLatitude, this.m_Dictionary.GPSLongitude});
    }

    @Override // com.clou.rs350.device.clinterface.IPTCTMeasurement, com.clou.rs350.device.MeterBaseDevice
    public List<byte[]> setPTSettingFlag(int PTSettingFlag) {
        ((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.PTSettingFlag))).Data[0] = Integer.valueOf(PTSettingFlag);
        this.m_CLT.DataMap = this.m_Data;
        return this.m_CLT.WriteData(new int[]{this.m_Dictionary.PTSettingFlag});
    }

    @Override // com.clou.rs350.device.clinterface.IMeter, com.clou.rs350.device.MeterBaseDevice
    public ExplainedModel[] getLNVoltageValue() {
        return new ExplainedModel[]{DataUtils.parse(((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.MeterUaValue))).Data[0], "V"), DataUtils.parse(((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.MeterUbValue))).Data[0], "V"), DataUtils.parse(((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.MeterUcValue))).Data[0], "V")};
    }

    @Override // com.clou.rs350.device.clinterface.IMeter, com.clou.rs350.device.MeterBaseDevice
    public ExplainedModel[] getLLVoltageValue() {
        return new ExplainedModel[]{DataUtils.parse(((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.MeterUabValue))).Data[0], "V"), DataUtils.parse(((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.MeterUbcValue))).Data[0], "V"), DataUtils.parse(((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.MeterUcaValue))).Data[0], "V")};
    }

    @Override // com.clou.rs350.device.clinterface.IMeter, com.clou.rs350.device.MeterBaseDevice
    public ExplainedModel[] getCurrentValue() {
        return new ExplainedModel[]{DataUtils.parse(((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.MeterIaValue))).Data[0], "A"), DataUtils.parse(((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.MeterIbValue))).Data[0], "A"), DataUtils.parse(((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.MeterIcValue))).Data[0], "A")};
    }

    @Override // com.clou.rs350.device.clinterface.IMeter, com.clou.rs350.device.MeterBaseDevice
    public ExplainedModel[] getPValue() {
        return new ExplainedModel[]{DataUtils.parse(((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.MeterPaValue))).Data[0], "W"), DataUtils.parse(((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.MeterPbValue))).Data[0], "W"), DataUtils.parse(((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.MeterPcValue))).Data[0], "W"), DataUtils.parse(((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.MeterPSumValue))).Data[0], "W")};
    }

    @Override // com.clou.rs350.device.clinterface.IMeter, com.clou.rs350.device.MeterBaseDevice
    public ExplainedModel[] getQValue() {
        return new ExplainedModel[]{DataUtils.parse(((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.MeterQaValue))).Data[0], "var"), DataUtils.parse(((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.MeterQbValue))).Data[0], "var"), DataUtils.parse(((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.MeterQcValue))).Data[0], "var"), DataUtils.parse(((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.MeterQSumValue))).Data[0], "var")};
    }

    @Override // com.clou.rs350.device.clinterface.IMeter, com.clou.rs350.device.MeterBaseDevice
    public ExplainedModel[] getSValue() {
        return new ExplainedModel[]{DataUtils.parse(((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.MeterSaValue))).Data[0], "VA"), DataUtils.parse(((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.MeterSbValue))).Data[0], "VA"), DataUtils.parse(((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.MeterScValue))).Data[0], "VA"), DataUtils.parse(((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.MeterSSumValue))).Data[0], "VA")};
    }

    @Override // com.clou.rs350.device.clinterface.IMeter, com.clou.rs350.device.MeterBaseDevice
    public ExplainedModel[] getCosValue() {
        return new ExplainedModel[]{DataUtils.parseRound(((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.MeterCosaValue))).Data[0], 4), DataUtils.parseRound(((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.MeterCosbValue))).Data[0], 4), DataUtils.parseRound(((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.MeterCoscValue))).Data[0], 4), DataUtils.parseRound(((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.MeterCosSumValue))).Data[0], 4)};
    }

    public double[] getAngles() {
        double[] angles = {DataUtils.convertToDouble(((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.MeterUaAngle))).Data[0]), DataUtils.convertToDouble(((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.MeterUbAngle))).Data[0]), DataUtils.convertToDouble(((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.MeterUcAngle))).Data[0]), DataUtils.convertToDouble(((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.MeterUabAngle))).Data[0]), DataUtils.convertToDouble(((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.MeterUbcAngle))).Data[0]), DataUtils.convertToDouble(((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.MeterUcaAngle))).Data[0]), DataUtils.convertToDouble(((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.MeterUcbAngle))).Data[0]), DataUtils.convertToDouble(((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.MeterIaAngle))).Data[0]), DataUtils.convertToDouble(((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.MeterIbAngle))).Data[0]), DataUtils.convertToDouble(((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.MeterIcAngle))).Data[0])};
        if (1 == this.m_VectorDefinition) {
            double referenceAngle = 0.0d;
            if (0.0d != DataUtils.convertToDouble(((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.MeterIaValue))).Data[0])) {
                referenceAngle = angles[7];
            } else if (0.0d != DataUtils.convertToDouble(((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.MeterIbValue))).Data[0])) {
                referenceAngle = angles[8] - 120.0d;
            } else if (0.0d != DataUtils.convertToDouble(((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.MeterIcValue))).Data[0])) {
                referenceAngle = angles[9] - 240.0d;
            }
            for (int i = 0; i < angles.length; i++) {
                angles[i] = angles[i] - referenceAngle;
                angles[i] = AngleUtil.adjustAngle(angles[i]);
            }
        }
        if (1 == this.m_AngleDefinition) {
            for (int i2 = 0; i2 < angles.length; i2++) {
                angles[i2] = (360.0d - angles[i2]) % 360.0d;
            }
        }
        return angles;
    }

    @Override // com.clou.rs350.device.clinterface.IMeter, com.clou.rs350.device.MeterBaseDevice
    public ExplainedModel[] getLNVoltageAngleValue() {
        double[] angles = getAngles();
        return new ExplainedModel[]{DataUtils.parse(Double.valueOf(angles[0]), "°"), DataUtils.parse(Double.valueOf(angles[1]), "°"), DataUtils.parse(Double.valueOf(angles[2]), "°")};
    }

    @Override // com.clou.rs350.device.clinterface.IMeter, com.clou.rs350.device.MeterBaseDevice
    public ExplainedModel[] getLLVoltageAngleValue() {
        double[] angles = getAngles();
        return new ExplainedModel[]{DataUtils.parse(Double.valueOf(angles[3]), "°"), DataUtils.parse(Double.valueOf(angles[4]), "°"), DataUtils.parse(Double.valueOf(angles[5]), "°"), DataUtils.parse(Double.valueOf(angles[6]), "°")};
    }

    @Override // com.clou.rs350.device.clinterface.IMeter, com.clou.rs350.device.MeterBaseDevice
    public ExplainedModel[] getCurrentAngleValue() {
        double[] angles = getAngles();
        return new ExplainedModel[]{DataUtils.parse(Double.valueOf(angles[7]), "°"), DataUtils.parse(Double.valueOf(angles[8]), "°"), DataUtils.parse(Double.valueOf(angles[9]), "°")};
    }

    @Override // com.clou.rs350.device.clinterface.IMeter, com.clou.rs350.device.MeterBaseDevice
    public ExplainedModel[] getLNVoltageCurrentAngleValue() {
        return new ExplainedModel[]{DataUtils.parse(((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.MeterUIaAngle))).Data[0], "°"), DataUtils.parse(((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.MeterUIbAngle))).Data[0], "°"), DataUtils.parse(((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.MeterUIcAngle))).Data[0], "°")};
    }

    @Override // com.clou.rs350.device.clinterface.IMeter, com.clou.rs350.device.MeterBaseDevice
    public ExplainedModel[] getLLVoltageCurrentAngleValue() {
        return new ExplainedModel[]{DataUtils.parse(((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.MeterUabIaAngle))).Data[0], "°"), DataUtils.parse(((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.MeterUbcIbAngle))).Data[0], "°"), DataUtils.parse(((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.MeterUcaIcAngle))).Data[0], "°"), DataUtils.parse(((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.MeterUcbIcAngle))).Data[0], "°")};
    }

    @Override // com.clou.rs350.device.clinterface.IMeter, com.clou.rs350.device.MeterBaseDevice
    public ExplainedModel[] getFrequencyValue() {
        return new ExplainedModel[]{DataUtils.parse(((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.MeterFrequency))).Data[0], "Hz")};
    }

    @Override // com.clou.rs350.device.clinterface.IMeter, com.clou.rs350.device.MeterBaseDevice
    public Double[] getHarmonicContantsValue(int Phase) {
        Object[] Data;
        Object[] objArr = new Object[0];
        if (Phase > 2) {
            Data = ((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.MeterCurrentHarmonicContent[Phase - 3]))).Data;
        } else {
            Data = ((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.MeterVoltageHarmonicContent[Phase]))).Data;
        }
        return DataUtils.parseDoubleArrayRemove0(Data, 5);
    }

    @Override // com.clou.rs350.device.clinterface.IMeter, com.clou.rs350.device.MeterBaseDevice
    public Double[] getHarmonicAnglesValue(int Phase) {
        Object[] Data;
        Object[] objArr = new Object[0];
        if (Phase > 2) {
            Data = ((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.MeterCurrentHarmonicAngle[Phase - 3]))).Data;
        } else {
            Data = ((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.MeterVoltageHarmonicAngle[Phase]))).Data;
        }
        return DataUtils.parseDoubleArrayRemove0(Data, 4, this.m_AngleDefinition);
    }

    @Override // com.clou.rs350.device.clinterface.IMeter, com.clou.rs350.device.MeterBaseDevice
    public ExplainedModel[] getTHDValue() {
        return new ExplainedModel[]{DataUtils.parseRound(((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.MeterTHD))).Data[0], 2, "%"), DataUtils.parseRound(((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.MeterTHD))).Data[1], 2, "%"), DataUtils.parseRound(((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.MeterTHD))).Data[2], 2, "%"), DataUtils.parseRound(((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.MeterTHD))).Data[3], 2, "%"), DataUtils.parseRound(((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.MeterTHD))).Data[4], 2, "%"), DataUtils.parseRound(((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.MeterTHD))).Data[5], 2, "%")};
    }

    @Override // com.clou.rs350.device.clinterface.IMeter, com.clou.rs350.device.MeterBaseDevice
    public Double[] getUIWaveValue(int Phase) {
        Object[] Data = new Object[0];
        switch (Phase) {
            case 0:
                Data = ((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.MeterUaWave))).Data;
                break;
            case 1:
                Data = ((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.MeterUbWave))).Data;
                break;
            case 2:
                Data = ((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.MeterUcWave))).Data;
                break;
            case 3:
                Data = ((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.MeterIaWave))).Data;
                break;
            case 4:
                Data = ((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.MeterIbWave))).Data;
                break;
            case 5:
                Data = ((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.MeterIcWave))).Data;
                break;
        }
        return DataUtils.parseDoubleArray(Data);
    }

    @Override // com.clou.rs350.device.clinterface.IWiringCheck, com.clou.rs350.device.MeterBaseDevice
    public int getWiringRemrakValue() {
        return OtherUtils.convertToInt(((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.WiringCheckRemark))).Data[0]);
    }

    @Override // com.clou.rs350.device.clinterface.IWiringCheck, com.clou.rs350.device.MeterBaseDevice
    public int getWiringCheckCos() {
        return OtherUtils.convertToInt(((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.WiringCheckCos))).Data[0]) - 1;
    }

    @Override // com.clou.rs350.device.clinterface.IWiringCheck, com.clou.rs350.device.MeterBaseDevice
    public ExplainedModel getCorrectionFactor() {
        return DataUtils.parseRound(((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.WiringCheckCorrectionFactor))).Data[0], 4);
    }

    @Override // com.clou.rs350.device.clinterface.IWiringCheck, com.clou.rs350.device.MeterBaseDevice
    public int[] getVoltagePhaseSequenceValue() {
        return new int[]{OtherUtils.convertToInt(((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.WiringCheckU1))).Data[0]), OtherUtils.convertToInt(((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.WiringCheckU2))).Data[0]), OtherUtils.convertToInt(((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.WiringCheckU3))).Data[0])};
    }

    @Override // com.clou.rs350.device.clinterface.IWiringCheck, com.clou.rs350.device.MeterBaseDevice
    public int[] getCurrentPhaseSequenceValue() {
        return new int[]{OtherUtils.convertToInt(((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.WiringCheckI1))).Data[0]), OtherUtils.convertToInt(((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.WiringCheckI2))).Data[0]), OtherUtils.convertToInt(((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.WiringCheckI3))).Data[0])};
    }

    @Override // com.clou.rs350.device.clinterface.IParameterSetting, com.clou.rs350.device.MeterBaseDevice
    public int getBatteryValue() {
        return DataUtils.convertToInt(Double.valueOf(DataUtils.convertToDouble(((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.DeviceBattery))).Data[0]) * 100.0d));
    }

    @Override // com.clou.rs350.device.MeterBaseDevice, com.clou.rs350.device.clinterface.IAdjust
    public boolean getAdjustResult() {
        return (DataUtils.parseDataInt((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.AdjustStateValue))) & 63) == (DataUtils.parseDataInt((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.AdjustStateResultValue))) & 63) && (DataUtils.parseDataInt((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.AdjustStateAngle))) & TIFFConstants.TIFFTAG_SUBFILETYPE) == (DataUtils.parseDataInt((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.AdjustStateResultAngle))) & TIFFConstants.TIFFTAG_SUBFILETYPE);
    }

    /* access modifiers changed from: protected */
    @Override // com.clou.rs350.device.BaseDevice, com.clou.rs350.device.MeterBaseDevice
    public void initRange() {
        this.S_ConnectType_All = new String[]{"U-Direct,I-Direct", "U-Direct,I-Clamp5A", "U-Direct,I-Clamp100A", "U-Direct,I-Clamp500A", "U-Direct,I-Clamp1000A", "I-Clamp100A,I-Clamp5A", "I-Clamp500A,I-Clamp5A", "I-Clamp1000A,I-Clamp5A"};
        this.S_ConnectType_Voltage = new String[]{OtherUtils.getString(R.string.text_U_Direct), OtherUtils.getString(R.string.text_U_Direct), OtherUtils.getString(R.string.text_U_Direct), OtherUtils.getString(R.string.text_U_Direct), OtherUtils.getString(R.string.text_U_Direct)};
        this.S_ConnectType_Current = new String[]{OtherUtils.getString(R.string.text_I_Direct), OtherUtils.getString(R.string.text_I_Clamp10A), OtherUtils.getString(R.string.text_I_Clamp100A), OtherUtils.getString(R.string.text_I_Clamp500A), OtherUtils.getString(R.string.text_I_Clamp1000A)};
        this.VoltageRange_All = new String[]{"5", "30", "60", "120", "240", "480"};
        this.CurrentRange_All = new String[]{"0.02", "0.05", "0.1", "0.2", "0.5", "1", "2", "5", "10", "20", "50", "100", "200", "500", "1000"};
        int[] iArr = new int[8];
        iArr[1] = 1;
        iArr[2] = 2;
        iArr[3] = 3;
        iArr[4] = 4;
        iArr[5] = 5;
        iArr[6] = 6;
        iArr[7] = 7;
        this.I_ConnectType_All_Index = iArr;
        this.S_ConnectType_Realtime = new String[]{"U-Direct,I-Direct", "U-Direct,I-Clamp5A", "U-Direct,I-Clamp100A", "U-Direct,I-Clamp500A", "U-Direct,I-Clamp1000A"};
        int[] iArr2 = new int[5];
        iArr2[1] = 1;
        iArr2[2] = 2;
        iArr2[3] = 3;
        iArr2[4] = 4;
        this.I_ConnectType_Realtime_Index = iArr2;
        this.S_ConnectType_CT = new String[]{"I-Clamp100A,I-Clamp5A", "I-Clamp500A,I-Clamp5A", "I-Clamp1000A,I-Clamp5A"};
        this.I_ConnectType_CT_Index = new int[]{5, 6, 7};
        this.VoltageRange = new ArrayList();
        this.VoltageRange.add(new String[]{"5V", "30V", "60V", "120V", "240V", "480V"});
        this.VoltageRange.add(new String[]{"5V", "30V", "60V", "120V", "240V", "480V"});
        this.VoltageRange.add(new String[]{"5V", "30V", "60V", "120V", "240V", "480V"});
        this.VoltageRange.add(new String[]{"5V", "30V", "60V", "120V", "240V", "480V"});
        this.VoltageRange.add(new String[]{"5V", "30V", "60V", "120V", "240V", "480V"});
        this.VoltageRange.add(new String[]{"2A", "5A", "10A", "20A", "50A", "100A"});
        this.VoltageRange.add(new String[]{"10A", "20A", "50A", "100A", "200A", "500A"});
        this.VoltageRange.add(new String[]{"50A", "100A", "200A", "500A", "1000A"});
        int[] iArr3 = new int[8];
        iArr3[5] = 18;
        iArr3[6] = 24;
        iArr3[7] = 30;
        this.VoltageRangeIndex = iArr3;
        this.CurrentRange = new ArrayList();
        this.CurrentRange.add(new String[]{"0.02A", "0.05A", "0.1A", "0.2A", "0.5A", "1A", "2A", "5A", "10A", "20A", "50A", "100A"});
        this.CurrentRange.add(new String[]{"0.1A", "0.2A", "0.5A", "1A", "2A", "5A"});
        this.CurrentRange.add(new String[]{"2A", "5A", "10A", "20A", "50A", "100A"});
        this.CurrentRange.add(new String[]{"10A", "20A", "50A", "100A", "200A", "500A"});
        this.CurrentRange.add(new String[]{"50A", "100A", "200A", "500A", "1000A"});
        this.CurrentRange.add(new String[]{"0.1A", "0.2A", "0.5A", "1A", "2A", "5A"});
        this.CurrentRange.add(new String[]{"0.1A", "0.2A", "0.5A", "1A", "2A", "5A"});
        this.CurrentRange.add(new String[]{"0.1A", "0.2A", "0.5A", "1A", "2A", "5A"});
        int[] iArr4 = new int[8];
        iArr4[1] = 12;
        iArr4[2] = 18;
        iArr4[3] = 24;
        iArr4[4] = 30;
        iArr4[5] = 12;
        iArr4[6] = 12;
        iArr4[7] = 12;
        this.CurrentRangeIndex = iArr4;
    }

    @Override // com.clou.rs350.device.clinterface.IParameterSetting, com.clou.rs350.device.MeterBaseDevice
    public int getWiringValue() {
        switch (((Number) ((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.Wiring))).Data[0]).intValue()) {
            case 0:
            case 8:
                return 0;
            case 64:
            case 72:
                return 1;
            default:
                return 0;
        }
    }

    @Override // com.clou.rs350.device.clinterface.IParameterSetting, com.clou.rs350.device.MeterBaseDevice
    public int getPQFlagValue() {
        return ((Number) ((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.PQPulseErrorFlag))).Data[0]).intValue();
    }

    @Override // com.clou.rs350.device.clinterface.IParameterSetting, com.clou.rs350.device.MeterBaseDevice
    public int getRangeAutoValue() {
        return ((Number) ((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.RangeAuto))).Data[0]).intValue();
    }

    @Override // com.clou.rs350.device.clinterface.IParameterSetting, com.clou.rs350.device.MeterBaseDevice
    public ExplainedModel[] getConnectTypeValue() {
        int tmpValue = ((Number) ((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.ConnectType))).Data[0]).intValue();
        return new ExplainedModel[]{new ExplainedModel(Integer.valueOf(tmpValue), this.S_ConnectType_Voltage[tmpValue]), new ExplainedModel(Integer.valueOf(tmpValue), this.S_ConnectType_Voltage[tmpValue]), new ExplainedModel(Integer.valueOf(tmpValue), this.S_ConnectType_Voltage[tmpValue]), new ExplainedModel(Integer.valueOf(tmpValue), this.S_ConnectType_Current[tmpValue]), new ExplainedModel(Integer.valueOf(tmpValue), this.S_ConnectType_Current[tmpValue]), new ExplainedModel(Integer.valueOf(tmpValue), this.S_ConnectType_Current[tmpValue])};
    }

    @Override // com.clou.rs350.device.clinterface.IParameterSetting, com.clou.rs350.device.MeterBaseDevice
    public ExplainedModel[] getRangeValue() {
        ExplainedModel[] tmpValue = getConnectTypeValue();
        try {
            return new ExplainedModel[]{new ExplainedModel(((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.ReadRange[0]))).Data[0], ((String[]) this.VoltageRange.get(tmpValue[0].getIntValue()))[((Number) ((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.ReadRange[0]))).Data[0]).intValue() - this.VoltageRangeIndex[tmpValue[0].getIntValue()]]), new ExplainedModel(((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.ReadRange[1]))).Data[0], ((String[]) this.VoltageRange.get(tmpValue[1].getIntValue()))[((Number) ((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.ReadRange[1]))).Data[0]).intValue() - this.VoltageRangeIndex[tmpValue[1].getIntValue()]]), new ExplainedModel(((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.ReadRange[2]))).Data[0], ((String[]) this.VoltageRange.get(tmpValue[2].getIntValue()))[((Number) ((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.ReadRange[2]))).Data[0]).intValue() - this.VoltageRangeIndex[tmpValue[2].getIntValue()]]), new ExplainedModel(((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.ReadRange[3]))).Data[0], ((String[]) this.CurrentRange.get(tmpValue[3].getIntValue()))[((Number) ((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.ReadRange[3]))).Data[0]).intValue() - this.CurrentRangeIndex[tmpValue[3].getIntValue()]]), new ExplainedModel(((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.ReadRange[4]))).Data[0], ((String[]) this.CurrentRange.get(tmpValue[4].getIntValue()))[((Number) ((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.ReadRange[4]))).Data[0]).intValue() - this.CurrentRangeIndex[tmpValue[4].getIntValue()]]), new ExplainedModel(((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.ReadRange[5]))).Data[0], ((String[]) this.CurrentRange.get(tmpValue[5].getIntValue()))[((Number) ((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.ReadRange[5]))).Data[0]).intValue() - this.CurrentRangeIndex[tmpValue[5].getIntValue()]])};
        } catch (Exception e) {
            return new ExplainedModel[]{new ExplainedModel(((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.ReadRange[0]))).Data[0], PdfObject.NOTHING), new ExplainedModel(((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.ReadRange[1]))).Data[0], PdfObject.NOTHING), new ExplainedModel(((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.ReadRange[2]))).Data[0], PdfObject.NOTHING), new ExplainedModel(((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.ReadRange[3]))).Data[0], PdfObject.NOTHING), new ExplainedModel(((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.ReadRange[4]))).Data[0], PdfObject.NOTHING), new ExplainedModel(((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.ReadRange[5]))).Data[0], PdfObject.NOTHING)};
        }
    }

    @Override // com.clou.rs350.device.MeterBaseDevice, com.clou.rs350.device.clinterface.ITest
    public ExplainedModel[] getErrorsValues(int index) {
        return new ExplainedModel[]{DataUtils.parseRound(((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.ErrorMeterError1[index]))).Data[0], 3), DataUtils.parseRound(((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.ErrorMeterError2[index]))).Data[0], 3), DataUtils.parseRound(((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.ErrorMeterError3[index]))).Data[0], 3), DataUtils.parseRound(((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.ErrorMeterError4[index]))).Data[0], 3), DataUtils.parseRound(((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.ErrorMeterError5[index]))).Data[0], 3), new ExplainedModel(), new ExplainedModel(), new ExplainedModel(), new ExplainedModel(), new ExplainedModel(), new ExplainedModel(), DataUtils.parseRound(((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.ErrorMeterErrorAvg[index]))).Data[0], 3), DataUtils.parseRound(((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.ErrorMeterErrorStd[index]))).Data[0], 3), DataUtils.parseint(((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.ErrorMeterPulseNum[index]))).Data[0]), DataUtils.parseint(((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.ErrorErrorCount[index]))).Data[0])};
    }

    @Override // com.clou.rs350.device.MeterBaseDevice, com.clou.rs350.device.clinterface.ITest
    public ExplainedModel getMeterConstantValues(int index) {
        return DataUtils.parseint(((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.ErrorMeterConstants[index]))).Data[0]);
    }

    @Override // com.clou.rs350.device.MeterBaseDevice, com.clou.rs350.device.clinterface.ITest
    public List<byte[]> getMeterConstant() {
        return this.m_CLT.AskData(new int[]{this.m_Dictionary.ErrorMeterConstants[0], this.m_Dictionary.ErrorMeterConstants[1], this.m_Dictionary.ErrorMeterConstants[2]});
    }

    @Override // com.clou.rs350.device.clinterface.IPTCTMeasurement, com.clou.rs350.device.MeterBaseDevice
    public ExplainedModel[] getCTErrorValue() {
        return new ExplainedModel[]{DataUtils.parse(((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.MeterIaValue))).Data[0], "A"), DataUtils.parse(((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.MeterIbValue))).Data[0], "A"), DataUtils.parseRound(((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.CTCurrentValueRatio))).Data[0], 3), DataUtils.parse(Double.valueOf(AngleUtil.adjustAngle(DataUtils.convertToDouble(((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.MeterIbAngle))).Data[0]) - DataUtils.convertToDouble(((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.MeterIaAngle))).Data[0]))), "°")};
    }

    @Override // com.clou.rs350.device.clinterface.IPTCTMeasurement, com.clou.rs350.device.MeterBaseDevice
    public ExplainedModel[] getCTBurdenValue() {
        return new ExplainedModel[]{DataUtils.parse(((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.MeterUbValue))).Data[0], "V"), DataUtils.parse(((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.CTCurrentValueSecondary))).Data[0], "A"), DataUtils.parse(((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.CTBurden))).Data[0], "VA")};
    }

    @Override // com.clou.rs350.device.clinterface.IParameterSetting, com.clou.rs350.device.MeterBaseDevice
    public int getDefinitionValue() {
        return 0;
    }

    @Override // com.clou.rs350.device.clinterface.IParameterSetting, com.clou.rs350.device.MeterBaseDevice
    public List<byte[]> setWiringData(int Wiring, int Pulse, int Definition) {
        switch (Wiring) {
            case 0:
                this.m_Wiring = 8;
                break;
            case 1:
                this.m_Wiring = 72;
                break;
            default:
                this.m_Wiring = 8;
                break;
        }
        ((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.Wiring))).Data[0] = Integer.valueOf(this.m_Wiring);
        ((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.PQPulseErrorFlag))).Data[0] = Integer.valueOf(Pulse);
        this.m_CLT.DataMap = this.m_Data;
        return this.m_CLT.WriteData(new int[]{this.m_Dictionary.Wiring, this.m_Dictionary.PQPulseErrorFlag});
    }

    @Override // com.clou.rs350.device.clinterface.IPTCTMeasurement, com.clou.rs350.device.MeterBaseDevice
    public List<byte[]> getPTBurden() {
        return null;
    }

    @Override // com.clou.rs350.device.clinterface.IPTCTMeasurement, com.clou.rs350.device.MeterBaseDevice
    public ExplainedModel[] getPTBurdenValue() {
        return new ExplainedModel[]{new ExplainedModel(), new ExplainedModel(), new ExplainedModel(), new ExplainedModel(), new ExplainedModel(), new ExplainedModel(), new ExplainedModel(), new ExplainedModel(), new ExplainedModel(), new ExplainedModel(), new ExplainedModel(), new ExplainedModel(), new ExplainedModel(), new ExplainedModel(), new ExplainedModel()};
    }

    @Override // com.clou.rs350.device.MeterBaseDevice, com.clou.rs350.device.clinterface.IAdjust
    public List<byte[]> getAllowAdjustFlag() {
        return null;
    }

    @Override // com.clou.rs350.device.MeterBaseDevice, com.clou.rs350.device.clinterface.IAdjust
    public boolean getAllowAdjustFlagValue(Context context) {
        return true;
    }

    @Override // com.clou.rs350.device.MeterBaseDevice, com.clou.rs350.device.clinterface.IAdjust
    public List<byte[]> cleanAllowAdjustFlag() {
        return null;
    }

    @Override // com.clou.rs350.device.clinterface.IAdjust
    public List<byte[]> getClampAdjustmentFlag() {
        return null;
    }

    @Override // com.clou.rs350.device.clinterface.IAdjust
    public int[] getClampAdjustmentFlagValue() {
        return new int[7];
    }
}
