package com.clou.rs350.device.constants;

public class CLTcmd {
    public static final byte ANSARY = 85;
    public static final byte ANSBOOK = 101;
    public static final byte ANSDAT = 80;
    public static final byte ANSKEY = 96;
    public static final byte ANSMEM = 89;
    public static final byte ANSVER = 57;
    public static final byte ASKARY = -91;
    public static final byte ASKBOOK = -107;
    public static final byte ASKDAT = -96;
    public static final byte ASKEVN = -61;
    public static final byte ASKKEY = -112;
    public static final byte ASKMEM = -87;
    public static final byte ASKOK = -64;
    public static final byte ASKVER = -55;
    public static final byte ECHBSY = 53;
    public static final byte ECHERR = 51;
    public static final byte ECHINH = 54;
    public static final byte ECHOK = 48;
    public static final byte PASSWD = -109;
    public static final byte READMETER = -42;
    public static final byte SENDBUFFER = 2;
    public static final byte SETBAUDRATE = 1;
    public static final byte WRTARY = -90;
    public static final byte WRTDAT = -93;
    public static final byte WRTMEM = -86;
}
