package com.clou.rs350.device.clinterface;

import com.clou.rs350.device.model.ExplainedModel;
import java.util.List;

public interface IPTCTMeasurement {
    List<byte[]> adjustPT();

    List<byte[]> cleanAutomaticCheckState();

    List<byte[]> getAutomaticCheckState();

    ExplainedModel[] getCTBurdenValue();

    ExplainedModel[] getCTErrorValue();

    List<byte[]> getCTValue();

    List<byte[]> getGPS();

    List<byte[]> getPTBurden();

    ExplainedModel[] getPTBurdenValue();

    int[] getPTConnectStateValue();

    List<byte[]> getPTDeviceState();

    List<byte[]> getPTError();

    ExplainedModel[] getPTErrorValue();

    List<byte[]> setAutomaticCheck(int i);

    List<byte[]> setCTChannel(int i);

    List<byte[]> setPTAddress(int i, int i2);

    List<byte[]> setPTConnect(int i);

    List<byte[]> setPTSettingFlag(int i);
}
