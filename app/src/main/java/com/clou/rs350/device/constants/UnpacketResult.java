package com.clou.rs350.device.constants;

public class UnpacketResult {
    public static final byte ASK = 16;
    public static final byte ASKERROR = -112;
    public static final byte BANWRITE = -126;
    public static final byte BUSY = -127;
    public static final byte DATAERROR = -125;
    public static final byte HASCONTINUE = 3;
    public static final byte MAPEMPTY = -124;
    public static final byte RECEIVEBURRER = 2;
    public static final byte RECEIVEDATA = 1;
    public static final byte SENDFALSE = Byte.MIN_VALUE;
    public static final byte SENDSUCCESS = 0;
    public static final byte WRITEDATA = 17;
    public static final byte WRITEERROR = -111;
}
