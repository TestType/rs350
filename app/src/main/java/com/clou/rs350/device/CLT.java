package com.clou.rs350.device;

import com.clou.rs350.device.constants.CLTcmd;
import com.clou.rs350.device.constants.UnpacketResult;
import com.clou.rs350.device.model.Buffer;
import com.clou.rs350.device.model.ConnectByteModel;
import com.clou.rs350.device.model.DataModel;
import com.clou.rs350.device.utils.BufferUtil;
import com.itextpdf.text.pdf.codec.JBIG2SegmentReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class CLT {
    public Map<Integer, DataModel> DataMap = null;
    public ConnectByteModel dataBuffer = new ConnectByteModel();
    Buffer m_ReceiveBuffer = new Buffer();
    private byte m_RxID = 0;
    private byte m_TxID = 0;

    public interface IOrganizeData {
        byte[] organizeData(int i, byte b, byte[] bArr, int i2);
    }

    public CLT(int txID, int rxID, Map<Integer, DataModel> map) {
        this.m_TxID = (byte) txID;
        this.m_RxID = (byte) rxID;
        this.DataMap = map;
    }

    public List<byte[]> AskData(int[] key) {
        return organizeData(key, new IOrganizeData() {
            /* class com.clou.rs350.device.CLT.AnonymousClass1 */

            @Override // com.clou.rs350.device.CLT.IOrganizeData
            public byte[] organizeData(int page, byte group, byte[] data, int length) {
                return CLT.this.organizeAskData(page, group, data);
            }
        });
    }

    public List<byte[]> WriteData(int[] key) {
        return organizeData(key, new IOrganizeData() {
            /* class com.clou.rs350.device.CLT.AnonymousClass2 */

            @Override // com.clou.rs350.device.CLT.IOrganizeData
            public byte[] organizeData(int page, byte group, byte[] data, int length) {
                return CLT.this.organizeWriteData(page, group, data, length);
            }
        });
    }

    public byte[] AnsOK() {
        return Packet((byte) 48, new byte[0]);
    }

    public byte[] AnsErr() {
        return Packet(CLTcmd.ECHERR, new byte[0]);
    }

    public List<byte[]> AskArray(int[] key) {
        if (this.DataMap == null) {
            return null;
        }
        List<byte[]> byteList = new ArrayList<>();
        for (int i = 0; i < key.length; i++) {
            byte[] buffer = new byte[5];
            buffer[0] = (byte) (key[i] >> 16);
            buffer[1] = (byte) (((key[i] >> 5) & 56) + (key[i] & 7));
            int dataLen = this.DataMap.get(Integer.valueOf(key[i])).DataLength;
            int maxLen = this.DataMap.get(Integer.valueOf(key[i])).ArrayLength * dataLen;
            int start = 0;
            int len = (244 / dataLen) * dataLen;
            while (maxLen > 0) {
                buffer[2] = (byte) (start & 255);
                buffer[3] = (byte) ((start >> 8) & 255);
                if (maxLen < len) {
                    len = maxLen;
                }
                buffer[4] = (byte) len;
                maxLen -= len;
                start += len;
                byteList.add(Packet(CLTcmd.ASKARY, buffer));
            }
        }
        return byteList;
    }

    public List<byte[]> WriteArray(int[] key) {
        if (this.DataMap == null) {
            return null;
        }
        List<byte[]> byteList = new ArrayList<>();
        for (int i = 0; i < key.length; i++) {
            DataModel data = this.DataMap.get(Integer.valueOf(key[i]));
            int arrayLen = data.ArrayLength;
            int start = 0;
            int index = 0;
            int len = 239 / data.DataLength;
            while (arrayLen > 0) {
                if (arrayLen < len) {
                    len = arrayLen;
                }
                int bufferLen = (data.DataLength * len) + 5;
                byte[] buffer = new byte[bufferLen];
                buffer[0] = (byte) (key[i] >> 16);
                buffer[1] = (byte) (((key[i] >> 5) & 56) + (key[i] & 7));
                buffer[2] = (byte) (start & 255);
                buffer[3] = (byte) ((start >> 8) & 255);
                for (int j = 5; j < bufferLen; j += data.DataLength) {
                    buffer[4] = (byte) (buffer[4] + data.DataLength);
                    System.arraycopy(getbyte(data.DataType, data.Data[index], data.getMultiple(), data.FormatString), 0, buffer, j, data.DataLength);
                    index++;
                    start += data.DataLength;
                }
                arrayLen -= len;
                byteList.add(Packet(CLTcmd.WRTARY, buffer));
            }
        }
        return byteList;
    }

    public int UnPacket(byte[] ReceiveBuffer) {
        try {
            if (this.DataMap == null) {
                return -124;
            }
            this.m_ReceiveBuffer.MergeBuffer(ReceiveBuffer);
            if (this.m_ReceiveBuffer.ReceiveBuffer[0] != -127) {
                this.m_ReceiveBuffer = new Buffer();
                return -125;
            } else if (this.m_ReceiveBuffer.length < 6 || this.m_ReceiveBuffer.length < (this.m_ReceiveBuffer.ReceiveBuffer[3] & 255)) {
                return -125;
            } else {
                if (this.m_ReceiveBuffer.ReceiveBuffer[(this.m_ReceiveBuffer.ReceiveBuffer[3] & 255) - 1] != BufferUtil.getXor(this.m_ReceiveBuffer.ReceiveBuffer, 1, (this.m_ReceiveBuffer.ReceiveBuffer[3] & 255) - 2)) {
                    this.m_ReceiveBuffer = new Buffer();
                    return -125;
                } else if ((this.m_ReceiveBuffer.ReceiveBuffer[1] == this.m_TxID || this.m_ReceiveBuffer.ReceiveBuffer[1] == -1) && this.m_ReceiveBuffer.ReceiveBuffer[2] == this.m_RxID) {
                    int result = 0;
                    switch (this.m_ReceiveBuffer.ReceiveBuffer[4]) {
                        case -93:
                            if (1 != UnpacketData(this.m_ReceiveBuffer.ReceiveBuffer)) {
                                result = -111;
                                break;
                            } else {
                                result = 17;
                                break;
                            }
                        case -90:
                            if (1 != UnpacketArray(this.m_ReceiveBuffer.ReceiveBuffer)) {
                                result = -111;
                                break;
                            } else {
                                result = 17;
                                break;
                            }
                        case -64:
                            result = 16;
                            break;
                        case -42:
                            result = UnpacketBuffer(this.m_ReceiveBuffer.ReceiveBuffer);
                            break;
                        case JBIG2SegmentReader.PAGE_INFORMATION /*{ENCODED_INT: 48}*/:
                            result = 0;
                            break;
                        case JBIG2SegmentReader.END_OF_FILE /*{ENCODED_INT: 51}*/:
                            result = -128;
                            break;
                        case 53:
                            result = -127;
                            break;
                        case 54:
                            result = -126;
                            break;
                        case 80:
                            result = UnpacketData(this.m_ReceiveBuffer.ReceiveBuffer);
                            break;
                        case 85:
                            result = UnpacketArray(this.m_ReceiveBuffer.ReceiveBuffer);
                            break;
                    }
                    this.m_ReceiveBuffer = new Buffer();
                    return result;
                } else {
                    this.m_ReceiveBuffer = new Buffer();
                    return -125;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            return -125;
        }
    }

    private List<byte[]> organizeData(int[] key, IOrganizeData DataPacket) {
        if (this.DataMap == null) {
            return null;
        }
        byte[] page = new byte[8];
        byte[][] group = {new byte[8], new byte[8], new byte[8], new byte[8], new byte[8], new byte[8], new byte[8], new byte[8]};
        int[] byteLen = new int[8];
        List<byte[]> byteList = new ArrayList<>();
        for (int i = 0; i < key.length; i++) {
            int intPage = key[i] >> 16;
            int intGroup = (key[i] >> 8) & 255;
            int intdata = key[i] & 255;
            if (this.DataMap.get(Integer.valueOf(key[i])).DataLength + byteLen[intPage] > 239) {
                byteList.add(DataPacket.organizeData(intPage, page[intPage], group[intPage], byteLen[intPage]));
                page[intPage] = 0;
                System.arraycopy(new byte[8], 0, group[intPage], 0, 8);
                byteLen[intPage] = 0;
            }
            page[intPage] = (byte) (page[intPage] | (1 << intGroup));
            group[intPage][intGroup] = (byte) (group[intPage][intGroup] | (1 << intdata));
            byteLen[intPage] = this.DataMap.get(Integer.valueOf(key[i])).DataLength + byteLen[intPage];
        }
        for (int i2 = 0; i2 < page.length; i2++) {
            if (page[i2] != 0) {
                byteList.add(DataPacket.organizeData(i2, page[i2], group[i2], byteLen[i2]));
            }
        }
        return byteList;
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private byte[] organizeAskData(int page, byte group, byte[] data) {
        byte[] tmpbuffer = new byte[10];
        int bufferLen = 2;
        tmpbuffer[0] = (byte) page;
        tmpbuffer[1] = group;
        for (int j = 0; j < 8; j++) {
            if (data[j] != 0) {
                tmpbuffer[bufferLen] = data[j];
                bufferLen++;
            }
        }
        byte[] buffer = new byte[bufferLen];
        System.arraycopy(tmpbuffer, 0, buffer, 0, bufferLen);
        return Packet(CLTcmd.ASKDAT, buffer);
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private byte[] organizeWriteData(int page, byte group, byte[] data, int len) {
        byte[] tmpBuffer = new byte[(len + 9)];
        int bufferLen = 2;
        tmpBuffer[0] = (byte) page;
        tmpBuffer[1] = group;
        for (int j = 0; j < 8; j++) {
            if (data[j] != 0) {
                tmpBuffer[bufferLen] = data[j];
                bufferLen++;
                for (int k = 0; k < 8; k++) {
                    if (((data[j] >> k) & 1) != 0) {
                        DataModel model = this.DataMap.get(Integer.valueOf((page << 16) + (j << 8) + k));
                        System.arraycopy(getbyte(model.DataType, model.Data[0], model.getMultiple(), model.FormatString), 0, tmpBuffer, bufferLen, model.DataLength);
                        bufferLen += model.DataLength;
                    }
                }
            }
        }
        byte[] buffer = new byte[bufferLen];
        System.arraycopy(tmpBuffer, 0, buffer, 0, bufferLen);
        return Packet(CLTcmd.WRTDAT, buffer);
    }

    private byte[] Packet(byte cmd, byte[] inputBuffer) {
        int len = inputBuffer.length + 6;
        byte[] buffer = new byte[len];
        buffer[0] = UnpacketResult.BUSY;
        buffer[1] = this.m_RxID;
        buffer[2] = this.m_TxID;
        buffer[3] = (byte) len;
        buffer[4] = cmd;
        System.arraycopy(inputBuffer, 0, buffer, 5, inputBuffer.length);
        buffer[len - 1] = BufferUtil.getXor(buffer, 1, len - 2);
        return buffer;
    }

    private int UnpacketData(byte[] inputBuffer) {
        try {
            byte page = inputBuffer[5];
            byte group = inputBuffer[6];
            int index = 7;
            for (int i = 0; i < 8; i++) {
                if (((group >> i) & 1) == 1) {
                    byte data = inputBuffer[index];
                    index++;
                    for (int j = 0; j < 8; j++) {
                        if (((data >> j) & 1) == 1) {
                            int key = (page << 16) + (i << 8) + j;
                            if (this.DataMap.containsKey(Integer.valueOf(key))) {
                                DataModel dataItem = this.DataMap.get(Integer.valueOf(key));
                                byte[] buffer = new byte[dataItem.DataLength];
                                System.arraycopy(inputBuffer, index, buffer, 0, dataItem.DataLength);
                                dataItem.Data[0] = getData(dataItem.DataType, buffer, dataItem.getMultiple());
                                this.DataMap.put(Integer.valueOf(key), dataItem);
                                index += dataItem.DataLength;
                            }
                        }
                    }
                }
            }
            return 1;
        } catch (Exception e) {
            e.printStackTrace();
            return -125;
        }
    }

    private int UnpacketArray(byte[] inputBuffer) {
        try {
            int key = (inputBuffer[5] << 16) + ((inputBuffer[6] & 56) << 5) + (inputBuffer[6] & 7);
            if (!this.DataMap.containsKey(Integer.valueOf(key))) {
                return 131;
            }
            DataModel dataItem = this.DataMap.get(Integer.valueOf(key));
            int start = ((inputBuffer[7] & 255) + (inputBuffer[8] << 8)) & 65535;
            int offset = start % dataItem.DataLength;
            int offset2 = offset == 0 ? 0 : dataItem.DataLength - offset;
            int index = (start + offset2) / dataItem.DataLength;
            int offset3 = offset2 + 10;
            for (int len = (inputBuffer[9] & 255) - offset2; len >= dataItem.DataLength; len -= dataItem.DataLength) {
                byte[] buffer = new byte[dataItem.DataLength];
                System.arraycopy(inputBuffer, offset3, buffer, 0, dataItem.DataLength);
                dataItem.Data[index] = getData(dataItem.DataType, buffer, dataItem.getMultiple());
                index++;
                offset3 += dataItem.DataLength;
            }
            this.DataMap.put(Integer.valueOf(key), dataItem);
            return 1;
        } catch (Exception e) {
            e.printStackTrace();
            return -125;
        }
    }

    private int UnpacketBuffer(byte[] inputBuffer) {
        try {
            this.dataBuffer.Buffer = new byte[(inputBuffer[3] - 8)];
            System.arraycopy(inputBuffer, 7, this.dataBuffer.Buffer, 0, this.dataBuffer.Buffer.length);
            return 2;
        } catch (Exception e) {
            e.printStackTrace();
            return -125;
        }
    }

    private Object getData(int type, byte[] inputBuffer, float multiple) {
        if (multiple != 1.0f) {
            switch (type) {
                case 1:
                    return Byte.valueOf(inputBuffer[0]);
                case 2:
                case 3:
                case 4:
                case 5:
                case 15:
                    return Float.valueOf((((float) BufferUtil.byteToint(inputBuffer, false, false)) / 1.0f) / multiple);
                case 6:
                case 7:
                case 8:
                case 9:
                    return Float.valueOf((((float) BufferUtil.byteToint(inputBuffer, false, true)) / 1.0f) / multiple);
                case 10:
                    return Float.valueOf(BufferUtil.byteTofloat(inputBuffer) / multiple);
                case 11:
                    return Double.valueOf(BufferUtil.byteTodouble(inputBuffer) / ((double) multiple));
                case 12:
                case 13:
                    return Double.valueOf(BufferUtil.byteTointxe1(inputBuffer) / ((double) multiple));
                case 14:
                    return Byte.valueOf(BufferUtil.BCDToint(inputBuffer[0]));
                default:
                    return null;
            }
        } else {
            switch (type) {
                case 1:
                    return Byte.valueOf(inputBuffer[0]);
                case 2:
                case 3:
                case 4:
                case 5:
                case 15:
                    return Long.valueOf(BufferUtil.byteToint(inputBuffer, false, false));
                case 6:
                case 7:
                case 8:
                case 9:
                    return Long.valueOf(BufferUtil.byteToint(inputBuffer, false, true));
                case 10:
                    return Float.valueOf(BufferUtil.byteTofloat(inputBuffer));
                case 11:
                    return Double.valueOf(BufferUtil.byteTodouble(inputBuffer));
                case 12:
                case 13:
                    return Double.valueOf(BufferUtil.byteTointxe1(inputBuffer));
                case 14:
                    return Byte.valueOf(BufferUtil.BCDToint(inputBuffer[0]));
                default:
                    return null;
            }
        }
    }

    private byte[] getbyte(int type, Object data, float multiple, String formatString) {
        if (1.0f != multiple) {
            switch (type) {
                case 1:
                    return new byte[]{((Number) data).byteValue()};
                case 2:
                case 6:
                    return BufferUtil.intTobyte((long) (((Number) data).doubleValue() * ((double) multiple)), false, 1);
                case 3:
                case 7:
                    return BufferUtil.intTobyte((long) (((Number) data).doubleValue() * ((double) multiple)), false, 2);
                case 4:
                case 8:
                    return BufferUtil.intTobyte((long) (((Number) data).doubleValue() * ((double) multiple)), false, 3);
                case 5:
                case 9:
                    return BufferUtil.intTobyte((long) (((Number) data).doubleValue() * ((double) multiple)), false, 4);
                case 10:
                    return BufferUtil.floatTobyte(((Number) data).floatValue() * multiple);
                case 11:
                    return BufferUtil.doubleTobyte(((Number) data).doubleValue() * ((double) multiple));
                case 12:
                    return BufferUtil.intxe1Tobyte(((Number) data).doubleValue() * ((double) multiple), 3, formatString);
                case 13:
                    return BufferUtil.intxe1Tobyte(((Number) data).doubleValue() * ((double) multiple), 4, formatString);
                case 14:
                    return new byte[]{BufferUtil.intToBCD(((Number) data).byteValue())};
                case 15:
                    return BufferUtil.intTobyte((long) (((Number) data).doubleValue() * ((double) multiple)), false, 8);
                default:
                    return null;
            }
        } else {
            switch (type) {
                case 1:
                    return new byte[]{((Number) data).byteValue()};
                case 2:
                case 6:
                    return BufferUtil.intTobyte(((Number) data).longValue(), false, 1);
                case 3:
                case 7:
                    return BufferUtil.intTobyte(((Number) data).longValue(), false, 2);
                case 4:
                case 8:
                    return BufferUtil.intTobyte(((Number) data).longValue(), false, 3);
                case 5:
                case 9:
                    return BufferUtil.intTobyte(((Number) data).longValue(), false, 4);
                case 10:
                    return BufferUtil.floatTobyte(((Number) data).floatValue());
                case 11:
                    return BufferUtil.doubleTobyte(((Number) data).doubleValue());
                case 12:
                    return BufferUtil.intxe1Tobyte(((Number) data).doubleValue(), 3, formatString);
                case 13:
                    return BufferUtil.intxe1Tobyte(((Number) data).doubleValue(), 4, formatString);
                case 14:
                    return new byte[]{BufferUtil.intToBCD(((Number) data).byteValue())};
                case 15:
                    return BufferUtil.intTobyte(((Number) data).longValue(), false, 8);
                default:
                    return null;
            }
        }
    }

    public List<byte[]> SetBaudrate(int comPort, int baudrate, int wordlength, int parity, int stopbits) {
        List<byte[]> buffer = new ArrayList<>();
        buffer.add(Packet(CLTcmd.READMETER, new byte[]{(byte) comPort, 1, 1, (byte) baudrate, (byte) wordlength, (byte) parity, (byte) stopbits, CLTcmd.WRTMEM, CLTcmd.WRTMEM, CLTcmd.WRTMEM}));
        return buffer;
    }

    public byte[] GetSendBuffer(int comPort, byte[] buffer) {
        int bufferLength = buffer.length;
        byte[] tmpBuffer = new byte[(bufferLength + 2)];
        tmpBuffer[0] = (byte) comPort;
        tmpBuffer[1] = 2;
        System.arraycopy(buffer, 0, tmpBuffer, 2, bufferLength);
        return Packet(CLTcmd.READMETER, tmpBuffer);
    }
}
