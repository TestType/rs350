package com.clou.rs350.device.clinterface;

import com.clou.rs350.device.model.ExplainedModel;
import java.util.List;

public interface IMeter {
    ExplainedModel[] getCosValue();

    ExplainedModel[] getCurrentAngleValue();

    ExplainedModel[] getCurrentCrestFactorValue();

    ExplainedModel[] getCurrentValue();

    ExplainedModel[] getFrequencyValue();

    List<byte[]> getHarmonic();

    List<byte[]> getHarmonic(int i);

    List<byte[]> getHarmonic(String str);

    List<byte[]> getHarmonicAngles(int i);

    Double[] getHarmonicAnglesValue(int i);

    Double[] getHarmonicContantsValue(int i);

    List<byte[]> getHarmonicContents(int i);

    List<byte[]> getHarmonicValues(int i);

    List<byte[]> getHarmonicValues(int i, int i2);

    ExplainedModel[] getHarmonicValuesValue(int i, int i2);

    ExplainedModel[] getLLVoltageAngleValue();

    ExplainedModel[] getLLVoltageCurrentAngleValue();

    ExplainedModel[] getLLVoltageValue();

    ExplainedModel[] getLNVoltageAngleValue();

    ExplainedModel[] getLNVoltageCurrentAngleValue();

    ExplainedModel[] getLNVoltageValue();

    List<byte[]> getMeasureBasicData();

    List<byte[]> getMeasureSpecialData();

    ExplainedModel[] getNeutralValue();

    ExplainedModel[] getPQPPValue();

    List<byte[]> getPQWave();

    Double[] getPQWaveValue(int i);

    ExplainedModel[] getPValue();

    ExplainedModel[] getQValue();

    ExplainedModel[] getSValue();

    ExplainedModel[] getSinValue();

    List<byte[]> getTHD();

    ExplainedModel[] getTHDValue();

    ExplainedModel[] getUIPPValue();

    List<byte[]> getUIWave();

    Double[] getUIWaveValue(int i);

    ExplainedModel[] getVoltageCrestFactorValue();
}
