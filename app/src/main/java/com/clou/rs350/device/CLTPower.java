package com.clou.rs350.device;

public class CLTPower extends BaseDevice {
    private CLT m_CLT;

    public CLTPower(BaseDictionary Dictionary, int TxID, int RxID) {
        this.m_Dictionary = Dictionary;
        this.m_Data = this.m_Dictionary.getDataMap();
        this.m_CLT = new CLT(TxID, RxID, this.m_Data);
    }

    @Override // com.clou.rs350.device.BaseDevice
    public int UnPacket(byte[] ReceiveBuffer) {
        int returnValue = this.m_CLT.UnPacket(ReceiveBuffer);
        if (132 == returnValue) {
            this.m_CLT.DataMap = this.m_Data;
            returnValue = this.m_CLT.UnPacket(ReceiveBuffer);
        }
        this.m_Data = this.m_CLT.DataMap;
        return returnValue;
    }

    @Override // com.clou.rs350.device.BaseDevice
    public byte[] AnsOK() {
        return null;
    }

    @Override // com.clou.rs350.device.BaseDevice
    public byte[] AnsErr() {
        return null;
    }

    /* access modifiers changed from: protected */
    @Override // com.clou.rs350.device.BaseDevice
    public void initRange() {
    }
}
