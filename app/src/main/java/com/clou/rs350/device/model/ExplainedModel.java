package com.clou.rs350.device.model;

import java.io.Serializable;

public class ExplainedModel implements Serializable {
    private Object o_Value = 0;
    private String s_Value = "---";

    public ExplainedModel() {
    }

    public ExplainedModel(Object Value, String stringValue) {
        this.o_Value = Value;
        this.s_Value = stringValue;
    }

    public String toString() {
        return this.s_Value;
    }

    public String getStringValue() {
        return this.s_Value;
    }

    public double getDoubleValue() {
        return ((Number) this.o_Value).doubleValue();
    }

    public int getIntValue() {
        return ((Number) this.o_Value).intValue();
    }
}
