package com.clou.rs350.device.constants;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class LinkMapList {
    private Map<Object, Object> keyMap = new HashMap();
    private Map<Object, Object> valueMap = new HashMap();
    private List<Object> values = new ArrayList();

    public void addData(Object key, Object value) {
        if (key != null && value != null && !this.keyMap.containsKey(key)) {
            this.keyMap.put(key, value);
            this.valueMap.put(value, key);
            this.values.add(value);
        }
    }

    public Object getDataByKey(Object key) {
        if (key != null) {
            return this.keyMap.get(key);
        }
        return null;
    }

    /* JADX INFO: Multiple debug info for r0v1 'value'  java.lang.Object: [D('value' java.lang.Object), D('defaultValue' java.lang.Object)] */
    public Object getDataByKey(Object key, Object defaultValue) {
        Object value = null;
        if (key != null) {
            value = this.keyMap.get(key);
        }
        return value == null ? defaultValue : value;
    }

    public Object getKeyByData(Object value) {
        if (value != null) {
            return this.valueMap.get(value);
        }
        return null;
    }

    public int getSize() {
        return this.keyMap.size();
    }

    public void removeKey(Object key) {
        if (key != null && this.keyMap.containsKey(key)) {
            Object value = this.keyMap.get(key);
            this.keyMap.remove(key);
            if (value != null && this.valueMap.containsKey(value)) {
                this.valueMap.remove(value);
            }
        }
    }

    public List<Object> getKeyList() {
        List<Object> keyList = new ArrayList<>();
        keyList.addAll(this.keyMap.keySet());
        return keyList;
    }

    public List<Object> getValueList() {
        List<Object> valueList = new ArrayList<>();
        valueList.addAll(this.keyMap.values());
        return valueList;
    }
}
