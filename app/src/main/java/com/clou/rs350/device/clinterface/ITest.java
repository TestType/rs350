package com.clou.rs350.device.clinterface;

import com.clou.rs350.device.model.ExplainedModel;
import java.util.List;

public interface ITest {
    List<byte[]> getCheckKFlag();

    int getCheckKFlagValue();

    List<byte[]> getDemand();

    ExplainedModel[] getDemandValues();

    List<byte[]> getEnergy();

    ExplainedModel[] getEnergyValues();

    List<byte[]> getErrors();

    ExplainedModel[] getErrorsValues(int i);

    List<byte[]> getMeterConstant();

    ExplainedModel getMeterConstantValues(int i);

    List<byte[]> setCheckKFlag(int i);

    List<byte[]> setDailyParameters(int[] iArr, int[] iArr2, int[] iArr3);

    List<byte[]> setErrorParameters(double d, int[] iArr, int[] iArr2, float[] fArr, int[] iArr3);

    List<byte[]> setErrorParameters(float[] fArr, int[] iArr);

    List<byte[]> setErrorParameters(int[] iArr);

    List<byte[]> setErrorParameters(int[] iArr, float[] fArr, int[] iArr2);

    List<byte[]> setErrorParameters(int[] iArr, int[] iArr2, float[] fArr, int[] iArr3);

    List<byte[]> setTestMode(int i);
}
