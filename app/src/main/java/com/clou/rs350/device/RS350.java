package com.clou.rs350.device;

import android.content.Context;

import com.clou.rs350.R;
import com.clou.rs350.db.model.DigitalMeterDataType;
import com.clou.rs350.db.model.StandardInfo;
import com.clou.rs350.device.constants.CLTConstant;
import com.clou.rs350.device.model.DataModel;
import com.clou.rs350.device.model.ExplainedModel;
import com.clou.rs350.device.utils.DataUtils;
import com.clou.rs350.utils.OtherUtils;
import com.itextpdf.text.pdf.PdfObject;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

public class RS350 extends CL3122 {
    public RS350() {
        super(new RS350Dictionary());
        initRange();
    }

    /* access modifiers changed from: protected */
    @Override // com.clou.rs350.device.BaseDevice, com.clou.rs350.device.CL3122, com.clou.rs350.device.MeterBaseDevice
    public void initRange() {
        this.S_ConnectType_All = new String[]{String.valueOf(OtherUtils.getString(R.string.text_U_Direct)) + "," + OtherUtils.getString(R.string.text_I_Direct), String.valueOf(OtherUtils.getString(R.string.text_U_Direct)) + "," + OtherUtils.getString(R.string.text_I_Clamp10A), String.valueOf(OtherUtils.getString(R.string.text_U_Direct)) + "," + OtherUtils.getString(R.string.text_I_Clamp100A), String.valueOf(OtherUtils.getString(R.string.text_U_Direct)) + "," + OtherUtils.getString(R.string.text_I_Clamp500A), String.valueOf(OtherUtils.getString(R.string.text_U_Direct)) + "," + OtherUtils.getString(R.string.text_I_Clamp1000A), String.valueOf(OtherUtils.getString(R.string.text_Pri_Clamp100A)) + "," + OtherUtils.getString(R.string.text_Sec_Clamp10A), String.valueOf(OtherUtils.getString(R.string.text_Pri_Clamp500A)) + "," + OtherUtils.getString(R.string.text_Sec_Clamp10A), String.valueOf(OtherUtils.getString(R.string.text_Pri_Clamp1000A)) + "," + OtherUtils.getString(R.string.text_Sec_Clamp10A)};
        this.S_ConnectType_Voltage = new String[]{OtherUtils.getString(R.string.text_U_Direct)};
        this.S_ConnectType_Current = new String[]{OtherUtils.getString(R.string.text_I_Direct), OtherUtils.getString(R.string.text_I_Clamp10A), OtherUtils.getString(R.string.text_I_Clamp100A), OtherUtils.getString(R.string.text_I_Clamp500A), OtherUtils.getString(R.string.text_I_Clamp1000A), OtherUtils.getString(R.string.text_I_flexct2000A), OtherUtils.getString(R.string.text_I_Clamp2000A), OtherUtils.getString(R.string.text_I_flexct3000A)};
        this.VoltageRange_All = new String[]{"60", "120", "240", "480"};
        this.CurrentRange_All = new String[]{"0.01", "0.02", "0.05", "0.1", "0.2", "0.5", "1", "2", "5", "10", "20", "50", "100", "200", "500", "1000", "1500", "2000", "3000"};
        int[] iArr = new int[8];
        iArr[1] = 1;
        iArr[2] = 2;
        iArr[3] = 3;
        iArr[4] = 4;
        iArr[5] = 5;
        iArr[6] = 6;
        iArr[7] = 7;
        this.I_ConnectType_All_Index = iArr;
        this.S_ConnectType_Realtime = new String[]{String.valueOf(OtherUtils.getString(R.string.text_U_Direct)) + "," + OtherUtils.getString(R.string.text_I_Direct), String.valueOf(OtherUtils.getString(R.string.text_U_Direct)) + "," + OtherUtils.getString(R.string.text_I_Clamp10A), String.valueOf(OtherUtils.getString(R.string.text_U_Direct)) + "," + OtherUtils.getString(R.string.text_I_Clamp100A), String.valueOf(OtherUtils.getString(R.string.text_U_Direct)) + "," + OtherUtils.getString(R.string.text_I_Clamp500A), String.valueOf(OtherUtils.getString(R.string.text_U_Direct)) + "," + OtherUtils.getString(R.string.text_I_Clamp1000A)};
        int[] iArr2 = new int[8];
        iArr2[1] = 1;
        iArr2[2] = 2;
        iArr2[3] = 3;
        iArr2[4] = 4;
        iArr2[5] = 5;
        iArr2[6] = 6;
        iArr2[7] = 7;
        this.I_ConnectType_Realtime_Index = iArr2;
        this.S_ConnectType_CT = new String[]{String.valueOf(OtherUtils.getString(R.string.text_Pri_Clamp100A)) + "," + OtherUtils.getString(R.string.text_Sec_Clamp10A), String.valueOf(OtherUtils.getString(R.string.text_Pri_Clamp500A)) + "," + OtherUtils.getString(R.string.text_Sec_Clamp10A), String.valueOf(OtherUtils.getString(R.string.text_Pri_Clamp1000A)) + "," + OtherUtils.getString(R.string.text_Sec_Clamp10A)};
        this.I_ConnectType_CT_Index = new int[]{5, 6, 7};
        this.VoltageRange = new ArrayList();
        this.VoltageRange.add(new String[]{"60V", "120V", "240V", "480V"});
        this.VoltageRange.add(new String[]{"0.1A", "10A", "100A"});
        this.VoltageRange.add(new String[]{"50A", "500A"});
        this.VoltageRange.add(new String[]{"100A", "1000A"});
        int[] iArr3 = new int[4];
        iArr3[1] = 18;
        iArr3[2] = 24;
        iArr3[3] = 30;
        this.VoltageRangeIndex = iArr3;
        this.CurrentRange = new ArrayList();
        this.CurrentRange.add(new String[]{"0.02A", "0.05A", "0.1A", "0.2A", "0.5A", "1A", "2A", "5A", "10A", "20A"});
        this.CurrentRange.add(new String[]{"0.1A", "0.2A", "0.5A", "1A", "2A", "5A", "10A"});
        this.CurrentRange.add(new String[]{"0.2A", "1A", "5A", "20A", "100A"});
        this.CurrentRange.add(new String[]{"5A", "20A", "100A", "500A"});
        this.CurrentRange.add(new String[]{"10A", "20A", "50A", "200A", "1000A"});
        this.CurrentRange.add(new String[]{"200A", "500A", "1000A", "2000A"});
        this.CurrentRange.add(new String[]{"20A", "100A", "500A", "2000A"});
        this.CurrentRange.add(new String[]{"200A", "500A", "1500A", "3000A"});
        this.CurrentRangeIndex = new int[]{1, 12, 20, 27, 33, 40, 46, 52};
        this.S_Neutral_Voltage_Range = new String[]{"480V", "60V"};
        this.S_Neutral_Current_Range = new String[]{"100A", "10A"};
    }

    @Override // com.clou.rs350.device.clinterface.IParameterSetting, com.clou.rs350.device.CL3122, com.clou.rs350.device.MeterBaseDevice
    public List<byte[]> setWiringData(int Wiring, int Pulse, int Definition) {
        ((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.Wiring))).Data[0] = Integer.valueOf((Wiring << 4) + Pulse);
        ((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.Definition))).Data[0] = Integer.valueOf(Definition);
        this.m_CLT.DataMap = this.m_Data;
        return this.m_CLT.WriteData(new int[]{this.m_Dictionary.Wiring, this.m_Dictionary.Definition});
    }

    @Override // com.clou.rs350.device.clinterface.IParameterSetting, com.clou.rs350.device.CL3122, com.clou.rs350.device.MeterBaseDevice
    public int getDefinitionValue() {
        return ((Number) ((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.Definition))).Data[0]).intValue();
    }

    @Override // com.clou.rs350.device.clinterface.IParameterSetting, com.clou.rs350.device.CL3122, com.clou.rs350.device.MeterBaseDevice
    public List<byte[]> setRange(int AutoConnectType, int connectType, int AutoRange, double UaRange, double UbRange, double UcRange, double IaRange, double IbRange, double IcRange) {
        ((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.ConnectType))).Data[0] = Integer.valueOf((AutoConnectType << 24) + connectType);
        ((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.RangeAuto))).Data[0] = Integer.valueOf(AutoRange);
        ((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.RangeUa))).Data[0] = Double.valueOf(UaRange);
        ((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.RangeUb))).Data[0] = Double.valueOf(UbRange);
        ((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.RangeUc))).Data[0] = Double.valueOf(UcRange);
        ((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.RangeIa))).Data[0] = Double.valueOf(IaRange);
        ((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.RangeIb))).Data[0] = Double.valueOf(IbRange);
        ((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.RangeIc))).Data[0] = Double.valueOf(IcRange);
        this.m_CLT.DataMap = this.m_Data;
        return this.m_CLT.WriteData(new int[]{this.m_Dictionary.ConnectType, this.m_Dictionary.RangeAuto, this.m_Dictionary.RangeUa, this.m_Dictionary.RangeUb, this.m_Dictionary.RangeUc, this.m_Dictionary.RangeIa, this.m_Dictionary.RangeIb, this.m_Dictionary.RangeIc});
    }

    @Override // com.clou.rs350.device.clinterface.IParameterSetting, com.clou.rs350.device.MeterBaseDevice
    public List<byte[]> setNeutralRange(int AutoRange, int UnRange, int InRange) {
        ((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.RangeNeutral))).Data[0] = Integer.valueOf(((AutoRange & 1) << 3) + UnRange + (InRange << 4) + (((AutoRange & 2) >> 1) << 7));
        this.m_CLT.DataMap = this.m_Data;
        return this.m_CLT.WriteData(new int[]{this.m_Dictionary.RangeNeutral});
    }

    @Override // com.clou.rs350.device.clinterface.IParameterSetting, com.clou.rs350.device.CL3122, com.clou.rs350.device.MeterBaseDevice
    public List<byte[]> getParameter() {
        return this.m_CLT.AskData(new int[]{this.m_Dictionary.Wiring, this.m_Dictionary.ConnectType, this.m_Dictionary.RangeAuto, this.m_Dictionary.ReadRange[0], this.m_Dictionary.ReadRange[1], this.m_Dictionary.ReadRange[2], this.m_Dictionary.ReadRange[3], this.m_Dictionary.ReadRange[4], this.m_Dictionary.ReadRange[5], this.m_Dictionary.OverLoadHint, this.m_Dictionary.Definition});
    }

    @Override // com.clou.rs350.device.clinterface.IParameterSetting, com.clou.rs350.device.MeterBaseDevice
    public boolean[] getOverloadValue() {
        boolean[] tmpResult = new boolean[6];
        int tmpValue = ((Number) ((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.OverLoadHint))).Data[0]).intValue();
        for (int i = 0; i < tmpResult.length; i++) {
            tmpResult[i] = ((tmpValue >> i) & 1) == 1;
        }
        return tmpResult;
    }

    @Override // com.clou.rs350.device.clinterface.IParameterSetting, com.clou.rs350.device.MeterBaseDevice
    public List<byte[]> cleanOverload() {
        ((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.OverLoadHint))).Data[0] = 0;
        return this.m_CLT.WriteData(new int[]{this.m_Dictionary.OverLoadHint});
    }

    @Override // com.clou.rs350.device.clinterface.IParameterSetting, com.clou.rs350.device.CL3122, com.clou.rs350.device.MeterBaseDevice
    public int getWiringValue() {
        return ((Number) ((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.Wiring))).Data[0]).intValue() >> 4;
    }

    @Override // com.clou.rs350.device.clinterface.IParameterSetting, com.clou.rs350.device.CL3122, com.clou.rs350.device.MeterBaseDevice
    public int getPQFlagValue() {
        return ((Number) ((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.Wiring))).Data[0]).intValue() & 15;
    }

    @Override // com.clou.rs350.device.clinterface.IParameterSetting, com.clou.rs350.device.CL3122, com.clou.rs350.device.MeterBaseDevice
    public ExplainedModel[] getConnectTypeValue() {
        int tmpValue = ((Number) ((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.ConnectType))).Data[0]).intValue();
        try {
            return new ExplainedModel[]{new ExplainedModel(0, this.S_ConnectType_Voltage[0]), new ExplainedModel(0, this.S_ConnectType_Voltage[0]), new ExplainedModel(0, this.S_ConnectType_Voltage[0]), new ExplainedModel(Integer.valueOf(tmpValue & 255), this.S_ConnectType_Current[tmpValue & 255]), new ExplainedModel(Integer.valueOf((tmpValue >> 8) & 255), this.S_ConnectType_Current[(tmpValue >> 8) & 255]), new ExplainedModel(Integer.valueOf((tmpValue >> 16) & 255), this.S_ConnectType_Current[(tmpValue >> 16) & 255])};
        } catch (Exception e) {
            return new ExplainedModel[]{new ExplainedModel(0, PdfObject.NOTHING), new ExplainedModel(0, PdfObject.NOTHING), new ExplainedModel(0, PdfObject.NOTHING), new ExplainedModel(Integer.valueOf(tmpValue & 255), PdfObject.NOTHING), new ExplainedModel(Integer.valueOf((tmpValue >> 8) & 255), PdfObject.NOTHING), new ExplainedModel(Integer.valueOf((tmpValue >> 16) & 255), PdfObject.NOTHING)};
        }
    }

    @Override // com.clou.rs350.device.CL3122, com.clou.rs350.device.clinterface.IMeter, com.clou.rs350.device.MeterBaseDevice
    public List<byte[]> getMeasureBasicData() {
        return this.m_CLT.AskData(new int[]{this.m_Dictionary.MeterUaValue, this.m_Dictionary.MeterUbValue, this.m_Dictionary.MeterUcValue, this.m_Dictionary.MeterUabValue, this.m_Dictionary.MeterUbcValue, this.m_Dictionary.MeterUcaValue, this.m_Dictionary.MeterIaValue, this.m_Dictionary.MeterIbValue, this.m_Dictionary.MeterIcValue, this.m_Dictionary.MeterUaAngle, this.m_Dictionary.MeterUbAngle, this.m_Dictionary.MeterUcAngle, this.m_Dictionary.MeterUabAngle, this.m_Dictionary.MeterUbcAngle, this.m_Dictionary.MeterUcaAngle, this.m_Dictionary.MeterUcbAngle, this.m_Dictionary.MeterIaAngle, this.m_Dictionary.MeterIbAngle, this.m_Dictionary.MeterIcAngle, this.m_Dictionary.MeterUIaAngle, this.m_Dictionary.MeterUIbAngle, this.m_Dictionary.MeterUIcAngle, this.m_Dictionary.MeterUabIaAngle, this.m_Dictionary.MeterUbcIbAngle, this.m_Dictionary.MeterUcaIcAngle, this.m_Dictionary.MeterUcbIcAngle, this.m_Dictionary.MeterPaValue, this.m_Dictionary.MeterPbValue, this.m_Dictionary.MeterPcValue, this.m_Dictionary.MeterPSumValue, this.m_Dictionary.MeterQaValue, this.m_Dictionary.MeterQbValue, this.m_Dictionary.MeterQcValue, this.m_Dictionary.MeterQSumValue, this.m_Dictionary.MeterSaValue, this.m_Dictionary.MeterSbValue, this.m_Dictionary.MeterScValue, this.m_Dictionary.MeterSSumValue, this.m_Dictionary.MeterCosaValue, this.m_Dictionary.MeterCosbValue, this.m_Dictionary.MeterCoscValue, this.m_Dictionary.MeterCosSumValue, this.m_Dictionary.MeterUnValue, this.m_Dictionary.MeterInValue, this.m_Dictionary.MeterFrequency});
    }

    @Override // com.clou.rs350.device.clinterface.IMeter, com.clou.rs350.device.MeterBaseDevice
    public ExplainedModel[] getNeutralValue() {
        return new ExplainedModel[]{DataUtils.parse(((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.MeterUnValue))).Data[0], "V"), DataUtils.parse(((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.MeterInValue))).Data[0], "A")};
    }

    @Override // com.clou.rs350.device.clinterface.IMeter, com.clou.rs350.device.MeterBaseDevice
    public List<byte[]> getMeasureSpecialData() {
        return this.m_CLT.AskData(new int[]{this.m_Dictionary.MeterTHDVoltage[0], this.m_Dictionary.MeterTHDVoltage[1], this.m_Dictionary.MeterTHDVoltage[2], this.m_Dictionary.MeterTHDCurrent[0], this.m_Dictionary.MeterTHDCurrent[1], this.m_Dictionary.MeterTHDCurrent[2], this.m_Dictionary.MeterCFUa, this.m_Dictionary.MeterCFUb, this.m_Dictionary.MeterCFUc, this.m_Dictionary.MeterCFIa, this.m_Dictionary.MeterCFIb, this.m_Dictionary.MeterCFIc});
    }

    @Override // com.clou.rs350.device.clinterface.IMeter, com.clou.rs350.device.MeterBaseDevice
    public ExplainedModel[] getVoltageCrestFactorValue() {
        return new ExplainedModel[]{DataUtils.parseRound(((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.MeterCFUa))).Data[0], 3), DataUtils.parseRound(((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.MeterCFUb))).Data[0], 3), DataUtils.parseRound(((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.MeterCFUc))).Data[0], 3)};
    }

    @Override // com.clou.rs350.device.clinterface.IMeter, com.clou.rs350.device.MeterBaseDevice
    public ExplainedModel[] getCurrentCrestFactorValue() {
        return new ExplainedModel[]{DataUtils.parseRound(((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.MeterCFIa))).Data[0], 3), DataUtils.parseRound(((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.MeterCFIb))).Data[0], 3), DataUtils.parseRound(((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.MeterCFIc))).Data[0], 3)};
    }

    @Override // com.clou.rs350.device.clinterface.IMeter, com.clou.rs350.device.MeterBaseDevice
    public ExplainedModel[] getHarmonicValuesValue(int Phase, int Flag) {
        Object[] Data;
        String Util;
        Object[] objArr = new Object[0];
        switch (Flag) {
            case 1:
                Data = ((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.MeterCurrentHarmonicValue[Phase]))).Data;
                Util = "A";
                break;
            case 2:
                Data = ((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.MeterPHarmonicValue[Phase]))).Data;
                Util = "W";
                break;
            case 3:
                Data = ((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.MeterQHarmonicValue[Phase]))).Data;
                Util = "var";
                break;
            case 4:
                Data = ((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.MeterSHarmonicValue[Phase]))).Data;
                Util = "VA";
                break;
            default:
                Data = ((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.MeterVoltageHarmonicValue[Phase]))).Data;
                Util = "V";
                break;
        }
        return DataUtils.parseDoubleArrayRemove0(Data, Util);
    }

    @Override // com.clou.rs350.device.CL3122, com.clou.rs350.device.clinterface.IMeter, com.clou.rs350.device.MeterBaseDevice
    public List<byte[]> getHarmonic(int type) {
        List<byte[]> buffersList = new ArrayList<>();
        int Phase = (type >> 8) - 1;
        if ((type & 1) == 1) {
            buffersList.addAll(this.m_CLT.AskArray(new int[]{this.m_Dictionary.MeterVoltageHarmonicValue[Phase], this.m_Dictionary.MeterVoltageHarmonicContent[Phase], this.m_Dictionary.MeterVoltageHarmonicAngle[Phase]}));
            buffersList.addAll(this.m_CLT.AskData(new int[]{this.m_Dictionary.MeterTHDVoltage[Phase]}));
        }
        if ((type & 2) == 2) {
            buffersList.addAll(this.m_CLT.AskArray(new int[]{this.m_Dictionary.MeterCurrentHarmonicValue[Phase], this.m_Dictionary.MeterCurrentHarmonicContent[Phase], this.m_Dictionary.MeterCurrentHarmonicAngle[Phase]}));
            buffersList.addAll(this.m_CLT.AskData(new int[]{this.m_Dictionary.MeterTHDCurrent[Phase]}));
        }
        return buffersList;
    }

    @Override // com.clou.rs350.device.clinterface.IMeter, com.clou.rs350.device.MeterBaseDevice
    public List<byte[]> getHarmonicValues(int type) {
        List<byte[]> buffersList = new ArrayList<>();
        int Phase = (type >> 8) - 1;
        if ((type & 1) == 1) {
            buffersList.addAll(this.m_CLT.AskArray(new int[]{this.m_Dictionary.MeterVoltageHarmonicValue[Phase]}));
        }
        if ((type & 2) == 2) {
            buffersList.addAll(this.m_CLT.AskArray(new int[]{this.m_Dictionary.MeterCurrentHarmonicValue[Phase]}));
        }
        if ((type & 4) == 4) {
            buffersList.addAll(this.m_CLT.AskArray(new int[]{this.m_Dictionary.MeterPHarmonicValue[Phase]}));
        }
        if ((type & 8) == 8) {
            buffersList.addAll(this.m_CLT.AskArray(new int[]{this.m_Dictionary.MeterQHarmonicValue[Phase]}));
        }
        if ((type & 16) == 16) {
            buffersList.addAll(this.m_CLT.AskArray(new int[]{this.m_Dictionary.MeterSHarmonicValue[Phase]}));
        }
        return buffersList;
    }

    @Override // com.clou.rs350.device.CL3122, com.clou.rs350.device.clinterface.IMeter, com.clou.rs350.device.MeterBaseDevice
    public List<byte[]> getHarmonic() {
        List<byte[]> buffersList = new ArrayList<>();
        for (int i = 1; i <= 3; i++) {
            buffersList.addAll(getHarmonicAngles((i << 8) + 3));
            buffersList.addAll(getHarmonicContents((i << 8) + 3));
            buffersList.addAll(getHarmonicValues((i << 8) + 31));
        }
        buffersList.addAll(getTHD());
        return buffersList;
    }

    @Override // com.clou.rs350.device.clinterface.IMeter, com.clou.rs350.device.MeterBaseDevice
    public List<byte[]> getHarmonicValues(int phase, int type) {
        return getHarmonicValues((phase << 8) + type);
    }

    @Override // com.clou.rs350.device.CL3122, com.clou.rs350.device.clinterface.IMeter, com.clou.rs350.device.MeterBaseDevice
    public List<byte[]> getTHD() {
        return this.m_CLT.AskData(new int[]{this.m_Dictionary.MeterTHDVoltage[0], this.m_Dictionary.MeterTHDVoltage[1], this.m_Dictionary.MeterTHDVoltage[2], this.m_Dictionary.MeterTHDCurrent[0], this.m_Dictionary.MeterTHDCurrent[1], this.m_Dictionary.MeterTHDCurrent[2]});
    }

    @Override // com.clou.rs350.device.CL3122, com.clou.rs350.device.clinterface.IMeter, com.clou.rs350.device.MeterBaseDevice
    public ExplainedModel[] getTHDValue() {
        return new ExplainedModel[]{DataUtils.parseRound(((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.MeterTHDVoltage[0]))).Data[0], 2, "%"), DataUtils.parseRound(((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.MeterTHDVoltage[1]))).Data[0], 2, "%"), DataUtils.parseRound(((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.MeterTHDVoltage[2]))).Data[0], 2, "%"), DataUtils.parseRound(((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.MeterTHDCurrent[0]))).Data[0], 2, "%"), DataUtils.parseRound(((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.MeterTHDCurrent[1]))).Data[0], 2, "%"), DataUtils.parseRound(((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.MeterTHDCurrent[2]))).Data[0], 2, "%")};
    }

    @Override // com.clou.rs350.device.clinterface.IParameterSetting, com.clou.rs350.device.CL3122, com.clou.rs350.device.MeterBaseDevice
    public List<byte[]> setParameter(int Wiring, int Pulse, int Auto, int UaRange, int UbRange, int UcRange, int IaRange, int IbRange, int IcRange) {
        ((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.Wiring))).Data[0] = Integer.valueOf((Wiring << 4) + Pulse);
        ((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.RangeAuto))).Data[0] = Integer.valueOf(Auto);
        ((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.RangeUa))).Data[0] = Integer.valueOf(UaRange);
        ((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.RangeUb))).Data[0] = Integer.valueOf(UbRange);
        ((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.RangeUc))).Data[0] = Integer.valueOf(UcRange);
        ((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.RangeIa))).Data[0] = Integer.valueOf(IaRange);
        ((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.RangeIb))).Data[0] = Integer.valueOf(IbRange);
        ((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.RangeIc))).Data[0] = Integer.valueOf(IcRange);
        this.m_CLT.DataMap = this.m_Data;
        return this.m_CLT.WriteData(new int[]{this.m_Dictionary.Wiring, this.m_Dictionary.ConnectType, this.m_Dictionary.RangeAuto, this.m_Dictionary.RangeUa, this.m_Dictionary.RangeUb, this.m_Dictionary.RangeUc, this.m_Dictionary.RangeIa, this.m_Dictionary.RangeIb, this.m_Dictionary.RangeIc});
    }

    @Override // com.clou.rs350.device.CL3122, com.clou.rs350.device.clinterface.IWiringCheck, com.clou.rs350.device.MeterBaseDevice
    public List<byte[]> getWiringCheck() {
        return this.m_CLT.AskData(new int[]{this.m_Dictionary.WiringCheckU1, this.m_Dictionary.WiringCheckU2, this.m_Dictionary.WiringCheckU3, this.m_Dictionary.WiringCheckI1, this.m_Dictionary.WiringCheckI2, this.m_Dictionary.WiringCheckI3, this.m_Dictionary.WiringCheckRemark, this.m_Dictionary.WiringCheckCorrectionFactor, this.m_Dictionary.WiringCheckCos});
    }

    @Override // com.clou.rs350.device.CL3122, com.clou.rs350.device.clinterface.IMeter, com.clou.rs350.device.MeterBaseDevice
    public List<byte[]> getUIWave() {
        return this.m_CLT.AskArray(new int[]{this.m_Dictionary.MeterUaWave, this.m_Dictionary.MeterUbWave, this.m_Dictionary.MeterUcWave, this.m_Dictionary.MeterIaWave, this.m_Dictionary.MeterIbWave, this.m_Dictionary.MeterIcWave, this.m_Dictionary.MeterWaveUIPP});
    }

    @Override // com.clou.rs350.device.clinterface.IMeter, com.clou.rs350.device.MeterBaseDevice
    public ExplainedModel[] getUIPPValue() {
        return new ExplainedModel[]{DataUtils.parse(((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.MeterWaveUIPP))).Data[0], "V"), DataUtils.parse(((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.MeterWaveUIPP))).Data[1], "V"), DataUtils.parse(((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.MeterWaveUIPP))).Data[2], "A"), DataUtils.parse(((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.MeterWaveUIPP))).Data[3], "A"), DataUtils.parse(((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.MeterWaveUIPP))).Data[4], "V"), DataUtils.parse(((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.MeterWaveUIPP))).Data[5], "V"), DataUtils.parse(((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.MeterWaveUIPP))).Data[6], "A"), DataUtils.parse(((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.MeterWaveUIPP))).Data[7], "A"), DataUtils.parse(((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.MeterWaveUIPP))).Data[8], "V"), DataUtils.parse(((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.MeterWaveUIPP))).Data[9], "V"), DataUtils.parse(((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.MeterWaveUIPP))).Data[10], "A"), DataUtils.parse(((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.MeterWaveUIPP))).Data[11], "A")};
    }

    @Override // com.clou.rs350.device.clinterface.IMeter, com.clou.rs350.device.MeterBaseDevice
    public List<byte[]> getPQWave() {
        return this.m_CLT.AskArray(new int[]{this.m_Dictionary.MeterPaWave, this.m_Dictionary.MeterPbWave, this.m_Dictionary.MeterPcWave, this.m_Dictionary.MeterQaWave, this.m_Dictionary.MeterQbWave, this.m_Dictionary.MeterQcWave, this.m_Dictionary.MeterWavePQPP});
    }

    @Override // com.clou.rs350.device.clinterface.IMeter, com.clou.rs350.device.MeterBaseDevice
    public Double[] getPQWaveValue(int Phase) {
        Object[] Data = new Object[0];
        switch (Phase) {
            case 0:
                Data = ((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.MeterPaWave))).Data;
                break;
            case 1:
                Data = ((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.MeterPbWave))).Data;
                break;
            case 2:
                Data = ((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.MeterPcWave))).Data;
                break;
            case 3:
                Data = ((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.MeterQaWave))).Data;
                break;
            case 4:
                Data = ((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.MeterQbWave))).Data;
                break;
            case 5:
                Data = ((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.MeterQcWave))).Data;
                break;
        }
        return DataUtils.parseDoubleArray(Data);
    }

    @Override // com.clou.rs350.device.clinterface.IMeter, com.clou.rs350.device.MeterBaseDevice
    public ExplainedModel[] getPQPPValue() {
        return new ExplainedModel[]{DataUtils.parse(((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.MeterWavePQPP))).Data[0], "W"), DataUtils.parse(((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.MeterWavePQPP))).Data[1], "W"), DataUtils.parse(((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.MeterWavePQPP))).Data[2], "var"), DataUtils.parse(((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.MeterWavePQPP))).Data[3], "var"), DataUtils.parse(((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.MeterWavePQPP))).Data[4], "W"), DataUtils.parse(((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.MeterWavePQPP))).Data[5], "W"), DataUtils.parse(((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.MeterWavePQPP))).Data[6], "var"), DataUtils.parse(((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.MeterWavePQPP))).Data[7], "var"), DataUtils.parse(((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.MeterWavePQPP))).Data[8], "W"), DataUtils.parse(((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.MeterWavePQPP))).Data[9], "W"), DataUtils.parse(((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.MeterWavePQPP))).Data[10], "var"), DataUtils.parse(((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.MeterWavePQPP))).Data[11], "var")};
    }

    @Override // com.clou.rs350.device.clinterface.IParameterSetting, com.clou.rs350.device.MeterBaseDevice
    public List<byte[]> setPTCTRatio(double PT, double CT, int RatioApply) {
        if (0.0d != PT) {
            ((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.PTRatio))).Data[0] = Double.valueOf(PT);
        }
        if (0.0d != CT) {
            ((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.CTRatio))).Data[0] = Double.valueOf(CT);
        }
        ((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.RatioApply))).Data[0] = Integer.valueOf(RatioApply);
        return this.m_CLT.WriteData(new int[]{this.m_Dictionary.PTRatio, this.m_Dictionary.CTRatio, this.m_Dictionary.RatioApply});
    }

    @Override // com.clou.rs350.device.clinterface.ITest, com.clou.rs350.device.CL3122, com.clou.rs350.device.MeterBaseDevice
    public List<byte[]> setErrorParameters(int[] errorCount, int[] measurementType, float[] constants, int[] pulses) {
        ((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.ErrorChannel))).Data[0] = Integer.valueOf(measurementType[0] + (measurementType[1] << 2) + (measurementType[2] << 4));
        ((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.ErrorCalcCount[0]))).Data[0] = Integer.valueOf(errorCount[0]);
        ((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.ErrorCalcCount[1]))).Data[0] = Integer.valueOf(errorCount[1]);
        ((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.ErrorCalcCount[2]))).Data[0] = Integer.valueOf(errorCount[2]);
        ((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.ErrorMeterConstants[0]))).Data[0] = Float.valueOf(constants[0]);
        ((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.ErrorMeterConstants[1]))).Data[0] = Float.valueOf(constants[1]);
        ((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.ErrorMeterConstants[2]))).Data[0] = Float.valueOf(constants[2]);
        ((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.ErrorPulseCount[0]))).Data[0] = Integer.valueOf(pulses[0]);
        ((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.ErrorPulseCount[1]))).Data[0] = Integer.valueOf(pulses[1]);
        ((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.ErrorPulseCount[2]))).Data[0] = Integer.valueOf(pulses[2]);
        ((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.ErrorErrorCount[0]))).Data[0] = 0;
        ((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.ErrorErrorCount[1]))).Data[0] = 0;
        ((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.ErrorErrorCount[2]))).Data[0] = 0;
        ((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.ErrorMeterPulseNum[0]))).Data[0] = -1;
        ((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.ErrorMeterPulseNum[1]))).Data[0] = -1;
        ((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.ErrorMeterPulseNum[2]))).Data[0] = -1;
        this.m_CLT.DataMap = this.m_Data;
        return this.m_CLT.WriteData(new int[]{this.m_Dictionary.ErrorChannel, this.m_Dictionary.ErrorCalcCount[0], this.m_Dictionary.ErrorCalcCount[1], this.m_Dictionary.ErrorCalcCount[2], this.m_Dictionary.ErrorMeterConstants[0], this.m_Dictionary.ErrorMeterConstants[1], this.m_Dictionary.ErrorMeterConstants[2], this.m_Dictionary.ErrorPulseCount[0], this.m_Dictionary.ErrorPulseCount[1], this.m_Dictionary.ErrorPulseCount[2], this.m_Dictionary.ErrorErrorCount[0], this.m_Dictionary.ErrorErrorCount[1], this.m_Dictionary.ErrorErrorCount[2], this.m_Dictionary.ErrorMeterPulseNum[0], this.m_Dictionary.ErrorMeterPulseNum[1], this.m_Dictionary.ErrorMeterPulseNum[2]});
    }

    @Override // com.clou.rs350.device.clinterface.ITest, com.clou.rs350.device.CL3122, com.clou.rs350.device.MeterBaseDevice
    public List<byte[]> getErrors() {
        return this.m_CLT.AskData(new int[]{this.m_Dictionary.ErrorErrorCount[0], this.m_Dictionary.ErrorErrorCount[1], this.m_Dictionary.ErrorErrorCount[2], this.m_Dictionary.ErrorMeterError1[0], this.m_Dictionary.ErrorMeterError1[1], this.m_Dictionary.ErrorMeterError1[2], this.m_Dictionary.ErrorMeterError2[0], this.m_Dictionary.ErrorMeterError2[1], this.m_Dictionary.ErrorMeterError2[2], this.m_Dictionary.ErrorMeterError3[0], this.m_Dictionary.ErrorMeterError3[1], this.m_Dictionary.ErrorMeterError3[2], this.m_Dictionary.ErrorMeterError4[0], this.m_Dictionary.ErrorMeterError4[1], this.m_Dictionary.ErrorMeterError4[2], this.m_Dictionary.ErrorMeterError5[0], this.m_Dictionary.ErrorMeterError5[1], this.m_Dictionary.ErrorMeterError5[2], this.m_Dictionary.ErrorMeterError6[0], this.m_Dictionary.ErrorMeterError6[1], this.m_Dictionary.ErrorMeterError6[2], this.m_Dictionary.ErrorMeterError7[0], this.m_Dictionary.ErrorMeterError7[1], this.m_Dictionary.ErrorMeterError5[2], this.m_Dictionary.ErrorMeterError8[0], this.m_Dictionary.ErrorMeterError8[1], this.m_Dictionary.ErrorMeterError8[2], this.m_Dictionary.ErrorMeterError9[0], this.m_Dictionary.ErrorMeterError9[1], this.m_Dictionary.ErrorMeterError9[2], this.m_Dictionary.ErrorMeterError10[0], this.m_Dictionary.ErrorMeterError10[1], this.m_Dictionary.ErrorMeterError10[2], this.m_Dictionary.ErrorMeterErrorAvg[0], this.m_Dictionary.ErrorMeterErrorAvg[1], this.m_Dictionary.ErrorMeterErrorAvg[2], this.m_Dictionary.ErrorMeterErrorStd[0], this.m_Dictionary.ErrorMeterErrorStd[1], this.m_Dictionary.ErrorMeterErrorStd[2], this.m_Dictionary.ErrorMeterPulseNum[0], this.m_Dictionary.ErrorMeterPulseNum[1], this.m_Dictionary.ErrorMeterPulseNum[2]});
    }

    @Override // com.clou.rs350.device.clinterface.ITest, com.clou.rs350.device.CL3122, com.clou.rs350.device.MeterBaseDevice
    public ExplainedModel[] getErrorsValues(int index) {
        return new ExplainedModel[]{DataUtils.parse(((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.ErrorMeterError1[index]))).Data[0], "%"), DataUtils.parse(((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.ErrorMeterError2[index]))).Data[0], "%"), DataUtils.parse(((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.ErrorMeterError3[index]))).Data[0], "%"), DataUtils.parse(((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.ErrorMeterError4[index]))).Data[0], "%"), DataUtils.parse(((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.ErrorMeterError5[index]))).Data[0], "%"), DataUtils.parse(((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.ErrorMeterError6[index]))).Data[0], "%"), DataUtils.parse(((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.ErrorMeterError7[index]))).Data[0], "%"), DataUtils.parse(((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.ErrorMeterError8[index]))).Data[0], "%"), DataUtils.parse(((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.ErrorMeterError9[index]))).Data[0], "%"), DataUtils.parse(((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.ErrorMeterError10[index]))).Data[0], "%"), DataUtils.parse(((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.ErrorMeterErrorAvg[index]))).Data[0], "%"), DataUtils.parse(((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.ErrorMeterErrorStd[index]))).Data[0], "%"), DataUtils.parseint(((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.ErrorMeterPulseNum[index]))).Data[0]), DataUtils.parseint(((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.ErrorErrorCount[index]))).Data[0])};
    }

    @Override // com.clou.rs350.device.clinterface.ITest, com.clou.rs350.device.CL3122, com.clou.rs350.device.MeterBaseDevice
    public List<byte[]> getEnergy() {
        return this.m_CLT.AskData(new int[]{this.m_Dictionary.EnergyValue, this.m_Dictionary.EnergyActiveValue, this.m_Dictionary.EnergyReactiveValue, this.m_Dictionary.EnergyApparentValue});
    }

    @Override // com.clou.rs350.device.clinterface.ITest, com.clou.rs350.device.CL3122, com.clou.rs350.device.MeterBaseDevice
    public ExplainedModel[] getEnergyValues() {
        return new ExplainedModel[]{DataUtils.parseRound(((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.EnergyValue))).Data[0], 4), DataUtils.parseRound(((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.EnergyActiveValue))).Data[0], 4), DataUtils.parseRound(((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.EnergyReactiveValue))).Data[0], 4), DataUtils.parseRound(((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.EnergyApparentValue))).Data[0], 4)};
    }

    @Override // com.clou.rs350.device.clinterface.ITest, com.clou.rs350.device.MeterBaseDevice
    public List<byte[]> getDemand() {
        return this.m_CLT.AskData(new int[]{this.m_Dictionary.DemandValue});
    }

    @Override // com.clou.rs350.device.clinterface.ITest, com.clou.rs350.device.MeterBaseDevice
    public ExplainedModel[] getDemandValues() {
        return new ExplainedModel[]{DataUtils.parse(((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.DemandValue))).Data[0], "W")};
    }

    @Override // com.clou.rs350.device.clinterface.ITest, com.clou.rs350.device.MeterBaseDevice
    public List<byte[]> setTestMode(int flag) {
        ((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.ErrorTestMode))).Data[0] = Integer.valueOf(flag);
        return this.m_CLT.WriteData(new int[]{this.m_Dictionary.ErrorTestMode});
    }

    @Override // com.clou.rs350.device.clinterface.ITest, com.clou.rs350.device.MeterBaseDevice
    public List<byte[]> setCheckKFlag(int flag) {
        ((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.ErrorCheckKFlag))).Data[0] = Integer.valueOf(flag);
        return this.m_CLT.WriteData(new int[]{this.m_Dictionary.ErrorCheckKFlag});
    }

    @Override // com.clou.rs350.device.clinterface.ITest, com.clou.rs350.device.MeterBaseDevice
    public int getCheckKFlagValue() {
        return ((Number) ((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.ErrorCheckKFlag))).Data[0]).intValue();
    }

    @Override // com.clou.rs350.device.clinterface.ITest, com.clou.rs350.device.MeterBaseDevice
    public List<byte[]> getCheckKFlag() {
        return this.m_CLT.AskData(new int[]{this.m_Dictionary.ErrorCheckKFlag});
    }

    @Override // com.clou.rs350.device.CL3122, com.clou.rs350.device.clinterface.IPTCTMeasurement, com.clou.rs350.device.MeterBaseDevice
    public List<byte[]> getCTValue() {
        return this.m_CLT.AskData(new int[]{this.m_Dictionary.CTCurrentValuePrimary, this.m_Dictionary.CTCurrentValueSecondary, this.m_Dictionary.CTVoltageValueSecondary, this.m_Dictionary.CTCurrentAnglePrimary, this.m_Dictionary.CTCurrentAngleSecondary, this.m_Dictionary.CTCurrentValueRatio, this.m_Dictionary.CTCurrentValuePhase, this.m_Dictionary.CTCurrentValueError, this.m_Dictionary.CTResistance, this.m_Dictionary.CTReactance, this.m_Dictionary.CTImpedance, this.m_Dictionary.CTPowerFactor, this.m_Dictionary.CTBurden});
    }

    @Override // com.clou.rs350.device.CL3122, com.clou.rs350.device.clinterface.IPTCTMeasurement, com.clou.rs350.device.MeterBaseDevice
    public ExplainedModel[] getCTBurdenValue() {
        return new ExplainedModel[]{DataUtils.parse(((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.CTVoltageValueSecondary))).Data[0], "V"), DataUtils.parse(((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.CTCurrentValueSecondary))).Data[0], "A"), DataUtils.parse(((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.CTBurden))).Data[0], "VA")};
    }

    @Override // com.clou.rs350.device.CL3122, com.clou.rs350.device.clinterface.IPTCTMeasurement, com.clou.rs350.device.MeterBaseDevice
    public List<byte[]> getPTBurden() {
        return this.m_CLT.AskData(new int[]{this.m_Dictionary.PTVoltage[0], this.m_Dictionary.PTVoltage[1], this.m_Dictionary.PTVoltage[2], this.m_Dictionary.PTCurrent[0], this.m_Dictionary.PTCurrent[1], this.m_Dictionary.PTCurrent[2], this.m_Dictionary.PTAngle[0], this.m_Dictionary.PTAngle[1], this.m_Dictionary.PTAngle[2], this.m_Dictionary.PTBurdenW[0], this.m_Dictionary.PTBurdenW[1], this.m_Dictionary.PTBurdenW[2], this.m_Dictionary.PTBurdenVA[0], this.m_Dictionary.PTBurdenVA[1], this.m_Dictionary.PTBurdenVA[2]});
    }

    @Override // com.clou.rs350.device.CL3122, com.clou.rs350.device.clinterface.IPTCTMeasurement, com.clou.rs350.device.MeterBaseDevice
    public ExplainedModel[] getPTBurdenValue() {
        return new ExplainedModel[]{DataUtils.parse(((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.PTVoltage[0]))).Data[0], "V"), DataUtils.parse(((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.PTVoltage[1]))).Data[0], "V"), DataUtils.parse(((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.PTVoltage[2]))).Data[0], "V"), DataUtils.parse(((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.PTCurrent[0]))).Data[0], "A"), DataUtils.parse(((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.PTCurrent[1]))).Data[0], "A"), DataUtils.parse(((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.PTCurrent[2]))).Data[0], "A"), DataUtils.parse(((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.PTAngle[0]))).Data[0], "°"), DataUtils.parse(((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.PTAngle[1]))).Data[0], "°"), DataUtils.parse(((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.PTAngle[2]))).Data[0], "°"), DataUtils.parse(((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.PTBurdenW[0]))).Data[0], "W"), DataUtils.parse(((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.PTBurdenW[1]))).Data[0], "W"), DataUtils.parse(((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.PTBurdenW[2]))).Data[0], "W"), DataUtils.parse(((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.PTBurdenVA[0]))).Data[0], "VA"), DataUtils.parse(((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.PTBurdenVA[1]))).Data[0], "VA"), DataUtils.parse(((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.PTBurdenVA[2]))).Data[0], "VA")};
    }

    @Override // com.clou.rs350.device.CL3122, com.clou.rs350.device.clinterface.IPTCTMeasurement, com.clou.rs350.device.MeterBaseDevice
    public List<byte[]> getPTError() {
        return this.m_CLT.AskData(new int[]{this.m_Dictionary.PTIsConnect, this.m_Dictionary.DeviceBattery, this.m_Dictionary.PTsetAddress, this.m_Dictionary.PTLocation1Voltage[0], this.m_Dictionary.PTLocation1Voltage[1], this.m_Dictionary.PTLocation1Voltage[2], this.m_Dictionary.PTLocation1Angle[0], this.m_Dictionary.PTLocation1Angle[1], this.m_Dictionary.PTLocation1Angle[2], this.m_Dictionary.PTLocation2Voltage[0], this.m_Dictionary.PTLocation2Voltage[1], this.m_Dictionary.PTLocation2Voltage[2], this.m_Dictionary.PTLocation2Angle[0], this.m_Dictionary.PTLocation2Angle[1], this.m_Dictionary.PTLocation2Angle[2], this.m_Dictionary.PTRatioError[0], this.m_Dictionary.PTRatioError[1], this.m_Dictionary.PTRatioError[2], this.m_Dictionary.PTAngleError[0], this.m_Dictionary.PTAngleError[1], this.m_Dictionary.PTAngleError[2], this.m_Dictionary.PTVoltageDropError[0], this.m_Dictionary.PTVoltageDropError[1], this.m_Dictionary.PTVoltageDropError[2], this.m_Dictionary.PTMeasureTime});
    }

    @Override // com.clou.rs350.device.CL3122, com.clou.rs350.device.clinterface.IPTCTMeasurement, com.clou.rs350.device.MeterBaseDevice
    public ExplainedModel[] getPTErrorValue() {
        int time = ((Number) ((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.PTMeasureTime))).Data[0]).intValue();
        return new ExplainedModel[]{DataUtils.parse(((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.PTLocation1Voltage[0]))).Data[0], "V"), DataUtils.parse(((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.PTLocation1Voltage[1]))).Data[0], "V"), DataUtils.parse(((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.PTLocation1Voltage[2]))).Data[0], "V"), DataUtils.parse(((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.PTLocation1Angle[0]))).Data[0], "°"), DataUtils.parse(((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.PTLocation1Angle[1]))).Data[0], "°"), DataUtils.parse(((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.PTLocation1Angle[2]))).Data[0], "°"), DataUtils.parse(((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.PTLocation2Voltage[0]))).Data[0], "V"), DataUtils.parse(((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.PTLocation2Voltage[1]))).Data[0], "V"), DataUtils.parse(((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.PTLocation2Voltage[2]))).Data[0], "V"), DataUtils.parse(((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.PTLocation2Angle[0]))).Data[0], "°"), DataUtils.parse(((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.PTLocation2Angle[1]))).Data[0], "°"), DataUtils.parse(((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.PTLocation2Angle[2]))).Data[0], "°"), DataUtils.parse(((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.PTVoltageDropError[0]))).Data[0], "°"), DataUtils.parse(((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.PTVoltageDropError[1]))).Data[0], "°"), DataUtils.parse(((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.PTVoltageDropError[2]))).Data[0], "°"), DataUtils.parseRound(((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.PTAngleError[0]))).Data[0], 3), DataUtils.parseRound(((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.PTAngleError[1]))).Data[0], 3), DataUtils.parseRound(((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.PTAngleError[2]))).Data[0], 3), DataUtils.parse(((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.PTRatioError[0]))).Data[0], "°"), DataUtils.parse(((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.PTRatioError[1]))).Data[0], "°"), DataUtils.parse(((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.PTRatioError[2]))).Data[0], "°"), new ExplainedModel(Integer.valueOf(time), String.valueOf(String.format("%02d", Integer.valueOf((time / 3600) % 24))) + ":" + String.format("%02d", Integer.valueOf((time / 60) % 60)) + ":" + String.format("%02d", Integer.valueOf(time % 60)))};
    }

    @Override // com.clou.rs350.device.MeterBaseDevice, com.clou.rs350.device.clinterface.IIntegration
    public List<byte[]> setIntegrationParameter(int Interval, int Period) {
        ((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.IntegrationTime))).Data[0] = Integer.valueOf(Interval);
        ((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.IntegrationPeriod))).Data[0] = Integer.valueOf(Period);
        return this.m_CLT.WriteData(new int[]{this.m_Dictionary.IntegrationTime, this.m_Dictionary.IntegrationPeriod});
    }

    @Override // com.clou.rs350.device.MeterBaseDevice, com.clou.rs350.device.clinterface.IIntegration
    public List<byte[]> getIntegrationData() {
        return this.m_CLT.AskData(new int[]{this.m_Dictionary.IntegrationUaValue, this.m_Dictionary.IntegrationUbValue, this.m_Dictionary.IntegrationUcValue, this.m_Dictionary.IntegrationUabValue, this.m_Dictionary.IntegrationUbcValue, this.m_Dictionary.IntegrationUcaValue, this.m_Dictionary.IntegrationIaValue, this.m_Dictionary.IntegrationIbValue, this.m_Dictionary.IntegrationIcValue, this.m_Dictionary.IntegrationUaAngle, this.m_Dictionary.IntegrationUbAngle, this.m_Dictionary.IntegrationUcAngle, this.m_Dictionary.IntegrationUabAngle, this.m_Dictionary.IntegrationUbcAngle, this.m_Dictionary.IntegrationUcaAngle, this.m_Dictionary.IntegrationUcbAngle, this.m_Dictionary.IntegrationIaAngle, this.m_Dictionary.IntegrationIbAngle, this.m_Dictionary.IntegrationIcAngle, this.m_Dictionary.IntegrationTHDVoltage[0], this.m_Dictionary.IntegrationTHDVoltage[1], this.m_Dictionary.IntegrationTHDVoltage[2], this.m_Dictionary.IntegrationTHDCurrent[0], this.m_Dictionary.IntegrationTHDCurrent[1], this.m_Dictionary.IntegrationTHDCurrent[2], this.m_Dictionary.IntegrationUIaAngle, this.m_Dictionary.IntegrationUIbAngle, this.m_Dictionary.IntegrationUIcAngle, this.m_Dictionary.IntegrationUabIaAngle, this.m_Dictionary.IntegrationUbcIbAngle, this.m_Dictionary.IntegrationUcaIcAngle, this.m_Dictionary.IntegrationUcbIcAngle, this.m_Dictionary.IntegrationPaValue, this.m_Dictionary.IntegrationPbValue, this.m_Dictionary.IntegrationPcValue, this.m_Dictionary.IntegrationPSumValue, this.m_Dictionary.IntegrationQaValue, this.m_Dictionary.IntegrationQbValue, this.m_Dictionary.IntegrationQcValue, this.m_Dictionary.IntegrationQSumValue, this.m_Dictionary.IntegrationSaValue, this.m_Dictionary.IntegrationSbValue, this.m_Dictionary.IntegrationScValue, this.m_Dictionary.IntegrationSSumValue, this.m_Dictionary.IntegrationCosaValue, this.m_Dictionary.IntegrationCosbValue, this.m_Dictionary.IntegrationCoscValue, this.m_Dictionary.IntegrationCosSumValue, this.m_Dictionary.IntegrationFrequency, this.m_Dictionary.IntegrationUnValue, this.m_Dictionary.IntegrationInValue, this.m_Dictionary.IntegrationFlag});
    }

    @Override // com.clou.rs350.device.MeterBaseDevice, com.clou.rs350.device.clinterface.IIntegration
    public int getIntegrationFlagValue() {
        int flag = ((Number) ((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.IntegrationFlag))).Data[0]).intValue();
        ((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.IntegrationFlag))).Data[0] = 0;
        return flag;
    }

    @Override // com.clou.rs350.device.MeterBaseDevice, com.clou.rs350.device.clinterface.IIntegration
    public List<byte[]> cleanIntegrationFlag() {
        ((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.IntegrationFlag))).Data[0] = 0;
        return this.m_CLT.WriteData(new int[]{this.m_Dictionary.IntegrationFlag});
    }

    @Override // com.clou.rs350.device.MeterBaseDevice, com.clou.rs350.device.clinterface.IIntegration
    public ExplainedModel[] getLNVoltageIntegrationValue() {
        return new ExplainedModel[]{DataUtils.parse(((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.IntegrationUaValue))).Data[0], "V"), DataUtils.parse(((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.IntegrationUbValue))).Data[0], "V"), DataUtils.parse(((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.IntegrationUcValue))).Data[0], "V")};
    }

    @Override // com.clou.rs350.device.MeterBaseDevice, com.clou.rs350.device.clinterface.IIntegration
    public ExplainedModel[] getLLVoltageIntegrationValue() {
        return new ExplainedModel[]{DataUtils.parse(((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.IntegrationUabValue))).Data[0], "V"), DataUtils.parse(((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.IntegrationUbcValue))).Data[0], "V"), DataUtils.parse(((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.IntegrationUcaValue))).Data[0], "V")};
    }

    @Override // com.clou.rs350.device.MeterBaseDevice, com.clou.rs350.device.clinterface.IIntegration
    public ExplainedModel[] getCurrentIntegrationValue() {
        return new ExplainedModel[]{DataUtils.parse(((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.IntegrationIaValue))).Data[0], "A"), DataUtils.parse(((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.IntegrationIbValue))).Data[0], "A"), DataUtils.parse(((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.IntegrationIcValue))).Data[0], "A")};
    }

    @Override // com.clou.rs350.device.MeterBaseDevice, com.clou.rs350.device.clinterface.IIntegration
    public ExplainedModel[] getPIntegrationValue() {
        return new ExplainedModel[]{DataUtils.parse(((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.IntegrationPaValue))).Data[0], "W"), DataUtils.parse(((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.IntegrationPbValue))).Data[0], "W"), DataUtils.parse(((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.IntegrationPcValue))).Data[0], "W"), DataUtils.parse(((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.IntegrationPSumValue))).Data[0], "W")};
    }

    @Override // com.clou.rs350.device.MeterBaseDevice, com.clou.rs350.device.clinterface.IIntegration
    public ExplainedModel[] getQIntegrationValue() {
        return new ExplainedModel[]{DataUtils.parse(((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.IntegrationQaValue))).Data[0], "var"), DataUtils.parse(((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.IntegrationQbValue))).Data[0], "var"), DataUtils.parse(((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.IntegrationQcValue))).Data[0], "var"), DataUtils.parse(((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.IntegrationQSumValue))).Data[0], "var")};
    }

    @Override // com.clou.rs350.device.MeterBaseDevice, com.clou.rs350.device.clinterface.IIntegration
    public ExplainedModel[] getSIntegrationValue() {
        return new ExplainedModel[]{DataUtils.parse(((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.IntegrationSaValue))).Data[0], "VA"), DataUtils.parse(((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.IntegrationSbValue))).Data[0], "VA"), DataUtils.parse(((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.IntegrationScValue))).Data[0], "VA"), DataUtils.parse(((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.IntegrationSSumValue))).Data[0], "VA")};
    }

    @Override // com.clou.rs350.device.MeterBaseDevice, com.clou.rs350.device.clinterface.IIntegration
    public ExplainedModel[] getCosIntegrationValue() {
        return new ExplainedModel[]{DataUtils.parseRound(((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.IntegrationCosaValue))).Data[0], 4), DataUtils.parseRound(((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.IntegrationCosbValue))).Data[0], 4), DataUtils.parseRound(((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.IntegrationCoscValue))).Data[0], 4), DataUtils.parseRound(((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.IntegrationCosSumValue))).Data[0], 4)};
    }

    @Override // com.clou.rs350.device.MeterBaseDevice, com.clou.rs350.device.clinterface.IIntegration
    public ExplainedModel[] getLNVoltageAngleIntegrationValue() {
        return new ExplainedModel[]{DataUtils.parse(((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.IntegrationUaAngle))).Data[0], "°"), DataUtils.parse(((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.IntegrationUbAngle))).Data[0], "°"), DataUtils.parse(((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.IntegrationUcAngle))).Data[0], "°")};
    }

    @Override // com.clou.rs350.device.MeterBaseDevice, com.clou.rs350.device.clinterface.IIntegration
    public ExplainedModel[] getLLVoltageAngleIntegrationValue() {
        return new ExplainedModel[]{DataUtils.parse(((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.IntegrationUabAngle))).Data[0], "°"), DataUtils.parse(((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.IntegrationUbcAngle))).Data[0], "°"), DataUtils.parse(((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.IntegrationUcaAngle))).Data[0], "°"), DataUtils.parse(((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.IntegrationUcbAngle))).Data[0], "°")};
    }

    @Override // com.clou.rs350.device.MeterBaseDevice, com.clou.rs350.device.clinterface.IIntegration
    public ExplainedModel[] getCurrentAngleIntegrationValue() {
        return new ExplainedModel[]{DataUtils.parse(((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.IntegrationIaAngle))).Data[0], "°"), DataUtils.parse(((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.IntegrationIbAngle))).Data[0], "°"), DataUtils.parse(((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.IntegrationIcAngle))).Data[0], "°")};
    }

    @Override // com.clou.rs350.device.MeterBaseDevice, com.clou.rs350.device.clinterface.IIntegration
    public ExplainedModel[] getLNVoltageCurrentAngleIntegrationValue() {
        return new ExplainedModel[]{DataUtils.parse(((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.IntegrationUIaAngle))).Data[0], "°"), DataUtils.parse(((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.IntegrationUIbAngle))).Data[0], "°"), DataUtils.parse(((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.IntegrationUIcAngle))).Data[0], "°")};
    }

    @Override // com.clou.rs350.device.MeterBaseDevice, com.clou.rs350.device.clinterface.IIntegration
    public ExplainedModel[] getLLVoltageCurrentAngleIntegrationValue() {
        return new ExplainedModel[]{DataUtils.parse(((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.IntegrationUabIaAngle))).Data[0], "°"), DataUtils.parse(((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.IntegrationUbcIbAngle))).Data[0], "°"), DataUtils.parse(((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.IntegrationUcaIcAngle))).Data[0], "°"), DataUtils.parse(((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.IntegrationUcbIcAngle))).Data[0], "°")};
    }

    @Override // com.clou.rs350.device.MeterBaseDevice, com.clou.rs350.device.clinterface.IIntegration
    public ExplainedModel[] getFrequencyIntegrationValue() {
        return new ExplainedModel[]{DataUtils.parse(((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.IntegrationFrequency))).Data[0], "Hz")};
    }

    @Override // com.clou.rs350.device.MeterBaseDevice, com.clou.rs350.device.clinterface.IIntegration
    public ExplainedModel[] getTHDIntegrationValue() {
        return new ExplainedModel[]{DataUtils.parseRound(((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.IntegrationTHDVoltage[0]))).Data[0], 2), DataUtils.parseRound(((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.IntegrationTHDVoltage[1]))).Data[0], 2), DataUtils.parseRound(((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.IntegrationTHDVoltage[2]))).Data[0], 2), DataUtils.parseRound(((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.IntegrationTHDCurrent[0]))).Data[0], 2), DataUtils.parseRound(((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.IntegrationTHDCurrent[1]))).Data[0], 2), DataUtils.parseRound(((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.IntegrationTHDCurrent[2]))).Data[0], 2)};
    }

    @Override // com.clou.rs350.device.MeterBaseDevice, com.clou.rs350.device.clinterface.IIntegration
    public ExplainedModel[] getNeutralIntegrationValue() {
        return new ExplainedModel[]{DataUtils.parse(((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.IntegrationUnValue))).Data[0], "V"), DataUtils.parse(((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.IntegrationInValue))).Data[0], "A")};
    }

    @Override // com.clou.rs350.device.clinterface.IParameterSetting, com.clou.rs350.device.MeterBaseDevice
    public List<byte[]> setStandardInfo(StandardInfo info) {
        List<byte[]> buffers = new ArrayList<>();
        buffers.addAll(setFunc_Mstate(CLTConstant.Func_Mstate_91));
        String tmpString = info.CalibrateDate != null ? String.valueOf(PdfObject.NOTHING) + (info.CalibrateDate.getTime() / 3600000) + "|" : String.valueOf(PdfObject.NOTHING) + "0|";
        String tmpString2 = info.Calib_Valid_Date != null ? String.valueOf(tmpString) + (info.Calib_Valid_Date.getTime() / 3600000) + "|" : String.valueOf(tmpString) + "0|";
        byte[] tmpByte = (String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf(tmpString2) + info.CertificateID + "|") + info.Calibrated_By + "|") + info.Function + "|") + info.MainBoardVersion + "|") + info.MeasureBoardVersion + "|") + info.NeutralBoardVersion + "|").getBytes();
        int length = tmpByte.length < ((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.StandardInfo))).Data.length ? tmpByte.length : ((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.StandardInfo))).Data.length;
        for (int i = 0; i < length; i++) {
            ((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.StandardInfo))).Data[i] = Byte.valueOf(tmpByte[i]);
        }
        buffers.addAll(this.m_CLT.WriteArray(new int[]{this.m_Dictionary.StandardInfo}));
        String tmpString3 = info.StandardSerialNo;
        for (int i2 = tmpString3.length(); i2 < 18; i2++) {
            tmpString3 = " " + tmpString3;
        }
        byte[] tmpByte2 = tmpString3.getBytes();
        if (tmpByte2.length < ((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.SerialNo))).Data.length) {
            int length2 = tmpByte2.length;
        } else {
            int length3 = ((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.SerialNo))).Data.length;
        }
        for (int i3 = 0; i3 < tmpByte2.length; i3++) {
            ((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.SerialNo))).Data[i3] = Byte.valueOf(tmpByte2[i3]);
        }
        buffers.addAll(this.m_CLT.WriteArray(new int[]{this.m_Dictionary.SerialNo}));
        ((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.StandardInfoFlag))).Data[0] = 1;
        buffers.addAll(this.m_CLT.WriteData(new int[]{this.m_Dictionary.StandardInfoFlag}));
        return buffers;
    }

    @Override // com.clou.rs350.device.clinterface.IParameterSetting, com.clou.rs350.device.MeterBaseDevice
    public List<byte[]> getStandardInfo() {
        return this.m_CLT.AskArray(new int[]{this.m_Dictionary.StandardInfo});
    }

    @Override // com.clou.rs350.device.clinterface.IParameterSetting, com.clou.rs350.device.MeterBaseDevice
    public StandardInfo getStandardInfoValue(StandardInfo info) {
        DataModel serioModel = (DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.StandardInfo));
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < serioModel.Data.length; i++) {
            sb.append((char) ((Byte) serioModel.Data[i]).byteValue());
        }
        String[] tmpStrings = sb.toString().split("\\|");
        try {
            info.CalibrateDate = new Date(OtherUtils.parseLong(tmpStrings[0]) * 3600000);
            info.Calib_Valid_Date = new Date(OtherUtils.parseLong(tmpStrings[1]) * 3600000);
            info.CertificateID = tmpStrings[2];
            info.Calibrated_By = tmpStrings[3];
            if (tmpStrings.length > 4) {
                info.Function = (int) OtherUtils.parseLong(tmpStrings[4]);
            } else {
                info.Function = 0;
            }
            if (tmpStrings.length > 5) {
                info.MainBoardVersion = tmpStrings[5].toUpperCase();
            } else {
                info.MainBoardVersion = PdfObject.NOTHING;
            }
            if (tmpStrings.length > 6) {
                info.MeasureBoardVersion = tmpStrings[6].toUpperCase();
            } else {
                info.MeasureBoardVersion = PdfObject.NOTHING;
            }
            if (tmpStrings.length > 7) {
                info.NeutralBoardVersion = tmpStrings[7].toUpperCase();
            } else {
                info.NeutralBoardVersion = PdfObject.NOTHING;
            }
        } catch (Exception e) {
            info.CalibrateDate = new Date(0);
            info.Calib_Valid_Date = new Date(0);
            info.CertificateID = PdfObject.NOTHING;
            info.Calibrated_By = PdfObject.NOTHING;
            info.MainBoardVersion = PdfObject.NOTHING;
            info.MeasureBoardVersion = PdfObject.NOTHING;
            info.NeutralBoardVersion = PdfObject.NOTHING;
        }
        return info;
    }

    @Override // com.clou.rs350.device.clinterface.IParameterSetting, com.clou.rs350.device.CL3122, com.clou.rs350.device.MeterBaseDevice
    public List<byte[]> setCalculateType(int apparentPower, int harmonicDefinition) {
        ((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.MeterSSumFlag))).Data[0] = Integer.valueOf(apparentPower);
        ((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.HarmonicTypeFlag))).Data[0] = Integer.valueOf(harmonicDefinition);
        return this.m_CLT.WriteData(new int[]{this.m_Dictionary.MeterSSumFlag, this.m_Dictionary.HarmonicTypeFlag});
    }

    @Override // com.clou.rs350.device.MeterBaseDevice, com.clou.rs350.device.clinterface.IAdjust
    public List<byte[]> setAdjustNeutral(double UnValue, double InValue) {
        List<byte[]> listbyte = setFunc_Mstate(CLTConstant.Func_Mstate_91);
        listbyte.addAll(setStart(1));
        double[] adjustValue = {UnValue, InValue};
        int[] key = {this.m_Dictionary.AdjustUnValue, this.m_Dictionary.AdjustInValue};
        int angleState = 0;
        for (int i = 0; i < 2; i++) {
            ((DataModel) this.m_Data.get(Integer.valueOf(key[i]))).Data[0] = Double.valueOf(adjustValue[i]);
            if (-1.0d != adjustValue[i]) {
                angleState |= 64 << i;
            }
        }
        this.m_CLT.DataMap = this.m_Data;
        listbyte.addAll(this.m_CLT.WriteData(key));
        ((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.AdjustStateValue))).Data[0] = 192;
        ((DataModel) this.m_Data.get(Integer.valueOf(this.m_Dictionary.AdjustStateAngle))).Data[0] = Integer.valueOf(angleState & 192);
        this.m_CLT.DataMap = this.m_Data;
        listbyte.addAll(this.m_CLT.WriteData(new int[]{this.m_Dictionary.AdjustStateValue, this.m_Dictionary.AdjustStateAngle}));
        return listbyte;
    }

    @Override // com.clou.rs350.device.CL3122, com.clou.rs350.device.MeterBaseDevice, com.clou.rs350.device.clinterface.IAdjust
    public List<byte[]> setDCAdjustFlag(int flag) {
        List<byte[]> listbyte = setFunc_Mstate(CLTConstant.Func_Mstate_91);
        ((DataModel) this.m_Data.get(Integer.valueOf(((RS350Dictionary) this.m_Dictionary).AdjustDCOffset))).Data[0] = Integer.valueOf(flag);
        listbyte.addAll(this.m_CLT.WriteData(new int[]{((RS350Dictionary) this.m_Dictionary).AdjustDCOffset}));
        return listbyte;
    }

    @Override // com.clou.rs350.device.CL3122, com.clou.rs350.device.MeterBaseDevice, com.clou.rs350.device.clinterface.IAdjust
    public List<byte[]> setResetAdjustmentFlag(int flag) {
        List<byte[]> listbyte = setFunc_Mstate(CLTConstant.Func_Mstate_91);
        ((DataModel) this.m_Data.get(Integer.valueOf(((RS350Dictionary) this.m_Dictionary).ResetAdjustmentData))).Data[0] = Integer.valueOf(flag);
        listbyte.addAll(this.m_CLT.WriteData(new int[]{((RS350Dictionary) this.m_Dictionary).ResetAdjustmentData}));
        return listbyte;
    }

    @Override // com.clou.rs350.device.MeterBaseDevice, com.clou.rs350.device.clinterface.IDigitalMetetTest
    public List<byte[]> setDigitalMeterParameters(int baudrate, int dataBits, int parity, int stopBits, int address, DigitalMeterDataType[] dataType, int pt, int ct) {
        List<byte[]> listbyte = setBaudrate(1, baudrate, dataBits, parity, stopBits);
        ((DataModel) this.m_Data.get(Integer.valueOf(((RS350Dictionary) this.m_Dictionary).DigitalMeterAddress))).Data[0] = Integer.valueOf(address);
        ((DataModel) this.m_Data.get(Integer.valueOf(((RS350Dictionary) this.m_Dictionary).DigitalPT))).Data[0] = Integer.valueOf(pt);
        ((DataModel) this.m_Data.get(Integer.valueOf(((RS350Dictionary) this.m_Dictionary).DigitalCT))).Data[0] = Integer.valueOf(ct);
        for (int i = 0; i < 3; i++) {
            ((DataModel) this.m_Data.get(Integer.valueOf(((RS350Dictionary) this.m_Dictionary).DigitalMeterDataType[i]))).Data[0] = Long.valueOf(((long) (dataType[i].Address + (dataType[i].Length << 16) + (dataType[i].DataType << 24))) + (((long) Float.floatToIntBits(dataType[i].Multiple)) << 32));
        }
        listbyte.addAll(this.m_CLT.WriteData(new int[]{((RS350Dictionary) this.m_Dictionary).DigitalMeterAddress, ((RS350Dictionary) this.m_Dictionary).DigitalMeterDataType[0], ((RS350Dictionary) this.m_Dictionary).DigitalMeterDataType[1], ((RS350Dictionary) this.m_Dictionary).DigitalMeterDataType[2], ((RS350Dictionary) this.m_Dictionary).DigitalPT, ((RS350Dictionary) this.m_Dictionary).DigitalCT}));
        return listbyte;
    }

    @Override // com.clou.rs350.device.MeterBaseDevice, com.clou.rs350.device.clinterface.IDigitalMetetTest
    public List<byte[]> getDigitalMeterErrors() {
        return this.m_CLT.AskData(new int[]{((RS350Dictionary) this.m_Dictionary).DigitalMeterPE1, ((RS350Dictionary) this.m_Dictionary).DigitalMeterQE1, ((RS350Dictionary) this.m_Dictionary).DigitalMeterSE1, ((RS350Dictionary) this.m_Dictionary).DigitalMeterP, ((RS350Dictionary) this.m_Dictionary).DigitalMeterQ, ((RS350Dictionary) this.m_Dictionary).DigitalMeterS, ((RS350Dictionary) this.m_Dictionary).DigitalMeterErrorCount});
    }

    @Override // com.clou.rs350.device.MeterBaseDevice, com.clou.rs350.device.clinterface.IDigitalMetetTest
    public ExplainedModel[] getDigitalMeterErrorsValues() {
        return new ExplainedModel[]{DataUtils.parse(((DataModel) this.m_Data.get(Integer.valueOf(((RS350Dictionary) this.m_Dictionary).DigitalMeterPE1))).Data[0], "%"), DataUtils.parse(((DataModel) this.m_Data.get(Integer.valueOf(((RS350Dictionary) this.m_Dictionary).DigitalMeterQE1))).Data[0], "%"), DataUtils.parse(((DataModel) this.m_Data.get(Integer.valueOf(((RS350Dictionary) this.m_Dictionary).DigitalMeterSE1))).Data[0], "%"), DataUtils.parseint(((DataModel) this.m_Data.get(Integer.valueOf(((RS350Dictionary) this.m_Dictionary).DigitalMeterErrorCount))).Data[0]), DataUtils.parse(((DataModel) this.m_Data.get(Integer.valueOf(((RS350Dictionary) this.m_Dictionary).DigitalMeterP))).Data[0], "W"), DataUtils.parse(((DataModel) this.m_Data.get(Integer.valueOf(((RS350Dictionary) this.m_Dictionary).DigitalMeterQ))).Data[0], "var"), DataUtils.parse(((DataModel) this.m_Data.get(Integer.valueOf(((RS350Dictionary) this.m_Dictionary).DigitalMeterS))).Data[0], "VA")};
    }

    @Override // com.clou.rs350.device.MeterBaseDevice, com.clou.rs350.device.clinterface.IDigitalMetetTest
    public List<byte[]> setDigitalMeterTestStartFlag(int Flag) {
        ((DataModel) this.m_Data.get(Integer.valueOf(((RS350Dictionary) this.m_Dictionary).DigitalMeterStartFlag))).Data[0] = Integer.valueOf(Flag);
        if (1 == Flag) {
            ((DataModel) this.m_Data.get(Integer.valueOf(((RS350Dictionary) this.m_Dictionary).DigitalMeterErrorCount))).Data[0] = 0;
            return this.m_CLT.WriteData(new int[]{((RS350Dictionary) this.m_Dictionary).DigitalMeterStartFlag, ((RS350Dictionary) this.m_Dictionary).DigitalMeterErrorCount});
        }
        return this.m_CLT.WriteData(new int[]{((RS350Dictionary) this.m_Dictionary).DigitalMeterStartFlag});
    }

    @Override // com.clou.rs350.device.CL3122, com.clou.rs350.device.MeterBaseDevice, com.clou.rs350.device.clinterface.IAdjust
    public List<byte[]> getAllowAdjustFlag() {
        return this.m_CLT.AskData(new int[]{((RS350Dictionary) this.m_Dictionary).AllowAdjustFlag});
    }

    @Override // com.clou.rs350.device.CL3122, com.clou.rs350.device.MeterBaseDevice, com.clou.rs350.device.clinterface.IAdjust
    public boolean getAllowAdjustFlagValue(Context context) {
        try {
            if ("MC-A2".equals(getStandardInfoValue(new StandardInfo(context)).MainBoardVersion.replace("_", "-"))) {
                return 1 == OtherUtils.convertToInt(((DataModel) this.m_Data.get(Integer.valueOf(((RS350Dictionary) this.m_Dictionary).AllowAdjustFlag))).Data[0]);
            }
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    @Override // com.clou.rs350.device.CL3122, com.clou.rs350.device.MeterBaseDevice, com.clou.rs350.device.clinterface.IAdjust
    public List<byte[]> cleanAllowAdjustFlag() {
        ((DataModel) this.m_Data.get(Integer.valueOf(((RS350Dictionary) this.m_Dictionary).AllowAdjustFlag))).Data[0] = 0;
        return this.m_CLT.WriteData(new int[]{((RS350Dictionary) this.m_Dictionary).AllowAdjustFlag});
    }

    @Override // com.clou.rs350.device.CL3122, com.clou.rs350.device.clinterface.IAdjust
    public List<byte[]> getClampAdjustmentFlag() {
        return this.m_CLT.AskArray(new int[]{((RS350Dictionary) this.m_Dictionary).ClampAdjustFlag});
    }

    @Override // com.clou.rs350.device.CL3122, com.clou.rs350.device.clinterface.IAdjust
    public int[] getClampAdjustmentFlagValue() {
        Object[] tmpData = ((DataModel) this.m_Data.get(Integer.valueOf(((RS350Dictionary) this.m_Dictionary).ClampAdjustFlag))).Data;
        int[] tmpDatas = new int[tmpData.length];
        for (int i = 0; i < tmpDatas.length; i++) {
            tmpDatas[i] = ((Integer) tmpData[i]).intValue();
        }
        return tmpDatas;
    }
}
