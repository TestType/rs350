package com.clou.rs350.device.clinterface;

import com.clou.rs350.device.model.ExplainedModel;
import java.util.List;

public interface IWiringCheck {
    ExplainedModel getCorrectionFactor();

    int[] getCurrentPhaseSequenceValue();

    int[] getVoltagePhaseSequenceValue();

    List<byte[]> getWiringCheck();

    int getWiringCheckCos();

    int getWiringRemrakValue();

    List<byte[]> setPowerFactor(int i);

    List<byte[]> setSimulationValue(double d, double d2, double d3, double d4, double d5, double d6, double d7, double d8, double d9, double d10, double d11, double d12);

    List<byte[]> setTestFlag(int i);
}
