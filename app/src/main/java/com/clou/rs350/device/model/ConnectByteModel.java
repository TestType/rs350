package com.clou.rs350.device.model;

import java.io.Serializable;

public class ConnectByteModel implements Serializable {
    private static final long serialVersionUID = 1;
    public byte[] Buffer = null;
    public int ComPort = 0;
    public int LocalPort = 0;
    public int ReferenceLocalPort = 0;
    public int ReferenceRemotePort = 0;
    public int RemotePort = 0;
}
