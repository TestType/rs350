package com.clou.rs350.device.clinterface;

import com.clou.rs350.device.model.ExplainedModel;
import java.util.List;

public interface IIntegration {
    List<byte[]> cleanIntegrationFlag();

    ExplainedModel[] getCosIntegrationValue();

    ExplainedModel[] getCurrentAngleIntegrationValue();

    ExplainedModel[] getCurrentCrestFactorIntegrationValue();

    ExplainedModel[] getCurrentIntegrationValue();

    ExplainedModel[] getFrequencyIntegrationValue();

    List<byte[]> getIntegrationData();

    int getIntegrationFlagValue();

    ExplainedModel[] getLLVoltageAngleIntegrationValue();

    ExplainedModel[] getLLVoltageCurrentAngleIntegrationValue();

    ExplainedModel[] getLLVoltageIntegrationValue();

    ExplainedModel[] getLNVoltageAngleIntegrationValue();

    ExplainedModel[] getLNVoltageCurrentAngleIntegrationValue();

    ExplainedModel[] getLNVoltageIntegrationValue();

    ExplainedModel[] getNeutralIntegrationValue();

    ExplainedModel[] getPIntegrationValue();

    ExplainedModel[] getQIntegrationValue();

    ExplainedModel[] getSIntegrationValue();

    ExplainedModel[] getSinIntegrationValue();

    ExplainedModel[] getTHDIntegrationValue();

    ExplainedModel[] getVoltageCrestFactorIntegrationValue();

    List<byte[]> setIntegrationParameter(int i, int i2);
}
