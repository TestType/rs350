package com.clou.rs350.device.model;

import com.itextpdf.text.pdf.PdfObject;

public class DataModel {
    public int ArrayLength;
    public Object[] Data;
    public int DataLength;
    public int DataType;
    public String FormatString;
    private float Multiple;

    public DataModel(int dataType, int dataLength, int arrayLength, float multiple) {
        this(dataType, dataLength, arrayLength, multiple, "#.########");
    }

    public DataModel(int dataType, int dataLength, int arrayLength, float multiple, String formatString) {
        this.Multiple = 1.0f;
        this.FormatString = PdfObject.NOTHING;
        this.DataType = dataType;
        this.DataLength = dataLength;
        this.ArrayLength = arrayLength;
        this.Multiple = multiple;
        this.Data = new Object[arrayLength];
        for (int i = 0; i < arrayLength; i++) {
            this.Data[i] = 0;
        }
        this.FormatString = formatString;
    }

    public float getMultiple() {
        return this.Multiple;
    }
}
