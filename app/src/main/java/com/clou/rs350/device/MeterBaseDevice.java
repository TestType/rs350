package com.clou.rs350.device;

import android.content.Context;
import com.clou.rs350.db.model.DigitalMeterDataType;
import com.clou.rs350.db.model.StandardInfo;
import com.clou.rs350.device.clinterface.IAdjust;
import com.clou.rs350.device.clinterface.IDigitalMetetTest;
import com.clou.rs350.device.clinterface.IIntegration;
import com.clou.rs350.device.clinterface.IMeter;
import com.clou.rs350.device.clinterface.IPTCTMeasurement;
import com.clou.rs350.device.clinterface.IParameterSetting;
import com.clou.rs350.device.clinterface.IPowerQuality;
import com.clou.rs350.device.clinterface.IReadMeter;
import com.clou.rs350.device.clinterface.ITest;
import com.clou.rs350.device.clinterface.IWiringCheck;
import com.clou.rs350.device.model.ConnectByteModel;
import com.clou.rs350.device.model.ExplainedModel;
import com.itextpdf.text.pdf.PdfObject;
import java.util.ArrayList;
import java.util.List;

public abstract class MeterBaseDevice extends BaseDevice implements IMeter, IAdjust, IParameterSetting, IPowerQuality, ITest, IWiringCheck, IReadMeter, IPTCTMeasurement, IIntegration, IDigitalMetetTest {
    @Override // com.clou.rs350.device.clinterface.IPTCTMeasurement
    public List<byte[]> setPTAddress(int location, int channel) {
        return new ArrayList();
    }

    @Override // com.clou.rs350.device.clinterface.IPTCTMeasurement
    public List<byte[]> setPTConnect(int connect) {
        return new ArrayList();
    }

    @Override // com.clou.rs350.device.clinterface.IPTCTMeasurement
    public List<byte[]> setCTChannel(int Channel) {
        return new ArrayList();
    }

    @Override // com.clou.rs350.device.clinterface.IPTCTMeasurement
    public List<byte[]> getCTValue() {
        return new ArrayList();
    }

    @Override // com.clou.rs350.device.clinterface.IPTCTMeasurement
    public List<byte[]> getPTError() {
        return new ArrayList();
    }

    @Override // com.clou.rs350.device.clinterface.IPTCTMeasurement
    public List<byte[]> getPTDeviceState() {
        return new ArrayList();
    }

    @Override // com.clou.rs350.device.clinterface.IPTCTMeasurement
    public List<byte[]> adjustPT() {
        return new ArrayList();
    }

    @Override // com.clou.rs350.device.clinterface.IPTCTMeasurement
    public List<byte[]> setAutomaticCheck(int check) {
        return new ArrayList();
    }

    @Override // com.clou.rs350.device.clinterface.IPTCTMeasurement
    public List<byte[]> setPTSettingFlag(int PTSettingFlag) {
        return new ArrayList();
    }

    @Override // com.clou.rs350.device.clinterface.IPTCTMeasurement
    public List<byte[]> getAutomaticCheckState() {
        return new ArrayList();
    }

    @Override // com.clou.rs350.device.clinterface.IPTCTMeasurement
    public List<byte[]> cleanAutomaticCheckState() {
        return new ArrayList();
    }

    @Override // com.clou.rs350.device.clinterface.IPTCTMeasurement
    public List<byte[]> getGPS() {
        return new ArrayList();
    }

    @Override // com.clou.rs350.device.clinterface.IReadMeter
    public List<byte[]> setBaudrate(int comPort, int baudrate, int databits, int parity, int stopbits) {
        return new ArrayList();
    }

    @Override // com.clou.rs350.device.clinterface.IReadMeter
    public byte[] sendBuffer(int comPort, byte[] buffer) {
        return new byte[1];
    }

    @Override // com.clou.rs350.device.clinterface.IReadMeter
    public List<byte[]> sendBuffer(int comPort, List<byte[]> list) {
        return new ArrayList();
    }

    @Override // com.clou.rs350.device.clinterface.IReadMeter
    public ConnectByteModel getReceiveModel() {
        return new ConnectByteModel();
    }

    @Override // com.clou.rs350.device.clinterface.IReadMeter
    public void clearReceiveBuffer() {
    }

    @Override // com.clou.rs350.device.clinterface.IWiringCheck
    public List<byte[]> getWiringCheck() {
        return new ArrayList();
    }

    @Override // com.clou.rs350.device.clinterface.IWiringCheck
    public List<byte[]> setPowerFactor(int powerFactor) {
        return new ArrayList();
    }

    @Override // com.clou.rs350.device.clinterface.IWiringCheck
    public List<byte[]> setTestFlag(int flag) {
        return new ArrayList();
    }

    @Override // com.clou.rs350.device.clinterface.IWiringCheck
    public List<byte[]> setSimulationValue(double ua, double ub, double uc, double uaAngle, double ubAngle, double ucAngle, double ia, double ib, double ic, double iaAngle, double ibAngle, double icAngle) {
        return new ArrayList();
    }

    @Override // com.clou.rs350.device.clinterface.ITest
    public List<byte[]> getCheckKFlag() {
        return new ArrayList();
    }

    @Override // com.clou.rs350.device.clinterface.ITest
    public List<byte[]> setTestMode(int flag) {
        return new ArrayList();
    }

    @Override // com.clou.rs350.device.clinterface.ITest
    public List<byte[]> getErrors() {
        return new ArrayList();
    }

    @Override // com.clou.rs350.device.clinterface.ITest
    public List<byte[]> getMeterConstant() {
        return new ArrayList();
    }

    @Override // com.clou.rs350.device.clinterface.ITest
    public List<byte[]> setCheckKFlag(int flag) {
        return new ArrayList();
    }

    @Override // com.clou.rs350.device.clinterface.ITest
    public List<byte[]> getEnergy() {
        return new ArrayList();
    }

    @Override // com.clou.rs350.device.clinterface.ITest
    public ExplainedModel[] getEnergyValues() {
        return new ExplainedModel[]{new ExplainedModel(), new ExplainedModel(), new ExplainedModel(), new ExplainedModel()};
    }

    @Override // com.clou.rs350.device.clinterface.ITest
    public List<byte[]> getDemand() {
        return new ArrayList();
    }

    @Override // com.clou.rs350.device.clinterface.ITest
    public ExplainedModel[] getDemandValues() {
        return new ExplainedModel[]{new ExplainedModel()};
    }

    @Override // com.clou.rs350.device.clinterface.ITest
    public List<byte[]> setErrorParameters(float[] constants, int[] pulses) {
        return new ArrayList();
    }

    @Override // com.clou.rs350.device.clinterface.ITest
    public List<byte[]> setErrorParameters(int[] measurementType) {
        return new ArrayList();
    }

    @Override // com.clou.rs350.device.clinterface.ITest
    public List<byte[]> setErrorParameters(int[] measurementType, float[] constants, int[] pulses) {
        return new ArrayList();
    }

    @Override // com.clou.rs350.device.clinterface.ITest
    public List<byte[]> setErrorParameters(int[] errorCount, int[] measurementType, float[] constants, int[] pulses) {
        return new ArrayList();
    }

    @Override // com.clou.rs350.device.clinterface.ITest
    public List<byte[]> setErrorParameters(double ct, int[] errorCount, int[] measurementType, float[] constants, int[] pulses) {
        return new ArrayList();
    }

    @Override // com.clou.rs350.device.clinterface.IParameterSetting
    public List<byte[]> cleanOverload() {
        return new ArrayList();
    }

    @Override // com.clou.rs350.device.clinterface.ITest
    public List<byte[]> setDailyParameters(int[] measurementType, int[] constants, int[] pulses) {
        return new ArrayList();
    }

    @Override // com.clou.rs350.device.clinterface.IPowerQuality
    public List<byte[]> getPowerQualityUnbalance() {
        return new ArrayList();
    }

    @Override // com.clou.rs350.device.clinterface.IPowerQuality
    public List<byte[]> getPowerQualityStability() {
        return new ArrayList();
    }

    @Override // com.clou.rs350.device.clinterface.IPowerQuality
    public List<byte[]> setPowerQualityStabilityParams(int StbMode, int PhaseStabTestTime, int PhaseStabSampleTime) {
        return new ArrayList();
    }

    @Override // com.clou.rs350.device.clinterface.IParameterSetting
    public List<byte[]> getBattery() {
        return new ArrayList();
    }

    @Override // com.clou.rs350.device.clinterface.IParameterSetting
    public List<byte[]> getWiringData() {
        return new ArrayList();
    }

    @Override // com.clou.rs350.device.clinterface.IParameterSetting
    public List<byte[]> setWiringData(int Wiring, int Pulse) {
        return new ArrayList();
    }

    @Override // com.clou.rs350.device.clinterface.IParameterSetting
    public List<byte[]> setPTCTRatio(double VT, double CR, int RatioApply) {
        return new ArrayList();
    }

    @Override // com.clou.rs350.device.clinterface.IParameterSetting
    public List<byte[]> getRange() {
        return new ArrayList();
    }

    @Override // com.clou.rs350.device.clinterface.IParameterSetting
    public List<byte[]> setRange(int AutoConnectType, int connectType, int AutoRange, double UaRange, double UbRange, double UcRange, double IaRange, double IbRange, double IcRange) {
        return new ArrayList();
    }

    @Override // com.clou.rs350.device.clinterface.IParameterSetting
    public List<byte[]> setNeutralRange(int AutoRange, int UnRange, int InRange) {
        return new ArrayList();
    }

    @Override // com.clou.rs350.device.clinterface.IParameterSetting
    public List<byte[]> getConnectType() {
        return new ArrayList();
    }

    @Override // com.clou.rs350.device.clinterface.IParameterSetting
    public List<byte[]> setConnectType(int ConnectType) {
        return new ArrayList();
    }

    @Override // com.clou.rs350.device.clinterface.IParameterSetting
    public List<byte[]> getParameter() {
        return new ArrayList();
    }

    @Override // com.clou.rs350.device.clinterface.IParameterSetting
    public List<byte[]> setParameter(int Wiring, int Pluse, int Auto, int UaRange, int UbRange, int UcRange, int IaRange, int IbRange, int IcRange) {
        return new ArrayList();
    }

    @Override // com.clou.rs350.device.clinterface.IParameterSetting
    public List<byte[]> getVersion() {
        return new ArrayList();
    }

    @Override // com.clou.rs350.device.clinterface.IParameterSetting
    public List<byte[]> getSerialNumber() {
        return new ArrayList();
    }

    @Override // com.clou.rs350.device.clinterface.IParameterSetting
    public List<byte[]> getStandardInfo() {
        return new ArrayList();
    }

    @Override // com.clou.rs350.device.clinterface.IParameterSetting
    public List<byte[]> setStandardInfo(StandardInfo info) {
        return new ArrayList();
    }

    @Override // com.clou.rs350.device.clinterface.IParameterSetting
    public StandardInfo getStandardInfoValue(StandardInfo info) {
        return info;
    }

    @Override // com.clou.rs350.device.clinterface.IParameterSetting
    public List<byte[]> getHardwareVersion() {
        return new ArrayList();
    }

    @Override // com.clou.rs350.device.clinterface.IParameterSetting
    public List<byte[]> setConstant(long constant) {
        return new ArrayList();
    }

    @Override // com.clou.rs350.device.clinterface.IParameterSetting
    public List<byte[]> getConstant() {
        return new ArrayList();
    }

    @Override // com.clou.rs350.device.clinterface.IParameterSetting
    public List<byte[]> setStart(int start) {
        return new ArrayList();
    }

    @Override // com.clou.rs350.device.clinterface.IParameterSetting
    public List<byte[]> getStartState() {
        return new ArrayList();
    }

    @Override // com.clou.rs350.device.clinterface.IParameterSetting
    public List<byte[]> getStartStateAndFunc_Mstate() {
        return new ArrayList();
    }

    @Override // com.clou.rs350.device.clinterface.IParameterSetting
    public List<byte[]> getFunc_Mstate() {
        return new ArrayList();
    }

    @Override // com.clou.rs350.device.clinterface.IParameterSetting
    public List<byte[]> setFunc_Mstate(int state) {
        return new ArrayList();
    }

    @Override // com.clou.rs350.device.clinterface.IParameterSetting
    public List<byte[]> setFunc_Mstate_Start(int state, int start) {
        return new ArrayList();
    }

    @Override // com.clou.rs350.device.clinterface.IAdjust
    public List<byte[]> setAdjust(double UaValue, double UbValue, double UcValue, double IaValue, double IbValue, double IcValue, double UaAngle, double UbAngle, double UcAngle, double IaAngle, double IbAngle, double IcAngle) {
        return new ArrayList();
    }

    @Override // com.clou.rs350.device.clinterface.IAdjust
    public List<byte[]> setAdjustNeutral(double UnValue, double InValue) {
        return new ArrayList();
    }

    @Override // com.clou.rs350.device.clinterface.IAdjust
    public List<byte[]> setAdjustValue(double UaValue, double UbValue, double UcValue, double IaValue, double IbValue, double IcValue, double UaAngle, double UbAngle, double UcAngle, double IaAngle, double IbAngle, double IcAngle) {
        return new ArrayList();
    }

    @Override // com.clou.rs350.device.clinterface.IAdjust
    public List<byte[]> getAdjustState() {
        return new ArrayList();
    }

    @Override // com.clou.rs350.device.clinterface.IAdjust
    public List<byte[]> setStopAdjust() {
        return new ArrayList();
    }

    @Override // com.clou.rs350.device.clinterface.IAdjust
    public List<byte[]> getAdjustFlag() {
        return new ArrayList();
    }

    @Override // com.clou.rs350.device.clinterface.IAdjust
    public List<byte[]> setAdjustFlag(int flag) {
        return new ArrayList();
    }

    @Override // com.clou.rs350.device.clinterface.IMeter
    public List<byte[]> getMeasureBasicData() {
        return new ArrayList();
    }

    @Override // com.clou.rs350.device.clinterface.IMeter
    public List<byte[]> getMeasureSpecialData() {
        return new ArrayList();
    }

    @Override // com.clou.rs350.device.clinterface.IMeter
    public List<byte[]> getHarmonic(String type) {
        return new ArrayList();
    }

    @Override // com.clou.rs350.device.clinterface.IMeter
    public List<byte[]> getHarmonicValues(int type) {
        return new ArrayList();
    }

    @Override // com.clou.rs350.device.clinterface.IMeter
    public List<byte[]> getHarmonicValues(int phase, int type) {
        return new ArrayList();
    }

    @Override // com.clou.rs350.device.clinterface.IMeter
    public List<byte[]> getHarmonic(int type) {
        return new ArrayList();
    }

    @Override // com.clou.rs350.device.clinterface.IMeter
    public List<byte[]> getTHD() {
        return new ArrayList();
    }

    @Override // com.clou.rs350.device.clinterface.IMeter
    public List<byte[]> getHarmonic() {
        return new ArrayList();
    }

    @Override // com.clou.rs350.device.clinterface.IMeter
    public List<byte[]> getUIWave() {
        return new ArrayList();
    }

    @Override // com.clou.rs350.device.clinterface.IMeter
    public ExplainedModel[] getUIPPValue() {
        return new ExplainedModel[]{new ExplainedModel(), new ExplainedModel(), new ExplainedModel(), new ExplainedModel(), new ExplainedModel(), new ExplainedModel(), new ExplainedModel(), new ExplainedModel(), new ExplainedModel(), new ExplainedModel(), new ExplainedModel(), new ExplainedModel()};
    }

    @Override // com.clou.rs350.device.clinterface.IMeter
    public List<byte[]> getPQWave() {
        return new ArrayList();
    }

    @Override // com.clou.rs350.device.clinterface.IMeter
    public ExplainedModel[] getPQPPValue() {
        return new ExplainedModel[]{new ExplainedModel(), new ExplainedModel(), new ExplainedModel(), new ExplainedModel(), new ExplainedModel(), new ExplainedModel(), new ExplainedModel(), new ExplainedModel(), new ExplainedModel(), new ExplainedModel(), new ExplainedModel(), new ExplainedModel()};
    }

    @Override // com.clou.rs350.device.clinterface.IParameterSetting
    public List<byte[]> setCalculateType(int apparentPower, int harmonicDefinition) {
        return new ArrayList();
    }

    @Override // com.clou.rs350.device.BaseDevice
    public int UnPacket(byte[] ReceiveBuffer) {
        return 0;
    }

    @Override // com.clou.rs350.device.BaseDevice
    public byte[] AnsOK() {
        return new byte[0];
    }

    @Override // com.clou.rs350.device.BaseDevice
    public byte[] AnsErr() {
        return new byte[0];
    }

    @Override // com.clou.rs350.device.clinterface.IIntegration
    public List<byte[]> getIntegrationData() {
        return new ArrayList();
    }

    @Override // com.clou.rs350.device.clinterface.IIntegration
    public List<byte[]> setIntegrationParameter(int Interval, int Period) {
        return new ArrayList();
    }

    @Override // com.clou.rs350.device.clinterface.IMeter
    public ExplainedModel[] getLNVoltageValue() {
        return new ExplainedModel[]{new ExplainedModel(), new ExplainedModel(), new ExplainedModel()};
    }

    @Override // com.clou.rs350.device.clinterface.IMeter
    public ExplainedModel[] getLLVoltageValue() {
        return new ExplainedModel[]{new ExplainedModel(), new ExplainedModel(), new ExplainedModel()};
    }

    @Override // com.clou.rs350.device.clinterface.IMeter
    public ExplainedModel[] getCurrentValue() {
        return new ExplainedModel[]{new ExplainedModel(), new ExplainedModel(), new ExplainedModel()};
    }

    @Override // com.clou.rs350.device.clinterface.IMeter
    public ExplainedModel[] getPValue() {
        return new ExplainedModel[]{new ExplainedModel(), new ExplainedModel(), new ExplainedModel(), new ExplainedModel()};
    }

    @Override // com.clou.rs350.device.clinterface.IMeter
    public ExplainedModel[] getQValue() {
        return new ExplainedModel[]{new ExplainedModel(), new ExplainedModel(), new ExplainedModel(), new ExplainedModel()};
    }

    @Override // com.clou.rs350.device.clinterface.IMeter
    public ExplainedModel[] getSValue() {
        return new ExplainedModel[]{new ExplainedModel(), new ExplainedModel(), new ExplainedModel(), new ExplainedModel()};
    }

    @Override // com.clou.rs350.device.clinterface.IMeter
    public ExplainedModel[] getCosValue() {
        return new ExplainedModel[]{new ExplainedModel(), new ExplainedModel(), new ExplainedModel(), new ExplainedModel()};
    }

    @Override // com.clou.rs350.device.clinterface.IMeter
    public ExplainedModel[] getSinValue() {
        return new ExplainedModel[]{new ExplainedModel(), new ExplainedModel(), new ExplainedModel(), new ExplainedModel()};
    }

    @Override // com.clou.rs350.device.clinterface.IMeter
    public ExplainedModel[] getLNVoltageAngleValue() {
        return new ExplainedModel[]{new ExplainedModel(), new ExplainedModel(), new ExplainedModel()};
    }

    @Override // com.clou.rs350.device.clinterface.IMeter
    public ExplainedModel[] getLLVoltageAngleValue() {
        return new ExplainedModel[]{new ExplainedModel(), new ExplainedModel(), new ExplainedModel(), new ExplainedModel()};
    }

    @Override // com.clou.rs350.device.clinterface.IMeter
    public ExplainedModel[] getLNVoltageCurrentAngleValue() {
        return new ExplainedModel[]{new ExplainedModel(), new ExplainedModel(), new ExplainedModel()};
    }

    @Override // com.clou.rs350.device.clinterface.IMeter
    public ExplainedModel[] getLLVoltageCurrentAngleValue() {
        return new ExplainedModel[]{new ExplainedModel(), new ExplainedModel(), new ExplainedModel(), new ExplainedModel()};
    }

    @Override // com.clou.rs350.device.clinterface.IMeter
    public ExplainedModel[] getFrequencyValue() {
        return new ExplainedModel[]{new ExplainedModel()};
    }

    @Override // com.clou.rs350.device.clinterface.IMeter
    public ExplainedModel[] getCurrentAngleValue() {
        return new ExplainedModel[]{new ExplainedModel(), new ExplainedModel(), new ExplainedModel()};
    }

    @Override // com.clou.rs350.device.clinterface.IMeter
    public ExplainedModel[] getNeutralValue() {
        return new ExplainedModel[]{new ExplainedModel(), new ExplainedModel()};
    }

    @Override // com.clou.rs350.device.clinterface.IAdjust
    public boolean getAdjustResult() {
        return false;
    }

    @Override // com.clou.rs350.device.clinterface.IMeter
    public ExplainedModel[] getHarmonicValuesValue(int Phase, int Flag) {
        return new ExplainedModel[]{new ExplainedModel(), new ExplainedModel(), new ExplainedModel(), new ExplainedModel(), new ExplainedModel(), new ExplainedModel(), new ExplainedModel(), new ExplainedModel(), new ExplainedModel(), new ExplainedModel(), new ExplainedModel(), new ExplainedModel(), new ExplainedModel(), new ExplainedModel(), new ExplainedModel(), new ExplainedModel(), new ExplainedModel(), new ExplainedModel(), new ExplainedModel(), new ExplainedModel(), new ExplainedModel(), new ExplainedModel(), new ExplainedModel(), new ExplainedModel(), new ExplainedModel(), new ExplainedModel(), new ExplainedModel(), new ExplainedModel(), new ExplainedModel(), new ExplainedModel(), new ExplainedModel(), new ExplainedModel(), new ExplainedModel(), new ExplainedModel(), new ExplainedModel(), new ExplainedModel(), new ExplainedModel(), new ExplainedModel(), new ExplainedModel(), new ExplainedModel(), new ExplainedModel(), new ExplainedModel(), new ExplainedModel(), new ExplainedModel(), new ExplainedModel(), new ExplainedModel(), new ExplainedModel(), new ExplainedModel(), new ExplainedModel(), new ExplainedModel(), new ExplainedModel(), new ExplainedModel(), new ExplainedModel(), new ExplainedModel(), new ExplainedModel(), new ExplainedModel(), new ExplainedModel(), new ExplainedModel(), new ExplainedModel(), new ExplainedModel(), new ExplainedModel(), new ExplainedModel(), new ExplainedModel(), new ExplainedModel()};
    }

    @Override // com.clou.rs350.device.clinterface.IMeter
    public Double[] getHarmonicContantsValue(int Phase) {
        return new Double[64];
    }

    @Override // com.clou.rs350.device.clinterface.IMeter
    public Double[] getHarmonicAnglesValue(int Phase) {
        return new Double[64];
    }

    @Override // com.clou.rs350.device.clinterface.IMeter
    public ExplainedModel[] getTHDValue() {
        return new ExplainedModel[]{new ExplainedModel(), new ExplainedModel(), new ExplainedModel(), new ExplainedModel(), new ExplainedModel(), new ExplainedModel()};
    }

    @Override // com.clou.rs350.device.clinterface.IMeter
    public Double[] getUIWaveValue(int Phase) {
        return new Double[64];
    }

    @Override // com.clou.rs350.device.clinterface.IMeter
    public Double[] getPQWaveValue(int Phase) {
        return new Double[64];
    }

    @Override // com.clou.rs350.device.clinterface.IMeter
    public ExplainedModel[] getVoltageCrestFactorValue() {
        return new ExplainedModel[]{new ExplainedModel(), new ExplainedModel(), new ExplainedModel()};
    }

    @Override // com.clou.rs350.device.clinterface.IMeter
    public ExplainedModel[] getCurrentCrestFactorValue() {
        return new ExplainedModel[]{new ExplainedModel(), new ExplainedModel(), new ExplainedModel()};
    }

    @Override // com.clou.rs350.device.clinterface.IParameterSetting
    public int getBatteryValue() {
        return 0;
    }

    @Override // com.clou.rs350.device.clinterface.IParameterSetting
    public String getVersionValue() {
        return PdfObject.NOTHING;
    }

    @Override // com.clou.rs350.device.clinterface.IParameterSetting
    public int getStartStateValue() {
        return 0;
    }

    @Override // com.clou.rs350.device.clinterface.IParameterSetting
    public int getFunctionValue() {
        return 0;
    }

    @Override // com.clou.rs350.device.clinterface.IParameterSetting
    public ExplainedModel[] getRangeValue() {
        return new ExplainedModel[]{new ExplainedModel(), new ExplainedModel(), new ExplainedModel(), new ExplainedModel(), new ExplainedModel(), new ExplainedModel()};
    }

    @Override // com.clou.rs350.device.clinterface.IParameterSetting
    public ExplainedModel[] getConnectTypeValue() {
        return new ExplainedModel[]{new ExplainedModel(), new ExplainedModel(), new ExplainedModel(), new ExplainedModel(), new ExplainedModel(), new ExplainedModel()};
    }

    @Override // com.clou.rs350.device.clinterface.IParameterSetting
    public long getConstantValue() {
        return 0;
    }

    @Override // com.clou.rs350.device.clinterface.ITest
    public int getCheckKFlagValue() {
        return 0;
    }

    @Override // com.clou.rs350.device.clinterface.ITest
    public ExplainedModel getMeterConstantValues(int index) {
        return new ExplainedModel();
    }

    @Override // com.clou.rs350.device.clinterface.IParameterSetting
    public boolean[] getOverloadValue() {
        return new boolean[6];
    }

    @Override // com.clou.rs350.device.clinterface.IIntegration
    public int getIntegrationFlagValue() {
        return 0;
    }

    @Override // com.clou.rs350.device.clinterface.IIntegration
    public List<byte[]> cleanIntegrationFlag() {
        return null;
    }

    @Override // com.clou.rs350.device.clinterface.IIntegration
    public ExplainedModel[] getLNVoltageIntegrationValue() {
        return new ExplainedModel[]{new ExplainedModel(), new ExplainedModel(), new ExplainedModel()};
    }

    @Override // com.clou.rs350.device.clinterface.IIntegration
    public ExplainedModel[] getLLVoltageIntegrationValue() {
        return new ExplainedModel[]{new ExplainedModel(), new ExplainedModel(), new ExplainedModel()};
    }

    @Override // com.clou.rs350.device.clinterface.IIntegration
    public ExplainedModel[] getCurrentIntegrationValue() {
        return new ExplainedModel[]{new ExplainedModel(), new ExplainedModel(), new ExplainedModel()};
    }

    @Override // com.clou.rs350.device.clinterface.IIntegration
    public ExplainedModel[] getPIntegrationValue() {
        return new ExplainedModel[]{new ExplainedModel(), new ExplainedModel(), new ExplainedModel(), new ExplainedModel()};
    }

    @Override // com.clou.rs350.device.clinterface.IIntegration
    public ExplainedModel[] getQIntegrationValue() {
        return new ExplainedModel[]{new ExplainedModel(), new ExplainedModel(), new ExplainedModel(), new ExplainedModel()};
    }

    @Override // com.clou.rs350.device.clinterface.IIntegration
    public ExplainedModel[] getSIntegrationValue() {
        return new ExplainedModel[]{new ExplainedModel(), new ExplainedModel(), new ExplainedModel(), new ExplainedModel()};
    }

    @Override // com.clou.rs350.device.clinterface.IIntegration
    public ExplainedModel[] getCosIntegrationValue() {
        return new ExplainedModel[]{new ExplainedModel(), new ExplainedModel(), new ExplainedModel(), new ExplainedModel()};
    }

    @Override // com.clou.rs350.device.clinterface.IIntegration
    public ExplainedModel[] getSinIntegrationValue() {
        return new ExplainedModel[]{new ExplainedModel(), new ExplainedModel(), new ExplainedModel(), new ExplainedModel()};
    }

    @Override // com.clou.rs350.device.clinterface.IIntegration
    public ExplainedModel[] getLNVoltageAngleIntegrationValue() {
        return new ExplainedModel[]{new ExplainedModel(), new ExplainedModel(), new ExplainedModel()};
    }

    @Override // com.clou.rs350.device.clinterface.IIntegration
    public ExplainedModel[] getLLVoltageAngleIntegrationValue() {
        return new ExplainedModel[]{new ExplainedModel(), new ExplainedModel(), new ExplainedModel(), new ExplainedModel()};
    }

    @Override // com.clou.rs350.device.clinterface.IIntegration
    public ExplainedModel[] getFrequencyIntegrationValue() {
        return new ExplainedModel[]{new ExplainedModel()};
    }

    @Override // com.clou.rs350.device.clinterface.IIntegration
    public ExplainedModel[] getCurrentAngleIntegrationValue() {
        return new ExplainedModel[]{new ExplainedModel(), new ExplainedModel(), new ExplainedModel()};
    }

    @Override // com.clou.rs350.device.clinterface.IIntegration
    public ExplainedModel[] getNeutralIntegrationValue() {
        return new ExplainedModel[]{new ExplainedModel(), new ExplainedModel()};
    }

    @Override // com.clou.rs350.device.clinterface.IIntegration
    public ExplainedModel[] getLNVoltageCurrentAngleIntegrationValue() {
        return new ExplainedModel[]{new ExplainedModel(), new ExplainedModel(), new ExplainedModel()};
    }

    @Override // com.clou.rs350.device.clinterface.IIntegration
    public ExplainedModel[] getLLVoltageCurrentAngleIntegrationValue() {
        return new ExplainedModel[]{new ExplainedModel(), new ExplainedModel(), new ExplainedModel(), new ExplainedModel()};
    }

    @Override // com.clou.rs350.device.clinterface.IIntegration
    public ExplainedModel[] getTHDIntegrationValue() {
        return new ExplainedModel[]{new ExplainedModel(), new ExplainedModel(), new ExplainedModel(), new ExplainedModel(), new ExplainedModel(), new ExplainedModel()};
    }

    @Override // com.clou.rs350.device.clinterface.IIntegration
    public ExplainedModel[] getVoltageCrestFactorIntegrationValue() {
        return new ExplainedModel[]{new ExplainedModel(), new ExplainedModel(), new ExplainedModel()};
    }

    @Override // com.clou.rs350.device.clinterface.IIntegration
    public ExplainedModel[] getCurrentCrestFactorIntegrationValue() {
        return new ExplainedModel[]{new ExplainedModel(), new ExplainedModel(), new ExplainedModel()};
    }

    @Override // com.clou.rs350.device.clinterface.IDigitalMetetTest
    public List<byte[]> getDigitalMeterErrors() {
        return new ArrayList();
    }

    @Override // com.clou.rs350.device.clinterface.IDigitalMetetTest
    public ExplainedModel[] getDigitalMeterErrorsValues() {
        return new ExplainedModel[]{new ExplainedModel(), new ExplainedModel(), new ExplainedModel(), new ExplainedModel()};
    }

    @Override // com.clou.rs350.device.clinterface.IDigitalMetetTest
    public List<byte[]> setDigitalMeterParameters(int baudrate, int dataBits, int parity, int stopBits, int address, DigitalMeterDataType[] dataType, int pt, int ct) {
        return new ArrayList();
    }

    @Override // com.clou.rs350.device.clinterface.IDigitalMetetTest
    public List<byte[]> setDigitalMeterTestStartFlag(int Flag) {
        return new ArrayList();
    }

    @Override // com.clou.rs350.device.clinterface.IPTCTMeasurement
    public ExplainedModel[] getCTErrorValue() {
        return new ExplainedModel[]{new ExplainedModel(), new ExplainedModel(), new ExplainedModel(), new ExplainedModel()};
    }

    @Override // com.clou.rs350.device.clinterface.IPTCTMeasurement
    public ExplainedModel[] getCTBurdenValue() {
        return new ExplainedModel[]{new ExplainedModel(), new ExplainedModel(), new ExplainedModel()};
    }

    @Override // com.clou.rs350.device.clinterface.IPTCTMeasurement
    public ExplainedModel[] getPTErrorValue() {
        return null;
    }

    @Override // com.clou.rs350.device.clinterface.IPTCTMeasurement
    public List<byte[]> getPTBurden() {
        return new ArrayList();
    }

    @Override // com.clou.rs350.device.clinterface.IPTCTMeasurement
    public ExplainedModel[] getPTBurdenValue() {
        return new ExplainedModel[]{new ExplainedModel(), new ExplainedModel(), new ExplainedModel(), new ExplainedModel(), new ExplainedModel(), new ExplainedModel(), new ExplainedModel(), new ExplainedModel(), new ExplainedModel(), new ExplainedModel(), new ExplainedModel(), new ExplainedModel(), new ExplainedModel(), new ExplainedModel(), new ExplainedModel()};
    }

    @Override // com.clou.rs350.device.clinterface.IPTCTMeasurement
    public int[] getPTConnectStateValue() {
        return new int[3];
    }

    @Override // com.clou.rs350.device.clinterface.IWiringCheck
    public int getWiringRemrakValue() {
        return 0;
    }

    @Override // com.clou.rs350.device.clinterface.IWiringCheck
    public ExplainedModel getCorrectionFactor() {
        return new ExplainedModel();
    }

    @Override // com.clou.rs350.device.clinterface.IWiringCheck
    public int getWiringCheckCos() {
        return 0;
    }

    @Override // com.clou.rs350.device.clinterface.IWiringCheck
    public int[] getVoltagePhaseSequenceValue() {
        return new int[]{1, 2, 4};
    }

    @Override // com.clou.rs350.device.clinterface.IWiringCheck
    public int[] getCurrentPhaseSequenceValue() {
        return new int[]{1, 2, 4};
    }

    @Override // com.clou.rs350.device.clinterface.ITest
    public ExplainedModel[] getErrorsValues(int index) {
        return new ExplainedModel[]{new ExplainedModel(), new ExplainedModel(), new ExplainedModel(), new ExplainedModel(), new ExplainedModel(), new ExplainedModel(), new ExplainedModel(), new ExplainedModel(), new ExplainedModel(), new ExplainedModel(), new ExplainedModel(), new ExplainedModel(), new ExplainedModel()};
    }

    @Override // com.clou.rs350.device.clinterface.IParameterSetting
    public int getWiringValue() {
        return 0;
    }

    @Override // com.clou.rs350.device.clinterface.IParameterSetting
    public int getDefinitionValue() {
        return 0;
    }

    @Override // com.clou.rs350.device.clinterface.IParameterSetting
    public int getPQFlagValue() {
        return 0;
    }

    @Override // com.clou.rs350.device.clinterface.IParameterSetting
    public int getRangeAutoValue() {
        return 0;
    }

    @Override // com.clou.rs350.device.clinterface.IParameterSetting
    public List<byte[]> setWiringData(int Wiring, int Pulse, int Definition) {
        return new ArrayList();
    }

    @Override // com.clou.rs350.device.clinterface.IParameterSetting
    public String getSerialNumberValue() {
        return PdfObject.NOTHING;
    }

    @Override // com.clou.rs350.device.clinterface.IAdjust
    public List<byte[]> setDCAdjustFlag(int flag) {
        return new ArrayList();
    }

    @Override // com.clou.rs350.device.clinterface.IAdjust
    public List<byte[]> setResetAdjustmentFlag(int flag) {
        return new ArrayList();
    }

    @Override // com.clou.rs350.device.clinterface.IAdjust
    public List<byte[]> getAllowAdjustFlag() {
        return new ArrayList();
    }

    @Override // com.clou.rs350.device.clinterface.IAdjust
    public boolean getAllowAdjustFlagValue(Context context) {
        return false;
    }

    @Override // com.clou.rs350.device.clinterface.IAdjust
    public List<byte[]> cleanAllowAdjustFlag() {
        return new ArrayList();
    }

    @Override // com.clou.rs350.device.clinterface.IMeter
    public List<byte[]> getHarmonicContents(int type) {
        return new ArrayList();
    }

    @Override // com.clou.rs350.device.clinterface.IMeter
    public List<byte[]> getHarmonicAngles(int type) {
        return new ArrayList();
    }

    /* access modifiers changed from: protected */
    @Override // com.clou.rs350.device.BaseDevice
    public void initRange() {
    }
}
