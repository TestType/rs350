package com.clou.rs350.device.clinterface;

import android.content.Context;
import java.util.List;

public interface IAdjust {
    List<byte[]> cleanAllowAdjustFlag();

    List<byte[]> getAdjustFlag();

    boolean getAdjustResult();

    List<byte[]> getAdjustState();

    List<byte[]> getAllowAdjustFlag();

    boolean getAllowAdjustFlagValue(Context context);

    List<byte[]> getClampAdjustmentFlag();

    int[] getClampAdjustmentFlagValue();

    List<byte[]> setAdjust(double d, double d2, double d3, double d4, double d5, double d6, double d7, double d8, double d9, double d10, double d11, double d12);

    List<byte[]> setAdjustFlag(int i);

    List<byte[]> setAdjustNeutral(double d, double d2);

    List<byte[]> setAdjustValue(double d, double d2, double d3, double d4, double d5, double d6, double d7, double d8, double d9, double d10, double d11, double d12);

    List<byte[]> setDCAdjustFlag(int i);

    List<byte[]> setResetAdjustmentFlag(int i);

    List<byte[]> setStopAdjust();
}
