package com.clou.rs350.device.clinterface;

import java.util.List;

public interface IPowerQuality {
    List<byte[]> getPowerQualityStability();

    List<byte[]> getPowerQualityUnbalance();

    List<byte[]> setPowerQualityStabilityParams(int i, int i2, int i3);
}
