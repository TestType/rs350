package com.clou.rs350.device;

import com.clou.rs350.R;
import com.clou.rs350.utils.OtherUtils;

import java.util.ArrayList;

public class CL3122_60A extends CL3122 {
    public CL3122_60A() {
        super(new CL3122_60ADictionary());
        initRange();
    }

    /* access modifiers changed from: protected */
    @Override // com.clou.rs350.device.BaseDevice, com.clou.rs350.device.CL3122, com.clou.rs350.device.MeterBaseDevice
    public void initRange() {
        this.S_ConnectType_All = new String[]{String.valueOf(OtherUtils.getString(R.string.text_U_Direct)) + "," + OtherUtils.getString(R.string.text_I_Direct), String.valueOf(OtherUtils.getString(R.string.text_U_Direct)) + "," + OtherUtils.getString(R.string.text_I_Clamp10A), String.valueOf(OtherUtils.getString(R.string.text_U_Direct)) + "," + OtherUtils.getString(R.string.text_I_Clamp100A), String.valueOf(OtherUtils.getString(R.string.text_U_Direct)) + "," + OtherUtils.getString(R.string.text_I_Clamp500A), String.valueOf(OtherUtils.getString(R.string.text_U_Direct)) + "," + OtherUtils.getString(R.string.text_I_Clamp1000A), String.valueOf(OtherUtils.getString(R.string.text_Pri_Clamp100A)) + "," + OtherUtils.getString(R.string.text_Sec_Clamp10A), String.valueOf(OtherUtils.getString(R.string.text_Pri_Clamp500A)) + "," + OtherUtils.getString(R.string.text_Sec_Clamp10A), String.valueOf(OtherUtils.getString(R.string.text_Pri_Clamp1000A)) + "," + OtherUtils.getString(R.string.text_Sec_Clamp10A)};
        this.S_ConnectType_Voltage = new String[]{OtherUtils.getString(R.string.text_U_Direct), OtherUtils.getString(R.string.text_U_Direct), OtherUtils.getString(R.string.text_U_Direct), OtherUtils.getString(R.string.text_U_Direct), OtherUtils.getString(R.string.text_U_Direct)};
        this.S_ConnectType_Current = new String[]{OtherUtils.getString(R.string.text_I_Direct), OtherUtils.getString(R.string.text_I_Clamp10A), OtherUtils.getString(R.string.text_I_Clamp100A), OtherUtils.getString(R.string.text_I_Clamp500A), OtherUtils.getString(R.string.text_I_Clamp1000A)};
        this.S_ConnectType_Voltage = new String[]{OtherUtils.getString(R.string.text_U_Direct)};
        this.S_ConnectType_Current = new String[]{OtherUtils.getString(R.string.text_I_Direct), OtherUtils.getString(R.string.text_I_Clamp10A), OtherUtils.getString(R.string.text_I_Clamp100A), OtherUtils.getString(R.string.text_I_Clamp500A), OtherUtils.getString(R.string.text_I_Clamp1000A)};
        this.VoltageRange_All = new String[]{"30", "50", "100", "200", "400"};
        this.CurrentRange_All = new String[]{"0.01", "0.04", "0.1", "0.2", "0.6", "1.0", "1.5", "3", "5", "6", "15", "30", "60", "50", "100", "200", "500", "1000"};
        int[] iArr = new int[8];
        iArr[1] = 1;
        iArr[2] = 2;
        iArr[3] = 3;
        iArr[4] = 4;
        iArr[5] = 5;
        iArr[6] = 6;
        iArr[7] = 7;
        this.I_ConnectType_All_Index = iArr;
        this.S_ConnectType_Realtime = new String[]{String.valueOf(OtherUtils.getString(R.string.text_U_Direct)) + "," + OtherUtils.getString(R.string.text_I_Direct), String.valueOf(OtherUtils.getString(R.string.text_U_Direct)) + "," + OtherUtils.getString(R.string.text_I_Clamp10A), String.valueOf(OtherUtils.getString(R.string.text_U_Direct)) + "," + OtherUtils.getString(R.string.text_I_Clamp100A), String.valueOf(OtherUtils.getString(R.string.text_U_Direct)) + "," + OtherUtils.getString(R.string.text_I_Clamp500A), String.valueOf(OtherUtils.getString(R.string.text_U_Direct)) + "," + OtherUtils.getString(R.string.text_I_Clamp1000A)};
        int[] iArr2 = new int[5];
        iArr2[1] = 1;
        iArr2[2] = 2;
        iArr2[3] = 3;
        iArr2[4] = 4;
        this.I_ConnectType_Realtime_Index = iArr2;
        this.S_ConnectType_CT = new String[]{String.valueOf(OtherUtils.getString(R.string.text_Pri_Clamp100A)) + "," + OtherUtils.getString(R.string.text_Sec_Clamp10A), String.valueOf(OtherUtils.getString(R.string.text_Pri_Clamp500A)) + "," + OtherUtils.getString(R.string.text_Sec_Clamp10A), String.valueOf(OtherUtils.getString(R.string.text_Pri_Clamp1000A)) + "," + OtherUtils.getString(R.string.text_Sec_Clamp10A)};
        this.I_ConnectType_CT_Index = new int[]{5, 6, 7};
        this.VoltageRange = new ArrayList();
        this.VoltageRange.add(new String[]{"30V", "50V", "100V", "200V", "400V"});
        this.VoltageRange.add(new String[]{"30V", "50V", "100V", "200V", "400V"});
        this.VoltageRange.add(new String[]{"30V", "50V", "100V", "200V", "400V"});
        this.VoltageRange.add(new String[]{"30V", "50V", "100V", "200V", "400V"});
        this.VoltageRange.add(new String[]{"30V", "50V", "100V", "200V", "400V"});
        this.VoltageRange.add(new String[]{"10A", "100A"});
        this.VoltageRange.add(new String[]{"50A", "500A"});
        this.VoltageRange.add(new String[]{"100A", "1000A"});
        this.VoltageRangeIndex = new int[]{3, 3, 3, 3, 3, 14, 16, 18};
        this.CurrentRange = new ArrayList();
        this.CurrentRange.add(new String[]{"0.01A", "0.04A", "0.1A", "0.2A", "0.6A", "1.5A", "3A", "6A", "15A", "30A", "60A"});
        this.CurrentRange.add(new String[]{"0.2A", "1", "5A"});
        this.CurrentRange.add(new String[]{"10A", "100A"});
        this.CurrentRange.add(new String[]{"50A", "500A"});
        this.CurrentRange.add(new String[]{"100A", "1000A"});
        this.CurrentRange.add(new String[]{"0.2A", "1", "5A"});
        this.CurrentRange.add(new String[]{"0.2A", "1", "5A"});
        this.CurrentRange.add(new String[]{"0.2A", "1", "5A"});
        int[] iArr3 = new int[8];
        iArr3[1] = 11;
        iArr3[2] = 14;
        iArr3[3] = 16;
        iArr3[4] = 18;
        iArr3[5] = 11;
        iArr3[6] = 11;
        iArr3[7] = 11;
        this.CurrentRangeIndex = iArr3;
    }
}
