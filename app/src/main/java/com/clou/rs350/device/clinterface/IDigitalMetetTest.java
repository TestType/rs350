package com.clou.rs350.device.clinterface;

import com.clou.rs350.db.model.DigitalMeterDataType;
import com.clou.rs350.device.model.ExplainedModel;
import java.util.List;

public interface IDigitalMetetTest {
    List<byte[]> getDigitalMeterErrors();

    ExplainedModel[] getDigitalMeterErrorsValues();

    List<byte[]> setDigitalMeterParameters(int i, int i2, int i3, int i4, int i5, DigitalMeterDataType[] digitalMeterDataTypeArr, int i6, int i7);

    List<byte[]> setDigitalMeterTestStartFlag(int i);
}
