package com.clou.rs350.device.clinterface;

import java.util.List;

public interface IPower {
    List<byte[]> CloseHarmonic();

    List<byte[]> PowerOff();

    List<byte[]> PowerOn(int i, double d, double d2, double d3, double d4, double d5, double d6, double d7, double d8, double d9, double d10, double d11, double d12, double d13, int i2);

    List<byte[]> PowerOn(int i, int i2, double d, double d2, String str, double d3);

    List<byte[]> SetHarmonic(double[] dArr, double[] dArr2, double[] dArr3, double[] dArr4, double[] dArr5, double[] dArr6, double[] dArr7, double[] dArr8, double[] dArr9, double[] dArr10, double[] dArr11, double[] dArr12);
}
