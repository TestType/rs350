package com.clou.rs350.device.clinterface;

import com.clou.rs350.db.model.StandardInfo;
import com.clou.rs350.device.model.ExplainedModel;
import java.util.List;

public interface IParameterSetting {
    List<byte[]> cleanOverload();

    List<byte[]> getBattery();

    int getBatteryValue();

    List<byte[]> getConnectType();

    ExplainedModel[] getConnectTypeValue();

    List<byte[]> getConstant();

    long getConstantValue();

    int getDefinitionValue();

    List<byte[]> getFunc_Mstate();

    int getFunctionValue();

    List<byte[]> getHardwareVersion();

    boolean[] getOverloadValue();

    int getPQFlagValue();

    List<byte[]> getParameter();

    List<byte[]> getRange();

    int getRangeAutoValue();

    ExplainedModel[] getRangeValue();

    List<byte[]> getSerialNumber();

    String getSerialNumberValue();

    List<byte[]> getStandardInfo();

    StandardInfo getStandardInfoValue(StandardInfo standardInfo);

    List<byte[]> getStartState();

    List<byte[]> getStartStateAndFunc_Mstate();

    int getStartStateValue();

    List<byte[]> getVersion();

    String getVersionValue();

    List<byte[]> getWiringData();

    int getWiringValue();

    List<byte[]> setCalculateType(int i, int i2);

    List<byte[]> setConnectType(int i);

    List<byte[]> setConstant(long j);

    List<byte[]> setFunc_Mstate(int i);

    List<byte[]> setFunc_Mstate_Start(int i, int i2);

    List<byte[]> setNeutralRange(int i, int i2, int i3);

    List<byte[]> setPTCTRatio(double d, double d2, int i);

    List<byte[]> setParameter(int i, int i2, int i3, int i4, int i5, int i6, int i7, int i8, int i9);

    List<byte[]> setRange(int i, int i2, int i3, double d, double d2, double d3, double d4, double d5, double d6);

    List<byte[]> setStandardInfo(StandardInfo standardInfo);

    List<byte[]> setStart(int i);

    List<byte[]> setWiringData(int i, int i2);

    List<byte[]> setWiringData(int i, int i2, int i3);
}
