package com.clou.rs350.device.utils;

import android.text.TextUtils;
import com.clou.rs350.device.constants.CLTConstant;
import com.clou.rs350.device.constants.UnpacketResult;
import com.itextpdf.text.pdf.BidiOrder;
import com.itextpdf.text.pdf.PdfObject;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.text.pdf.codec.JBIG2SegmentReader;
import com.itextpdf.xmp.XMPError;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;

public class BufferUtil {
    public static byte getXor(byte[] buffer, int begin, int end) {
        byte chksum = 0;
        for (int i = begin; i <= end; i++) {
            chksum = (byte) (buffer[i] ^ chksum);
        }
        return chksum;
    }

    public static byte getChecksum(byte[] buffer, int begin, int end) {
        byte chksum = 0;
        for (int i = begin; i <= end; i++) {
            chksum = (byte) (buffer[i] + chksum);
        }
        return chksum;
    }

    public static boolean getEqual(byte[] buffer_0, byte[] buffer_1) {
        if (buffer_0.length != buffer_1.length) {
            return false;
        }
        for (int i = 0; i < buffer_0.length; i++) {
            if (buffer_0[i] != buffer_1[i]) {
                return false;
            }
        }
        return true;
    }

    public static byte[] intTobyte(int data, boolean highBefore) {
        return intTobyte((long) data, highBefore, 4);
    }

    public static byte[] intTobyte(long data, boolean highBefore, int len) {
        byte[] buffer = new byte[len];
        if (highBefore) {
            for (int i = len - 1; i >= 0; i--) {
                buffer[i] = (byte) ((int) (data & 255));
                data >>= 8;
            }
        } else {
            for (int i2 = 0; i2 < len; i2++) {
                buffer[i2] = (byte) ((int) (data & 255));
                data >>= 8;
            }
        }
        return buffer;
    }

    public static long byteToint(byte[] buffer, boolean highBefore, boolean sign) {
        int len = buffer.length;
        long data = 0;
        if (highBefore) {
            if (sign && (buffer[0] & UnpacketResult.SENDFALSE) == 128) {
                for (int i = 0; i < 8 - len; i++) {
                    data = (data << 8) + 255;
                }
            }
            for (byte b : buffer) {
                data = (data << 8) | ((long) (b & 255));
            }
        } else {
            if (sign && (buffer[buffer.length - 1] & UnpacketResult.SENDFALSE) == 128) {
                for (int i2 = 0; i2 < 8 - len; i2++) {
                    data = (data << 8) + 255;
                }
            }
            for (int i3 = len - 1; i3 >= 0; i3--) {
                data = (data << 8) | ((long) (buffer[i3] & 255));
            }
        }
        return data;
    }

    public static byte[] floatTobyte(float inputData) {
        return intTobyte((long) Float.floatToIntBits(inputData), false, 4);
    }

    public static float byteTofloat(byte[] inputData) {
        return Float.intBitsToFloat((int) byteToint(inputData, false, false));
    }

    public static byte[] doubleTobyte(double inputData) {
        return intTobyte(Double.doubleToLongBits(inputData), false, 8);
    }

    public static double byteTodouble(byte[] inputData) {
        return Double.longBitsToDouble(byteToint(inputData, false, false));
    }

    public static byte[] intxe1Tobyte(double inputData, int intLen, String formatString) {
        byte[] buffer = new byte[(intLen + 1)];
        DecimalFormat df = new DecimalFormat(formatString);
        DecimalFormatSymbols symbols = new DecimalFormatSymbols();
        symbols.setDecimalSeparator('.');
        df.setDecimalFormatSymbols(symbols);
        String tmpString = df.format(inputData);
        int e = 0;
        if (tmpString.contains(".")) {
            e = (tmpString.length() - 1) - tmpString.indexOf(".");
        } else {
            int i = tmpString.length() - 1;
            while (i > 0 && tmpString.charAt(i) == '0') {
                e--;
                i--;
            }
        }
        System.arraycopy(intTobyte((long) ((int) Math.round(Math.pow(10.0d, (double) e) * inputData)), false, intLen), 0, buffer, 0, intLen);
        buffer[intLen] = (byte) (0 - e);
        return buffer;
    }

    public static double byteTointxe1(byte[] inputData) {
        int len = inputData.length;
        byte[] tmpbuffer = new byte[(len - 1)];
        System.arraycopy(inputData, 0, tmpbuffer, 0, len - 1);
        return ((double) byteToint(tmpbuffer, false, true)) * Math.pow(10.0d, (double) byteToint(new byte[]{inputData[len - 1]}, false, true));
    }

    public static byte BCDToint(byte inputData) {
        return (byte) ((((inputData >> 4) & 15) * 10) + (inputData & BidiOrder.B));
    }

    public static byte BCDToint(byte inputData, byte password) {
        byte inputData2 = (byte) (inputData - password);
        return (byte) ((((inputData2 >> 4) & 15) * 10) + (inputData2 & BidiOrder.B));
    }

    public static byte intToBCD(byte inputData) {
        return (byte) ((((inputData / 10) & 15) << 4) + (inputData % 10));
    }

    public static String toHexString(byte[] buffer) {
        String tmpString = PdfObject.NOTHING;
        if (buffer != null) {
            for (int i = 0; i < buffer.length; i++) {
                tmpString = String.valueOf(toHexString(buffer[i])) + tmpString;
            }
        }
        return tmpString;
    }

    public static String toHexString(byte byteHex) {
        char[] cHex = {'0', '1', PdfWriter.VERSION_1_2, PdfWriter.VERSION_1_3, PdfWriter.VERSION_1_4, PdfWriter.VERSION_1_5, PdfWriter.VERSION_1_6, PdfWriter.VERSION_1_7, '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};
        return String.valueOf(String.valueOf(PdfObject.NOTHING) + cHex[(byteHex >> 4) & 15]) + cHex[byteHex & BidiOrder.B];
    }

    public static String parseHexString(byte byteHex) {
        char[] cHex = {'0', '1', PdfWriter.VERSION_1_2, PdfWriter.VERSION_1_3, PdfWriter.VERSION_1_4, PdfWriter.VERSION_1_5, PdfWriter.VERSION_1_6, PdfWriter.VERSION_1_7, '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};
        return String.valueOf(String.valueOf(PdfObject.NOTHING) + cHex[byteHex >> 4]) + cHex[byteHex & BidiOrder.B];
    }

    public static int parseHexInt(String sHex) {
        if (TextUtils.isEmpty(sHex)) {
            return -1;
        }
        int iHex = 0;
        int i = 0;
        while (i < 8 && i < sHex.length()) {
            iHex = (iHex << 4) + parseHexCharToInt(sHex.charAt(i));
            i++;
        }
        return iHex;
    }

    private static int parseHexCharToInt(char charHex) {
        switch (charHex) {
            case JBIG2SegmentReader.PAGE_INFORMATION /*{ENCODED_INT: 48}*/:
                return 0;
            case '1':
                return 1;
            case '2':
                return 2;
            case JBIG2SegmentReader.END_OF_FILE /*{ENCODED_INT: 51}*/:
                return 3;
            case JBIG2SegmentReader.PROFILES /*{ENCODED_INT: 52}*/:
                return 4;
            case '5':
                return 5;
            case '6':
                return 6;
            case '7':
                return 7;
            case '8':
                return 8;
            case '9':
                return 9;
            case CLTConstant.Func_Mstate_41:
            case 'a':
                return 10;
            case CLTConstant.Func_Mstate_42:
            case 'b':
                return 11;
            case CLTConstant.Func_Mstate_43:
            case 'c':
                return 12;
            case 'D':
            case 'd':
                return 13;
            case 'E':
            case 'e':
                return 14;
            case 'F':
            case XMPError.BADXPATH /*{ENCODED_INT: 102}*/:
                return 15;
            default:
                return 0;
        }
    }
}
