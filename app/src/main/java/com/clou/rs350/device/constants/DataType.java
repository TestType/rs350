package com.clou.rs350.device.constants;

public class DataType {
    public static final int ASC = 1;
    public static final int BCD1 = 14;
    public static final int FLOAT4 = 10;
    public static final int FLOAT8 = 11;
    public static final int INT3E1 = 12;
    public static final int INT4E1 = 13;
    public static final int SINT1 = 6;
    public static final int SINT2 = 7;
    public static final int SINT3 = 8;
    public static final int SINT4 = 9;
    public static final int UINT1 = 2;
    public static final int UINT2 = 3;
    public static final int UINT3 = 4;
    public static final int UINT4 = 5;
    public static final int UINT8 = 15;
}
