package com.clou.rs350.device;

import com.clou.rs350.device.model.DataModel;
import java.util.List;
import java.util.Map;

public abstract class BaseDevice {
    protected List<String[]> CurrentRange = null;
    protected int[] CurrentRangeIndex = null;
    protected String[] CurrentRange_All = new String[0];
    protected int[] I_ConnectType_All_Index = null;
    protected int[] I_ConnectType_CT_Index = null;
    protected int[] I_ConnectType_Realtime_Index = null;
    protected String[] S_ConnectType_All = new String[0];
    protected String[] S_ConnectType_CT = new String[0];
    protected String[] S_ConnectType_Current = new String[0];
    protected String[] S_ConnectType_Realtime = new String[0];
    protected String[] S_ConnectType_Voltage = new String[0];
    protected String[] S_Neutral_Current_Range = new String[0];
    protected String[] S_Neutral_Voltage_Range = new String[0];
    protected List<String[]> VoltageRange = null;
    protected int[] VoltageRangeIndex = null;
    protected String[] VoltageRange_All = new String[0];
    protected int m_AngleDefinition;
    protected Map<Integer, DataModel> m_Data = null;
    protected BaseDictionary m_Dictionary;
    protected int m_VectorDefinition;
    protected int m_VectorType;

    public abstract byte[] AnsErr();

    public abstract byte[] AnsOK();

    public abstract int UnPacket(byte[] bArr);

    /* access modifiers changed from: protected */
    public abstract void initRange();

    public void setAngleDefinition(int AngleDefinition, int VectorDefinition, int VectorType) {
        this.m_AngleDefinition = AngleDefinition;
        this.m_VectorDefinition = VectorDefinition;
        this.m_VectorType = VectorType;
    }

    public int getVectorType() {
        return this.m_VectorType;
    }

    /* access modifiers changed from: protected */
    public BaseDictionary getDictionary() {
        return this.m_Dictionary;
    }

    /* access modifiers changed from: protected */
    public Map<Integer, DataModel> getData() {
        return this.m_Data;
    }

    public List<String[]> getVoltageRange() {
        return this.VoltageRange;
    }

    public List<String[]> getCurrentRange() {
        return this.CurrentRange;
    }

    public int[] getVoltageRangeIndex() {
        return this.VoltageRangeIndex;
    }

    public int[] getCurrentRangeIndex() {
        return this.CurrentRangeIndex;
    }

    public String[] getS_ConnectType_All() {
        return this.S_ConnectType_All;
    }

    public String[] getS_ConnectType_Voltage() {
        return this.S_ConnectType_Voltage;
    }

    public String[] getS_ConnectType_Current() {
        return this.S_ConnectType_Current;
    }

    public int[] getI_ConnectType_All_Index() {
        return this.I_ConnectType_All_Index;
    }

    public String[] getS_ConnectType_Realtime() {
        return this.S_ConnectType_Realtime;
    }

    public int[] getI_ConnectType_Realtime_Index() {
        return this.I_ConnectType_Realtime_Index;
    }

    public String[] getS_ConnectType_CT() {
        return this.S_ConnectType_CT;
    }

    public int[] getI_ConnectType_CT_Index() {
        return this.I_ConnectType_CT_Index;
    }

    public String[] getVoltageRange_All() {
        return this.VoltageRange_All;
    }

    public String[] getCurrentRange_All() {
        return this.CurrentRange_All;
    }

    public String[] getS_Neutral_Voltage_Range() {
        return this.S_Neutral_Voltage_Range;
    }

    public String[] getS_Neutral_Current_Range() {
        return this.S_Neutral_Current_Range;
    }
}
