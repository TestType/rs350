package com.clou.rs350.device.model;

public class Buffer {
    public byte[] ReceiveBuffer = new byte[256];
    public int length = 0;

    public void MergeBuffer(byte[] Buffer) {
        int copyLength = 256 < this.length + Buffer.length ? 256 - this.length : Buffer.length;
        try {
            System.arraycopy(Buffer, 0, this.ReceiveBuffer, this.length, copyLength);
            this.length += copyLength;
        } catch (Exception e) {
        }
    }
}
