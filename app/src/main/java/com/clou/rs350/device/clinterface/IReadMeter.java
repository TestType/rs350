package com.clou.rs350.device.clinterface;

import com.clou.rs350.device.model.ConnectByteModel;
import java.util.List;

public interface IReadMeter {
    void clearReceiveBuffer();

    ConnectByteModel getReceiveModel();

    List<byte[]> sendBuffer(int i, List<byte[]> list);

    byte[] sendBuffer(int i, byte[] bArr);

    List<byte[]> setBaudrate(int i, int i2, int i3, int i4, int i5);
}
