package com.clou.rs350.constants;

public class ConstTag {
    public static final String TAG_BLUETOOTH = "RS350_bluetooth";
    public static final String TAG_BROADCAST = "RS350_broadcast";
    public static final String TAG_SOCKET = "RS350_socket";
    public static final String TAG_VERSION = "RS350_version";
}
