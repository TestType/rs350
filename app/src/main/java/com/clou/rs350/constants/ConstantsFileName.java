package com.clou.rs350.constants;

public class ConstantsFileName {
    public static final String CUSTOMERINFOCSVNAME = "customerinfo.csv";
    public static final String FIELDSETTINGCSVNAME = "fieldsetting.csv";
    public static final String METERTYPECSVNAME = "metertype.csv";
    public static final String OPERATORINFOCSVNAME = "operatorinfo.csv";
    public static final String REGISTERREADINGSCHEMECSVNAME = "registerreadingscheme.csv";
    public static final String SEALSCHEMECSVNAME = "sealscheme.csv";
    public static final String SEQUENCESCHEMECSVNAME = "sequencescheme.csv";
    public static final String SITEDATACSVNAME = "sitedata.csv";
    public static final String VISUALSCHEMECSVNAME = "visualscheme.csv";
}
