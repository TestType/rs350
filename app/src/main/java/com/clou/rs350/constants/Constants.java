package com.clou.rs350.constants;

public class Constants {
    public static final String CALIBARTTIMEFORMAT = "yyyy/MM/dd";
    public static final int DEFAULT_SOCKET_TIMEOUT = 400;
    public static final int DEVICEMODEL_CL3122C = 2;
    public static final int DEVICEMODEL_CL3122E = 1;
    public static final int DEVICEMODEL_CL3122_60A = 3;
    public static final int DEVICEMODEL_RS350 = 0;
    public static final int STATU_CONNECTING = 1;
    public static final int STATU_SERVER_CONNECTED = 3;
    public static final int STATU_SOCKET_CONNECTED = 2;
    public static final int STATU_UNCONNECT = 0;
    public static final int SUPER_MANAGER = 101;
    public static final String TIMEFORMAT = "yyyy/MM/dd HH:mm:ss";
}
