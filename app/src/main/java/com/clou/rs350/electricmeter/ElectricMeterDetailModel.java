package com.clou.rs350.electricmeter;

import com.itextpdf.text.pdf.PdfObject;

public class ElectricMeterDetailModel {
    private int m_DataLength = 0;
    private String m_Format = PdfObject.NOTHING;
    private String m_Name = PdfObject.NOTHING;
    private int m_Ratio = 1;
    private String m_StandardValue = PdfObject.NOTHING;
    private String m_Unit = PdfObject.NOTHING;
    private String m_Value = PdfObject.NOTHING;

    public String getName() {
        return this.m_Name;
    }

    public void setName(String name) {
        this.m_Name = name;
    }

    public String getFormat() {
        return this.m_Format;
    }

    public void setFormat(String format) {
        this.m_Format = format;
        if (this.m_Format.contains(".")) {
            this.m_Ratio = (int) Math.pow(10.0d, (double) ((this.m_Format.length() - this.m_Format.indexOf(".")) - 1));
        }
        String tmpString = this.m_Format.replace(".", PdfObject.NOTHING);
        this.m_DataLength = (tmpString.length() / 2) + (tmpString.length() % 2);
    }

    public String getUnit() {
        return this.m_Unit;
    }

    public void setUnit(String unit) {
        this.m_Unit = unit;
    }

    public String getStandardValue() {
        return this.m_StandardValue;
    }

    public void setStandardValue(String standardValue) {
        this.m_StandardValue = standardValue;
    }

    public String getValue() {
        return this.m_Value;
    }

    public void setValue(String value) {
        this.m_Value = value;
    }

    public int getDataLength() {
        return this.m_DataLength;
    }

    public int getRatio() {
        return this.m_Ratio;
    }
}
