package com.clou.rs350.electricmeter;

import android.text.TextUtils;
import com.clou.rs350.utils.OtherUtils;
import com.itextpdf.text.pdf.PdfObject;
import java.util.ArrayList;
import java.util.List;

public class ElectricMeterDataModel {
    private List<ElectricMeterDetailModel> m_DataModel = new ArrayList();
    private String m_ID = PdfObject.NOTHING;
    private String m_Name = PdfObject.NOTHING;

    public String getID() {
        return this.m_ID;
    }

    public void setID(String ID) {
        this.m_ID = ID;
    }

    public int getIDInt() {
        if (!TextUtils.isEmpty(this.m_ID)) {
            return OtherUtils.parseHexInt(this.m_ID);
        }
        return 0;
    }

    public String getName() {
        return this.m_Name;
    }

    public void setName(String m_Name2) {
        this.m_Name = m_Name2;
    }

    public List<ElectricMeterDetailModel> getDataModel() {
        if (this.m_DataModel == null) {
            this.m_DataModel = new ArrayList();
        }
        return this.m_DataModel;
    }

    public void setDataModel(List<ElectricMeterDetailModel> m_DataModel2) {
        this.m_DataModel = m_DataModel2;
    }

    public void copyTo(ElectricMeterDataModel other) {
        other.m_ID = this.m_ID;
        other.m_Name = this.m_Name;
        other.m_DataModel = new ArrayList();
        other.m_DataModel.addAll(this.m_DataModel);
    }
}
