package com.clou.rs350.electricmeter;

import android.annotation.SuppressLint;
import com.clou.rs350.utils.OtherUtils;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

@SuppressLint({"UseSparseArrays"})
public class ElectricMeterModel {
    private static ElectricMeterModel model;
    Map<Integer, ElectricMeterDataModel> m_DataMap;
    Map<String, List<Integer>> m_GroupToSetting;
    List<String> m_GroupToSettingName;
    Map<String, List<Integer>> m_GroupToShow;
    List<String> m_GroupToShowName;

    private ElectricMeterModel() {
        this.m_DataMap = null;
        this.m_GroupToSetting = null;
        this.m_GroupToShow = null;
        this.m_GroupToSettingName = null;
        this.m_GroupToShowName = null;
        this.m_DataMap = new Hashtable();
        this.m_GroupToSetting = new Hashtable();
        this.m_GroupToShow = new Hashtable();
        this.m_GroupToSettingName = new ArrayList();
        this.m_GroupToShowName = new ArrayList();
    }

    public static ElectricMeterModel getInstance() {
        if (model == null) {
            model = new ElectricMeterModel();
        }
        return model;
    }

    public static ElectricMeterModel newInstance() {
        return new ElectricMeterModel();
    }

    public void loadXMLData(String protocol) {
    }

    public Map<Integer, ElectricMeterDataModel> getM_DataMap() {
        return this.m_DataMap;
    }

    public ElectricMeterDataModel getM_DataModel(String identifying) {
        return this.m_DataMap.get(Integer.valueOf(OtherUtils.parseHexInt(identifying)));
    }

    public ElectricMeterDataModel getM_DataModel(int key) {
        return this.m_DataMap.get(Integer.valueOf(key));
    }

    public void setM_DataMap(Map<Integer, ElectricMeterDataModel> m_DataMap2) {
        this.m_DataMap = m_DataMap2;
    }

    public Map<String, List<Integer>> getM_Group() {
        return this.m_GroupToSetting;
    }

    public void setM_Group(Map<String, List<Integer>> m_Group) {
        this.m_GroupToSetting = m_Group;
    }

    public Map<String, List<Integer>> getM_GroupToShow() {
        return this.m_GroupToShow;
    }

    public void setM_GroupToShow(Map<String, List<Integer>> m_GroupToShow2) {
        this.m_GroupToShow = m_GroupToShow2;
    }

    public List<String> getM_GroupName() {
        return this.m_GroupToSettingName;
    }

    public void setM_GroupName(List<String> m_GroupName) {
        this.m_GroupToSettingName = m_GroupName;
    }

    public List<String> getM_GroupToShowName() {
        return this.m_GroupToShowName;
    }

    public void setM_GroupToShowName(List<String> m_GroupToShowName2) {
        this.m_GroupToShowName = m_GroupToShowName2;
    }
}
