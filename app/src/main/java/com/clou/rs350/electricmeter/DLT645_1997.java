package com.clou.rs350.electricmeter;

import com.clou.rs350.device.constants.CLTcmd;
import com.clou.rs350.device.utils.BufferUtil;
import com.clou.rs350.electricmeter.DLT645;
import com.itextpdf.text.DocWriter;
import com.itextpdf.text.pdf.PdfObject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DLT645_1997 implements IElectricMeter {
    private final byte CMD_READ_ADDRESS;
    private final byte CMD_READ_CONTINUE;
    private final byte CMD_READ_DATA;
    DLT645 m_DLT645;
    private Map<Integer, ElectricMeterDataModel> m_DataMap;
    private byte[] m_Key;
    private int m_ReadIndex;

    public DLT645_1997() {
        this.CMD_READ_DATA = 1;
        this.CMD_READ_CONTINUE = 2;
        this.CMD_READ_ADDRESS = 19;
        this.m_ReadIndex = 0;
        this.m_DataMap = null;
        this.m_Key = new byte[2];
        this.m_DLT645 = new DLT645();
        this.m_DataMap = new HashMap();
        ElectricMeterDataModel tmpModel = this.m_DLT645.ProduceData("C010", "日期及星期", "YYMMDDWW", "年月日星期", PdfObject.NOTHING);
        this.m_DataMap.put(Integer.valueOf(tmpModel.getIDInt()), tmpModel);
        ElectricMeterDataModel tmpModel2 = this.m_DLT645.ProduceData("C011", "时间", "hhmmss", "时分秒", PdfObject.NOTHING);
        this.m_DataMap.put(Integer.valueOf(tmpModel2.getIDInt()), tmpModel2);
        ElectricMeterDataModel tmpModel3 = this.m_DLT645.ProduceData("C032", "表号", "NNNNNNNNNNNN", PdfObject.NOTHING, PdfObject.NOTHING);
        this.m_DataMap.put(Integer.valueOf(tmpModel3.getIDInt()), tmpModel3);
        ElectricMeterDataModel tmpModel4 = this.m_DLT645.ProduceData("9010", "正向有功总电能", "XXXXXX.XX", "kWh", PdfObject.NOTHING);
        this.m_DataMap.put(Integer.valueOf(tmpModel4.getIDInt()), tmpModel4);
        ElectricMeterDataModel tmpModel5 = this.m_DLT645.ProduceData("9011", "正向有功费率1电能", "XXXXXX.XX", "kWh", PdfObject.NOTHING);
        this.m_DataMap.put(Integer.valueOf(tmpModel5.getIDInt()), tmpModel5);
        ElectricMeterDataModel tmpModel6 = this.m_DLT645.ProduceData("9012", "正向有功费率2电能", "XXXXXX.XX", "kWh", PdfObject.NOTHING);
        this.m_DataMap.put(Integer.valueOf(tmpModel6.getIDInt()), tmpModel6);
        ElectricMeterDataModel tmpModel7 = this.m_DLT645.ProduceData("9013", "正向有功费率3电能", "XXXXXX.XX", "kWh", PdfObject.NOTHING);
        this.m_DataMap.put(Integer.valueOf(tmpModel7.getIDInt()), tmpModel7);
        ElectricMeterDataModel tmpModel8 = this.m_DLT645.ProduceData("9014", "正向有功费率4电能", "XXXXXX.XX", "kWh", PdfObject.NOTHING);
        this.m_DataMap.put(Integer.valueOf(tmpModel8.getIDInt()), tmpModel8);
        ElectricMeterDataModel tmpModel9 = this.m_DLT645.ProduceData("9020", "反向有功总电能", "XXXXXX.XX", "kWh", PdfObject.NOTHING);
        this.m_DataMap.put(Integer.valueOf(tmpModel9.getIDInt()), tmpModel9);
        ElectricMeterDataModel tmpModel10 = this.m_DLT645.ProduceData("9021", "反向有功费率1电能", "XXXXXX.XX", "kWh", PdfObject.NOTHING);
        this.m_DataMap.put(Integer.valueOf(tmpModel10.getIDInt()), tmpModel10);
        ElectricMeterDataModel tmpModel11 = this.m_DLT645.ProduceData("9022", "反向有功费率2电能", "XXXXXX.XX", "kWh", PdfObject.NOTHING);
        this.m_DataMap.put(Integer.valueOf(tmpModel11.getIDInt()), tmpModel11);
        ElectricMeterDataModel tmpModel12 = this.m_DLT645.ProduceData("9023", "反向有功费率3电能", "XXXXXX.XX", "kWh", PdfObject.NOTHING);
        this.m_DataMap.put(Integer.valueOf(tmpModel12.getIDInt()), tmpModel12);
        ElectricMeterDataModel tmpModel13 = this.m_DLT645.ProduceData("9024", "反向有功费率4电能", "XXXXXX.XX", "kWh", PdfObject.NOTHING);
        this.m_DataMap.put(Integer.valueOf(tmpModel13.getIDInt()), tmpModel13);
        ElectricMeterDataModel tmpModel14 = this.m_DLT645.ProduceData("9110", "组合无功1总电能", "XXXXXX.XX", "kvarh", PdfObject.NOTHING);
        this.m_DataMap.put(Integer.valueOf(tmpModel14.getIDInt()), tmpModel14);
        ElectricMeterDataModel tmpModel15 = this.m_DLT645.ProduceData("9111", "组合无功1费率1电能", "XXXXXX.XX", "kvarh", PdfObject.NOTHING);
        this.m_DataMap.put(Integer.valueOf(tmpModel15.getIDInt()), tmpModel15);
        ElectricMeterDataModel tmpModel16 = this.m_DLT645.ProduceData("9112", "组合无功1费率2电能", "XXXXXX.XX", "kvarh", PdfObject.NOTHING);
        this.m_DataMap.put(Integer.valueOf(tmpModel16.getIDInt()), tmpModel16);
        ElectricMeterDataModel tmpModel17 = this.m_DLT645.ProduceData("9113", "组合无功1费率3电能", "XXXXXX.XX", "kvarh", PdfObject.NOTHING);
        this.m_DataMap.put(Integer.valueOf(tmpModel17.getIDInt()), tmpModel17);
        ElectricMeterDataModel tmpModel18 = this.m_DLT645.ProduceData("9114", "组合无功1费率4电能", "XXXXXX.XX", "kvarh", PdfObject.NOTHING);
        this.m_DataMap.put(Integer.valueOf(tmpModel18.getIDInt()), tmpModel18);
        ElectricMeterDataModel tmpModel19 = this.m_DLT645.ProduceData("9120", "组合无功2总电能", "XXXXXX.XX", "kvarh", PdfObject.NOTHING);
        this.m_DataMap.put(Integer.valueOf(tmpModel19.getIDInt()), tmpModel19);
        ElectricMeterDataModel tmpModel20 = this.m_DLT645.ProduceData("9121", "组合无功2费率1电能", "XXXXXX.XX", "kvarh", PdfObject.NOTHING);
        this.m_DataMap.put(Integer.valueOf(tmpModel20.getIDInt()), tmpModel20);
        ElectricMeterDataModel tmpModel21 = this.m_DLT645.ProduceData("9122", "组合无功2费率2电能", "XXXXXX.XX", "kvarh", PdfObject.NOTHING);
        this.m_DataMap.put(Integer.valueOf(tmpModel21.getIDInt()), tmpModel21);
        ElectricMeterDataModel tmpModel22 = this.m_DLT645.ProduceData("9123", "组合无功2费率3电能", "XXXXXX.XX", "kvarh", PdfObject.NOTHING);
        this.m_DataMap.put(Integer.valueOf(tmpModel22.getIDInt()), tmpModel22);
        ElectricMeterDataModel tmpModel23 = this.m_DLT645.ProduceData("9124", "组合无功2费率4电能", "XXXXXX.XX", "kvarh", PdfObject.NOTHING);
        this.m_DataMap.put(Integer.valueOf(tmpModel23.getIDInt()), tmpModel23);
        ElectricMeterDataModel tmpModel24 = this.m_DLT645.ProduceData("B611", "A相电压", "XXX", "V", PdfObject.NOTHING);
        this.m_DataMap.put(Integer.valueOf(tmpModel24.getIDInt()), tmpModel24);
        ElectricMeterDataModel tmpModel25 = this.m_DLT645.ProduceData("B612", "B相电压", "XXX", "V", PdfObject.NOTHING);
        this.m_DataMap.put(Integer.valueOf(tmpModel25.getIDInt()), tmpModel25);
        ElectricMeterDataModel tmpModel26 = this.m_DLT645.ProduceData("B613", "C相电压", "XXX", "V", PdfObject.NOTHING);
        this.m_DataMap.put(Integer.valueOf(tmpModel26.getIDInt()), tmpModel26);
        ElectricMeterDataModel tmpModel27 = this.m_DLT645.ProduceData("B621", "A相电流", "XX.XX", "A", PdfObject.NOTHING);
        this.m_DataMap.put(Integer.valueOf(tmpModel27.getIDInt()), tmpModel27);
        ElectricMeterDataModel tmpModel28 = this.m_DLT645.ProduceData("B622", "B相电流", "XX.XX", "A", PdfObject.NOTHING);
        this.m_DataMap.put(Integer.valueOf(tmpModel28.getIDInt()), tmpModel28);
        ElectricMeterDataModel tmpModel29 = this.m_DLT645.ProduceData("B623", "C相电流", "XX.XX", "A", PdfObject.NOTHING);
        this.m_DataMap.put(Integer.valueOf(tmpModel29.getIDInt()), tmpModel29);
        ElectricMeterDataModel tmpModel30 = this.m_DLT645.ProduceData("B630", "瞬时总有功功率", "XX.XXXX", "kW", PdfObject.NOTHING);
        this.m_DataMap.put(Integer.valueOf(tmpModel30.getIDInt()), tmpModel30);
        ElectricMeterDataModel tmpModel31 = this.m_DLT645.ProduceData("B631", "瞬时A相有功功率", "XX.XXXX", "kW", PdfObject.NOTHING);
        this.m_DataMap.put(Integer.valueOf(tmpModel31.getIDInt()), tmpModel31);
        ElectricMeterDataModel tmpModel32 = this.m_DLT645.ProduceData("B632", "瞬时B相有功功率", "XX.XXXX", "kW", PdfObject.NOTHING);
        this.m_DataMap.put(Integer.valueOf(tmpModel32.getIDInt()), tmpModel32);
        ElectricMeterDataModel tmpModel33 = this.m_DLT645.ProduceData("B633", "瞬时C相有功功率", "XX.XXXX", "kW", PdfObject.NOTHING);
        this.m_DataMap.put(Integer.valueOf(tmpModel33.getIDInt()), tmpModel33);
        ElectricMeterDataModel tmpModel34 = this.m_DLT645.ProduceData("B640", "瞬时总无功功率", "XX.XX", "kvar", PdfObject.NOTHING);
        this.m_DataMap.put(Integer.valueOf(tmpModel34.getIDInt()), tmpModel34);
        ElectricMeterDataModel tmpModel35 = this.m_DLT645.ProduceData("B641", "瞬时A相无功功率", "XX.XX", "kvar", PdfObject.NOTHING);
        this.m_DataMap.put(Integer.valueOf(tmpModel35.getIDInt()), tmpModel35);
        ElectricMeterDataModel tmpModel36 = this.m_DLT645.ProduceData("B642", "瞬时B相无功功率", "XX.XX", "kvar", PdfObject.NOTHING);
        this.m_DataMap.put(Integer.valueOf(tmpModel36.getIDInt()), tmpModel36);
        ElectricMeterDataModel tmpModel37 = this.m_DLT645.ProduceData("B643", "瞬时C相无功功率", "XX.XX", "kvar", PdfObject.NOTHING);
        this.m_DataMap.put(Integer.valueOf(tmpModel37.getIDInt()), tmpModel37);
        ElectricMeterDataModel tmpModel38 = this.m_DLT645.ProduceData("B650", "总功率因数", "X.XXX", "*", PdfObject.NOTHING);
        this.m_DataMap.put(Integer.valueOf(tmpModel38.getIDInt()), tmpModel38);
        ElectricMeterDataModel tmpModel39 = this.m_DLT645.ProduceData("B651", "A相功率因数", "X.XXX", "*", PdfObject.NOTHING);
        this.m_DataMap.put(Integer.valueOf(tmpModel39.getIDInt()), tmpModel39);
        ElectricMeterDataModel tmpModel40 = this.m_DLT645.ProduceData("B652", "B相功率因数", "X.XXX", "*", PdfObject.NOTHING);
        this.m_DataMap.put(Integer.valueOf(tmpModel40.getIDInt()), tmpModel40);
        ElectricMeterDataModel tmpModel41 = this.m_DLT645.ProduceData("B653", "C相功率因数", "X.XXX", "*", PdfObject.NOTHING);
        this.m_DataMap.put(Integer.valueOf(tmpModel41.getIDInt()), tmpModel41);
    }

    @Override // com.clou.rs350.electricmeter.IElectricMeter
    public String GetAddress() {
        return BufferUtil.toHexString(this.m_DLT645.MeterAddress);
    }

    @Override // com.clou.rs350.electricmeter.IElectricMeter
    public void SetAddress(String address) {
        this.m_DLT645.MeterAddress = new byte[]{CLTcmd.WRTMEM, CLTcmd.WRTMEM, CLTcmd.WRTMEM, CLTcmd.WRTMEM, CLTcmd.WRTMEM, CLTcmd.WRTMEM};
        int i = 0;
        while (i < address.length() / 2) {
            this.m_DLT645.MeterAddress[i] = BufferUtil.intToBCD(Byte.valueOf(address.substring(address.length() - ((i + 1) * 2), address.length() - (i * 2))).byteValue());
            i++;
        }
        if (address.length() % 2 != 0) {
            this.m_DLT645.MeterAddress[i] = (byte) (Byte.valueOf(address.substring(0, 1)).byteValue() + CLTcmd.ASKDAT);
        }
    }

    @Override // com.clou.rs350.electricmeter.IElectricMeter
    public Map<Integer, ElectricMeterDataModel> getDataMap() {
        return this.m_DataMap;
    }

    @Override // com.clou.rs350.electricmeter.IElectricMeter
    public void setDataMap(Map<Integer, ElectricMeterDataModel> m_DataMap2) {
        this.m_DataMap = m_DataMap2;
    }

    @Override // com.clou.rs350.electricmeter.IElectricMeter
    public List<byte[]> WakeUp() {
        List<byte[]> buffers = new ArrayList<>();
        buffers.add(new byte[]{-2, -2, -2, -2});
        return buffers;
    }

    @Override // com.clou.rs350.electricmeter.IElectricMeter
    public List<byte[]> ReadData(int[] key) {
        List<byte[]> buffers = new ArrayList<>();
        for (int i = 0; i < key.length; i++) {
            buffers.add(this.m_DLT645.packet((byte) 1, new byte[]{(byte) ((key[i] & 255) + 51), (byte) (((key[i] >> 8) & 255) + 51)}));
        }
        return buffers;
    }

    @Override // com.clou.rs350.electricmeter.IElectricMeter
    public List<byte[]> ReadAddress() {
        this.m_DLT645.MeterAddress = new byte[]{-103, -103, -103, -103, -103, -103};
        return ReadData(new int[]{49202});
    }

    @Override // com.clou.rs350.electricmeter.IElectricMeter
    public byte[] ReadContinue() {
        return this.m_DLT645.packet((byte) 2, this.m_Key);
    }

    @Override // com.clou.rs350.electricmeter.IElectricMeter
    public int Unpacket(byte[] buffer) {
        return this.m_DLT645.unPacket(buffer, new DLT645.IUnPacket() {
            /* class com.clou.rs350.electricmeter.DLT645_1997.AnonymousClass1 */

            @Override // com.clou.rs350.electricmeter.DLT645.IUnPacket
            public int unPacket(byte[] buffer) {
                boolean hasContinue;
                int result = 0;
                if ((buffer[8] & DocWriter.SPACE) == 32) {
                    hasContinue = true;
                } else {
                    hasContinue = false;
                }
                System.arraycopy(buffer, 10, DLT645_1997.this.m_Key, 0, 2);
                switch (buffer[8] & 31) {
                    case 1:
                    case 2:
                        result = DLT645_1997.this.unpacketReadData(buffer);
                        break;
                }
                if (hasContinue) {
                    return 3;
                }
                DLT645_1997.this.m_ReadIndex = 0;
                return result;
            }
        });
    }

    public int unpacketReadData(byte[] buffer) {
        System.arraycopy(buffer, 10, this.m_Key, 0, 2);
        int key = ((buffer[10] & 255) - 51) + (((buffer[11] & 255) - 51) << 8);
        int length = buffer[9] + 10;
        int i = 12;
        while (i < length) {
            if (170 == buffer[i]) {
                i++;
                this.m_ReadIndex++;
            } else {
                int DataLength = this.m_DataMap.get(Integer.valueOf(key)).getDataModel().get(this.m_ReadIndex).getDataLength();
                byte[] b_Data = new byte[DataLength];
                System.arraycopy(buffer, i, b_Data, 0, DataLength);
                this.m_DataMap.get(Integer.valueOf(key)).getDataModel().get(this.m_ReadIndex).setValue(this.m_DLT645.Explean(this.m_DataMap.get(Integer.valueOf(key)).getDataModel().get(this.m_ReadIndex), b_Data));
                i += DataLength + 1;
                this.m_ReadIndex++;
            }
        }
        return 1;
    }

    @Override // com.clou.rs350.electricmeter.IElectricMeter
    public List<byte[]> ReadTime() {
        return ReadData(new int[]{49168, 49169});
    }

    @Override // com.clou.rs350.electricmeter.IElectricMeter
    public List<byte[]> ReadBasicData() {
        return ReadData(new int[]{36880, 36881, 36882, 36883, 36884, 36896, 36897, 36898, 36899, 36900, 37136, 37137, 37138, 37139, 37140, 37152, 37153, 37154, 37155, 37156, 46640, 46656, 46609, 46610, 46611, 46625, 46626, 46627});
    }

    @Override // com.clou.rs350.electricmeter.IElectricMeter
    public String GetDateAndTime() {
        return String.valueOf(this.m_DataMap.get(49168).getDataModel().get(0).getValue()) + " " + this.m_DataMap.get(49169).getDataModel().get(0).getValue();
    }

    @Override // com.clou.rs350.electricmeter.IElectricMeter
    public String GetTime() {
        return this.m_DataMap.get(49169).getDataModel().get(0).getValue();
    }

    @Override // com.clou.rs350.electricmeter.IElectricMeter
    public String[] getBasicData() {
        int[] tmpKey = {46609, 46610, 46611, 46625, 46626, 46627, 46640, 46656, 36880, 36881, 36882, 36883, 36884, 36896, 36897, 36898, 36899, 36900, 37136, 37137, 37138, 37139, 37140, 37152, 37153, 37154, 37155, 37156};
        String[] tmpString = new String[28];
        for (int i = 0; i < tmpString.length; i++) {
            ElectricMeterDetailModel tmpModel = this.m_DataMap.get(Integer.valueOf(tmpKey[i])).getDataModel().get(0);
            tmpString[i] = String.valueOf(tmpModel.getValue()) + tmpModel.getUnit();
        }
        return tmpString;
    }
}
