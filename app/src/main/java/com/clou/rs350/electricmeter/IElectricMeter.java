package com.clou.rs350.electricmeter;

import java.util.List;
import java.util.Map;

public interface IElectricMeter {
    String GetAddress();

    String GetDateAndTime();

    String GetTime();

    List<byte[]> ReadAddress();

    List<byte[]> ReadBasicData();

    byte[] ReadContinue();

    List<byte[]> ReadData(int[] iArr);

    List<byte[]> ReadTime();

    void SetAddress(String str);

    int Unpacket(byte[] bArr);

    List<byte[]> WakeUp();

    String[] getBasicData();

    Map<Integer, ElectricMeterDataModel> getDataMap();

    void setDataMap(Map<Integer, ElectricMeterDataModel> map);
}
