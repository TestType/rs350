package com.clou.rs350.electricmeter;

import com.clou.rs350.device.constants.CLTcmd;
import com.clou.rs350.device.utils.BufferUtil;
import com.clou.rs350.electricmeter.DLT645;
import com.itextpdf.text.DocWriter;
import com.itextpdf.text.pdf.PdfObject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DLT645_2007 implements IElectricMeter {
    private final byte CMD_READ_ADDRESS;
    private final byte CMD_READ_CONTINUE;
    private final byte CMD_READ_DATA;
    private final byte CMD_TIME_CORRECTION;
    private int m_BufferIndex;
    DLT645 m_DLT645;
    private Map<Integer, ElectricMeterDataModel> m_DataMap;
    private byte[] m_Key;
    private int m_ReadIndex;

    public DLT645_2007() {
        this.CMD_READ_DATA = 17;
        this.CMD_READ_CONTINUE = 18;
        this.CMD_READ_ADDRESS = 19;
        this.CMD_TIME_CORRECTION = 8;
        this.m_ReadIndex = 0;
        this.m_BufferIndex = 0;
        this.m_Key = new byte[4];
        this.m_DataMap = null;
        this.m_DLT645 = new DLT645();
        this.m_DataMap = new HashMap();
        ElectricMeterDataModel tmpModel = this.m_DLT645.ProduceData("04000101", "日期及星期", "YYMMDDWW", "年月日星期", PdfObject.NOTHING);
        this.m_DataMap.put(Integer.valueOf(tmpModel.getIDInt()), tmpModel);
        ElectricMeterDataModel tmpModel2 = this.m_DLT645.ProduceData("04000102", "时间", "hhmmss", "时分秒", PdfObject.NOTHING);
        this.m_DataMap.put(Integer.valueOf(tmpModel2.getIDInt()), tmpModel2);
        ElectricMeterDataModel tmpModel3 = this.m_DLT645.ProduceData("00010000", "正向有功总电能", "XXXXXX.XX", "kWh", PdfObject.NOTHING);
        this.m_DataMap.put(Integer.valueOf(tmpModel3.getIDInt()), tmpModel3);
        ElectricMeterDataModel tmpModel4 = this.m_DLT645.ProduceData("00010100", "正向有功费率1电能", "XXXXXX.XX", "kWh", PdfObject.NOTHING);
        this.m_DataMap.put(Integer.valueOf(tmpModel4.getIDInt()), tmpModel4);
        ElectricMeterDataModel tmpModel5 = this.m_DLT645.ProduceData("00010200", "正向有功费率2电能", "XXXXXX.XX", "kWh", PdfObject.NOTHING);
        this.m_DataMap.put(Integer.valueOf(tmpModel5.getIDInt()), tmpModel5);
        ElectricMeterDataModel tmpModel6 = this.m_DLT645.ProduceData("00010300", "正向有功费率3电能", "XXXXXX.XX", "kWh", PdfObject.NOTHING);
        this.m_DataMap.put(Integer.valueOf(tmpModel6.getIDInt()), tmpModel6);
        ElectricMeterDataModel tmpModel7 = this.m_DLT645.ProduceData("00010400", "正向有功费率4电能", "XXXXXX.XX", "kWh", PdfObject.NOTHING);
        this.m_DataMap.put(Integer.valueOf(tmpModel7.getIDInt()), tmpModel7);
        ElectricMeterDataModel tmpModel8 = this.m_DLT645.ProduceData("00020000", "反向有功总电能", "XXXXXX.XX", "kWh", PdfObject.NOTHING);
        this.m_DataMap.put(Integer.valueOf(tmpModel8.getIDInt()), tmpModel8);
        ElectricMeterDataModel tmpModel9 = this.m_DLT645.ProduceData("00020100", "反向有功费率1电能", "XXXXXX.XX", "kWh", PdfObject.NOTHING);
        this.m_DataMap.put(Integer.valueOf(tmpModel9.getIDInt()), tmpModel9);
        ElectricMeterDataModel tmpModel10 = this.m_DLT645.ProduceData("00020200", "反向有功费率2电能", "XXXXXX.XX", "kWh", PdfObject.NOTHING);
        this.m_DataMap.put(Integer.valueOf(tmpModel10.getIDInt()), tmpModel10);
        ElectricMeterDataModel tmpModel11 = this.m_DLT645.ProduceData("00020300", "反向有功费率3电能", "XXXXXX.XX", "kWh", PdfObject.NOTHING);
        this.m_DataMap.put(Integer.valueOf(tmpModel11.getIDInt()), tmpModel11);
        ElectricMeterDataModel tmpModel12 = this.m_DLT645.ProduceData("00020400", "反向有功费率4电能", "XXXXXX.XX", "kWh", PdfObject.NOTHING);
        this.m_DataMap.put(Integer.valueOf(tmpModel12.getIDInt()), tmpModel12);
        ElectricMeterDataModel tmpModel13 = this.m_DLT645.ProduceData("00030000", "组合无功1总电能", "XXXXXX.XX", "kvarh", PdfObject.NOTHING);
        this.m_DataMap.put(Integer.valueOf(tmpModel13.getIDInt()), tmpModel13);
        ElectricMeterDataModel tmpModel14 = this.m_DLT645.ProduceData("00030100", "组合无功1费率1电能", "XXXXXX.XX", "kvarh", PdfObject.NOTHING);
        this.m_DataMap.put(Integer.valueOf(tmpModel14.getIDInt()), tmpModel14);
        ElectricMeterDataModel tmpModel15 = this.m_DLT645.ProduceData("00030200", "组合无功1费率2电能", "XXXXXX.XX", "kvarh", PdfObject.NOTHING);
        this.m_DataMap.put(Integer.valueOf(tmpModel15.getIDInt()), tmpModel15);
        ElectricMeterDataModel tmpModel16 = this.m_DLT645.ProduceData("00030300", "组合无功1费率3电能", "XXXXXX.XX", "kvarh", PdfObject.NOTHING);
        this.m_DataMap.put(Integer.valueOf(tmpModel16.getIDInt()), tmpModel16);
        ElectricMeterDataModel tmpModel17 = this.m_DLT645.ProduceData("00030400", "组合无功1费率4电能", "XXXXXX.XX", "kvarh", PdfObject.NOTHING);
        this.m_DataMap.put(Integer.valueOf(tmpModel17.getIDInt()), tmpModel17);
        ElectricMeterDataModel tmpModel18 = this.m_DLT645.ProduceData("00040000", "组合无功2总电能", "XXXXXX.XX", "kvarh", PdfObject.NOTHING);
        this.m_DataMap.put(Integer.valueOf(tmpModel18.getIDInt()), tmpModel18);
        ElectricMeterDataModel tmpModel19 = this.m_DLT645.ProduceData("00040100", "组合无功2费率1电能", "XXXXXX.XX", "kvarh", PdfObject.NOTHING);
        this.m_DataMap.put(Integer.valueOf(tmpModel19.getIDInt()), tmpModel19);
        ElectricMeterDataModel tmpModel20 = this.m_DLT645.ProduceData("00040200", "组合无功2费率2电能", "XXXXXX.XX", "kvarh", PdfObject.NOTHING);
        this.m_DataMap.put(Integer.valueOf(tmpModel20.getIDInt()), tmpModel20);
        ElectricMeterDataModel tmpModel21 = this.m_DLT645.ProduceData("00040300", "组合无功2费率3电能", "XXXXXX.XX", "kvarh", PdfObject.NOTHING);
        this.m_DataMap.put(Integer.valueOf(tmpModel21.getIDInt()), tmpModel21);
        ElectricMeterDataModel tmpModel22 = this.m_DLT645.ProduceData("00040400", "组合无功2费率4电能", "XXXXXX.XX", "kvarh", PdfObject.NOTHING);
        this.m_DataMap.put(Integer.valueOf(tmpModel22.getIDInt()), tmpModel22);
        ElectricMeterDataModel tmpModel23 = this.m_DLT645.ProduceData("02010100", "A相电压", "XXX.X", "V", PdfObject.NOTHING);
        this.m_DataMap.put(Integer.valueOf(tmpModel23.getIDInt()), tmpModel23);
        ElectricMeterDataModel tmpModel24 = this.m_DLT645.ProduceData("02010200", "B相电压", "XXX.X", "V", PdfObject.NOTHING);
        this.m_DataMap.put(Integer.valueOf(tmpModel24.getIDInt()), tmpModel24);
        ElectricMeterDataModel tmpModel25 = this.m_DLT645.ProduceData("02010300", "C相电压", "XXX.X", "V", PdfObject.NOTHING);
        this.m_DataMap.put(Integer.valueOf(tmpModel25.getIDInt()), tmpModel25);
        ElectricMeterDataModel tmpModel26 = this.m_DLT645.ProduceData("02020100", "A相电流", "XXX.XXX", "A", PdfObject.NOTHING);
        this.m_DataMap.put(Integer.valueOf(tmpModel26.getIDInt()), tmpModel26);
        ElectricMeterDataModel tmpModel27 = this.m_DLT645.ProduceData("02020200", "B相电流", "XXX.XXX", "A", PdfObject.NOTHING);
        this.m_DataMap.put(Integer.valueOf(tmpModel27.getIDInt()), tmpModel27);
        ElectricMeterDataModel tmpModel28 = this.m_DLT645.ProduceData("02020300", "C相电流", "XXX.XXX", "A", PdfObject.NOTHING);
        this.m_DataMap.put(Integer.valueOf(tmpModel28.getIDInt()), tmpModel28);
        ElectricMeterDataModel tmpModel29 = this.m_DLT645.ProduceData("02030000", "瞬时总有功功率", "XX.XXXX", "kW", PdfObject.NOTHING);
        this.m_DataMap.put(Integer.valueOf(tmpModel29.getIDInt()), tmpModel29);
        ElectricMeterDataModel tmpModel30 = this.m_DLT645.ProduceData("02030100", "瞬时A相有功功率", "XX.XXXX", "kW", PdfObject.NOTHING);
        this.m_DataMap.put(Integer.valueOf(tmpModel30.getIDInt()), tmpModel30);
        ElectricMeterDataModel tmpModel31 = this.m_DLT645.ProduceData("02030200", "瞬时B相有功功率", "XX.XXXX", "kW", PdfObject.NOTHING);
        this.m_DataMap.put(Integer.valueOf(tmpModel31.getIDInt()), tmpModel31);
        ElectricMeterDataModel tmpModel32 = this.m_DLT645.ProduceData("02030300", "瞬时C相有功功率", "XX.XXXX", "kW", PdfObject.NOTHING);
        this.m_DataMap.put(Integer.valueOf(tmpModel32.getIDInt()), tmpModel32);
        ElectricMeterDataModel tmpModel33 = this.m_DLT645.ProduceData("02040000", "瞬时总无功功率", "XX.XXXX", "kvar", PdfObject.NOTHING);
        this.m_DataMap.put(Integer.valueOf(tmpModel33.getIDInt()), tmpModel33);
        ElectricMeterDataModel tmpModel34 = this.m_DLT645.ProduceData("02040100", "瞬时A相无功功率", "XX.XXXX", "kvar", PdfObject.NOTHING);
        this.m_DataMap.put(Integer.valueOf(tmpModel34.getIDInt()), tmpModel34);
        ElectricMeterDataModel tmpModel35 = this.m_DLT645.ProduceData("02040200", "瞬时B相无功功率", "XX.XXXX", "kvar", PdfObject.NOTHING);
        this.m_DataMap.put(Integer.valueOf(tmpModel35.getIDInt()), tmpModel35);
        ElectricMeterDataModel tmpModel36 = this.m_DLT645.ProduceData("02040300", "瞬时C相无功功率", "XX.XXXX", "kvar", PdfObject.NOTHING);
        this.m_DataMap.put(Integer.valueOf(tmpModel36.getIDInt()), tmpModel36);
        ElectricMeterDataModel tmpModel37 = this.m_DLT645.ProduceData("02050000", "瞬时总视在功率", "XX.XXXX", "kVA", PdfObject.NOTHING);
        this.m_DataMap.put(Integer.valueOf(tmpModel37.getIDInt()), tmpModel37);
        ElectricMeterDataModel tmpModel38 = this.m_DLT645.ProduceData("02050100", "瞬时A相视在功率", "XX.XXXX", "kVA", PdfObject.NOTHING);
        this.m_DataMap.put(Integer.valueOf(tmpModel38.getIDInt()), tmpModel38);
        ElectricMeterDataModel tmpModel39 = this.m_DLT645.ProduceData("02050200", "瞬时B相视在功率", "XX.XXXX", "kVA", PdfObject.NOTHING);
        this.m_DataMap.put(Integer.valueOf(tmpModel39.getIDInt()), tmpModel39);
        ElectricMeterDataModel tmpModel40 = this.m_DLT645.ProduceData("02050300", "瞬时C相视在功率", "XX.XXXX", "kVA", PdfObject.NOTHING);
        this.m_DataMap.put(Integer.valueOf(tmpModel40.getIDInt()), tmpModel40);
        ElectricMeterDataModel tmpModel41 = this.m_DLT645.ProduceData("02060000", "总功率因数", "X.XXX", "*", PdfObject.NOTHING);
        this.m_DataMap.put(Integer.valueOf(tmpModel41.getIDInt()), tmpModel41);
        ElectricMeterDataModel tmpModel42 = this.m_DLT645.ProduceData("02060100", "A相功率因数", "X.XXX", "*", PdfObject.NOTHING);
        this.m_DataMap.put(Integer.valueOf(tmpModel42.getIDInt()), tmpModel42);
        ElectricMeterDataModel tmpModel43 = this.m_DLT645.ProduceData("02060200", "B相功率因数", "X.XXX", "*", PdfObject.NOTHING);
        this.m_DataMap.put(Integer.valueOf(tmpModel43.getIDInt()), tmpModel43);
        ElectricMeterDataModel tmpModel44 = this.m_DLT645.ProduceData("02060300", "C相功率因数", "X.XXX", "*", PdfObject.NOTHING);
        this.m_DataMap.put(Integer.valueOf(tmpModel44.getIDInt()), tmpModel44);
        ElectricMeterDataModel tmpModel45 = this.m_DLT645.ProduceData("02800002", "电网频率", "XX.XX", "*", PdfObject.NOTHING);
        this.m_DataMap.put(Integer.valueOf(tmpModel45.getIDInt()), tmpModel45);
        ElectricMeterDataModel tmpModel46 = this.m_DLT645.ProduceData("03010000", new MeterDataNames[]{new MeterDataNames("A相失压总次数", "XXXXXX", "*", PdfObject.NOTHING), new MeterDataNames("总累计时间", "XXXXXX", "*", PdfObject.NOTHING), new MeterDataNames("B相失压总次数", "XXXXXX", "*", PdfObject.NOTHING), new MeterDataNames("总累计时间", "XXXXXX", "*", PdfObject.NOTHING), new MeterDataNames("C相失压总次数", "XXXXXX", "*", PdfObject.NOTHING), new MeterDataNames("总累计时间", "XXXXXX", "*", PdfObject.NOTHING)});
        this.m_DataMap.put(Integer.valueOf(tmpModel46.getIDInt()), tmpModel46);
        ElectricMeterDataModel tmpModel47 = this.m_DLT645.ProduceData("03010101", new MeterDataNames[]{new MeterDataNames("A相失压发生时刻", "YYMMDDhhmmss", "*", PdfObject.NOTHING), new MeterDataNames("A相失压结束时刻", "YYMMDDhhmmss", "*", PdfObject.NOTHING), new MeterDataNames("A相失压正向有功", "XXXXXX.XX", "kWh", PdfObject.NOTHING), new MeterDataNames("A相失压反向有功", "XXXXXX.XX", "kWh", PdfObject.NOTHING)});
        this.m_DataMap.put(Integer.valueOf(tmpModel47.getIDInt()), tmpModel47);
        ElectricMeterDataModel tmpModel48 = this.m_DLT645.ProduceData("03020000", new MeterDataNames[]{new MeterDataNames("A相欠压总次数", "XXXXXX", "*", PdfObject.NOTHING), new MeterDataNames("总累计时间", "XXXXXX", "*", PdfObject.NOTHING), new MeterDataNames("B相欠压总次数", "XXXXXX", "*", PdfObject.NOTHING), new MeterDataNames("总累计时间", "XXXXXX", "*", PdfObject.NOTHING), new MeterDataNames("C相欠压总次数", "XXXXXX", "*", PdfObject.NOTHING), new MeterDataNames("总累计时间", "XXXXXX", "*", PdfObject.NOTHING)});
        this.m_DataMap.put(Integer.valueOf(tmpModel48.getIDInt()), tmpModel48);
        ElectricMeterDataModel tmpModel49 = this.m_DLT645.ProduceData("03300000", "编程总次数", "XXXXXX", "*", PdfObject.NOTHING);
        this.m_DataMap.put(Integer.valueOf(tmpModel49.getIDInt()), tmpModel49);
    }

    @Override // com.clou.rs350.electricmeter.IElectricMeter
    public List<byte[]> ReadData(int[] key) {
        List<byte[]> buffers = new ArrayList<>();
        for (int i = 0; i < key.length; i++) {
            buffers.add(this.m_DLT645.packet((byte) 17, new byte[]{(byte) ((key[i] & 255) + 51), (byte) (((key[i] >> 8) & 255) + 51), (byte) (((key[i] >> 16) & 255) + 51), (byte) (((key[i] >> 24) & 255) + 51)}));
        }
        return buffers;
    }

    @Override // com.clou.rs350.electricmeter.IElectricMeter
    public List<byte[]> WakeUp() {
        List<byte[]> buffers = new ArrayList<>();
        buffers.add(new byte[]{-2, -2, -2, -2});
        return buffers;
    }

    @Override // com.clou.rs350.electricmeter.IElectricMeter
    public byte[] ReadContinue() {
        this.m_BufferIndex++;
        byte[] buffer = new byte[5];
        System.arraycopy(this.m_Key, 0, buffer, 0, 4);
        buffer[4] = (byte) this.m_BufferIndex;
        return this.m_DLT645.packet((byte) 18, buffer);
    }

    @Override // com.clou.rs350.electricmeter.IElectricMeter
    public List<byte[]> ReadAddress() {
        List<byte[]> buffers = new ArrayList<>();
        this.m_DLT645.MeterAddress = new byte[]{CLTcmd.WRTMEM, CLTcmd.WRTMEM, CLTcmd.WRTMEM, CLTcmd.WRTMEM, CLTcmd.WRTMEM, CLTcmd.WRTMEM};
        buffers.add(this.m_DLT645.packet((byte) 19, new byte[0]));
        return buffers;
    }

    @Override // com.clou.rs350.electricmeter.IElectricMeter
    public int Unpacket(byte[] buffer) {
        return this.m_DLT645.unPacket(buffer, new DLT645.IUnPacket() {
            /* class com.clou.rs350.electricmeter.DLT645_2007.AnonymousClass1 */

            @Override // com.clou.rs350.electricmeter.DLT645.IUnPacket
            public int unPacket(byte[] buffer) {
                boolean hasContinue;
                int result = 0;
                if ((buffer[8] & DocWriter.SPACE) == 32) {
                    hasContinue = true;
                } else {
                    hasContinue = false;
                }
                switch (buffer[8] & 31) {
                    case 17:
                        result = DLT645_2007.this.unpacketReadData(buffer, false);
                        break;
                    case 18:
                        result = DLT645_2007.this.unpacketReadData(buffer, true);
                        break;
                    case 19:
                        result = DLT645_2007.this.unpacketReadAddress(buffer);
                        break;
                }
                if (hasContinue) {
                    return 3;
                }
                DLT645_2007.this.m_ReadIndex = 0;
                DLT645_2007.this.m_BufferIndex = 0;
                return result;
            }
        });
    }

    public int unpacketReadAddress(byte[] buffer) {
        return 1;
    }

    public int unpacketReadData(byte[] buffer, boolean isContinue) {
        int i = 0;
        System.arraycopy(buffer, 10, this.m_Key, 0, 4);
        int key = ((buffer[10] & 255) - 51) + (((buffer[11] & 255) - 51) << 8) + (((buffer[12] & 255) - 51) << 16) + (((buffer[13] & 255) - 51) << 24);
        int i2 = buffer[9] + 10;
        if (isContinue) {
            i = 1;
        }
        int length = i2 - i;
        int i3 = 14;
        while (i3 < length) {
            try {
                int DataLength = this.m_DataMap.get(Integer.valueOf(key)).getDataModel().get(this.m_ReadIndex).getDataLength();
                byte[] b_Data = new byte[DataLength];
                System.arraycopy(buffer, i3, b_Data, 0, DataLength);
                this.m_DataMap.get(Integer.valueOf(key)).getDataModel().get(this.m_ReadIndex).setValue(this.m_DLT645.Explean(this.m_DataMap.get(Integer.valueOf(key)).getDataModel().get(this.m_ReadIndex), b_Data));
                i3 += DataLength;
                this.m_ReadIndex++;
            } catch (Exception e) {
                return -125;
            }
        }
        if (isContinue) {
            return 1;
        }
        this.m_BufferIndex = buffer[length + 1];
        return 1;
    }

    @Override // com.clou.rs350.electricmeter.IElectricMeter
    public String GetAddress() {
        return BufferUtil.toHexString(this.m_DLT645.MeterAddress);
    }

    @Override // com.clou.rs350.electricmeter.IElectricMeter
    public void SetAddress(String address) {
        this.m_DLT645.MeterAddress = new byte[6];
        int i = 0;
        while (i < address.length() / 2 && i < this.m_DLT645.MeterAddress.length) {
            this.m_DLT645.MeterAddress[i] = (byte) BufferUtil.parseHexInt(address.substring(address.length() - ((i + 1) * 2), address.length() - (i * 2)));
            i++;
        }
        if (address.length() % 2 != 0 && i < this.m_DLT645.MeterAddress.length) {
            this.m_DLT645.MeterAddress[i] = (byte) BufferUtil.parseHexInt(address.substring(0, 1));
        }
        if (BufferUtil.getEqual(this.m_DLT645.MeterAddress, new byte[6])) {
            this.m_DLT645.MeterAddress = new byte[]{CLTcmd.WRTMEM, CLTcmd.WRTMEM, CLTcmd.WRTMEM, CLTcmd.WRTMEM, CLTcmd.WRTMEM, CLTcmd.WRTMEM};
        }
    }

    @Override // com.clou.rs350.electricmeter.IElectricMeter
    public Map<Integer, ElectricMeterDataModel> getDataMap() {
        return this.m_DataMap;
    }

    @Override // com.clou.rs350.electricmeter.IElectricMeter
    public void setDataMap(Map<Integer, ElectricMeterDataModel> m_DataMap2) {
        this.m_DataMap = m_DataMap2;
    }

    @Override // com.clou.rs350.electricmeter.IElectricMeter
    public List<byte[]> ReadTime() {
        return ReadData(new int[]{67109121, 67109122});
    }

    @Override // com.clou.rs350.electricmeter.IElectricMeter
    public List<byte[]> ReadBasicData() {
        return ReadData(new int[]{33620224, 33620480, 33620736, 33685760, 33686016, 33686272, 33751040, 33816576, 65536, 65792, 66048, 66304, 66560, 131072, 131328, 131584, 131840, 132096, 196608, 196864, 197120, 197376, 197632, 262144, 262400, 262656, 262912, 263168});
    }

    @Override // com.clou.rs350.electricmeter.IElectricMeter
    public String GetDateAndTime() {
        return String.valueOf(this.m_DataMap.get(67109121).getDataModel().get(0).getValue()) + " " + this.m_DataMap.get(67109122).getDataModel().get(0).getValue();
    }

    @Override // com.clou.rs350.electricmeter.IElectricMeter
    public String GetTime() {
        return this.m_DataMap.get(67109122).getDataModel().get(0).getValue();
    }

    @Override // com.clou.rs350.electricmeter.IElectricMeter
    public String[] getBasicData() {
        int[] tmpKey = {33620224, 33620480, 33620736, 33685760, 33686016, 33686272, 33751040, 33816576, 65536, 65792, 66048, 66304, 66560, 131072, 131328, 131584, 131840, 132096, 196608, 196864, 197120, 197376, 197632, 262144, 262400, 262656, 262912, 263168};
        String[] tmpString = new String[tmpKey.length];
        for (int i = 0; i < tmpString.length; i++) {
            ElectricMeterDetailModel tmpModel = this.m_DataMap.get(Integer.valueOf(tmpKey[i])).getDataModel().get(0);
            tmpString[i] = String.valueOf(tmpModel.getValue()) + tmpModel.getUnit();
        }
        return tmpString;
    }
}
