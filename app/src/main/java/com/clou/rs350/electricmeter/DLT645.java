package com.clou.rs350.electricmeter;

import com.clou.rs350.device.constants.CLTcmd;
import com.clou.rs350.device.model.Buffer;
import com.clou.rs350.device.utils.BufferUtil;
import com.itextpdf.text.pdf.PdfObject;
import java.util.ArrayList;
import java.util.List;

public class DLT645 {
    byte[] BroadcastAddress = {-103, -103, -103, -103, -103, -103};
    public byte[] MeterAddress = {CLTcmd.WRTMEM, CLTcmd.WRTMEM, CLTcmd.WRTMEM, CLTcmd.WRTMEM, CLTcmd.WRTMEM, CLTcmd.WRTMEM};
    Buffer m_ReceiveBuffer = new Buffer();

    interface IUnPacket {
        int unPacket(byte[] bArr);
    }

    public byte[] wakeup() {
        return new byte[]{-2, -2, -2, -2};
    }

    public byte[] packet(byte cmd, byte[] inputBuffer) {
        return packet(cmd, inputBuffer, false);
    }

    public byte[] packet(byte cmd, byte[] inputBuffer, boolean broadcast) {
        int len = inputBuffer.length + 16;
        byte[] buffer = new byte[len];
        buffer[0] = -2;
        buffer[1] = -2;
        buffer[2] = -2;
        buffer[3] = -2;
        buffer[4] = 104;
        if (broadcast) {
            System.arraycopy(this.BroadcastAddress, 0, buffer, 5, 6);
        } else {
            System.arraycopy(this.MeterAddress, 0, buffer, 5, 6);
        }
        buffer[11] = 104;
        buffer[12] = cmd;
        buffer[13] = (byte) inputBuffer.length;
        buffer[len - 1] = 22;
        System.arraycopy(inputBuffer, 0, buffer, 14, inputBuffer.length);
        buffer[len - 2] = BufferUtil.getChecksum(buffer, 4, len - 3);
        return buffer;
    }

    public byte[] intTOBCD(int iData, int length, int password) {
        byte[] bData = new byte[length];
        for (int i = 0; i < length; i++) {
            bData[i] = (byte) (BufferUtil.intToBCD((byte) (iData & 255)) + password);
            iData >>= 8;
        }
        return bData;
    }

    public long BCDToLong(byte[] bData, int password) {
        long iData = 0;
        for (int i = bData.length - 1; i >= 0; i--) {
            iData = (iData * 100) + ((long) (BufferUtil.BCDToint((byte) ((bData[i] & 255) - password)) & 255));
        }
        return iData;
    }

    public String BCDToString(byte[] bData, int password) {
        return String.valueOf(BCDToLong(bData, password));
    }

    public int unPacket(byte[] buffer, IUnPacket unpacket) {
        int result;
        this.m_ReceiveBuffer.MergeBuffer(buffer);
        while (this.m_ReceiveBuffer.length > 0 && this.m_ReceiveBuffer.ReceiveBuffer[0] == -2) {
            Buffer buffer2 = this.m_ReceiveBuffer;
            buffer2.length--;
            byte[] tmpBuffer = new byte[256];
            System.arraycopy(this.m_ReceiveBuffer.ReceiveBuffer, 1, tmpBuffer, 0, this.m_ReceiveBuffer.length);
            this.m_ReceiveBuffer.ReceiveBuffer = tmpBuffer;
        }
        if (this.m_ReceiveBuffer.length <= 0) {
            return -125;
        }
        if (this.m_ReceiveBuffer.ReceiveBuffer[0] != 104) {
            this.m_ReceiveBuffer = new Buffer();
            return -125;
        } else if (this.m_ReceiveBuffer.length < 12 || this.m_ReceiveBuffer.length < (this.m_ReceiveBuffer.ReceiveBuffer[9] & 255) + 12) {
            return -125;
        } else {
            if (this.m_ReceiveBuffer.ReceiveBuffer[(this.m_ReceiveBuffer.ReceiveBuffer[9] & 255) + 10] == BufferUtil.getChecksum(this.m_ReceiveBuffer.ReceiveBuffer, 0, (this.m_ReceiveBuffer.ReceiveBuffer[9] & 255) + 9) || this.m_ReceiveBuffer.ReceiveBuffer[(this.m_ReceiveBuffer.ReceiveBuffer[9] & 255) + 11] == 22) {
                if (BufferUtil.getEqual(this.MeterAddress, new byte[]{CLTcmd.WRTMEM, CLTcmd.WRTMEM, CLTcmd.WRTMEM, CLTcmd.WRTMEM, CLTcmd.WRTMEM, CLTcmd.WRTMEM}) || BufferUtil.getEqual(this.MeterAddress, new byte[]{-103, -103, -103, -103, -103, -103})) {
                    System.arraycopy(this.m_ReceiveBuffer.ReceiveBuffer, 1, this.MeterAddress, 0, 6);
                }
                byte[] tmpAddress = new byte[6];
                System.arraycopy(this.m_ReceiveBuffer.ReceiveBuffer, 1, tmpAddress, 0, 6);
                if (!BufferUtil.getEqual(this.MeterAddress, tmpAddress)) {
                    this.m_ReceiveBuffer = new Buffer();
                    return -125;
                }
                if (unpacket != null) {
                    result = unpacket.unPacket(this.m_ReceiveBuffer.ReceiveBuffer);
                } else {
                    result = 255;
                }
                this.m_ReceiveBuffer = new Buffer();
                return result;
            }
            this.m_ReceiveBuffer = new Buffer();
            return -125;
        }
    }

    public String Explean(ElectricMeterDetailModel model, byte[] buffer) {
        String value = PdfObject.NOTHING;
        String form = model.getFormat();
        if (!form.startsWith("X") && !form.startsWith("x") && !form.startsWith("N") && !form.startsWith("n")) {
            for (int i = 0; i < form.length() / 2; i++) {
                value = String.valueOf(String.valueOf(value) + ((int) BufferUtil.BCDToint(buffer[(buffer.length - i) - 1], CLTcmd.ECHERR))) + getUnit(form.substring(i * 2));
            }
            return value.substring(0, value.length() - 1);
        } else if (model.getRatio() != 1) {
            return new StringBuilder(String.valueOf(((double) BCDToLong(buffer, 51)) / ((double) model.getRatio()))).toString();
        } else {
            return BCDToString(buffer, 51);
        }
    }

    private String getUnit(String form) {
        if (form.startsWith("Y") || form.startsWith("M")) {
            return "-";
        }
        if (form.startsWith("h") || form.startsWith("m")) {
            return ":";
        }
        return " ";
    }

    public ElectricMeterDataModel ProduceData(String identifying, String name, String style, String unit, String compareValue) {
        ElectricMeterDetailModel electricMeterDetailModel = updateMeterDataName(name, style, unit, compareValue);
        List<ElectricMeterDetailModel> m_DataModel = new ArrayList<>();
        m_DataModel.add(electricMeterDetailModel);
        ElectricMeterDataModel tempDataModel = updateMeterData(identifying, name);
        tempDataModel.setDataModel(m_DataModel);
        return tempDataModel;
    }

    public ElectricMeterDataModel ProduceData(String identifying, MeterDataNames[] datas) {
        if (datas.length <= 0) {
            return updateMeterData(identifying, PdfObject.NOTHING);
        }
        List<ElectricMeterDetailModel> tmpDetailModel = new ArrayList<>();
        for (int i = 0; i < datas.length; i++) {
            tmpDetailModel.add(updateMeterDataName(datas[i].Name, datas[i].Style, datas[i].Unit, datas[i].CompareValue));
        }
        ElectricMeterDataModel tmpDataModel = updateMeterData(identifying, datas[0].Name);
        tmpDataModel.setDataModel(tmpDetailModel);
        return tmpDataModel;
    }

    public ElectricMeterDataModel updateMeterData(String identifying, String name) {
        ElectricMeterDataModel tempDataModel = new ElectricMeterDataModel();
        tempDataModel.setID(identifying);
        tempDataModel.setName(name);
        return tempDataModel;
    }

    public ElectricMeterDetailModel updateMeterDataName(String name, String style, String unit, String compareValue) {
        ElectricMeterDetailModel electricMeterDetailModel = new ElectricMeterDetailModel();
        electricMeterDetailModel.setName(name);
        electricMeterDetailModel.setFormat(style);
        electricMeterDetailModel.setUnit(unit);
        electricMeterDetailModel.setStandardValue(compareValue);
        return electricMeterDetailModel;
    }
}
