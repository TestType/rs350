package com.clou.rs350.electricmeter;

public class MeterDataNames {
    String CompareValue;
    String Name;
    String Style;
    String Unit;

    public MeterDataNames(String name, String style, String unit, String compareValue) {
        this.Name = name;
        this.Style = style;
        this.Unit = unit;
        this.CompareValue = compareValue;
    }
}
