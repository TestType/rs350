package com.clou.rs350.db.model.sequence;

import com.itextpdf.text.pdf.PdfObject;
import java.io.Serializable;

public class FieldItem implements Serializable {
    public int Id = 0;
    public String Item = PdfObject.NOTHING;
    public int RequiredSetting = 0;

    public FieldItem() {
    }

    public FieldItem(String item, int id, int requiredSetting) {
        this.Item = item;
        this.Id = id;
        this.RequiredSetting = requiredSetting;
    }
}
