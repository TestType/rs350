package com.clou.rs350.db.model.sequence;

import android.graphics.Bitmap;
import com.itextpdf.text.pdf.PdfObject;
import java.io.Serializable;

public class SignatureItem implements Serializable {
    public boolean HasSign = false;
    public int Index;
    public String Name = PdfObject.NOTHING;
    public Bitmap SignatureBmp;
    public String SignaturePath = PdfObject.NOTHING;
}
