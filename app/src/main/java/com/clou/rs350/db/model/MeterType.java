package com.clou.rs350.db.model;

import com.itextpdf.text.pdf.PdfObject;
import java.io.Serializable;

public class MeterType implements Serializable {
    public String ActiveAccuracy = PdfObject.NOTHING;
    public String ActiveConstant = PdfObject.NOTHING;
    public String ApparentAccuracy = PdfObject.NOTHING;
    public String ApparentConstant = PdfObject.NOTHING;
    public String BasicCurrent = PdfObject.NOTHING;
    public int IsRead = 0;
    public String Manufacturer = PdfObject.NOTHING;
    public String MaxCurrent = PdfObject.NOTHING;
    public String NominalFrequency = PdfObject.NOTHING;
    public String NominalVoltage = PdfObject.NOTHING;
    public String ReactiveAccuracy = PdfObject.NOTHING;
    public String ReactiveConstant = PdfObject.NOTHING;
    public String Type = PdfObject.NOTHING;
    public int Wiring = 0;
    public String WiringType = PdfObject.NOTHING;
    public boolean isNew = true;

    public void CopyTypeData(MeterType Data) {
        this.Type = Data.Type;
        this.Wiring = Data.Wiring;
        this.WiringType = Data.WiringType;
        this.Manufacturer = Data.Manufacturer;
        this.ActiveAccuracy = Data.ActiveAccuracy;
        this.ReactiveAccuracy = Data.ReactiveAccuracy;
        this.ApparentAccuracy = Data.ApparentAccuracy;
        this.ActiveConstant = Data.ActiveConstant;
        this.ReactiveConstant = Data.ReactiveConstant;
        this.ApparentConstant = Data.ApparentConstant;
        this.MaxCurrent = Data.MaxCurrent;
        this.BasicCurrent = Data.BasicCurrent;
        this.NominalVoltage = Data.NominalVoltage;
        this.NominalFrequency = Data.NominalFrequency;
    }
}
