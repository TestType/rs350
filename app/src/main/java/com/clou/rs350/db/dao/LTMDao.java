package com.clou.rs350.db.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import com.clou.rs350.constants.Constants;
import com.clou.rs350.db.dao.BaseTestDao;
import com.clou.rs350.db.model.DBDataModel;
import com.clou.rs350.db.model.IntegrationData;
import com.clou.rs350.db.model.LTM;
import com.clou.rs350.manager.DatabaseManager;
import com.clou.rs350.utils.DataProcessing;
import com.clou.rs350.utils.OtherUtils;
import com.itextpdf.text.pdf.PdfObject;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class LTMDao extends BaseTestDao {
    private static final String TABLE_NAME = "TestLTM";

    private static final class TableColumns {
        public static final String ActivePowerL1 = "ActivePowerL1";
        public static final String ActivePowerL2 = "ActivePowerL2";
        public static final String ActivePowerL3 = "ActivePowerL3";
        public static final String ActivePowerTotal = "ActivePowerTotal";
        public static final String AngleDefinition = "AngleDefinition";
        public static final String AngleU12I1 = "AngleU12I1";
        public static final String AngleU23I2 = "AngleU23I2";
        public static final String AngleU32I3 = "AngleU32I3";
        public static final String AngleVoltageL1L2 = "AngleVoltageL1L2";
        public static final String AngleVoltageL2L3 = "AngleVoltageL2L3";
        public static final String AngleVoltageL3L1 = "AngleVoltageL3L1";
        public static final String ApparentPowerL1 = "ApparentPowerL1";
        public static final String ApparentPowerL2 = "ApparentPowerL2";
        public static final String ApparentPowerL3 = "ApparentPowerL3";
        public static final String ApparentPowerTotal = "ApparentPowerTotal";
        public static final String ApparentPowerType = "ApparentPowerType";
        public static final String CurrentAngleL1 = "CurrentAngleL1";
        public static final String CurrentAngleL2 = "CurrentAngleL2";
        public static final String CurrentAngleL3 = "CurrentAngleL3";
        public static final String CurrentLN = "CurrentLN";
        public static final String CurrentValueL1 = "CurrentValueL1";
        public static final String CurrentValueL2 = "CurrentValueL2";
        public static final String CurrentValueL3 = "CurrentValueL3";
        public static final String Frequency = "Frequency";
        public static final String MeasureTime = "MeasureTime";
        public static final String PowerFactorL1 = "PowerFactorL1";
        public static final String PowerFactorL2 = "PowerFactorL2";
        public static final String PowerFactorL3 = "PowerFactorL3";
        public static final String PowerFactorTotal = "PowerFactorTotal";
        public static final String ReactivePowerL1 = "ReactivePowerL1";
        public static final String ReactivePowerL2 = "ReactivePowerL2";
        public static final String ReactivePowerL3 = "ReactivePowerL3";
        public static final String ReactivePowerTotal = "ReactivePowerTotal";
        public static final String THDI1 = "THDI1";
        public static final String THDI2 = "THDI2";
        public static final String THDI3 = "THDI3";
        public static final String THDU1 = "THDU1";
        public static final String THDU2 = "THDU2";
        public static final String THDU3 = "THDU3";
        public static final String VoltageAngleL1 = "VoltageAngleL1";
        public static final String VoltageAngleL2 = "VoltageAngleL2";
        public static final String VoltageAngleL3 = "VoltageAngleL3";
        public static final String VoltageCurrentAngleL1 = "VoltageCurrentAngleL1";
        public static final String VoltageCurrentAngleL2 = "VoltageCurrentAngleL2";
        public static final String VoltageCurrentAngleL3 = "VoltageCurrentAngleL3";
        public static final String VoltageL1L2 = "VoltageL1L2";
        public static final String VoltageL2L3 = "VoltageL2L3";
        public static final String VoltageL3L1 = "VoltageL3L1";
        public static final String VoltageLN = "VoltageLN";
        public static final String VoltageValueL1 = "VoltageValueL1";
        public static final String VoltageValueL2 = "VoltageValueL2";
        public static final String VoltageValueL3 = "VoltageValueL3";

        private TableColumns() {
        }
    }

    public LTMDao(Context context) {
        super(context, TABLE_NAME);
    }

    public void createTable(SQLiteDatabase db) {
        this.m_TableName = TABLE_NAME;
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append(getCreatTableString());
        stringBuffer.append(TableColumns.MeasureTime).append(" VARCHAR(30),");
        stringBuffer.append("VoltageValueL1").append(" VARCHAR(20),");
        stringBuffer.append("VoltageValueL2").append(" VARCHAR(20),");
        stringBuffer.append("VoltageValueL3").append(" VARCHAR(20),");
        stringBuffer.append("CurrentValueL1").append(" VARCHAR(20),");
        stringBuffer.append("CurrentValueL2").append(" VARCHAR(20),");
        stringBuffer.append("CurrentValueL3").append(" VARCHAR(20),");
        stringBuffer.append("VoltageAngleL1").append(" VARCHAR(20),");
        stringBuffer.append("VoltageAngleL2").append(" VARCHAR(20),");
        stringBuffer.append("VoltageAngleL3").append(" VARCHAR(20),");
        stringBuffer.append("CurrentAngleL1").append(" VARCHAR(20),");
        stringBuffer.append("CurrentAngleL2").append(" VARCHAR(20),");
        stringBuffer.append("CurrentAngleL3").append(" VARCHAR(20),");
        stringBuffer.append("VoltageCurrentAngleL1").append(" VARCHAR(50),");
        stringBuffer.append("VoltageCurrentAngleL2").append(" VARCHAR(50),");
        stringBuffer.append("VoltageCurrentAngleL3").append(" VARCHAR(50),");
        stringBuffer.append("ActivePowerL1").append(" VARCHAR(20),");
        stringBuffer.append("ActivePowerL2").append(" VARCHAR(20),");
        stringBuffer.append("ActivePowerL3").append(" VARCHAR(20),");
        stringBuffer.append("ActivePowerTotal").append(" VARCHAR(20),");
        stringBuffer.append("ReactivePowerL1").append(" VARCHAR(20),");
        stringBuffer.append("ReactivePowerL2").append(" VARCHAR(20),");
        stringBuffer.append("ReactivePowerL3").append(" VARCHAR(20),");
        stringBuffer.append("ReactivePowerTotal").append(" VARCHAR(20),");
        stringBuffer.append("ApparentPowerL1").append(" VARCHAR(20),");
        stringBuffer.append("ApparentPowerL2").append(" VARCHAR(20),");
        stringBuffer.append("ApparentPowerL3").append(" VARCHAR(20),");
        stringBuffer.append("ApparentPowerTotal").append(" VARCHAR(20),");
        stringBuffer.append("ApparentPowerType").append(" VARCHAR(10),");
        stringBuffer.append("PowerFactorL1").append(" VARCHAR(10),");
        stringBuffer.append("PowerFactorL2").append(" VARCHAR(10),");
        stringBuffer.append("PowerFactorL3").append(" VARCHAR(10),");
        stringBuffer.append("PowerFactorTotal").append(" VARCHAR(10),");
        stringBuffer.append("VoltageL1L2").append(" VARCHAR(20),");
        stringBuffer.append("VoltageL2L3").append(" VARCHAR(20),");
        stringBuffer.append("VoltageL3L1").append(" VARCHAR(20),");
        stringBuffer.append("AngleVoltageL3L1").append(" VARCHAR(10),");
        stringBuffer.append("AngleVoltageL1L2").append(" VARCHAR(10),");
        stringBuffer.append("AngleVoltageL2L3").append(" VARCHAR(10),");
        stringBuffer.append("AngleU12I1").append(" VARCHAR(10),");
        stringBuffer.append("AngleU23I2").append(" VARCHAR(10),");
        stringBuffer.append("AngleU32I3").append(" VARCHAR(10),");
        stringBuffer.append("Frequency").append(" VARCHAR(10),");
        stringBuffer.append("THDU1").append(" VARCHAR(10),");
        stringBuffer.append("THDU2").append(" VARCHAR(10),");
        stringBuffer.append("THDU3").append(" VARCHAR(10),");
        stringBuffer.append("THDI1").append(" VARCHAR(10),");
        stringBuffer.append("THDI2").append(" VARCHAR(10),");
        stringBuffer.append("THDI3").append(" VARCHAR(10),");
        stringBuffer.append("VoltageLN").append(" VARCHAR(20),");
        stringBuffer.append("CurrentLN").append(" VARCHAR(20));");
        db.execSQL(stringBuffer.toString());
    }

    @Override // com.clou.rs350.db.dao.BaseDao, com.clou.rs350.db.dao.BaseTestDao
    public void upgradeTable(SQLiteDatabase db, int oldVersion, int newVersion) {
        if (oldVersion <= 3) {
            db.execSQL("alter table LTM rename to TestLTM");
        }
        super.upgradeTable(db, oldVersion, newVersion);
    }

    @Override // com.clou.rs350.db.dao.BaseDao, com.clou.rs350.db.dao.BaseTestDao
    public synchronized boolean insertObject(Object Data, String Time) {
        boolean z;
        SQLiteDatabase db = DatabaseManager.getWriteableDatabase(this.m_Context);
        ContentValues values = new ContentValues();
        long status = 0;
        LTM objects = (LTM) Data;
        for (int i = 0; i < objects.IntegrationDatas.size(); i++) {
            IntegrationData object = objects.IntegrationDatas.get(i);
            values.put(this.m_TableKey, objects.SerialNo.toString());
            values.put("CreateTime", Time);
            values.put(SameColumns.WiringType, object.WiringValue);
            values.put("IntWiringType", Integer.valueOf(object.Wiring));
            values.put(SameColumns.ConnectionMode, object.ConnectionType.toString());
            values.put(SameColumns.VoltageRangeL1, object.VoltageRangeL1.toString());
            values.put(SameColumns.VoltageRangeL2, object.VoltageRangeL2.toString());
            values.put(SameColumns.VoltageRangeL3, object.VoltageRangeL3.toString());
            values.put(SameColumns.CurrentRangeL1, object.CurrentRangeL1.toString());
            values.put(SameColumns.CurrentRangeL2, object.CurrentRangeL2.toString());
            values.put(SameColumns.CurrentRangeL3, object.CurrentRangeL3.toString());
            values.put(TableColumns.MeasureTime, object.TimeStamp.toString());
            values.put("VoltageValueL1", object.VoltageValueL1.toString());
            values.put("VoltageValueL2", object.VoltageValueL2.toString());
            values.put("VoltageValueL3", object.VoltageValueL3.toString());
            values.put("CurrentValueL1", object.CurrentValueL1.toString());
            values.put("CurrentValueL2", object.CurrentValueL2.toString());
            values.put("CurrentValueL3", object.CurrentValueL3.toString());
            values.put("VoltageAngleL1", object.VoltageAngleL1.toString());
            values.put("VoltageAngleL2", object.VoltageAngleL2.toString());
            values.put("VoltageAngleL3", object.VoltageAngleL3.toString());
            values.put("CurrentAngleL1", object.CurrentAngleL1.toString());
            values.put("CurrentAngleL2", object.CurrentAngleL2.toString());
            values.put("CurrentAngleL3", object.CurrentAngleL3.toString());
            values.put("VoltageCurrentAngleL1", object.VoltageCurrentAngleL1.toString());
            values.put("VoltageCurrentAngleL2", object.VoltageCurrentAngleL2.toString());
            values.put("VoltageCurrentAngleL3", object.VoltageCurrentAngleL3.toString());
            values.put("ActivePowerL1", object.ActivePowerL1.toString());
            values.put("ActivePowerL2", object.ActivePowerL2.toString());
            values.put("ActivePowerL3", object.ActivePowerL3.toString());
            values.put("ActivePowerTotal", object.ActivePowerTotal.toString());
            values.put("ReactivePowerL1", object.ReactivePowerL1.toString());
            values.put("ReactivePowerL2", object.ReactivePowerL2.toString());
            values.put("ReactivePowerL3", object.ReactivePowerL3.toString());
            values.put("ReactivePowerTotal", object.ReactivePowerTotal.toString());
            values.put("ApparentPowerL1", object.ApparentPowerL1.toString());
            values.put("ApparentPowerL2", object.ApparentPowerL2.toString());
            values.put("ApparentPowerL3", object.ApparentPowerL3.toString());
            values.put("ApparentPowerTotal", object.ApparentPowerTotal.toString());
            values.put("ApparentPowerType", object.ApparentPowerType.toString());
            values.put("PowerFactorL1", object.PowerFactorL1.toString());
            values.put("PowerFactorL2", object.PowerFactorL2.toString());
            values.put("PowerFactorL3", object.PowerFactorL3.toString());
            values.put("PowerFactorTotal", object.PowerFactorTotal.toString());
            values.put("VoltageL1L2", object.VoltageValueL1L2.toString());
            values.put("VoltageL2L3", object.VoltageValueL2L3.toString());
            values.put("VoltageL3L1", object.VoltageValueL3L1.toString());
            values.put("AngleVoltageL3L1", object.VoltageAngleL3L1.toString());
            values.put("AngleVoltageL1L2", object.VoltageAngleL1L2.toString());
            values.put("AngleVoltageL2L3", object.VoltageAngleL2L3.toString());
            values.put("AngleU12I1", object.AngleU12I1.toString());
            values.put("AngleU23I2", object.AngleU23I2.toString());
            values.put("AngleU32I3", object.AngleU31I3.toString());
            values.put("THDU1", object.THDU1.toString());
            values.put("THDU2", object.THDU2.toString());
            values.put("THDU3", object.THDU3.toString());
            values.put("THDI1", object.THDI1.toString());
            values.put("THDI2", object.THDI2.toString());
            values.put("THDI3", object.THDI3.toString());
            values.put("VoltageLN", object.VoltageLN.toString());
            values.put("CurrentLN", object.CurrentLN.toString());
            values.put("Frequency", object.Frequency.toString());
            status += db.insert(TABLE_NAME, null, values);
        }
        if (status != -1) {
            z = true;
        } else {
            z = false;
        }
        return z;
    }

    /* access modifiers changed from: protected */
    @Override // com.clou.rs350.db.dao.BaseDao, com.clou.rs350.db.dao.BaseTestDao
    public Object parseData(Cursor cursor) {
        LTM values = new LTM();
        values.SerialNo = cursor.getString(cursor.getColumnIndex(this.m_TableKey));
        values.SaveTime = cursor.getString(cursor.getColumnIndex("CreateTime"));
        values.WiringValue = cursor.getString(cursor.getColumnIndex(SameColumns.WiringType));
        values.Wiring = cursor.getInt(cursor.getColumnIndex("IntWiringType"));
        do {
            IntegrationData value = new IntegrationData();
            value.TimeStamp.s_Value = cursor.getString(cursor.getColumnIndex(TableColumns.MeasureTime));
            try {
                Date date = new SimpleDateFormat(Constants.TIMEFORMAT).parse(value.TimeStamp.s_Value);
                value.TimeStamp.l_Value = date.getTime();
            } catch (ParseException e) {
                e.printStackTrace();
            }
            value.VoltageRangeL1 = cursor.getString(cursor.getColumnIndex(SameColumns.VoltageRangeL1));
            value.VoltageRangeL2 = cursor.getString(cursor.getColumnIndex(SameColumns.VoltageRangeL2));
            value.VoltageRangeL3 = cursor.getString(cursor.getColumnIndex(SameColumns.VoltageRangeL3));
            value.CurrentRangeL1 = cursor.getString(cursor.getColumnIndex(SameColumns.CurrentRangeL1));
            value.CurrentRangeL2 = cursor.getString(cursor.getColumnIndex(SameColumns.CurrentRangeL2));
            value.CurrentRangeL3 = cursor.getString(cursor.getColumnIndex(SameColumns.CurrentRangeL3));
            value.VoltageValueL1 = getModel(cursor.getString(cursor.getColumnIndex("VoltageValueL1")));
            value.VoltageValueL2 = getModel(cursor.getString(cursor.getColumnIndex("VoltageValueL2")));
            value.VoltageValueL3 = getModel(cursor.getString(cursor.getColumnIndex("VoltageValueL3")));
            values.VoltageLTML1.dataList.add(value.VoltageValueL1);
            values.VoltageLTML2.dataList.add(value.VoltageValueL2);
            values.VoltageLTML3.dataList.add(value.VoltageValueL3);
            value.CurrentValueL1 = getModel(cursor.getString(cursor.getColumnIndex("CurrentValueL1")));
            value.CurrentValueL2 = getModel(cursor.getString(cursor.getColumnIndex("CurrentValueL2")));
            value.CurrentValueL3 = getModel(cursor.getString(cursor.getColumnIndex("CurrentValueL3")));
            values.CurrentLTML1.dataList.add(value.CurrentValueL1);
            values.CurrentLTML2.dataList.add(value.CurrentValueL2);
            values.CurrentLTML3.dataList.add(value.CurrentValueL3);
            value.VoltageValueL1L2 = getModel(cursor.getString(cursor.getColumnIndex("VoltageL1L2")));
            value.VoltageValueL2L3 = getModel(cursor.getString(cursor.getColumnIndex("VoltageL2L3")));
            value.VoltageValueL3L1 = getModel(cursor.getString(cursor.getColumnIndex("VoltageL3L1")));
            values.VoltageLTML1L2.dataList.add(value.VoltageValueL1L2);
            values.VoltageLTML2L3.dataList.add(value.VoltageValueL2L3);
            values.VoltageLTML3L1.dataList.add(value.VoltageValueL3L1);
            value.VoltageAngleL1 = getModel(cursor.getString(cursor.getColumnIndex("VoltageAngleL1")));
            value.VoltageAngleL2 = getModel(cursor.getString(cursor.getColumnIndex("VoltageAngleL2")));
            value.VoltageAngleL3 = getModel(cursor.getString(cursor.getColumnIndex("VoltageAngleL3")));
            values.VoltageAngleLTML1.dataList.add(value.VoltageAngleL1);
            values.VoltageAngleLTML2.dataList.add(value.VoltageAngleL2);
            values.VoltageAngleLTML3.dataList.add(value.VoltageAngleL3);
            value.CurrentAngleL1 = getModel(cursor.getString(cursor.getColumnIndex("CurrentAngleL1")));
            value.CurrentAngleL2 = getModel(cursor.getString(cursor.getColumnIndex("CurrentAngleL2")));
            value.CurrentAngleL3 = getModel(cursor.getString(cursor.getColumnIndex("CurrentAngleL3")));
            values.CurrentAngleLTML1.dataList.add(value.CurrentAngleL1);
            values.CurrentAngleLTML2.dataList.add(value.CurrentAngleL2);
            values.CurrentAngleLTML3.dataList.add(value.CurrentAngleL3);
            value.VoltageAngleL1L2 = getModel(cursor.getString(cursor.getColumnIndex("AngleVoltageL1L2")));
            value.VoltageAngleL2L3 = getModel(cursor.getString(cursor.getColumnIndex("AngleVoltageL2L3")));
            value.VoltageAngleL3L1 = getModel(cursor.getString(cursor.getColumnIndex("AngleVoltageL3L1")));
            values.VoltageAngleLTML1L2.dataList.add(value.VoltageAngleL1L2);
            values.VoltageAngleLTML2L3.dataList.add(value.VoltageAngleL2L3);
            values.VoltageAngleLTML3L1.dataList.add(value.VoltageAngleL3L1);
            value.VoltageCurrentAngleL1 = getModel(cursor.getString(cursor.getColumnIndex("VoltageCurrentAngleL1")));
            value.VoltageCurrentAngleL2 = getModel(cursor.getString(cursor.getColumnIndex("VoltageCurrentAngleL2")));
            value.VoltageCurrentAngleL3 = getModel(cursor.getString(cursor.getColumnIndex("VoltageCurrentAngleL3")));
            values.VoltageCurrentAngleLTML1.dataList.add(value.VoltageCurrentAngleL1);
            values.VoltageCurrentAngleLTML2.dataList.add(value.VoltageCurrentAngleL2);
            values.VoltageCurrentAngleLTML3.dataList.add(value.VoltageCurrentAngleL3);
            value.AngleU12I1 = getModel(cursor.getString(cursor.getColumnIndex("AngleU12I1")));
            value.AngleU23I2 = getModel(cursor.getString(cursor.getColumnIndex("AngleU23I2")));
            value.AngleU31I3 = getModel(cursor.getString(cursor.getColumnIndex("AngleU32I3")));
            values.AngleLTMU12I1.dataList.add(value.AngleU12I1);
            values.AngleLTMU23I2.dataList.add(value.AngleU23I2);
            values.AngleLTMU31I3.dataList.add(value.AngleU31I3);
            value.ActivePowerL1 = getModel(cursor.getString(cursor.getColumnIndex("ActivePowerL1")));
            value.ActivePowerL2 = getModel(cursor.getString(cursor.getColumnIndex("ActivePowerL2")));
            value.ActivePowerL3 = getModel(cursor.getString(cursor.getColumnIndex("ActivePowerL3")));
            value.ActivePowerTotal = getModel(cursor.getString(cursor.getColumnIndex("ActivePowerTotal")));
            values.ActivePowerLTML1.dataList.add(value.ActivePowerL1);
            values.ActivePowerLTML2.dataList.add(value.ActivePowerL2);
            values.ActivePowerLTML3.dataList.add(value.ActivePowerL3);
            values.ActivePowerLTMTotal.dataList.add(value.ActivePowerTotal);
            value.ReactivePowerL1 = getModel(cursor.getString(cursor.getColumnIndex("ReactivePowerL1")));
            value.ReactivePowerL2 = getModel(cursor.getString(cursor.getColumnIndex("ReactivePowerL2")));
            value.ReactivePowerL3 = getModel(cursor.getString(cursor.getColumnIndex("ReactivePowerL3")));
            value.ReactivePowerTotal = getModel(cursor.getString(cursor.getColumnIndex("ReactivePowerTotal")));
            values.ReactivePowerLTML1.dataList.add(value.ReactivePowerL1);
            values.ReactivePowerLTML2.dataList.add(value.ReactivePowerL2);
            values.ReactivePowerLTML3.dataList.add(value.ReactivePowerL3);
            values.ReactivePowerLTMTotal.dataList.add(value.ReactivePowerTotal);
            value.ApparentPowerL1 = getModel(cursor.getString(cursor.getColumnIndex("ApparentPowerL1")));
            value.ApparentPowerL2 = getModel(cursor.getString(cursor.getColumnIndex("ApparentPowerL2")));
            value.ApparentPowerL3 = getModel(cursor.getString(cursor.getColumnIndex("ApparentPowerL3")));
            value.ApparentPowerTotal = getModel(cursor.getString(cursor.getColumnIndex("ApparentPowerTotal")));
            values.ApparentPowerLTML1.dataList.add(value.ApparentPowerL1);
            values.ApparentPowerLTML2.dataList.add(value.ApparentPowerL2);
            values.ApparentPowerLTML3.dataList.add(value.ApparentPowerL3);
            values.ApparentPowerLTMTotal.dataList.add(value.ApparentPowerTotal);
            value.PowerFactorL1 = getModel(cursor.getString(cursor.getColumnIndex("PowerFactorL1")));
            value.PowerFactorL2 = getModel(cursor.getString(cursor.getColumnIndex("PowerFactorL2")));
            value.PowerFactorL3 = getModel(cursor.getString(cursor.getColumnIndex("PowerFactorL3")));
            value.PowerFactorTotal = getModel(cursor.getString(cursor.getColumnIndex("PowerFactorTotal")));
            values.PowerFactorLTML1.dataList.add(value.PowerFactorL1);
            values.PowerFactorLTML2.dataList.add(value.PowerFactorL2);
            values.PowerFactorLTML3.dataList.add(value.PowerFactorL3);
            values.PowerFactorLTMTotal.dataList.add(value.PowerFactorTotal);
            value.THDU1 = getModel(cursor.getString(cursor.getColumnIndex("THDU1")));
            value.THDU2 = getModel(cursor.getString(cursor.getColumnIndex("THDU2")));
            value.THDU3 = getModel(cursor.getString(cursor.getColumnIndex("THDU3")));
            value.THDI1 = getModel(cursor.getString(cursor.getColumnIndex("THDI1")));
            value.THDI2 = getModel(cursor.getString(cursor.getColumnIndex("THDI2")));
            value.THDI3 = getModel(cursor.getString(cursor.getColumnIndex("THDI3")));
            values.THDU1LTM.dataList.add(value.THDU1);
            values.THDU2LTM.dataList.add(value.THDU2);
            values.THDU3LTM.dataList.add(value.THDU3);
            values.THDI1LTM.dataList.add(value.THDI1);
            values.THDI2LTM.dataList.add(value.THDI2);
            values.THDI3LTM.dataList.add(value.THDI3);
            value.VoltageLN = getModel(cursor.getString(cursor.getColumnIndex("VoltageLN")));
            value.CurrentLN = getModel(cursor.getString(cursor.getColumnIndex("CurrentLN")));
            value.Frequency = getModel(cursor.getString(cursor.getColumnIndex("Frequency")));
            values.VoltageLTMLN.dataList.add(value.VoltageLN);
            values.CurrentLTMLN.dataList.add(value.CurrentLN);
            values.FrequencyLTM.dataList.add(value.Frequency);
            values.Times.add(value.TimeStamp.s_Value);
            values.IntegrationDatas.add(value);
        } while (cursor.moveToNext());
        return values;
    }

    private DBDataModel getModel(String strValue) {
        return new DBDataModel(OtherUtils.parseDouble(getDoubleString(strValue)), strValue);
    }

    private String getDoubleString(String strValue) {
        float multiply = 1.0f;
        String tmpValue = strValue;
        if (tmpValue.contains("m")) {
            multiply = 0.001f;
            tmpValue = tmpValue.replace("m", PdfObject.NOTHING);
        } else if (tmpValue.contains("k")) {
            multiply = 1000.0f;
            tmpValue = tmpValue.replace("k", PdfObject.NOTHING);
        } else if (tmpValue.contains("M")) {
            multiply = 1000000.0f;
            tmpValue = tmpValue.replace("M", PdfObject.NOTHING);
        }
        String tmpValue2 = tmpValue.replace("V", PdfObject.NOTHING).replace("A", PdfObject.NOTHING).replace("W", PdfObject.NOTHING).replace("var", PdfObject.NOTHING).replace("VA", PdfObject.NOTHING).replace("Hz", PdfObject.NOTHING).replace("°", PdfObject.NOTHING).replace("%", PdfObject.NOTHING).trim();
        if (DataProcessing.isNumeric(tmpValue2)) {
            return new StringBuilder(String.valueOf(OtherUtils.parseDouble(tmpValue2) * ((double) multiply))).toString();
        }
        return strValue;
    }

    /* access modifiers changed from: protected */
    @Override // com.clou.rs350.db.dao.BaseDao
    public void exportDataToCSV(Cursor cursor, String fileName) {
        String tmpString;
        String strCSV = PdfObject.NOTHING;
        try {
            String[] tmp_Names = cursor.getColumnNames();
            FileOutputStream fos = new FileOutputStream(new File(fileName), false);
            for (int i = 0; i < tmp_Names.length; i++) {
                String strCSV2 = String.valueOf(strCSV) + tmp_Names[i];
                strCSV = i < tmp_Names.length - 1 ? String.valueOf(strCSV2) + "," : String.valueOf(strCSV2) + "\n";
            }
            fos.write(strCSV.getBytes());
            while (cursor.moveToNext()) {
                String strCSV3 = PdfObject.NOTHING;
                for (int i2 = 0; i2 < tmp_Names.length; i2++) {
                    String tmpString2 = cursor.getString(cursor.getColumnIndex(tmp_Names[i2]));
                    if (tmpString2 != null) {
                        tmpString = tmpString2.replaceAll("\"", "\"\"");
                    } else {
                        tmpString = PdfObject.NOTHING;
                    }
                    String strCSV4 = (tmpString.contains(",") || tmpString.contains("\"")) ? String.valueOf(strCSV3) + "\"" + tmpString + "\"" : String.valueOf(strCSV3) + tmpString;
                    strCSV3 = i2 < tmp_Names.length - 1 ? String.valueOf(strCSV4) + "," : String.valueOf(strCSV4) + "\n";
                }
                fos.write(strCSV3.getBytes());
            }
            cursor.close();
            fos.flush();
            fos.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e2) {
            e2.printStackTrace();
        }
    }
}
