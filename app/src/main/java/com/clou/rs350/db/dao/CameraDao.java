package com.clou.rs350.db.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import com.clou.rs350.CLApplication;
import com.clou.rs350.db.model.DataBaseRecordItem;
import com.clou.rs350.db.model.sequence.CameraData;
import com.clou.rs350.db.model.sequence.CameraItem;
import com.clou.rs350.manager.DatabaseManager;
import com.itextpdf.text.pdf.PdfObject;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class CameraDao extends BaseTestDao {
    private static final String TABLE_NAME = "TestCamera";

    private static final class TableColumns {
        public static final String PhotoIndex = "PhotoIndex";
        public static final String PhotoPath = "PhotoPath";
        public static final String PhotosIndex = "PhotosIndex";

        private TableColumns() {
        }
    }

    public CameraDao(Context context) {
        super(context, TABLE_NAME);
    }

    public void createTable(SQLiteDatabase db) {
        this.m_TableName = TABLE_NAME;
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("create table if not exists ").append(this.m_TableName).append(" (");
        stringBuffer.append("intMyID").append(" integer primary key autoincrement,");
        stringBuffer.append(this.m_TableKey).append(" VARCHAR(50),");
        stringBuffer.append("CreateTime").append(" VARCHAR(50),");
        stringBuffer.append("IsRead").append(" integer default 0,");
        stringBuffer.append(TableColumns.PhotosIndex).append(" integer,");
        stringBuffer.append(TableColumns.PhotoIndex).append(" integer,");
        stringBuffer.append(TableColumns.PhotoPath).append(" VARCHAR(50));");
        db.execSQL(stringBuffer.toString());
    }

    @Override // com.clou.rs350.db.dao.BaseDao, com.clou.rs350.db.dao.BaseTestDao
    public void upgradeTable(SQLiteDatabase db, int oldVersion, int newVersion) {
        if (oldVersion <= 3) {
            db.execSQL("alter table Camera rename to TestCamera");
        }
    }

    @Override // com.clou.rs350.db.dao.BaseDao, com.clou.rs350.db.dao.BaseTestDao
    public synchronized boolean insertObject(Object Data, String Time) {
        boolean z;
        SQLiteDatabase db = DatabaseManager.getWriteableDatabase(this.m_Context);
        ContentValues values = new ContentValues();
        long status = 0;
        CameraData objects = (CameraData) Data;
        for (int i = 0; i < objects.Photos.size(); i++) {
            CameraItem object = objects.Photos.get(i);
            values.put(this.m_TableKey, objects.SerialNo.toString());
            values.put("CreateTime", Time);
            values.put(TableColumns.PhotosIndex, Integer.valueOf(objects.Index));
            values.put(TableColumns.PhotoIndex, Integer.valueOf(object.Index));
            String imagePath = CLApplication.app.getImagePath();
            File tmpPhoto = new File(String.valueOf(imagePath) + File.separator + object.PhotoPath);
            if (tmpPhoto.exists()) {
                object.PhotoPath = object.PhotoPath.replace("Tmp", (String.valueOf(objects.SerialNo) + "_" + Time).replace(" ", PdfObject.NOTHING).replace("/", PdfObject.NOTHING).replace(":", PdfObject.NOTHING));
                tmpPhoto.renameTo(new File(String.valueOf(imagePath) + File.separator + object.PhotoPath));
            }
            values.put(TableColumns.PhotoPath, object.PhotoPath);
            status += db.insert(TABLE_NAME, null, values);
        }
        if (status != -1) {
            z = true;
        } else {
            z = false;
        }
        return z;
    }

    /* access modifiers changed from: protected */
    @Override // com.clou.rs350.db.dao.BaseDao, com.clou.rs350.db.dao.BaseTestDao
    public Object parseData(Cursor cursor) {
        CameraData values = new CameraData();
        values.SerialNo = cursor.getString(cursor.getColumnIndex(this.m_TableKey));
        do {
            CameraItem value = new CameraItem();
            value.Index = cursor.getInt(cursor.getColumnIndex(TableColumns.PhotoIndex));
            value.PhotoPath = cursor.getString(cursor.getColumnIndex(TableColumns.PhotoPath));
            values.Photos.add(value);
        } while (cursor.moveToNext());
        return values;
    }

    @Override // com.clou.rs350.db.dao.BaseDao
    public List<Object> queryDataListByKeyAndTime(String serialNo, String time) {
        Cursor cursor = DatabaseManager.getWriteableDatabase(this.m_Context).query(this.m_TableName, null, String.valueOf(this.m_TableKey) + " =? and " + "CreateTime" + " =?", new String[]{serialNo, time}, null, null, null);
        List<Object> Datas = new ArrayList<>();
        Datas.add(new CameraData());
        while (cursor.moveToNext()) {
            int Index = cursor.getInt(cursor.getColumnIndex(TableColumns.PhotosIndex));
            int Size = Datas.size() - 1;
            if (Index != ((CameraData) Datas.get(Size)).Index) {
                Datas.add(new CameraData());
                Size++;
            }
            ((CameraData) Datas.get(Size)).SerialNo = cursor.getString(cursor.getColumnIndex(this.m_TableKey));
            ((CameraData) Datas.get(Size)).Index = cursor.getInt(cursor.getColumnIndex(TableColumns.PhotosIndex));
            CameraItem value = new CameraItem();
            value.Index = cursor.getInt(cursor.getColumnIndex(TableColumns.PhotoIndex));
            value.PhotoPath = cursor.getString(cursor.getColumnIndex(TableColumns.PhotoPath));
            ((CameraData) Datas.get(Size)).Photos.add(value);
        }
        cursor.close();
        if (Datas.size() > 1 || ((CameraData) Datas.get(0)).Photos.size() > 0) {
            return Datas;
        }
        return null;
    }

    @Override // com.clou.rs350.db.dao.BaseDao, com.clou.rs350.db.dao.BaseTestDao
    public void deleteRecordByKey(String serialNo, List<DataBaseRecordItem> list) {
        List<Object> Datas;
        String imagePath = CLApplication.app.getImagePath();
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).isSelect() && (Datas = queryDataListByKeyAndTime(serialNo, list.get(i).getDatetime())) != null) {
                for (int j = 0; j < Datas.size(); j++) {
                    CameraData tmpData = (CameraData) Datas.get(j);
                    for (int k = 0; k < tmpData.Photos.size(); k++) {
                        File tmpImg = new File(String.valueOf(imagePath) + File.separator + tmpData.Photos.get(k).PhotoPath);
                        if (tmpImg.exists()) {
                            tmpImg.delete();
                        }
                    }
                }
            }
        }
        super.deleteRecordByKey(serialNo, list);
    }
}
