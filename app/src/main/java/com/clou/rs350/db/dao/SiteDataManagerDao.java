package com.clou.rs350.db.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.clou.rs350.R;
import com.clou.rs350.constants.Constants;
import com.clou.rs350.db.model.MeterType;
import com.clou.rs350.db.model.SiteData;
import com.clou.rs350.manager.DatabaseManager;
import com.clou.rs350.utils.InfoUtil;

import java.sql.Date;
import java.text.SimpleDateFormat;

public class SiteDataManagerDao extends BaseDao {
    private static final String TABLE_NAME = "SiteDataInfo";

    private static final class TableColumns {
        public static final String CTAccuracyClass = "CTAccuracyClass";
        public static final String CTBurden = "CTBurden";
        public static final String CTEnable = "CTEnable";
        public static final String CTManufacturer = "CTManufacturer";
        public static final String CTPrimary_Installed = "CTPrimary_Installed";
        public static final String CTPrimary_Meter = "CTPrimary_Meter";
        public static final String CTSecondary_Installed = "CTSecondary_Installed";
        public static final String CTSecondary_Meter = "CTSecondary_Meter";
        public static final String CTType = "CTType";
        public static final String CreateTime = "CreateTime";
        public static final String CustomerSerialNo = "CustomerSerialNo";
        public static final String DeviceMeasureAt = "DeviceMeasureAt";
        public static final String ID = "intMyID";
        public static final String IntWiringType = "IntWiringType";
        public static final String IsRead = "IsRead";
        public static final String Meter1_Certification_Year = "Meter1_Certification_Year";
        public static final String Meter1_Production_Year = "Meter1_Production_Year";
        public static final String Meter1_Sr = "Meter1_Sr";
        public static final String Meter1_Type = "Meter1_Type";
        public static final String Meter2_Certification_Year = "Meter2_Certification_Year";
        public static final String Meter2_Production_Year = "Meter2_Production_Year";
        public static final String Meter2_Sr = "Meter2_Sr";
        public static final String Meter2_Type = "Meter2_Type";
        public static final String Meter3_Certification_Year = "Meter3_Certification_Year";
        public static final String Meter3_Production_Year = "Meter3_Production_Year";
        public static final String Meter3_Sr = "Meter3_Sr";
        public static final String Meter3_Type = "Meter3_Type";
        public static final String Meter_Count = "Meter_Count";
        public static final String MultiplyingFactor = "MultiplyingFactor";
        public static final String ObservedLoad = "ObservedLoad";
        public static final String PTAccuracyClass = "PTAccuracyClass";
        public static final String PTBurden = "PTBurden";
        public static final String PTEnable = "PTEnable";
        public static final String PTManufacturer = "PTManufacturer";
        public static final String PTPrimary_Installed = "PTPrimary_Installed";
        public static final String PTPrimary_Meter = "PTPrimary_Meter";
        public static final String PTSecondary_Installed = "PTSecondary_Installed";
        public static final String PTSecondary_Meter = "PTSecondary_Meter";
        public static final String RatioApply = "RatioApply";
        public static final String SanctionedLoad = "SanctionedLoad";

        private TableColumns() {
        }
    }

    public SiteDataManagerDao(Context context) {
        super(context, TABLE_NAME, "CustomerSerialNo");
    }

    public void createTable(SQLiteDatabase db) {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("create table if not exists ").append(TABLE_NAME).append(" (");
        stringBuffer.append("intMyID").append(" integer primary key autoincrement,");
        stringBuffer.append("CustomerSerialNo").append(" VARCHAR(50),");
        stringBuffer.append("CreateTime").append(" VARCHAR(50),");
        stringBuffer.append("IsRead").append(" integer default 0,");
        stringBuffer.append("Meter_Count").append(" integer default 1,");
        stringBuffer.append("Meter1_Sr").append(" VARCHAR(50),");
        stringBuffer.append("Meter1_Type").append(" VARCHAR(50),");
        stringBuffer.append("Meter1_Production_Year").append(" VARCHAR(50),");
        stringBuffer.append("Meter1_Certification_Year").append(" VARCHAR(50),");
        stringBuffer.append("Meter2_Sr").append(" VARCHAR(50),");
        stringBuffer.append("Meter2_Type").append(" VARCHAR(50),");
        stringBuffer.append("Meter2_Production_Year").append(" VARCHAR(50),");
        stringBuffer.append("Meter2_Certification_Year").append(" VARCHAR(50),");
        stringBuffer.append("Meter3_Sr").append(" VARCHAR(50),");
        stringBuffer.append("Meter3_Type").append(" VARCHAR(50),");
        stringBuffer.append("Meter3_Production_Year").append(" VARCHAR(50),");
        stringBuffer.append("Meter3_Certification_Year").append(" VARCHAR(50),");
        stringBuffer.append("IntWiringType").append(" integer default 0,");
        stringBuffer.append("PTEnable").append(" integer default 2,");
        stringBuffer.append("PTPrimary_Meter").append(" VARCHAR(20),");
        stringBuffer.append("PTSecondary_Meter").append(" VARCHAR(20),");
        stringBuffer.append("PTPrimary_Installed").append(" VARCHAR(20),");
        stringBuffer.append("PTSecondary_Installed").append(" VARCHAR(20),");
        stringBuffer.append("PTAccuracyClass").append(" VARCHAR(20),");
        stringBuffer.append("PTBurden").append(" VARCHAR(20),");
        stringBuffer.append("PTManufacturer").append(" VARCHAR(50),");
        stringBuffer.append("CTEnable").append(" integer default 2,");
        stringBuffer.append("CTType").append(" VARCHAR(20),");
        stringBuffer.append("CTPrimary_Meter").append(" VARCHAR(20),");
        stringBuffer.append("CTSecondary_Meter").append(" VARCHAR(20),");
        stringBuffer.append("CTPrimary_Installed").append(" VARCHAR(20),");
        stringBuffer.append("CTSecondary_Installed").append(" VARCHAR(20),");
        stringBuffer.append("CTAccuracyClass").append(" VARCHAR(20),");
        stringBuffer.append("CTBurden").append(" VARCHAR(20),");
        stringBuffer.append("CTManufacturer").append(" VARCHAR(50),");
        stringBuffer.append(TableColumns.DeviceMeasureAt).append(" integer default 0,");
        stringBuffer.append("RatioApply").append(" integer default 0,");
        stringBuffer.append("MultiplyingFactor").append(" VARCHAR(50),");
        stringBuffer.append("SanctionedLoad").append(" VARCHAR(50),");
        stringBuffer.append("ObservedLoad").append(" VARCHAR(50));");
        db.execSQL(stringBuffer.toString());
    }

    @Override // com.clou.rs350.db.dao.BaseDao
    public void upgradeTable(SQLiteDatabase db, int oldVersion, int newVersion) {
        if (oldVersion <= 3) {
            db.execSQL("alter table SiteDataManager rename to SiteDataInfo");
        }
        if (oldVersion <= 4) {
            db.execSQL("alter table SiteDataInfo add column PTManufacturer VARCHAR(50)");
            db.execSQL("alter table SiteDataInfo add column CTManufacturer VARCHAR(50)");
        }
        if (oldVersion < 21) {
            db.execSQL("alter table SiteDataInfo add column IntWiringType integer default 0");
        }
        if (oldVersion < 30) {
            db.execSQL("alter table SiteDataInfo add column DeviceMeasureAt integer default 0");
        }
    }

    public synchronized boolean insertObject(Object Data) {
        boolean z = true;
        synchronized (this) {
            SQLiteDatabase db = DatabaseManager.getWriteableDatabase(this.m_Context);
            ContentValues values = new ContentValues();
            Date SaveTime = new Date(System.currentTimeMillis());
            SimpleDateFormat sDateFormat = new SimpleDateFormat(Constants.TIMEFORMAT);
            SiteData object = (SiteData) Data;
            values.put("CustomerSerialNo", object.CustomerSr);
            values.put("CreateTime", sDateFormat.format(SaveTime).toString());
            values.put("Meter_Count", Integer.valueOf(object.Meter_Count));
            values.put("Meter1_Sr", object.MeterInfo[0].SerialNo);
            values.put("Meter1_Type", object.MeterInfo[0].Type);
            values.put("Meter1_Production_Year", object.MeterInfo[0].ProductionYear);
            values.put("Meter1_Certification_Year", object.Meter_CertificationYear[0]);
            values.put("Meter2_Sr", object.MeterInfo[1].SerialNo);
            values.put("Meter2_Type", object.MeterInfo[1].Type);
            values.put("Meter2_Production_Year", object.MeterInfo[1].ProductionYear);
            values.put("Meter2_Certification_Year", object.Meter_CertificationYear[1]);
            values.put("Meter3_Sr", object.MeterInfo[2].SerialNo);
            values.put("Meter3_Type", object.MeterInfo[2].Type);
            values.put("Meter3_Production_Year", object.MeterInfo[2].ProductionYear);
            values.put("Meter3_Certification_Year", object.Meter_CertificationYear[2]);
            values.put("IntWiringType", Integer.valueOf(object.Wiring));
            values.put("PTEnable", Integer.valueOf(object.PTEnable));
            values.put("PTPrimary_Meter", object.PTPrimary.s_Value);
            values.put("PTSecondary_Meter", object.PTSecondary.s_Value);
            values.put("PTPrimary_Installed", object.PTPrimary_Installed.s_Value);
            values.put("PTSecondary_Installed", object.PTSecondary_Installed.s_Value);
            values.put("PTAccuracyClass", object.PTAccuracyClass);
            values.put("PTBurden", object.PTBurden.s_Value);
            values.put("PTManufacturer", object.PTManufacturer);
            values.put("CTEnable", Integer.valueOf(object.CTEnable));
            values.put("CTType", object.CTType);
            values.put("CTPrimary_Meter", object.CTPrimary.s_Value);
            values.put("CTSecondary_Meter", object.CTSecondary.s_Value);
            values.put("CTPrimary_Installed", object.CTPrimary_Installed.s_Value);
            values.put("CTSecondary_Installed", object.CTSecondary_Installed.s_Value);
            values.put("CTAccuracyClass", object.CTAccuracyClass);
            values.put("CTBurden", object.CTBurden.s_Value);
            values.put("CTManufacturer", object.CTManufacturer);
            values.put(TableColumns.DeviceMeasureAt, Integer.valueOf(object.Measurement_Position));
            values.put("RatioApply", Integer.valueOf(object.RatioApply));
            values.put("MultiplyingFactor", object.MultiplyingFactor);
            values.put("SanctionedLoad", object.SanctionedLoad);
            values.put("ObservedLoad", object.ObservedLoad);
            if (0 + db.insert(TABLE_NAME, null, values) == -1) {
                z = false;
            }
        }
        return z;
    }

    public void updateObjectByKey(String serialNo, SiteData object) {
        SQLiteDatabase db = DatabaseManager.getWriteableDatabase(this.m_Context);
        ContentValues values = new ContentValues();
        values.put("CustomerSerialNo", object.CustomerSr);
        values.put("Meter_Count", Integer.valueOf(object.Meter_Count));
        values.put("Meter1_Sr", object.MeterInfo[0].SerialNo);
        values.put("Meter1_Type", object.MeterInfo[0].Type);
        values.put("Meter1_Production_Year", object.MeterInfo[0].ProductionYear);
        values.put("Meter1_Certification_Year", object.Meter_CertificationYear[0]);
        values.put("Meter2_Sr", object.MeterInfo[1].SerialNo);
        values.put("Meter2_Type", object.MeterInfo[1].Type);
        values.put("Meter2_Production_Year", object.MeterInfo[1].ProductionYear);
        values.put("Meter2_Certification_Year", object.Meter_CertificationYear[1]);
        values.put("Meter3_Sr", object.MeterInfo[2].SerialNo);
        values.put("Meter3_Type", object.MeterInfo[2].Type);
        values.put("Meter3_Production_Year", object.MeterInfo[2].ProductionYear);
        values.put("Meter3_Certification_Year", object.Meter_CertificationYear[2]);
        values.put("IntWiringType", Integer.valueOf(object.Wiring));
        values.put("PTEnable", Integer.valueOf(object.PTEnable));
        values.put("PTPrimary_Meter", object.PTPrimary.s_Value);
        values.put("PTSecondary_Meter", object.PTSecondary.s_Value);
        values.put("PTPrimary_Installed", object.PTPrimary_Installed.s_Value);
        values.put("PTSecondary_Installed", object.PTSecondary_Installed.s_Value);
        values.put("PTAccuracyClass", object.PTAccuracyClass);
        values.put("PTBurden", object.PTBurden.s_Value);
        values.put("PTManufacturer", object.PTManufacturer);
        values.put("CTEnable", Integer.valueOf(object.CTEnable));
        values.put("CTType", object.CTType);
        values.put("CTPrimary_Meter", object.CTPrimary.s_Value);
        values.put("CTSecondary_Meter", object.CTSecondary.s_Value);
        values.put("CTPrimary_Installed", object.CTPrimary_Installed.s_Value);
        values.put("CTSecondary_Installed", object.CTSecondary_Installed.s_Value);
        values.put("CTAccuracyClass", object.CTAccuracyClass);
        values.put("CTBurden", object.CTBurden.s_Value);
        values.put("CTManufacturer", object.CTManufacturer);
        values.put(TableColumns.DeviceMeasureAt, Integer.valueOf(object.Measurement_Position));
        values.put("RatioApply", Integer.valueOf(object.RatioApply));
        values.put("MultiplyingFactor", object.MultiplyingFactor);
        values.put("SanctionedLoad", object.SanctionedLoad);
        values.put("ObservedLoad", object.ObservedLoad);
        values.put("CreateTime", String.valueOf(System.currentTimeMillis()));
        values.put("IsRead", (Integer) 0);
        db.update(TABLE_NAME, values, "CustomerSerialNo = ?", new String[]{serialNo});
    }

    public boolean queryIfExistBySerialNo(String serialNo) {
        boolean value = false;
        Cursor cursor = DatabaseManager.getWriteableDatabase(this.m_Context).rawQuery("select * from SiteDataInfo where CustomerSerialNo = ?", new String[]{serialNo});
        if (cursor.moveToNext()) {
            value = true;
        }
        cursor.close();
        return value;
    }

    public Object queryObjectBySerialNo(String serialNo) {
        Object value = null;
        Cursor cursor = DatabaseManager.getWriteableDatabase(this.m_Context).rawQuery("select * from SiteDataInfo where CustomerSerialNo = ?", new String[]{serialNo});
        if (cursor.moveToNext()) {
            value = parseData(cursor);
        }
        cursor.close();
        return value;
    }

    /* access modifiers changed from: protected */
    @Override // com.clou.rs350.db.dao.BaseDao
    public Object parseData(Cursor cursor) {
        SiteData value = new SiteData();
        value.CustomerSr = parseString(cursor.getString(cursor.getColumnIndex("CustomerSerialNo")));
        value.Meter_Count = cursor.getInt(cursor.getColumnIndex("Meter_Count"));
        value.MeterInfo[0].SerialNo = parseString(cursor.getString(cursor.getColumnIndex("Meter1_Sr")));
        value.MeterInfo[0].Type = parseString(cursor.getString(cursor.getColumnIndex("Meter1_Type")));
        MeterType tmpType = new MeterTypeDao(this.m_Context).queryObjectByType(value.MeterInfo[0].Type);
        if (tmpType != null) {
            value.MeterInfo[0].CopyTypeData(tmpType);
        }
        value.MeterInfo[0].ProductionYear = parseString(cursor.getString(cursor.getColumnIndex("Meter1_Production_Year")));
        value.Meter_CertificationYear[0] = parseString(cursor.getString(cursor.getColumnIndex("Meter1_Certification_Year")));
        value.MeterInfo[1].SerialNo = parseString(cursor.getString(cursor.getColumnIndex("Meter2_Sr")));
        value.MeterInfo[1].Type = parseString(cursor.getString(cursor.getColumnIndex("Meter2_Type")));
        MeterType tmpType2 = new MeterTypeDao(this.m_Context).queryObjectByType(value.MeterInfo[1].Type);
        if (tmpType2 != null) {
            value.MeterInfo[1].CopyTypeData(tmpType2);
        }
        value.MeterInfo[1].ProductionYear = parseString(cursor.getString(cursor.getColumnIndex("Meter2_Production_Year")));
        value.Meter_CertificationYear[1] = parseString(cursor.getString(cursor.getColumnIndex("Meter2_Certification_Year")));
        value.MeterInfo[2].SerialNo = parseString(cursor.getString(cursor.getColumnIndex("Meter3_Sr")));
        value.MeterInfo[2].Type = parseString(cursor.getString(cursor.getColumnIndex("Meter3_Type")));
        MeterType tmpType3 = new MeterTypeDao(this.m_Context).queryObjectByType(value.MeterInfo[2].Type);
        if (tmpType3 != null) {
            value.MeterInfo[2].CopyTypeData(tmpType3);
        }
        value.MeterInfo[2].ProductionYear = parseString(cursor.getString(cursor.getColumnIndex("Meter3_Production_Year")));
        value.Meter_CertificationYear[2] = parseString(cursor.getString(cursor.getColumnIndex("Meter3_Certification_Year")));
        value.Wiring = cursor.getInt(cursor.getColumnIndex("IntWiringType"));
        value.WiringType = this.m_Context.getResources().getStringArray(R.array.type_of_connection)[value.Wiring];
        value.PTEnable = cursor.getInt(cursor.getColumnIndex("PTEnable"));
        value.CTEnable = cursor.getInt(cursor.getColumnIndex("CTEnable"));
        value.PTPrimary = InfoUtil.parseVoltageRatio(parseString(cursor.getString(cursor.getColumnIndex("PTPrimary_Meter"))), true);
        value.PTSecondary = InfoUtil.parseVoltageRatio(parseString(cursor.getString(cursor.getColumnIndex("PTSecondary_Meter"))), false);
        value.PTPrimary_Installed = InfoUtil.parseVoltageRatio(parseString(cursor.getString(cursor.getColumnIndex("PTPrimary_Installed"))), true);
        value.PTSecondary_Installed = InfoUtil.parseVoltageRatio(parseString(cursor.getString(cursor.getColumnIndex("PTSecondary_Installed"))), false);
        value.PTAccuracyClass = parseString(cursor.getString(cursor.getColumnIndex("PTAccuracyClass")));
        value.PTBurden = InfoUtil.parseBurden(parseString(cursor.getString(cursor.getColumnIndex("PTBurden"))));
        value.PTManufacturer = parseString(cursor.getString(cursor.getColumnIndex("PTManufacturer")));
        value.CTType = parseString(cursor.getString(cursor.getColumnIndex("CTType")));
        value.CTPrimary = InfoUtil.parseCurrentRatio(parseString(cursor.getString(cursor.getColumnIndex("CTPrimary_Meter"))));
        value.CTSecondary = InfoUtil.parseCurrentRatio(parseString(cursor.getString(cursor.getColumnIndex("CTSecondary_Meter"))));
        value.CTPrimary_Installed = InfoUtil.parseCurrentRatio(parseString(cursor.getString(cursor.getColumnIndex("CTPrimary_Installed"))));
        value.CTSecondary_Installed = InfoUtil.parseCurrentRatio(parseString(cursor.getString(cursor.getColumnIndex("CTSecondary_Installed"))));
        value.CTAccuracyClass = parseString(cursor.getString(cursor.getColumnIndex("CTAccuracyClass")));
        value.CTBurden = InfoUtil.parseBurden(parseString(cursor.getString(cursor.getColumnIndex("CTBurden"))));
        value.CTManufacturer = parseString(cursor.getString(cursor.getColumnIndex("CTManufacturer")));
        value.Measurement_Position = cursor.getInt(cursor.getColumnIndex(TableColumns.DeviceMeasureAt));
        value.RatioApply = cursor.getInt(cursor.getColumnIndex("RatioApply"));
        value.MultiplyingFactor = parseString(cursor.getString(cursor.getColumnIndex("MultiplyingFactor")));
        value.SanctionedLoad = parseString(cursor.getString(cursor.getColumnIndex("SanctionedLoad")));
        value.ObservedLoad = parseString(cursor.getString(cursor.getColumnIndex("ObservedLoad")));
        return value;
    }
}
