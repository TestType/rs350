package com.clou.rs350.db.model;

import com.clou.rs350.device.MeterBaseDevice;
import com.clou.rs350.ui.model.LTMData;
import com.itextpdf.text.pdf.PdfObject;
import java.util.ArrayList;
import java.util.List;

public class LTM extends Range {
    public LTMData ActivePowerLTML1 = new LTMData("P1");
    public LTMData ActivePowerLTML2 = new LTMData("P2");
    public LTMData ActivePowerLTML3 = new LTMData("P3");
    public LTMData ActivePowerLTMTotal = new LTMData("ΣP");
    public LTMData AngleLTMU12I1 = new LTMData("φU12I1");
    public LTMData AngleLTMU23I2 = new LTMData("φU23I2");
    public LTMData AngleLTMU31I3 = new LTMData("φU31I3");
    public LTMData ApparentPowerLTML1 = new LTMData("S1");
    public LTMData ApparentPowerLTML2 = new LTMData("S2");
    public LTMData ApparentPowerLTML3 = new LTMData("S3");
    public LTMData ApparentPowerLTMTotal = new LTMData("ΣS");
    public LTMData CurrentAngleLTML1 = new LTMData("φI1");
    public LTMData CurrentAngleLTML2 = new LTMData("φI2");
    public LTMData CurrentAngleLTML3 = new LTMData("φI3");
    public LTMData CurrentLTML1 = new LTMData("I1");
    public LTMData CurrentLTML2 = new LTMData("I2");
    public LTMData CurrentLTML3 = new LTMData("I3");
    public LTMData CurrentLTMLN = new LTMData("In");
    public DBDataModel DefindStartTime = new DBDataModel();
    public DBDataModel DefindStopTime = new DBDataModel();
    public LTMData FrequencyLTM = new LTMData("F");
    public List<IntegrationData> IntegrationDatas = new ArrayList();
    public DBDataModel Interval = new DBDataModel(1L, "1");
    public DBDataModel Period = new DBDataModel(1L, "1");
    public LTMData PowerFactorLTML1 = new LTMData("PF1");
    public LTMData PowerFactorLTML2 = new LTMData("PF2");
    public LTMData PowerFactorLTML3 = new LTMData("PF3");
    public LTMData PowerFactorLTMTotal = new LTMData("ΣPF");
    public LTMData ReactivePowerLTML1 = new LTMData("Q1");
    public LTMData ReactivePowerLTML2 = new LTMData("Q2");
    public LTMData ReactivePowerLTML3 = new LTMData("Q3");
    public LTMData ReactivePowerLTMTotal = new LTMData("ΣQ");
    public String SaveTime = PdfObject.NOTHING;
    public String SerialNo = PdfObject.NOTHING;
    public LTMData THDI1LTM = new LTMData("LTMI1");
    public LTMData THDI2LTM = new LTMData("LTMI2");
    public LTMData THDI3LTM = new LTMData("LTMI3");
    public LTMData THDU1LTM = new LTMData("LTMU1");
    public LTMData THDU2LTM = new LTMData("LTMU2");
    public LTMData THDU3LTM = new LTMData("LTMU3");
    public List<String> Times = new ArrayList();
    public LTMData VoltageAngleLTML1 = new LTMData("φU1");
    public LTMData VoltageAngleLTML1L2 = new LTMData("φU12");
    public LTMData VoltageAngleLTML2 = new LTMData("φU2");
    public LTMData VoltageAngleLTML2L3 = new LTMData("φU23");
    public LTMData VoltageAngleLTML3 = new LTMData("φU3");
    public LTMData VoltageAngleLTML3L1 = new LTMData("φU31");
    public LTMData VoltageCurrentAngleLTML1 = new LTMData("φUI1");
    public LTMData VoltageCurrentAngleLTML2 = new LTMData("φUI2");
    public LTMData VoltageCurrentAngleLTML3 = new LTMData("φUI3");
    public LTMData VoltageLTML1 = new LTMData("U1");
    public LTMData VoltageLTML1L2 = new LTMData("U12");
    public LTMData VoltageLTML2 = new LTMData("U2");
    public LTMData VoltageLTML2L3 = new LTMData("U13");
    public LTMData VoltageLTML3 = new LTMData("U3");
    public LTMData VoltageLTML3L1 = new LTMData("U11");
    public LTMData VoltageLTMLN = new LTMData("Un");
    public boolean WithDefindTime = false;
    private boolean m_HasData = false;
    private boolean m_IsParse = false;

    public LTM() {
    }

    public LTM(MeterBaseDevice Device) {
        parseData(Device);
    }

    public void parseData(MeterBaseDevice Device) {
        if (1 == Device.getIntegrationFlagValue()) {
            this.m_IsParse = true;
            if (!this.m_HasData) {
                this.m_HasData = true;
                return;
            }
            IntegrationData tmpData = new IntegrationData(Device);
            this.VoltageLTML1.dataList.add(tmpData.VoltageValueL1);
            this.VoltageLTML2.dataList.add(tmpData.VoltageValueL2);
            this.VoltageLTML3.dataList.add(tmpData.VoltageValueL3);
            this.CurrentLTML1.dataList.add(tmpData.CurrentValueL1);
            this.CurrentLTML2.dataList.add(tmpData.CurrentValueL2);
            this.CurrentLTML3.dataList.add(tmpData.CurrentValueL3);
            this.VoltageLTML1L2.dataList.add(tmpData.VoltageValueL1L2);
            this.VoltageLTML2L3.dataList.add(tmpData.VoltageValueL2L3);
            this.VoltageLTML3L1.dataList.add(tmpData.VoltageValueL3L1);
            this.VoltageAngleLTML1.dataList.add(tmpData.VoltageAngleL1);
            this.VoltageAngleLTML2.dataList.add(tmpData.VoltageAngleL2);
            this.VoltageAngleLTML3.dataList.add(tmpData.VoltageAngleL3);
            this.CurrentAngleLTML1.dataList.add(tmpData.CurrentAngleL1);
            this.CurrentAngleLTML2.dataList.add(tmpData.CurrentAngleL2);
            this.CurrentAngleLTML3.dataList.add(tmpData.CurrentAngleL3);
            this.VoltageAngleLTML1L2.dataList.add(tmpData.VoltageAngleL1L2);
            this.VoltageAngleLTML2L3.dataList.add(tmpData.VoltageAngleL2L3);
            this.VoltageAngleLTML3L1.dataList.add(tmpData.VoltageAngleL3L1);
            this.VoltageCurrentAngleLTML1.dataList.add(tmpData.VoltageCurrentAngleL1);
            this.VoltageCurrentAngleLTML2.dataList.add(tmpData.VoltageCurrentAngleL2);
            this.VoltageCurrentAngleLTML3.dataList.add(tmpData.VoltageCurrentAngleL3);
            this.AngleLTMU12I1.dataList.add(tmpData.AngleU12I1);
            this.AngleLTMU23I2.dataList.add(tmpData.AngleU23I2);
            this.AngleLTMU31I3.dataList.add(tmpData.AngleU31I3);
            this.ActivePowerLTML1.dataList.add(tmpData.ActivePowerL1);
            this.ActivePowerLTML2.dataList.add(tmpData.ActivePowerL2);
            this.ActivePowerLTML3.dataList.add(tmpData.ActivePowerL3);
            this.ActivePowerLTMTotal.dataList.add(tmpData.ActivePowerTotal);
            this.ReactivePowerLTML1.dataList.add(tmpData.ReactivePowerL1);
            this.ReactivePowerLTML2.dataList.add(tmpData.ReactivePowerL2);
            this.ReactivePowerLTML3.dataList.add(tmpData.ReactivePowerL3);
            this.ReactivePowerLTMTotal.dataList.add(tmpData.ReactivePowerTotal);
            this.ApparentPowerLTML1.dataList.add(tmpData.ApparentPowerL1);
            this.ApparentPowerLTML2.dataList.add(tmpData.ApparentPowerL2);
            this.ApparentPowerLTML3.dataList.add(tmpData.ApparentPowerL3);
            this.ApparentPowerLTMTotal.dataList.add(tmpData.ApparentPowerTotal);
            this.PowerFactorLTML1.dataList.add(tmpData.PowerFactorL1);
            this.PowerFactorLTML2.dataList.add(tmpData.PowerFactorL2);
            this.PowerFactorLTML3.dataList.add(tmpData.PowerFactorL3);
            this.PowerFactorLTMTotal.dataList.add(tmpData.PowerFactorTotal);
            this.THDU1LTM.dataList.add(tmpData.THDU1);
            this.THDU2LTM.dataList.add(tmpData.THDU2);
            this.THDU3LTM.dataList.add(tmpData.THDU3);
            this.THDI1LTM.dataList.add(tmpData.THDI1);
            this.THDI2LTM.dataList.add(tmpData.THDI2);
            this.THDI3LTM.dataList.add(tmpData.THDI3);
            this.VoltageLTMLN.dataList.add(tmpData.VoltageLN);
            this.CurrentLTMLN.dataList.add(tmpData.CurrentLN);
            this.FrequencyLTM.dataList.add(tmpData.Frequency);
            this.Times.add(tmpData.TimeStamp.s_Value);
            this.IntegrationDatas.add(tmpData);
        }
    }

    public void cleanData() {
        this.IntegrationDatas = new ArrayList();
        this.VoltageLTML1 = new LTMData("U1");
        this.VoltageLTML2 = new LTMData("U2");
        this.VoltageLTML3 = new LTMData("U3");
        this.CurrentLTML1 = new LTMData("I1");
        this.CurrentLTML2 = new LTMData("I2");
        this.CurrentLTML3 = new LTMData("I3");
        this.VoltageLTML1L2 = new LTMData("U12");
        this.VoltageLTML2L3 = new LTMData("U13");
        this.VoltageLTML3L1 = new LTMData("U11");
        this.VoltageAngleLTML1 = new LTMData("φU1");
        this.VoltageAngleLTML2 = new LTMData("φU2");
        this.VoltageAngleLTML3 = new LTMData("φU3");
        this.CurrentAngleLTML1 = new LTMData("φI1");
        this.CurrentAngleLTML2 = new LTMData("φI2");
        this.CurrentAngleLTML3 = new LTMData("φI3");
        this.VoltageAngleLTML1L2 = new LTMData("φU12");
        this.VoltageAngleLTML2L3 = new LTMData("φU23");
        this.VoltageAngleLTML3L1 = new LTMData("φU31");
        this.VoltageCurrentAngleLTML1 = new LTMData("φUI1");
        this.VoltageCurrentAngleLTML2 = new LTMData("φUI2");
        this.VoltageCurrentAngleLTML3 = new LTMData("φUI3");
        this.AngleLTMU12I1 = new LTMData("φU12I1");
        this.AngleLTMU23I2 = new LTMData("φU23I2");
        this.AngleLTMU31I3 = new LTMData("φU31I3");
        this.ActivePowerLTML1 = new LTMData("P1");
        this.ActivePowerLTML2 = new LTMData("P2");
        this.ActivePowerLTML3 = new LTMData("P3");
        this.ActivePowerLTMTotal = new LTMData("ΣP");
        this.ReactivePowerLTML1 = new LTMData("Q1");
        this.ReactivePowerLTML2 = new LTMData("Q2");
        this.ReactivePowerLTML3 = new LTMData("Q3");
        this.ReactivePowerLTMTotal = new LTMData("ΣQ");
        this.ApparentPowerLTML1 = new LTMData("S1");
        this.ApparentPowerLTML2 = new LTMData("S2");
        this.ApparentPowerLTML3 = new LTMData("S3");
        this.ApparentPowerLTMTotal = new LTMData("ΣS");
        this.PowerFactorLTML1 = new LTMData("PF1");
        this.PowerFactorLTML2 = new LTMData("PF2");
        this.PowerFactorLTML3 = new LTMData("PF3");
        this.PowerFactorLTMTotal = new LTMData("ΣPF");
        this.THDU1LTM = new LTMData("LTMU1");
        this.THDU2LTM = new LTMData("LTMU2");
        this.THDU3LTM = new LTMData("LTMU3");
        this.THDI1LTM = new LTMData("LTMI1");
        this.THDI2LTM = new LTMData("LTMI2");
        this.THDI3LTM = new LTMData("LTMI3");
        this.VoltageLTMLN = new LTMData("Un");
        this.CurrentLTMLN = new LTMData("In");
        this.FrequencyLTM = new LTMData("F");
        this.Times = new ArrayList();
        this.m_HasData = false;
        this.m_IsParse = false;
    }

    public boolean hasData() {
        return this.m_HasData;
    }

    public boolean isParse() {
        boolean flag = this.m_IsParse;
        this.m_IsParse = false;
        return flag;
    }

    public IntegrationData getNewData() {
        if (this.IntegrationDatas.size() > 0) {
            return this.IntegrationDatas.get(this.IntegrationDatas.size() - 1);
        }
        return new IntegrationData();
    }
}
