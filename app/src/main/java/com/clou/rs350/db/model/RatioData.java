package com.clou.rs350.db.model;

import com.itextpdf.text.pdf.PdfObject;

public class RatioData extends MeterType {
    public String CTAccuracyClass = PdfObject.NOTHING;
    public DBDataModel CTBurden = new DBDataModel();
    public int CTEnable = 2;
    public String CTManufacturer = PdfObject.NOTHING;
    public DBDataModel CTPrimary = new DBDataModel(1L, PdfObject.NOTHING);
    public DBDataModel CTSecondary = new DBDataModel(1L, PdfObject.NOTHING);
    public String CTType = PdfObject.NOTHING;
    public int IsRead = 0;
    public int Measurement_Position = 0;
    public String PTAccuracyClass = PdfObject.NOTHING;
    public DBDataModel PTBurden = new DBDataModel();
    public int PTEnable = 2;
    public String PTManufacturer = PdfObject.NOTHING;
    public DBDataModel PTPrimary = new DBDataModel(1L, PdfObject.NOTHING);
    public DBDataModel PTSecondary = new DBDataModel(1L, PdfObject.NOTHING);
    public int RatioApply = 0;
    public boolean isNew = true;

    public void CopyRatioData(RatioData Data) {
        this.PTEnable = Data.PTEnable;
        this.PTPrimary = Data.PTPrimary;
        this.PTSecondary = Data.PTSecondary;
        this.PTAccuracyClass = Data.PTAccuracyClass;
        this.PTBurden = Data.PTBurden;
        this.PTManufacturer = Data.PTManufacturer;
        this.CTEnable = Data.CTEnable;
        this.CTType = Data.CTType;
        this.CTPrimary = Data.CTPrimary;
        this.CTSecondary = Data.CTSecondary;
        this.CTAccuracyClass = Data.CTAccuracyClass;
        this.CTBurden = Data.CTBurden;
        this.CTManufacturer = Data.CTManufacturer;
        this.RatioApply = Data.RatioApply;
        this.isNew = Data.isNew;
        CopyTypeData(Data);
    }
}
