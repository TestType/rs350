package com.clou.rs350.db.model;

import android.content.Context;

import com.clou.rs350.Preferences;
import com.clou.rs350.R;
import com.clou.rs350.constants.Constants;
import com.clou.rs350.utils.DeviceUtil;
import com.itextpdf.text.pdf.PdfObject;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

public class StandardInfo implements Serializable {
    public String Accuracy = "0.05";
    public String AppVersion = PdfObject.NOTHING;
    public Date Calib_Valid_Date = null;
    public Date CalibrateDate = null;
    public String Calibrated_By = PdfObject.NOTHING;
    public String CertificateID = PdfObject.NOTHING;
    public String FirmwareVersion = PdfObject.NOTHING;
    public int Function = 0;
    public String MainBoardVersion = PdfObject.NOTHING;
    public String Manufacturer = PdfObject.NOTHING;
    public String MeasureBoardVersion = PdfObject.NOTHING;
    public String NeutralBoardVersion = PdfObject.NOTHING;
    public String StandardSerialNo = PdfObject.NOTHING;
    private Context m_Context;

    public StandardInfo(Context context) {
        this.m_Context = context;
    }

    public void setStandardSerialNo(String standardSerialNo) {
        this.StandardSerialNo = standardSerialNo;
    }

    public void setFirmwareVersion(String FirmwareVersion2) {
        this.FirmwareVersion = FirmwareVersion2;
    }

    public String getCalibrateDate() {
        if (this.CalibrateDate != null) {
            return new SimpleDateFormat(Constants.CALIBARTTIMEFORMAT).format(this.CalibrateDate).toString();
        }
        return PdfObject.NOTHING;
    }

    public String getCalib_Valid_Date() {
        if (this.Calib_Valid_Date != null) {
            return new SimpleDateFormat(Constants.CALIBARTTIMEFORMAT).format(this.Calib_Valid_Date).toString();
        }
        return PdfObject.NOTHING;
    }

    public void loadInfo() {
        this.Manufacturer = this.m_Context.getResources().getString(R.string.text_start_bottom);
        this.AppVersion = DeviceUtil.getVersionName(this.m_Context);
        this.FirmwareVersion = Preferences.getString(Preferences.Key.FIRMWAREVERSION, PdfObject.NOTHING);
        this.MainBoardVersion = Preferences.getString(Preferences.Key.MAINBOARDVERSION, PdfObject.NOTHING);
        this.MeasureBoardVersion = Preferences.getString(Preferences.Key.MEASUREBOARDVERSION, PdfObject.NOTHING);
        this.NeutralBoardVersion = Preferences.getString(Preferences.Key.NEUTRALBOARDVERSION, PdfObject.NOTHING);
    }

    public void saveInfo() {
        Preferences.putString(Preferences.Key.FIRMWAREVERSION, this.FirmwareVersion);
        Preferences.putString(Preferences.Key.MAINBOARDVERSION, this.MainBoardVersion);
        Preferences.putString(Preferences.Key.MEASUREBOARDVERSION, this.MeasureBoardVersion);
        Preferences.putString(Preferences.Key.NEUTRALBOARDVERSION, this.NeutralBoardVersion);
    }
}
