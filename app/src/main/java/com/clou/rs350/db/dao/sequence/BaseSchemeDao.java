package com.clou.rs350.db.dao.sequence;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import com.clou.rs350.db.dao.BaseDao;
import com.clou.rs350.manager.DatabaseManager;
import com.itextpdf.text.pdf.PdfObject;
import java.util.ArrayList;
import java.util.List;

public class BaseSchemeDao extends BaseDao {
    protected String m_TableOrder = PdfObject.NOTHING;

    protected static final class SchemeTableColumns {
        public static final String Language = "Language";

        protected SchemeTableColumns() {
        }
    }

    public BaseSchemeDao(Context context, String tableName, String key) {
        super(context, tableName, key);
    }

    public Object queryByKey(String key) {
        Cursor cursor = DatabaseManager.getWriteableDatabase(this.m_Context).query(this.m_TableName, null, String.valueOf(this.m_TableKey) + " =? ", new String[]{key}, null, null, this.m_TableOrder);
        Object Data = null;
        if (cursor.moveToFirst()) {
            Data = parseData(cursor);
        }
        cursor.close();
        return Data;
    }

    @Override // com.clou.rs350.db.dao.BaseDao
    public void upgradeTable(SQLiteDatabase db, int oldVersion, int newVersion) {
        if (oldVersion < 8) {
            db.execSQL("alter table " + this.m_TableName + " add column " + SchemeTableColumns.Language + " integer default 0");
        }
    }

    public List<String> queryAllKey(int language) {
        List<String> values = new ArrayList<>();
        SQLiteDatabase db = DatabaseManager.getWriteableDatabase(this.m_Context);
        String[] selectionArgs = {new StringBuilder(String.valueOf(language)).toString()};
        Cursor cursor = db.query(this.m_TableName, new String[]{this.m_TableKey}, "Language =? ", selectionArgs, null, null, null);
        while (cursor.moveToNext()) {
            String tmpName = cursor.getString(0);
            if (!values.contains(tmpName)) {
                values.add(tmpName);
            }
        }
        cursor.close();
        return values;
    }

    public List<String> queryAllKey(String condition, int language) {
        if (condition.isEmpty()) {
            return queryAllKey(language);
        }
        List<String> values = new ArrayList<>();
        Cursor cursor = DatabaseManager.getWriteableDatabase(this.m_Context).query(this.m_TableName, new String[]{this.m_TableKey}, String.valueOf(this.m_TableKey) + " LIKE ? and " + SchemeTableColumns.Language + " =? ", new String[]{condition, new StringBuilder(String.valueOf(language)).toString()}, null, null, null);
        while (cursor.moveToNext()) {
            String tmpName = cursor.getString(0);
            if (!values.contains(tmpName)) {
                values.add(tmpName);
            }
        }
        cursor.close();
        return values;
    }
}
