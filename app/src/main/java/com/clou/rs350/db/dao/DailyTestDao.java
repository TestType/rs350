package com.clou.rs350.db.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import com.clou.rs350.db.dao.BaseTestDao;
import com.clou.rs350.db.model.DailyTest;
import com.clou.rs350.manager.DatabaseManager;
import com.clou.rs350.utils.OtherUtils;

public class DailyTestDao extends BaseTestDao {
    public static final String TABLE_NAME = "TestDailyTest";

    private static final class TableColumns {
        public static final String ConstantUnit = "ConstantUnit";
        public static final String Error1 = "Error1";
        public static final String Error10 = "Error10";
        public static final String Error2 = "Error2";
        public static final String Error3 = "Error3";
        public static final String Error4 = "Error4";
        public static final String Error5 = "Error5";
        public static final String Error6 = "Error6";
        public static final String Error7 = "Error7";
        public static final String Error8 = "Error8";
        public static final String Error9 = "Error9";
        public static final String ErrorAverage = "ErrorAverage";
        public static final String ErrorCount = "ErrorCount";
        public static final String ErrorMore = "ErrorMore";
        public static final String ErrorStandard = "ErrorStandard";
        public static final String MeterConstant = "MeterConstant";
        public static final String MeterIndex = "MeterIndex";
        public static final String PulseSamplingMethod = "PulseSamplingMethod";
        public static final String Pulses = "Pulses";
        public static final String Result = "Result";
        public static final String RunTime = "RunTime";

        private TableColumns() {
        }
    }

    public DailyTestDao(Context context) {
        super(context, TABLE_NAME);
    }

    public void createTable(SQLiteDatabase db) {
        this.m_TableName = TABLE_NAME;
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append(getCreatTableString());
        stringBuffer.append(SameColumns.MeterSerialNo).append(" VARCHAR(50),");
        stringBuffer.append("MeterIndex").append(" VARCHAR(5),");
        stringBuffer.append("PulseSamplingMethod").append(" VARCHAR(50),");
        stringBuffer.append("MeterConstant").append(" VARCHAR(50),");
        stringBuffer.append("ConstantUnit").append(" VARCHAR(50),");
        stringBuffer.append("Pulses").append(" VARCHAR(50),");
        stringBuffer.append("ErrorCount").append(" VARCHAR(10),");
        stringBuffer.append("RunTime").append(" VARCHAR(50),");
        stringBuffer.append("Error1").append(" VARCHAR(20),");
        stringBuffer.append("Error2").append(" VARCHAR(20),");
        stringBuffer.append("Error3").append(" VARCHAR(20),");
        stringBuffer.append("Error4").append(" VARCHAR(20),");
        stringBuffer.append("Error5").append(" VARCHAR(20),");
        stringBuffer.append("Error6").append(" VARCHAR(20),");
        stringBuffer.append("Error7").append(" VARCHAR(20),");
        stringBuffer.append("Error8").append(" VARCHAR(20),");
        stringBuffer.append("Error9").append(" VARCHAR(20),");
        stringBuffer.append("Error10").append(" VARCHAR(20),");
        stringBuffer.append("ErrorMore").append(" VARCHAR(200),");
        stringBuffer.append("ErrorAverage").append(" VARCHAR(20),");
        stringBuffer.append("ErrorStandard").append(" VARCHAR(20),");
        stringBuffer.append("Result").append(" VARCHAR(20));");
        db.execSQL(stringBuffer.toString());
    }

    @Override // com.clou.rs350.db.dao.BaseDao, com.clou.rs350.db.dao.BaseTestDao
    public void upgradeTable(SQLiteDatabase db, int oldVersion, int newVersion) {
        if (oldVersion < 11) {
            createTable(db);
        }
        super.upgradeTable(db, oldVersion, newVersion);
    }

    @Override // com.clou.rs350.db.dao.BaseDao, com.clou.rs350.db.dao.BaseTestDao
    public synchronized boolean insertObject(Object Data, String Time) {
        boolean z = true;
        synchronized (this) {
            SQLiteDatabase db = DatabaseManager.getWriteableDatabase(this.m_Context);
            ContentValues values = new ContentValues();
            DailyTest object = (DailyTest) Data;
            values.put(this.m_TableKey, object.CustomerSerialNo);
            values.put(SameColumns.MeterSerialNo, object.MeterSerialNo);
            values.put("CreateTime", Time);
            values.put("MeterIndex", object.MeterIndex);
            values.put(SameColumns.WiringType, object.WiringValue);
            values.put("IntWiringType", Integer.valueOf(object.Wiring));
            values.put(SameColumns.ConnectionMode, object.ConnectionType);
            values.put(SameColumns.VoltageRangeL1, object.VoltageRangeL1);
            values.put(SameColumns.VoltageRangeL2, object.VoltageRangeL2);
            values.put(SameColumns.VoltageRangeL3, object.VoltageRangeL3);
            values.put(SameColumns.CurrentRangeL1, object.CurrentRangeL1);
            values.put(SameColumns.CurrentRangeL2, object.CurrentRangeL2);
            values.put(SameColumns.CurrentRangeL3, object.CurrentRangeL3);
            values.put("PulseSamplingMethod", object.Pulses);
            values.put("MeterConstant", object.MeterConstant);
            values.put("ConstantUnit", object.ConstantUnit);
            values.put("Pulses", object.Pulses);
            values.put("ErrorCount", Integer.valueOf(object.ErrorCount));
            values.put("RunTime", object.RunTime);
            values.put("Error1", object.getError(0));
            values.put("Error2", object.getError(1));
            values.put("Error3", object.getError(2));
            values.put("Error4", object.getError(3));
            values.put("Error5", object.getError(4));
            values.put("Error6", object.getError(5));
            values.put("Error7", object.getError(6));
            values.put("Error8", object.getError(7));
            values.put("Error9", object.getError(8));
            values.put("Error10", object.getError(9));
            values.put("ErrorMore", object.ErrorMore);
            values.put("ErrorAverage", object.ErrorAverage);
            values.put("ErrorStandard", object.ErrorStandard);
            values.put("Result", object.Result.s_Value);
            if (db.insert(TABLE_NAME, null, values) == -1) {
                z = false;
            }
        }
        return z;
    }

    /* access modifiers changed from: protected */
    @Override // com.clou.rs350.db.dao.BaseDao, com.clou.rs350.db.dao.BaseTestDao
    public Object parseData(Cursor cursor) {
        DailyTest value = new DailyTest();
        value.CustomerSerialNo = parseString(cursor.getString(cursor.getColumnIndex(this.m_TableKey)));
        value.MeterSerialNo = parseString(cursor.getString(cursor.getColumnIndex(SameColumns.MeterSerialNo)));
        value.MeterIndex = parseString(cursor.getString(cursor.getColumnIndex("MeterIndex")));
        value.WiringValue = parseString(cursor.getString(cursor.getColumnIndex(SameColumns.WiringType)));
        value.Wiring = cursor.getInt(cursor.getColumnIndex("IntWiringType"));
        value.ConnectionType = parseString(cursor.getString(cursor.getColumnIndex(SameColumns.ConnectionMode)));
        value.VoltageRangeL1 = parseString(cursor.getString(cursor.getColumnIndex(SameColumns.VoltageRangeL1)));
        value.VoltageRangeL2 = parseString(cursor.getString(cursor.getColumnIndex(SameColumns.VoltageRangeL2)));
        value.VoltageRangeL3 = parseString(cursor.getString(cursor.getColumnIndex(SameColumns.VoltageRangeL3)));
        value.CurrentRangeL1 = parseString(cursor.getString(cursor.getColumnIndex(SameColumns.CurrentRangeL1)));
        value.CurrentRangeL2 = parseString(cursor.getString(cursor.getColumnIndex(SameColumns.CurrentRangeL2)));
        value.CurrentRangeL3 = parseString(cursor.getString(cursor.getColumnIndex(SameColumns.CurrentRangeL3)));
        value.Pulses = parseString(cursor.getString(cursor.getColumnIndex("Pulses")));
        value.MeterConstant = parseString(cursor.getString(cursor.getColumnIndex("MeterConstant")));
        value.ConstantUnit = parseString(cursor.getString(cursor.getColumnIndex("ConstantUnit")));
        value.ErrorCount = OtherUtils.parseInt(parseString(cursor.getString(cursor.getColumnIndex("ErrorCount"))));
        value.hasErrorCount = value.ErrorCount;
        value.RunTime = parseString(cursor.getString(cursor.getColumnIndex("RunTime")));
        value.Error[0].s_Value = parseString(cursor.getString(cursor.getColumnIndex("Error1")));
        value.Error[1].s_Value = parseString(cursor.getString(cursor.getColumnIndex("Error2")));
        value.Error[2].s_Value = parseString(cursor.getString(cursor.getColumnIndex("Error3")));
        value.Error[3].s_Value = parseString(cursor.getString(cursor.getColumnIndex("Error4")));
        value.Error[4].s_Value = parseString(cursor.getString(cursor.getColumnIndex("Error5")));
        value.Error[5].s_Value = parseString(cursor.getString(cursor.getColumnIndex("Error6")));
        value.Error[6].s_Value = parseString(cursor.getString(cursor.getColumnIndex("Error7")));
        value.Error[7].s_Value = parseString(cursor.getString(cursor.getColumnIndex("Error8")));
        value.Error[8].s_Value = parseString(cursor.getString(cursor.getColumnIndex("Error9")));
        value.Error[9].s_Value = parseString(cursor.getString(cursor.getColumnIndex("Error10")));
        value.ErrorMore = parseString(cursor.getString(cursor.getColumnIndex("ErrorMore")));
        value.ErrorAverage = parseString(cursor.getString(cursor.getColumnIndex("ErrorAverage")));
        value.ErrorStandard = parseString(cursor.getString(cursor.getColumnIndex("ErrorStandard")));
        value.Result.s_Value = parseString(cursor.getString(cursor.getColumnIndex("Result")));
        return value;
    }
}
