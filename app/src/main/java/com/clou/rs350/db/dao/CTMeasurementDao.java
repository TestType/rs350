package com.clou.rs350.db.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.clou.rs350.R;
import com.clou.rs350.db.model.CTMeasurement;
import com.clou.rs350.manager.DatabaseManager;
import com.clou.rs350.utils.OtherUtils;

public class CTMeasurementDao extends BaseTestDao {
    private static final String TABLE_NAME = "TestCTMeasurement";

    private static final class TableColumns {
        public static final String CurrentAnglePrimaryL1 = "CurrentAnglePrimaryL1";
        public static final String CurrentAnglePrimaryL2 = "CurrentAnglePrimaryL2";
        public static final String CurrentAnglePrimaryL3 = "CurrentAnglePrimaryL3";
        public static final String CurrentAngleSecondaryL1 = "CurrentAngleSecondaryL1";
        public static final String CurrentAngleSecondaryL2 = "CurrentAngleSecondaryL2";
        public static final String CurrentAngleSecondaryL3 = "CurrentAngleSecondaryL3";
        public static final String CurrentRangePriL1 = "CurrentRangePriL1";
        public static final String CurrentRangePriL2 = "CurrentRangePriL2";
        public static final String CurrentRangePriL3 = "CurrentRangePriL3";
        public static final String CurrentRangeSecL1 = "CurrentRangeSecL1";
        public static final String CurrentRangeSecL2 = "CurrentRangeSecL2";
        public static final String CurrentRangeSecL3 = "CurrentRangeSecL3";
        public static final String CurrentValueErrorL1 = "CurrentValueErrorL1";
        public static final String CurrentValueErrorL2 = "CurrentValueErrorL2";
        public static final String CurrentValueErrorL3 = "CurrentValueErrorL3";
        public static final String CurrentValueNormalPrimary = "CurrentValueNormalPrimary";
        public static final String CurrentValueNormalRatio = "CurrentValueNormalRatio";
        public static final String CurrentValueNormalSecondary = "CurrentValueNormalSecondary";
        public static final String CurrentValuePhaseL1 = "CurrentValuePhaseL1";
        public static final String CurrentValuePhaseL2 = "CurrentValuePhaseL2";
        public static final String CurrentValuePhaseL3 = "CurrentValuePhaseL3";
        public static final String CurrentValuePrimaryL1 = "CurrentValuePrimaryL1";
        public static final String CurrentValuePrimaryL2 = "CurrentValuePrimaryL2";
        public static final String CurrentValuePrimaryL3 = "CurrentValuePrimaryL3";
        public static final String CurrentValueRatioL1 = "CurrentValueRatioL1";
        public static final String CurrentValueRatioL2 = "CurrentValueRatioL2";
        public static final String CurrentValueRatioL3 = "CurrentValueRatioL3";
        public static final String CurrentValueSecondaryL1 = "CurrentValueSecondaryL1";
        public static final String CurrentValueSecondaryL2 = "CurrentValueSecondaryL2";
        public static final String CurrentValueSecondaryL3 = "CurrentValueSecondaryL3";
        public static final String ID = "intMyID";
        public static final String ResultL1 = "ResultL1";
        public static final String ResultL2 = "ResultL2";
        public static final String ResultL3 = "ResultL3";

        private TableColumns() {
        }
    }

    public CTMeasurementDao(Context context) {
        super(context, TABLE_NAME);
    }

    public void createTable(SQLiteDatabase db) {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("create table if not exists ").append(TABLE_NAME).append(" (");
        stringBuffer.append("intMyID").append(" integer primary key autoincrement,");
        stringBuffer.append(this.m_TableKey).append(" VARCHAR(50),");
        stringBuffer.append("CreateTime").append(" VARCHAR(50),");
        stringBuffer.append(SameColumns.WiringType).append(" VARCHAR(50),");
        stringBuffer.append("IntWiringType").append(" integer default 0,");
        stringBuffer.append(SameColumns.ConnectionMode).append(" VARCHAR(50),");
        stringBuffer.append(TableColumns.CurrentRangePriL1).append(" VARCHAR(50),");
        stringBuffer.append("CurrentRangeSecL1").append(" VARCHAR(50),");
        stringBuffer.append(TableColumns.CurrentRangePriL2).append(" VARCHAR(50),");
        stringBuffer.append("CurrentRangeSecL2").append(" VARCHAR(50),");
        stringBuffer.append(TableColumns.CurrentRangePriL3).append(" VARCHAR(50),");
        stringBuffer.append("CurrentRangeSecL3").append(" VARCHAR(50),");
        stringBuffer.append(TableColumns.CurrentValuePrimaryL1).append(" VARCHAR(20),");
        stringBuffer.append(TableColumns.CurrentValuePrimaryL2).append(" VARCHAR(20),");
        stringBuffer.append(TableColumns.CurrentValuePrimaryL3).append(" VARCHAR(20),");
        stringBuffer.append("CurrentValueSecondaryL1").append(" VARCHAR(20),");
        stringBuffer.append("CurrentValueSecondaryL2").append(" VARCHAR(20),");
        stringBuffer.append("CurrentValueSecondaryL3").append(" VARCHAR(20),");
        stringBuffer.append(TableColumns.CurrentAnglePrimaryL1).append(" VARCHAR(20),");
        stringBuffer.append(TableColumns.CurrentAnglePrimaryL2).append(" VARCHAR(20),");
        stringBuffer.append(TableColumns.CurrentAnglePrimaryL3).append(" VARCHAR(20),");
        stringBuffer.append(TableColumns.CurrentAngleSecondaryL1).append(" VARCHAR(20),");
        stringBuffer.append(TableColumns.CurrentAngleSecondaryL2).append(" VARCHAR(20),");
        stringBuffer.append(TableColumns.CurrentAngleSecondaryL3).append(" VARCHAR(20),");
        stringBuffer.append(TableColumns.CurrentValueRatioL1).append(" VARCHAR(20),");
        stringBuffer.append(TableColumns.CurrentValueRatioL2).append(" VARCHAR(20),");
        stringBuffer.append(TableColumns.CurrentValueRatioL3).append(" VARCHAR(20),");
        stringBuffer.append(TableColumns.CurrentValuePhaseL1).append(" VARCHAR(20),");
        stringBuffer.append(TableColumns.CurrentValuePhaseL2).append(" VARCHAR(20),");
        stringBuffer.append(TableColumns.CurrentValuePhaseL3).append(" VARCHAR(20),");
        stringBuffer.append(TableColumns.CurrentValueErrorL1).append(" VARCHAR(20),");
        stringBuffer.append(TableColumns.CurrentValueErrorL2).append(" VARCHAR(20),");
        stringBuffer.append(TableColumns.CurrentValueErrorL3).append(" VARCHAR(20),");
        stringBuffer.append("ResultL1").append(" VARCHAR(30),");
        stringBuffer.append("ResultL2").append(" VARCHAR(30),");
        stringBuffer.append("ResultL3").append(" VARCHAR(30),");
        stringBuffer.append(TableColumns.CurrentValueNormalPrimary).append(" VARCHAR(50),");
        stringBuffer.append(TableColumns.CurrentValueNormalSecondary).append(" VARCHAR(50),");
        stringBuffer.append(TableColumns.CurrentValueNormalRatio).append(" VARCHAR(50));");
        db.execSQL(stringBuffer.toString());
    }

    @Override // com.clou.rs350.db.dao.BaseDao, com.clou.rs350.db.dao.BaseTestDao
    public void upgradeTable(SQLiteDatabase db, int oldVersion, int newVersion) {
        if (oldVersion <= 3) {
            db.execSQL("alter table CTMeasurement rename to TestCTMeasurement");
        }
        super.upgradeTable(db, oldVersion, newVersion);
    }

    @Override // com.clou.rs350.db.dao.BaseDao, com.clou.rs350.db.dao.BaseTestDao
    public synchronized boolean insertObject(Object Data, String Time) {
        boolean z = true;
        synchronized (this) {
            SQLiteDatabase db = DatabaseManager.getWriteableDatabase(this.m_Context);
            ContentValues values = new ContentValues();
            CTMeasurement object = (CTMeasurement) Data;
            values.put(this.m_TableKey, object.SerialNo);
            values.put("CreateTime", Time);
            values.put(SameColumns.WiringType, object.WiringValue);
            values.put("IntWiringType", Integer.valueOf(object.Wiring));
            values.put(SameColumns.ConnectionMode, object.ConnectionType);
            values.put(TableColumns.CurrentRangePriL1, object.CurrentRangePri[0]);
            values.put("CurrentRangeSecL1", object.CurrentRangeSec[0]);
            values.put(TableColumns.CurrentRangePriL2, object.CurrentRangePri[1]);
            values.put("CurrentRangeSecL2", object.CurrentRangeSec[1]);
            values.put(TableColumns.CurrentRangePriL3, object.CurrentRangePri[2]);
            values.put("CurrentRangeSecL3", object.CurrentRangeSec[2]);
            values.put(TableColumns.CurrentValuePrimaryL1, object.CurrentValuePrimary[0].s_Value);
            values.put(TableColumns.CurrentValuePrimaryL2, object.CurrentValuePrimary[1].s_Value);
            values.put(TableColumns.CurrentValuePrimaryL3, object.CurrentValuePrimary[2].s_Value);
            values.put("CurrentValueSecondaryL1", object.CurrentValueSecondary[0].s_Value);
            values.put("CurrentValueSecondaryL2", object.CurrentValueSecondary[1].s_Value);
            values.put("CurrentValueSecondaryL3", object.CurrentValueSecondary[2].s_Value);
            values.put(TableColumns.CurrentAnglePrimaryL1, object.CurrentAnglePrimary[0].s_Value);
            values.put(TableColumns.CurrentAnglePrimaryL2, object.CurrentAnglePrimary[1].s_Value);
            values.put(TableColumns.CurrentAnglePrimaryL3, object.CurrentAnglePrimary[2].s_Value);
            values.put(TableColumns.CurrentAngleSecondaryL1, object.CurrentAngleSecondary[0].s_Value);
            values.put(TableColumns.CurrentAngleSecondaryL2, object.CurrentAngleSecondary[1].s_Value);
            values.put(TableColumns.CurrentAngleSecondaryL3, object.CurrentAngleSecondary[2].s_Value);
            values.put(TableColumns.CurrentValueRatioL1, object.CurrentValueRatio[0].s_Value);
            values.put(TableColumns.CurrentValueRatioL2, object.CurrentValueRatio[1].s_Value);
            values.put(TableColumns.CurrentValueRatioL3, object.CurrentValueRatio[2].s_Value);
            values.put(TableColumns.CurrentValuePhaseL1, object.CurrentValuePhase[0].s_Value);
            values.put(TableColumns.CurrentValuePhaseL2, object.CurrentValuePhase[1].s_Value);
            values.put(TableColumns.CurrentValuePhaseL3, object.CurrentValuePhase[2].s_Value);
            values.put(TableColumns.CurrentValueErrorL1, object.CurrentValueError[0].s_Value);
            values.put(TableColumns.CurrentValueErrorL2, object.CurrentValueError[1].s_Value);
            values.put(TableColumns.CurrentValueErrorL3, object.CurrentValueError[2].s_Value);
            values.put("ResultL1", object.Result[0].s_Value);
            values.put("ResultL2", object.Result[1].s_Value);
            values.put("ResultL3", object.Result[2].s_Value);
            values.put(TableColumns.CurrentValueNormalPrimary, object.CurrentValueNominalPrimary.s_Value);
            values.put(TableColumns.CurrentValueNormalSecondary, object.CurrentValueNominalSecondary.s_Value);
            values.put(TableColumns.CurrentValueNormalRatio, object.NormalRatio.s_Value);
            if (db.insert(TABLE_NAME, null, values) == -1) {
                z = false;
            }
        }
        return z;
    }

    /* access modifiers changed from: protected */
    @Override // com.clou.rs350.db.dao.BaseDao, com.clou.rs350.db.dao.BaseTestDao
    public Object parseData(Cursor cursor) {
        CTMeasurement value = new CTMeasurement();
        value.SerialNo = cursor.getString(cursor.getColumnIndex(this.m_TableKey));
        value.WiringValue = cursor.getString(cursor.getColumnIndex(SameColumns.WiringType));
        value.Wiring = cursor.getInt(cursor.getColumnIndex("IntWiringType"));
        value.ConnectionType = cursor.getString(cursor.getColumnIndex(SameColumns.ConnectionMode));
        value.CurrentRangePri[0] = cursor.getString(cursor.getColumnIndex(TableColumns.CurrentRangePriL1));
        value.CurrentRangeSec[0] = cursor.getString(cursor.getColumnIndex("CurrentRangeSecL1"));
        value.CurrentRangePri[1] = cursor.getString(cursor.getColumnIndex(TableColumns.CurrentRangePriL2));
        value.CurrentRangeSec[1] = cursor.getString(cursor.getColumnIndex("CurrentRangeSecL2"));
        value.CurrentRangePri[2] = cursor.getString(cursor.getColumnIndex(TableColumns.CurrentRangePriL3));
        value.CurrentRangeSec[2] = cursor.getString(cursor.getColumnIndex("CurrentRangeSecL3"));
        value.CurrentValuePrimary[0].s_Value = cursor.getString(cursor.getColumnIndex(TableColumns.CurrentValuePrimaryL1));
        value.CurrentValuePrimary[1].s_Value = cursor.getString(cursor.getColumnIndex(TableColumns.CurrentValuePrimaryL2));
        value.CurrentValuePrimary[2].s_Value = cursor.getString(cursor.getColumnIndex(TableColumns.CurrentValuePrimaryL3));
        value.CurrentValueSecondary[0].s_Value = cursor.getString(cursor.getColumnIndex("CurrentValueSecondaryL1"));
        value.CurrentValueSecondary[1].s_Value = cursor.getString(cursor.getColumnIndex("CurrentValueSecondaryL2"));
        value.CurrentValueSecondary[2].s_Value = cursor.getString(cursor.getColumnIndex("CurrentValueSecondaryL3"));
        value.CurrentAnglePrimary[0].s_Value = cursor.getString(cursor.getColumnIndex(TableColumns.CurrentAnglePrimaryL1));
        value.CurrentAnglePrimary[1].s_Value = cursor.getString(cursor.getColumnIndex(TableColumns.CurrentAnglePrimaryL2));
        value.CurrentAnglePrimary[2].s_Value = cursor.getString(cursor.getColumnIndex(TableColumns.CurrentAnglePrimaryL3));
        value.CurrentAngleSecondary[0].s_Value = cursor.getString(cursor.getColumnIndex(TableColumns.CurrentAngleSecondaryL1));
        value.CurrentAngleSecondary[1].s_Value = cursor.getString(cursor.getColumnIndex(TableColumns.CurrentAngleSecondaryL2));
        value.CurrentAngleSecondary[2].s_Value = cursor.getString(cursor.getColumnIndex(TableColumns.CurrentAngleSecondaryL3));
        value.CurrentValueRatio[0].s_Value = cursor.getString(cursor.getColumnIndex(TableColumns.CurrentValueRatioL1));
        value.CurrentValueRatio[1].s_Value = cursor.getString(cursor.getColumnIndex(TableColumns.CurrentValueRatioL2));
        value.CurrentValueRatio[2].s_Value = cursor.getString(cursor.getColumnIndex(TableColumns.CurrentValueRatioL3));
        value.CurrentValuePhase[0].s_Value = cursor.getString(cursor.getColumnIndex(TableColumns.CurrentValuePhaseL1));
        value.CurrentValuePhase[1].s_Value = cursor.getString(cursor.getColumnIndex(TableColumns.CurrentValuePhaseL2));
        value.CurrentValuePhase[2].s_Value = cursor.getString(cursor.getColumnIndex(TableColumns.CurrentValuePhaseL3));
        value.CurrentValueError[0].s_Value = cursor.getString(cursor.getColumnIndex(TableColumns.CurrentValueErrorL1));
        value.CurrentValueError[1].s_Value = cursor.getString(cursor.getColumnIndex(TableColumns.CurrentValueErrorL2));
        value.CurrentValueError[2].s_Value = cursor.getString(cursor.getColumnIndex(TableColumns.CurrentValueErrorL3));
        value.Result[0] = OtherUtils.getResultValue(cursor.getString(cursor.getColumnIndex("ResultL1")), R.array.limit, this.m_Context);
        value.Result[1] = OtherUtils.getResultValue(cursor.getString(cursor.getColumnIndex("ResultL2")), R.array.limit, this.m_Context);
        value.Result[2] = OtherUtils.getResultValue(cursor.getString(cursor.getColumnIndex("ResultL3")), R.array.limit, this.m_Context);
        value.CurrentValueNominalPrimary.s_Value = cursor.getString(cursor.getColumnIndex(TableColumns.CurrentValueNormalPrimary));
        value.CurrentValueNominalSecondary.s_Value = cursor.getString(cursor.getColumnIndex(TableColumns.CurrentValueNormalSecondary));
        value.NormalRatio.s_Value = cursor.getString(cursor.getColumnIndex(TableColumns.CurrentValueNormalRatio));
        return value;
    }
}
