package com.clou.rs350.db.model;

import com.itextpdf.text.pdf.PdfObject;

public class MeterInfo extends MeterType {
    public String CustomerSerialNo = PdfObject.NOTHING;
    public String ProductionYear = PdfObject.NOTHING;
    public String SerialNo = PdfObject.NOTHING;
    private MeterInfo meterBaseInfo = null;

    public MeterInfo getMeterBaseInfo() {
        if (this.meterBaseInfo == null) {
            this.meterBaseInfo = new MeterInfo();
        }
        return this.meterBaseInfo;
    }

    public void CopyMeterInfo(MeterInfo Data) {
        this.CustomerSerialNo = Data.CustomerSerialNo;
        this.ProductionYear = Data.ProductionYear;
    }
}
