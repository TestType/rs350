package com.clou.rs350.db.model.sequence;

import com.itextpdf.text.pdf.PdfObject;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class SignatureData implements Serializable {
    public String CreatTime = PdfObject.NOTHING;
    public int Index = 0;
    public String SerialNo = PdfObject.NOTHING;
    public List<SignatureItem> Signatures = new ArrayList();
    public String Title = PdfObject.NOTHING;

    public SignatureData() {
    }

    public SignatureData(int index) {
        this.Index = index;
    }
}
