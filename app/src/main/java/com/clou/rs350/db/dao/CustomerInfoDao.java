package com.clou.rs350.db.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import com.clou.rs350.constants.Constants;
import com.clou.rs350.db.model.CustomerInfo;
import com.clou.rs350.manager.DatabaseManager;
import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

public class CustomerInfoDao extends BaseDao {
    private static final String TABLE_NAME = "CustomerInfo";

    private static final class TableColumns {
        public static final String AddStreet = "AddStreet";
        public static final String City = "City";
        public static final String ConstactPhoneNumber = "ConstactPhoneNumber";
        public static final String ContactPersonName = "ContactPersonName";
        public static final String CustomerName = "CustomerName";
        public static final String CustomerSerialNo = "CustomerSerialNo";
        public static final String District = "District";
        public static final String Email = "Email";
        public static final String HouseNumber = "HouseNumber";
        public static final String Latitude = "Latitude";
        public static final String Longitude = "Longitude";
        public static final String Pin = "Pin";
        public static final String State = "State";
        public static final String TypeofConnection = "TypeofConnection";

        private TableColumns() {
        }
    }

    public CustomerInfoDao(Context context) {
        super(context, "CustomerInfo", "CustomerSerialNo");
    }

    public void selectData(Context context) {
        DatabaseManager.getWriteableDatabase(context).query("CustomerInfo", null, null, null, null, null, null).close();
    }

    public void createTable(SQLiteDatabase db) {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("create table if not exists ").append(this.m_TableName).append(" ( ").append("intMyID").append(" integer primary key autoincrement,");
        stringBuffer.append("CreateTime").append(" VARCHAR(50),");
        stringBuffer.append("IsRead").append(" integer default 0,");
        stringBuffer.append("CustomerSerialNo").append(" VARCHAR(50),");
        stringBuffer.append("CustomerName").append(" VARCHAR(50),");
        stringBuffer.append(TableColumns.AddStreet).append(" VARCHAR(50),");
        stringBuffer.append(TableColumns.HouseNumber).append(" VARCHAR(50),");
        stringBuffer.append("City").append(" VARCHAR(50),");
        stringBuffer.append("District").append(" VARCHAR(50),");
        stringBuffer.append(TableColumns.Pin).append(" VARCHAR(50),");
        stringBuffer.append("State").append(" VARCHAR(50),");
        stringBuffer.append(TableColumns.ContactPersonName).append(" VARCHAR(50),");
        stringBuffer.append("Email").append(" VARCHAR(150),");
        stringBuffer.append(TableColumns.ConstactPhoneNumber).append(" VARCHAR(50),");
        stringBuffer.append(TableColumns.Longitude).append(" VARCHAR(50),");
        stringBuffer.append(TableColumns.Latitude).append(" VARCHAR(50),");
        stringBuffer.append(TableColumns.TypeofConnection).append(" VARCHAR(50));");
        db.execSQL(stringBuffer.toString());
    }

    public synchronized boolean insertObject(Object Data) {
        SQLiteDatabase db;
        ContentValues values;
        db = DatabaseManager.getWriteableDatabase(this.m_Context);
        values = new ContentValues();
        Date SaveTime = new Date(System.currentTimeMillis());
        SimpleDateFormat sDateFormat = new SimpleDateFormat(Constants.TIMEFORMAT);
        CustomerInfo object = (CustomerInfo) Data;
        values.put("CustomerSerialNo", object.CustomerSr.toString());
        values.put("CustomerName", object.CustomerName.toString());
        values.put("CreateTime", sDateFormat.format(SaveTime).toString());
        values.put(TableColumns.AddStreet, object.AddStreet.toString());
        values.put(TableColumns.HouseNumber, object.HouseNumber);
        values.put("City", object.City.toString());
        values.put("District", object.District.toString());
        values.put(TableColumns.Pin, object.Pin.toString());
        values.put("State", object.State.toString());
        values.put(TableColumns.ContactPersonName, object.ContactPersonName.toString());
        values.put("Email", object.Email.toString());
        values.put(TableColumns.ConstactPhoneNumber, object.ConstactPhoneNumber.toString());
        values.put(TableColumns.Longitude, object.Longitude.toString());
        values.put(TableColumns.Latitude, object.Latitude.toString());
        values.put(TableColumns.TypeofConnection, object.TypeofConnection.toString());
        return db.insert(this.m_TableName, null, values) != -1;
    }

    public void updateObjectByKey(String serialNo, CustomerInfo object) {
        SQLiteDatabase db = DatabaseManager.getWriteableDatabase(this.m_Context);
        ContentValues values = new ContentValues();
        values.put("CustomerSerialNo", object.CustomerSr);
        values.put("CustomerName", object.CustomerName);
        values.put(TableColumns.AddStreet, object.AddStreet);
        values.put(TableColumns.HouseNumber, object.HouseNumber);
        values.put("City", object.City);
        values.put("District", object.District);
        values.put(TableColumns.Pin, object.Pin);
        values.put("State", object.State);
        values.put(TableColumns.ContactPersonName, object.ContactPersonName);
        values.put("Email", object.Email);
        values.put(TableColumns.ConstactPhoneNumber, object.ConstactPhoneNumber);
        values.put(TableColumns.Longitude, object.Longitude);
        values.put(TableColumns.Latitude, object.Latitude);
        values.put(TableColumns.TypeofConnection, object.TypeofConnection);
        values.put("IsRead", (Integer) 0);
        db.update(this.m_TableName, values, "CustomerSerialNo = ?", new String[]{serialNo});
    }

    public List<CustomerInfo> queryAllInfo() {
        List<CustomerInfo> values = new ArrayList<>();
        Cursor cursor = DatabaseManager.getWriteableDatabase(this.m_Context).query(this.m_TableName, null, null, null, null, null, "CustomerSerialNo");
        while (cursor.moveToNext()) {
            values.add((CustomerInfo) parseData(cursor));
        }
        cursor.close();
        return values;
    }

    public CustomerInfo queryObjectBySerialNo(String serialNo) {
        CustomerInfo value = null;
        Cursor cursor = DatabaseManager.getWriteableDatabase(this.m_Context).rawQuery("select * from CustomerInfo where CustomerSerialNo = ?", new String[]{serialNo});
        if (cursor.moveToNext()) {
            value = (CustomerInfo) parseData(cursor);
        }
        cursor.close();
        return value;
    }

    /* access modifiers changed from: protected */
    @Override // com.clou.rs350.db.dao.BaseDao
    public Object parseData(Cursor cursor) {
        CustomerInfo value = new CustomerInfo();
        value.CustomerSr = cursor.getString(cursor.getColumnIndex("CustomerSerialNo"));
        value.CustomerName = cursor.getString(cursor.getColumnIndex("CustomerName"));
        value.AddStreet = cursor.getString(cursor.getColumnIndex(TableColumns.AddStreet));
        value.HouseNumber = cursor.getString(cursor.getColumnIndex(TableColumns.HouseNumber));
        value.City = cursor.getString(cursor.getColumnIndex("City"));
        value.District = cursor.getString(cursor.getColumnIndex("District"));
        value.Pin = cursor.getString(cursor.getColumnIndex(TableColumns.Pin));
        value.State = cursor.getString(cursor.getColumnIndex("State"));
        value.ContactPersonName = cursor.getString(cursor.getColumnIndex(TableColumns.ContactPersonName));
        value.Email = cursor.getString(cursor.getColumnIndex("Email"));
        value.ConstactPhoneNumber = cursor.getString(cursor.getColumnIndex(TableColumns.ConstactPhoneNumber));
        value.Longitude = cursor.getString(cursor.getColumnIndex(TableColumns.Longitude));
        value.Latitude = cursor.getString(cursor.getColumnIndex(TableColumns.Latitude));
        value.TypeofConnection = cursor.getString(cursor.getColumnIndex(TableColumns.TypeofConnection));
        value.isNew = false;
        return value;
    }

    public List<CustomerInfo> queryForDataBaseOrderSerialNo(int type, String serialNo) {
        List<CustomerInfo> values = new ArrayList<>();
        SQLiteDatabase db = DatabaseManager.getWriteableDatabase(this.m_Context);
        Cursor cursor = null;
        switch (type) {
            case 0:
                cursor = db.query(this.m_TableName, null, "CustomerSerialNo = ?", new String[]{serialNo}, null, null, null);
                break;
            case 1:
                cursor = db.query(this.m_TableName, null, "CustomerSerialNo > ?", new String[]{serialNo}, null, null, null);
                break;
            case 2:
                cursor = db.query(this.m_TableName, null, "CustomerSerialNo < ?", new String[]{serialNo}, null, null, null);
                break;
            case 3:
                cursor = db.query(this.m_TableName, null, "CustomerSerialNo >= ?", new String[]{serialNo}, null, null, null);
                break;
            case 4:
                cursor = db.query(this.m_TableName, null, "CustomerSerialNo <= ?", new String[]{serialNo}, null, null, null);
                break;
        }
        while (cursor.moveToNext()) {
            values.add((CustomerInfo) parseData(cursor));
        }
        cursor.close();
        return values;
    }
}
