package com.clou.rs350.db.model;

import java.io.Serializable;

public class DBDataModel implements Serializable {
    public double d_Value = 0.0d;
    public long l_Value = 0;
    public String s_Value = "---";

    public DBDataModel() {
    }

    public DBDataModel(double doubleValue, String stringValue) {
        this.d_Value = doubleValue;
        this.s_Value = stringValue;
    }

    public DBDataModel(long longValue, String stringValue) {
        this.l_Value = longValue;
        this.s_Value = stringValue;
    }

    public String toString() {
        return this.s_Value;
    }
}
