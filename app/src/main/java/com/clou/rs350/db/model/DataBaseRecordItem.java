package com.clou.rs350.db.model;

public class DataBaseRecordItem {
    private int intMyID;
    private boolean isSelect;
    private String m_Datetime;
    private String m_TestItem;

    public DataBaseRecordItem() {
    }

    public DataBaseRecordItem(int intMyID2, String datetime, String testItem, boolean isSelect2) {
        this.intMyID = intMyID2;
        this.m_Datetime = datetime;
        this.m_TestItem = testItem;
        this.isSelect = isSelect2;
    }

    public String getDatetime() {
        return this.m_Datetime;
    }

    public String getTestItem() {
        return this.m_TestItem;
    }

    public void setDatetime(String datetime) {
        this.m_Datetime = datetime;
    }

    public boolean isSelect() {
        return this.isSelect;
    }

    public void setSelect(boolean isSelect2) {
        this.isSelect = isSelect2;
    }

    public String toString() {
        return "DataBaseRecordItem [datetime=" + this.m_Datetime + ", isSelect=" + this.isSelect + "]";
    }

    public int getIntMyID() {
        return this.intMyID;
    }

    public void setIntMyID(int intMyID2) {
        this.intMyID = intMyID2;
    }
}
