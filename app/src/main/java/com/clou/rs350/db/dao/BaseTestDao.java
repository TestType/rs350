package com.clou.rs350.db.dao;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import com.clou.rs350.db.model.DataBaseRecordItem;
import com.clou.rs350.manager.DatabaseManager;
import java.util.List;

public class BaseTestDao extends BaseDao {

    protected static final class SameColumns {
        public static final String ConnectionMode = "ConnectionMode";
        public static final String CurrentRangeL1 = "CurrentRangeL1";
        public static final String CurrentRangeL2 = "CurrentRangeL2";
        public static final String CurrentRangeL3 = "CurrentRangeL3";
        public static final String CustomerSerialNo = "CustomerSerialNo";
        public static final String IntWiringType = "IntWiringType";
        public static final String MeterSerialNo = "MeterSerialNo";
        public static final String VoltageRangeL1 = "VoltageRangeL1";
        public static final String VoltageRangeL2 = "VoltageRangeL2";
        public static final String VoltageRangeL3 = "VoltageRangeL3";
        public static final String WiringType = "MeasurementMode";

        protected SameColumns() {
        }
    }

    public BaseTestDao(Context context, String tableName) {
        super(context, tableName, "CustomerSerialNo");
    }

    /* access modifiers changed from: protected */
    @Override // com.clou.rs350.db.dao.BaseDao
    public Object parseData(Cursor cursor) {
        return null;
    }

    /* access modifiers changed from: protected */
    @Override // com.clou.rs350.db.dao.BaseDao
    public String getCreatTableString() {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append(super.getCreatTableString()).append("IntWiringType").append(" integer default 0,").append(SameColumns.WiringType).append(" VARCHAR(50),").append(SameColumns.ConnectionMode).append(" VARCHAR(20),").append(SameColumns.VoltageRangeL1).append(" VARCHAR(20),").append(SameColumns.VoltageRangeL2).append(" VARCHAR(20),").append(SameColumns.VoltageRangeL3).append(" VARCHAR(20),").append(SameColumns.CurrentRangeL1).append(" VARCHAR(20),").append(SameColumns.CurrentRangeL2).append(" VARCHAR(20),").append(SameColumns.CurrentRangeL3).append(" VARCHAR(20),");
        return stringBuffer.toString();
    }

    @Override // com.clou.rs350.db.dao.BaseDao
    public synchronized boolean insertObject(Object Data, String Time) {
        return false;
    }

    @Override // com.clou.rs350.db.dao.BaseDao
    public void upgradeTable(SQLiteDatabase db, int oldVersion, int newVersion) {
        if (oldVersion < 22) {
            db.execSQL("alter table " + this.m_TableName + " add column " + "IntWiringType" + " integer default 0");
        }
        super.upgradeTable(db, oldVersion, newVersion);
    }

    @Override // com.clou.rs350.db.dao.BaseDao
    public void deleteRecordByKey(String key, List<DataBaseRecordItem> list) {
        SQLiteDatabase db = DatabaseManager.getWriteableDatabase(this.m_Context);
        for (DataBaseRecordItem item : list) {
            if (item.isSelect()) {
                db.delete(this.m_TableName, String.valueOf(this.m_TableKey) + " = ? AND " + "CreateTime" + " = ?", new String[]{key, item.getDatetime()});
            }
        }
    }

    @Override // com.clou.rs350.db.dao.BaseDao
    public void deleteRecordByKey(String key, DataBaseRecordItem item) {
        SQLiteDatabase db = DatabaseManager.getWriteableDatabase(this.m_Context);
        if (item.isSelect()) {
            db.delete(this.m_TableName, String.valueOf(this.m_TableKey) + " = ? AND " + "CreateTime" + " = ?", new String[]{key, item.getDatetime()});
        }
    }
}
