package com.clou.rs350.db.model;

import android.os.AsyncTask;
import com.clou.rs350.callback.ICompleteCallback;
import com.clou.rs350.callback.ILoadCallback;
import com.clou.rs350.device.MeterBaseDevice;
import com.clou.rs350.device.model.ExplainedModel;
import com.clou.rs350.task.MyTask;
import com.clou.rs350.utils.DataProcessing;
import com.clou.rs350.utils.OtherUtils;
import com.itextpdf.text.pdf.PdfObject;
import java.lang.reflect.Array;

public class HarmonicMeasurement extends BasicMeasurement {
    public String[] HarmonicAngle = {"---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---", "---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---", "---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---", "---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---", "---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---", "---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---"};
    public String[] HarmonicContent = {"---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---", "---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---", "---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---", "---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---", "---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---", "---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---"};
    public int HarmonicDefination;
    public String[] HarmonicValue = {"---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---", "---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---", "---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---", "---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---", "---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---", "---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---"};
    public String[] PowerHarmonicValue = {"---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---", "---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---", "---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---", "---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---", "---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---", "---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---", "---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---", "---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---", "---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---|---"};
    public String SerialNo = PdfObject.NOTHING;
    public String THD = "---|---|---|---|---|---";
    public Double[][] arrCurrentAngle = ((Double[][]) Array.newInstance(Double.class, 3, 64));
    public Double[][] arrCurrentContent = ((Double[][]) Array.newInstance(Double.class, 3, 64));
    public ExplainedModel[][] arrCurrentValue = ((ExplainedModel[][]) Array.newInstance(ExplainedModel.class, 3, 64));
    public ExplainedModel[][] arrPValue = ((ExplainedModel[][]) Array.newInstance(ExplainedModel.class, 3, 64));
    public ExplainedModel[][] arrQValue = ((ExplainedModel[][]) Array.newInstance(ExplainedModel.class, 3, 64));
    public ExplainedModel[][] arrSValue = ((ExplainedModel[][]) Array.newInstance(ExplainedModel.class, 3, 64));
    public Double[] arrTHD = {Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d)};
    public Double[][] arrVoltageAngle = ((Double[][]) Array.newInstance(Double.class, 3, 64));
    public Double[][] arrVoltageContent = ((Double[][]) Array.newInstance(Double.class, 3, 64));
    public ExplainedModel[][] arrVoltageValue = ((ExplainedModel[][]) Array.newInstance(ExplainedModel.class, 3, 64));
    private MyTask task = null;

    public HarmonicMeasurement() {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 64; j++) {
                this.arrVoltageContent[i][j] = Double.valueOf(0.0d);
                this.arrVoltageAngle[i][j] = Double.valueOf(0.0d);
                this.arrCurrentContent[i][j] = Double.valueOf(0.0d);
                this.arrCurrentAngle[i][j] = Double.valueOf(0.0d);
                this.arrVoltageValue[i][j] = new ExplainedModel();
                this.arrCurrentValue[i][j] = new ExplainedModel();
                this.arrPValue[i][j] = new ExplainedModel();
                this.arrQValue[i][j] = new ExplainedModel();
                this.arrSValue[i][j] = new ExplainedModel();
            }
        }
    }

    @Override // com.clou.rs350.db.model.BasicMeasurement
    public void parseData(MeterBaseDevice Device) {
        ExplainedModel tmpVoltageReference;
        ExplainedModel tmpCurrentReference;
        parseRangeData(Device);
        ExplainedModel[] tmpVoltageValues = Device.getLNVoltageValue();
        ExplainedModel[] tmpCurrentValues = Device.getCurrentValue();
        for (int i = 0; i < 3; i++) {
            this.arrVoltageValue[i] = Device.getHarmonicValuesValue(i, 0);
            this.arrCurrentValue[i] = Device.getHarmonicValuesValue(i, 1);
            this.arrPValue[i] = Device.getHarmonicValuesValue(i, 2);
            this.arrQValue[i] = Device.getHarmonicValuesValue(i, 3);
            this.arrSValue[i] = Device.getHarmonicValuesValue(i, 4);
            this.arrVoltageAngle[i] = Device.getHarmonicAnglesValue(i);
            this.arrCurrentAngle[i] = Device.getHarmonicAnglesValue(i + 3);
            if (this.HarmonicDefination == 0) {
                tmpVoltageReference = this.arrVoltageValue[i][0];
                tmpCurrentReference = this.arrCurrentValue[i][0];
            } else {
                tmpVoltageReference = tmpVoltageValues[i];
                tmpCurrentReference = tmpCurrentValues[i];
            }
            this.arrVoltageContent[i] = new Double[this.arrVoltageValue[i].length];
            this.arrCurrentContent[i] = new Double[this.arrVoltageValue[i].length];
            for (int j = 0; j < this.arrVoltageValue[i].length; j++) {
                if (0.0d != tmpVoltageReference.getDoubleValue()) {
                    this.arrVoltageContent[i][j] = Double.valueOf((this.arrVoltageValue[i][j].getDoubleValue() / tmpVoltageReference.getDoubleValue()) * 100.0d);
                } else {
                    this.arrVoltageContent[i][j] = Double.valueOf(0.0d);
                }
                if (0.0d != tmpCurrentReference.getDoubleValue()) {
                    this.arrCurrentContent[i][j] = Double.valueOf((this.arrCurrentValue[i][j].getDoubleValue() / tmpCurrentReference.getDoubleValue()) * 100.0d);
                } else {
                    this.arrCurrentContent[i][j] = Double.valueOf(0.0d);
                }
            }
        }
        ExplainedModel[] tmpValue = Device.getTHDValue();
        this.arrTHD = new Double[]{Double.valueOf(tmpValue[0].getDoubleValue()), Double.valueOf(tmpValue[1].getDoubleValue()), Double.valueOf(tmpValue[2].getDoubleValue()), Double.valueOf(tmpValue[3].getDoubleValue()), Double.valueOf(tmpValue[4].getDoubleValue()), Double.valueOf(tmpValue[5].getDoubleValue())};
    }

    public void parseData(final MeterBaseDevice Device, final ICompleteCallback callback) {
        if (this.task == null || this.task.isDestoryed) {
            this.task = new MyTask(new ILoadCallback() {
                /* class com.clou.rs350.db.model.HarmonicMeasurement.AnonymousClass1 */

                @Override // com.clou.rs350.callback.ILoadCallback
                public Object run() {
                    HarmonicMeasurement.this.parseData(Device);
                    return true;
                }

                @Override // com.clou.rs350.callback.ILoadCallback
                public void callback(Object result) {
                    if (callback != null) {
                        callback.onComplete();
                    }
                }
            });
            this.task.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, 0);
        }
    }

    public void buildStringData() {
        this.HarmonicContent[0] = buildValue(this.arrVoltageContent[0], 4);
        this.HarmonicContent[1] = buildValue(this.arrVoltageContent[1], 4);
        this.HarmonicContent[2] = buildValue(this.arrVoltageContent[2], 4);
        this.HarmonicContent[3] = buildValue(this.arrCurrentContent[0], 4);
        this.HarmonicContent[4] = buildValue(this.arrCurrentContent[1], 4);
        this.HarmonicContent[5] = buildValue(this.arrCurrentContent[2], 4);
        this.HarmonicAngle[0] = buildValue(this.arrVoltageAngle[0], 3);
        this.HarmonicAngle[1] = buildValue(this.arrVoltageAngle[1], 3);
        this.HarmonicAngle[2] = buildValue(this.arrVoltageAngle[2], 3);
        this.HarmonicAngle[3] = buildValue(this.arrCurrentAngle[0], 3);
        this.HarmonicAngle[4] = buildValue(this.arrCurrentAngle[1], 3);
        this.HarmonicAngle[5] = buildValue(this.arrCurrentAngle[2], 3);
        this.HarmonicValue[0] = buildValue(this.arrVoltageValue[0]);
        this.HarmonicValue[1] = buildValue(this.arrVoltageValue[1]);
        this.HarmonicValue[2] = buildValue(this.arrVoltageValue[2]);
        this.HarmonicValue[3] = buildValue(this.arrCurrentValue[0]);
        this.HarmonicValue[4] = buildValue(this.arrCurrentValue[1]);
        this.HarmonicValue[5] = buildValue(this.arrCurrentValue[2]);
        this.PowerHarmonicValue[0] = buildValue(this.arrPValue[0]);
        this.PowerHarmonicValue[1] = buildValue(this.arrPValue[1]);
        this.PowerHarmonicValue[2] = buildValue(this.arrPValue[2]);
        this.PowerHarmonicValue[3] = buildValue(this.arrQValue[0]);
        this.PowerHarmonicValue[4] = buildValue(this.arrQValue[1]);
        this.PowerHarmonicValue[5] = buildValue(this.arrQValue[2]);
        this.PowerHarmonicValue[6] = buildValue(this.arrSValue[0]);
        this.PowerHarmonicValue[7] = buildValue(this.arrSValue[1]);
        this.PowerHarmonicValue[8] = buildValue(this.arrSValue[2]);
        this.THD = buildValue(this.arrTHD, 2);
    }

    private String buildValue(Double[] value, int pointIndex) {
        if (value == null) {
            return PdfObject.NOTHING;
        }
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < value.length; i++) {
            if (i == 0) {
                sb.append(DataProcessing.RoundValueString(value[i], pointIndex));
            } else {
                sb.append("|");
                sb.append(DataProcessing.RoundValueString(value[i], pointIndex));
            }
        }
        return sb.toString();
    }

    private String buildValue(ExplainedModel[] value) {
        if (value == null) {
            return PdfObject.NOTHING;
        }
        StringBuffer sb = new StringBuffer();
        for (int i = 0; i < value.length; i++) {
            if (i == 0) {
                sb.append(value[i].getStringValue());
            } else {
                sb.append("|");
                sb.append(value[i].getStringValue());
            }
        }
        return sb.toString();
    }

    public Double[] parseDouble(String data) {
        String[] stringArray = data.split("\\|");
        Double[] dataArray = new Double[stringArray.length];
        for (int i = 0; i < stringArray.length; i++) {
            dataArray[i] = Double.valueOf(OtherUtils.parseDouble(stringArray[i]));
        }
        return dataArray;
    }

    public ExplainedModel[] parseModel(String data) {
        String[] stringArray = data.split("\\|");
        ExplainedModel[] dataArray = new ExplainedModel[stringArray.length];
        for (int i = 0; i < stringArray.length; i++) {
            dataArray[i] = new ExplainedModel(0, stringArray[i]);
        }
        return dataArray;
    }

    public ExplainedModel[] parseModel(String data, String replace) {
        String[] stringArray = data.split("\\|");
        ExplainedModel[] dataArray = new ExplainedModel[stringArray.length];
        for (int i = 0; i < stringArray.length; i++) {
            dataArray[i] = new ExplainedModel(Double.valueOf(OtherUtils.parseDouble(stringArray[i].replace(replace, PdfObject.NOTHING))), stringArray[i]);
        }
        return dataArray;
    }

    public void setValue(Double[] arrUa, Double[] arrUb, Double[] arrUc, Double[] arrAngleUa, Double[] arrAngleUb, Double[] arrAngleUc, Double[] arrIa, Double[] arrIb, Double[] arrIc, Double[] arrAngleIa, Double[] arrAngleIb, Double[] arrAngleIc) {
        this.arrVoltageContent[0] = arrUa;
        this.arrVoltageContent[1] = arrUb;
        this.arrVoltageContent[2] = arrUc;
        this.arrVoltageAngle[0] = arrAngleUa;
        this.arrVoltageAngle[1] = arrAngleUb;
        this.arrVoltageAngle[2] = arrAngleUc;
        this.arrCurrentContent[0] = arrIa;
        this.arrCurrentContent[1] = arrIb;
        this.arrCurrentContent[2] = arrIc;
        this.arrCurrentAngle[0] = arrAngleIa;
        this.arrCurrentAngle[1] = arrAngleIb;
        this.arrCurrentAngle[2] = arrAngleIc;
    }
}
