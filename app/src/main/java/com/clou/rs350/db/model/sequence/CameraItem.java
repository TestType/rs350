package com.clou.rs350.db.model.sequence;

import com.itextpdf.text.pdf.PdfObject;
import java.io.Serializable;

public class CameraItem implements Serializable {
    public int Index;
    public String PhotoPath = PdfObject.NOTHING;
}
