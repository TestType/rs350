package com.clou.rs350.db.model;

import com.itextpdf.text.pdf.PdfObject;

public class SiteData extends RatioData {
    public DBDataModel CTPrimary_Installed = new DBDataModel(1L, PdfObject.NOTHING);
    public DBDataModel CTSecondary_Installed = new DBDataModel(1L, PdfObject.NOTHING);
    public String CustomerSr = PdfObject.NOTHING;
    public MeterInfo[] MeterInfo = {new MeterInfo(), new MeterInfo(), new MeterInfo()};
    public String[] Meter_CertificationYear = {PdfObject.NOTHING, PdfObject.NOTHING, PdfObject.NOTHING};
    public int Meter_Count = 1;
    public String MultiplyingFactor = PdfObject.NOTHING;
    public String ObservedLoad = PdfObject.NOTHING;
    public DBDataModel PTPrimary_Installed = new DBDataModel(1L, PdfObject.NOTHING);
    public DBDataModel PTSecondary_Installed = new DBDataModel(1L, PdfObject.NOTHING);
    public String SanctionedLoad = PdfObject.NOTHING;
}
