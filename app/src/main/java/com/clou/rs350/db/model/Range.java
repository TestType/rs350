package com.clou.rs350.db.model;

import com.clou.rs350.device.MeterBaseDevice;
import com.clou.rs350.device.model.ExplainedModel;
import com.clou.rs350.device.utils.DataUtils;
import com.clou.rs350.utils.DataProcessing;
import com.clou.rs350.utils.OtherUtils;
import com.itextpdf.text.pdf.PdfObject;
import java.io.Serializable;

public class Range implements Serializable {
    public boolean CannotDiscernClamp = false;
    public String ConnectionType = PdfObject.NOTHING;
    public ExplainedModel[] ConnectionTypeIndex = {new ExplainedModel(), new ExplainedModel(), new ExplainedModel(), new ExplainedModel(), new ExplainedModel(), new ExplainedModel()};
    public ExplainedModel CurrentConnectionTypeL1 = new ExplainedModel();
    public ExplainedModel CurrentConnectionTypeL2 = new ExplainedModel();
    public ExplainedModel CurrentConnectionTypeL3 = new ExplainedModel();
    public String CurrentRangeL1 = PdfObject.NOTHING;
    public int CurrentRangeL1Index = 0;
    public String CurrentRangeL2 = PdfObject.NOTHING;
    public int CurrentRangeL2Index = 0;
    public String CurrentRangeL3 = PdfObject.NOTHING;
    public int CurrentRangeL3Index = 0;
    public int Definition = 0;
    public boolean[] OverloadHint = new boolean[6];
    public int PQFlag = 0;
    public ExplainedModel VoltageConnectionTypeL1 = new ExplainedModel();
    public ExplainedModel VoltageConnectionTypeL2 = new ExplainedModel();
    public ExplainedModel VoltageConnectionTypeL3 = new ExplainedModel();
    public String VoltageRangeL1 = PdfObject.NOTHING;
    public int VoltageRangeL1Index = 0;
    public String VoltageRangeL2 = PdfObject.NOTHING;
    public int VoltageRangeL2Index = 0;
    public String VoltageRangeL3 = PdfObject.NOTHING;
    public int VoltageRangeL3Index = 0;
    public int Wiring = 0;
    public String WiringValue = "4Wa";

    public void parseRangeData(MeterBaseDevice Device) {
        int i = -1;
        if (Device != null) {
            this.CannotDiscernClamp = false;
            try {
                this.ConnectionTypeIndex = Device.getConnectTypeValue();
                int[] VoltageIndex = Device.getVoltageRangeIndex();
                int[] CurrentIndex = Device.getCurrentRangeIndex();
                this.Wiring = Device.getWiringValue();
                this.PQFlag = Device.getPQFlagValue();
                this.Definition = Device.getDefinitionValue();
                this.WiringValue = OtherUtils.parseWiring(this.Wiring, this.PQFlag);
                this.OverloadHint = Device.getOverloadValue();
                ExplainedModel[] tmpConnectTypeValue = Device.getConnectTypeValue();
                ExplainedModel[] tmpRangeValue = Device.getRangeValue();
                try {
                    this.VoltageConnectionTypeL1 = new ExplainedModel(Integer.valueOf(tmpConnectTypeValue[0].getIntValue()), tmpConnectTypeValue[0].getStringValue());
                    if (255 == tmpConnectTypeValue[0].getIntValue()) {
                        this.CannotDiscernClamp = true;
                    }
                    this.VoltageRangeL1Index = tmpRangeValue[0].getIntValue() - VoltageIndex[this.ConnectionTypeIndex[0].getIntValue()];
                    this.VoltageRangeL1 = tmpRangeValue[0].getStringValue();
                } catch (Exception e) {
                }
                try {
                    this.VoltageConnectionTypeL2 = new ExplainedModel(Integer.valueOf(tmpConnectTypeValue[1].getIntValue()), tmpConnectTypeValue[1].getStringValue());
                    if (255 == tmpConnectTypeValue[1].getIntValue()) {
                        this.CannotDiscernClamp = true;
                    }
                    this.VoltageRangeL2Index = tmpRangeValue[1].getIntValue() - VoltageIndex[this.ConnectionTypeIndex[1].getIntValue()];
                    this.VoltageRangeL2 = tmpRangeValue[1].getStringValue();
                } catch (Exception e2) {
                }
                try {
                    this.VoltageConnectionTypeL3 = new ExplainedModel(Integer.valueOf(tmpConnectTypeValue[2].getIntValue()), tmpConnectTypeValue[2].getStringValue());
                    if (255 == tmpConnectTypeValue[2].getIntValue()) {
                        this.CannotDiscernClamp = true;
                    }
                    this.VoltageRangeL3Index = tmpRangeValue[2].getIntValue() - VoltageIndex[this.ConnectionTypeIndex[2].getIntValue()];
                    this.VoltageRangeL3 = tmpRangeValue[2].getStringValue();
                } catch (Exception e3) {
                }
                try {
                    this.CurrentConnectionTypeL1 = new ExplainedModel(Integer.valueOf(tmpConnectTypeValue[3].getIntValue()), tmpConnectTypeValue[3].getStringValue());
                    if (255 == tmpConnectTypeValue[3].getIntValue()) {
                        this.CannotDiscernClamp = true;
                    }
                    this.CurrentRangeL1Index = tmpRangeValue[3].getIntValue() - CurrentIndex[this.ConnectionTypeIndex[3].getIntValue()];
                    this.CurrentRangeL1 = tmpRangeValue[3].getStringValue();
                } catch (Exception e4) {
                }
                try {
                    this.CurrentConnectionTypeL2 = new ExplainedModel(Integer.valueOf(tmpConnectTypeValue[4].getIntValue()), tmpConnectTypeValue[4].getStringValue());
                    if (255 == tmpConnectTypeValue[4].getIntValue()) {
                        this.CannotDiscernClamp = true;
                    }
                    this.CurrentRangeL2Index = tmpRangeValue[4].getIntValue() - CurrentIndex[this.ConnectionTypeIndex[4].getIntValue()];
                    this.CurrentRangeL2 = tmpRangeValue[4].getStringValue();
                } catch (Exception e5) {
                }
                try {
                    this.CurrentConnectionTypeL3 = new ExplainedModel(Integer.valueOf(tmpConnectTypeValue[5].getIntValue()), tmpConnectTypeValue[5].getStringValue());
                    if (255 == tmpConnectTypeValue[5].getIntValue()) {
                        this.CannotDiscernClamp = true;
                    }
                    this.CurrentRangeL3Index = tmpRangeValue[5].getIntValue() - CurrentIndex[this.ConnectionTypeIndex[5].getIntValue()];
                    this.CurrentRangeL3 = tmpRangeValue[5].getStringValue();
                } catch (Exception e6) {
                }
                this.ConnectionType = this.VoltageConnectionTypeL1 + " " + this.CurrentConnectionTypeL1;
                int i_Auto = Device.getRangeAutoValue();
                this.VoltageRangeL1Index = (i_Auto & 1) == 0 ? -1 : this.VoltageRangeL1Index;
                this.VoltageRangeL2Index = (i_Auto & 4) == 0 ? -1 : this.VoltageRangeL2Index;
                this.VoltageRangeL3Index = (i_Auto & 16) == 0 ? -1 : this.VoltageRangeL3Index;
                this.CurrentRangeL1Index = (i_Auto & 2) == 0 ? -1 : this.CurrentRangeL1Index;
                this.CurrentRangeL2Index = (i_Auto & 8) == 0 ? -1 : this.CurrentRangeL2Index;
                if ((i_Auto & 32) != 0) {
                    i = this.CurrentRangeL3Index;
                }
                this.CurrentRangeL3Index = i;
            } catch (Exception e7) {
            }
        }
    }

    /* access modifiers changed from: protected */
    public DBDataModel parse(ExplainedModel value) {
        DBDataModel model = new DBDataModel();
        try {
            model.d_Value = value.getDoubleValue();
            model.s_Value = value.getStringValue();
        } catch (Exception e) {
        }
        return model;
    }

    /* access modifiers changed from: protected */
    public DBDataModel parse(Object value, String unit) {
        DBDataModel model = new DBDataModel();
        try {
            model.d_Value = OtherUtils.convertToDouble(value);
            model.s_Value = DataUtils.add_unit(Double.valueOf(model.d_Value), unit);
        } catch (Exception e) {
        }
        return model;
    }

    /* access modifiers changed from: protected */
    public DBDataModel parseRound(Object value) {
        DBDataModel model = new DBDataModel();
        try {
            model.d_Value = OtherUtils.convertToDouble(value);
            model.s_Value = DataProcessing.RoundValue(Double.valueOf(model.d_Value));
        } catch (Exception e) {
        }
        return model;
    }

    /* access modifiers changed from: protected */
    public DBDataModel parse(Object value, int dotIndex, String unit) {
        DBDataModel model = new DBDataModel();
        try {
            model.d_Value = OtherUtils.convertToDouble(value);
            model.s_Value = String.valueOf(DataProcessing.RoundValueString(Double.valueOf(model.d_Value), dotIndex)) + unit;
        } catch (Exception e) {
        }
        return model;
    }

    /* access modifiers changed from: protected */
    public DBDataModel parseRound(Object value, int dotIndex) {
        DBDataModel model = new DBDataModel();
        try {
            model.d_Value = OtherUtils.convertToDouble(value);
            model.s_Value = DataProcessing.RoundValueString(Double.valueOf(model.d_Value), dotIndex);
        } catch (Exception e) {
        }
        return model;
    }

    public static int getAuto(int[] values) {
        int i;
        int i2;
        int i3 = 0;
        int auto = 0 | (values[0] == 0 ? 0 : 1);
        if (values[1] == 0) {
            i = 0;
        } else {
            i = 2;
        }
        int auto2 = auto | i;
        if (values[2] == 0) {
            i2 = 0;
        } else {
            i2 = 4;
        }
        int auto3 = auto2 | i2 | (values[3] == 0 ? 0 : 8) | (values[4] == 0 ? 0 : 16);
        if (values[5] != 0) {
            i3 = 32;
        }
        return auto3 | i3;
    }
}
