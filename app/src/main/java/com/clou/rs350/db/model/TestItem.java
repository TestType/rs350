package com.clou.rs350.db.model;

import com.clou.rs350.model.ClouData;
import com.itextpdf.text.pdf.PdfObject;
import java.io.Serializable;

public class TestItem implements Serializable {
    public int AngleDefinition = 0;
    public String Calib_Date = PdfObject.NOTHING;
    public String Calib_Valid_Date = PdfObject.NOTHING;
    public String CertificateID = PdfObject.NOTHING;
    public String CustomerSerialNo = PdfObject.NOTHING;
    public String Designation = PdfObject.NOTHING;
    public String Humidity = PdfObject.NOTHING;
    public String[] MeterSerialNo = {PdfObject.NOTHING, PdfObject.NOTHING, PdfObject.NOTHING};
    public String OperatorID = PdfObject.NOTHING;
    public String OperatorName = PdfObject.NOTHING;
    public String Remark = PdfObject.NOTHING;
    public String StandardSerialNo = PdfObject.NOTHING;
    public String Temperature = PdfObject.NOTHING;
    public String TestItem = PdfObject.NOTHING;
    public String TestTime = PdfObject.NOTHING;

    public void parseData(String testItem) {
        this.TestItem = testItem;
        parseData();
    }

    public void parseData() {
        StandardInfo tmpStandardInfo = ClouData.getInstance().getStandardInfo();
        this.StandardSerialNo = tmpStandardInfo.StandardSerialNo;
        this.Calib_Date = tmpStandardInfo.getCalibrateDate();
        this.Calib_Valid_Date = tmpStandardInfo.getCalib_Valid_Date();
        this.CertificateID = tmpStandardInfo.CertificateID;
        OperatorInfo tmpOperatorInfo = ClouData.getInstance().getOperatorInfo();
        this.OperatorID = tmpOperatorInfo.OperatorID;
        this.OperatorName = tmpOperatorInfo.OperatorName;
        this.Designation = tmpOperatorInfo.Designation;
        this.AngleDefinition = (ClouData.getInstance().getSettingBasic().VectorDefinition << 1) + ClouData.getInstance().getSettingBasic().AngleDefinition;
    }
}
