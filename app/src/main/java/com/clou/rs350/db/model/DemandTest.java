package com.clou.rs350.db.model;

import com.clou.rs350.device.MeterBaseDevice;
import com.clou.rs350.device.model.ExplainedModel;
import com.clou.rs350.model.ClouData;
import com.clou.rs350.utils.OtherUtils;
import com.itextpdf.text.pdf.PdfObject;

public class DemandTest extends BasicMeasurement {
    public double[] Accuracy;
    public DBDataModel[] BeginPower;
    public DBDataModel[] EndPower;
    public DBDataModel[] Error;
    public String MeterIndex;
    public String MeterSerialNo;
    public DBDataModel[] Result;
    public String RunningTime;
    public DBDataModel[] StandardPower;
    public long TestEndTime;
    public long TestStartTime;
    public long TestTime;
    public int Unit;

    public DemandTest() {
        this.MeterIndex = "0";
        this.MeterSerialNo = PdfObject.NOTHING;
        this.Unit = 0;
        this.Accuracy = new double[]{0.0d, 0.0d, 0.0d};
        this.Result = new DBDataModel[]{new DBDataModel(-1L, PdfObject.NOTHING), new DBDataModel(-1L, PdfObject.NOTHING), new DBDataModel(-1L, PdfObject.NOTHING)};
        this.BeginPower = new DBDataModel[]{new DBDataModel(), new DBDataModel(), new DBDataModel()};
        this.EndPower = new DBDataModel[]{new DBDataModel(), new DBDataModel(), new DBDataModel()};
        this.StandardPower = new DBDataModel[]{new DBDataModel(), new DBDataModel(), new DBDataModel()};
        this.Error = new DBDataModel[]{new DBDataModel(), new DBDataModel(), new DBDataModel()};
        this.TestTime = 15;
        this.TestStartTime = 0;
        this.TestEndTime = 0;
        this.RunningTime = PdfObject.NOTHING;
        this.MeterIndex = "0";
    }

    public DemandTest(int meterIndex) {
        this.MeterIndex = "0";
        this.MeterSerialNo = PdfObject.NOTHING;
        this.Unit = 0;
        this.Accuracy = new double[]{0.0d, 0.0d, 0.0d};
        this.Result = new DBDataModel[]{new DBDataModel(-1L, PdfObject.NOTHING), new DBDataModel(-1L, PdfObject.NOTHING), new DBDataModel(-1L, PdfObject.NOTHING)};
        this.BeginPower = new DBDataModel[]{new DBDataModel(), new DBDataModel(), new DBDataModel()};
        this.EndPower = new DBDataModel[]{new DBDataModel(), new DBDataModel(), new DBDataModel()};
        this.StandardPower = new DBDataModel[]{new DBDataModel(), new DBDataModel(), new DBDataModel()};
        this.Error = new DBDataModel[]{new DBDataModel(), new DBDataModel(), new DBDataModel()};
        this.TestTime = 15;
        this.TestStartTime = 0;
        this.TestEndTime = 0;
        this.RunningTime = PdfObject.NOTHING;
        this.MeterIndex = new StringBuilder(String.valueOf(meterIndex)).toString();
    }

    public void setPowerValue(int pqFlag, long testTime, double begainEnergy, double endEnergy) {
        String unit = OtherUtils.parseUnitByWiring(pqFlag, "kW");
        this.TestTime = testTime;
        this.BeginPower[pqFlag] = new DBDataModel(begainEnergy, String.valueOf(begainEnergy) + unit);
        this.EndPower[pqFlag] = new DBDataModel(endEnergy, String.valueOf(endEnergy) + unit);
        calculateError(pqFlag);
    }

    public void setPowerValue(int pqFlag, double endPower) {
        String tmpUnit = OtherUtils.parseUnitByWiring(pqFlag, "kW");
        switch (this.Unit) {
            case 1:
                tmpUnit = "k" + tmpUnit;
                break;
            case 2:
                tmpUnit = "M" + tmpUnit;
                break;
        }
        this.EndPower[pqFlag] = new DBDataModel(endPower, String.valueOf(endPower) + tmpUnit);
        calculateError(pqFlag);
    }

    public void calculateError(int pqFlag) {
        double standardValue = this.StandardPower[pqFlag].d_Value;
        float Multiple = 1.0f;
        switch (this.Unit) {
            case 1:
                Multiple = 1000.0f;
                break;
            case 2:
                Multiple = 1000000.0f;
                break;
        }
        if (this.EndPower[pqFlag].d_Value > 0.0d) {
            this.Error[pqFlag] = parse(Double.valueOf(100.0d * (((this.EndPower[pqFlag].d_Value * ((double) Multiple)) - standardValue) / standardValue)), ClouData.getInstance().getSettingBasic().ErrorDigit, "%");
            this.Result[pqFlag] = OtherUtils.getErrorResult(this.Accuracy[pqFlag], this.Error[pqFlag]);
        }
        if (this.EndPower[pqFlag].d_Value == this.BeginPower[pqFlag].d_Value && this.BeginPower[pqFlag].d_Value > this.StandardPower[pqFlag].d_Value) {
            this.Error[pqFlag] = parse(0, 3, "%");
            this.Result[pqFlag] = OtherUtils.getErrorResult(this.Accuracy[pqFlag], this.Error[pqFlag]);
        }
    }

    public String getRunningTime() {
        return null;
    }

    public long getRunningTimeMs() {
        if (this.TestStartTime > 0) {
            return System.currentTimeMillis() - this.TestStartTime;
        }
        return 0;
    }

    @Override // com.clou.rs350.db.model.BasicMeasurement
    public void parseData(MeterBaseDevice Device) {
        super.parseData(Device);
        int PQFlag = Device.getPQFlagValue();
        ExplainedModel[] tmpValues = Device.getDemandValues();
        this.StandardPower[PQFlag] = parse(Double.valueOf(tmpValues[0].getDoubleValue()), new String[]{"W", "var", "VA"}[PQFlag]);
    }

    public String getRemainTime() {
        long testTime = this.TestStartTime > 0 ? getRunningTimeMs() : this.TestStartTime;
        long remainTime = (this.TestTime * 60000) - testTime;
        if (remainTime <= 0) {
            return PdfObject.NOTHING;
        }
        this.RunningTime = OtherUtils.getTime(testTime / 1000);
        return OtherUtils.getTime(remainTime / 1000);
    }

    @Override // com.clou.rs350.db.model.BasicMeasurement
    public boolean isEmpty() {
        boolean result = true;
        for (int i = 0; i < 3; i++) {
            if (-1 != this.Result[i].l_Value) {
                result = false;
            }
        }
        return result;
    }

    public void cleanError() {
        this.Result = new DBDataModel[]{new DBDataModel(-1L, PdfObject.NOTHING), new DBDataModel(-1L, PdfObject.NOTHING), new DBDataModel(-1L, PdfObject.NOTHING)};
        this.BeginPower = new DBDataModel[]{new DBDataModel(), new DBDataModel(), new DBDataModel()};
        this.EndPower = new DBDataModel[]{new DBDataModel(), new DBDataModel(), new DBDataModel()};
        this.StandardPower = new DBDataModel[]{new DBDataModel(), new DBDataModel(), new DBDataModel()};
        this.Error = new DBDataModel[]{new DBDataModel(), new DBDataModel(), new DBDataModel()};
    }
}
