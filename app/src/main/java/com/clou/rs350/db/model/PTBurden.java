package com.clou.rs350.db.model;

import android.database.Cursor;

import com.clou.rs350.CLApplication;
import com.clou.rs350.R;
import com.clou.rs350.device.MeterBaseDevice;
import com.clou.rs350.device.model.ExplainedModel;
import com.itextpdf.text.pdf.PdfObject;

public class PTBurden extends BasicMeasurement {
    public DBDataModel[] BurdenPersents;
    public DBDataModel[] BurdenVA;
    public DBDataModel[] BurdenW;
    public DBDataModel[] CurrentValue;
    public DBDataModel NormalRatio;
    public DBDataModel PTVA;
    public DBDataModel[] Result;
    public String SerialNo;
    public DBDataModel[] VoltageCurrentAngle;
    public DBDataModel[] VoltageValue;
    public DBDataModel VoltageValueNormalSecondary;

    public PTBurden() {
        this.SerialNo = PdfObject.NOTHING;
        this.VoltageValue = new DBDataModel[]{new DBDataModel(), new DBDataModel(), new DBDataModel()};
        this.CurrentValue = new DBDataModel[]{new DBDataModel(), new DBDataModel(), new DBDataModel()};
        this.VoltageCurrentAngle = new DBDataModel[]{new DBDataModel(), new DBDataModel(), new DBDataModel()};
        this.BurdenW = new DBDataModel[]{new DBDataModel(), new DBDataModel(), new DBDataModel()};
        this.BurdenVA = new DBDataModel[]{new DBDataModel(), new DBDataModel(), new DBDataModel()};
        this.BurdenPersents = new DBDataModel[]{new DBDataModel(), new DBDataModel(), new DBDataModel()};
        this.Result = new DBDataModel[]{new DBDataModel(-1L, "---"), new DBDataModel(-1L, "---"), new DBDataModel(-1L, "---")};
        this.PTVA = new DBDataModel();
        this.VoltageValueNormalSecondary = new DBDataModel();
        this.NormalRatio = new DBDataModel();
    }

    public PTBurden(Cursor cursor) {
        this.SerialNo = PdfObject.NOTHING;
        this.VoltageValue = new DBDataModel[]{new DBDataModel(), new DBDataModel(), new DBDataModel()};
        this.CurrentValue = new DBDataModel[]{new DBDataModel(), new DBDataModel(), new DBDataModel()};
        this.VoltageCurrentAngle = new DBDataModel[]{new DBDataModel(), new DBDataModel(), new DBDataModel()};
        this.BurdenW = new DBDataModel[]{new DBDataModel(), new DBDataModel(), new DBDataModel()};
        this.BurdenVA = new DBDataModel[]{new DBDataModel(), new DBDataModel(), new DBDataModel()};
        this.BurdenPersents = new DBDataModel[]{new DBDataModel(), new DBDataModel(), new DBDataModel()};
        this.Result = new DBDataModel[]{new DBDataModel(-1L, "---"), new DBDataModel(-1L, "---"), new DBDataModel(-1L, "---")};
        this.PTVA = new DBDataModel();
        this.VoltageValueNormalSecondary = new DBDataModel();
        this.NormalRatio = new DBDataModel();
    }

    public void parseData(MeterBaseDevice Device, int Index) {
        ExplainedModel[] TmpValues = Device.getPTBurdenValue();
        this.VoltageValue[0] = parse(TmpValues[0]);
        this.VoltageValue[1] = parse(TmpValues[1]);
        this.VoltageValue[2] = parse(TmpValues[2]);
        this.CurrentValue[0] = parse(TmpValues[3]);
        this.CurrentValue[1] = parse(TmpValues[4]);
        this.CurrentValue[2] = parse(TmpValues[5]);
        this.VoltageCurrentAngle[0] = parse(TmpValues[6]);
        this.VoltageCurrentAngle[1] = parse(TmpValues[7]);
        this.VoltageCurrentAngle[2] = parse(TmpValues[8]);
        this.BurdenW[0] = parse(TmpValues[9]);
        this.BurdenW[1] = parse(TmpValues[10]);
        this.BurdenW[2] = parse(TmpValues[11]);
        this.BurdenVA[0] = parse(TmpValues[12]);
        this.BurdenVA[1] = parse(TmpValues[13]);
        this.BurdenVA[2] = parse(TmpValues[14]);
        CalcPersents(0);
        CalcPersents(1);
        CalcPersents(2);
    }

    public void setVAValue(double VA) {
        this.PTVA = new DBDataModel(VA, String.valueOf(VA) + "VA");
        CalcPersents(0);
        CalcPersents(1);
        CalcPersents(2);
    }

    public void CalcPersents(int Index) {
        if (!this.BurdenVA[Index].s_Value.equals("---")) {
            this.BurdenPersents[Index] = parse(Double.valueOf((this.BurdenVA[Index].d_Value * 100.0d) / this.PTVA.d_Value), "%");
            String[] s_Result = CLApplication.app.getResources().getStringArray(R.array.limit);
            int i_Result = 0;
            if (this.BurdenPersents[Index].d_Value < 80.0d || this.BurdenPersents[Index].d_Value > 100.0d) {
                i_Result = 1;
            }
            this.Result[Index] = new DBDataModel((long) i_Result, s_Result[i_Result]);
        }
    }

    public void setRatedValue(double currentValueNormalRatio) {
        if (currentValueNormalRatio > 0.0d) {
            this.NormalRatio = parseRound(Double.valueOf(currentValueNormalRatio));
        }
    }

    public void cleanData() {
        this.VoltageValue = new DBDataModel[]{new DBDataModel(), new DBDataModel(), new DBDataModel()};
        this.CurrentValue = new DBDataModel[]{new DBDataModel(), new DBDataModel(), new DBDataModel()};
        this.VoltageCurrentAngle = new DBDataModel[]{new DBDataModel(), new DBDataModel(), new DBDataModel()};
        this.BurdenW = new DBDataModel[]{new DBDataModel(), new DBDataModel(), new DBDataModel()};
        this.BurdenVA = new DBDataModel[]{new DBDataModel(), new DBDataModel(), new DBDataModel()};
        this.BurdenPersents = new DBDataModel[]{new DBDataModel(), new DBDataModel(), new DBDataModel()};
        this.Result = new DBDataModel[]{new DBDataModel(-1L, "---"), new DBDataModel(-1L, "---"), new DBDataModel(-1L, "---")};
    }
}
