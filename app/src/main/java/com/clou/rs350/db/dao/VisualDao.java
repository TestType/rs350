package com.clou.rs350.db.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import com.clou.rs350.db.model.sequence.SealData;
import com.clou.rs350.db.model.sequence.SealItem;
import com.clou.rs350.db.model.sequence.VisualData;
import com.clou.rs350.db.model.sequence.VisualItem;
import com.clou.rs350.manager.DatabaseManager;
import java.util.ArrayList;
import java.util.List;

public class VisualDao extends BaseTestDao {
    private static final String TABLE_NAME = "TestVisual";

    private static final class TableColumns {
        public static final String Activities = "Activities";
        public static final String ActivitiesIndex = "ActivitiesIndex";
        public static final String Remark = "Remark";
        public static final String Result = "Result";
        public static final String VisualIndex = "VisualIndex";

        private TableColumns() {
        }
    }

    public VisualDao(Context context) {
        super(context, TABLE_NAME);
    }

    public void createTable(SQLiteDatabase db) {
        this.m_TableName = TABLE_NAME;
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("create table if not exists ").append(this.m_TableName).append(" (");
        stringBuffer.append("intMyID").append(" integer primary key autoincrement,");
        stringBuffer.append(this.m_TableKey).append(" VARCHAR(50),");
        stringBuffer.append("CreateTime").append(" VARCHAR(50),");
        stringBuffer.append("IsRead").append(" integer default 0,");
        stringBuffer.append(TableColumns.VisualIndex).append(" integer,");
        stringBuffer.append(TableColumns.ActivitiesIndex).append(" integer,");
        stringBuffer.append("Activities").append(" VARCHAR(50),");
        stringBuffer.append("Result").append(" VARCHAR(20),");
        stringBuffer.append("Remark").append(" VARCHAR(500));");
        db.execSQL(stringBuffer.toString());
    }

    @Override // com.clou.rs350.db.dao.BaseDao, com.clou.rs350.db.dao.BaseTestDao
    public void upgradeTable(SQLiteDatabase db, int oldVersion, int newVersion) {
        if (oldVersion <= 3) {
            db.execSQL("alter table Visual rename to TestVisual");
        }
    }

    @Override // com.clou.rs350.db.dao.BaseDao, com.clou.rs350.db.dao.BaseTestDao
    public synchronized boolean insertObject(Object Data, String Time) {
        boolean z;
        SQLiteDatabase db = DatabaseManager.getWriteableDatabase(this.m_Context);
        ContentValues values = new ContentValues();
        long status = 0;
        VisualData objects = (VisualData) Data;
        for (int i = 0; i < objects.Visuals.size(); i++) {
            VisualItem object = objects.Visuals.get(i);
            values.put(this.m_TableKey, objects.SerialNo.toString());
            values.put("CreateTime", Time);
            values.put(TableColumns.VisualIndex, Integer.valueOf(objects.Index));
            values.put(TableColumns.ActivitiesIndex, Integer.valueOf(object.Index));
            values.put("Activities", object.Activities);
            values.put("Result", object.Result.s_Value);
            values.put("Remark", objects.Remark);
            status += db.insert(TABLE_NAME, null, values);
        }
        if (status != -1) {
            z = true;
        } else {
            z = false;
        }
        return z;
    }

    /* access modifiers changed from: protected */
    @Override // com.clou.rs350.db.dao.BaseDao, com.clou.rs350.db.dao.BaseTestDao
    public Object parseData(Cursor cursor) {
        SealData values = new SealData();
        values.SerialNo = cursor.getString(cursor.getColumnIndex(this.m_TableKey));
        do {
            SealItem value = new SealItem();
            value.Index = cursor.getInt(cursor.getColumnIndex(TableColumns.ActivitiesIndex));
            value.SealLocation = cursor.getString(cursor.getColumnIndex("Remark"));
            value.SealSr = cursor.getString(cursor.getColumnIndex("Activities"));
            values.Seals.add(value);
        } while (cursor.moveToNext());
        return values;
    }

    @Override // com.clou.rs350.db.dao.BaseDao
    public List<Object> queryDataListByKeyAndTime(String serialNo, String time) {
        Cursor cursor = DatabaseManager.getWriteableDatabase(this.m_Context).query(this.m_TableName, null, String.valueOf(this.m_TableKey) + " =? and " + "CreateTime" + " =?", new String[]{serialNo, time}, null, null, null);
        List<Object> Datas = new ArrayList<>();
        Datas.add(new VisualData());
        while (cursor.moveToNext()) {
            int Index = cursor.getInt(cursor.getColumnIndex(TableColumns.VisualIndex));
            int Size = Datas.size() - 1;
            if (Index != ((VisualData) Datas.get(Size)).Index) {
                Datas.add(new SealData());
                Size++;
            }
            ((VisualData) Datas.get(Size)).SerialNo = cursor.getString(cursor.getColumnIndex(this.m_TableKey));
            ((VisualData) Datas.get(Size)).Index = cursor.getInt(cursor.getColumnIndex(TableColumns.VisualIndex));
            ((VisualData) Datas.get(Size)).Remark = cursor.getString(cursor.getColumnIndex("Remark"));
            VisualItem value = new VisualItem();
            value.Index = cursor.getInt(cursor.getColumnIndex(TableColumns.ActivitiesIndex));
            value.Activities = cursor.getString(cursor.getColumnIndex("Activities"));
            value.Result.s_Value = cursor.getString(cursor.getColumnIndex("Result"));
            ((VisualData) Datas.get(Size)).Visuals.add(value);
        }
        cursor.close();
        if (Datas.size() > 1 || ((VisualData) Datas.get(0)).Visuals.size() > 0) {
            return Datas;
        }
        return null;
    }
}
