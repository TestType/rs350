package com.clou.rs350.db.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import com.clou.rs350.db.dao.BaseTestDao;
import com.clou.rs350.db.model.PTComparison;
import com.clou.rs350.manager.DatabaseManager;

public class PTComparisonDao extends BaseTestDao {
    private static final String TABLE_NAME = "TestPTMeasurement";

    private static final class TableColumns {
        public static final String AngleErrorL1 = "AngleErrorL1";
        public static final String AngleErrorL2 = "AngleErrorL2";
        public static final String AngleErrorL3 = "AngleErrorL3";
        public static final String ID = "intMyID";
        public static final String Location1AngleL1 = "Location1AngleL1";
        public static final String Location1AngleL2 = "Location1AngleL2";
        public static final String Location1AngleL3 = "Location1AngleL3";
        public static final String Location1VoltageL1 = "Location1VoltageL1";
        public static final String Location1VoltageL2 = "Location1VoltageL2";
        public static final String Location1VoltageL3 = "Location1VoltageL3";
        public static final String Location2AngleL1 = "Location2AngleL1";
        public static final String Location2AngleL2 = "Location2AngleL2";
        public static final String Location2AngleL3 = "Location2AngleL3";
        public static final String Location2VoltageL1 = "Location2VoltageL1";
        public static final String Location2VoltageL2 = "Location2VoltageL2";
        public static final String Location2VoltageL3 = "Location2VoltageL3";
        public static final String RatioErrorL1 = "RatioErrorL1";
        public static final String RatioErrorL2 = "RatioErrorL2";
        public static final String RatioErrorL3 = "RatioErrorL3";
        public static final String TestTime = "TestTime";
        public static final String VoltageDifferentL1 = "VoltageDifferentL1";
        public static final String VoltageDifferentL2 = "VoltageDifferentL2";
        public static final String VoltageDifferentL3 = "VoltageDifferentL3";

        private TableColumns() {
        }
    }

    public PTComparisonDao(Context context) {
        super(context, TABLE_NAME);
    }

    public void createTable(SQLiteDatabase db) {
        new StringBuffer();
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("create table if not exists ").append(TABLE_NAME).append(" (");
        stringBuffer.append("intMyID").append(" integer primary key autoincrement,");
        stringBuffer.append(this.m_TableKey).append(" VARCHAR(50),");
        stringBuffer.append("CreateTime").append(" VARCHAR(50),");
        stringBuffer.append("IntWiringType").append(" integer default 0,");
        stringBuffer.append(SameColumns.WiringType).append(" VARCHAR(50),");
        stringBuffer.append(SameColumns.ConnectionMode).append(" VARCHAR(50),");
        stringBuffer.append("TestTime").append(" VARCHAR(20),");
        stringBuffer.append(TableColumns.Location1VoltageL1).append(" VARCHAR(20),");
        stringBuffer.append(TableColumns.Location1VoltageL2).append(" VARCHAR(20),");
        stringBuffer.append(TableColumns.Location1VoltageL3).append(" VARCHAR(20),");
        stringBuffer.append(TableColumns.Location1AngleL1).append(" VARCHAR(20),");
        stringBuffer.append(TableColumns.Location1AngleL2).append(" VARCHAR(20),");
        stringBuffer.append(TableColumns.Location1AngleL3).append(" VARCHAR(20),");
        stringBuffer.append(TableColumns.Location2VoltageL1).append(" VARCHAR(20),");
        stringBuffer.append(TableColumns.Location2VoltageL2).append(" VARCHAR(20),");
        stringBuffer.append(TableColumns.Location2VoltageL3).append(" VARCHAR(20),");
        stringBuffer.append(TableColumns.Location2AngleL1).append(" VARCHAR(20),");
        stringBuffer.append(TableColumns.Location2AngleL2).append(" VARCHAR(20),");
        stringBuffer.append(TableColumns.Location2AngleL3).append(" VARCHAR(20),");
        stringBuffer.append(TableColumns.VoltageDifferentL1).append(" VARCHAR(20),");
        stringBuffer.append(TableColumns.VoltageDifferentL2).append(" VARCHAR(20),");
        stringBuffer.append(TableColumns.VoltageDifferentL3).append(" VARCHAR(20),");
        stringBuffer.append(TableColumns.AngleErrorL1).append(" VARCHAR(20),");
        stringBuffer.append(TableColumns.AngleErrorL2).append(" VARCHAR(20),");
        stringBuffer.append(TableColumns.AngleErrorL3).append(" VARCHAR(20),");
        stringBuffer.append(TableColumns.RatioErrorL1).append(" VARCHAR(20),");
        stringBuffer.append(TableColumns.RatioErrorL2).append(" VARCHAR(20),");
        stringBuffer.append(TableColumns.RatioErrorL3).append(" VARCHAR(20));");
        db.execSQL(stringBuffer.toString());
    }

    @Override // com.clou.rs350.db.dao.BaseDao, com.clou.rs350.db.dao.BaseTestDao
    public void upgradeTable(SQLiteDatabase db, int oldVersion, int newVersion) {
        if (oldVersion <= 3) {
            db.execSQL("alter table PTMeasurement rename to TestPTMeasurement");
        }
        super.upgradeTable(db, oldVersion, newVersion);
    }

    @Override // com.clou.rs350.db.dao.BaseDao, com.clou.rs350.db.dao.BaseTestDao
    public synchronized boolean insertObject(Object Data, String Time) {
        boolean z = true;
        synchronized (this) {
            SQLiteDatabase db = DatabaseManager.getWriteableDatabase(this.m_Context);
            ContentValues values = new ContentValues();
            PTComparison object = (PTComparison) Data;
            values.put(this.m_TableKey, object.SerialNo);
            values.put("CreateTime", Time);
            values.put(SameColumns.WiringType, object.WiringValue);
            values.put("IntWiringType", Integer.valueOf(object.Wiring));
            values.put(SameColumns.ConnectionMode, object.ConnectionType);
            values.put("TestTime", object.Time);
            values.put(TableColumns.Location1VoltageL1, object.Location1Voltage[0].s_Value);
            values.put(TableColumns.Location1VoltageL2, object.Location1Voltage[1].s_Value);
            values.put(TableColumns.Location1VoltageL3, object.Location1Voltage[2].s_Value);
            values.put(TableColumns.Location1AngleL1, object.Location1Angle[0].s_Value);
            values.put(TableColumns.Location1AngleL2, object.Location1Angle[1].s_Value);
            values.put(TableColumns.Location1AngleL3, object.Location1Angle[2].s_Value);
            values.put(TableColumns.Location2VoltageL1, object.Location2Voltage[0].s_Value);
            values.put(TableColumns.Location2VoltageL2, object.Location2Voltage[1].s_Value);
            values.put(TableColumns.Location2VoltageL3, object.Location2Voltage[2].s_Value);
            values.put(TableColumns.Location2AngleL1, object.Location2Angle[0].s_Value);
            values.put(TableColumns.Location2AngleL2, object.Location2Angle[1].s_Value);
            values.put(TableColumns.Location2AngleL3, object.Location2Angle[2].s_Value);
            values.put(TableColumns.AngleErrorL1, object.AngleError[0].s_Value);
            values.put(TableColumns.AngleErrorL2, object.AngleError[1].s_Value);
            values.put(TableColumns.AngleErrorL3, object.AngleError[2].s_Value);
            values.put(TableColumns.VoltageDifferentL1, object.VoltageDifferent[0].s_Value);
            values.put(TableColumns.VoltageDifferentL2, object.VoltageDifferent[1].s_Value);
            values.put(TableColumns.VoltageDifferentL3, object.VoltageDifferent[2].s_Value);
            values.put(TableColumns.RatioErrorL1, object.RatioError[0].s_Value);
            values.put(TableColumns.RatioErrorL2, object.RatioError[1].s_Value);
            values.put(TableColumns.RatioErrorL3, object.RatioError[2].s_Value);
            if (db.insert(TABLE_NAME, null, values) == -1) {
                z = false;
            }
        }
        return z;
    }

    /* access modifiers changed from: protected */
    @Override // com.clou.rs350.db.dao.BaseDao, com.clou.rs350.db.dao.BaseTestDao
    public Object parseData(Cursor cursor) {
        PTComparison value = new PTComparison();
        value.SerialNo = cursor.getString(cursor.getColumnIndex(this.m_TableKey));
        value.WiringValue = cursor.getString(cursor.getColumnIndex(SameColumns.WiringType));
        value.Wiring = cursor.getInt(cursor.getColumnIndex("IntWiringType"));
        value.ConnectionType = cursor.getString(cursor.getColumnIndex(SameColumns.ConnectionMode));
        value.Time = cursor.getString(cursor.getColumnIndex("TestTime"));
        value.Location1Voltage[0].s_Value = cursor.getString(cursor.getColumnIndex(TableColumns.Location1VoltageL1));
        value.Location1Voltage[1].s_Value = cursor.getString(cursor.getColumnIndex(TableColumns.Location1VoltageL2));
        value.Location1Voltage[2].s_Value = cursor.getString(cursor.getColumnIndex(TableColumns.Location1VoltageL3));
        value.Location1Angle[0].s_Value = cursor.getString(cursor.getColumnIndex(TableColumns.Location1AngleL1));
        value.Location1Angle[1].s_Value = cursor.getString(cursor.getColumnIndex(TableColumns.Location1AngleL2));
        value.Location1Angle[2].s_Value = cursor.getString(cursor.getColumnIndex(TableColumns.Location1AngleL3));
        value.Location2Voltage[0].s_Value = cursor.getString(cursor.getColumnIndex(TableColumns.Location2VoltageL1));
        value.Location2Voltage[1].s_Value = cursor.getString(cursor.getColumnIndex(TableColumns.Location2VoltageL2));
        value.Location2Voltage[2].s_Value = cursor.getString(cursor.getColumnIndex(TableColumns.Location2VoltageL3));
        value.Location2Angle[0].s_Value = cursor.getString(cursor.getColumnIndex(TableColumns.Location2AngleL1));
        value.Location2Angle[1].s_Value = cursor.getString(cursor.getColumnIndex(TableColumns.Location2AngleL2));
        value.Location2Angle[2].s_Value = cursor.getString(cursor.getColumnIndex(TableColumns.Location2AngleL3));
        value.AngleError[0].s_Value = cursor.getString(cursor.getColumnIndex(TableColumns.AngleErrorL1));
        value.AngleError[1].s_Value = cursor.getString(cursor.getColumnIndex(TableColumns.AngleErrorL2));
        value.AngleError[2].s_Value = cursor.getString(cursor.getColumnIndex(TableColumns.AngleErrorL3));
        value.VoltageDifferent[0].s_Value = cursor.getString(cursor.getColumnIndex(TableColumns.VoltageDifferentL1));
        value.VoltageDifferent[1].s_Value = cursor.getString(cursor.getColumnIndex(TableColumns.VoltageDifferentL2));
        value.VoltageDifferent[2].s_Value = cursor.getString(cursor.getColumnIndex(TableColumns.VoltageDifferentL3));
        value.RatioError[0].s_Value = cursor.getString(cursor.getColumnIndex(TableColumns.RatioErrorL1));
        value.RatioError[1].s_Value = cursor.getString(cursor.getColumnIndex(TableColumns.RatioErrorL2));
        value.RatioError[2].s_Value = cursor.getString(cursor.getColumnIndex(TableColumns.RatioErrorL3));
        return value;
    }
}
