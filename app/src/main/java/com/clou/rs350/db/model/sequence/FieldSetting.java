package com.clou.rs350.db.model.sequence;

import com.itextpdf.text.pdf.PdfObject;
import java.io.Serializable;
import java.util.List;
import java.util.Map;

public class FieldSetting implements Serializable {
    public String Element = PdfObject.NOTHING;
    public List<String> ItemKeys;
    public Map<String, FieldItem> Items;
    public int TitleId = 0;
}
