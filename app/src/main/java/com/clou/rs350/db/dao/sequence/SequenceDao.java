package com.clou.rs350.db.dao.sequence;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import com.clou.rs350.db.dao.sequence.BaseSchemeDao;
import com.clou.rs350.db.model.sequence.SequenceData;
import com.clou.rs350.db.model.sequence.SequenceItem;
import com.clou.rs350.manager.DatabaseManager;

public class SequenceDao extends BaseSchemeDao {
    private static final String TABLE_NAME = "Sequence";
    protected Context m_Context;

    private static final class TableColumns {
        public static final String ID = "intMyID";
        public static final String IsRead = "IsRead";
        public static final String ItemFunction = "ItemFunction";
        public static final String ItemIndex = "ItemIndex";
        public static final String ItemName = "ItemName";
        public static final String MustDo = "MustDo";
        public static final String SequenceName = "SequenceName";

        private TableColumns() {
        }
    }

    public SequenceDao(Context context) {
        super(context, TABLE_NAME, TableColumns.SequenceName);
        this.m_TableOrder = TableColumns.ItemIndex;
    }

    public void createTable(SQLiteDatabase db) {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("create table if not exists ").append(TABLE_NAME).append(" (");
        stringBuffer.append("intMyID").append(" integer primary key autoincrement,");
        stringBuffer.append(TableColumns.SequenceName).append(" VARCHAR(50),");
        stringBuffer.append(TableColumns.ItemName).append(" VARCHAR(50),");
        stringBuffer.append(TableColumns.ItemFunction).append(" VARCHAR(50),");
        stringBuffer.append(TableColumns.ItemIndex).append(" integer,");
        stringBuffer.append(TableColumns.MustDo).append(" integer default 0,");
        stringBuffer.append(SchemeTableColumns.Language).append(" integer,");
        stringBuffer.append("IsRead").append(" integer default 0);");
        db.execSQL(stringBuffer.toString());
    }

    public synchronized boolean insertObject(Object Data) {
        boolean z = false;
        synchronized (this) {
            SQLiteDatabase db = DatabaseManager.getWriteableDatabase(this.m_Context);
            ContentValues values = new ContentValues();
            long status = 0;
            SequenceData objects = (SequenceData) Data;
            for (int i = 0; i < objects.SequenceItems.size(); i++) {
                SequenceItem object = objects.SequenceItems.get(i);
                values.put(TableColumns.SequenceName, objects.SequenceName.toString());
                values.put(SchemeTableColumns.Language, Integer.valueOf(objects.Language));
                values.put(TableColumns.ItemName, object.ItemName);
                values.put(TableColumns.ItemFunction, object.ItemFunction);
                values.put(TableColumns.ItemIndex, Integer.valueOf(object.Index));
                values.put(TableColumns.MustDo, Integer.valueOf(object.MustDo));
                values.put("IsRead", (Integer) 0);
                status += db.insert(TABLE_NAME, null, values);
            }
            if (status != -1) {
                z = true;
            }
        }
        return z;
    }

    /* access modifiers changed from: protected */
    @Override // com.clou.rs350.db.dao.BaseDao
    public SequenceData parseData(Cursor cursor) {
        SequenceData values = new SequenceData();
        values.SequenceName = parseString(cursor.getString(cursor.getColumnIndex(TableColumns.SequenceName)));
        values.Language = cursor.getInt(cursor.getColumnIndex(SchemeTableColumns.Language));
        do {
            SequenceItem value = new SequenceItem();
            value.ItemName = parseString(cursor.getString(cursor.getColumnIndex(TableColumns.ItemName)));
            value.ItemFunction = parseString(cursor.getString(cursor.getColumnIndex(TableColumns.ItemFunction)));
            value.Index = cursor.getInt(cursor.getColumnIndex(TableColumns.ItemIndex));
            value.MustDo = cursor.getInt(cursor.getColumnIndex(TableColumns.MustDo));
            values.SequenceItems.add(value);
        } while (cursor.moveToNext());
        return values;
    }

    @Override // com.clou.rs350.db.dao.BaseDao, com.clou.rs350.db.dao.sequence.BaseSchemeDao
    public void upgradeTable(SQLiteDatabase db, int oldVersion, int newVersion) {
        if (oldVersion < 24) {
            db.execSQL("alter table Sequence add column MustDo integer default 0");
        }
        super.upgradeTable(db, oldVersion, newVersion);
    }
}
