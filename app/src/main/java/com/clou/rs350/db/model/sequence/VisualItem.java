package com.clou.rs350.db.model.sequence;

import com.clou.rs350.db.model.DBDataModel;
import com.itextpdf.text.pdf.PdfObject;
import java.io.Serializable;

public class VisualItem implements Serializable {
    public String Activities = PdfObject.NOTHING;
    public int Index;
    public int RequiredField = 0;
    public DBDataModel Result = new DBDataModel(-1L, PdfObject.NOTHING);
}
