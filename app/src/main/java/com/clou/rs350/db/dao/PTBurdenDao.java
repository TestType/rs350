package com.clou.rs350.db.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.clou.rs350.R;
import com.clou.rs350.db.model.PTBurden;
import com.clou.rs350.manager.DatabaseManager;
import com.clou.rs350.utils.OtherUtils;

public class PTBurdenDao extends BaseTestDao {
    private static final String TABLE_NAME = "TestPTBurden";

    private static final class TableColumns {
        public static final String BurdenPersentsL1 = "BurdenPersentsL1";
        public static final String BurdenPersentsL2 = "BurdenPersentsL2";
        public static final String BurdenPersentsL3 = "BurdenPersentsL3";
        public static final String BurdenVAL1 = "BurdenVAL1";
        public static final String BurdenVAL2 = "BurdenVAL2";
        public static final String BurdenVAL3 = "BurdenVAL3";
        public static final String BurdenWL1 = "BurdenWL1";
        public static final String BurdenWL2 = "BurdenWL2";
        public static final String BurdenWL3 = "BurdenWL3";
        public static final String CurrentValueL1 = "CurrentValueL1";
        public static final String CurrentValueL2 = "CurrentValueL2";
        public static final String CurrentValueL3 = "CurrentValueL3";
        public static final String ID = "intMyID";
        public static final String NormalRatio = "NormalRatio";
        public static final String PTVA = "PTVA";
        public static final String ResultL1 = "ResultL1";
        public static final String ResultL2 = "ResultL2";
        public static final String ResultL3 = "ResultL3";
        public static final String VoltageCurrentAngleL1 = "VoltageCurrentAngleL1";
        public static final String VoltageCurrentAngleL2 = "VoltageCurrentAngleL2";
        public static final String VoltageCurrentAngleL3 = "VoltageCurrentAngleL3";
        public static final String VoltageValueL1 = "VoltageValueL1";
        public static final String VoltageValueL2 = "VoltageValueL2";
        public static final String VoltageValueL3 = "VoltageValueL3";
        public static final String VoltageValueNormalSecondary = "VoltageValueNormalSecondary";

        private TableColumns() {
        }
    }

    public PTBurdenDao(Context context) {
        super(context, TABLE_NAME);
    }

    public void createTable(SQLiteDatabase db) {
        new StringBuffer();
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("create table if not exists ").append(TABLE_NAME).append(" (");
        stringBuffer.append("intMyID").append(" integer primary key autoincrement,");
        stringBuffer.append(this.m_TableKey).append(" VARCHAR(50),");
        stringBuffer.append("CreateTime").append(" VARCHAR(50),");
        stringBuffer.append(SameColumns.WiringType).append(" VARCHAR(50),");
        stringBuffer.append("IntWiringType").append(" integer default 0,");
        stringBuffer.append(SameColumns.ConnectionMode).append(" VARCHAR(50),");
        stringBuffer.append(SameColumns.VoltageRangeL1).append(" VARCHAR(50),");
        stringBuffer.append(SameColumns.VoltageRangeL2).append(" VARCHAR(50),");
        stringBuffer.append(SameColumns.VoltageRangeL3).append(" VARCHAR(50),");
        stringBuffer.append(SameColumns.CurrentRangeL1).append(" VARCHAR(50),");
        stringBuffer.append(SameColumns.CurrentRangeL2).append(" VARCHAR(50),");
        stringBuffer.append(SameColumns.CurrentRangeL3).append(" VARCHAR(50),");
        stringBuffer.append("VoltageValueL1").append(" VARCHAR(20),");
        stringBuffer.append("VoltageValueL2").append(" VARCHAR(20),");
        stringBuffer.append("VoltageValueL3").append(" VARCHAR(20),");
        stringBuffer.append("CurrentValueL1").append(" VARCHAR(20),");
        stringBuffer.append("CurrentValueL2").append(" VARCHAR(20),");
        stringBuffer.append("CurrentValueL3").append(" VARCHAR(20),");
        stringBuffer.append("VoltageCurrentAngleL1").append(" VARCHAR(20),");
        stringBuffer.append("VoltageCurrentAngleL2").append(" VARCHAR(20),");
        stringBuffer.append("VoltageCurrentAngleL3").append(" VARCHAR(20),");
        stringBuffer.append(TableColumns.BurdenWL1).append(" VARCHAR(20),");
        stringBuffer.append(TableColumns.BurdenWL2).append(" VARCHAR(20),");
        stringBuffer.append(TableColumns.BurdenWL3).append(" VARCHAR(20),");
        stringBuffer.append(TableColumns.BurdenVAL1).append(" VARCHAR(20),");
        stringBuffer.append(TableColumns.BurdenVAL2).append(" VARCHAR(20),");
        stringBuffer.append(TableColumns.BurdenVAL3).append(" VARCHAR(20),");
        stringBuffer.append(TableColumns.BurdenPersentsL1).append(" VARCHAR(20),");
        stringBuffer.append(TableColumns.BurdenPersentsL2).append(" VARCHAR(20),");
        stringBuffer.append(TableColumns.BurdenPersentsL3).append(" VARCHAR(20),");
        stringBuffer.append("ResultL1").append(" VARCHAR(20),");
        stringBuffer.append("ResultL2").append(" VARCHAR(20),");
        stringBuffer.append("ResultL3").append(" VARCHAR(20),");
        stringBuffer.append(TableColumns.PTVA).append(" VARCHAR(20),");
        stringBuffer.append(TableColumns.VoltageValueNormalSecondary).append(" VARCHAR(20),");
        stringBuffer.append(TableColumns.NormalRatio).append(" VARCHAR(20));");
        db.execSQL(stringBuffer.toString());
    }

    @Override // com.clou.rs350.db.dao.BaseDao, com.clou.rs350.db.dao.BaseTestDao
    public void upgradeTable(SQLiteDatabase db, int oldVersion, int newVersion) {
        if (oldVersion <= 3) {
            db.execSQL("alter table PTBurden rename to TestPTBurden");
        }
        super.upgradeTable(db, oldVersion, newVersion);
    }

    @Override // com.clou.rs350.db.dao.BaseDao, com.clou.rs350.db.dao.BaseTestDao
    public synchronized boolean insertObject(Object Data, String Time) {
        boolean z = true;
        synchronized (this) {
            SQLiteDatabase db = DatabaseManager.getWriteableDatabase(this.m_Context);
            ContentValues values = new ContentValues();
            PTBurden object = (PTBurden) Data;
            values.put(this.m_TableKey, object.SerialNo);
            values.put("CreateTime", Time);
            values.put(SameColumns.WiringType, object.WiringValue);
            values.put("IntWiringType", Integer.valueOf(object.Wiring));
            values.put(SameColumns.ConnectionMode, object.ConnectionType);
            values.put(SameColumns.VoltageRangeL1, object.VoltageRangeL1.toString());
            values.put(SameColumns.VoltageRangeL2, object.VoltageRangeL2.toString());
            values.put(SameColumns.VoltageRangeL3, object.VoltageRangeL3.toString());
            values.put(SameColumns.CurrentRangeL1, object.CurrentRangeL1.toString());
            values.put(SameColumns.CurrentRangeL2, object.CurrentRangeL2.toString());
            values.put(SameColumns.CurrentRangeL3, object.CurrentRangeL3.toString());
            values.put("VoltageValueL1", object.VoltageValue[0].s_Value);
            values.put("VoltageValueL2", object.VoltageValue[1].s_Value);
            values.put("VoltageValueL3", object.VoltageValue[2].s_Value);
            values.put("CurrentValueL1", object.CurrentValue[0].s_Value);
            values.put("CurrentValueL2", object.CurrentValue[1].s_Value);
            values.put("CurrentValueL3", object.CurrentValue[2].s_Value);
            values.put("VoltageCurrentAngleL1", object.VoltageCurrentAngle[0].s_Value);
            values.put("VoltageCurrentAngleL2", object.VoltageCurrentAngle[1].s_Value);
            values.put("VoltageCurrentAngleL3", object.VoltageCurrentAngle[2].s_Value);
            values.put(TableColumns.BurdenWL1, object.BurdenW[0].s_Value);
            values.put(TableColumns.BurdenWL2, object.BurdenW[1].s_Value);
            values.put(TableColumns.BurdenWL3, object.BurdenW[2].s_Value);
            values.put(TableColumns.BurdenVAL1, object.BurdenVA[0].s_Value);
            values.put(TableColumns.BurdenVAL2, object.BurdenVA[1].s_Value);
            values.put(TableColumns.BurdenVAL3, object.BurdenVA[2].s_Value);
            values.put(TableColumns.BurdenPersentsL1, object.BurdenPersents[0].s_Value);
            values.put(TableColumns.BurdenPersentsL2, object.BurdenPersents[1].s_Value);
            values.put(TableColumns.BurdenPersentsL3, object.BurdenPersents[2].s_Value);
            values.put("ResultL1", object.Result[0].s_Value);
            values.put("ResultL2", object.Result[1].s_Value);
            values.put("ResultL3", object.Result[2].s_Value);
            values.put(TableColumns.PTVA, object.PTVA.s_Value);
            values.put(TableColumns.VoltageValueNormalSecondary, object.VoltageValueNormalSecondary.s_Value);
            values.put(TableColumns.NormalRatio, object.NormalRatio.s_Value);
            if (db.insert(TABLE_NAME, null, values) == -1) {
                z = false;
            }
        }
        return z;
    }

    /* access modifiers changed from: protected */
    @Override // com.clou.rs350.db.dao.BaseDao, com.clou.rs350.db.dao.BaseTestDao
    public Object parseData(Cursor cursor) {
        PTBurden value = new PTBurden();
        value.SerialNo = cursor.getString(cursor.getColumnIndex(this.m_TableKey));
        value.WiringValue = cursor.getString(cursor.getColumnIndex(SameColumns.WiringType));
        value.Wiring = cursor.getInt(cursor.getColumnIndex("IntWiringType"));
        value.ConnectionType = cursor.getString(cursor.getColumnIndex(SameColumns.ConnectionMode));
        value.VoltageRangeL1 = cursor.getString(cursor.getColumnIndex(SameColumns.VoltageRangeL1));
        value.VoltageRangeL2 = cursor.getString(cursor.getColumnIndex(SameColumns.VoltageRangeL2));
        value.VoltageRangeL3 = cursor.getString(cursor.getColumnIndex(SameColumns.VoltageRangeL3));
        value.CurrentRangeL1 = cursor.getString(cursor.getColumnIndex(SameColumns.CurrentRangeL1));
        value.CurrentRangeL2 = cursor.getString(cursor.getColumnIndex(SameColumns.CurrentRangeL2));
        value.CurrentRangeL3 = cursor.getString(cursor.getColumnIndex(SameColumns.CurrentRangeL3));
        value.VoltageValue[0].s_Value = cursor.getString(cursor.getColumnIndex("VoltageValueL1"));
        value.VoltageValue[1].s_Value = cursor.getString(cursor.getColumnIndex("VoltageValueL2"));
        value.VoltageValue[2].s_Value = cursor.getString(cursor.getColumnIndex("VoltageValueL3"));
        value.CurrentValue[0].s_Value = cursor.getString(cursor.getColumnIndex("CurrentValueL1"));
        value.CurrentValue[1].s_Value = cursor.getString(cursor.getColumnIndex("CurrentValueL2"));
        value.CurrentValue[2].s_Value = cursor.getString(cursor.getColumnIndex("CurrentValueL3"));
        value.VoltageCurrentAngle[0].s_Value = cursor.getString(cursor.getColumnIndex("VoltageCurrentAngleL1"));
        value.VoltageCurrentAngle[1].s_Value = cursor.getString(cursor.getColumnIndex("VoltageCurrentAngleL2"));
        value.VoltageCurrentAngle[2].s_Value = cursor.getString(cursor.getColumnIndex("VoltageCurrentAngleL3"));
        value.BurdenW[0].s_Value = cursor.getString(cursor.getColumnIndex(TableColumns.BurdenWL1));
        value.BurdenW[1].s_Value = cursor.getString(cursor.getColumnIndex(TableColumns.BurdenWL2));
        value.BurdenW[2].s_Value = cursor.getString(cursor.getColumnIndex(TableColumns.BurdenWL3));
        value.BurdenVA[0].s_Value = cursor.getString(cursor.getColumnIndex(TableColumns.BurdenVAL1));
        value.BurdenVA[1].s_Value = cursor.getString(cursor.getColumnIndex(TableColumns.BurdenVAL2));
        value.BurdenVA[2].s_Value = cursor.getString(cursor.getColumnIndex(TableColumns.BurdenVAL3));
        value.BurdenPersents[0].s_Value = cursor.getString(cursor.getColumnIndex(TableColumns.BurdenPersentsL1));
        value.BurdenPersents[1].s_Value = cursor.getString(cursor.getColumnIndex(TableColumns.BurdenPersentsL2));
        value.BurdenPersents[2].s_Value = cursor.getString(cursor.getColumnIndex(TableColumns.BurdenPersentsL3));
        value.Result[0] = OtherUtils.getResultValue(cursor.getString(cursor.getColumnIndex("ResultL1")), R.array.limit, this.m_Context);
        value.Result[1] = OtherUtils.getResultValue(cursor.getString(cursor.getColumnIndex("ResultL2")), R.array.limit, this.m_Context);
        value.Result[2] = OtherUtils.getResultValue(cursor.getString(cursor.getColumnIndex("ResultL3")), R.array.limit, this.m_Context);
        value.PTVA.s_Value = cursor.getString(cursor.getColumnIndex(TableColumns.PTVA));
        value.VoltageValueNormalSecondary.s_Value = cursor.getString(cursor.getColumnIndex(TableColumns.VoltageValueNormalSecondary));
        value.NormalRatio.s_Value = cursor.getString(cursor.getColumnIndex(TableColumns.NormalRatio));
        return value;
    }
}
