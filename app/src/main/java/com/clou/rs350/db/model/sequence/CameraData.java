package com.clou.rs350.db.model.sequence;

import com.itextpdf.text.pdf.PdfObject;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class CameraData implements Serializable {
    public String CreatTime = PdfObject.NOTHING;
    public int Index = 0;
    public List<CameraItem> Photos = new ArrayList();
    public String SerialNo = PdfObject.NOTHING;

    public CameraData() {
    }

    public CameraData(int index) {
        this.Index = index;
    }
}
