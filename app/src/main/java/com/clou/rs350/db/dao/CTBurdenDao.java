package com.clou.rs350.db.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.clou.rs350.R;
import com.clou.rs350.db.model.CTBurden;
import com.clou.rs350.manager.DatabaseManager;
import com.clou.rs350.utils.OtherUtils;

public class CTBurdenDao extends BaseTestDao {
    private static final String TABLE_NAME = "TestCTBurden";

    private static final class TableColumns {
        public static final String BurdenL1 = "BurdenL1";
        public static final String BurdenL2 = "BurdenL2";
        public static final String BurdenL3 = "BurdenL3";
        public static final String BurdenPersentL1 = "BurdenPersentL1";
        public static final String BurdenPersentL2 = "BurdenPersentL2";
        public static final String BurdenPersentL3 = "BurdenPersentL3";
        public static final String CTTerminal = "CTTerminal";
        public static final String CTVA = "CTVA";
        public static final String CurrentRangeSecL1 = "CurrentRangeSecL1";
        public static final String CurrentRangeSecL2 = "CurrentRangeSecL2";
        public static final String CurrentRangeSecL3 = "CurrentRangeSecL3";
        public static final String CurrentValueSecondaryL1 = "CurrentValueSecondaryL1";
        public static final String CurrentValueSecondaryL2 = "CurrentValueSecondaryL2";
        public static final String CurrentValueSecondaryL3 = "CurrentValueSecondaryL3";
        public static final String ID = "intMyID";
        public static final String LengthOfWire = "LengthOfWire";
        public static final String ResultL1 = "ResultL1";
        public static final String ResultL2 = "ResultL2";
        public static final String ResultL3 = "ResultL3";
        public static final String SizeOfWire = "SizeOfWire";
        public static final String VoltageRangeSecL1 = "VoltageRangeSecL1";
        public static final String VoltageRangeSecL2 = "VoltageRangeSecL2";
        public static final String VoltageRangeSecL3 = "VoltageRangeSecL3";
        public static final String VoltageValueSecondaryL1 = "VoltageValueSecondaryL1";
        public static final String VoltageValueSecondaryL2 = "VoltageValueSecondaryL2";
        public static final String VoltageValueSecondaryL3 = "VoltageValueSecondaryL3";

        private TableColumns() {
        }
    }

    public CTBurdenDao(Context context) {
        super(context, TABLE_NAME);
    }

    public void createTable(SQLiteDatabase db) {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("create table if not exists ").append(TABLE_NAME).append(" (");
        stringBuffer.append("intMyID").append(" integer primary key autoincrement,");
        stringBuffer.append(this.m_TableKey).append(" VARCHAR(50),");
        stringBuffer.append("CreateTime").append(" VARCHAR(50),");
        stringBuffer.append("IntWiringType").append(" integer default 0,");
        stringBuffer.append(SameColumns.WiringType).append(" VARCHAR(50),");
        stringBuffer.append(SameColumns.ConnectionMode).append(" VARCHAR(50),");
        stringBuffer.append(TableColumns.VoltageRangeSecL1).append(" VARCHAR(50),");
        stringBuffer.append("CurrentRangeSecL1").append(" VARCHAR(50),");
        stringBuffer.append(TableColumns.VoltageRangeSecL2).append(" VARCHAR(50),");
        stringBuffer.append("CurrentRangeSecL2").append(" VARCHAR(50),");
        stringBuffer.append(TableColumns.VoltageRangeSecL3).append(" VARCHAR(50),");
        stringBuffer.append("CurrentRangeSecL3").append(" VARCHAR(50),");
        stringBuffer.append(TableColumns.VoltageValueSecondaryL1).append(" VARCHAR(20),");
        stringBuffer.append(TableColumns.VoltageValueSecondaryL2).append(" VARCHAR(20),");
        stringBuffer.append(TableColumns.VoltageValueSecondaryL3).append(" VARCHAR(20),");
        stringBuffer.append("CurrentValueSecondaryL1").append(" VARCHAR(20),");
        stringBuffer.append("CurrentValueSecondaryL2").append(" VARCHAR(20),");
        stringBuffer.append("CurrentValueSecondaryL3").append(" VARCHAR(20),");
        stringBuffer.append(TableColumns.BurdenL1).append(" VARCHAR(20),");
        stringBuffer.append(TableColumns.BurdenL2).append(" VARCHAR(20),");
        stringBuffer.append(TableColumns.BurdenL3).append(" VARCHAR(20),");
        stringBuffer.append(TableColumns.BurdenPersentL1).append(" VARCHAR(20),");
        stringBuffer.append(TableColumns.BurdenPersentL2).append(" VARCHAR(20),");
        stringBuffer.append(TableColumns.BurdenPersentL3).append(" VARCHAR(20),");
        stringBuffer.append("ResultL1").append(" VARCHAR(30),");
        stringBuffer.append("ResultL2").append(" VARCHAR(30),");
        stringBuffer.append("ResultL3").append(" VARCHAR(30),");
        stringBuffer.append(TableColumns.CTVA).append(" VARCHAR(50),");
        stringBuffer.append(TableColumns.CTTerminal).append(" integer default 0,");
        stringBuffer.append(TableColumns.LengthOfWire).append(" VARCHAR(50),");
        stringBuffer.append(TableColumns.SizeOfWire).append(" VARCHAR(50));");
        db.execSQL(stringBuffer.toString());
    }

    @Override // com.clou.rs350.db.dao.BaseDao, com.clou.rs350.db.dao.BaseTestDao
    public void upgradeTable(SQLiteDatabase db, int oldVersion, int newVersion) {
        if (oldVersion <= 3) {
            db.execSQL("alter table CTBurden rename to TestCTBurden");
        }
        super.upgradeTable(db, oldVersion, newVersion);
    }

    @Override // com.clou.rs350.db.dao.BaseDao, com.clou.rs350.db.dao.BaseTestDao
    public synchronized boolean insertObject(Object Data, String Time) {
        boolean z = true;
        synchronized (this) {
            SQLiteDatabase db = DatabaseManager.getWriteableDatabase(this.m_Context);
            ContentValues values = new ContentValues();
            CTBurden object = (CTBurden) Data;
            values.put(this.m_TableKey, object.SerialNo);
            values.put("CreateTime", Time);
            values.put(SameColumns.WiringType, object.WiringValue);
            values.put("IntWiringType", Integer.valueOf(object.Wiring));
            values.put(SameColumns.ConnectionMode, object.ConnectionType);
            values.put(TableColumns.VoltageRangeSecL1, object.VoltageRangeSec[0]);
            values.put("CurrentRangeSecL1", object.CurrentRangeSec[0]);
            values.put(TableColumns.VoltageRangeSecL2, object.VoltageRangeSec[1]);
            values.put("CurrentRangeSecL2", object.CurrentRangeSec[1]);
            values.put(TableColumns.VoltageRangeSecL3, object.VoltageRangeSec[2]);
            values.put("CurrentRangeSecL3", object.CurrentRangeSec[2]);
            values.put(TableColumns.VoltageValueSecondaryL1, object.VoltageValueSecondary[0].s_Value);
            values.put(TableColumns.VoltageValueSecondaryL2, object.VoltageValueSecondary[1].s_Value);
            values.put(TableColumns.VoltageValueSecondaryL3, object.VoltageValueSecondary[2].s_Value);
            values.put("CurrentValueSecondaryL1", object.CurrentValueSecondary[0].s_Value);
            values.put("CurrentValueSecondaryL2", object.CurrentValueSecondary[1].s_Value);
            values.put("CurrentValueSecondaryL3", object.CurrentValueSecondary[2].s_Value);
            values.put(TableColumns.BurdenL1, object.Burden[0].s_Value);
            values.put(TableColumns.BurdenL2, object.Burden[1].s_Value);
            values.put(TableColumns.BurdenL3, object.Burden[2].s_Value);
            values.put(TableColumns.BurdenPersentL1, object.BurdenPersents[0].s_Value);
            values.put(TableColumns.BurdenPersentL2, object.BurdenPersents[1].s_Value);
            values.put(TableColumns.BurdenPersentL3, object.BurdenPersents[2].s_Value);
            values.put("ResultL1", object.Result[0].s_Value);
            values.put("ResultL2", object.Result[1].s_Value);
            values.put("ResultL3", object.Result[2].s_Value);
            values.put(TableColumns.CTVA, object.CTVA.s_Value);
            values.put(TableColumns.CTTerminal, Integer.valueOf(object.CTTerminal));
            values.put(TableColumns.LengthOfWire, object.LengthOfWire.s_Value);
            values.put(TableColumns.SizeOfWire, object.SizeOfWire.s_Value);
            if (db.insert(TABLE_NAME, null, values) == -1) {
                z = false;
            }
        }
        return z;
    }

    /* access modifiers changed from: protected */
    @Override // com.clou.rs350.db.dao.BaseDao, com.clou.rs350.db.dao.BaseTestDao
    public Object parseData(Cursor cursor) {
        CTBurden value = new CTBurden();
        value.SerialNo = cursor.getString(cursor.getColumnIndex(this.m_TableKey));
        value.WiringValue = cursor.getString(cursor.getColumnIndex(SameColumns.WiringType));
        value.Wiring = cursor.getInt(cursor.getColumnIndex("IntWiringType"));
        value.ConnectionType = cursor.getString(cursor.getColumnIndex(SameColumns.ConnectionMode));
        value.VoltageRangeSec[0] = cursor.getString(cursor.getColumnIndex(TableColumns.VoltageRangeSecL1));
        value.CurrentRangeSec[0] = cursor.getString(cursor.getColumnIndex("CurrentRangeSecL1"));
        value.VoltageRangeSec[1] = cursor.getString(cursor.getColumnIndex(TableColumns.VoltageRangeSecL2));
        value.CurrentRangeSec[1] = cursor.getString(cursor.getColumnIndex("CurrentRangeSecL2"));
        value.VoltageRangeSec[2] = cursor.getString(cursor.getColumnIndex(TableColumns.VoltageRangeSecL3));
        value.CurrentRangeSec[2] = cursor.getString(cursor.getColumnIndex("CurrentRangeSecL3"));
        value.VoltageValueSecondary[0].s_Value = cursor.getString(cursor.getColumnIndex(TableColumns.VoltageValueSecondaryL1));
        value.VoltageValueSecondary[1].s_Value = cursor.getString(cursor.getColumnIndex(TableColumns.VoltageValueSecondaryL2));
        value.VoltageValueSecondary[2].s_Value = cursor.getString(cursor.getColumnIndex(TableColumns.VoltageValueSecondaryL3));
        value.CurrentValueSecondary[0].s_Value = cursor.getString(cursor.getColumnIndex("CurrentValueSecondaryL1"));
        value.CurrentValueSecondary[1].s_Value = cursor.getString(cursor.getColumnIndex("CurrentValueSecondaryL2"));
        value.CurrentValueSecondary[2].s_Value = cursor.getString(cursor.getColumnIndex("CurrentValueSecondaryL3"));
        value.Burden[0].s_Value = cursor.getString(cursor.getColumnIndex(TableColumns.BurdenL1));
        value.Burden[1].s_Value = cursor.getString(cursor.getColumnIndex(TableColumns.BurdenL2));
        value.Burden[2].s_Value = cursor.getString(cursor.getColumnIndex(TableColumns.BurdenL3));
        value.BurdenPersents[0].s_Value = cursor.getString(cursor.getColumnIndex(TableColumns.BurdenPersentL1));
        value.BurdenPersents[1].s_Value = cursor.getString(cursor.getColumnIndex(TableColumns.BurdenPersentL2));
        value.BurdenPersents[2].s_Value = cursor.getString(cursor.getColumnIndex(TableColumns.BurdenPersentL3));
        value.Result[0] = OtherUtils.getResultValue(cursor.getString(cursor.getColumnIndex("ResultL1")), R.array.limit, this.m_Context);
        value.Result[1] = OtherUtils.getResultValue(cursor.getString(cursor.getColumnIndex("ResultL2")), R.array.limit, this.m_Context);
        value.Result[2] = OtherUtils.getResultValue(cursor.getString(cursor.getColumnIndex("ResultL3")), R.array.limit, this.m_Context);
        value.CTVA.s_Value = cursor.getString(cursor.getColumnIndex(TableColumns.CTVA));
        value.CTTerminal = cursor.getInt(cursor.getColumnIndex(TableColumns.CTTerminal));
        value.LengthOfWire.s_Value = cursor.getString(cursor.getColumnIndex(TableColumns.LengthOfWire));
        value.SizeOfWire.s_Value = cursor.getString(cursor.getColumnIndex(TableColumns.SizeOfWire));
        return value;
    }
}
