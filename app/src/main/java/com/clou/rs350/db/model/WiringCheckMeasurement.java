package com.clou.rs350.db.model;

import android.content.res.Resources;

import com.clou.rs350.CLApplication;
import com.clou.rs350.Preferences;
import com.clou.rs350.R;
import com.clou.rs350.device.MeterBaseDevice;
import com.clou.rs350.utils.AngleUtil;
import com.clou.rs350.utils.OtherUtils;
import com.itextpdf.text.pdf.PdfObject;

public class WiringCheckMeasurement extends BasicMeasurement {
    public int AngleDefinition;
    public DBDataModel CorrectionFactor = new DBDataModel();
    public DBDataModel[] Limits = {new DBDataModel(-1L, "---"), new DBDataModel(-1L, "---"), new DBDataModel(-1L, "---"), new DBDataModel(-1L, "---"), new DBDataModel(-1L, "---"), new DBDataModel(-1L, "---"), new DBDataModel(-1L, "---"), new DBDataModel(-1L, "---"), new DBDataModel(-1L, "---")};
    public String Remark = PdfObject.NOTHING;
    public DBDataModel[] SiteCurrentBalance = {new DBDataModel(-1L, "---"), new DBDataModel(-1L, "---"), new DBDataModel(-1L, "---")};
    public DBDataModel[] SiteCurrentDirection = {new DBDataModel(-1L, "---"), new DBDataModel(-1L, "---"), new DBDataModel(-1L, "---")};
    public DBDataModel[] SiteCurrentSymmetry = {new DBDataModel(-1L, "---"), new DBDataModel(-1L, "---"), new DBDataModel(-1L, "---")};
    public DBDataModel[] SiteHarmonic = {new DBDataModel(-1L, "---"), new DBDataModel(-1L, "---"), new DBDataModel(-1L, "---"), new DBDataModel(-1L, "---"), new DBDataModel(-1L, "---"), new DBDataModel(-1L, "---")};
    public DBDataModel[] SiteState = {new DBDataModel(-1L, "---"), new DBDataModel(-1L, "---"), new DBDataModel(-1L, "---"), new DBDataModel(-1L, "---"), new DBDataModel(-1L, "---"), new DBDataModel(-1L, "---")};
    public DBDataModel[] SiteVoltageBalance = {new DBDataModel(-1L, "---"), new DBDataModel(-1L, "---"), new DBDataModel(-1L, "---")};
    public DBDataModel[] SiteVoltageSymmetry = {new DBDataModel(-1L, "---"), new DBDataModel(-1L, "---"), new DBDataModel(-1L, "---")};
    public int VectorDefinition;
    public int WiringCheckCos;
    public int intRemarkTag = 0;
    private int[] phaseSequence = new int[6];
    public int powerFactorValue = 0;

    public void setLimits() {
        float limit = Preferences.getFloat(Preferences.Key.VOLTAGEUNBALANCE, 0.2f);
        this.Limits[0] = new DBDataModel((double) limit, String.valueOf(limit) + "%");
        float limit2 = Preferences.getFloat(Preferences.Key.VOLTAGESYMMETRY, 3.0f);
        this.Limits[1] = new DBDataModel((double) limit2, String.valueOf(limit2) + "°");
        float limit3 = Preferences.getFloat(Preferences.Key.CURRENTUNBALANCE, 0.5f);
        this.Limits[2] = new DBDataModel((double) limit3, String.valueOf(limit3) + "%");
        float limit4 = Preferences.getFloat(Preferences.Key.CURRENTSYMMETRY, 3.0f);
        this.Limits[3] = new DBDataModel((double) limit4, String.valueOf(limit4) + "°");
        float limit5 = Preferences.getFloat(Preferences.Key.VOLTAGEHARMONICLIMIT, 0.5f);
        this.Limits[4] = new DBDataModel((double) limit5, String.valueOf(limit5) + "%");
        float limit6 = Preferences.getFloat(Preferences.Key.CURRENTHARMONICLIMIT, 0.5f);
        this.Limits[5] = new DBDataModel((double) limit6, String.valueOf(limit6) + "%");
        float limit7 = Preferences.getFloat(Preferences.Key.VOLTAGENOMINAL, 230.0f);
        this.Limits[6] = new DBDataModel((double) limit7, String.valueOf(limit7) + "V");
        float limit8 = Preferences.getFloat(Preferences.Key.VOLTAGEUPPERLIMIT, 5.0f);
        this.Limits[7] = new DBDataModel((double) limit8, String.valueOf(limit8) + "%");
        float limit9 = Preferences.getFloat(Preferences.Key.VOLTAGELOWERLIMIT, 5.0f);
        this.Limits[8] = new DBDataModel((double) limit9, String.valueOf(limit9) + "%");
    }

    public void parseData(MeterBaseDevice Device, int AngleDefinition2) {
        super.parseData(Device);
        this.AngleDefinition = AngleDefinition2;
        int[] tmpSequence = Device.getVoltagePhaseSequenceValue();
        this.phaseSequence[0] = tmpSequence[0];
        this.phaseSequence[1] = tmpSequence[1];
        this.phaseSequence[2] = tmpSequence[2];
        int[] tmpSequence2 = Device.getCurrentPhaseSequenceValue();
        this.phaseSequence[3] = tmpSequence2[0];
        this.phaseSequence[4] = tmpSequence2[1];
        this.phaseSequence[5] = tmpSequence2[2];
        this.CorrectionFactor = parse(Device.getCorrectionFactor());
        this.WiringCheckCos = Device.getWiringCheckCos();
        if (this.WiringCheckCos < 0 || 3 < this.WiringCheckCos) {
            this.WiringCheckCos = 0;
        }
        int IntRemark = Device.getWiringRemrakValue();
        this.intRemarkTag = IntRemark;
        getResult(Device.getWiringValue(), this.phaseSequence, IntRemark);
        calcSiteAnalysis(this.phaseSequence, AngleDefinition2);
    }

    private int getVoltageState(double voltage) {
        int result = 0;
        if (this.Limits[6].d_Value * ((this.Limits[7].d_Value / 100.0d) + 1.0d) < voltage) {
            result = 1;
        }
        if (this.Limits[6].d_Value * (1.0d - (this.Limits[8].d_Value / 100.0d)) > voltage) {
            return 2;
        }
        return result;
    }

    private void calcSiteAnalysis(int[] Sequence, int AngleDefinition2) {
        Resources res = CLApplication.app.getResources();
        String[] tmpString = res.getStringArray(R.array.sitestate);
        for (int i = 3; i < 6; i++) {
            int tmpInt = Sequence[i] == 0 ? 1 : 0;
            this.SiteState[i] = new DBDataModel((long) tmpInt, tmpString[tmpInt]);
        }
        String[] tmpString2 = res.getStringArray(R.array.voltagestate);
        int tmpInt2 = getVoltageState(this.VoltageValueL1Show.d_Value);
        this.SiteState[0] = new DBDataModel((long) tmpInt2, tmpString2[tmpInt2]);
        int tmpInt3 = getVoltageState(this.VoltageValueL2Show.d_Value);
        this.SiteState[1] = new DBDataModel((long) tmpInt3, tmpString2[tmpInt3]);
        int tmpInt4 = getVoltageState(this.VoltageValueL3Show.d_Value);
        this.SiteState[2] = new DBDataModel((long) tmpInt4, tmpString2[tmpInt4]);
        double tmpVoltageAVG = ((this.VoltageValueL1.d_Value + this.VoltageValueL2.d_Value) + this.VoltageValueL3.d_Value) / 3.0d;
        double tmpCurrentAVG = ((this.CurrentValueL1.d_Value + this.CurrentValueL2.d_Value) + this.CurrentValueL3.d_Value) / 3.0d;
        String[] tmpString3 = res.getStringArray(R.array.sitedirection);
        int tmpInt5 = (this.VoltageCurrentAngleL1.d_Value <= 90.0d || this.VoltageCurrentAngleL1.d_Value >= 270.0d) ? 0 : 1;
        this.SiteCurrentDirection[0] = new DBDataModel((long) tmpInt5, tmpString3[tmpInt5]);
        int tmpInt6 = (this.VoltageCurrentAngleL2.d_Value <= 90.0d || this.VoltageCurrentAngleL2.d_Value >= 270.0d) ? 0 : 1;
        this.SiteCurrentDirection[1] = new DBDataModel((long) tmpInt6, tmpString3[tmpInt6]);
        int tmpInt7 = (this.VoltageCurrentAngleL3.d_Value <= 90.0d || this.VoltageCurrentAngleL3.d_Value >= 270.0d) ? 0 : 1;
        this.SiteCurrentDirection[2] = new DBDataModel((long) tmpInt7, tmpString3[tmpInt7]);
        String[] tmpString4 = res.getStringArray(R.array.sitebalance);
        int tmpInt8 = Math.abs((this.VoltageValueL1.d_Value - tmpVoltageAVG) / tmpVoltageAVG) <= this.Limits[0].d_Value / 100.0d ? 0 : 1;
        this.SiteVoltageBalance[0] = new DBDataModel((long) tmpInt8, tmpString4[tmpInt8]);
        int tmpInt9 = Math.abs((this.VoltageValueL2.d_Value - tmpVoltageAVG) / tmpVoltageAVG) <= this.Limits[0].d_Value / 100.0d ? 0 : 1;
        this.SiteVoltageBalance[1] = new DBDataModel((long) tmpInt9, tmpString4[tmpInt9]);
        int tmpInt10 = Math.abs((this.VoltageValueL3.d_Value - tmpVoltageAVG) / tmpVoltageAVG) <= this.Limits[0].d_Value / 100.0d ? 0 : 1;
        this.SiteVoltageBalance[2] = new DBDataModel((long) tmpInt10, tmpString4[tmpInt10]);
        int tmpInt11 = Math.abs((this.CurrentValueL1.d_Value - tmpCurrentAVG) / tmpCurrentAVG) <= this.Limits[2].d_Value / 100.0d ? 0 : 1;
        this.SiteCurrentBalance[0] = new DBDataModel((long) tmpInt11, tmpString4[tmpInt11]);
        int tmpInt12 = Math.abs((this.CurrentValueL2.d_Value - tmpCurrentAVG) / tmpCurrentAVG) <= this.Limits[2].d_Value / 100.0d ? 0 : 1;
        this.SiteCurrentBalance[1] = new DBDataModel((long) tmpInt12, tmpString4[tmpInt12]);
        int tmpInt13 = Math.abs((this.CurrentValueL3.d_Value - tmpCurrentAVG) / tmpCurrentAVG) <= this.Limits[2].d_Value / 100.0d ? 0 : 1;
        this.SiteCurrentBalance[2] = new DBDataModel((long) tmpInt13, tmpString4[tmpInt13]);
        String[] tmpString5 = res.getStringArray(R.array.limit);
        float[] tmpSymmetry = new float[6];
        if (AngleDefinition2 == 0) {
            tmpSymmetry[0] = (float) Math.abs(AngleUtil.adjustAngle(this.VoltageAngleL1.d_Value - this.VoltageAngleL2.d_Value) - 120.0d);
            tmpSymmetry[1] = (float) Math.abs(AngleUtil.adjustAngle(this.VoltageAngleL2.d_Value - this.VoltageAngleL3.d_Value) - 120.0d);
            tmpSymmetry[2] = (float) Math.abs(AngleUtil.adjustAngle(this.VoltageAngleL3.d_Value - this.VoltageAngleL1.d_Value) - 120.0d);
            tmpSymmetry[3] = (float) Math.abs(AngleUtil.adjustAngle(this.CurrentAngleL1.d_Value - this.CurrentAngleL2.d_Value) - 120.0d);
            tmpSymmetry[4] = (float) Math.abs(AngleUtil.adjustAngle(this.CurrentAngleL2.d_Value - this.CurrentAngleL3.d_Value) - 120.0d);
            tmpSymmetry[5] = (float) Math.abs(AngleUtil.adjustAngle(this.CurrentAngleL3.d_Value - this.CurrentAngleL1.d_Value) - 120.0d);
        } else {
            tmpSymmetry[0] = (float) Math.abs(AngleUtil.adjustAngle(this.VoltageAngleL2.d_Value - this.VoltageAngleL1.d_Value) - 120.0d);
            tmpSymmetry[1] = (float) Math.abs(AngleUtil.adjustAngle(this.VoltageAngleL3.d_Value - this.VoltageAngleL2.d_Value) - 120.0d);
            tmpSymmetry[2] = (float) Math.abs(AngleUtil.adjustAngle(this.VoltageAngleL1.d_Value - this.VoltageAngleL3.d_Value) - 120.0d);
            tmpSymmetry[3] = (float) Math.abs(AngleUtil.adjustAngle(this.CurrentAngleL2.d_Value - this.CurrentAngleL1.d_Value) - 120.0d);
            tmpSymmetry[4] = (float) Math.abs(AngleUtil.adjustAngle(this.CurrentAngleL3.d_Value - this.CurrentAngleL2.d_Value) - 120.0d);
            tmpSymmetry[5] = (float) Math.abs(AngleUtil.adjustAngle(this.CurrentAngleL1.d_Value - this.CurrentAngleL3.d_Value) - 120.0d);
        }
        int tmpInt14 = ((double) tmpSymmetry[0]) <= this.Limits[1].d_Value ? 0 : 1;
        this.SiteVoltageSymmetry[0] = new DBDataModel((long) tmpInt14, tmpString5[tmpInt14]);
        int tmpInt15 = ((double) tmpSymmetry[1]) <= this.Limits[1].d_Value ? 0 : 1;
        this.SiteVoltageSymmetry[1] = new DBDataModel((long) tmpInt15, tmpString5[tmpInt15]);
        int tmpInt16 = ((double) tmpSymmetry[2]) <= this.Limits[1].d_Value ? 0 : 1;
        this.SiteVoltageSymmetry[2] = new DBDataModel((long) tmpInt16, tmpString5[tmpInt16]);
        int tmpInt17 = ((double) tmpSymmetry[3]) <= this.Limits[3].d_Value ? 0 : 1;
        this.SiteCurrentSymmetry[0] = new DBDataModel((long) tmpInt17, tmpString5[tmpInt17]);
        int tmpInt18 = ((double) tmpSymmetry[4]) <= this.Limits[3].d_Value ? 0 : 1;
        this.SiteCurrentSymmetry[1] = new DBDataModel((long) tmpInt18, tmpString5[tmpInt18]);
        int tmpInt19 = ((double) tmpSymmetry[5]) <= this.Limits[3].d_Value ? 0 : 1;
        this.SiteCurrentSymmetry[2] = new DBDataModel((long) tmpInt19, tmpString5[tmpInt19]);
        int tmpInt20 = this.THDU1.d_Value <= this.Limits[4].d_Value ? 0 : 1;
        this.SiteHarmonic[0] = new DBDataModel((long) tmpInt20, tmpString5[tmpInt20]);
        int tmpInt21 = this.THDU2.d_Value <= this.Limits[4].d_Value ? 0 : 1;
        this.SiteHarmonic[1] = new DBDataModel((long) tmpInt21, tmpString5[tmpInt21]);
        int tmpInt22 = this.THDU3.d_Value <= this.Limits[4].d_Value ? 0 : 1;
        this.SiteHarmonic[2] = new DBDataModel((long) tmpInt22, tmpString5[tmpInt22]);
        int tmpInt23 = this.THDI1.d_Value <= this.Limits[5].d_Value ? 0 : 1;
        this.SiteHarmonic[3] = new DBDataModel((long) tmpInt23, tmpString5[tmpInt23]);
        int tmpInt24 = this.THDI2.d_Value <= this.Limits[5].d_Value ? 0 : 1;
        this.SiteHarmonic[4] = new DBDataModel((long) tmpInt24, tmpString5[tmpInt24]);
        int tmpInt25 = this.THDI3.d_Value <= this.Limits[5].d_Value ? 0 : 1;
        this.SiteHarmonic[5] = new DBDataModel((long) tmpInt25, tmpString5[tmpInt25]);
        if (2 == this.Wiring) {
            this.SiteState[1] = new DBDataModel(-1L, "---");
            this.SiteState[2] = new DBDataModel(-1L, "---");
            this.SiteState[4] = new DBDataModel(-1L, "---");
            this.SiteState[5] = new DBDataModel(-1L, "---");
            this.SiteCurrentDirection[1] = new DBDataModel(-1L, "---");
            this.SiteCurrentDirection[2] = new DBDataModel(-1L, "---");
            this.SiteVoltageBalance = new DBDataModel[]{new DBDataModel(-1L, "---"), new DBDataModel(-1L, "---"), new DBDataModel(-1L, "---")};
            this.SiteCurrentBalance = new DBDataModel[]{new DBDataModel(-1L, "---"), new DBDataModel(-1L, "---"), new DBDataModel(-1L, "---")};
            this.SiteVoltageSymmetry = new DBDataModel[]{new DBDataModel(-1L, "---"), new DBDataModel(-1L, "---"), new DBDataModel(-1L, "---")};
            this.SiteCurrentSymmetry = new DBDataModel[]{new DBDataModel(-1L, "---"), new DBDataModel(-1L, "---"), new DBDataModel(-1L, "---")};
            this.SiteHarmonic[1] = new DBDataModel(-1L, "---");
            this.SiteHarmonic[2] = new DBDataModel(-1L, "---");
            this.SiteHarmonic[4] = new DBDataModel(-1L, "---");
            this.SiteHarmonic[5] = new DBDataModel(-1L, "---");
        }
    }

    public void getResult(int wiring, int[] phaseSequence2, int IntRemark) {
        int[] tmp_PhaseSequence;
        int[] textKey;
        Resources res = CLApplication.app.getResources();
        String tmpRemark = PdfObject.NOTHING;
        this.Remark = PdfObject.NOTHING;
        if (1 == wiring) {
            tmp_PhaseSequence = new int[]{phaseSequence2[0], phaseSequence2[1], phaseSequence2[2], phaseSequence2[3], phaseSequence2[5]};
            textKey = new int[]{R.string.text_u1, R.string.text_u2, R.string.text_u3, R.string.text_i1, R.string.text_i3};
        } else {
            tmp_PhaseSequence = new int[]{phaseSequence2[0], phaseSequence2[1], phaseSequence2[2], phaseSequence2[3], phaseSequence2[4], phaseSequence2[5]};
            textKey = new int[]{R.string.text_u1, R.string.text_u2, R.string.text_u3, R.string.text_i1, R.string.text_i2, R.string.text_i3};
        }
        for (int i = 0; i < 3; i++) {
            parsePhaseSequence(tmp_PhaseSequence[i]);
        }
        for (int i2 = 3; i2 < tmp_PhaseSequence.length; i2++) {
            if (OtherUtils.isEmpty(parsePhaseSequence(tmp_PhaseSequence[i2]).trim())) {
                tmpRemark = String.valueOf(tmpRemark) + res.getString(textKey[i2]) + " ";
            }
        }
        int tempIntRemark = IntRemark;
        String[] remarks = res.getStringArray(R.array.wiring_Remark01);
        for (int i3 = 0; i3 <= 21; i3++) {
            this.Remark = String.valueOf(this.Remark) + ((tempIntRemark & 1) == 1 ? remarks[i3] : PdfObject.NOTHING);
            tempIntRemark >>= 1;
        }
        int indexRemark24 = (IntRemark >> 24) & 7;
        this.Remark = String.valueOf(this.Remark) + (indexRemark24 <= 7 ? res.getStringArray(R.array.wiring_Remark)[indexRemark24] : PdfObject.NOTHING);
    }

    private String[] getCurrectSequence(boolean is3P) {
        Resources res = CLApplication.app.getResources();
        if (is3P) {
            return new String[]{String.valueOf(res.getString(R.string.text_1)) + " | " + res.getString(R.string.text_2) + " | " + res.getString(R.string.text_3), String.valueOf(res.getString(R.string.text_1)) + " | " + res.getString(R.string.text_3)};
        }
        return new String[]{String.valueOf(res.getString(R.string.text_1)) + " | " + res.getString(R.string.text_2) + " | " + res.getString(R.string.text_3), String.valueOf(res.getString(R.string.text_1)) + " | " + res.getString(R.string.text_2) + " | " + res.getString(R.string.text_3)};
    }

    private String parsePhaseSequence(int phaseSequenceValue) {
        Resources res = CLApplication.app.getResources();
        switch (phaseSequenceValue) {
            case 1:
                return res.getString(R.string.text_1);
            case 2:
                return res.getString(R.string.text_2);
            case 4:
                return res.getString(R.string.text_3);
            case 8:
                return "-" + res.getString(R.string.text_1);
            case 16:
                return "-" + res.getString(R.string.text_2);
            case 32:
                return "-" + res.getString(R.string.text_3);
            default:
                return " ";
        }
    }

    private int parseIntPhaseSequence(int phaseSequenceValue) {
        CLApplication.app.getResources();
        switch (phaseSequenceValue) {
            case 1:
            case 8:
                return 1;
            case 2:
            case 16:
                return 2;
            case 4:
            case 32:
                return 3;
            default:
                return 4;
        }
    }
}
