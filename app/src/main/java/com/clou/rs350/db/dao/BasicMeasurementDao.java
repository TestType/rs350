package com.clou.rs350.db.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import com.clou.rs350.db.dao.BaseTestDao;
import com.clou.rs350.db.model.BasicMeasurement;
import com.clou.rs350.manager.DatabaseManager;

public class BasicMeasurementDao extends BaseTestDao {
    private static final String TABLE_NAME = "TestBasicMeasurement";

    private static final class TableColumns {
        public static final String ActivePowerL1 = "ActivePowerL1";
        public static final String ActivePowerL2 = "ActivePowerL2";
        public static final String ActivePowerL3 = "ActivePowerL3";
        public static final String ActivePowerTotal = "ActivePowerTotal";
        public static final String AngleU12I1 = "AngleU12I1";
        public static final String AngleU23I2 = "AngleU23I2";
        public static final String AngleU32I3 = "AngleU32I3";
        public static final String AngleVoltageL1L2 = "AngleVoltageL1L2";
        public static final String AngleVoltageL2L3 = "AngleVoltageL2L3";
        public static final String AngleVoltageL3L1 = "AngleVoltageL3L1";
        public static final String ApparentPowerL1 = "ApparentPowerL1";
        public static final String ApparentPowerL2 = "ApparentPowerL2";
        public static final String ApparentPowerL3 = "ApparentPowerL3";
        public static final String ApparentPowerTotal = "ApparentPowerTotal";
        public static final String ApparentPowerType = "ApparentPowerType";
        public static final String CFI1 = "CFI1";
        public static final String CFI2 = "CFI2";
        public static final String CFI3 = "CFI3";
        public static final String CFU1 = "CFU1";
        public static final String CFU2 = "CFU2";
        public static final String CFU3 = "CFU3";
        public static final String CurrentAngleL1 = "CurrentAngleL1";
        public static final String CurrentAngleL2 = "CurrentAngleL2";
        public static final String CurrentAngleL3 = "CurrentAngleL3";
        public static final String CurrentLN = "CurrentLN";
        public static final String CurrentPhaseSequence = "CurrentPhaseSequence";
        public static final String CurrentValueL1 = "CurrentValueL1";
        public static final String CurrentValueL2 = "CurrentValueL2";
        public static final String CurrentValueL3 = "CurrentValueL3";
        public static final String Frequency = "Frequency";
        public static final String PhaseSequence = "PhaseSequence";
        public static final String PowerFactorL1 = "PowerFactorL1";
        public static final String PowerFactorL2 = "PowerFactorL2";
        public static final String PowerFactorL3 = "PowerFactorL3";
        public static final String PowerFactorTotal = "PowerFactorTotal";
        public static final String ReactivePowerL1 = "ReactivePowerL1";
        public static final String ReactivePowerL2 = "ReactivePowerL2";
        public static final String ReactivePowerL3 = "ReactivePowerL3";
        public static final String ReactivePowerTotal = "ReactivePowerTotal";
        public static final String THDI1 = "THDI1";
        public static final String THDI2 = "THDI2";
        public static final String THDI3 = "THDI3";
        public static final String THDU1 = "THDU1";
        public static final String THDU2 = "THDU2";
        public static final String THDU3 = "THDU3";
        public static final String VoltageAngleL1 = "VoltageAngleL1";
        public static final String VoltageAngleL2 = "VoltageAngleL2";
        public static final String VoltageAngleL3 = "VoltageAngleL3";
        public static final String VoltageCurrentAngleL1 = "VoltageCurrentAngleL1";
        public static final String VoltageCurrentAngleL2 = "VoltageCurrentAngleL2";
        public static final String VoltageCurrentAngleL3 = "VoltageCurrentAngleL3";
        public static final String VoltageL1L2 = "VoltageL1L2";
        public static final String VoltageL2L3 = "VoltageL2L3";
        public static final String VoltageL3L1 = "VoltageL3L1";
        public static final String VoltageLN = "VoltageLN";
        public static final String VoltagePhaseSequence = "VoltagePhaseSequence";
        public static final String VoltageValueL1 = "VoltageValueL1";
        public static final String VoltageValueL2 = "VoltageValueL2";
        public static final String VoltageValueL3 = "VoltageValueL3";

        private TableColumns() {
        }
    }

    public BasicMeasurementDao(Context context) {
        super(context, TABLE_NAME);
    }

    public void createTable(SQLiteDatabase db) {
        this.m_TableName = TABLE_NAME;
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append(getCreatTableString());
        stringBuffer.append("VoltageValueL1").append(" VARCHAR(20),");
        stringBuffer.append("VoltageValueL2").append(" VARCHAR(20),");
        stringBuffer.append("VoltageValueL3").append(" VARCHAR(20),");
        stringBuffer.append("CurrentValueL1").append(" VARCHAR(20),");
        stringBuffer.append("CurrentValueL2").append(" VARCHAR(20),");
        stringBuffer.append("CurrentValueL3").append(" VARCHAR(20),");
        stringBuffer.append("VoltageAngleL1").append(" VARCHAR(20),");
        stringBuffer.append("VoltageAngleL2").append(" VARCHAR(20),");
        stringBuffer.append("VoltageAngleL3").append(" VARCHAR(20),");
        stringBuffer.append("CurrentAngleL1").append(" VARCHAR(20),");
        stringBuffer.append("CurrentAngleL2").append(" VARCHAR(20),");
        stringBuffer.append("CurrentAngleL3").append(" VARCHAR(20),");
        stringBuffer.append("VoltageCurrentAngleL1").append(" VARCHAR(50),");
        stringBuffer.append("VoltageCurrentAngleL2").append(" VARCHAR(50),");
        stringBuffer.append("VoltageCurrentAngleL3").append(" VARCHAR(50),");
        stringBuffer.append("ActivePowerL1").append(" VARCHAR(20),");
        stringBuffer.append("ActivePowerL2").append(" VARCHAR(20),");
        stringBuffer.append("ActivePowerL3").append(" VARCHAR(20),");
        stringBuffer.append("ActivePowerTotal").append(" VARCHAR(20),");
        stringBuffer.append("ReactivePowerL1").append(" VARCHAR(20),");
        stringBuffer.append("ReactivePowerL2").append(" VARCHAR(20),");
        stringBuffer.append("ReactivePowerL3").append(" VARCHAR(20),");
        stringBuffer.append("ReactivePowerTotal").append(" VARCHAR(20),");
        stringBuffer.append("ApparentPowerL1").append(" VARCHAR(20),");
        stringBuffer.append("ApparentPowerL2").append(" VARCHAR(20),");
        stringBuffer.append("ApparentPowerL3").append(" VARCHAR(20),");
        stringBuffer.append("ApparentPowerTotal").append(" VARCHAR(20),");
        stringBuffer.append("ApparentPowerType").append(" VARCHAR(10),");
        stringBuffer.append("PowerFactorL1").append(" VARCHAR(10),");
        stringBuffer.append("PowerFactorL2").append(" VARCHAR(10),");
        stringBuffer.append("PowerFactorL3").append(" VARCHAR(10),");
        stringBuffer.append("PowerFactorTotal").append(" VARCHAR(10),");
        stringBuffer.append("VoltageL1L2").append(" VARCHAR(20),");
        stringBuffer.append("VoltageL2L3").append(" VARCHAR(20),");
        stringBuffer.append("VoltageL3L1").append(" VARCHAR(20),");
        stringBuffer.append("AngleVoltageL3L1").append(" VARCHAR(10),");
        stringBuffer.append("AngleVoltageL1L2").append(" VARCHAR(10),");
        stringBuffer.append("AngleVoltageL2L3").append(" VARCHAR(10),");
        stringBuffer.append("AngleU12I1").append(" VARCHAR(10),");
        stringBuffer.append("AngleU23I2").append(" VARCHAR(10),");
        stringBuffer.append("AngleU32I3").append(" VARCHAR(10),");
        stringBuffer.append("Frequency").append(" VARCHAR(10),");
        stringBuffer.append("THDU1").append(" VARCHAR(10),");
        stringBuffer.append("THDU2").append(" VARCHAR(10),");
        stringBuffer.append("THDU3").append(" VARCHAR(10),");
        stringBuffer.append("THDI1").append(" VARCHAR(10),");
        stringBuffer.append("THDI2").append(" VARCHAR(10),");
        stringBuffer.append("THDI3").append(" VARCHAR(10),");
        stringBuffer.append(TableColumns.CFU1).append(" VARCHAR(10),");
        stringBuffer.append(TableColumns.CFU2).append(" VARCHAR(10),");
        stringBuffer.append(TableColumns.CFU3).append(" VARCHAR(10),");
        stringBuffer.append(TableColumns.CFI1).append(" VARCHAR(10),");
        stringBuffer.append(TableColumns.CFI2).append(" VARCHAR(10),");
        stringBuffer.append(TableColumns.CFI3).append(" VARCHAR(10),");
        stringBuffer.append("VoltageLN").append(" VARCHAR(20),");
        stringBuffer.append("CurrentLN").append(" VARCHAR(20),");
        stringBuffer.append(TableColumns.PhaseSequence).append(" VARCHAR(20),");
        stringBuffer.append("VoltagePhaseSequence").append(" VARCHAR(20),");
        stringBuffer.append("CurrentPhaseSequence").append(" VARCHAR(20));");
        db.execSQL(stringBuffer.toString());
    }

    @Override // com.clou.rs350.db.dao.BaseDao, com.clou.rs350.db.dao.BaseTestDao
    public void upgradeTable(SQLiteDatabase db, int oldVersion, int newVersion) {
        if (oldVersion <= 3) {
            db.execSQL("alter table BasicMeasurement rename to TestBasicMeasurement");
        }
        super.upgradeTable(db, oldVersion, newVersion);
    }

    @Override // com.clou.rs350.db.dao.BaseDao, com.clou.rs350.db.dao.BaseTestDao
    public synchronized boolean insertObject(Object Data, String Time) {
        SQLiteDatabase db;
        ContentValues values;
        db = DatabaseManager.getWriteableDatabase(this.m_Context);
        values = new ContentValues();
        BasicMeasurement object = (BasicMeasurement) Data;
        values.put(this.m_TableKey, object.CustomerSerialNo.toString());
        values.put("CreateTime", Time);
        values.put(SameColumns.WiringType, object.WiringValue);
        values.put("IntWiringType", Integer.valueOf(object.Wiring));
        values.put(SameColumns.ConnectionMode, object.ConnectionType.toString());
        values.put(SameColumns.VoltageRangeL1, object.VoltageRangeL1.toString());
        values.put(SameColumns.VoltageRangeL2, object.VoltageRangeL2.toString());
        values.put(SameColumns.VoltageRangeL3, object.VoltageRangeL3.toString());
        values.put(SameColumns.CurrentRangeL1, object.CurrentRangeL1.toString());
        values.put(SameColumns.CurrentRangeL2, object.CurrentRangeL2.toString());
        values.put(SameColumns.CurrentRangeL3, object.CurrentRangeL3.toString());
        values.put("VoltageValueL1", object.VoltageValueL1.toString());
        values.put("VoltageValueL2", object.VoltageValueL2.toString());
        values.put("VoltageValueL3", object.VoltageValueL3.toString());
        values.put("CurrentValueL1", object.CurrentValueL1.toString());
        values.put("CurrentValueL2", object.CurrentValueL2.toString());
        values.put("CurrentValueL3", object.CurrentValueL3.toString());
        values.put("VoltageAngleL1", object.VoltageAngleL1.toString());
        values.put("VoltageAngleL2", object.VoltageAngleL2.toString());
        values.put("VoltageAngleL3", object.VoltageAngleL3.toString());
        values.put("CurrentAngleL1", object.CurrentAngleL1.toString());
        values.put("CurrentAngleL2", object.CurrentAngleL2.toString());
        values.put("CurrentAngleL3", object.CurrentAngleL3.toString());
        values.put("VoltageCurrentAngleL1", object.VoltageCurrentAngleL1.toString());
        values.put("VoltageCurrentAngleL2", object.VoltageCurrentAngleL2.toString());
        values.put("VoltageCurrentAngleL3", object.VoltageCurrentAngleL3.toString());
        values.put("ActivePowerL1", object.ActivePowerL1.toString());
        values.put("ActivePowerL2", object.ActivePowerL2.toString());
        values.put("ActivePowerL3", object.ActivePowerL3.toString());
        values.put("ActivePowerTotal", object.ActivePowerTotal.toString());
        values.put("ReactivePowerL1", object.ReactivePowerL1.toString());
        values.put("ReactivePowerL2", object.ReactivePowerL2.toString());
        values.put("ReactivePowerL3", object.ReactivePowerL3.toString());
        values.put("ReactivePowerTotal", object.ReactivePowerTotal.toString());
        values.put("ApparentPowerL1", object.ApparentPowerL1.toString());
        values.put("ApparentPowerL2", object.ApparentPowerL2.toString());
        values.put("ApparentPowerL3", object.ApparentPowerL3.toString());
        values.put("ApparentPowerTotal", object.ApparentPowerTotal.toString());
        values.put("ApparentPowerType", object.ApparentPowerType.toString());
        values.put("PowerFactorL1", object.PowerFactorL1.toString());
        values.put("PowerFactorL2", object.PowerFactorL2.toString());
        values.put("PowerFactorL3", object.PowerFactorL3.toString());
        values.put("PowerFactorTotal", object.PowerFactorTotal.toString());
        values.put("VoltageL1L2", object.VoltageValueL1L2.toString());
        values.put("VoltageL2L3", object.VoltageValueL2L3.toString());
        values.put("VoltageL3L1", object.VoltageValueL3L1.toString());
        values.put("AngleVoltageL3L1", object.VoltageAngleL3L1.toString());
        values.put("AngleVoltageL1L2", object.VoltageAngleL1L2.toString());
        values.put("AngleVoltageL2L3", object.VoltageAngleL2L3.toString());
        values.put("AngleU12I1", object.AngleU12I1.toString());
        values.put("AngleU23I2", object.AngleU23I2.toString());
        values.put("AngleU32I3", object.AngleU31I3.toString());
        values.put("THDU1", object.THDU1.toString());
        values.put("THDU2", object.THDU2.toString());
        values.put("THDU3", object.THDU3.toString());
        values.put("THDI1", object.THDI1.toString());
        values.put("THDI2", object.THDI2.toString());
        values.put("THDI3", object.THDI3.toString());
        values.put(TableColumns.CFU1, object.CFU1.toString());
        values.put(TableColumns.CFU2, object.CFU2.toString());
        values.put(TableColumns.CFU3, object.CFU3.toString());
        values.put(TableColumns.CFI1, object.CFI1.toString());
        values.put(TableColumns.CFI2, object.CFI2.toString());
        values.put(TableColumns.CFI3, object.CFI3.toString());
        values.put("VoltageLN", object.VoltageLN.toString());
        values.put("CurrentLN", object.CurrentLN.toString());
        values.put("Frequency", object.Frequency.toString());
        values.put(TableColumns.PhaseSequence, object.PhaseSequence);
        values.put("VoltagePhaseSequence", object.VoltagePhaseSequence);
        values.put("CurrentPhaseSequence", object.CurrentPhaseSequence);
        return db.insert(TABLE_NAME, null, values) != -1;
    }

    /* access modifiers changed from: protected */
    @Override // com.clou.rs350.db.dao.BaseDao, com.clou.rs350.db.dao.BaseTestDao
    public Object parseData(Cursor cursor) {
        BasicMeasurement value = new BasicMeasurement();
        value.CustomerSerialNo = parseString(cursor.getString(cursor.getColumnIndex(this.m_TableKey)));
        value.WiringValue = parseString(cursor.getString(cursor.getColumnIndex(SameColumns.WiringType)));
        value.Wiring = cursor.getInt(cursor.getColumnIndex("IntWiringType"));
        value.VoltageRangeL1 = parseString(cursor.getString(cursor.getColumnIndex(SameColumns.VoltageRangeL1)));
        value.VoltageRangeL2 = parseString(cursor.getString(cursor.getColumnIndex(SameColumns.VoltageRangeL2)));
        value.VoltageRangeL3 = parseString(cursor.getString(cursor.getColumnIndex(SameColumns.VoltageRangeL3)));
        value.CurrentRangeL1 = parseString(cursor.getString(cursor.getColumnIndex(SameColumns.CurrentRangeL1)));
        value.CurrentRangeL2 = parseString(cursor.getString(cursor.getColumnIndex(SameColumns.CurrentRangeL2)));
        value.CurrentRangeL3 = parseString(cursor.getString(cursor.getColumnIndex(SameColumns.CurrentRangeL3)));
        value.VoltageValueL1.s_Value = parseString(cursor.getString(cursor.getColumnIndex("VoltageValueL1")));
        value.VoltageValueL2.s_Value = parseString(cursor.getString(cursor.getColumnIndex("VoltageValueL2")));
        value.VoltageValueL3.s_Value = parseString(cursor.getString(cursor.getColumnIndex("VoltageValueL3")));
        value.CurrentValueL1.s_Value = parseString(cursor.getString(cursor.getColumnIndex("CurrentValueL1")));
        value.CurrentValueL2.s_Value = parseString(cursor.getString(cursor.getColumnIndex("CurrentValueL2")));
        value.CurrentValueL3.s_Value = parseString(cursor.getString(cursor.getColumnIndex("CurrentValueL3")));
        value.VoltageValueL1L2.s_Value = parseString(cursor.getString(cursor.getColumnIndex("VoltageL1L2")));
        value.VoltageValueL2L3.s_Value = parseString(cursor.getString(cursor.getColumnIndex("VoltageL2L3")));
        value.VoltageValueL3L1.s_Value = parseString(cursor.getString(cursor.getColumnIndex("VoltageL3L1")));
        value.VoltageAngleL1.s_Value = parseString(cursor.getString(cursor.getColumnIndex("VoltageAngleL1")));
        value.VoltageAngleL2.s_Value = parseString(cursor.getString(cursor.getColumnIndex("VoltageAngleL2")));
        value.VoltageAngleL3.s_Value = parseString(cursor.getString(cursor.getColumnIndex("VoltageAngleL3")));
        value.CurrentAngleL1.s_Value = parseString(cursor.getString(cursor.getColumnIndex("CurrentAngleL1")));
        value.CurrentAngleL2.s_Value = parseString(cursor.getString(cursor.getColumnIndex("CurrentAngleL2")));
        value.CurrentAngleL3.s_Value = parseString(cursor.getString(cursor.getColumnIndex("CurrentAngleL3")));
        value.VoltageAngleL1L2.s_Value = parseString(cursor.getString(cursor.getColumnIndex("AngleVoltageL1L2")));
        value.VoltageAngleL2L3.s_Value = parseString(cursor.getString(cursor.getColumnIndex("AngleVoltageL2L3")));
        value.VoltageAngleL3L1.s_Value = parseString(cursor.getString(cursor.getColumnIndex("AngleVoltageL3L1")));
        value.VoltageCurrentAngleL1.s_Value = parseString(cursor.getString(cursor.getColumnIndex("VoltageCurrentAngleL1")));
        value.VoltageCurrentAngleL2.s_Value = parseString(cursor.getString(cursor.getColumnIndex("VoltageCurrentAngleL2")));
        value.VoltageCurrentAngleL3.s_Value = parseString(cursor.getString(cursor.getColumnIndex("VoltageCurrentAngleL3")));
        value.AngleU12I1.s_Value = parseString(cursor.getString(cursor.getColumnIndex("AngleU12I1")));
        value.AngleU23I2.s_Value = parseString(cursor.getString(cursor.getColumnIndex("AngleU23I2")));
        value.AngleU31I3.s_Value = parseString(cursor.getString(cursor.getColumnIndex("AngleU32I3")));
        value.ActivePowerL1.s_Value = parseString(cursor.getString(cursor.getColumnIndex("ActivePowerL1")));
        value.ActivePowerL2.s_Value = parseString(cursor.getString(cursor.getColumnIndex("ActivePowerL2")));
        value.ActivePowerL3.s_Value = parseString(cursor.getString(cursor.getColumnIndex("ActivePowerL3")));
        value.ActivePowerTotal.s_Value = parseString(cursor.getString(cursor.getColumnIndex("ActivePowerTotal")));
        value.ReactivePowerL1.s_Value = parseString(cursor.getString(cursor.getColumnIndex("ReactivePowerL1")));
        value.ReactivePowerL2.s_Value = parseString(cursor.getString(cursor.getColumnIndex("ReactivePowerL2")));
        value.ReactivePowerL3.s_Value = parseString(cursor.getString(cursor.getColumnIndex("ReactivePowerL3")));
        value.ReactivePowerTotal.s_Value = parseString(cursor.getString(cursor.getColumnIndex("ReactivePowerTotal")));
        value.ApparentPowerL1.s_Value = parseString(cursor.getString(cursor.getColumnIndex("ApparentPowerL1")));
        value.ApparentPowerL2.s_Value = parseString(cursor.getString(cursor.getColumnIndex("ApparentPowerL2")));
        value.ApparentPowerL3.s_Value = parseString(cursor.getString(cursor.getColumnIndex("ApparentPowerL3")));
        value.ApparentPowerTotal.s_Value = parseString(cursor.getString(cursor.getColumnIndex("ApparentPowerTotal")));
        value.PowerFactorL1.s_Value = parseString(cursor.getString(cursor.getColumnIndex("PowerFactorL1")));
        value.PowerFactorL2.s_Value = parseString(cursor.getString(cursor.getColumnIndex("PowerFactorL2")));
        value.PowerFactorL3.s_Value = parseString(cursor.getString(cursor.getColumnIndex("PowerFactorL3")));
        value.PowerFactorTotal.s_Value = parseString(cursor.getString(cursor.getColumnIndex("PowerFactorTotal")));
        value.THDU1.s_Value = parseString(cursor.getString(cursor.getColumnIndex("THDU1")));
        value.THDU2.s_Value = parseString(cursor.getString(cursor.getColumnIndex("THDU2")));
        value.THDU3.s_Value = parseString(cursor.getString(cursor.getColumnIndex("THDU3")));
        value.THDI1.s_Value = parseString(cursor.getString(cursor.getColumnIndex("THDI1")));
        value.THDI2.s_Value = parseString(cursor.getString(cursor.getColumnIndex("THDI2")));
        value.THDI3.s_Value = parseString(cursor.getString(cursor.getColumnIndex("THDI3")));
        value.CFU1.s_Value = parseString(cursor.getString(cursor.getColumnIndex(TableColumns.CFU1)));
        value.CFU2.s_Value = parseString(cursor.getString(cursor.getColumnIndex(TableColumns.CFU2)));
        value.CFU3.s_Value = parseString(cursor.getString(cursor.getColumnIndex(TableColumns.CFU3)));
        value.CFI1.s_Value = parseString(cursor.getString(cursor.getColumnIndex(TableColumns.CFI1)));
        value.CFI2.s_Value = parseString(cursor.getString(cursor.getColumnIndex(TableColumns.CFI2)));
        value.CFI3.s_Value = parseString(cursor.getString(cursor.getColumnIndex(TableColumns.CFI3)));
        value.VoltageLN.s_Value = parseString(cursor.getString(cursor.getColumnIndex("VoltageLN")));
        value.CurrentLN.s_Value = parseString(cursor.getString(cursor.getColumnIndex("CurrentLN")));
        value.Frequency.s_Value = parseString(cursor.getString(cursor.getColumnIndex("Frequency")));
        value.PhaseSequence = parseString(cursor.getString(cursor.getColumnIndex(TableColumns.PhaseSequence)));
        value.VoltagePhaseSequence = parseString(cursor.getString(cursor.getColumnIndex("VoltagePhaseSequence")));
        value.CurrentPhaseSequence = parseString(cursor.getString(cursor.getColumnIndex("CurrentPhaseSequence")));
        return value;
    }
}
