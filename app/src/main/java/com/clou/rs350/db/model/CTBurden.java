package com.clou.rs350.db.model;

import android.database.Cursor;

import com.clou.rs350.CLApplication;
import com.clou.rs350.R;
import com.clou.rs350.device.MeterBaseDevice;
import com.clou.rs350.device.model.ExplainedModel;
import com.itextpdf.text.pdf.PdfObject;

public class CTBurden extends BasicMeasurement {
    public DBDataModel[] Burden;
    public DBDataModel[] BurdenPersents;
    public int CTTerminal;
    public DBDataModel CTVA;
    public String[] CurrentRangeSec;
    public DBDataModel[] CurrentValueSecondary;
    public DBDataModel LengthOfWire;
    private DBDataModel[] MeasureBurden;
    public DBDataModel[] Result;
    public String SerialNo;
    public DBDataModel SizeOfWire;
    public String[] VoltageRangeSec;
    public DBDataModel[] VoltageValueSecondary;
    private double m_Resistance;

    public CTBurden() {
        this.SerialNo = PdfObject.NOTHING;
        this.VoltageRangeSec = new String[]{PdfObject.NOTHING, PdfObject.NOTHING, PdfObject.NOTHING};
        this.CurrentRangeSec = new String[]{PdfObject.NOTHING, PdfObject.NOTHING, PdfObject.NOTHING};
        this.CurrentValueSecondary = new DBDataModel[]{new DBDataModel(), new DBDataModel(), new DBDataModel()};
        this.VoltageValueSecondary = new DBDataModel[]{new DBDataModel(), new DBDataModel(), new DBDataModel()};
        this.MeasureBurden = new DBDataModel[]{new DBDataModel(), new DBDataModel(), new DBDataModel()};
        this.Burden = new DBDataModel[]{new DBDataModel(), new DBDataModel(), new DBDataModel()};
        this.BurdenPersents = new DBDataModel[]{new DBDataModel(), new DBDataModel(), new DBDataModel()};
        this.Result = new DBDataModel[]{new DBDataModel(-1L, "---"), new DBDataModel(-1L, "---"), new DBDataModel(-1L, "---")};
        this.CTVA = new DBDataModel();
        this.CTTerminal = 0;
        this.LengthOfWire = new DBDataModel();
        this.SizeOfWire = new DBDataModel();
        this.m_Resistance = 0.0d;
    }

    public CTBurden(Cursor cursor) {
        this.SerialNo = PdfObject.NOTHING;
        this.VoltageRangeSec = new String[]{PdfObject.NOTHING, PdfObject.NOTHING, PdfObject.NOTHING};
        this.CurrentRangeSec = new String[]{PdfObject.NOTHING, PdfObject.NOTHING, PdfObject.NOTHING};
        this.CurrentValueSecondary = new DBDataModel[]{new DBDataModel(), new DBDataModel(), new DBDataModel()};
        this.VoltageValueSecondary = new DBDataModel[]{new DBDataModel(), new DBDataModel(), new DBDataModel()};
        this.MeasureBurden = new DBDataModel[]{new DBDataModel(), new DBDataModel(), new DBDataModel()};
        this.Burden = new DBDataModel[]{new DBDataModel(), new DBDataModel(), new DBDataModel()};
        this.BurdenPersents = new DBDataModel[]{new DBDataModel(), new DBDataModel(), new DBDataModel()};
        this.Result = new DBDataModel[]{new DBDataModel(-1L, "---"), new DBDataModel(-1L, "---"), new DBDataModel(-1L, "---")};
        this.CTVA = new DBDataModel();
        this.CTTerminal = 0;
        this.LengthOfWire = new DBDataModel();
        this.SizeOfWire = new DBDataModel();
        this.m_Resistance = 0.0d;
    }

    public void parseData(MeterBaseDevice Device, int Index) {
        parseData(Device);
        ExplainedModel[] TmpValues = Device.getCTBurdenValue();
        this.VoltageValueSecondary[Index] = parse(TmpValues[0]);
        this.CurrentValueSecondary[Index] = parse(TmpValues[1]);
        this.MeasureBurden[Index] = parse(TmpValues[2]);
        CalcVA(Index);
        this.VoltageRangeSec[Index] = this.VoltageRangeL2;
        this.CurrentRangeSec[Index] = this.CurrentRangeL2;
    }

    public void CalcVA(int Index) {
        if (!this.MeasureBurden[Index].s_Value.equals("---")) {
            if (this.CTTerminal == 0) {
                this.Burden[Index] = this.MeasureBurden[Index];
            } else {
                this.Burden[Index] = parse(Double.valueOf(this.MeasureBurden[Index].d_Value + (this.CurrentValueSecondary[Index].d_Value * this.CurrentValueSecondary[Index].d_Value * this.m_Resistance)), "VA");
            }
            CalcPersents(Index);
        }
    }

    public void CalcPersents(int Index) {
        if (!this.Burden[Index].s_Value.equals("---")) {
            this.BurdenPersents[Index] = parse(Double.valueOf((this.Burden[Index].d_Value * 100.0d) / this.CTVA.d_Value), "%");
            String[] s_Result = CLApplication.app.getResources().getStringArray(R.array.limit);
            int i_Result = 0;
            if (this.BurdenPersents[Index].d_Value < 20.0d || this.BurdenPersents[Index].d_Value > 100.0d) {
                i_Result = 1;
            }
            this.Result[Index] = new DBDataModel((long) i_Result, s_Result[i_Result]);
        }
    }

    public void setVAValue(double VA) {
        this.CTVA = new DBDataModel(VA, String.valueOf(VA) + "VA");
        CalcPersents(0);
        CalcPersents(1);
        CalcPersents(2);
    }

    public void setWireValue(double length, double size) {
        this.LengthOfWire = new DBDataModel(length, String.valueOf(length) + "m");
        this.SizeOfWire = new DBDataModel(size, String.valueOf(size) + "mm");
        if (0.0d != size) {
            this.m_Resistance = (0.0172d * length) / size;
        }
    }

    @Override // com.clou.rs350.db.model.BasicMeasurement
    public boolean isEmpty() {
        boolean result = true;
        for (int i = 0; i < 3; i++) {
            if (-1 != this.Result[i].l_Value) {
                result = false;
            }
        }
        return result;
    }

    public void cleanData() {
        this.CurrentValueSecondary = new DBDataModel[]{new DBDataModel(), new DBDataModel(), new DBDataModel()};
        this.VoltageValueSecondary = new DBDataModel[]{new DBDataModel(), new DBDataModel(), new DBDataModel()};
        this.MeasureBurden = new DBDataModel[]{new DBDataModel(), new DBDataModel(), new DBDataModel()};
        this.Burden = new DBDataModel[]{new DBDataModel(), new DBDataModel(), new DBDataModel()};
        this.BurdenPersents = new DBDataModel[]{new DBDataModel(), new DBDataModel(), new DBDataModel()};
        this.Result = new DBDataModel[]{new DBDataModel(-1L, "---"), new DBDataModel(-1L, "---"), new DBDataModel(-1L, "---")};
    }
}
