package com.clou.rs350.db.upload;

import com.clou.rs350.device.utils.BufferUtil;
import com.joanzapata.pdfview.util.Constants;
import java.nio.ByteBuffer;

public abstract class SendPacket {
    private byte bt_head = ((byte) this.head);
    private int head = Constants.Pinch.QUICK_MOVE_THRESHOLD_TIME;

    public abstract byte[] GetBody();

    public byte[] GetPacketData() {
        ByteBuffer buf = ByteBuffer.allocate(1024);
        buf.put(this.bt_head);
        buf.put(GetBody());
        buf.put(BufferUtil.getChecksum(buf.array(), 0, buf.position()));
        byte[] bt_result = buf.array();
        buf.clear();
        return bt_result;
    }
}
