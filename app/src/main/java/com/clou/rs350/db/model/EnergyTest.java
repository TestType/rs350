package com.clou.rs350.db.model;

import com.clou.rs350.device.MeterBaseDevice;
import com.clou.rs350.device.model.ExplainedModel;
import com.clou.rs350.model.ClouData;
import com.clou.rs350.utils.OtherUtils;
import com.itextpdf.text.pdf.PdfObject;

public class EnergyTest extends BasicMeasurement {
    public double[] Accuracy;
    public DBDataModel[] BeginEnergy;
    public DBDataModel[] Difference;
    public DBDataModel[] EndEnergy;
    public DBDataModel[] Error;
    public String MeterIndex;
    public String MeterSerialNo;
    public DBDataModel[] Power;
    public DBDataModel[] Result;
    public long RunningTime;
    public DBDataModel[] StandardEnergy;
    public long TestEndTime;
    public long TestStartTime;
    public int Unit;
    public boolean[] testFlag;

    public EnergyTest() {
        this.MeterIndex = "0";
        this.MeterSerialNo = PdfObject.NOTHING;
        this.Unit = 1;
        this.testFlag = new boolean[]{true, true, true};
        this.Accuracy = new double[]{0.0d, 0.0d, 0.0d};
        this.Result = new DBDataModel[]{new DBDataModel(-1L, PdfObject.NOTHING), new DBDataModel(-1L, PdfObject.NOTHING), new DBDataModel(-1L, PdfObject.NOTHING)};
        this.BeginEnergy = new DBDataModel[]{new DBDataModel(), new DBDataModel(), new DBDataModel()};
        this.EndEnergy = new DBDataModel[]{new DBDataModel(), new DBDataModel(), new DBDataModel()};
        this.Difference = new DBDataModel[]{new DBDataModel(), new DBDataModel(), new DBDataModel()};
        this.StandardEnergy = new DBDataModel[]{new DBDataModel(), new DBDataModel(), new DBDataModel()};
        this.Error = new DBDataModel[]{new DBDataModel(), new DBDataModel(), new DBDataModel()};
        this.TestStartTime = 0;
        this.TestEndTime = 0;
        this.RunningTime = 0;
        this.Power = new DBDataModel[]{new DBDataModel(), new DBDataModel(), new DBDataModel()};
        this.MeterIndex = "0";
    }

    public EnergyTest(int meterIndex) {
        this.MeterIndex = "0";
        this.MeterSerialNo = PdfObject.NOTHING;
        this.Unit = 1;
        this.testFlag = new boolean[]{true, true, true};
        this.Accuracy = new double[]{0.0d, 0.0d, 0.0d};
        this.Result = new DBDataModel[]{new DBDataModel(-1L, PdfObject.NOTHING), new DBDataModel(-1L, PdfObject.NOTHING), new DBDataModel(-1L, PdfObject.NOTHING)};
        this.BeginEnergy = new DBDataModel[]{new DBDataModel(), new DBDataModel(), new DBDataModel()};
        this.EndEnergy = new DBDataModel[]{new DBDataModel(), new DBDataModel(), new DBDataModel()};
        this.Difference = new DBDataModel[]{new DBDataModel(), new DBDataModel(), new DBDataModel()};
        this.StandardEnergy = new DBDataModel[]{new DBDataModel(), new DBDataModel(), new DBDataModel()};
        this.Error = new DBDataModel[]{new DBDataModel(), new DBDataModel(), new DBDataModel()};
        this.TestStartTime = 0;
        this.TestEndTime = 0;
        this.RunningTime = 0;
        this.Power = new DBDataModel[]{new DBDataModel(), new DBDataModel(), new DBDataModel()};
        this.MeterIndex = new StringBuilder(String.valueOf(meterIndex)).toString();
    }

    public void setEnergyValue(int pqFlag, double beginEnergy, double endEnergy) {
        String tmpUnit = OtherUtils.parseUnitByWiring(pqFlag, "Wh");
        switch (this.Unit) {
            case 1:
                tmpUnit = "k" + tmpUnit;
                break;
            case 2:
                tmpUnit = "M" + tmpUnit;
                break;
        }
        this.BeginEnergy[pqFlag] = new DBDataModel(beginEnergy, String.valueOf(beginEnergy) + tmpUnit);
        this.EndEnergy[pqFlag] = new DBDataModel(endEnergy, String.valueOf(endEnergy) + tmpUnit);
        double defValue = Math.abs(endEnergy - beginEnergy);
        if (defValue < 0.0d) {
            defValue = 0.0d;
        }
        this.Difference[pqFlag] = parse(Double.valueOf(defValue), 4, tmpUnit);
        calculateError(pqFlag);
    }

    public void parseStandardEnergy(MeterBaseDevice Device, boolean all) {
        super.parseData(Device);
        if (all) {
            parseALLStandardEnergy(Device);
        } else {
            parseStandardEnergy(Device);
        }
    }

    private void parseStandardEnergy(MeterBaseDevice Device) {
        int pqFlag = Device.getPQFlagValue();
        ExplainedModel[] tmpPowerValues = {Device.getPValue()[3], Device.getQValue()[3], Device.getSValue()[3]};
        String[] tmpUnits = {"Wh", "varh", "VAh"};
        String tmpUnit = PdfObject.NOTHING;
        double multiple = 1.0d;
        switch (this.Unit) {
            case 1:
                tmpUnit = "k" + tmpUnit;
                break;
            case 2:
                tmpUnit = "M" + tmpUnit;
                multiple = 0.001d;
                break;
            default:
                multiple = 1000.0d;
                break;
        }
        this.StandardEnergy[pqFlag] = parse(Double.valueOf(Device.getEnergyValues()[0].getDoubleValue() * multiple), 4, String.valueOf(tmpUnit) + tmpUnits[pqFlag]);
        this.Power[pqFlag] = parse(tmpPowerValues[pqFlag]);
    }

    private void parseALLStandardEnergy(MeterBaseDevice Device) {
        Device.getPQFlagValue();
        ExplainedModel[] tmpPowerValues = {Device.getPValue()[3], Device.getQValue()[3], Device.getSValue()[3]};
        String[] tmpUnits = {"Wh", "varh", "VAh"};
        String tmpUnit = PdfObject.NOTHING;
        double multiple = 1.0d;
        switch (this.Unit) {
            case 1:
                tmpUnit = "k" + tmpUnit;
                break;
            case 2:
                tmpUnit = "M" + tmpUnit;
                multiple = 0.001d;
                break;
            default:
                multiple = 1000.0d;
                break;
        }
        ExplainedModel[] tmpValues = Device.getEnergyValues();
        for (int i = 0; i < 3; i++) {
            if (this.testFlag[i]) {
                this.StandardEnergy[i] = parse(Double.valueOf(Math.abs(tmpValues[i + 1].getDoubleValue()) * multiple), 4, String.valueOf(tmpUnit) + tmpUnits[i]);
                this.Power[i] = parse(tmpPowerValues[i]);
            }
        }
    }

    private void calculateError(int pqFlag) {
        double diffrenceValue = this.Difference[pqFlag].d_Value;
        double standardValue = (double) (((float) ((int) ((this.StandardEnergy[pqFlag].d_Value * 10000.0d) + 0.5d))) / 10000.0f);
        if (diffrenceValue <= 0.0d || standardValue <= 0.0d) {
            this.Error[pqFlag] = new DBDataModel();
            this.Result[pqFlag] = new DBDataModel(-1L, PdfObject.NOTHING);
            return;
        }
        int digit = ClouData.getInstance().getSettingBasic().ErrorDigit;
        this.Error[pqFlag] = parse(Double.valueOf((100.0d * (diffrenceValue - standardValue)) / standardValue), digit, "%");
        this.Result[pqFlag] = OtherUtils.getErrorResult(this.Accuracy[pqFlag], this.Error[pqFlag]);
    }

    @Override // com.clou.rs350.db.model.BasicMeasurement
    public boolean isEmpty() {
        boolean result = true;
        for (int i = 0; i < 3; i++) {
            if (-1 != this.Result[i].l_Value) {
                result = false;
            }
        }
        return result;
    }

    public void cleanError() {
        this.Result = new DBDataModel[]{new DBDataModel(-1L, PdfObject.NOTHING), new DBDataModel(-1L, PdfObject.NOTHING), new DBDataModel(-1L, PdfObject.NOTHING)};
        this.BeginEnergy = new DBDataModel[]{new DBDataModel(), new DBDataModel(), new DBDataModel()};
        this.EndEnergy = new DBDataModel[]{new DBDataModel(), new DBDataModel(), new DBDataModel()};
        this.Difference = new DBDataModel[]{new DBDataModel(), new DBDataModel(), new DBDataModel()};
        this.StandardEnergy = new DBDataModel[]{new DBDataModel(), new DBDataModel(), new DBDataModel()};
        this.Error = new DBDataModel[]{new DBDataModel(), new DBDataModel(), new DBDataModel()};
    }
}
