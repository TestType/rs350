package com.clou.rs350.db.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import com.clou.rs350.db.model.sequence.RegisterReadingData;
import com.clou.rs350.db.model.sequence.RegisterReadingItem;
import com.clou.rs350.manager.DatabaseManager;
import java.util.ArrayList;
import java.util.List;

public class RegisterReadingDao extends BaseTestDao {
    private static final String TABLE_NAME = "TestRegisterReading";

    private static final class TableColumns {
        public static final String Register = "Register";
        public static final String RegisterIndex = "RegisterIndex";
        public static final String RegisterReadingIndex = "RegisterReadingIndex";
        public static final String RegisterValue = "RegisterValue";

        private TableColumns() {
        }
    }

    public RegisterReadingDao(Context context) {
        super(context, TABLE_NAME);
    }

    public void createTable(SQLiteDatabase db) {
        this.m_TableName = TABLE_NAME;
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("create table if not exists ").append(this.m_TableName).append(" (");
        stringBuffer.append("intMyID").append(" integer primary key autoincrement,");
        stringBuffer.append(this.m_TableKey).append(" VARCHAR(50),");
        stringBuffer.append("CreateTime").append(" VARCHAR(50),");
        stringBuffer.append("IsRead").append(" integer default 0,");
        stringBuffer.append(TableColumns.RegisterReadingIndex).append(" integer,");
        stringBuffer.append(TableColumns.RegisterIndex).append(" integer,");
        stringBuffer.append(TableColumns.RegisterValue).append(" VARCHAR(50),");
        stringBuffer.append("Register").append(" VARCHAR(50));");
        db.execSQL(stringBuffer.toString());
    }

    @Override // com.clou.rs350.db.dao.BaseDao, com.clou.rs350.db.dao.BaseTestDao
    public void upgradeTable(SQLiteDatabase db, int oldVersion, int newVersion) {
        if (oldVersion <= 1) {
            createTable(db);
        }
        if (oldVersion <= 3 && oldVersion > 1) {
            db.execSQL("alter table RegisterReading rename to TestRegisterReading");
        }
    }

    @Override // com.clou.rs350.db.dao.BaseDao, com.clou.rs350.db.dao.BaseTestDao
    public synchronized boolean insertObject(Object Data, String Time) {
        boolean z;
        SQLiteDatabase db = DatabaseManager.getWriteableDatabase(this.m_Context);
        ContentValues values = new ContentValues();
        long status = 0;
        RegisterReadingData objects = (RegisterReadingData) Data;
        for (int i = 0; i < objects.Registers.size(); i++) {
            RegisterReadingItem object = objects.Registers.get(i);
            values.put(this.m_TableKey, objects.SerialNo.toString());
            values.put("CreateTime", Time);
            values.put(TableColumns.RegisterReadingIndex, Integer.valueOf(objects.Index));
            values.put(TableColumns.RegisterIndex, Integer.valueOf(i));
            values.put("Register", object.Register);
            values.put(TableColumns.RegisterValue, object.RegisterValue);
            status += db.insert(TABLE_NAME, null, values);
        }
        if (status != -1) {
            z = true;
        } else {
            z = false;
        }
        return z;
    }

    /* access modifiers changed from: protected */
    @Override // com.clou.rs350.db.dao.BaseDao, com.clou.rs350.db.dao.BaseTestDao
    public Object parseData(Cursor cursor) {
        RegisterReadingData values = new RegisterReadingData();
        values.SerialNo = cursor.getString(cursor.getColumnIndex(this.m_TableKey));
        do {
            RegisterReadingItem value = new RegisterReadingItem();
            value.Index = cursor.getInt(cursor.getColumnIndex(TableColumns.RegisterIndex));
            value.Register = cursor.getString(cursor.getColumnIndex("Register"));
            value.RegisterValue = cursor.getString(cursor.getColumnIndex(TableColumns.RegisterValue));
            values.Registers.add(value);
        } while (cursor.moveToNext());
        return values;
    }

    @Override // com.clou.rs350.db.dao.BaseDao
    public List<Object> queryDataListByKeyAndTime(String serialNo, String time) {
        Cursor cursor = DatabaseManager.getWriteableDatabase(this.m_Context).query(this.m_TableName, null, String.valueOf(this.m_TableKey) + " =? and " + "CreateTime" + " =?", new String[]{serialNo, time}, null, null, null);
        List<Object> Datas = new ArrayList<>();
        Datas.add(new RegisterReadingData());
        int Size = 0;
        while (cursor.moveToNext()) {
            if (cursor.getInt(cursor.getColumnIndex(TableColumns.RegisterReadingIndex)) != ((RegisterReadingData) Datas.get(Size)).Index) {
                Datas.add(new RegisterReadingData());
                Size++;
            }
            ((RegisterReadingData) Datas.get(Size)).SerialNo = cursor.getString(cursor.getColumnIndex(this.m_TableKey));
            ((RegisterReadingData) Datas.get(Size)).Index = cursor.getInt(cursor.getColumnIndex(TableColumns.RegisterReadingIndex));
            RegisterReadingItem value = new RegisterReadingItem();
            value.Index = cursor.getInt(cursor.getColumnIndex(TableColumns.RegisterIndex));
            value.Register = cursor.getString(cursor.getColumnIndex("Register"));
            value.RegisterValue = cursor.getString(cursor.getColumnIndex(TableColumns.RegisterValue));
            ((RegisterReadingData) Datas.get(Size)).Registers.add(value);
        }
        cursor.close();
        if (Datas.size() > 1 || ((RegisterReadingData) Datas.get(0)).Registers.size() > 0) {
            return Datas;
        }
        return null;
    }
}
