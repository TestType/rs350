package com.clou.rs350.db.model;

import com.itextpdf.text.pdf.PdfObject;

public class DigitalMeterType extends BasicMeasurement {
    public double Accuracy = 0.0d;
    public DigitalMeterDataType[] DataType = {new DigitalMeterDataType(0, 1, 1, 1.0f), new DigitalMeterDataType(0, 1, 1, 1.0f), new DigitalMeterDataType(0, 1, 1, 1.0f)};
    public int IsRead = 0;
    public String Type = PdfObject.NOTHING;
    public boolean isNew = true;

    public void CopyData(DigitalMeterType resource) {
        this.Type = resource.Type;
        this.DataType = resource.DataType;
    }
}
