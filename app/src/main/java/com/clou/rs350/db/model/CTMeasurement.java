package com.clou.rs350.db.model;

import com.clou.rs350.device.MeterBaseDevice;
import com.clou.rs350.device.model.ExplainedModel;
import com.clou.rs350.model.ClouData;
import com.clou.rs350.utils.OtherUtils;
import com.itextpdf.text.pdf.PdfObject;

public class CTMeasurement extends BasicMeasurement {
    public double Accuracy = 0.0d;
    public DBDataModel[] CurrentAnglePrimary = {new DBDataModel(), new DBDataModel(), new DBDataModel()};
    public DBDataModel[] CurrentAngleSecondary = {new DBDataModel(), new DBDataModel(), new DBDataModel()};
    public String[] CurrentRangePri = {PdfObject.NOTHING, PdfObject.NOTHING, PdfObject.NOTHING};
    public String[] CurrentRangeSec = {PdfObject.NOTHING, PdfObject.NOTHING, PdfObject.NOTHING};
    public DBDataModel[] CurrentValueError = {new DBDataModel(), new DBDataModel(), new DBDataModel()};
    public DBDataModel CurrentValueNominalPrimary = new DBDataModel();
    public DBDataModel CurrentValueNominalSecondary = new DBDataModel();
    public DBDataModel[] CurrentValuePhase = {new DBDataModel(), new DBDataModel(), new DBDataModel()};
    public DBDataModel[] CurrentValuePrimary = {new DBDataModel(), new DBDataModel(), new DBDataModel()};
    public DBDataModel[] CurrentValueRatio = {new DBDataModel(), new DBDataModel(), new DBDataModel()};
    public DBDataModel[] CurrentValueSecondary = {new DBDataModel(), new DBDataModel(), new DBDataModel()};
    public DBDataModel NormalRatio = new DBDataModel();
    public DBDataModel[] Result = {new DBDataModel(-1L, "---"), new DBDataModel(-1L, "---"), new DBDataModel(-1L, "---")};
    public String SerialNo = PdfObject.NOTHING;
    double[][] m_AngleLimit = {new double[]{900.0d, 480.0d, 300.0d, 300.0d}, new double[]{1800.0d, 900.0d, 600.0d, 600.0d}, new double[]{5400.0d, 2700.0d, 1800.0d, 1800.0d}, new double[]{10800.0d, 5400.0d, 3600.0d, 3600.0d}, new double[]{648000.0d, 648000.0d, 648000.0d, 648000.0d}, new double[]{648000.0d, 648000.0d, 648000.0d, 648000.0d}};
    double[][] m_RatioLimit = {new double[]{0.4d, 0.2d, 0.1d, 0.1d}, new double[]{0.75d, 0.25d, 0.2d, 0.2d}, new double[]{1.5d, 0.75d, 0.5d, 0.5d}, new double[]{3.0d, 1.5d, 1.0d, 1.0d}, new double[]{3.0d, 3.0d, 3.0d, 3.0d}, new double[]{5.0d, 5.0d, 5.0d, 5.0d}};

    public void parseData(MeterBaseDevice Device, int Index) {
        parseData(Device);
        ExplainedModel[] TmpValues = Device.getCTErrorValue();
        this.CurrentValuePrimary[Index] = parse(TmpValues[0]);
        this.CurrentValueSecondary[Index] = parse(TmpValues[1]);
        if (0.0d == this.CurrentValuePrimary[Index].d_Value || 0.0d == this.CurrentValueSecondary[Index].d_Value) {
            this.CurrentValueRatio[Index] = new DBDataModel();
        } else {
            double ratio = this.CurrentValuePrimary[Index].d_Value / this.CurrentValueSecondary[Index].d_Value;
            this.CurrentValueRatio[Index] = new DBDataModel(ratio, new StringBuilder(String.valueOf(ratio)).toString());
        }
        computerTestRatedValue(Index);
        this.CurrentValuePhase[Index] = calculatePhaseAngle(TmpValues[3].getDoubleValue());
        this.CurrentRangePri[Index] = this.CurrentRangeL1;
        this.CurrentRangeSec[Index] = this.CurrentRangeL2;
    }

    private DBDataModel calculatePhaseAngle(double angle) {
        if (angle > 180.0d) {
            angle -= 360.0d;
        }
        double angle2 = angle * 3600.0d;
        int deg = (int) angle2;
        int cen = Math.abs(deg % 60);
        int deg2 = deg / 60;
        return new DBDataModel(angle2, String.valueOf(deg2 / 60) + "°" + Math.abs(deg2 % 60) + "'" + cen + "\"");
    }

    public void setRatedValue(double ip, double is) {
        if (ip > 0.0d || is > 0.0d) {
            this.CurrentValueNominalPrimary = parse(Double.valueOf(ip), "A");
            this.CurrentValueNominalSecondary = parse(Double.valueOf(is), "A");
            this.NormalRatio = parseRound(Double.valueOf(ip / is));
            computerTestRatedValue();
        }
    }

    public void setAccuracy(double value) {
        this.Accuracy = value;
        computerTestRatedValue();
    }

    public void computerTestRatedValue() {
        computerTestRatedValue(0);
        computerTestRatedValue(1);
        computerTestRatedValue(2);
    }

    public int ratioGuessing() {
        int count = 0;
        double ratiosum = 0.0d;
        for (int i = 0; i < 3; i++) {
            if (0.0d != this.CurrentValueRatio[i].d_Value) {
                ratiosum += this.CurrentValueRatio[i].d_Value;
                count++;
            }
        }
        if (count == 0) {
            return 1;
        }
        if (0.0d == this.CurrentValueNominalSecondary.d_Value) {
            return 2;
        }
        this.CurrentValueNominalPrimary = parse(Double.valueOf(((double) Math.round(ratiosum / ((double) count))) * this.CurrentValueNominalSecondary.d_Value), "A");
        setRatedValue(this.CurrentValueNominalPrimary.d_Value, this.CurrentValueNominalSecondary.d_Value);
        return 0;
    }

    public void computerTestRatedValue(int index) {
        if (this.NormalRatio.d_Value == 0.0d || this.CurrentValuePrimary[index].d_Value == 0.0d) {
            this.CurrentValueError[index] = new DBDataModel();
            this.Result[index] = new DBDataModel(-1L, "---");
            return;
        }
        this.CurrentValueError[index] = parse(Double.valueOf((((this.CurrentValueSecondary[index].d_Value * this.NormalRatio.d_Value) - this.CurrentValuePrimary[index].d_Value) / this.CurrentValuePrimary[index].d_Value) * 100.0d), ClouData.getInstance().getSettingBasic().ErrorDigit, "%");
        this.Result[index] = getResult(index);
    }

    private DBDataModel getResult(int index) {
        double[] ratioLimits;
        double[] angleLimits;
        if (0.0d == this.Accuracy) {
            return new DBDataModel(-1L, "---");
        }
        if (this.Accuracy <= 0.1d) {
            ratioLimits = this.m_RatioLimit[0];
            angleLimits = this.m_AngleLimit[0];
        } else if (this.Accuracy <= 0.2d) {
            ratioLimits = this.m_RatioLimit[1];
            angleLimits = this.m_AngleLimit[1];
        } else if (this.Accuracy <= 0.5d) {
            ratioLimits = this.m_RatioLimit[2];
            angleLimits = this.m_AngleLimit[2];
        } else if (this.Accuracy <= 1.0d) {
            ratioLimits = this.m_RatioLimit[3];
            angleLimits = this.m_AngleLimit[3];
        } else if (this.Accuracy <= 3.0d) {
            ratioLimits = this.m_RatioLimit[4];
            angleLimits = this.m_AngleLimit[4];
        } else if (this.Accuracy > 5.0d) {
            return new DBDataModel(-1L, PdfObject.NOTHING);
        } else {
            ratioLimits = this.m_RatioLimit[5];
            angleLimits = this.m_AngleLimit[5];
        }
        double percentage = this.CurrentValueSecondary[index].d_Value / this.CurrentValueNominalSecondary.d_Value;
        double ratioLimit = 0.0d;
        double angleLimit = 0.0d;
        if (percentage <= 0.05d) {
            ratioLimit = ratioLimits[0];
            angleLimit = angleLimits[0];
        } else if (percentage <= 0.2d) {
            ratioLimit = ratioLimits[1];
            angleLimit = angleLimits[1];
        } else if (percentage <= 1.0d) {
            ratioLimit = ratioLimits[2];
            angleLimit = angleLimits[2];
        } else if (percentage <= 1.2d) {
            ratioLimit = ratioLimits[3];
            angleLimit = angleLimits[3];
        }
        int result = (Math.abs(this.CurrentValueError[index].d_Value) >= ratioLimit || Math.abs(this.CurrentValuePhase[index].d_Value) >= angleLimit) ? 1 : 0;
        return new DBDataModel((long) result, OtherUtils.getErrorResult(result));
    }

    @Override // com.clou.rs350.db.model.BasicMeasurement
    public boolean isEmpty() {
        boolean result = true;
        for (int i = 0; i < 3; i++) {
            if (-1 != this.Result[i].l_Value) {
                result = false;
            }
        }
        return result;
    }

    public void cleanData() {
        this.CurrentValuePrimary = new DBDataModel[]{new DBDataModel(), new DBDataModel(), new DBDataModel()};
        this.CurrentValueSecondary = new DBDataModel[]{new DBDataModel(), new DBDataModel(), new DBDataModel()};
        this.CurrentAnglePrimary = new DBDataModel[]{new DBDataModel(), new DBDataModel(), new DBDataModel()};
        this.CurrentAngleSecondary = new DBDataModel[]{new DBDataModel(), new DBDataModel(), new DBDataModel()};
        this.CurrentValueRatio = new DBDataModel[]{new DBDataModel(), new DBDataModel(), new DBDataModel()};
        this.CurrentValuePhase = new DBDataModel[]{new DBDataModel(), new DBDataModel(), new DBDataModel()};
        this.CurrentValueError = new DBDataModel[]{new DBDataModel(), new DBDataModel(), new DBDataModel()};
        this.Result = new DBDataModel[]{new DBDataModel(-1L, "---"), new DBDataModel(-1L, "---"), new DBDataModel(-1L, "---")};
    }
}
