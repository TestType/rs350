package com.clou.rs350.db.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import com.clou.rs350.db.model.sequence.SealData;
import com.clou.rs350.db.model.sequence.SealItem;
import com.clou.rs350.manager.DatabaseManager;
import java.util.ArrayList;
import java.util.List;

public class SealDao extends BaseTestDao {
    private static final String TABLE_NAME = "TestSeal";

    private static final class TableColumns {
        public static final String SealIndex = "SealIndex";
        public static final String SealLocation = "SealLocation";
        public static final String SealSr = "SealSr";
        public static final String SealsIndex = "SealsIndex";

        private TableColumns() {
        }
    }

    public SealDao(Context context) {
        super(context, TABLE_NAME);
    }

    public void createTable(SQLiteDatabase db) {
        this.m_TableName = TABLE_NAME;
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("create table if not exists ").append(this.m_TableName).append(" (");
        stringBuffer.append("intMyID").append(" integer primary key autoincrement,");
        stringBuffer.append(this.m_TableKey).append(" VARCHAR(50),");
        stringBuffer.append("CreateTime").append(" VARCHAR(50),");
        stringBuffer.append("IsRead").append(" integer default 0,");
        stringBuffer.append(TableColumns.SealsIndex).append(" integer,");
        stringBuffer.append(TableColumns.SealIndex).append(" integer,");
        stringBuffer.append(TableColumns.SealSr).append(" VARCHAR(50),");
        stringBuffer.append("SealLocation").append(" VARCHAR(50));");
        db.execSQL(stringBuffer.toString());
    }

    @Override // com.clou.rs350.db.dao.BaseDao, com.clou.rs350.db.dao.BaseTestDao
    public void upgradeTable(SQLiteDatabase db, int oldVersion, int newVersion) {
        if (oldVersion <= 3) {
            db.execSQL("alter table Seal rename to TestSeal");
        }
    }

    @Override // com.clou.rs350.db.dao.BaseDao, com.clou.rs350.db.dao.BaseTestDao
    public synchronized boolean insertObject(Object Data, String Time) {
        boolean z;
        SQLiteDatabase db = DatabaseManager.getWriteableDatabase(this.m_Context);
        ContentValues values = new ContentValues();
        long status = 0;
        SealData objects = (SealData) Data;
        for (int i = 0; i < objects.Seals.size(); i++) {
            SealItem object = objects.Seals.get(i);
            values.put(this.m_TableKey, objects.SerialNo.toString());
            values.put("CreateTime", Time);
            values.put(TableColumns.SealsIndex, Integer.valueOf(objects.Index));
            values.put(TableColumns.SealIndex, Integer.valueOf(i));
            values.put("SealLocation", object.SealLocation);
            values.put(TableColumns.SealSr, object.SealSr);
            status += db.insert(TABLE_NAME, null, values);
        }
        if (status != -1) {
            z = true;
        } else {
            z = false;
        }
        return z;
    }

    /* access modifiers changed from: protected */
    @Override // com.clou.rs350.db.dao.BaseDao, com.clou.rs350.db.dao.BaseTestDao
    public Object parseData(Cursor cursor) {
        SealData values = new SealData();
        values.SerialNo = cursor.getString(cursor.getColumnIndex(this.m_TableKey));
        do {
            SealItem value = new SealItem();
            value.Index = cursor.getInt(cursor.getColumnIndex(TableColumns.SealIndex));
            value.SealLocation = cursor.getString(cursor.getColumnIndex("SealLocation"));
            value.SealSr = cursor.getString(cursor.getColumnIndex(TableColumns.SealSr));
            values.Seals.add(value);
        } while (cursor.moveToNext());
        return values;
    }

    @Override // com.clou.rs350.db.dao.BaseDao
    public List<Object> queryDataListByKeyAndTime(String serialNo, String time) {
        Cursor cursor = DatabaseManager.getWriteableDatabase(this.m_Context).query(this.m_TableName, null, String.valueOf(this.m_TableKey) + " =? and " + "CreateTime" + " =?", new String[]{serialNo, time}, null, null, null);
        List<Object> Datas = new ArrayList<>();
        Datas.add(new SealData());
        int Size = 0;
        while (cursor.moveToNext()) {
            if (cursor.getInt(cursor.getColumnIndex(TableColumns.SealsIndex)) != ((SealData) Datas.get(Size)).Index) {
                Datas.add(new SealData());
                Size++;
            }
            ((SealData) Datas.get(Size)).SerialNo = cursor.getString(cursor.getColumnIndex(this.m_TableKey));
            ((SealData) Datas.get(Size)).Index = cursor.getInt(cursor.getColumnIndex(TableColumns.SealsIndex));
            SealItem value = new SealItem();
            value.Index = cursor.getInt(cursor.getColumnIndex(TableColumns.SealIndex));
            value.SealLocation = cursor.getString(cursor.getColumnIndex("SealLocation"));
            value.SealSr = cursor.getString(cursor.getColumnIndex(TableColumns.SealSr));
            ((SealData) Datas.get(Size)).Seals.add(value);
        }
        cursor.close();
        if (Datas.size() > 1 || ((SealData) Datas.get(0)).Seals.size() > 0) {
            return Datas;
        }
        return null;
    }
}
