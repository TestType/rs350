package com.clou.rs350.db.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import com.clou.rs350.db.dao.BaseTestDao;
import com.clou.rs350.db.model.DigitalMeterTest;
import com.clou.rs350.manager.DatabaseManager;
import com.clou.rs350.utils.OtherUtils;

public class DigitalMeterTestDao extends BaseTestDao {
    public static final String TABLE_NAME = "TestDigitalMeterTest";

    private static final class TableColumns {
        public static final String ActivePowerL1 = "ActivePowerL1";
        public static final String ActivePowerL2 = "ActivePowerL2";
        public static final String ActivePowerL3 = "ActivePowerL3";
        public static final String ActivePowerTotal = "ActivePowerTotal";
        public static final String ApparentPowerL1 = "ApparentPowerL1";
        public static final String ApparentPowerL2 = "ApparentPowerL2";
        public static final String ApparentPowerL3 = "ApparentPowerL3";
        public static final String ApparentPowerTotal = "ApparentPowerTotal";
        public static final String CurrentAngleL1 = "CurrentAngleL1";
        public static final String CurrentAngleL2 = "CurrentAngleL2";
        public static final String CurrentAngleL3 = "CurrentAngleL3";
        public static final String CurrentValueL1 = "CurrentValueL1";
        public static final String CurrentValueL2 = "CurrentValueL2";
        public static final String CurrentValueL3 = "CurrentValueL3";
        public static final String DeviceActivePowerTotal = "DeviceActivePowerTotal";
        public static final String DeviceApparentPowerTotal = "DeviceApparentPowerTotal";
        public static final String DeviceReactivePowerTotal = "DeviceReactivePowerTotal";
        public static final String ErrorCount = "ErrorCount";
        public static final String Frequency = "Frequency";
        public static final String MeterIndex = "MeterIndex";
        public static final String PError1 = "PError1";
        public static final String PError2 = "PError2";
        public static final String PError3 = "PError3";
        public static final String PError4 = "PError4";
        public static final String PError5 = "PError5";
        public static final String PErrorAverage = "PErrorAverage";
        public static final String PErrorStandard = "PErrorStandard";
        public static final String PowerFactorL1 = "PowerFactorL1";
        public static final String PowerFactorL2 = "PowerFactorL2";
        public static final String PowerFactorL3 = "PowerFactorL3";
        public static final String PowerFactorTotal = "PowerFactorTotal";
        public static final String QError1 = "QError1";
        public static final String QError2 = "QError2";
        public static final String QError3 = "QError3";
        public static final String QError4 = "QError4";
        public static final String QError5 = "QError5";
        public static final String QErrorAverage = "QErrorAverage";
        public static final String QErrorStandard = "QErrorStandard";
        public static final String ReactivePowerL1 = "ReactivePowerL1";
        public static final String ReactivePowerL2 = "ReactivePowerL2";
        public static final String ReactivePowerL3 = "ReactivePowerL3";
        public static final String ReactivePowerTotal = "ReactivePowerTotal";
        public static final String Result = "Result";
        public static final String RunTime = "RunTime";
        public static final String SError1 = "SError1";
        public static final String SError2 = "SError2";
        public static final String SError3 = "SError3";
        public static final String SError4 = "SError4";
        public static final String SError5 = "SError5";
        public static final String SErrorAverage = "SErrorAverage";
        public static final String SErrorStandard = "SErrorStandard";
        public static final String VoltageAngleL1 = "VoltageAngleL1";
        public static final String VoltageAngleL2 = "VoltageAngleL2";
        public static final String VoltageAngleL3 = "VoltageAngleL3";
        public static final String VoltageCurrentAngleL1 = "VoltageCurrentAngleL1";
        public static final String VoltageCurrentAngleL2 = "VoltageCurrentAngleL2";
        public static final String VoltageCurrentAngleL3 = "VoltageCurrentAngleL3";
        public static final String VoltageValueL1 = "VoltageValueL1";
        public static final String VoltageValueL2 = "VoltageValueL2";
        public static final String VoltageValueL3 = "VoltageValueL3";
        public static final String VoltageVectorAngleL1 = "VoltageVectorAngleL1";
        public static final String VoltageVectorAngleL2 = "VoltageVectorAngleL2";
        public static final String VoltageVectorAngleL3 = "VoltageVectorAngleL3";
        public static final String VoltageVectorValueL1 = "VoltageVectorValueL1";
        public static final String VoltageVectorValueL2 = "VoltageVectorValueL2";
        public static final String VoltageVectorValueL3 = "VoltageVectorValueL3";

        private TableColumns() {
        }
    }

    public DigitalMeterTestDao(Context context) {
        super(context, TABLE_NAME);
    }

    public void createTable(SQLiteDatabase db) {
        this.m_TableName = TABLE_NAME;
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append(getCreatTableString());
        stringBuffer.append(SameColumns.MeterSerialNo).append(" VARCHAR(50),");
        stringBuffer.append("MeterIndex").append(" VARCHAR(5),");
        stringBuffer.append("VoltageValueL1").append(" VARCHAR(20),");
        stringBuffer.append("VoltageValueL2").append(" VARCHAR(20),");
        stringBuffer.append("VoltageValueL3").append(" VARCHAR(20),");
        stringBuffer.append("VoltageVectorValueL1").append(" VARCHAR(20),");
        stringBuffer.append("VoltageVectorValueL2").append(" VARCHAR(20),");
        stringBuffer.append("VoltageVectorValueL3").append(" VARCHAR(20),");
        stringBuffer.append("CurrentValueL1").append(" VARCHAR(20),");
        stringBuffer.append("CurrentValueL2").append(" VARCHAR(20),");
        stringBuffer.append("CurrentValueL3").append(" VARCHAR(20),");
        stringBuffer.append("VoltageAngleL1").append(" VARCHAR(20),");
        stringBuffer.append("VoltageAngleL2").append(" VARCHAR(20),");
        stringBuffer.append("VoltageAngleL3").append(" VARCHAR(20),");
        stringBuffer.append("VoltageVectorAngleL1").append(" VARCHAR(20),");
        stringBuffer.append("VoltageVectorAngleL2").append(" VARCHAR(20),");
        stringBuffer.append("VoltageVectorAngleL3").append(" VARCHAR(20),");
        stringBuffer.append("CurrentAngleL1").append(" VARCHAR(20),");
        stringBuffer.append("CurrentAngleL2").append(" VARCHAR(20),");
        stringBuffer.append("CurrentAngleL3").append(" VARCHAR(20),");
        stringBuffer.append("Frequency").append(" VARCHAR(20),");
        stringBuffer.append("VoltageCurrentAngleL1").append(" VARCHAR(20),");
        stringBuffer.append("VoltageCurrentAngleL2").append(" VARCHAR(20),");
        stringBuffer.append("VoltageCurrentAngleL3").append(" VARCHAR(20),");
        stringBuffer.append("RunTime").append(" VARCHAR(50),");
        stringBuffer.append("ErrorCount").append(" VARCHAR(10),");
        stringBuffer.append(TableColumns.PError1).append(" VARCHAR(20),");
        stringBuffer.append(TableColumns.PError2).append(" VARCHAR(20),");
        stringBuffer.append(TableColumns.PError3).append(" VARCHAR(20),");
        stringBuffer.append(TableColumns.PError4).append(" VARCHAR(20),");
        stringBuffer.append(TableColumns.PError5).append(" VARCHAR(20),");
        stringBuffer.append(TableColumns.PErrorAverage).append(" VARCHAR(20),");
        stringBuffer.append(TableColumns.PErrorStandard).append(" VARCHAR(20),");
        stringBuffer.append(TableColumns.QError1).append(" VARCHAR(20),");
        stringBuffer.append(TableColumns.QError2).append(" VARCHAR(20),");
        stringBuffer.append(TableColumns.QError3).append(" VARCHAR(20),");
        stringBuffer.append(TableColumns.QError4).append(" VARCHAR(20),");
        stringBuffer.append(TableColumns.QError5).append(" VARCHAR(20),");
        stringBuffer.append(TableColumns.QErrorAverage).append(" VARCHAR(20),");
        stringBuffer.append(TableColumns.QErrorStandard).append(" VARCHAR(20),");
        stringBuffer.append(TableColumns.SError1).append(" VARCHAR(20),");
        stringBuffer.append(TableColumns.SError2).append(" VARCHAR(20),");
        stringBuffer.append(TableColumns.SError3).append(" VARCHAR(20),");
        stringBuffer.append(TableColumns.SError4).append(" VARCHAR(20),");
        stringBuffer.append(TableColumns.SError5).append(" VARCHAR(20),");
        stringBuffer.append(TableColumns.SErrorAverage).append(" VARCHAR(20),");
        stringBuffer.append(TableColumns.SErrorStandard).append(" VARCHAR(20),");
        stringBuffer.append("Result").append(" VARCHAR(20),");
        stringBuffer.append(TableColumns.DeviceActivePowerTotal).append(" VARCHAR(20),");
        stringBuffer.append(TableColumns.DeviceReactivePowerTotal).append(" VARCHAR(20),");
        stringBuffer.append(TableColumns.DeviceApparentPowerTotal).append(" VARCHAR(20),");
        stringBuffer.append("ActivePowerL1").append(" VARCHAR(20),");
        stringBuffer.append("ActivePowerL2").append(" VARCHAR(20),");
        stringBuffer.append("ActivePowerL3").append(" VARCHAR(20),");
        stringBuffer.append("ActivePowerTotal").append(" VARCHAR(20),");
        stringBuffer.append("ReactivePowerL1").append(" VARCHAR(20),");
        stringBuffer.append("ReactivePowerL2").append(" VARCHAR(20),");
        stringBuffer.append("ReactivePowerL3").append(" VARCHAR(20),");
        stringBuffer.append("ReactivePowerTotal").append(" VARCHAR(20),");
        stringBuffer.append("ApparentPowerL1").append(" VARCHAR(20),");
        stringBuffer.append("ApparentPowerL2").append(" VARCHAR(20),");
        stringBuffer.append("ApparentPowerL3").append(" VARCHAR(20),");
        stringBuffer.append("ApparentPowerTotal").append(" VARCHAR(20),");
        stringBuffer.append("PowerFactorL1").append(" VARCHAR(10),");
        stringBuffer.append("PowerFactorL2").append(" VARCHAR(10),");
        stringBuffer.append("PowerFactorL3").append(" VARCHAR(10),");
        stringBuffer.append("PowerFactorTotal").append(" VARCHAR(10));");
        db.execSQL(stringBuffer.toString());
    }

    @Override // com.clou.rs350.db.dao.BaseDao, com.clou.rs350.db.dao.BaseTestDao
    public void upgradeTable(SQLiteDatabase db, int oldVersion, int newVersion) {
        if (oldVersion < 15) {
            createTable(db);
        }
        super.upgradeTable(db, oldVersion, newVersion);
    }

    @Override // com.clou.rs350.db.dao.BaseDao, com.clou.rs350.db.dao.BaseTestDao
    public synchronized boolean insertObject(Object Data, String Time) {
        boolean z = true;
        synchronized (this) {
            SQLiteDatabase db = DatabaseManager.getWriteableDatabase(this.m_Context);
            ContentValues values = new ContentValues();
            DigitalMeterTest object = (DigitalMeterTest) Data;
            values.put(this.m_TableKey, object.CustomerSerialNo);
            values.put(SameColumns.MeterSerialNo, object.MeterSerialNo);
            values.put("CreateTime", Time);
            values.put("MeterIndex", object.MeterIndex);
            values.put(SameColumns.WiringType, object.WiringValue);
            values.put("IntWiringType", Integer.valueOf(object.Wiring));
            values.put(SameColumns.ConnectionMode, object.ConnectionType);
            values.put(SameColumns.VoltageRangeL1, object.VoltageRangeL1);
            values.put(SameColumns.VoltageRangeL2, object.VoltageRangeL2);
            values.put(SameColumns.VoltageRangeL3, object.VoltageRangeL3);
            values.put(SameColumns.CurrentRangeL1, object.CurrentRangeL1);
            values.put(SameColumns.CurrentRangeL2, object.CurrentRangeL2);
            values.put(SameColumns.CurrentRangeL3, object.CurrentRangeL3);
            values.put("VoltageValueL1", object.VoltageValueL1Show.s_Value);
            values.put("VoltageValueL2", object.VoltageValueL2Show.s_Value);
            values.put("VoltageValueL3", object.VoltageValueL3Show.s_Value);
            values.put("VoltageVectorValueL1", object.VoltageValueL1Vector.s_Value);
            values.put("VoltageVectorValueL2", object.VoltageValueL2Vector.s_Value);
            values.put("VoltageVectorValueL3", object.VoltageValueL3Vector.s_Value);
            values.put("CurrentValueL1", object.CurrentValueL1.s_Value);
            values.put("CurrentValueL2", object.CurrentValueL2.s_Value);
            values.put("CurrentValueL3", object.CurrentValueL3.s_Value);
            values.put("VoltageAngleL1", object.VoltageAngleL1Show.s_Value);
            values.put("VoltageAngleL2", object.VoltageAngleL2Show.s_Value);
            values.put("VoltageAngleL3", object.VoltageAngleL3Show.s_Value);
            values.put("VoltageVectorAngleL1", object.VoltageAngleL1Vector.s_Value);
            values.put("VoltageVectorAngleL2", object.VoltageAngleL2Vector.s_Value);
            values.put("VoltageVectorAngleL3", object.VoltageAngleL3Vector.s_Value);
            values.put("CurrentAngleL1", object.CurrentAngleL1.s_Value);
            values.put("CurrentAngleL2", object.CurrentAngleL2.s_Value);
            values.put("CurrentAngleL3", object.CurrentAngleL3.s_Value);
            values.put("Frequency", object.Frequency.s_Value);
            values.put("VoltageCurrentAngleL1", object.VoltageCurrentAngleL1Show.s_Value);
            values.put("VoltageCurrentAngleL2", object.VoltageCurrentAngleL2Show.s_Value);
            values.put("VoltageCurrentAngleL3", object.VoltageCurrentAngleL3Show.s_Value);
            values.put("ActivePowerL1", object.ActivePowerL1.s_Value);
            values.put("ActivePowerL2", object.ActivePowerL2.s_Value);
            values.put("ActivePowerL3", object.ActivePowerL3.s_Value);
            values.put("ActivePowerTotal", object.ActivePowerTotal.s_Value);
            values.put("ReactivePowerL1", object.ReactivePowerL1.s_Value);
            values.put("ReactivePowerL2", object.ReactivePowerL2.s_Value);
            values.put("ReactivePowerL3", object.ReactivePowerL3.s_Value);
            values.put("ReactivePowerTotal", object.ReactivePowerTotal.s_Value);
            values.put("ApparentPowerL1", object.ApparentPowerL1.s_Value);
            values.put("ApparentPowerL2", object.ApparentPowerL2.s_Value);
            values.put("ApparentPowerL3", object.ApparentPowerL3.s_Value);
            values.put("ApparentPowerTotal", object.ApparentPowerTotal.s_Value);
            values.put("PowerFactorL1", object.PowerFactorL1.toString());
            values.put("PowerFactorL2", object.PowerFactorL2.toString());
            values.put("PowerFactorL3", object.PowerFactorL3.toString());
            values.put("PowerFactorTotal", object.PowerFactorTotal.s_Value);
            values.put("RunTime", object.RunTime);
            values.put("ErrorCount", Integer.valueOf(object.ErrorCount));
            values.put(TableColumns.PError1, object.getError1(0));
            values.put(TableColumns.PError2, object.getError2(0));
            values.put(TableColumns.PError3, object.getError3(0));
            values.put(TableColumns.PError4, object.getError4(0));
            values.put(TableColumns.PError5, object.getError5(0));
            values.put(TableColumns.PErrorAverage, object.ErrorAverage[0].s_Value);
            values.put(TableColumns.PErrorStandard, object.ErrorStandard[0].s_Value);
            values.put(TableColumns.QError1, object.getError1(1));
            values.put(TableColumns.QError2, object.getError2(1));
            values.put(TableColumns.QError3, object.getError3(1));
            values.put(TableColumns.QError4, object.getError4(1));
            values.put(TableColumns.QError5, object.getError5(1));
            values.put(TableColumns.QErrorAverage, object.ErrorAverage[1].s_Value);
            values.put(TableColumns.QErrorStandard, object.ErrorStandard[1].s_Value);
            values.put(TableColumns.SError1, object.getError1(2));
            values.put(TableColumns.SError2, object.getError2(2));
            values.put(TableColumns.SError3, object.getError3(2));
            values.put(TableColumns.SError4, object.getError4(2));
            values.put(TableColumns.SError5, object.getError5(2));
            values.put(TableColumns.SErrorAverage, object.ErrorAverage[2].s_Value);
            values.put(TableColumns.SErrorStandard, object.ErrorStandard[2].s_Value);
            values.put("Result", object.Result.s_Value);
            if (db.insert(TABLE_NAME, null, values) == -1) {
                z = false;
            }
        }
        return z;
    }

    /* access modifiers changed from: protected */
    @Override // com.clou.rs350.db.dao.BaseDao, com.clou.rs350.db.dao.BaseTestDao
    public Object parseData(Cursor cursor) {
        DigitalMeterTest value = new DigitalMeterTest();
        value.CustomerSerialNo = parseString(cursor.getString(cursor.getColumnIndex(this.m_TableKey)));
        value.MeterSerialNo = parseString(cursor.getString(cursor.getColumnIndex(SameColumns.MeterSerialNo)));
        value.MeterIndex = parseString(cursor.getString(cursor.getColumnIndex("MeterIndex")));
        value.WiringValue = parseString(cursor.getString(cursor.getColumnIndex(SameColumns.WiringType)));
        value.Wiring = cursor.getInt(cursor.getColumnIndex("IntWiringType"));
        value.ConnectionType = parseString(cursor.getString(cursor.getColumnIndex(SameColumns.ConnectionMode)));
        value.VoltageRangeL1 = parseString(cursor.getString(cursor.getColumnIndex(SameColumns.VoltageRangeL1)));
        value.VoltageRangeL2 = parseString(cursor.getString(cursor.getColumnIndex(SameColumns.VoltageRangeL2)));
        value.VoltageRangeL3 = parseString(cursor.getString(cursor.getColumnIndex(SameColumns.VoltageRangeL3)));
        value.CurrentRangeL1 = parseString(cursor.getString(cursor.getColumnIndex(SameColumns.CurrentRangeL1)));
        value.CurrentRangeL2 = parseString(cursor.getString(cursor.getColumnIndex(SameColumns.CurrentRangeL2)));
        value.CurrentRangeL3 = parseString(cursor.getString(cursor.getColumnIndex(SameColumns.CurrentRangeL3)));
        value.VoltageValueL1Show.s_Value = parseString(cursor.getString(cursor.getColumnIndex("VoltageValueL1")));
        value.VoltageValueL2Show.s_Value = parseString(cursor.getString(cursor.getColumnIndex("VoltageValueL2")));
        value.VoltageValueL3Show.s_Value = parseString(cursor.getString(cursor.getColumnIndex("VoltageValueL3")));
        value.CurrentValueL1.s_Value = parseString(cursor.getString(cursor.getColumnIndex("CurrentValueL1")));
        value.CurrentValueL2.s_Value = parseString(cursor.getString(cursor.getColumnIndex("CurrentValueL2")));
        value.CurrentValueL3.s_Value = parseString(cursor.getString(cursor.getColumnIndex("CurrentValueL3")));
        value.ActivePowerL1.s_Value = parseString(cursor.getString(cursor.getColumnIndex("ActivePowerL1")));
        value.ActivePowerL2.s_Value = parseString(cursor.getString(cursor.getColumnIndex("ActivePowerL2")));
        value.ActivePowerL3.s_Value = parseString(cursor.getString(cursor.getColumnIndex("ActivePowerL3")));
        value.ActivePowerTotal.s_Value = parseString(cursor.getString(cursor.getColumnIndex("ActivePowerTotal")));
        value.ReactivePowerL1.s_Value = parseString(cursor.getString(cursor.getColumnIndex("ReactivePowerL1")));
        value.ReactivePowerL2.s_Value = parseString(cursor.getString(cursor.getColumnIndex("ReactivePowerL2")));
        value.ReactivePowerL3.s_Value = parseString(cursor.getString(cursor.getColumnIndex("ReactivePowerL3")));
        value.ReactivePowerTotal.s_Value = parseString(cursor.getString(cursor.getColumnIndex("ReactivePowerTotal")));
        value.ApparentPowerL1.s_Value = parseString(cursor.getString(cursor.getColumnIndex("ApparentPowerL1")));
        value.ApparentPowerL2.s_Value = parseString(cursor.getString(cursor.getColumnIndex("ApparentPowerL2")));
        value.ApparentPowerL3.s_Value = parseString(cursor.getString(cursor.getColumnIndex("ApparentPowerL3")));
        value.ApparentPowerTotal.s_Value = parseString(cursor.getString(cursor.getColumnIndex("ApparentPowerTotal")));
        value.PowerFactorL1.s_Value = parseString(cursor.getString(cursor.getColumnIndex("PowerFactorL1")));
        value.PowerFactorL2.s_Value = parseString(cursor.getString(cursor.getColumnIndex("PowerFactorL2")));
        value.PowerFactorL3.s_Value = parseString(cursor.getString(cursor.getColumnIndex("PowerFactorL3")));
        value.PowerFactorTotal.s_Value = parseString(cursor.getString(cursor.getColumnIndex("PowerFactorTotal")));
        value.ErrorCount = OtherUtils.parseInt(parseString(cursor.getString(cursor.getColumnIndex("ErrorCount"))));
        value.hasErrorCount = value.ErrorCount;
        value.RunTime = parseString(cursor.getString(cursor.getColumnIndex("RunTime")));
        value.Error1[0].s_Value = parseString(cursor.getString(cursor.getColumnIndex(TableColumns.PError1)));
        value.Error2[0].s_Value = parseString(cursor.getString(cursor.getColumnIndex(TableColumns.PError2)));
        value.Error3[0].s_Value = parseString(cursor.getString(cursor.getColumnIndex(TableColumns.PError3)));
        value.Error4[0].s_Value = parseString(cursor.getString(cursor.getColumnIndex(TableColumns.PError4)));
        value.Error5[0].s_Value = parseString(cursor.getString(cursor.getColumnIndex(TableColumns.PError5)));
        value.ErrorAverage[0].s_Value = parseString(cursor.getString(cursor.getColumnIndex(TableColumns.PErrorAverage)));
        value.ErrorStandard[0].s_Value = parseString(cursor.getString(cursor.getColumnIndex(TableColumns.PErrorStandard)));
        value.Error1[1].s_Value = parseString(cursor.getString(cursor.getColumnIndex(TableColumns.QError1)));
        value.Error2[1].s_Value = parseString(cursor.getString(cursor.getColumnIndex(TableColumns.QError2)));
        value.Error3[1].s_Value = parseString(cursor.getString(cursor.getColumnIndex(TableColumns.QError3)));
        value.Error4[1].s_Value = parseString(cursor.getString(cursor.getColumnIndex(TableColumns.QError4)));
        value.Error5[1].s_Value = parseString(cursor.getString(cursor.getColumnIndex(TableColumns.QError5)));
        value.ErrorAverage[1].s_Value = parseString(cursor.getString(cursor.getColumnIndex(TableColumns.QErrorAverage)));
        value.ErrorStandard[1].s_Value = parseString(cursor.getString(cursor.getColumnIndex(TableColumns.QErrorStandard)));
        value.Error1[2].s_Value = parseString(cursor.getString(cursor.getColumnIndex(TableColumns.SError1)));
        value.Error2[2].s_Value = parseString(cursor.getString(cursor.getColumnIndex(TableColumns.SError2)));
        value.Error3[2].s_Value = parseString(cursor.getString(cursor.getColumnIndex(TableColumns.SError3)));
        value.Error4[2].s_Value = parseString(cursor.getString(cursor.getColumnIndex(TableColumns.SError4)));
        value.Error5[2].s_Value = parseString(cursor.getString(cursor.getColumnIndex(TableColumns.SError5)));
        value.ErrorAverage[2].s_Value = parseString(cursor.getString(cursor.getColumnIndex(TableColumns.SErrorAverage)));
        value.ErrorStandard[2].s_Value = parseString(cursor.getString(cursor.getColumnIndex(TableColumns.SErrorStandard)));
        value.Result.s_Value = parseString(cursor.getString(cursor.getColumnIndex("Result")));
        return value;
    }
}
