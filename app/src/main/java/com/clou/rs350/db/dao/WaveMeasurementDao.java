package com.clou.rs350.db.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import com.clou.rs350.db.model.WaveMeasurement;
import com.clou.rs350.db.model.WaveValue;
import com.clou.rs350.manager.DatabaseManager;
import com.clou.rs350.utils.DataProcessing;

public class WaveMeasurementDao extends BaseTestDao {
    private static final String TABLE_NAME = "TestWaveformMeasurement";

    public static final class TableColumns {
        public static final String CurrentValueL1 = "CurrentValueL1";
        public static final String CurrentValueL2 = "CurrentValueL2";
        public static final String CurrentValueL3 = "CurrentValueL3";
        public static final String CurrentWaveformL1 = "CurrentWaveformL1";
        public static final String CurrentWaveformL1Max = "CurrentWaveformL1Max";
        public static final String CurrentWaveformL1Min = "CurrentWaveformL1Min";
        public static final String CurrentWaveformL2 = "CurrentWaveformL2";
        public static final String CurrentWaveformL2Max = "CurrentWaveformL2Max";
        public static final String CurrentWaveformL2Min = "CurrentWaveformL2Min";
        public static final String CurrentWaveformL3 = "CurrentWaveformL3";
        public static final String CurrentWaveformL3Max = "CurrentWaveformL3Max";
        public static final String CurrentWaveformL3Min = "CurrentWaveformL3Min";
        public static final String PWaveformL1 = "PWaveformL1";
        public static final String PWaveformL1Max = "PWaveformL1Max";
        public static final String PWaveformL1Min = "PWaveformL1Min";
        public static final String PWaveformL2 = "PWaveformL2";
        public static final String PWaveformL2Max = "PWaveformL2Max";
        public static final String PWaveformL2Min = "PWaveformL2Min";
        public static final String PWaveformL3 = "PWaveformL3";
        public static final String PWaveformL3Max = "PWaveformL3Max";
        public static final String PWaveformL3Min = "PWaveformL3Min";
        public static final String QWaveformL1 = "QWaveformL1";
        public static final String QWaveformL1Max = "QWaveformL1Max";
        public static final String QWaveformL1Min = "QWaveformL1Min";
        public static final String QWaveformL2 = "QWaveformL2";
        public static final String QWaveformL2Max = "QWaveformL2Max";
        public static final String QWaveformL2Min = "QWaveformL2Min";
        public static final String QWaveformL3 = "QWaveformL3";
        public static final String QWaveformL3Max = "QWaveformL3Max";
        public static final String QWaveformL3Min = "QWaveformL3Min";
        public static final String VoltageValueL1 = "VoltageValueL1";
        public static final String VoltageValueL2 = "VoltageValueL2";
        public static final String VoltageValueL3 = "VoltageValueL3";
        public static final String VoltageWaveformL1 = "VoltageWaveformL1";
        public static final String VoltageWaveformL1Max = "VoltageWaveformL1Max";
        public static final String VoltageWaveformL1Min = "VoltageWaveformL1Min";
        public static final String VoltageWaveformL2 = "VoltageWaveformL2";
        public static final String VoltageWaveformL2Max = "VoltageWaveformL2Max";
        public static final String VoltageWaveformL2Min = "VoltageWaveformL2Min";
        public static final String VoltageWaveformL3 = "VoltageWaveformL3";
        public static final String VoltageWaveformL3Max = "VoltageWaveformL3Max";
        public static final String VoltageWaveformL3Min = "VoltageWaveformL3Min";

        private TableColumns() {
        }
    }

    public WaveMeasurementDao(Context context) {
        super(context, TABLE_NAME);
    }

    public void createTable(SQLiteDatabase db) {
        this.m_TableName = TABLE_NAME;
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append(getCreatTableString());
        stringBuffer.append("VoltageValueL1").append(" VARCHAR(20),");
        stringBuffer.append("VoltageValueL2").append(" VARCHAR(20),");
        stringBuffer.append("VoltageValueL3").append(" VARCHAR(20),");
        stringBuffer.append("CurrentValueL1").append(" VARCHAR(20),");
        stringBuffer.append("CurrentValueL2").append(" VARCHAR(20),");
        stringBuffer.append("CurrentValueL3").append(" VARCHAR(20),");
        stringBuffer.append(TableColumns.VoltageWaveformL1Max).append(" VARCHAR(20),");
        stringBuffer.append(TableColumns.VoltageWaveformL1Min).append(" VARCHAR(20),");
        stringBuffer.append(TableColumns.VoltageWaveformL1).append(" VARCHAR(1000),");
        stringBuffer.append(TableColumns.VoltageWaveformL2Max).append(" VARCHAR(20),");
        stringBuffer.append(TableColumns.VoltageWaveformL2Min).append(" VARCHAR(20),");
        stringBuffer.append(TableColumns.VoltageWaveformL2).append(" VARCHAR(1000),");
        stringBuffer.append(TableColumns.VoltageWaveformL3Max).append(" VARCHAR(20),");
        stringBuffer.append(TableColumns.VoltageWaveformL3Min).append(" VARCHAR(20),");
        stringBuffer.append(TableColumns.VoltageWaveformL3).append(" VARCHAR(1000),");
        stringBuffer.append(TableColumns.CurrentWaveformL1Max).append(" VARCHAR(20),");
        stringBuffer.append(TableColumns.CurrentWaveformL1Min).append(" VARCHAR(20),");
        stringBuffer.append(TableColumns.CurrentWaveformL1).append(" VARCHAR(1000),");
        stringBuffer.append(TableColumns.CurrentWaveformL2Max).append(" VARCHAR(20),");
        stringBuffer.append(TableColumns.CurrentWaveformL2Min).append(" VARCHAR(20),");
        stringBuffer.append(TableColumns.CurrentWaveformL2).append(" VARCHAR(1000),");
        stringBuffer.append(TableColumns.CurrentWaveformL3Max).append(" VARCHAR(20),");
        stringBuffer.append(TableColumns.CurrentWaveformL3Min).append(" VARCHAR(20),");
        stringBuffer.append(TableColumns.CurrentWaveformL3).append(" VARCHAR(1000),");
        stringBuffer.append(TableColumns.PWaveformL1Max).append(" VARCHAR(20),");
        stringBuffer.append(TableColumns.PWaveformL1Min).append(" VARCHAR(20),");
        stringBuffer.append(TableColumns.PWaveformL1).append(" VARCHAR(1000),");
        stringBuffer.append(TableColumns.PWaveformL2Max).append(" VARCHAR(20),");
        stringBuffer.append(TableColumns.PWaveformL2Min).append(" VARCHAR(20),");
        stringBuffer.append(TableColumns.PWaveformL2).append(" VARCHAR(1000),");
        stringBuffer.append(TableColumns.PWaveformL3Max).append(" VARCHAR(20),");
        stringBuffer.append(TableColumns.PWaveformL3Min).append(" VARCHAR(20),");
        stringBuffer.append(TableColumns.PWaveformL3).append(" VARCHAR(1000),");
        stringBuffer.append(TableColumns.QWaveformL1Max).append(" VARCHAR(20),");
        stringBuffer.append(TableColumns.QWaveformL1Min).append(" VARCHAR(20),");
        stringBuffer.append(TableColumns.QWaveformL1).append(" VARCHAR(1000),");
        stringBuffer.append(TableColumns.QWaveformL2Max).append(" VARCHAR(20),");
        stringBuffer.append(TableColumns.QWaveformL2Min).append(" VARCHAR(20),");
        stringBuffer.append(TableColumns.QWaveformL2).append(" VARCHAR(1000),");
        stringBuffer.append(TableColumns.QWaveformL3Max).append(" VARCHAR(20),");
        stringBuffer.append(TableColumns.QWaveformL3Min).append(" VARCHAR(20),");
        stringBuffer.append(TableColumns.QWaveformL3).append(" VARCHAR(1000));");
        db.execSQL(stringBuffer.toString());
    }

    @Override // com.clou.rs350.db.dao.BaseDao, com.clou.rs350.db.dao.BaseTestDao
    public void upgradeTable(SQLiteDatabase db, int oldVersion, int newVersion) {
        if (oldVersion <= 3) {
            db.execSQL("alter table WaveformMeasurement rename to TestWaveformMeasurement");
        }
        if (oldVersion <= 5) {
            db.execSQL("alter table TestWaveformMeasurement add column VoltageWaveformL1Max VARCHAR(20)");
            db.execSQL("alter table TestWaveformMeasurement add column VoltageWaveformL1Min VARCHAR(20)");
            db.execSQL("alter table TestWaveformMeasurement add column VoltageWaveformL2Max VARCHAR(20)");
            db.execSQL("alter table TestWaveformMeasurement add column VoltageWaveformL2Min VARCHAR(20)");
            db.execSQL("alter table TestWaveformMeasurement add column VoltageWaveformL3Max VARCHAR(20)");
            db.execSQL("alter table TestWaveformMeasurement add column VoltageWaveformL3Min VARCHAR(20)");
            db.execSQL("alter table TestWaveformMeasurement add column CurrentWaveformL1Max VARCHAR(20)");
            db.execSQL("alter table TestWaveformMeasurement add column CurrentWaveformL1Min VARCHAR(20)");
            db.execSQL("alter table TestWaveformMeasurement add column CurrentWaveformL2Max VARCHAR(20)");
            db.execSQL("alter table TestWaveformMeasurement add column CurrentWaveformL2Min VARCHAR(20)");
            db.execSQL("alter table TestWaveformMeasurement add column CurrentWaveformL3Max VARCHAR(20)");
            db.execSQL("alter table TestWaveformMeasurement add column CurrentWaveformL3Min VARCHAR(20)");
            db.execSQL("alter table TestWaveformMeasurement add column PWaveformL1Max VARCHAR(20)");
            db.execSQL("alter table TestWaveformMeasurement add column PWaveformL1Min VARCHAR(20)");
            db.execSQL("alter table TestWaveformMeasurement add column PWaveformL2Max VARCHAR(20)");
            db.execSQL("alter table TestWaveformMeasurement add column PWaveformL2Min VARCHAR(20)");
            db.execSQL("alter table TestWaveformMeasurement add column PWaveformL3Max VARCHAR(20)");
            db.execSQL("alter table TestWaveformMeasurement add column PWaveformL3Min VARCHAR(20)");
            db.execSQL("alter table TestWaveformMeasurement add column QWaveformL1Max VARCHAR(20)");
            db.execSQL("alter table TestWaveformMeasurement add column QWaveformL1Min VARCHAR(20)");
            db.execSQL("alter table TestWaveformMeasurement add column QWaveformL2Max VARCHAR(20)");
            db.execSQL("alter table TestWaveformMeasurement add column QWaveformL2Min VARCHAR(20)");
            db.execSQL("alter table TestWaveformMeasurement add column QWaveformL3Max VARCHAR(20)");
            db.execSQL("alter table TestWaveformMeasurement add column QWaveformL3Min VARCHAR(20)");
        }
        super.upgradeTable(db, oldVersion, newVersion);
    }

    @Override // com.clou.rs350.db.dao.BaseDao, com.clou.rs350.db.dao.BaseTestDao
    public synchronized boolean insertObject(Object Data, String Time) {
        SQLiteDatabase db;
        ContentValues values;
        db = DatabaseManager.getWriteableDatabase(this.m_Context);
        values = new ContentValues();
        WaveMeasurement object = (WaveMeasurement) Data;
        values.put(this.m_TableKey, object.SerialNo);
        values.put("CreateTime", Time);
        values.put(SameColumns.WiringType, object.WiringValue);
        values.put("IntWiringType", Integer.valueOf(object.Wiring));
        values.put(SameColumns.ConnectionMode, object.ConnectionType);
        values.put(SameColumns.VoltageRangeL1, object.VoltageRangeL1);
        values.put(SameColumns.VoltageRangeL2, object.VoltageRangeL2);
        values.put(SameColumns.VoltageRangeL3, object.VoltageRangeL3);
        values.put(SameColumns.CurrentRangeL1, object.CurrentRangeL1);
        values.put(SameColumns.CurrentRangeL2, object.CurrentRangeL2);
        values.put(SameColumns.CurrentRangeL3, object.CurrentRangeL3);
        values.put("VoltageValueL1", object.VoltageValueL1.s_Value);
        values.put("VoltageValueL2", object.VoltageValueL2.s_Value);
        values.put("VoltageValueL3", object.VoltageValueL3.s_Value);
        values.put("CurrentValueL1", object.CurrentValueL1.s_Value);
        values.put("CurrentValueL2", object.CurrentValueL2.s_Value);
        values.put("CurrentValueL3", object.CurrentValueL3.s_Value);
        values.put(TableColumns.VoltageWaveformL1, object.VoltageWaveformL1);
        values.put(TableColumns.VoltageWaveformL2, object.VoltageWaveformL2);
        values.put(TableColumns.VoltageWaveformL3, object.VoltageWaveformL3);
        values.put(TableColumns.CurrentWaveformL1, object.CurrentWaveformL1);
        values.put(TableColumns.CurrentWaveformL2, object.CurrentWaveformL2);
        values.put(TableColumns.CurrentWaveformL3, object.CurrentWaveformL3);
        values.put(TableColumns.VoltageWaveformL1Max, object.waveUa.MaxValue.s_Value);
        values.put(TableColumns.VoltageWaveformL1Min, object.waveUa.MinValue.s_Value);
        values.put(TableColumns.VoltageWaveformL2Max, object.waveUb.MaxValue.s_Value);
        values.put(TableColumns.VoltageWaveformL2Min, object.waveUb.MinValue.s_Value);
        values.put(TableColumns.VoltageWaveformL3Max, object.waveUc.MaxValue.s_Value);
        values.put(TableColumns.VoltageWaveformL3Min, object.waveUc.MinValue.s_Value);
        values.put(TableColumns.CurrentWaveformL1Max, object.waveIa.MaxValue.s_Value);
        values.put(TableColumns.CurrentWaveformL1Min, object.waveIa.MinValue.s_Value);
        values.put(TableColumns.CurrentWaveformL2Max, object.waveIb.MaxValue.s_Value);
        values.put(TableColumns.CurrentWaveformL2Min, object.waveIb.MinValue.s_Value);
        values.put(TableColumns.CurrentWaveformL3Max, object.waveIc.MaxValue.s_Value);
        values.put(TableColumns.CurrentWaveformL3Min, object.waveIc.MinValue.s_Value);
        values.put(TableColumns.PWaveformL1, object.PWaveformL1);
        values.put(TableColumns.PWaveformL2, object.PWaveformL2);
        values.put(TableColumns.PWaveformL3, object.PWaveformL3);
        values.put(TableColumns.QWaveformL1, object.QWaveformL1);
        values.put(TableColumns.QWaveformL2, object.QWaveformL2);
        values.put(TableColumns.QWaveformL3, object.QWaveformL3);
        values.put(TableColumns.PWaveformL1Max, object.wavePa.MaxValue.s_Value);
        values.put(TableColumns.PWaveformL1Min, object.wavePa.MinValue.s_Value);
        values.put(TableColumns.PWaveformL2Max, object.wavePb.MaxValue.s_Value);
        values.put(TableColumns.PWaveformL2Min, object.wavePb.MinValue.s_Value);
        values.put(TableColumns.PWaveformL3Max, object.wavePc.MaxValue.s_Value);
        values.put(TableColumns.PWaveformL3Min, object.wavePc.MinValue.s_Value);
        values.put(TableColumns.QWaveformL1Max, object.waveQa.MaxValue.s_Value);
        values.put(TableColumns.QWaveformL1Min, object.waveQa.MinValue.s_Value);
        values.put(TableColumns.QWaveformL2Max, object.waveQb.MaxValue.s_Value);
        values.put(TableColumns.QWaveformL2Min, object.waveQb.MinValue.s_Value);
        values.put(TableColumns.QWaveformL3Max, object.waveQc.MaxValue.s_Value);
        values.put(TableColumns.QWaveformL3Min, object.waveQc.MinValue.s_Value);
        return db.insert(TABLE_NAME, null, values) != -1;
    }

    /* access modifiers changed from: protected */
    @Override // com.clou.rs350.db.dao.BaseDao, com.clou.rs350.db.dao.BaseTestDao
    public Object parseData(Cursor cursor) {
        WaveMeasurement value = new WaveMeasurement();
        value.SerialNo = cursor.getString(cursor.getColumnIndex(this.m_TableKey));
        value.WiringValue = cursor.getString(cursor.getColumnIndex(SameColumns.WiringType));
        value.Wiring = cursor.getInt(cursor.getColumnIndex("IntWiringType"));
        value.VoltageRangeL1 = cursor.getString(cursor.getColumnIndex(SameColumns.VoltageRangeL1));
        value.VoltageRangeL2 = cursor.getString(cursor.getColumnIndex(SameColumns.VoltageRangeL2));
        value.VoltageRangeL3 = cursor.getString(cursor.getColumnIndex(SameColumns.VoltageRangeL3));
        value.CurrentRangeL1 = cursor.getString(cursor.getColumnIndex(SameColumns.CurrentRangeL1));
        value.CurrentRangeL2 = cursor.getString(cursor.getColumnIndex(SameColumns.CurrentRangeL2));
        value.CurrentRangeL3 = cursor.getString(cursor.getColumnIndex(SameColumns.CurrentRangeL3));
        value.VoltageValueL1.s_Value = cursor.getString(cursor.getColumnIndex("VoltageValueL1"));
        value.VoltageValueL2.s_Value = cursor.getString(cursor.getColumnIndex("VoltageValueL2"));
        value.VoltageValueL3.s_Value = cursor.getString(cursor.getColumnIndex("VoltageValueL3"));
        value.CurrentValueL1.s_Value = cursor.getString(cursor.getColumnIndex("CurrentValueL1"));
        value.CurrentValueL2.s_Value = cursor.getString(cursor.getColumnIndex("CurrentValueL2"));
        value.CurrentValueL3.s_Value = cursor.getString(cursor.getColumnIndex("CurrentValueL3"));
        value.waveUa.Wave = DataProcessing.parseDouble(cursor.getString(cursor.getColumnIndex(TableColumns.VoltageWaveformL1)), "\\|");
        value.waveUb.Wave = DataProcessing.parseDouble(cursor.getString(cursor.getColumnIndex(TableColumns.VoltageWaveformL2)), "\\|");
        value.waveUc.Wave = DataProcessing.parseDouble(cursor.getString(cursor.getColumnIndex(TableColumns.VoltageWaveformL3)), "\\|");
        value.waveIa.Wave = DataProcessing.parseDouble(cursor.getString(cursor.getColumnIndex(TableColumns.CurrentWaveformL1)), "\\|");
        value.waveIb.Wave = DataProcessing.parseDouble(cursor.getString(cursor.getColumnIndex(TableColumns.CurrentWaveformL2)), "\\|");
        value.waveIc.Wave = DataProcessing.parseDouble(cursor.getString(cursor.getColumnIndex(TableColumns.CurrentWaveformL3)), "\\|");
        value.waveUa.MaxValue = DataProcessing.parseData(cursor.getString(cursor.getColumnIndex(TableColumns.VoltageWaveformL1Max)), "V");
        value.waveUa.MinValue = DataProcessing.parseData(cursor.getString(cursor.getColumnIndex(TableColumns.VoltageWaveformL1Min)), "V");
        value.waveUb.MaxValue = DataProcessing.parseData(cursor.getString(cursor.getColumnIndex(TableColumns.VoltageWaveformL2Max)), "V");
        value.waveUb.MinValue = DataProcessing.parseData(cursor.getString(cursor.getColumnIndex(TableColumns.VoltageWaveformL2Min)), "V");
        value.waveUc.MaxValue = DataProcessing.parseData(cursor.getString(cursor.getColumnIndex(TableColumns.VoltageWaveformL3Max)), "V");
        value.waveUc.MinValue = DataProcessing.parseData(cursor.getString(cursor.getColumnIndex(TableColumns.VoltageWaveformL3Min)), "V");
        value.waveIa.MaxValue = DataProcessing.parseData(cursor.getString(cursor.getColumnIndex(TableColumns.CurrentWaveformL1Max)), "A");
        value.waveIa.MinValue = DataProcessing.parseData(cursor.getString(cursor.getColumnIndex(TableColumns.CurrentWaveformL1Min)), "A");
        value.waveIb.MaxValue = DataProcessing.parseData(cursor.getString(cursor.getColumnIndex(TableColumns.CurrentWaveformL2Max)), "A");
        value.waveIb.MinValue = DataProcessing.parseData(cursor.getString(cursor.getColumnIndex(TableColumns.CurrentWaveformL2Min)), "A");
        value.waveIc.MaxValue = DataProcessing.parseData(cursor.getString(cursor.getColumnIndex(TableColumns.CurrentWaveformL3Max)), "A");
        value.waveIc.MinValue = DataProcessing.parseData(cursor.getString(cursor.getColumnIndex(TableColumns.CurrentWaveformL3Min)), "A");
        value.waveUa = new WaveValue(value.waveUa.MaxValue, value.waveUa.MinValue, value.waveUa.Wave);
        value.waveUb = new WaveValue(value.waveUb.MaxValue, value.waveUb.MinValue, value.waveUb.Wave);
        value.waveUc = new WaveValue(value.waveUc.MaxValue, value.waveUc.MinValue, value.waveUc.Wave);
        value.waveIa = new WaveValue(value.waveIa.MaxValue, value.waveIa.MinValue, value.waveIa.Wave);
        value.waveIb = new WaveValue(value.waveIb.MaxValue, value.waveIb.MinValue, value.waveIb.Wave);
        value.waveIc = new WaveValue(value.waveIc.MaxValue, value.waveIc.MinValue, value.waveIc.Wave);
        value.wavePa.Wave = DataProcessing.parseDouble(cursor.getString(cursor.getColumnIndex(TableColumns.PWaveformL1)), "\\|");
        value.wavePb.Wave = DataProcessing.parseDouble(cursor.getString(cursor.getColumnIndex(TableColumns.PWaveformL2)), "\\|");
        value.wavePc.Wave = DataProcessing.parseDouble(cursor.getString(cursor.getColumnIndex(TableColumns.PWaveformL3)), "\\|");
        value.waveQa.Wave = DataProcessing.parseDouble(cursor.getString(cursor.getColumnIndex(TableColumns.QWaveformL1)), "\\|");
        value.waveQb.Wave = DataProcessing.parseDouble(cursor.getString(cursor.getColumnIndex(TableColumns.QWaveformL2)), "\\|");
        value.waveQc.Wave = DataProcessing.parseDouble(cursor.getString(cursor.getColumnIndex(TableColumns.QWaveformL3)), "\\|");
        value.wavePa.MaxValue = DataProcessing.parseData(cursor.getString(cursor.getColumnIndex(TableColumns.PWaveformL1Max)), "W");
        value.wavePa.MinValue = DataProcessing.parseData(cursor.getString(cursor.getColumnIndex(TableColumns.PWaveformL1Min)), "W");
        value.wavePb.MaxValue = DataProcessing.parseData(cursor.getString(cursor.getColumnIndex(TableColumns.PWaveformL2Max)), "W");
        value.wavePb.MinValue = DataProcessing.parseData(cursor.getString(cursor.getColumnIndex(TableColumns.PWaveformL2Min)), "W");
        value.wavePc.MaxValue = DataProcessing.parseData(cursor.getString(cursor.getColumnIndex(TableColumns.PWaveformL3Max)), "W");
        value.wavePc.MinValue = DataProcessing.parseData(cursor.getString(cursor.getColumnIndex(TableColumns.PWaveformL3Min)), "W");
        value.waveQa.MaxValue = DataProcessing.parseData(cursor.getString(cursor.getColumnIndex(TableColumns.QWaveformL1Max)), "var");
        value.waveQa.MinValue = DataProcessing.parseData(cursor.getString(cursor.getColumnIndex(TableColumns.QWaveformL1Min)), "var");
        value.waveQb.MaxValue = DataProcessing.parseData(cursor.getString(cursor.getColumnIndex(TableColumns.QWaveformL2Max)), "var");
        value.waveQb.MinValue = DataProcessing.parseData(cursor.getString(cursor.getColumnIndex(TableColumns.QWaveformL2Min)), "var");
        value.waveQc.MaxValue = DataProcessing.parseData(cursor.getString(cursor.getColumnIndex(TableColumns.QWaveformL3Max)), "var");
        value.waveQc.MinValue = DataProcessing.parseData(cursor.getString(cursor.getColumnIndex(TableColumns.QWaveformL3Min)), "var");
        value.wavePa = new WaveValue(value.wavePa.MaxValue, value.wavePa.MinValue, value.wavePa.Wave);
        value.wavePb = new WaveValue(value.wavePb.MaxValue, value.wavePb.MinValue, value.wavePb.Wave);
        value.wavePc = new WaveValue(value.wavePc.MaxValue, value.wavePc.MinValue, value.wavePc.Wave);
        value.waveQa = new WaveValue(value.waveQa.MaxValue, value.waveQa.MinValue, value.waveQa.Wave);
        value.waveQb = new WaveValue(value.waveQb.MaxValue, value.waveQb.MinValue, value.waveQb.Wave);
        value.waveQc = new WaveValue(value.waveQc.MaxValue, value.waveQc.MinValue, value.waveQc.Wave);
        return value;
    }
}
