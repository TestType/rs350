package com.clou.rs350.db.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import com.clou.rs350.db.model.DataBaseRecordItem;
import com.clou.rs350.db.model.report.DataItem;
import com.clou.rs350.manager.DatabaseManager;
import com.itextpdf.text.pdf.PdfObject;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class BaseDao {
    protected Context m_Context;
    protected String m_TableKey = PdfObject.NOTHING;
    protected String m_TableName = PdfObject.NOTHING;

    protected static final class BaseColumns {
        public static final String CreateTime = "CreateTime";
        public static final String ID = "intMyID";
        public static final String IsRead = "IsRead";

        protected BaseColumns() {
        }
    }

    public BaseDao(Context context, String tableName, String tableKey) {
        this.m_Context = context;
        this.m_TableName = tableName;
        this.m_TableKey = tableKey;
    }

    public String getTableName() {
        return this.m_TableName;
    }

    /* access modifiers changed from: protected */
    public String parseString(String tmpString) {
        if (tmpString == null) {
            return PdfObject.NOTHING;
        }
        return tmpString;
    }

    /* access modifiers changed from: protected */
    public Object parseData(Cursor cursor) {
        return null;
    }

    /* access modifiers changed from: protected */
    public String getCreatTableString() {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("create table if not exists ").append(this.m_TableName).append(" (").append("intMyID").append(" integer primary key autoincrement,").append(this.m_TableKey).append(" VARCHAR(50),").append("CreateTime").append(" VARCHAR(50),").append("IsRead").append(" integer default 0,");
        return stringBuffer.toString();
    }

    public synchronized boolean insertObject(Object Data, String Time) {
        return false;
    }

    public int deleteRecordByKey(String key) {
        return DatabaseManager.getWriteableDatabase(this.m_Context).delete(this.m_TableName, String.valueOf(this.m_TableKey) + " = ?", new String[]{key});
    }

    public void deleteRecordByKey(String key, List<DataBaseRecordItem> list) {
        SQLiteDatabase db = DatabaseManager.getWriteableDatabase(this.m_Context);
        for (DataBaseRecordItem item : list) {
            if (item.isSelect()) {
                db.delete(this.m_TableName, String.valueOf(this.m_TableKey) + " = ? AND " + "CreateTime" + " = ?", new String[]{key, item.getDatetime()});
            }
        }
    }

    public void deleteRecordByKey(String key, DataBaseRecordItem item) {
        SQLiteDatabase db = DatabaseManager.getWriteableDatabase(this.m_Context);
        if (item.isSelect()) {
            db.delete(this.m_TableName, String.valueOf(this.m_TableKey) + " = ? AND " + "CreateTime" + " = ?", new String[]{key, item.getDatetime()});
        }
    }

    private List<DataItem> queryReportDataByKeyAndTime(String selection, String[] selectionArgs) {
        Cursor cursor = DatabaseManager.getWriteableDatabase(this.m_Context).query(this.m_TableName, null, selection, selectionArgs, null, null, null);
        List<DataItem> Datas = new ArrayList<>();
        String[] tmp_Names = cursor.getColumnNames();
        while (cursor.moveToNext()) {
            DataItem Data = new DataItem();
            for (int i = 0; i < tmp_Names.length; i++) {
                Data.DataMap.put(tmp_Names[i], cursor.getString(cursor.getColumnIndex(tmp_Names[i])));
            }
            Datas.add(Data);
        }
        cursor.close();
        if (Datas.size() == 0) {
            return null;
        }
        return Datas;
    }

    public List<DataItem> queryReportDataByKeyAndTime(String key, String time) {
        return queryReportDataByKeyAndTime(String.valueOf(this.m_TableKey) + " =? and " + "CreateTime" + " =?", new String[]{key, time});
    }

    public List<DataItem> queryReportDataByKeyAndTime(String key) {
        return queryReportDataByKeyAndTime(String.valueOf(this.m_TableKey) + " =?", new String[]{key});
    }

    public Object queryObjectByKey(String key) {
        Object value = null;
        Cursor cursor = DatabaseManager.getWriteableDatabase(this.m_Context).rawQuery("select * from " + this.m_TableName + " where " + this.m_TableKey + " = ?", new String[]{key});
        if (cursor.moveToNext()) {
            value = parseData(cursor);
        }
        cursor.close();
        return value;
    }

    public Object queryDataByKeyAndTime(String key, String time) {
        Cursor cursor = DatabaseManager.getWriteableDatabase(this.m_Context).query(this.m_TableName, null, String.valueOf(this.m_TableKey) + " =? and " + "CreateTime" + " =?", new String[]{key, time}, null, null, null);
        Object Data = null;
        if (cursor.moveToFirst()) {
            Data = parseData(cursor);
        }
        cursor.close();
        return Data;
    }

    public List<Object> queryDataListByKeyAndTime(String key, String time) {
        Cursor cursor = DatabaseManager.getWriteableDatabase(this.m_Context).query(this.m_TableName, null, String.valueOf(this.m_TableKey) + " =? and " + "CreateTime" + " =?", new String[]{key, time}, null, null, null);
        List<Object> Datas = new ArrayList<>();
        while (cursor.moveToNext()) {
            Datas.add(parseData(cursor));
        }
        cursor.close();
        return Datas;
    }

    public void upgradeTable(SQLiteDatabase db, int oldVersion, int newVersion) {
    }

    public boolean queryIfExistByKey(String Key) {
        boolean value = false;
        Cursor cursor = DatabaseManager.getWriteableDatabase(this.m_Context).rawQuery("select * from " + this.m_TableName + " where " + this.m_TableKey + " = ?", new String[]{Key});
        if (cursor.moveToNext()) {
            value = true;
        }
        cursor.close();
        return value;
    }

    public List<String> queryAllKey() {
        List<String> values = new ArrayList<>();
        Cursor cursor = DatabaseManager.getWriteableDatabase(this.m_Context).query(this.m_TableName, new String[]{this.m_TableKey}, null, null, null, null, this.m_TableKey);
        while (cursor.moveToNext()) {
            String tmpName = cursor.getString(0);
            if (!values.contains(tmpName)) {
                values.add(tmpName);
            }
        }
        cursor.close();
        return values;
    }

    public List<String> queryAllKey(String condition) {
        if (condition.isEmpty()) {
            return queryAllKey();
        }
        List<String> values = new ArrayList<>();
        Cursor cursor = DatabaseManager.getWriteableDatabase(this.m_Context).query(this.m_TableName, new String[]{this.m_TableKey}, String.valueOf(this.m_TableKey) + " LIKE ? ", new String[]{"%" + condition + "%"}, null, null, this.m_TableKey);
        while (cursor.moveToNext()) {
            String tmpName = cursor.getString(0);
            if (!values.contains(tmpName)) {
                values.add(tmpName);
            }
        }
        cursor.close();
        return values;
    }

    private String clearStartAndEndQuote(String str) {
        if (str == null || str.length() < 2) {
            return str;
        }
        if (str.indexOf("\"") == 0) {
            str = str.substring(1, str.length());
        }
        if (str.lastIndexOf("\"") == str.length() - 1) {
            str = str.substring(0, str.length() - 1);
        }
        return str.replaceAll("\"\"", "\"");
    }

    private String[] clearStartAndEndQuoteList(String[] values) {
        String[] result = new String[values.length];
        for (int i = 0; i < values.length; i++) {
            result[i] = clearStartAndEndQuote(values[i]);
        }
        return result;
    }

    public void importDataFromCSV(String fileName) {
        File inFile = new File(fileName);
        try {
            SQLiteDatabase db = DatabaseManager.getWriteableDatabase(this.m_Context);
            Cursor cursor = db.query(this.m_TableName, null, null, null, null, null, null);
            BufferedReader reader = new BufferedReader(new FileReader(inFile));
            int row = 0;
            String[] columoName = cursor.getColumnNames();
            String[] inportName = null;
            List<String> newKey = new ArrayList<>();
            while (true) {
                String inString = reader.readLine();
                if (inString == null) {
                    reader.close();
                    return;
                }
                if (row == 0) {
                    inportName = clearStartAndEndQuoteList(inString.split(",(?=([^\\\"]*\\\"[^\\\"]*\\\")*[^\\\"]*$)", -1));
                } else {
                    Map<String, String> datas = new HashMap<>();
                    String[] values = clearStartAndEndQuoteList(inString.split(",(?=([^\\\"]*\\\"[^\\\"]*\\\")*[^\\\"]*$)", -1));
                    for (int i = 0; i < inportName.length; i++) {
                        datas.put(inportName[i].trim(), values[i].trim());
                    }
                    ContentValues inportData = new ContentValues();
                    for (int i2 = 0; i2 < columoName.length; i2++) {
                        if (!columoName[i2].equals("intMyID") && datas.containsKey(columoName[i2])) {
                            inportData.put(columoName[i2], datas.get(columoName[i2]));
                        }
                    }
                    if (inportData.size() > 0) {
                        if (!newKey.contains(datas.get(this.m_TableKey))) {
                            db.delete(this.m_TableName, String.valueOf(this.m_TableKey) + " = ?", new String[]{datas.get(this.m_TableKey)});
                            newKey.add(datas.get(this.m_TableKey));
                        }
                        db.insert(this.m_TableName, null, inportData);
                    }
                }
                row++;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void exportDataToCSV(String fileName) {
        exportDataToCSV(DatabaseManager.getWriteableDatabase(this.m_Context).query(this.m_TableName, null, null, null, null, null, null), fileName);
    }

    public void exportDataToCSV(String key, String time, String fileName) {
        exportDataToCSV(DatabaseManager.getWriteableDatabase(this.m_Context).query(this.m_TableName, null, String.valueOf(this.m_TableKey) + " =? and " + "CreateTime" + " =?", new String[]{key, time}, null, null, null), fileName);
    }

    /* access modifiers changed from: protected */
    public void exportDataToCSV(Cursor cursor, String fileName) {
        String tmpString;
        String strCSV = PdfObject.NOTHING;
        try {
            String[] tmp_Names = cursor.getColumnNames();
            FileOutputStream fos = new FileOutputStream(new File(fileName), false);
            for (int i = 0; i < tmp_Names.length; i++) {
                String strCSV2 = String.valueOf(strCSV) + tmp_Names[i];
                strCSV = i < tmp_Names.length - 1 ? String.valueOf(strCSV2) + "," : String.valueOf(strCSV2) + "\n";
            }
            fos.write(strCSV.getBytes());
            while (cursor.moveToNext()) {
                String strCSV3 = PdfObject.NOTHING;
                for (int i2 = 0; i2 < tmp_Names.length; i2++) {
                    String tmpString2 = cursor.getString(cursor.getColumnIndex(tmp_Names[i2]));
                    if (tmpString2 != null) {
                        tmpString = tmpString2.replaceAll("\"", "\"\"");
                    } else {
                        tmpString = PdfObject.NOTHING;
                    }
                    String strCSV4 = (tmpString.contains(",") || tmpString.contains("\"")) ? String.valueOf(strCSV3) + "\"" + tmpString + "\"" : String.valueOf(strCSV3) + tmpString;
                    strCSV3 = i2 < tmp_Names.length - 1 ? String.valueOf(strCSV4) + "," : String.valueOf(strCSV4) + "\n";
                }
                fos.write(strCSV3.getBytes());
            }
            cursor.close();
            fos.flush();
            fos.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e2) {
            e2.printStackTrace();
        }
    }
}
