package com.clou.rs350.db.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import com.clou.rs350.db.dao.BaseTestDao;
import com.clou.rs350.db.model.DemandTest;
import com.clou.rs350.manager.DatabaseManager;

public class DemandTestDao extends BaseTestDao {
    public static final String TABLE_NAME = "TestDemandTest";

    private static final class TableColumns {
        public static final String ActiveBeginPower = "ActiveBeginPower";
        public static final String ActiveEndPower = "ActiveEndPower";
        public static final String ActiveError = "ActiveError";
        public static final String ActivePowerL1 = "ActivePowerL1";
        public static final String ActivePowerL2 = "ActivePowerL2";
        public static final String ActivePowerL3 = "ActivePowerL3";
        public static final String ActivePowerTotal = "ActivePowerTotal";
        public static final String ActiveResult = "ActiveResult";
        public static final String ActiveStandardPower = "ActiveStandardPower";
        public static final String ApparentBeginPower = "ApparentBeginPower";
        public static final String ApparentEndPower = "ApparentEndPower";
        public static final String ApparentError = "ApparentError";
        public static final String ApparentPowerL1 = "ApparentPowerL1";
        public static final String ApparentPowerL2 = "ApparentPowerL2";
        public static final String ApparentPowerL3 = "ApparentPowerL3";
        public static final String ApparentPowerTotal = "ApparentPowerTotal";
        public static final String ApparentResult = "ApparentResult";
        public static final String ApparentStandardPower = "ApparentStandardPower";
        public static final String CurrentAngleL1 = "CurrentAngleL1";
        public static final String CurrentAngleL2 = "CurrentAngleL2";
        public static final String CurrentAngleL3 = "CurrentAngleL3";
        public static final String CurrentValueL1 = "CurrentValueL1";
        public static final String CurrentValueL2 = "CurrentValueL2";
        public static final String CurrentValueL3 = "CurrentValueL3";
        public static final String Frequency = "Frequency";
        public static final String MeterIndex = "MeterIndex";
        public static final String PowerFactorL1 = "PowerFactorL1";
        public static final String PowerFactorL2 = "PowerFactorL2";
        public static final String PowerFactorL3 = "PowerFactorL3";
        public static final String PowerFactorTotal = "PowerFactorTotal";
        public static final String ReactiveBeginPower = "ReactiveBeginPower";
        public static final String ReactiveEndPower = "ReactiveEndPower";
        public static final String ReactiveError = "ReactiveError";
        public static final String ReactivePowerL1 = "ReactivePowerL1";
        public static final String ReactivePowerL2 = "ReactivePowerL2";
        public static final String ReactivePowerL3 = "ReactivePowerL3";
        public static final String ReactivePowerTotal = "ReactivePowerTotal";
        public static final String ReactiveResult = "ReactiveResult";
        public static final String ReactiveStandardPower = "ReactiveStandardPower";
        public static final String RunningTime = "RunningTime";
        public static final String TestEndTime = "TestEndTime";
        public static final String TestStartTime = "TestStartTime";
        public static final String TestTime = "TestTime";
        public static final String VoltageAngleL1 = "VoltageAngleL1";
        public static final String VoltageAngleL2 = "VoltageAngleL2";
        public static final String VoltageAngleL3 = "VoltageAngleL3";
        public static final String VoltageValueL1 = "VoltageValueL1";
        public static final String VoltageValueL2 = "VoltageValueL2";
        public static final String VoltageValueL3 = "VoltageValueL3";
        public static final String VoltageVectorAngleL1 = "VoltageVectorAngleL1";
        public static final String VoltageVectorAngleL2 = "VoltageVectorAngleL2";
        public static final String VoltageVectorAngleL3 = "VoltageVectorAngleL3";
        public static final String VoltageVectorValueL1 = "VoltageVectorValueL1";
        public static final String VoltageVectorValueL2 = "VoltageVectorValueL2";
        public static final String VoltageVectorValueL3 = "VoltageVectorValueL3";

        private TableColumns() {
        }
    }

    public DemandTestDao(Context context) {
        super(context, TABLE_NAME);
    }

    public void createTable(SQLiteDatabase db) {
        this.m_TableName = TABLE_NAME;
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append(getCreatTableString());
        stringBuffer.append(SameColumns.MeterSerialNo).append(" VARCHAR(50),");
        stringBuffer.append("MeterIndex").append(" VARCHAR(5),");
        stringBuffer.append("TestTime").append(" VARCHAR(20),");
        stringBuffer.append("TestStartTime").append(" VARCHAR(20),");
        stringBuffer.append("TestEndTime").append(" VARCHAR(20),");
        stringBuffer.append("RunningTime").append(" VARCHAR(20),");
        stringBuffer.append(TableColumns.ActiveBeginPower).append(" VARCHAR(20),");
        stringBuffer.append(TableColumns.ActiveEndPower).append(" VARCHAR(20),");
        stringBuffer.append(TableColumns.ActiveStandardPower).append(" VARCHAR(20),");
        stringBuffer.append("ActiveError").append(" VARCHAR(15),");
        stringBuffer.append("ActiveResult").append(" VARCHAR(20),");
        stringBuffer.append(TableColumns.ReactiveBeginPower).append(" VARCHAR(20),");
        stringBuffer.append(TableColumns.ReactiveEndPower).append(" VARCHAR(20),");
        stringBuffer.append(TableColumns.ReactiveStandardPower).append(" VARCHAR(20),");
        stringBuffer.append("ReactiveError").append(" VARCHAR(15),");
        stringBuffer.append("ReactiveResult").append(" VARCHAR(20),");
        stringBuffer.append(TableColumns.ApparentBeginPower).append(" VARCHAR(20),");
        stringBuffer.append(TableColumns.ApparentEndPower).append(" VARCHAR(20),");
        stringBuffer.append(TableColumns.ApparentStandardPower).append(" VARCHAR(20),");
        stringBuffer.append("ApparentError").append(" VARCHAR(15),");
        stringBuffer.append("ApparentResult").append(" VARCHAR(20),");
        stringBuffer.append("VoltageValueL1").append(" VARCHAR(20),");
        stringBuffer.append("VoltageValueL2").append(" VARCHAR(20),");
        stringBuffer.append("VoltageValueL3").append(" VARCHAR(20),");
        stringBuffer.append("VoltageVectorValueL1").append(" VARCHAR(20),");
        stringBuffer.append("VoltageVectorValueL2").append(" VARCHAR(20),");
        stringBuffer.append("VoltageVectorValueL3").append(" VARCHAR(20),");
        stringBuffer.append("CurrentValueL1").append(" VARCHAR(20),");
        stringBuffer.append("CurrentValueL2").append(" VARCHAR(20),");
        stringBuffer.append("CurrentValueL3").append(" VARCHAR(20),");
        stringBuffer.append("VoltageAngleL1").append(" VARCHAR(20),");
        stringBuffer.append("VoltageAngleL2").append(" VARCHAR(20),");
        stringBuffer.append("VoltageAngleL3").append(" VARCHAR(20),");
        stringBuffer.append("VoltageVectorAngleL1").append(" VARCHAR(20),");
        stringBuffer.append("VoltageVectorAngleL2").append(" VARCHAR(20),");
        stringBuffer.append("VoltageVectorAngleL3").append(" VARCHAR(20),");
        stringBuffer.append("CurrentAngleL1").append(" VARCHAR(20),");
        stringBuffer.append("CurrentAngleL2").append(" VARCHAR(20),");
        stringBuffer.append("CurrentAngleL3").append(" VARCHAR(20),");
        stringBuffer.append("Frequency").append(" VARCHAR(20),");
        stringBuffer.append("ActivePowerL1").append(" VARCHAR(20),");
        stringBuffer.append("ActivePowerL2").append(" VARCHAR(20),");
        stringBuffer.append("ActivePowerL3").append(" VARCHAR(20),");
        stringBuffer.append("ActivePowerTotal").append(" VARCHAR(20),");
        stringBuffer.append("ReactivePowerL1").append(" VARCHAR(20),");
        stringBuffer.append("ReactivePowerL2").append(" VARCHAR(20),");
        stringBuffer.append("ReactivePowerL3").append(" VARCHAR(20),");
        stringBuffer.append("ReactivePowerTotal").append(" VARCHAR(20),");
        stringBuffer.append("ApparentPowerL1").append(" VARCHAR(20),");
        stringBuffer.append("ApparentPowerL2").append(" VARCHAR(20),");
        stringBuffer.append("ApparentPowerL3").append(" VARCHAR(20),");
        stringBuffer.append("ApparentPowerTotal").append(" VARCHAR(20),");
        stringBuffer.append("PowerFactorL1").append(" VARCHAR(10),");
        stringBuffer.append("PowerFactorL2").append(" VARCHAR(10),");
        stringBuffer.append("PowerFactorL3").append(" VARCHAR(10),");
        stringBuffer.append("PowerFactorTotal").append(" VARCHAR(10));");
        db.execSQL(stringBuffer.toString());
    }

    @Override // com.clou.rs350.db.dao.BaseDao, com.clou.rs350.db.dao.BaseTestDao
    public void upgradeTable(SQLiteDatabase db, int oldVersion, int newVersion) {
        if (oldVersion <= 3) {
            db.execSQL("alter table DemandTest rename to TestDemandTest");
        }
        super.upgradeTable(db, oldVersion, newVersion);
    }

    @Override // com.clou.rs350.db.dao.BaseDao, com.clou.rs350.db.dao.BaseTestDao
    public synchronized boolean insertObject(Object Data, String Time) {
        boolean z = true;
        synchronized (this) {
            SQLiteDatabase db = DatabaseManager.getWriteableDatabase(this.m_Context);
            ContentValues values = new ContentValues();
            DemandTest object = (DemandTest) Data;
            values.put(this.m_TableKey, object.CustomerSerialNo);
            values.put(SameColumns.MeterSerialNo, object.MeterSerialNo);
            values.put("CreateTime", Time);
            values.put("MeterIndex", object.MeterIndex);
            values.put(SameColumns.WiringType, object.WiringValue);
            values.put("IntWiringType", Integer.valueOf(object.Wiring));
            values.put(SameColumns.ConnectionMode, object.ConnectionType);
            values.put(SameColumns.VoltageRangeL1, object.VoltageRangeL1);
            values.put(SameColumns.VoltageRangeL2, object.VoltageRangeL2);
            values.put(SameColumns.VoltageRangeL3, object.VoltageRangeL3);
            values.put(SameColumns.CurrentRangeL1, object.CurrentRangeL1);
            values.put(SameColumns.CurrentRangeL2, object.CurrentRangeL2);
            values.put(SameColumns.CurrentRangeL3, object.CurrentRangeL3);
            values.put("VoltageValueL1", object.VoltageValueL1Show.s_Value);
            values.put("VoltageValueL2", object.VoltageValueL2Show.s_Value);
            values.put("VoltageValueL3", object.VoltageValueL3Show.s_Value);
            values.put("VoltageVectorValueL1", object.VoltageValueL1Vector.s_Value);
            values.put("VoltageVectorValueL2", object.VoltageValueL2Vector.s_Value);
            values.put("VoltageVectorValueL3", object.VoltageValueL3Vector.s_Value);
            values.put("CurrentValueL1", object.CurrentValueL1.s_Value);
            values.put("CurrentValueL2", object.CurrentValueL2.s_Value);
            values.put("CurrentValueL3", object.CurrentValueL3.s_Value);
            values.put("VoltageAngleL1", object.VoltageAngleL1Show.s_Value);
            values.put("VoltageAngleL2", object.VoltageAngleL2Show.s_Value);
            values.put("VoltageAngleL3", object.VoltageAngleL3Show.s_Value);
            values.put("VoltageVectorAngleL1", object.VoltageAngleL1Vector.s_Value);
            values.put("VoltageVectorAngleL2", object.VoltageAngleL2Vector.s_Value);
            values.put("VoltageVectorAngleL3", object.VoltageAngleL3Vector.s_Value);
            values.put("CurrentAngleL1", object.CurrentAngleL1.s_Value);
            values.put("CurrentAngleL2", object.CurrentAngleL2.s_Value);
            values.put("CurrentAngleL3", object.CurrentAngleL3.s_Value);
            values.put("Frequency", object.Frequency.s_Value);
            values.put("ActivePowerL1", object.ActivePowerL1.s_Value);
            values.put("ActivePowerL2", object.ActivePowerL2.s_Value);
            values.put("ActivePowerL3", object.ActivePowerL3.s_Value);
            values.put("ActivePowerTotal", object.ActivePowerTotal.s_Value);
            values.put("ReactivePowerL1", object.ReactivePowerL1.s_Value);
            values.put("ReactivePowerL2", object.ReactivePowerL2.s_Value);
            values.put("ReactivePowerL3", object.ReactivePowerL3.s_Value);
            values.put("ReactivePowerTotal", object.ReactivePowerTotal.s_Value);
            values.put("ApparentPowerL1", object.ApparentPowerL1.s_Value);
            values.put("ApparentPowerL2", object.ApparentPowerL2.s_Value);
            values.put("ApparentPowerL3", object.ApparentPowerL3.s_Value);
            values.put("ApparentPowerTotal", object.ApparentPowerTotal.s_Value);
            values.put("PowerFactorL1", object.PowerFactorL1.toString());
            values.put("PowerFactorL2", object.PowerFactorL2.toString());
            values.put("PowerFactorL3", object.PowerFactorL3.toString());
            values.put("PowerFactorTotal", object.PowerFactorTotal.s_Value);
            values.put("TestTime", Long.valueOf(object.TestTime));
            values.put("TestStartTime", Long.valueOf(object.TestStartTime));
            values.put("TestEndTime", Long.valueOf(object.TestEndTime));
            values.put("RunningTime", object.RunningTime);
            values.put(TableColumns.ActiveBeginPower, object.BeginPower[0].s_Value);
            values.put(TableColumns.ActiveEndPower, object.EndPower[0].s_Value);
            values.put(TableColumns.ActiveStandardPower, object.StandardPower[0].s_Value);
            values.put("ActiveError", object.Error[0].s_Value);
            values.put("ActiveResult", object.Result[0].s_Value);
            values.put(TableColumns.ReactiveBeginPower, object.BeginPower[1].s_Value);
            values.put(TableColumns.ReactiveEndPower, object.EndPower[1].s_Value);
            values.put(TableColumns.ReactiveStandardPower, object.StandardPower[1].s_Value);
            values.put("ReactiveError", object.Error[1].s_Value);
            values.put("ReactiveResult", object.Result[1].s_Value);
            values.put(TableColumns.ApparentBeginPower, object.BeginPower[2].s_Value);
            values.put(TableColumns.ApparentEndPower, object.EndPower[2].s_Value);
            values.put(TableColumns.ApparentStandardPower, object.StandardPower[2].s_Value);
            values.put("ApparentError", object.Error[2].s_Value);
            values.put("ApparentResult", object.Result[2].s_Value);
            if (db.insert(TABLE_NAME, null, values) == -1) {
                z = false;
            }
        }
        return z;
    }

    /* access modifiers changed from: protected */
    @Override // com.clou.rs350.db.dao.BaseDao, com.clou.rs350.db.dao.BaseTestDao
    public Object parseData(Cursor cursor) {
        DemandTest value = new DemandTest();
        value.CustomerSerialNo = cursor.getString(cursor.getColumnIndex(this.m_TableKey));
        value.MeterSerialNo = cursor.getString(cursor.getColumnIndex(SameColumns.MeterSerialNo));
        value.MeterIndex = cursor.getString(cursor.getColumnIndex("MeterIndex"));
        value.WiringValue = cursor.getString(cursor.getColumnIndex(SameColumns.WiringType));
        value.Wiring = cursor.getInt(cursor.getColumnIndex("IntWiringType"));
        value.ConnectionType = cursor.getString(cursor.getColumnIndex(SameColumns.ConnectionMode));
        value.VoltageRangeL1 = cursor.getString(cursor.getColumnIndex(SameColumns.VoltageRangeL1));
        value.VoltageRangeL2 = cursor.getString(cursor.getColumnIndex(SameColumns.VoltageRangeL2));
        value.VoltageRangeL3 = cursor.getString(cursor.getColumnIndex(SameColumns.VoltageRangeL3));
        value.CurrentRangeL1 = cursor.getString(cursor.getColumnIndex(SameColumns.CurrentRangeL1));
        value.CurrentRangeL2 = cursor.getString(cursor.getColumnIndex(SameColumns.CurrentRangeL2));
        value.CurrentRangeL3 = cursor.getString(cursor.getColumnIndex(SameColumns.CurrentRangeL3));
        value.RunningTime = cursor.getString(cursor.getColumnIndex("RunningTime"));
        value.VoltageValueL1Show.s_Value = cursor.getString(cursor.getColumnIndex("VoltageValueL1"));
        value.VoltageValueL2Show.s_Value = cursor.getString(cursor.getColumnIndex("VoltageValueL2"));
        value.VoltageValueL3Show.s_Value = cursor.getString(cursor.getColumnIndex("VoltageValueL3"));
        value.CurrentValueL1.s_Value = cursor.getString(cursor.getColumnIndex("CurrentValueL1"));
        value.CurrentValueL2.s_Value = cursor.getString(cursor.getColumnIndex("CurrentValueL2"));
        value.CurrentValueL3.s_Value = cursor.getString(cursor.getColumnIndex("CurrentValueL3"));
        value.ActivePowerL1.s_Value = cursor.getString(cursor.getColumnIndex("ActivePowerL1"));
        value.ActivePowerL2.s_Value = cursor.getString(cursor.getColumnIndex("ActivePowerL2"));
        value.ActivePowerL3.s_Value = cursor.getString(cursor.getColumnIndex("ActivePowerL3"));
        value.ActivePowerTotal.s_Value = cursor.getString(cursor.getColumnIndex("ActivePowerTotal"));
        value.ReactivePowerL1.s_Value = cursor.getString(cursor.getColumnIndex("ReactivePowerL1"));
        value.ReactivePowerL2.s_Value = cursor.getString(cursor.getColumnIndex("ReactivePowerL2"));
        value.ReactivePowerL3.s_Value = cursor.getString(cursor.getColumnIndex("ReactivePowerL3"));
        value.ReactivePowerTotal.s_Value = cursor.getString(cursor.getColumnIndex("ReactivePowerTotal"));
        value.ApparentPowerL1.s_Value = cursor.getString(cursor.getColumnIndex("ApparentPowerL1"));
        value.ApparentPowerL2.s_Value = cursor.getString(cursor.getColumnIndex("ApparentPowerL2"));
        value.ApparentPowerL3.s_Value = cursor.getString(cursor.getColumnIndex("ApparentPowerL3"));
        value.ApparentPowerTotal.s_Value = cursor.getString(cursor.getColumnIndex("ApparentPowerTotal"));
        value.PowerFactorL1.s_Value = cursor.getString(cursor.getColumnIndex("PowerFactorL1"));
        value.PowerFactorL2.s_Value = cursor.getString(cursor.getColumnIndex("PowerFactorL2"));
        value.PowerFactorL3.s_Value = cursor.getString(cursor.getColumnIndex("PowerFactorL3"));
        value.PowerFactorTotal.s_Value = cursor.getString(cursor.getColumnIndex("PowerFactorTotal"));
        value.BeginPower[0].s_Value = cursor.getString(cursor.getColumnIndex(TableColumns.ActiveBeginPower));
        value.EndPower[0].s_Value = cursor.getString(cursor.getColumnIndex(TableColumns.ActiveEndPower));
        value.StandardPower[0].s_Value = cursor.getString(cursor.getColumnIndex(TableColumns.ActiveStandardPower));
        value.Error[0].s_Value = cursor.getString(cursor.getColumnIndex("ActiveError"));
        value.Result[0].s_Value = cursor.getString(cursor.getColumnIndex("ActiveResult"));
        value.BeginPower[1].s_Value = cursor.getString(cursor.getColumnIndex(TableColumns.ReactiveBeginPower));
        value.EndPower[1].s_Value = cursor.getString(cursor.getColumnIndex(TableColumns.ReactiveEndPower));
        value.StandardPower[1].s_Value = cursor.getString(cursor.getColumnIndex(TableColumns.ReactiveStandardPower));
        value.Error[1].s_Value = cursor.getString(cursor.getColumnIndex("ReactiveError"));
        value.Result[1].s_Value = cursor.getString(cursor.getColumnIndex("ReactiveResult"));
        value.BeginPower[2].s_Value = cursor.getString(cursor.getColumnIndex(TableColumns.ApparentBeginPower));
        value.EndPower[2].s_Value = cursor.getString(cursor.getColumnIndex(TableColumns.ApparentEndPower));
        value.StandardPower[2].s_Value = cursor.getString(cursor.getColumnIndex(TableColumns.ApparentStandardPower));
        value.Error[2].s_Value = cursor.getString(cursor.getColumnIndex("ApparentError"));
        value.Result[2].s_Value = cursor.getString(cursor.getColumnIndex("ApparentResult"));
        return value;
    }
}
