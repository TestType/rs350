package com.clou.rs350.db.model;

import com.itextpdf.text.pdf.PdfObject;
import java.io.Serializable;

public class CustomerInfo implements Serializable {
    public String AddStreet = PdfObject.NOTHING;
    public String City = PdfObject.NOTHING;
    public String ConstactPhoneNumber = PdfObject.NOTHING;
    public String ContactPersonName = PdfObject.NOTHING;
    public String CustomerName = PdfObject.NOTHING;
    public String CustomerSr = PdfObject.NOTHING;
    public String District = PdfObject.NOTHING;
    public String Email = PdfObject.NOTHING;
    public String HouseNumber = PdfObject.NOTHING;
    public String Latitude = PdfObject.NOTHING;
    public String Longitude = PdfObject.NOTHING;
    public String Pin = PdfObject.NOTHING;
    public String State = PdfObject.NOTHING;
    public String TypeofConnection = PdfObject.NOTHING;
    public boolean isNew = true;
    private CustomerInfo m_Info = null;

    public CustomerInfo getCustomerInfo() {
        if (this.m_Info == null) {
            this.m_Info = new CustomerInfo();
        }
        return this.m_Info;
    }
}
