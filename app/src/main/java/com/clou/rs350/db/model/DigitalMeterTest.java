package com.clou.rs350.db.model;

import android.text.TextUtils;
import com.clou.rs350.device.MeterBaseDevice;
import com.clou.rs350.device.model.Baudrate;
import com.clou.rs350.device.model.ExplainedModel;
import com.clou.rs350.model.ClouData;
import com.itextpdf.text.pdf.PdfObject;

public class DigitalMeterTest extends DigitalMeterType {
    public int Address;
    public int CT;
    public Baudrate DeviceBaudrate;
    public DBDataModel[] Error1;
    public DBDataModel[] Error2;
    public DBDataModel[] Error3;
    public DBDataModel[] Error4;
    public DBDataModel[] Error5;
    public DBDataModel[] ErrorAverage;
    public int ErrorCount;
    public DBDataModel[] ErrorStandard;
    public String MeterIndex;
    public DBDataModel MeterP;
    public DBDataModel MeterQ;
    public DBDataModel MeterS;
    public String MeterSerialNo;
    public int PT;
    public DBDataModel Result;
    public String RunTime;
    public int deviceHasErrorCount;
    public int hasErrorCount;

    public DigitalMeterTest() {
        this.MeterSerialNo = PdfObject.NOTHING;
        this.Result = new DBDataModel(-1L, PdfObject.NOTHING);
        this.DeviceBaudrate = new Baudrate();
        this.Address = 0;
        this.PT = 1;
        this.CT = 1;
        this.MeterIndex = "0";
        this.ErrorCount = 5;
        this.RunTime = PdfObject.NOTHING;
        this.Error1 = new DBDataModel[]{new DBDataModel(), new DBDataModel(), new DBDataModel()};
        this.Error2 = new DBDataModel[]{new DBDataModel(), new DBDataModel(), new DBDataModel()};
        this.Error3 = new DBDataModel[]{new DBDataModel(), new DBDataModel(), new DBDataModel()};
        this.Error4 = new DBDataModel[]{new DBDataModel(), new DBDataModel(), new DBDataModel()};
        this.Error5 = new DBDataModel[]{new DBDataModel(), new DBDataModel(), new DBDataModel()};
        this.ErrorAverage = new DBDataModel[]{new DBDataModel(), new DBDataModel(), new DBDataModel()};
        this.ErrorStandard = new DBDataModel[]{new DBDataModel(), new DBDataModel(), new DBDataModel()};
        this.MeterP = new DBDataModel();
        this.MeterQ = new DBDataModel();
        this.MeterS = new DBDataModel();
        this.hasErrorCount = 0;
        this.deviceHasErrorCount = 0;
        this.DeviceBaudrate.baudrate = new DBDataModel(0L, "600");
    }

    public DigitalMeterTest(int meterIndex) {
        this();
        this.MeterIndex = new StringBuilder(String.valueOf(meterIndex)).toString();
    }

    public DigitalMeterTest(MeterBaseDevice Device, int meterNo) {
        this.MeterSerialNo = PdfObject.NOTHING;
        this.Result = new DBDataModel(-1L, PdfObject.NOTHING);
        this.DeviceBaudrate = new Baudrate();
        this.Address = 0;
        this.PT = 1;
        this.CT = 1;
        this.MeterIndex = "0";
        this.ErrorCount = 5;
        this.RunTime = PdfObject.NOTHING;
        this.Error1 = new DBDataModel[]{new DBDataModel(), new DBDataModel(), new DBDataModel()};
        this.Error2 = new DBDataModel[]{new DBDataModel(), new DBDataModel(), new DBDataModel()};
        this.Error3 = new DBDataModel[]{new DBDataModel(), new DBDataModel(), new DBDataModel()};
        this.Error4 = new DBDataModel[]{new DBDataModel(), new DBDataModel(), new DBDataModel()};
        this.Error5 = new DBDataModel[]{new DBDataModel(), new DBDataModel(), new DBDataModel()};
        this.ErrorAverage = new DBDataModel[]{new DBDataModel(), new DBDataModel(), new DBDataModel()};
        this.ErrorStandard = new DBDataModel[]{new DBDataModel(), new DBDataModel(), new DBDataModel()};
        this.MeterP = new DBDataModel();
        this.MeterQ = new DBDataModel();
        this.MeterS = new DBDataModel();
        this.hasErrorCount = 0;
        this.deviceHasErrorCount = 0;
        parseData(Device, meterNo);
    }

    public void parseData(MeterBaseDevice Device, int meterNo) {
        parseData(Device);
        ExplainedModel[] tmpValues = Device.getDigitalMeterErrorsValues();
        int tmpCount = tmpValues[3].getIntValue();
        int digit = ClouData.getInstance().getSettingBasic().ErrorDigit;
        if (tmpCount > this.deviceHasErrorCount) {
            this.deviceHasErrorCount = tmpCount;
            this.hasErrorCount++;
            int tmpCount2 = this.hasErrorCount;
            if (this.hasErrorCount >= 5) {
                tmpCount2 = 5;
            }
            for (int i = 0; i < 3; i++) {
                this.Error5[i] = this.Error4[i];
                this.Error4[i] = this.Error3[i];
                this.Error3[i] = this.Error2[i];
                this.Error2[i] = this.Error1[i];
                this.Error1[i] = parseRound(Double.valueOf(tmpValues[i].getDoubleValue()), digit);
                double tmpAverage = ((((this.Error1[i].d_Value + this.Error2[i].d_Value) + this.Error3[i].d_Value) + this.Error4[i].d_Value) + this.Error5[i].d_Value) / ((double) tmpCount2);
                this.ErrorAverage[i] = parseRound(Double.valueOf(tmpAverage), digit);
                if (this.hasErrorCount >= 5) {
                    this.ErrorStandard[i] = parseRound(Double.valueOf(Math.sqrt(((((Math.pow(this.Error1[i].d_Value - tmpAverage, 2.0d) + Math.pow(this.Error2[i].d_Value - tmpAverage, 2.0d)) + Math.pow(this.Error3[i].d_Value - tmpAverage, 2.0d)) + Math.pow(this.Error4[i].d_Value - tmpAverage, 2.0d)) + Math.pow(this.Error5[i].d_Value - tmpAverage, 2.0d)) / 5.0d)), digit);
                }
            }
            this.MeterP = parse(tmpValues[4]);
            this.MeterQ = parse(tmpValues[5]);
            this.MeterS = parse(tmpValues[6]);
        }
    }

    public void cleanError() {
        this.hasErrorCount = 0;
        this.deviceHasErrorCount = 0;
        this.Error1 = new DBDataModel[]{new DBDataModel(), new DBDataModel(), new DBDataModel()};
        this.Error2 = new DBDataModel[]{new DBDataModel(), new DBDataModel(), new DBDataModel()};
        this.Error3 = new DBDataModel[]{new DBDataModel(), new DBDataModel(), new DBDataModel()};
        this.Error4 = new DBDataModel[]{new DBDataModel(), new DBDataModel(), new DBDataModel()};
        this.Error5 = new DBDataModel[]{new DBDataModel(), new DBDataModel(), new DBDataModel()};
        this.ErrorAverage = new DBDataModel[]{new DBDataModel(), new DBDataModel(), new DBDataModel()};
        this.ErrorStandard = new DBDataModel[]{new DBDataModel(), new DBDataModel(), new DBDataModel()};
    }

    public String getError1(int pqFlag) {
        if (this.hasErrorCount < 1) {
            this.Error1[pqFlag].s_Value = "---";
        }
        return this.Error1[pqFlag].s_Value;
    }

    public String getError2(int pqFlag) {
        if (this.hasErrorCount < 2) {
            this.Error2[pqFlag].s_Value = "---";
        }
        return this.Error2[pqFlag].s_Value;
    }

    public String getError3(int pqFlag) {
        if (this.hasErrorCount < 3) {
            this.Error3[pqFlag].s_Value = "---";
        }
        return this.Error3[pqFlag].s_Value;
    }

    public String getError4(int pqFlag) {
        if (this.hasErrorCount < 4) {
            this.Error4[pqFlag].s_Value = "---";
        }
        return this.Error4[pqFlag].s_Value;
    }

    public String getError5(int pqFlag) {
        if (this.hasErrorCount < 5) {
            this.Error5[pqFlag].s_Value = "---";
        }
        return this.Error5[pqFlag].s_Value;
    }

    public String getErrorStandard(int pqFlag, int ErrorCount2) {
        if (this.hasErrorCount < ErrorCount2) {
            this.ErrorStandard[pqFlag].s_Value = "---";
        }
        return this.ErrorStandard[pqFlag].s_Value;
    }

    public String getErrorAverage(int pqFlag, int ErrorCount2) {
        if (this.hasErrorCount < ErrorCount2) {
            this.ErrorAverage[pqFlag].s_Value = "---";
        }
        return this.ErrorAverage[pqFlag].s_Value;
    }

    @Override // com.clou.rs350.db.model.BasicMeasurement
    public boolean isEmpty() {
        return TextUtils.isEmpty(this.Error1[0].s_Value) || "---".equals(this.Error1);
    }
}
