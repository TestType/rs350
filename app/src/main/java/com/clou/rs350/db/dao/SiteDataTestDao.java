package com.clou.rs350.db.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import com.clou.rs350.db.model.SiteData;
import com.clou.rs350.manager.DatabaseManager;
import com.clou.rs350.utils.InfoUtil;
import com.itextpdf.text.pdf.PdfObject;

public class SiteDataTestDao extends BaseTestDao {
    private static final String TABLE_NAME = "TestSiteData";

    private static final class TableColumns {
        public static final String CTAccuracyClass = "CTAccuracyClass";
        public static final String CTBurden = "CTBurden";
        public static final String CTEnable = "CTEnable";
        public static final String CTManufacturer = "CTManufacturer";
        public static final String CTPrimary_Installed = "CTPrimary_Installed";
        public static final String CTPrimary_Meter = "CTPrimary_Meter";
        public static final String CTSecondary_Installed = "CTSecondary_Installed";
        public static final String CTSecondary_Meter = "CTSecondary_Meter";
        public static final String CTType = "CTType";
        public static final String Meter1_ActiveAccuracy = "Meter1_ActiveAccuracy";
        public static final String Meter1_ActiveConstant = "Meter1_ActiveConstant";
        public static final String Meter1_ApparentAccuracy = "Meter1_ApparentAccuracy";
        public static final String Meter1_ApparentConstant = "Meter1_ApparentConstant";
        public static final String Meter1_BaseCurrent = "Meter1_BaseCurrent";
        public static final String Meter1_Certification_Year = "Meter1_Certification_Year";
        public static final String Meter1_Manufacturer = "Meter1_Manufacturer";
        public static final String Meter1_MaxCurrent = "Meter1_MaxCurrent";
        public static final String Meter1_NominalFrequency = "Meter1_NominalFrequency";
        public static final String Meter1_NominalVoltage = "Meter1_NominalVoltage";
        public static final String Meter1_Production_Year = "Meter1_Production_Year";
        public static final String Meter1_ReactiveAccuracy = "Meter1_ReactiveAccuracy";
        public static final String Meter1_ReactiveConstant = "Meter1_ReactiveConstant";
        public static final String Meter1_Sr = "Meter1_Sr";
        public static final String Meter1_Type = "Meter1_Type";
        public static final String Meter1_WiringType = "Meter1_WiringType";
        public static final String Meter2_ActiveAccuracy = "Meter2_ActiveAccuracy";
        public static final String Meter2_ActiveConstant = "Meter2_ActiveConstant";
        public static final String Meter2_ApparentAccuracy = "Meter2_ApparentAccuracy";
        public static final String Meter2_ApparentConstant = "Meter2_ApparentConstant";
        public static final String Meter2_BaseCurrent = "Meter2_BaseCurrent";
        public static final String Meter2_Certification_Year = "Meter2_Certification_Year";
        public static final String Meter2_Manufacturer = "Meter2_Manufacturer";
        public static final String Meter2_MaxCurrent = "Meter2_MaxCurrent";
        public static final String Meter2_NominalFrequency = "Meter2_NominalFrequency";
        public static final String Meter2_NominalVoltage = "Meter2_NominalVoltage";
        public static final String Meter2_Production_Year = "Meter2_Production_Year";
        public static final String Meter2_ReactiveAccuracy = "Meter2_ReactiveAccuracy";
        public static final String Meter2_ReactiveConstant = "Meter2_ReactiveConstant";
        public static final String Meter2_Sr = "Meter2_Sr";
        public static final String Meter2_Type = "Meter2_Type";
        public static final String Meter2_WiringType = "Meter2_WiringType";
        public static final String Meter3_ActiveAccuracy = "Meter3_ActiveAccuracy";
        public static final String Meter3_ActiveConstant = "Meter3_ActiveConstant";
        public static final String Meter3_ApparentAccuracy = "Meter3_ApparentAccuracy";
        public static final String Meter3_ApparentConstant = "Meter3_ApparentConstant";
        public static final String Meter3_BaseCurrent = "Meter3_BaseCurrent";
        public static final String Meter3_Certification_Year = "Meter3_Certification_Year";
        public static final String Meter3_Manufacturer = "Meter3_Manufacturer";
        public static final String Meter3_MaxCurrent = "Meter3_MaxCurrent";
        public static final String Meter3_NominalFrequency = "Meter3_NominalFrequency";
        public static final String Meter3_NominalVoltage = "Meter3_NominalVoltage";
        public static final String Meter3_Production_Year = "Meter3_Production_Year";
        public static final String Meter3_ReactiveAccuracy = "Meter3_ReactiveAccuracy";
        public static final String Meter3_ReactiveConstant = "Meter3_ReactiveConstant";
        public static final String Meter3_Sr = "Meter3_Sr";
        public static final String Meter3_Type = "Meter3_Type";
        public static final String Meter3_WiringType = "Meter3_WiringType";
        public static final String Meter_Count = "Meter_Count";
        public static final String MultiplyingFactor = "MultiplyingFactor";
        public static final String ObservedLoad = "ObservedLoad";
        public static final String PTAccuracyClass = "PTAccuracyClass";
        public static final String PTBurden = "PTBurden";
        public static final String PTEnable = "PTEnable";
        public static final String PTManufacturer = "PTManufacturer";
        public static final String PTPrimary_Installed = "PTPrimary_Installed";
        public static final String PTPrimary_Meter = "PTPrimary_Meter";
        public static final String PTSecondary_Installed = "PTSecondary_Installed";
        public static final String PTSecondary_Meter = "PTSecondary_Meter";
        public static final String RatioApply = "RatioApply";
        public static final String SanctionedLoad = "SanctionedLoad";
        public static final String WiringType = "WiringType";

        private TableColumns() {
        }
    }

    public SiteDataTestDao(Context context) {
        super(context, TABLE_NAME);
    }

    public void createTable(SQLiteDatabase db) {
        this.m_TableName = TABLE_NAME;
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("create table if not exists ").append(this.m_TableName).append(" (");
        stringBuffer.append("intMyID").append(" integer primary key autoincrement,");
        stringBuffer.append(this.m_TableKey).append(" VARCHAR(50),");
        stringBuffer.append("CreateTime").append(" VARCHAR(50),");
        stringBuffer.append("IsRead").append(" integer default 0,");
        stringBuffer.append("Meter_Count").append(" integer default 1,");
        stringBuffer.append("Meter1_Sr").append(" VARCHAR(50),");
        stringBuffer.append("Meter1_Type").append(" VARCHAR(50),");
        stringBuffer.append(TableColumns.Meter1_WiringType).append(" VARCHAR(50),");
        stringBuffer.append(TableColumns.Meter1_ActiveAccuracy).append(" VARCHAR(50),");
        stringBuffer.append(TableColumns.Meter1_ReactiveAccuracy).append(" VARCHAR(50),");
        stringBuffer.append(TableColumns.Meter1_ApparentAccuracy).append(" VARCHAR(50),");
        stringBuffer.append(TableColumns.Meter1_Manufacturer).append(" VARCHAR(50),");
        stringBuffer.append(TableColumns.Meter1_ActiveConstant).append(" VARCHAR(50),");
        stringBuffer.append(TableColumns.Meter1_ReactiveConstant).append(" VARCHAR(50),");
        stringBuffer.append(TableColumns.Meter1_ApparentConstant).append(" VARCHAR(50),");
        stringBuffer.append(TableColumns.Meter1_MaxCurrent).append(" VARCHAR(10),");
        stringBuffer.append(TableColumns.Meter1_BaseCurrent).append(" VARCHAR(10),");
        stringBuffer.append(TableColumns.Meter1_NominalVoltage).append(" VARCHAR(10),");
        stringBuffer.append(TableColumns.Meter1_NominalFrequency).append(" VARCHAR(10),");
        stringBuffer.append("Meter1_Production_Year").append(" VARCHAR(50),");
        stringBuffer.append("Meter1_Certification_Year").append(" VARCHAR(50),");
        stringBuffer.append("Meter2_Sr").append(" VARCHAR(50),");
        stringBuffer.append("Meter2_Type").append(" VARCHAR(50),");
        stringBuffer.append(TableColumns.Meter2_WiringType).append(" VARCHAR(50),");
        stringBuffer.append(TableColumns.Meter2_ActiveAccuracy).append(" VARCHAR(50),");
        stringBuffer.append(TableColumns.Meter2_ReactiveAccuracy).append(" VARCHAR(50),");
        stringBuffer.append(TableColumns.Meter2_ApparentAccuracy).append(" VARCHAR(50),");
        stringBuffer.append(TableColumns.Meter2_Manufacturer).append(" VARCHAR(50),");
        stringBuffer.append(TableColumns.Meter2_ActiveConstant).append(" VARCHAR(50),");
        stringBuffer.append(TableColumns.Meter2_ReactiveConstant).append(" VARCHAR(50),");
        stringBuffer.append(TableColumns.Meter2_ApparentConstant).append(" VARCHAR(50),");
        stringBuffer.append(TableColumns.Meter2_MaxCurrent).append(" VARCHAR(10),");
        stringBuffer.append(TableColumns.Meter2_BaseCurrent).append(" VARCHAR(10),");
        stringBuffer.append(TableColumns.Meter2_NominalVoltage).append(" VARCHAR(10),");
        stringBuffer.append(TableColumns.Meter2_NominalFrequency).append(" VARCHAR(10),");
        stringBuffer.append("Meter2_Production_Year").append(" VARCHAR(50),");
        stringBuffer.append("Meter2_Certification_Year").append(" VARCHAR(50),");
        stringBuffer.append("Meter3_Sr").append(" VARCHAR(50),");
        stringBuffer.append("Meter3_Type").append(" VARCHAR(50),");
        stringBuffer.append(TableColumns.Meter3_WiringType).append(" VARCHAR(50),");
        stringBuffer.append(TableColumns.Meter3_ActiveAccuracy).append(" VARCHAR(50),");
        stringBuffer.append(TableColumns.Meter3_ReactiveAccuracy).append(" VARCHAR(50),");
        stringBuffer.append(TableColumns.Meter3_ApparentAccuracy).append(" VARCHAR(50),");
        stringBuffer.append(TableColumns.Meter3_Manufacturer).append(" VARCHAR(50),");
        stringBuffer.append(TableColumns.Meter3_ActiveConstant).append(" VARCHAR(50),");
        stringBuffer.append(TableColumns.Meter3_ReactiveConstant).append(" VARCHAR(50),");
        stringBuffer.append(TableColumns.Meter3_ApparentConstant).append(" VARCHAR(50),");
        stringBuffer.append(TableColumns.Meter3_MaxCurrent).append(" VARCHAR(10),");
        stringBuffer.append(TableColumns.Meter3_BaseCurrent).append(" VARCHAR(10),");
        stringBuffer.append(TableColumns.Meter3_NominalVoltage).append(" VARCHAR(10),");
        stringBuffer.append(TableColumns.Meter3_NominalFrequency).append(" VARCHAR(10),");
        stringBuffer.append("Meter3_Production_Year").append(" VARCHAR(50),");
        stringBuffer.append("Meter3_Certification_Year").append(" VARCHAR(50),");
        stringBuffer.append(TableColumns.WiringType).append(" VARCHAR(50),");
        stringBuffer.append("PTEnable").append(" integer default 2,");
        stringBuffer.append("PTPrimary_Meter").append(" VARCHAR(20),");
        stringBuffer.append("PTSecondary_Meter").append(" VARCHAR(20),");
        stringBuffer.append("PTPrimary_Installed").append(" VARCHAR(20),");
        stringBuffer.append("PTSecondary_Installed").append(" VARCHAR(20),");
        stringBuffer.append("PTAccuracyClass").append(" VARCHAR(20),");
        stringBuffer.append("PTBurden").append(" VARCHAR(20),");
        stringBuffer.append("PTManufacturer").append(" VARCHAR(50),");
        stringBuffer.append("CTEnable").append(" integer default 2,");
        stringBuffer.append("CTType").append(" VARCHAR(20),");
        stringBuffer.append("CTPrimary_Meter").append(" VARCHAR(20),");
        stringBuffer.append("CTSecondary_Meter").append(" VARCHAR(20),");
        stringBuffer.append("CTPrimary_Installed").append(" VARCHAR(20),");
        stringBuffer.append("CTSecondary_Installed").append(" VARCHAR(20),");
        stringBuffer.append("CTAccuracyClass").append(" VARCHAR(20),");
        stringBuffer.append("CTBurden").append(" VARCHAR(20),");
        stringBuffer.append("CTManufacturer").append(" VARCHAR(50),");
        stringBuffer.append("RatioApply").append(" integer default 0,");
        stringBuffer.append("MultiplyingFactor").append(" VARCHAR(50),");
        stringBuffer.append("SanctionedLoad").append(" VARCHAR(50),");
        stringBuffer.append("ObservedLoad").append(" VARCHAR(50));");
        db.execSQL(stringBuffer.toString());
    }

    @Override // com.clou.rs350.db.dao.BaseDao, com.clou.rs350.db.dao.BaseTestDao
    public void upgradeTable(SQLiteDatabase db, int oldVersion, int newVersion) {
        if (oldVersion <= 3) {
            db.execSQL("alter table SiteDataTest rename to TestSiteData");
        }
        if (oldVersion <= 4) {
            db.execSQL("alter table TestSiteData add column PTManufacturer VARCHAR(50)");
            db.execSQL("alter table TestSiteData add column CTManufacturer VARCHAR(50)");
        }
        if (oldVersion <= 6) {
            db.execSQL("alter table TestSiteData add column Meter1_WiringType VARCHAR(50)");
            db.execSQL("alter table TestSiteData add column Meter1_Manufacturer VARCHAR(50)");
            db.execSQL("alter table TestSiteData add column Meter1_ActiveAccuracy VARCHAR(50)");
            db.execSQL("alter table TestSiteData add column Meter1_ReactiveAccuracy VARCHAR(50)");
            db.execSQL("alter table TestSiteData add column Meter1_ApparentAccuracy VARCHAR(50)");
            db.execSQL("alter table TestSiteData add column Meter1_ActiveConstant VARCHAR(50)");
            db.execSQL("alter table TestSiteData add column Meter1_ReactiveConstant VARCHAR(50)");
            db.execSQL("alter table TestSiteData add column Meter1_ApparentConstant VARCHAR(50)");
            db.execSQL("alter table TestSiteData add column Meter1_MaxCurrent VARCHAR(10)");
            db.execSQL("alter table TestSiteData add column Meter1_BaseCurrent VARCHAR(10)");
            db.execSQL("alter table TestSiteData add column Meter1_NominalVoltage VARCHAR(10)");
            db.execSQL("alter table TestSiteData add column Meter1_NominalFrequency VARCHAR(10)");
            db.execSQL("alter table TestSiteData add column Meter2_WiringType VARCHAR(50)");
            db.execSQL("alter table TestSiteData add column Meter2_Manufacturer VARCHAR(50)");
            db.execSQL("alter table TestSiteData add column Meter2_ActiveAccuracy VARCHAR(50)");
            db.execSQL("alter table TestSiteData add column Meter2_ReactiveAccuracy VARCHAR(50)");
            db.execSQL("alter table TestSiteData add column Meter2_ApparentAccuracy VARCHAR(50)");
            db.execSQL("alter table TestSiteData add column Meter2_ActiveConstant VARCHAR(50)");
            db.execSQL("alter table TestSiteData add column Meter2_ReactiveConstant VARCHAR(50)");
            db.execSQL("alter table TestSiteData add column Meter2_ApparentConstant VARCHAR(50)");
            db.execSQL("alter table TestSiteData add column Meter2_MaxCurrent VARCHAR(10)");
            db.execSQL("alter table TestSiteData add column Meter2_BaseCurrent VARCHAR(10)");
            db.execSQL("alter table TestSiteData add column Meter2_NominalVoltage VARCHAR(10)");
            db.execSQL("alter table TestSiteData add column Meter2_NominalFrequency VARCHAR(10)");
            db.execSQL("alter table TestSiteData add column Meter3_WiringType VARCHAR(50)");
            db.execSQL("alter table TestSiteData add column Meter3_Manufacturer VARCHAR(50)");
            db.execSQL("alter table TestSiteData add column Meter3_ActiveAccuracy VARCHAR(50)");
            db.execSQL("alter table TestSiteData add column Meter3_ReactiveAccuracy VARCHAR(50)");
            db.execSQL("alter table TestSiteData add column Meter3_ApparentAccuracy VARCHAR(50)");
            db.execSQL("alter table TestSiteData add column Meter3_ActiveConstant VARCHAR(50)");
            db.execSQL("alter table TestSiteData add column Meter3_ReactiveConstant VARCHAR(50)");
            db.execSQL("alter table TestSiteData add column Meter3_ApparentConstant VARCHAR(50)");
            db.execSQL("alter table TestSiteData add column Meter3_MaxCurrent VARCHAR(10)");
            db.execSQL("alter table TestSiteData add column Meter3_BaseCurrent VARCHAR(10)");
            db.execSQL("alter table TestSiteData add column Meter3_NominalVoltage VARCHAR(10)");
            db.execSQL("alter table TestSiteData add column Meter3_NominalFrequency VARCHAR(10)");
        }
    }

    @Override // com.clou.rs350.db.dao.BaseDao, com.clou.rs350.db.dao.BaseTestDao
    public synchronized boolean insertObject(Object Data, String Time) {
        boolean z = true;
        synchronized (this) {
            SQLiteDatabase db = DatabaseManager.getWriteableDatabase(this.m_Context);
            ContentValues values = new ContentValues();
            SiteData object = (SiteData) Data;
            values.put(this.m_TableKey, object.CustomerSr);
            values.put("CreateTime", Time);
            values.put("Meter_Count", Integer.valueOf(object.Meter_Count));
            values.put("Meter1_Sr", object.MeterInfo[0].SerialNo);
            values.put("Meter1_Type", object.MeterInfo[0].Type);
            values.put(TableColumns.Meter1_WiringType, object.MeterInfo[0].WiringType);
            values.put(TableColumns.Meter1_ActiveAccuracy, object.MeterInfo[0].ActiveAccuracy);
            values.put(TableColumns.Meter1_ReactiveAccuracy, object.MeterInfo[0].ReactiveAccuracy);
            values.put(TableColumns.Meter1_ApparentAccuracy, object.MeterInfo[0].ApparentAccuracy);
            values.put(TableColumns.Meter1_Manufacturer, object.MeterInfo[0].Manufacturer);
            values.put(TableColumns.Meter1_ActiveConstant, object.MeterInfo[0].ActiveConstant);
            values.put(TableColumns.Meter1_ReactiveConstant, object.MeterInfo[0].ReactiveConstant);
            values.put(TableColumns.Meter1_ApparentConstant, object.MeterInfo[0].ApparentConstant);
            values.put(TableColumns.Meter1_MaxCurrent, object.MeterInfo[0].MaxCurrent);
            values.put(TableColumns.Meter1_BaseCurrent, object.MeterInfo[0].BasicCurrent);
            values.put(TableColumns.Meter1_NominalVoltage, object.MeterInfo[0].NominalVoltage);
            values.put(TableColumns.Meter1_NominalFrequency, object.MeterInfo[0].NominalFrequency);
            values.put("Meter1_Production_Year", object.MeterInfo[0].ProductionYear);
            values.put("Meter1_Certification_Year", object.Meter_CertificationYear[0]);
            values.put("Meter2_Sr", object.MeterInfo[1].SerialNo);
            values.put("Meter2_Type", object.MeterInfo[1].Type);
            values.put(TableColumns.Meter2_WiringType, object.MeterInfo[1].WiringType);
            values.put(TableColumns.Meter2_ActiveAccuracy, object.MeterInfo[1].ActiveAccuracy);
            values.put(TableColumns.Meter2_ReactiveAccuracy, object.MeterInfo[1].ReactiveAccuracy);
            values.put(TableColumns.Meter2_ApparentAccuracy, object.MeterInfo[1].ApparentAccuracy);
            values.put(TableColumns.Meter2_Manufacturer, object.MeterInfo[1].Manufacturer);
            values.put(TableColumns.Meter2_ActiveConstant, object.MeterInfo[1].ActiveConstant);
            values.put(TableColumns.Meter2_ReactiveConstant, object.MeterInfo[1].ReactiveConstant);
            values.put(TableColumns.Meter2_ApparentConstant, object.MeterInfo[1].ApparentConstant);
            values.put(TableColumns.Meter2_MaxCurrent, object.MeterInfo[1].MaxCurrent);
            values.put(TableColumns.Meter2_BaseCurrent, object.MeterInfo[1].BasicCurrent);
            values.put(TableColumns.Meter2_NominalVoltage, object.MeterInfo[1].NominalVoltage);
            values.put(TableColumns.Meter2_NominalFrequency, object.MeterInfo[1].NominalFrequency);
            values.put("Meter2_Production_Year", object.MeterInfo[1].ProductionYear);
            values.put("Meter2_Certification_Year", object.Meter_CertificationYear[1]);
            values.put("Meter3_Sr", object.MeterInfo[2].SerialNo);
            values.put("Meter3_Type", object.MeterInfo[2].Type);
            values.put(TableColumns.Meter3_WiringType, object.MeterInfo[2].WiringType);
            values.put(TableColumns.Meter3_ActiveAccuracy, object.MeterInfo[2].ActiveAccuracy);
            values.put(TableColumns.Meter3_ReactiveAccuracy, object.MeterInfo[2].ReactiveAccuracy);
            values.put(TableColumns.Meter3_ApparentAccuracy, object.MeterInfo[2].ApparentAccuracy);
            values.put(TableColumns.Meter3_Manufacturer, object.MeterInfo[2].Manufacturer);
            values.put(TableColumns.Meter3_ActiveConstant, object.MeterInfo[2].ActiveConstant);
            values.put(TableColumns.Meter3_ReactiveConstant, object.MeterInfo[2].ReactiveConstant);
            values.put(TableColumns.Meter3_ApparentConstant, object.MeterInfo[2].ApparentConstant);
            values.put(TableColumns.Meter3_MaxCurrent, object.MeterInfo[2].MaxCurrent);
            values.put(TableColumns.Meter3_BaseCurrent, object.MeterInfo[2].BasicCurrent);
            values.put(TableColumns.Meter3_NominalVoltage, object.MeterInfo[2].NominalVoltage);
            values.put(TableColumns.Meter3_NominalFrequency, object.MeterInfo[2].NominalFrequency);
            values.put("Meter3_Production_Year", object.MeterInfo[2].ProductionYear);
            values.put("Meter3_Certification_Year", object.Meter_CertificationYear[2]);
            values.put(TableColumns.WiringType, object.WiringType);
            values.put("PTEnable", Integer.valueOf(object.PTEnable));
            values.put("PTPrimary_Meter", object.PTPrimary.s_Value);
            values.put("PTSecondary_Meter", object.PTSecondary.s_Value);
            values.put("PTPrimary_Installed", object.PTPrimary_Installed.s_Value);
            values.put("PTSecondary_Installed", object.PTSecondary_Installed.s_Value);
            values.put("PTAccuracyClass", object.PTAccuracyClass);
            values.put("PTBurden", object.PTBurden.s_Value);
            values.put("PTManufacturer", object.PTManufacturer);
            values.put("CTEnable", Integer.valueOf(object.CTEnable));
            values.put("CTType", object.CTType);
            values.put("CTPrimary_Meter", object.CTPrimary.s_Value);
            values.put("CTSecondary_Meter", object.CTSecondary.s_Value);
            values.put("CTPrimary_Installed", object.CTPrimary_Installed.s_Value);
            values.put("CTSecondary_Installed", object.CTSecondary_Installed.s_Value);
            values.put("CTAccuracyClass", object.CTAccuracyClass);
            values.put("CTBurden", object.CTBurden.s_Value);
            values.put("CTManufacturer", object.CTManufacturer);
            values.put("RatioApply", Integer.valueOf(object.RatioApply));
            values.put("MultiplyingFactor", object.MultiplyingFactor);
            values.put("SanctionedLoad", object.SanctionedLoad);
            values.put("ObservedLoad", object.ObservedLoad);
            if (0 + db.insert(TABLE_NAME, null, values) == -1) {
                z = false;
            }
        }
        return z;
    }

    /* access modifiers changed from: protected */
    @Override // com.clou.rs350.db.dao.BaseDao, com.clou.rs350.db.dao.BaseTestDao
    public Object parseData(Cursor cursor) {
        SiteData value = new SiteData();
        value.CustomerSr = cursor.getString(cursor.getColumnIndex(this.m_TableKey));
        value.Meter_Count = cursor.getInt(cursor.getColumnIndex("Meter_Count"));
        value.MeterInfo[0].SerialNo = parseString(cursor.getString(cursor.getColumnIndex("Meter1_Sr")));
        value.MeterInfo[0].Type = parseString(cursor.getString(cursor.getColumnIndex("Meter1_Type")));
        value.MeterInfo[0].WiringType = parseString(cursor.getString(cursor.getColumnIndex(TableColumns.Meter1_WiringType)));
        value.MeterInfo[0].ActiveAccuracy = parseString(cursor.getString(cursor.getColumnIndex(TableColumns.Meter1_ActiveAccuracy))).replace(" ", PdfObject.NOTHING);
        value.MeterInfo[0].ReactiveAccuracy = parseString(cursor.getString(cursor.getColumnIndex(TableColumns.Meter1_ReactiveAccuracy))).replace(" ", PdfObject.NOTHING);
        value.MeterInfo[0].ApparentAccuracy = parseString(cursor.getString(cursor.getColumnIndex(TableColumns.Meter1_ApparentAccuracy))).replace(" ", PdfObject.NOTHING);
        value.MeterInfo[0].Manufacturer = parseString(cursor.getString(cursor.getColumnIndex(TableColumns.Meter1_Manufacturer)));
        value.MeterInfo[0].ActiveConstant = parseString(cursor.getString(cursor.getColumnIndex(TableColumns.Meter1_ActiveConstant))).replace(" ", PdfObject.NOTHING);
        value.MeterInfo[0].ReactiveConstant = parseString(cursor.getString(cursor.getColumnIndex(TableColumns.Meter1_ReactiveConstant))).replace(" ", PdfObject.NOTHING);
        value.MeterInfo[0].ApparentConstant = parseString(cursor.getString(cursor.getColumnIndex(TableColumns.Meter1_ApparentConstant))).replace(" ", PdfObject.NOTHING);
        value.MeterInfo[0].MaxCurrent = parseString(cursor.getString(cursor.getColumnIndex(TableColumns.Meter1_MaxCurrent))).replace(" ", PdfObject.NOTHING);
        value.MeterInfo[0].BasicCurrent = parseString(cursor.getString(cursor.getColumnIndex(TableColumns.Meter1_BaseCurrent))).replace(" ", PdfObject.NOTHING);
        value.MeterInfo[0].NominalVoltage = parseString(cursor.getString(cursor.getColumnIndex(TableColumns.Meter1_NominalVoltage))).replace(" ", PdfObject.NOTHING);
        value.MeterInfo[0].NominalFrequency = parseString(cursor.getString(cursor.getColumnIndex(TableColumns.Meter1_NominalFrequency))).replace(" ", PdfObject.NOTHING);
        value.MeterInfo[0].ProductionYear = parseString(cursor.getString(cursor.getColumnIndex("Meter1_Production_Year")));
        value.Meter_CertificationYear[0] = parseString(cursor.getString(cursor.getColumnIndex("Meter1_Certification_Year")));
        value.MeterInfo[1].SerialNo = parseString(cursor.getString(cursor.getColumnIndex("Meter2_Sr")));
        value.MeterInfo[1].Type = parseString(cursor.getString(cursor.getColumnIndex("Meter2_Type")));
        value.MeterInfo[1].WiringType = parseString(cursor.getString(cursor.getColumnIndex(TableColumns.Meter2_WiringType)));
        value.MeterInfo[1].ActiveAccuracy = parseString(cursor.getString(cursor.getColumnIndex(TableColumns.Meter2_ActiveAccuracy))).replace(" ", PdfObject.NOTHING);
        value.MeterInfo[1].ReactiveAccuracy = parseString(cursor.getString(cursor.getColumnIndex(TableColumns.Meter2_ReactiveAccuracy))).replace(" ", PdfObject.NOTHING);
        value.MeterInfo[1].ApparentAccuracy = parseString(cursor.getString(cursor.getColumnIndex(TableColumns.Meter2_ApparentAccuracy))).replace(" ", PdfObject.NOTHING);
        value.MeterInfo[1].Manufacturer = parseString(cursor.getString(cursor.getColumnIndex(TableColumns.Meter2_Manufacturer)));
        value.MeterInfo[1].ActiveConstant = parseString(cursor.getString(cursor.getColumnIndex(TableColumns.Meter2_ActiveConstant))).replace(" ", PdfObject.NOTHING);
        value.MeterInfo[1].ReactiveConstant = parseString(cursor.getString(cursor.getColumnIndex(TableColumns.Meter2_ReactiveConstant))).replace(" ", PdfObject.NOTHING);
        value.MeterInfo[1].ApparentConstant = parseString(cursor.getString(cursor.getColumnIndex(TableColumns.Meter2_ApparentConstant))).replace(" ", PdfObject.NOTHING);
        value.MeterInfo[1].MaxCurrent = parseString(cursor.getString(cursor.getColumnIndex(TableColumns.Meter2_MaxCurrent))).replace(" ", PdfObject.NOTHING);
        value.MeterInfo[1].BasicCurrent = parseString(cursor.getString(cursor.getColumnIndex(TableColumns.Meter2_BaseCurrent))).replace(" ", PdfObject.NOTHING);
        value.MeterInfo[1].NominalVoltage = parseString(cursor.getString(cursor.getColumnIndex(TableColumns.Meter2_NominalVoltage))).replace(" ", PdfObject.NOTHING);
        value.MeterInfo[1].NominalFrequency = parseString(cursor.getString(cursor.getColumnIndex(TableColumns.Meter2_NominalFrequency))).replace(" ", PdfObject.NOTHING);
        value.MeterInfo[1].ProductionYear = parseString(cursor.getString(cursor.getColumnIndex("Meter2_Production_Year")));
        value.Meter_CertificationYear[1] = parseString(cursor.getString(cursor.getColumnIndex("Meter2_Certification_Year")));
        value.MeterInfo[2].SerialNo = parseString(cursor.getString(cursor.getColumnIndex("Meter3_Sr")));
        value.MeterInfo[2].Type = parseString(cursor.getString(cursor.getColumnIndex("Meter3_Type")));
        value.MeterInfo[2].WiringType = parseString(cursor.getString(cursor.getColumnIndex(TableColumns.WiringType)));
        value.MeterInfo[2].ActiveAccuracy = parseString(cursor.getString(cursor.getColumnIndex(TableColumns.Meter3_ActiveAccuracy))).replace(" ", PdfObject.NOTHING);
        value.MeterInfo[2].ReactiveAccuracy = parseString(cursor.getString(cursor.getColumnIndex(TableColumns.Meter3_ReactiveAccuracy))).replace(" ", PdfObject.NOTHING);
        value.MeterInfo[2].ApparentAccuracy = parseString(cursor.getString(cursor.getColumnIndex(TableColumns.Meter3_ApparentAccuracy))).replace(" ", PdfObject.NOTHING);
        value.MeterInfo[2].Manufacturer = parseString(cursor.getString(cursor.getColumnIndex(TableColumns.Meter3_Manufacturer)));
        value.MeterInfo[2].ActiveConstant = parseString(cursor.getString(cursor.getColumnIndex(TableColumns.Meter3_ActiveConstant))).replace(" ", PdfObject.NOTHING);
        value.MeterInfo[2].ReactiveConstant = parseString(cursor.getString(cursor.getColumnIndex(TableColumns.Meter3_ReactiveConstant))).replace(" ", PdfObject.NOTHING);
        value.MeterInfo[2].ApparentConstant = parseString(cursor.getString(cursor.getColumnIndex(TableColumns.Meter3_ApparentConstant))).replace(" ", PdfObject.NOTHING);
        value.MeterInfo[2].MaxCurrent = parseString(cursor.getString(cursor.getColumnIndex(TableColumns.Meter3_MaxCurrent))).replace(" ", PdfObject.NOTHING);
        value.MeterInfo[2].BasicCurrent = parseString(cursor.getString(cursor.getColumnIndex(TableColumns.Meter3_BaseCurrent))).replace(" ", PdfObject.NOTHING);
        value.MeterInfo[2].NominalVoltage = parseString(cursor.getString(cursor.getColumnIndex(TableColumns.Meter3_NominalVoltage))).replace(" ", PdfObject.NOTHING);
        value.MeterInfo[2].NominalFrequency = parseString(cursor.getString(cursor.getColumnIndex(TableColumns.Meter3_NominalFrequency))).replace(" ", PdfObject.NOTHING);
        value.MeterInfo[2].ProductionYear = parseString(cursor.getString(cursor.getColumnIndex("Meter3_Production_Year")));
        value.Meter_CertificationYear[2] = parseString(cursor.getString(cursor.getColumnIndex("Meter3_Certification_Year")));
        value.WiringType = parseString(cursor.getString(cursor.getColumnIndex(TableColumns.WiringType)));
        value.PTEnable = cursor.getInt(cursor.getColumnIndex("PTEnable"));
        value.CTEnable = cursor.getInt(cursor.getColumnIndex("CTEnable"));
        value.PTPrimary = InfoUtil.parseVoltageRatio(parseString(cursor.getString(cursor.getColumnIndex("PTPrimary_Meter"))), true);
        value.PTSecondary = InfoUtil.parseVoltageRatio(parseString(cursor.getString(cursor.getColumnIndex("PTSecondary_Meter"))), false);
        value.PTPrimary_Installed = InfoUtil.parseVoltageRatio(parseString(cursor.getString(cursor.getColumnIndex("PTPrimary_Installed"))), true);
        value.PTSecondary_Installed = InfoUtil.parseVoltageRatio(parseString(cursor.getString(cursor.getColumnIndex("PTSecondary_Installed"))), false);
        value.PTAccuracyClass = parseString(cursor.getString(cursor.getColumnIndex("PTAccuracyClass")));
        value.PTBurden = InfoUtil.parseBurden(parseString(cursor.getString(cursor.getColumnIndex("PTBurden"))));
        value.PTManufacturer = parseString(cursor.getString(cursor.getColumnIndex("PTManufacturer")));
        value.CTType = parseString(cursor.getString(cursor.getColumnIndex("CTType")));
        value.CTPrimary = InfoUtil.parseCurrentRatio(parseString(cursor.getString(cursor.getColumnIndex("CTPrimary_Meter"))));
        value.CTSecondary = InfoUtil.parseCurrentRatio(parseString(cursor.getString(cursor.getColumnIndex("CTSecondary_Meter"))));
        value.CTPrimary_Installed = InfoUtil.parseCurrentRatio(parseString(cursor.getString(cursor.getColumnIndex("CTPrimary_Installed"))));
        value.CTSecondary_Installed = InfoUtil.parseCurrentRatio(parseString(cursor.getString(cursor.getColumnIndex("CTSecondary_Installed"))));
        value.CTAccuracyClass = parseString(cursor.getString(cursor.getColumnIndex("CTAccuracyClass")));
        value.CTBurden = InfoUtil.parseBurden(parseString(cursor.getString(cursor.getColumnIndex("CTBurden"))));
        value.CTManufacturer = parseString(cursor.getString(cursor.getColumnIndex("CTManufacturer")));
        value.RatioApply = cursor.getInt(cursor.getColumnIndex("RatioApply"));
        value.MultiplyingFactor = parseString(cursor.getString(cursor.getColumnIndex("MultiplyingFactor")));
        value.SanctionedLoad = parseString(cursor.getString(cursor.getColumnIndex("SanctionedLoad")));
        value.ObservedLoad = parseString(cursor.getString(cursor.getColumnIndex("ObservedLoad")));
        return value;
    }
}
