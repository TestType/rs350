package com.clou.rs350.db.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import com.clou.rs350.db.dao.BaseTestDao;
import com.clou.rs350.db.model.ReadMeter;
import com.clou.rs350.manager.DatabaseManager;

public class ReadMeterDao extends BaseTestDao {
    public static final String TABLE_NAME = "TestReadMeter";

    private static final class TableColumns {
        public static final String ActivePowerTotal = "ActivePowerTotal";
        public static final String Address = "Address";
        public static final String CurrentValueL1 = "CurrentValueL1";
        public static final String CurrentValueL2 = "CurrentValueL2";
        public static final String CurrentValueL3 = "CurrentValueL3";
        public static final String Energy1 = "Energy1";
        public static final String Energy10 = "Energy10";
        public static final String Energy11 = "Energy11";
        public static final String Energy12 = "Energy12";
        public static final String Energy13 = "Energy13";
        public static final String Energy14 = "Energy14";
        public static final String Energy15 = "Energy15";
        public static final String Energy16 = "Energy16";
        public static final String Energy17 = "Energy17";
        public static final String Energy18 = "Energy18";
        public static final String Energy19 = "Energy19";
        public static final String Energy2 = "Energy2";
        public static final String Energy20 = "Energy20";
        public static final String Energy3 = "Energy3";
        public static final String Energy4 = "Energy4";
        public static final String Energy5 = "Energy5";
        public static final String Energy6 = "Energy6";
        public static final String Energy7 = "Energy7";
        public static final String Energy8 = "Energy8";
        public static final String Energy9 = "Energy9";
        public static final String MeterIndex = "MeterIndex";
        public static final String ReactivePowerTotal = "ReactivePowerTotal";
        public static final String StandardTime = "StandardTime";
        public static final String Time = "Time";
        public static final String TimeError = "TimeError";
        public static final String VoltageValueL1 = "VoltageValueL1";
        public static final String VoltageValueL2 = "VoltageValueL2";
        public static final String VoltageValueL3 = "VoltageValueL3";

        private TableColumns() {
        }
    }

    public ReadMeterDao(Context context) {
        super(context, TABLE_NAME);
    }

    public void createTable(SQLiteDatabase db) {
        this.m_TableName = TABLE_NAME;
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("create table if not exists ").append(this.m_TableName).append(" (");
        stringBuffer.append("intMyID").append(" integer primary key autoincrement,");
        stringBuffer.append(this.m_TableKey).append(" VARCHAR(50),");
        stringBuffer.append("CreateTime").append(" VARCHAR(50),");
        stringBuffer.append("IsRead").append(" integer default 0,");
        stringBuffer.append(SameColumns.MeterSerialNo).append(" VARCHAR(50),");
        stringBuffer.append("MeterIndex").append(" VARCHAR(5),");
        stringBuffer.append(TableColumns.Address).append(" VARCHAR(50),");
        stringBuffer.append(TableColumns.Time).append(" VARCHAR(20),");
        stringBuffer.append(TableColumns.StandardTime).append(" VARCHAR(20),");
        stringBuffer.append(TableColumns.TimeError).append(" VARCHAR(20),");
        stringBuffer.append("VoltageValueL1").append(" VARCHAR(20),");
        stringBuffer.append("VoltageValueL2").append(" VARCHAR(20),");
        stringBuffer.append("VoltageValueL3").append(" VARCHAR(20),");
        stringBuffer.append("CurrentValueL1").append(" VARCHAR(20),");
        stringBuffer.append("CurrentValueL2").append(" VARCHAR(20),");
        stringBuffer.append("CurrentValueL3").append(" VARCHAR(20),");
        stringBuffer.append("ActivePowerTotal").append(" VARCHAR(20),");
        stringBuffer.append("ReactivePowerTotal").append(" VARCHAR(20),");
        stringBuffer.append("Energy1").append(" VARCHAR(20),");
        stringBuffer.append("Energy2").append(" VARCHAR(20),");
        stringBuffer.append("Energy3").append(" VARCHAR(20),");
        stringBuffer.append("Energy4").append(" VARCHAR(20),");
        stringBuffer.append("Energy5").append(" VARCHAR(20),");
        stringBuffer.append("Energy6").append(" VARCHAR(20),");
        stringBuffer.append("Energy7").append(" VARCHAR(20),");
        stringBuffer.append("Energy8").append(" VARCHAR(20),");
        stringBuffer.append("Energy9").append(" VARCHAR(20),");
        stringBuffer.append("Energy10").append(" VARCHAR(20),");
        stringBuffer.append(TableColumns.Energy11).append(" VARCHAR(20),");
        stringBuffer.append(TableColumns.Energy12).append(" VARCHAR(20),");
        stringBuffer.append(TableColumns.Energy13).append(" VARCHAR(20),");
        stringBuffer.append(TableColumns.Energy14).append(" VARCHAR(20),");
        stringBuffer.append(TableColumns.Energy15).append(" VARCHAR(20),");
        stringBuffer.append(TableColumns.Energy16).append(" VARCHAR(20),");
        stringBuffer.append(TableColumns.Energy17).append(" VARCHAR(20),");
        stringBuffer.append(TableColumns.Energy18).append(" VARCHAR(20),");
        stringBuffer.append(TableColumns.Energy19).append(" VARCHAR(20),");
        stringBuffer.append(TableColumns.Energy20).append(" VARCHAR(20));");
        db.execSQL(stringBuffer.toString());
    }

    @Override // com.clou.rs350.db.dao.BaseDao, com.clou.rs350.db.dao.BaseTestDao
    public void upgradeTable(SQLiteDatabase db, int oldVersion, int newVersion) {
        if (oldVersion < 12) {
            createTable(db);
        } else if (oldVersion < 13) {
            db.execSQL("alter table TestReadMeter add column MeterIndex VARCHAR(5)");
        } else if (oldVersion < 14) {
            db.execSQL("alter table TestReadMeter add column StandardTime VARCHAR(20)");
            db.execSQL("alter table TestReadMeter add column TimeError VARCHAR(20)");
        }
    }

    @Override // com.clou.rs350.db.dao.BaseDao, com.clou.rs350.db.dao.BaseTestDao
    public synchronized boolean insertObject(Object Data, String Time) {
        boolean z = true;
        synchronized (this) {
            SQLiteDatabase db = DatabaseManager.getWriteableDatabase(this.m_Context);
            ContentValues values = new ContentValues();
            ReadMeter object = (ReadMeter) Data;
            values.put(this.m_TableKey, object.CustomerSerialNo);
            values.put("CreateTime", Time);
            values.put("MeterIndex", object.MeterIndex);
            values.put(TableColumns.Address, object.Address);
            values.put(TableColumns.Time, object.DateAndTime);
            values.put(TableColumns.StandardTime, object.StandardTime);
            values.put(TableColumns.TimeError, object.TimeError);
            values.put("VoltageValueL1", object.U1);
            values.put("VoltageValueL2", object.U2);
            values.put("VoltageValueL3", object.U3);
            values.put("CurrentValueL1", object.I1);
            values.put("CurrentValueL2", object.I2);
            values.put("CurrentValueL3", object.I3);
            values.put("ActivePowerTotal", object.Psum);
            values.put("ReactivePowerTotal", object.Qsum);
            values.put("Energy1", object.Energys[0]);
            values.put("Energy2", object.Energys[1]);
            values.put("Energy3", object.Energys[2]);
            values.put("Energy4", object.Energys[3]);
            values.put("Energy5", object.Energys[4]);
            values.put("Energy6", object.Energys[5]);
            values.put("Energy7", object.Energys[6]);
            values.put("Energy8", object.Energys[7]);
            values.put("Energy9", object.Energys[8]);
            values.put("Energy10", object.Energys[9]);
            values.put(TableColumns.Energy11, object.Energys[10]);
            values.put(TableColumns.Energy12, object.Energys[11]);
            values.put(TableColumns.Energy13, object.Energys[12]);
            values.put(TableColumns.Energy14, object.Energys[13]);
            values.put(TableColumns.Energy15, object.Energys[14]);
            values.put(TableColumns.Energy16, object.Energys[15]);
            values.put(TableColumns.Energy17, object.Energys[16]);
            values.put(TableColumns.Energy18, object.Energys[17]);
            values.put(TableColumns.Energy19, object.Energys[18]);
            values.put(TableColumns.Energy20, object.Energys[19]);
            if (db.insert(TABLE_NAME, null, values) == -1) {
                z = false;
            }
        }
        return z;
    }

    /* access modifiers changed from: protected */
    @Override // com.clou.rs350.db.dao.BaseDao, com.clou.rs350.db.dao.BaseTestDao
    public Object parseData(Cursor cursor) {
        ReadMeter value = new ReadMeter();
        value.CustomerSerialNo = parseString(cursor.getString(cursor.getColumnIndex(this.m_TableKey)));
        value.MeterIndex = parseString(cursor.getString(cursor.getColumnIndex("MeterIndex")));
        value.Address = parseString(cursor.getString(cursor.getColumnIndex(TableColumns.Address)));
        value.DateAndTime = parseString(cursor.getString(cursor.getColumnIndex(TableColumns.Time)));
        value.StandardTime = parseString(cursor.getString(cursor.getColumnIndex(TableColumns.StandardTime)));
        value.TimeError = parseString(cursor.getString(cursor.getColumnIndex(TableColumns.TimeError)));
        value.U1 = parseString(cursor.getString(cursor.getColumnIndex("VoltageValueL1")));
        value.U2 = parseString(cursor.getString(cursor.getColumnIndex("VoltageValueL2")));
        value.U3 = parseString(cursor.getString(cursor.getColumnIndex("VoltageValueL3")));
        value.I1 = parseString(cursor.getString(cursor.getColumnIndex("CurrentValueL1")));
        value.I2 = parseString(cursor.getString(cursor.getColumnIndex("CurrentValueL2")));
        value.I3 = parseString(cursor.getString(cursor.getColumnIndex("CurrentValueL3")));
        value.Psum = parseString(cursor.getString(cursor.getColumnIndex("ActivePowerTotal")));
        value.Qsum = parseString(cursor.getString(cursor.getColumnIndex("ReactivePowerTotal")));
        value.Energys[0] = parseString(cursor.getString(cursor.getColumnIndex("Energy1")));
        value.Energys[1] = parseString(cursor.getString(cursor.getColumnIndex("Energy2")));
        value.Energys[2] = parseString(cursor.getString(cursor.getColumnIndex("Energy3")));
        value.Energys[3] = parseString(cursor.getString(cursor.getColumnIndex("Energy4")));
        value.Energys[4] = parseString(cursor.getString(cursor.getColumnIndex("Energy5")));
        value.Energys[5] = parseString(cursor.getString(cursor.getColumnIndex("Energy6")));
        value.Energys[6] = parseString(cursor.getString(cursor.getColumnIndex("Energy7")));
        value.Energys[7] = parseString(cursor.getString(cursor.getColumnIndex("Energy8")));
        value.Energys[8] = parseString(cursor.getString(cursor.getColumnIndex("Energy9")));
        value.Energys[9] = parseString(cursor.getString(cursor.getColumnIndex("Energy10")));
        value.Energys[10] = parseString(cursor.getString(cursor.getColumnIndex(TableColumns.Energy11)));
        value.Energys[11] = parseString(cursor.getString(cursor.getColumnIndex(TableColumns.Energy12)));
        value.Energys[12] = parseString(cursor.getString(cursor.getColumnIndex(TableColumns.Energy13)));
        value.Energys[13] = parseString(cursor.getString(cursor.getColumnIndex(TableColumns.Energy14)));
        value.Energys[14] = parseString(cursor.getString(cursor.getColumnIndex(TableColumns.Energy15)));
        value.Energys[15] = parseString(cursor.getString(cursor.getColumnIndex(TableColumns.Energy16)));
        value.Energys[16] = parseString(cursor.getString(cursor.getColumnIndex(TableColumns.Energy17)));
        value.Energys[17] = parseString(cursor.getString(cursor.getColumnIndex(TableColumns.Energy18)));
        value.Energys[18] = parseString(cursor.getString(cursor.getColumnIndex(TableColumns.Energy19)));
        value.Energys[19] = parseString(cursor.getString(cursor.getColumnIndex(TableColumns.Energy20)));
        return value;
    }
}
