package com.clou.rs350.db.model.sequence;

import com.itextpdf.text.pdf.PdfObject;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class SequenceData implements Serializable {
    public int Language = 0;
    public List<SequenceItem> SequenceItems = new ArrayList();
    public String SequenceName = PdfObject.NOTHING;
}
