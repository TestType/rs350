package com.clou.rs350.db.model;

import java.util.List;

public class ConTypeRange {
    public String[] connectType = null;
    public int[] connectTypeIndex = null;
    public List<String[]> currentArray = null;
    public List<String[]> voltageArray = null;
}
