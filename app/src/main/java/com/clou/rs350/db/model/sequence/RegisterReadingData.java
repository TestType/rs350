package com.clou.rs350.db.model.sequence;

import com.itextpdf.text.pdf.PdfObject;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class RegisterReadingData implements Serializable {
    public String CreatTime = PdfObject.NOTHING;
    public int Index = 0;
    public int Language = 0;
    public List<RegisterReadingItem> Registers = new ArrayList();
    public int RegistersCount = 0;
    public String SchemeName = PdfObject.NOTHING;
    public String SerialNo = PdfObject.NOTHING;
    public int isAfter = 0;

    public RegisterReadingData() {
    }

    public RegisterReadingData(int index) {
        this.Index = index;
    }
}
