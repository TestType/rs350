package com.clou.rs350.db.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import com.clou.rs350.constants.Constants;
import com.clou.rs350.db.model.StandardInfo;
import com.clou.rs350.manager.DatabaseManager;
import java.sql.Date;
import java.text.SimpleDateFormat;

public class StandardBaseInfoDao {
    private static final String TABLE_NAME = "StandardBaseInfo";

    private static final class TableColumns {
        public static final String Accuracy = "Accuracy";
        public static final String Calib_Valid_Date = "Calib_Valid_Date";
        public static final String Calibrated_By = "Calibrated_By";
        public static final String CalibrationDate = "CalibrationDate";
        public static final String CertificateID = "CertificateID";
        public static final String Company = "Company";
        public static final String CreateTime = "CreateTime";
        public static final String FirmwareVersion = "FirmwareVersion";
        public static final String ID = "intMyID";
        public static final String IsRead = "IsRead";
        public static final String StandardSerialNo = "StandardSerialNo";

        private TableColumns() {
        }
    }

    public static void createTable(SQLiteDatabase db) {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("create table if not exists ").append(TABLE_NAME).append(" ( ");
        stringBuffer.append("intMyID").append(" integer primary key autoincrement,");
        stringBuffer.append("CreateTime").append(" VARCHAR(50),");
        stringBuffer.append("IsRead").append(" integer default 0,");
        stringBuffer.append("StandardSerialNo").append(" VARCHAR(50),");
        stringBuffer.append(TableColumns.FirmwareVersion).append(" VARCHAR(50),");
        stringBuffer.append("Accuracy").append(" VARCHAR(50),");
        stringBuffer.append(TableColumns.Company).append(" VARCHAR(100),");
        stringBuffer.append(TableColumns.CalibrationDate).append(" VARCHAR(50),");
        stringBuffer.append(TableColumns.Calib_Valid_Date).append(" VARCHAR(50),");
        stringBuffer.append(TableColumns.CertificateID).append(" VARCHAR(50),");
        stringBuffer.append(TableColumns.Calibrated_By).append(" VARCHAR(50));");
        db.execSQL(stringBuffer.toString());
    }

    public static synchronized boolean insertObject(Object Data, Context context) {
        boolean z;
        synchronized (StandardBaseInfoDao.class) {
            SQLiteDatabase db = DatabaseManager.getWriteableDatabase(context);
            ContentValues values = new ContentValues();
            StandardInfo object = (StandardInfo) Data;
            values.put("CreateTime", new SimpleDateFormat(Constants.TIMEFORMAT).format(new Date(System.currentTimeMillis())).toString());
            values.put("StandardSerialNo", object.StandardSerialNo);
            values.put(TableColumns.FirmwareVersion, object.FirmwareVersion);
            values.put("Accuracy", object.Accuracy.toString());
            values.put(TableColumns.Company, object.Manufacturer);
            values.put(TableColumns.CertificateID, object.CertificateID.toString());
            values.put(TableColumns.Calibrated_By, object.Calibrated_By.toString());
            z = db.insert(TABLE_NAME, null, values) != -1;
        }
        return z;
    }

    public static void updateObjectByKey(String serialNo, StandardInfo object, Context context) {
        SQLiteDatabase db = DatabaseManager.getWriteableDatabase(context);
        ContentValues values = new ContentValues();
        values.put("StandardSerialNo", object.StandardSerialNo);
        values.put(TableColumns.FirmwareVersion, object.FirmwareVersion);
        values.put("Accuracy", object.Accuracy.toString());
        values.put(TableColumns.Company, object.Manufacturer);
        values.put(TableColumns.CertificateID, object.CertificateID.toString());
        values.put(TableColumns.Calibrated_By, object.Calibrated_By.toString());
        values.put("CreateTime", String.valueOf(System.currentTimeMillis()));
        values.put("IsRead", (Integer) 0);
        db.update(TABLE_NAME, values, "StandardSerialNo = ?", new String[]{serialNo});
    }

    public static boolean queryIfExistBySerialNo(String serialNo, Context context) {
        boolean value = false;
        Cursor cursor = DatabaseManager.getWriteableDatabase(context).rawQuery("select * from StandardBaseInfo where StandardSerialNo = ?", new String[]{serialNo});
        if (cursor.moveToNext()) {
            value = true;
        }
        cursor.close();
        return value;
    }

    public static StandardInfo queryObjectBySerialNo(String serialNo, Context context) {
        StandardInfo value = null;
        Cursor cursor = DatabaseManager.getWriteableDatabase(context).rawQuery("select * from StandardBaseInfo where StandardSerialNo = ?", new String[]{serialNo});
        if (cursor.moveToNext()) {
            value = parseData(cursor, context);
        }
        cursor.close();
        return value;
    }

    private static StandardInfo parseData(Cursor cursor, Context context) {
        return new StandardInfo(context);
    }
}
