package com.clou.rs350.db.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import com.clou.rs350.db.model.OperatorInfo;
import com.clou.rs350.manager.DatabaseManager;
import com.itextpdf.text.pdf.PdfObject;
import com.itextpdf.text.xml.xmp.XmpMMProperties;

public class OperatorInfoDao extends BaseDao {
    private static final String TABLE_NAME = "OperatorInfo";

    private static final class TableColumns {
        public static final String AUTHORITY = "Authority";
        public static final String DESIGNATION = "Designation";
        public static final String ID = "intMyID";
        public static final String ISREAD = "IsRead";
        public static final String OPERATORID = "OperatorID";
        public static final String OPERATORNAME = "OperatorName";
        public static final String PASSWORD = "Password";

        private TableColumns() {
        }
    }

    public OperatorInfoDao(Context context) {
        super(context, TABLE_NAME, "OperatorID");
    }

    public void createTable(SQLiteDatabase db) {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("create table if not exists ").append(TABLE_NAME);
        stringBuffer.append(" (").append("intMyID");
        stringBuffer.append(" integer primary key autoincrement,");
        stringBuffer.append("IsRead").append(" integer default 0,");
        stringBuffer.append("OperatorID").append(" varchar(50),");
        stringBuffer.append("OperatorName").append(" varchar(50),");
        stringBuffer.append(TableColumns.PASSWORD).append(" varchar(12),");
        stringBuffer.append("Designation").append(" varchar(50),");
        stringBuffer.append(TableColumns.AUTHORITY).append(" integer default 0);");
        db.execSQL(stringBuffer.toString());
        ContentValues values = new ContentValues();
        values.put("OperatorName", "System Manager");
        values.put("OperatorID", XmpMMProperties.MANAGER);
        values.put(TableColumns.PASSWORD, PdfObject.NOTHING);
        values.put("Designation", "System Manager");
        values.put(TableColumns.AUTHORITY, (Integer) 1);
        db.insert(TABLE_NAME, null, values);
        ContentValues values2 = new ContentValues();
        values2.put("OperatorName", "Administrator");
        values2.put("OperatorID", "Administrator");
        values2.put(TableColumns.PASSWORD, "admin");
        values2.put("Designation", "Administrator");
        values2.put(TableColumns.AUTHORITY, (Integer) 3);
        db.insert(TABLE_NAME, null, values2);
        ContentValues values3 = new ContentValues();
        values3.put("OperatorName", "Demo");
        values3.put("OperatorID", "Demo");
        values3.put(TableColumns.PASSWORD, PdfObject.NOTHING);
        values3.put("Designation", "Demo");
        values3.put(TableColumns.AUTHORITY, (Integer) 2);
        db.insert(TABLE_NAME, null, values3);
    }

    public static synchronized boolean insertObject(Object Data, Context context) {
        boolean z;
        synchronized (OperatorInfoDao.class) {
            SQLiteDatabase db = DatabaseManager.getWriteableDatabase(context);
            ContentValues values = new ContentValues();
            OperatorInfo object = (OperatorInfo) Data;
            values.put("OperatorName", object.OperatorName);
            values.put("OperatorID", object.OperatorID);
            values.put(TableColumns.PASSWORD, object.Password);
            values.put("Designation", object.Designation);
            values.put(TableColumns.AUTHORITY, Integer.valueOf(object.Authority));
            z = db.insert(TABLE_NAME, null, values) != -1;
        }
        return z;
    }

    @Override // com.clou.rs350.db.dao.BaseDao
    public void upgradeTable(SQLiteDatabase db, int oldVersion, int newVersion) {
        if (oldVersion < 23) {
            ContentValues values = new ContentValues();
            values.put("OperatorName", "Administrator");
            values.put("OperatorID", "Administrator");
            values.put(TableColumns.PASSWORD, "admin");
            values.put("Designation", "Administrator");
            values.put(TableColumns.AUTHORITY, (Integer) 3);
            db.insert(TABLE_NAME, null, values);
        }
        if (oldVersion < 28) {
            ContentValues values2 = new ContentValues();
            values2.put("OperatorName", "Demo");
            values2.put("OperatorID", "Demo");
            values2.put(TableColumns.PASSWORD, PdfObject.NOTHING);
            values2.put("Designation", "Demo");
            values2.put(TableColumns.AUTHORITY, (Integer) 2);
            db.insert(TABLE_NAME, null, values2);
        }
        super.upgradeTable(db, oldVersion, newVersion);
    }

    /* access modifiers changed from: protected */
    @Override // com.clou.rs350.db.dao.BaseDao
    public Object parseData(Cursor cursor) {
        OperatorInfo value = new OperatorInfo();
        value.OperatorName = parseString(cursor.getString(cursor.getColumnIndex("OperatorName")));
        value.OperatorID = parseString(cursor.getString(cursor.getColumnIndex("OperatorID")));
        value.Password = parseString(cursor.getString(cursor.getColumnIndex(TableColumns.PASSWORD)));
        value.Designation = parseString(cursor.getString(cursor.getColumnIndex("Designation")));
        value.Authority = cursor.getInt(cursor.getColumnIndex(TableColumns.AUTHORITY));
        return value;
    }

    public static void updateObjectByKey(String id, OperatorInfo object, Context context) {
        SQLiteDatabase db = DatabaseManager.getWriteableDatabase(context);
        ContentValues values = new ContentValues();
        values.put("OperatorName", object.OperatorName);
        values.put("OperatorID", object.OperatorID);
        values.put(TableColumns.PASSWORD, object.Password);
        values.put("Designation", object.Designation);
        values.put(TableColumns.AUTHORITY, Integer.valueOf(object.Authority));
        values.put("IsRead", (Integer) 0);
        db.update(TABLE_NAME, values, "OperatorID = ?", new String[]{id});
    }

    public Object queryByKey(String id) {
        Cursor cursor = DatabaseManager.getWriteableDatabase(this.m_Context).query(TABLE_NAME, null, "OperatorID =? ", new String[]{id}, null, null, null);
        Object Data = null;
        if (cursor.moveToFirst()) {
            Data = parseData(cursor);
        }
        cursor.close();
        return Data;
    }
}
