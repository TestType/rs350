package com.clou.rs350.db.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import com.clou.rs350.db.dao.BaseTestDao;
import com.clou.rs350.db.model.HarmonicMeasurement;
import com.clou.rs350.manager.DatabaseManager;

public class HarmonicMeasurementDao extends BaseTestDao {
    private static final String TABLE_NAME = "TestHarmonicMeasurement";

    private static final class TableColumns {
        public static final String CurrentHarmonicAngleL1 = "CurrentHarmonicAngleL1";
        public static final String CurrentHarmonicAngleL2 = "CurrentHarmonicAngleL2";
        public static final String CurrentHarmonicAngleL3 = "CurrentHarmonicAngleL3";
        public static final String CurrentHarmonicL1 = "CurrentHarmonicL1";
        public static final String CurrentHarmonicL2 = "CurrentHarmonicL2";
        public static final String CurrentHarmonicL3 = "CurrentHarmonicL3";
        public static final String CurrentHarmonicValueL1 = "CurrentHarmonicValueL1";
        public static final String CurrentHarmonicValueL2 = "CurrentHarmonicValueL2";
        public static final String CurrentHarmonicValueL3 = "CurrentHarmonicValueL3";
        public static final String PHarmonicValueL1 = "PHarmonicValueL1";
        public static final String PHarmonicValueL2 = "PHarmonicValueL2";
        public static final String PHarmonicValueL3 = "PHarmonicValueL3";
        public static final String QHarmonicValueL1 = "QHarmonicValueL1";
        public static final String QHarmonicValueL2 = "QHarmonicValueL2";
        public static final String QHarmonicValueL3 = "QHarmonicValueL3";
        public static final String SHarmonicValueL1 = "SHarmonicValueL1";
        public static final String SHarmonicValueL2 = "SHarmonicValueL2";
        public static final String SHarmonicValueL3 = "SHarmonicValueL3";
        public static final String THD = "THD";
        public static final String VoltageHarmonicAngleL1 = "VoltageHarmonicAngleL1";
        public static final String VoltageHarmonicAngleL2 = "VoltageHarmonicAngleL2";
        public static final String VoltageHarmonicAngleL3 = "VoltageHarmonicAngleL3";
        public static final String VoltageHarmonicL1 = "VoltageHarmonicL1";
        public static final String VoltageHarmonicL2 = "VoltageHarmonicL2";
        public static final String VoltageHarmonicL3 = "VoltageHarmonicL3";
        public static final String VoltageHarmonicValueL1 = "VoltageHarmonicValueL1";
        public static final String VoltageHarmonicValueL2 = "VoltageHarmonicValueL2";
        public static final String VoltageHarmonicValueL3 = "VoltageHarmonicValueL3";

        private TableColumns() {
        }
    }

    public HarmonicMeasurementDao(Context context) {
        super(context, TABLE_NAME);
    }

    public void createTable(SQLiteDatabase db) {
        this.m_TableName = TABLE_NAME;
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append(getCreatTableString());
        stringBuffer.append(TableColumns.VoltageHarmonicAngleL1).append(" VARCHAR(1000),");
        stringBuffer.append(TableColumns.VoltageHarmonicAngleL2).append(" VARCHAR(1000),");
        stringBuffer.append(TableColumns.VoltageHarmonicAngleL3).append(" VARCHAR(1000),");
        stringBuffer.append(TableColumns.CurrentHarmonicAngleL1).append(" VARCHAR(1000),");
        stringBuffer.append(TableColumns.CurrentHarmonicAngleL2).append(" VARCHAR(1000),");
        stringBuffer.append(TableColumns.CurrentHarmonicAngleL3).append(" VARCHAR(1000),");
        stringBuffer.append(TableColumns.VoltageHarmonicL1).append(" VARCHAR(1000),");
        stringBuffer.append(TableColumns.VoltageHarmonicL2).append(" VARCHAR(1000),");
        stringBuffer.append(TableColumns.VoltageHarmonicL3).append(" VARCHAR(1000),");
        stringBuffer.append(TableColumns.CurrentHarmonicL1).append(" VARCHAR(1000),");
        stringBuffer.append(TableColumns.CurrentHarmonicL2).append(" VARCHAR(1000),");
        stringBuffer.append(TableColumns.CurrentHarmonicL3).append(" VARCHAR(1000),");
        stringBuffer.append(TableColumns.VoltageHarmonicValueL1).append(" VARCHAR(1000),");
        stringBuffer.append(TableColumns.VoltageHarmonicValueL2).append(" VARCHAR(1000),");
        stringBuffer.append(TableColumns.VoltageHarmonicValueL3).append(" VARCHAR(1000),");
        stringBuffer.append(TableColumns.CurrentHarmonicValueL1).append(" VARCHAR(1000),");
        stringBuffer.append(TableColumns.CurrentHarmonicValueL2).append(" VARCHAR(1000),");
        stringBuffer.append(TableColumns.CurrentHarmonicValueL3).append(" VARCHAR(1000),");
        stringBuffer.append(TableColumns.THD).append(" VARCHAR(100),");
        stringBuffer.append(TableColumns.PHarmonicValueL1).append(" VARCHAR(1000),");
        stringBuffer.append(TableColumns.PHarmonicValueL2).append(" VARCHAR(1000),");
        stringBuffer.append(TableColumns.PHarmonicValueL3).append(" VARCHAR(1000),");
        stringBuffer.append(TableColumns.QHarmonicValueL1).append(" VARCHAR(1000),");
        stringBuffer.append(TableColumns.QHarmonicValueL2).append(" VARCHAR(1000),");
        stringBuffer.append(TableColumns.QHarmonicValueL3).append(" VARCHAR(1000),");
        stringBuffer.append(TableColumns.SHarmonicValueL1).append(" VARCHAR(1000),");
        stringBuffer.append(TableColumns.SHarmonicValueL2).append(" VARCHAR(1000),");
        stringBuffer.append(TableColumns.SHarmonicValueL3).append(" VARCHAR(1000));");
        db.execSQL(stringBuffer.toString());
    }

    @Override // com.clou.rs350.db.dao.BaseDao, com.clou.rs350.db.dao.BaseTestDao
    public void upgradeTable(SQLiteDatabase db, int oldVersion, int newVersion) {
        if (oldVersion <= 3) {
            db.execSQL("alter table HarmonicMeasurement rename to TestHarmonicMeasurement");
        }
        super.upgradeTable(db, oldVersion, newVersion);
    }

    @Override // com.clou.rs350.db.dao.BaseDao, com.clou.rs350.db.dao.BaseTestDao
    public synchronized boolean insertObject(Object Data, String Time) {
        boolean z = true;
        synchronized (this) {
            SQLiteDatabase db = DatabaseManager.getWriteableDatabase(this.m_Context);
            ContentValues values = new ContentValues();
            HarmonicMeasurement object = (HarmonicMeasurement) Data;
            values.put(this.m_TableKey, object.SerialNo);
            values.put("CreateTime", Time);
            values.put(SameColumns.WiringType, object.WiringValue);
            values.put("IntWiringType", Integer.valueOf(object.Wiring));
            values.put(SameColumns.ConnectionMode, object.ConnectionType);
            values.put(SameColumns.VoltageRangeL1, object.VoltageRangeL1);
            values.put(SameColumns.VoltageRangeL2, object.VoltageRangeL2);
            values.put(SameColumns.VoltageRangeL3, object.VoltageRangeL3);
            values.put(SameColumns.CurrentRangeL1, object.CurrentRangeL1);
            values.put(SameColumns.CurrentRangeL2, object.CurrentRangeL2);
            values.put(SameColumns.CurrentRangeL3, object.CurrentRangeL3);
            values.put(TableColumns.VoltageHarmonicAngleL1, object.HarmonicAngle[0]);
            values.put(TableColumns.VoltageHarmonicAngleL2, object.HarmonicAngle[1]);
            values.put(TableColumns.VoltageHarmonicAngleL3, object.HarmonicAngle[2]);
            values.put(TableColumns.CurrentHarmonicAngleL1, object.HarmonicAngle[3]);
            values.put(TableColumns.CurrentHarmonicAngleL2, object.HarmonicAngle[4]);
            values.put(TableColumns.CurrentHarmonicAngleL3, object.HarmonicAngle[5]);
            values.put(TableColumns.VoltageHarmonicL1, object.HarmonicContent[0]);
            values.put(TableColumns.VoltageHarmonicL2, object.HarmonicContent[1]);
            values.put(TableColumns.VoltageHarmonicL3, object.HarmonicContent[2]);
            values.put(TableColumns.CurrentHarmonicL1, object.HarmonicContent[3]);
            values.put(TableColumns.CurrentHarmonicL2, object.HarmonicContent[4]);
            values.put(TableColumns.CurrentHarmonicL3, object.HarmonicContent[5]);
            values.put(TableColumns.VoltageHarmonicValueL1, object.HarmonicValue[0]);
            values.put(TableColumns.VoltageHarmonicValueL2, object.HarmonicValue[1]);
            values.put(TableColumns.VoltageHarmonicValueL3, object.HarmonicValue[2]);
            values.put(TableColumns.CurrentHarmonicValueL1, object.HarmonicValue[3]);
            values.put(TableColumns.CurrentHarmonicValueL2, object.HarmonicValue[4]);
            values.put(TableColumns.CurrentHarmonicValueL3, object.HarmonicValue[5]);
            values.put(TableColumns.THD, object.THD);
            values.put(TableColumns.PHarmonicValueL1, object.PowerHarmonicValue[0]);
            values.put(TableColumns.PHarmonicValueL2, object.PowerHarmonicValue[1]);
            values.put(TableColumns.PHarmonicValueL3, object.PowerHarmonicValue[2]);
            values.put(TableColumns.QHarmonicValueL1, object.PowerHarmonicValue[3]);
            values.put(TableColumns.QHarmonicValueL2, object.PowerHarmonicValue[4]);
            values.put(TableColumns.QHarmonicValueL3, object.PowerHarmonicValue[5]);
            values.put(TableColumns.SHarmonicValueL1, object.PowerHarmonicValue[6]);
            values.put(TableColumns.SHarmonicValueL2, object.PowerHarmonicValue[7]);
            values.put(TableColumns.SHarmonicValueL3, object.PowerHarmonicValue[8]);
            if (db.insert(TABLE_NAME, null, values) == -1) {
                z = false;
            }
        }
        return z;
    }

    /* access modifiers changed from: protected */
    @Override // com.clou.rs350.db.dao.BaseDao, com.clou.rs350.db.dao.BaseTestDao
    public Object parseData(Cursor cursor) {
        HarmonicMeasurement value = new HarmonicMeasurement();
        value.SerialNo = cursor.getString(cursor.getColumnIndex(this.m_TableKey));
        value.WiringValue = cursor.getString(cursor.getColumnIndex(SameColumns.WiringType));
        value.Wiring = cursor.getInt(cursor.getColumnIndex("IntWiringType"));
        value.VoltageRangeL1 = cursor.getString(cursor.getColumnIndex(SameColumns.VoltageRangeL1));
        value.VoltageRangeL2 = cursor.getString(cursor.getColumnIndex(SameColumns.VoltageRangeL2));
        value.VoltageRangeL3 = cursor.getString(cursor.getColumnIndex(SameColumns.VoltageRangeL3));
        value.CurrentRangeL1 = cursor.getString(cursor.getColumnIndex(SameColumns.CurrentRangeL1));
        value.CurrentRangeL2 = cursor.getString(cursor.getColumnIndex(SameColumns.CurrentRangeL2));
        value.CurrentRangeL3 = cursor.getString(cursor.getColumnIndex(SameColumns.CurrentRangeL3));
        value.arrVoltageContent[0] = value.parseDouble(cursor.getString(cursor.getColumnIndex(TableColumns.VoltageHarmonicL1)));
        value.arrVoltageContent[1] = value.parseDouble(cursor.getString(cursor.getColumnIndex(TableColumns.VoltageHarmonicL2)));
        value.arrVoltageContent[2] = value.parseDouble(cursor.getString(cursor.getColumnIndex(TableColumns.VoltageHarmonicL3)));
        value.arrCurrentContent[0] = value.parseDouble(cursor.getString(cursor.getColumnIndex(TableColumns.CurrentHarmonicL1)));
        value.arrCurrentContent[1] = value.parseDouble(cursor.getString(cursor.getColumnIndex(TableColumns.CurrentHarmonicL2)));
        value.arrCurrentContent[2] = value.parseDouble(cursor.getString(cursor.getColumnIndex(TableColumns.CurrentHarmonicL3)));
        value.arrVoltageAngle[0] = value.parseDouble(cursor.getString(cursor.getColumnIndex(TableColumns.VoltageHarmonicAngleL1)));
        value.arrVoltageAngle[1] = value.parseDouble(cursor.getString(cursor.getColumnIndex(TableColumns.VoltageHarmonicAngleL2)));
        value.arrVoltageAngle[2] = value.parseDouble(cursor.getString(cursor.getColumnIndex(TableColumns.VoltageHarmonicAngleL3)));
        value.arrCurrentAngle[0] = value.parseDouble(cursor.getString(cursor.getColumnIndex(TableColumns.CurrentHarmonicAngleL1)));
        value.arrCurrentAngle[1] = value.parseDouble(cursor.getString(cursor.getColumnIndex(TableColumns.CurrentHarmonicAngleL2)));
        value.arrCurrentAngle[2] = value.parseDouble(cursor.getString(cursor.getColumnIndex(TableColumns.CurrentHarmonicAngleL3)));
        value.arrVoltageValue[0] = value.parseModel(cursor.getString(cursor.getColumnIndex(TableColumns.VoltageHarmonicValueL1)));
        value.arrVoltageValue[1] = value.parseModel(cursor.getString(cursor.getColumnIndex(TableColumns.VoltageHarmonicValueL2)));
        value.arrVoltageValue[2] = value.parseModel(cursor.getString(cursor.getColumnIndex(TableColumns.VoltageHarmonicValueL3)));
        value.arrCurrentValue[0] = value.parseModel(cursor.getString(cursor.getColumnIndex(TableColumns.CurrentHarmonicValueL1)));
        value.arrCurrentValue[1] = value.parseModel(cursor.getString(cursor.getColumnIndex(TableColumns.CurrentHarmonicValueL2)));
        value.arrCurrentValue[2] = value.parseModel(cursor.getString(cursor.getColumnIndex(TableColumns.CurrentHarmonicValueL3)));
        value.arrTHD = value.parseDouble(cursor.getString(cursor.getColumnIndex(TableColumns.THD)));
        value.arrPValue[0] = value.parseModel(cursor.getString(cursor.getColumnIndex(TableColumns.PHarmonicValueL1)));
        value.arrPValue[1] = value.parseModel(cursor.getString(cursor.getColumnIndex(TableColumns.PHarmonicValueL2)));
        value.arrPValue[2] = value.parseModel(cursor.getString(cursor.getColumnIndex(TableColumns.PHarmonicValueL3)));
        value.arrQValue[0] = value.parseModel(cursor.getString(cursor.getColumnIndex(TableColumns.QHarmonicValueL1)));
        value.arrQValue[1] = value.parseModel(cursor.getString(cursor.getColumnIndex(TableColumns.QHarmonicValueL2)));
        value.arrQValue[2] = value.parseModel(cursor.getString(cursor.getColumnIndex(TableColumns.QHarmonicValueL3)));
        value.arrSValue[0] = value.parseModel(cursor.getString(cursor.getColumnIndex(TableColumns.SHarmonicValueL1)));
        value.arrSValue[1] = value.parseModel(cursor.getString(cursor.getColumnIndex(TableColumns.SHarmonicValueL2)));
        value.arrSValue[2] = value.parseModel(cursor.getString(cursor.getColumnIndex(TableColumns.SHarmonicValueL3)));
        return value;
    }
}
