package com.clou.rs350.db.model;

public class DigitalMeterDataType {
    public int Address;
    public int DataType;
    public int Length;
    public float Multiple;

    public DigitalMeterDataType(int address, int registerCount, int dataType, float multiple) {
        this.Address = address;
        this.Length = registerCount;
        this.DataType = dataType;
        this.Multiple = multiple;
    }

    public DigitalMeterDataType() {
    }
}
