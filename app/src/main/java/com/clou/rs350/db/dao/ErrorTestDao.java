package com.clou.rs350.db.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import com.clou.rs350.db.dao.BaseTestDao;
import com.clou.rs350.db.model.ErrorTest;
import com.clou.rs350.manager.DatabaseManager;
import com.clou.rs350.utils.OtherUtils;

public class ErrorTestDao extends BaseTestDao {
    public static final String TABLE_NAME = "TestErrorTest";

    private static final class TableColumns {
        public static final String ActivePowerL1 = "ActivePowerL1";
        public static final String ActivePowerL2 = "ActivePowerL2";
        public static final String ActivePowerL3 = "ActivePowerL3";
        public static final String ActivePowerTotal = "ActivePowerTotal";
        public static final String ApparentPowerL1 = "ApparentPowerL1";
        public static final String ApparentPowerL2 = "ApparentPowerL2";
        public static final String ApparentPowerL3 = "ApparentPowerL3";
        public static final String ApparentPowerTotal = "ApparentPowerTotal";
        public static final String ConstantUnit = "ConstantUnit";
        public static final String CurrentAngleL1 = "CurrentAngleL1";
        public static final String CurrentAngleL2 = "CurrentAngleL2";
        public static final String CurrentAngleL3 = "CurrentAngleL3";
        public static final String CurrentValueL1 = "CurrentValueL1";
        public static final String CurrentValueL2 = "CurrentValueL2";
        public static final String CurrentValueL3 = "CurrentValueL3";
        public static final String Energy1 = "Energy1";
        public static final String Energy10 = "Energy10";
        public static final String Energy2 = "Energy2";
        public static final String Energy3 = "Energy3";
        public static final String Energy4 = "Energy4";
        public static final String Energy5 = "Energy5";
        public static final String Energy6 = "Energy6";
        public static final String Energy7 = "Energy7";
        public static final String Energy8 = "Energy8";
        public static final String Energy9 = "Energy9";
        public static final String Error1 = "Error1";
        public static final String Error10 = "Error10";
        public static final String Error2 = "Error2";
        public static final String Error3 = "Error3";
        public static final String Error4 = "Error4";
        public static final String Error5 = "Error5";
        public static final String Error6 = "Error6";
        public static final String Error7 = "Error7";
        public static final String Error8 = "Error8";
        public static final String Error9 = "Error9";
        public static final String ErrorAverage = "ErrorAverage";
        public static final String ErrorCount = "ErrorCount";
        public static final String ErrorMore = "ErrorMore";
        public static final String ErrorStandard = "ErrorStandard";
        public static final String Frequency = "Frequency";
        public static final String MeterConstant = "MeterConstant";
        public static final String MeterIndex = "MeterIndex";
        public static final String PowerFactorL1 = "PowerFactorL1";
        public static final String PowerFactorL2 = "PowerFactorL2";
        public static final String PowerFactorL3 = "PowerFactorL3";
        public static final String PowerFactorTotal = "PowerFactorTotal";
        public static final String PulseSamplingMethod = "PulseSamplingMethod";
        public static final String Pulses = "Pulses";
        public static final String ReactivePowerL1 = "ReactivePowerL1";
        public static final String ReactivePowerL2 = "ReactivePowerL2";
        public static final String ReactivePowerL3 = "ReactivePowerL3";
        public static final String ReactivePowerTotal = "ReactivePowerTotal";
        public static final String Result = "Result";
        public static final String RunTime = "RunTime";
        public static final String VoltageAngleL1 = "VoltageAngleL1";
        public static final String VoltageAngleL2 = "VoltageAngleL2";
        public static final String VoltageAngleL3 = "VoltageAngleL3";
        public static final String VoltageCurrentAngleL1 = "VoltageCurrentAngleL1";
        public static final String VoltageCurrentAngleL2 = "VoltageCurrentAngleL2";
        public static final String VoltageCurrentAngleL3 = "VoltageCurrentAngleL3";
        public static final String VoltageValueL1 = "VoltageValueL1";
        public static final String VoltageValueL2 = "VoltageValueL2";
        public static final String VoltageValueL3 = "VoltageValueL3";
        public static final String VoltageVectorAngleL1 = "VoltageVectorAngleL1";
        public static final String VoltageVectorAngleL2 = "VoltageVectorAngleL2";
        public static final String VoltageVectorAngleL3 = "VoltageVectorAngleL3";
        public static final String VoltageVectorValueL1 = "VoltageVectorValueL1";
        public static final String VoltageVectorValueL2 = "VoltageVectorValueL2";
        public static final String VoltageVectorValueL3 = "VoltageVectorValueL3";

        private TableColumns() {
        }
    }

    public ErrorTestDao(Context context) {
        super(context, TABLE_NAME);
    }

    public void createTable(SQLiteDatabase db) {
        this.m_TableName = TABLE_NAME;
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append(getCreatTableString());
        stringBuffer.append(SameColumns.MeterSerialNo).append(" VARCHAR(50),");
        stringBuffer.append("MeterIndex").append(" VARCHAR(5),");
        stringBuffer.append("VoltageValueL1").append(" VARCHAR(20),");
        stringBuffer.append("VoltageValueL2").append(" VARCHAR(20),");
        stringBuffer.append("VoltageValueL3").append(" VARCHAR(20),");
        stringBuffer.append("VoltageVectorValueL1").append(" VARCHAR(20),");
        stringBuffer.append("VoltageVectorValueL2").append(" VARCHAR(20),");
        stringBuffer.append("VoltageVectorValueL3").append(" VARCHAR(20),");
        stringBuffer.append("CurrentValueL1").append(" VARCHAR(20),");
        stringBuffer.append("CurrentValueL2").append(" VARCHAR(20),");
        stringBuffer.append("CurrentValueL3").append(" VARCHAR(20),");
        stringBuffer.append("VoltageAngleL1").append(" VARCHAR(20),");
        stringBuffer.append("VoltageAngleL2").append(" VARCHAR(20),");
        stringBuffer.append("VoltageAngleL3").append(" VARCHAR(20),");
        stringBuffer.append("VoltageVectorAngleL1").append(" VARCHAR(20),");
        stringBuffer.append("VoltageVectorAngleL2").append(" VARCHAR(20),");
        stringBuffer.append("VoltageVectorAngleL3").append(" VARCHAR(20),");
        stringBuffer.append("CurrentAngleL1").append(" VARCHAR(20),");
        stringBuffer.append("CurrentAngleL2").append(" VARCHAR(20),");
        stringBuffer.append("CurrentAngleL3").append(" VARCHAR(20),");
        stringBuffer.append("Frequency").append(" VARCHAR(20),");
        stringBuffer.append("VoltageCurrentAngleL1").append(" VARCHAR(20),");
        stringBuffer.append("VoltageCurrentAngleL2").append(" VARCHAR(20),");
        stringBuffer.append("VoltageCurrentAngleL3").append(" VARCHAR(20),");
        stringBuffer.append("PulseSamplingMethod").append(" VARCHAR(50),");
        stringBuffer.append("MeterConstant").append(" VARCHAR(50),");
        stringBuffer.append("ConstantUnit").append(" VARCHAR(50),");
        stringBuffer.append("Pulses").append(" VARCHAR(50),");
        stringBuffer.append("ErrorCount").append(" VARCHAR(10),");
        stringBuffer.append("RunTime").append(" VARCHAR(50),");
        stringBuffer.append("Error1").append(" VARCHAR(20),");
        stringBuffer.append("Error2").append(" VARCHAR(20),");
        stringBuffer.append("Error3").append(" VARCHAR(20),");
        stringBuffer.append("Error4").append(" VARCHAR(20),");
        stringBuffer.append("Error5").append(" VARCHAR(20),");
        stringBuffer.append("Error6").append(" VARCHAR(20),");
        stringBuffer.append("Error7").append(" VARCHAR(20),");
        stringBuffer.append("Error8").append(" VARCHAR(20),");
        stringBuffer.append("Error9").append(" VARCHAR(20),");
        stringBuffer.append("Error10").append(" VARCHAR(20),");
        stringBuffer.append("ErrorMore").append(" VARCHAR(200),");
        stringBuffer.append("ErrorAverage").append(" VARCHAR(20),");
        stringBuffer.append("ErrorStandard").append(" VARCHAR(20),");
        stringBuffer.append("Energy1").append(" VARCHAR(20),");
        stringBuffer.append("Energy2").append(" VARCHAR(20),");
        stringBuffer.append("Energy3").append(" VARCHAR(20),");
        stringBuffer.append("Energy4").append(" VARCHAR(20),");
        stringBuffer.append("Energy5").append(" VARCHAR(20),");
        stringBuffer.append("Energy6").append(" VARCHAR(20),");
        stringBuffer.append("Energy7").append(" VARCHAR(20),");
        stringBuffer.append("Energy8").append(" VARCHAR(20),");
        stringBuffer.append("Energy9").append(" VARCHAR(20),");
        stringBuffer.append("Energy10").append(" VARCHAR(20),");
        stringBuffer.append("Result").append(" VARCHAR(20),");
        stringBuffer.append("ActivePowerL1").append(" VARCHAR(20),");
        stringBuffer.append("ActivePowerL2").append(" VARCHAR(20),");
        stringBuffer.append("ActivePowerL3").append(" VARCHAR(20),");
        stringBuffer.append("ActivePowerTotal").append(" VARCHAR(20),");
        stringBuffer.append("ReactivePowerL1").append(" VARCHAR(20),");
        stringBuffer.append("ReactivePowerL2").append(" VARCHAR(20),");
        stringBuffer.append("ReactivePowerL3").append(" VARCHAR(20),");
        stringBuffer.append("ReactivePowerTotal").append(" VARCHAR(20),");
        stringBuffer.append("ApparentPowerL1").append(" VARCHAR(20),");
        stringBuffer.append("ApparentPowerL2").append(" VARCHAR(20),");
        stringBuffer.append("ApparentPowerL3").append(" VARCHAR(20),");
        stringBuffer.append("ApparentPowerTotal").append(" VARCHAR(20),");
        stringBuffer.append("PowerFactorL1").append(" VARCHAR(10),");
        stringBuffer.append("PowerFactorL2").append(" VARCHAR(10),");
        stringBuffer.append("PowerFactorL3").append(" VARCHAR(10),");
        stringBuffer.append("PowerFactorTotal").append(" VARCHAR(10));");
        db.execSQL(stringBuffer.toString());
    }

    @Override // com.clou.rs350.db.dao.BaseDao, com.clou.rs350.db.dao.BaseTestDao
    public void upgradeTable(SQLiteDatabase db, int oldVersion, int newVersion) {
        if (oldVersion < 4) {
            db.execSQL("alter table ErrorTest rename to TestErrorTest");
        }
        if (oldVersion < 9) {
            db.execSQL("alter table TestErrorTest add column Energy1 VARCHAR(20)");
            db.execSQL("alter table TestErrorTest add column Energy2 VARCHAR(20)");
            db.execSQL("alter table TestErrorTest add column Energy3 VARCHAR(20)");
            db.execSQL("alter table TestErrorTest add column Energy4 VARCHAR(20)");
            db.execSQL("alter table TestErrorTest add column Energy5 VARCHAR(20)");
            db.execSQL("alter table TestErrorTest add column Energy6 VARCHAR(20)");
            db.execSQL("alter table TestErrorTest add column Energy7 VARCHAR(20)");
            db.execSQL("alter table TestErrorTest add column Energy8 VARCHAR(20)");
            db.execSQL("alter table TestErrorTest add column Energy9 VARCHAR(20)");
            db.execSQL("alter table TestErrorTest add column Energy10 VARCHAR(20)");
        }
        super.upgradeTable(db, oldVersion, newVersion);
    }

    @Override // com.clou.rs350.db.dao.BaseDao, com.clou.rs350.db.dao.BaseTestDao
    public synchronized boolean insertObject(Object Data, String Time) {
        boolean z = true;
        synchronized (this) {
            SQLiteDatabase db = DatabaseManager.getWriteableDatabase(this.m_Context);
            ContentValues values = new ContentValues();
            ErrorTest object = (ErrorTest) Data;
            values.put(this.m_TableKey, object.CustomerSerialNo);
            values.put(SameColumns.MeterSerialNo, object.MeterSerialNo);
            values.put("CreateTime", Time);
            values.put("MeterIndex", object.MeterIndex);
            values.put(SameColumns.WiringType, object.WiringValue);
            values.put("IntWiringType", Integer.valueOf(object.Wiring));
            values.put(SameColumns.ConnectionMode, object.ConnectionType);
            values.put(SameColumns.VoltageRangeL1, object.VoltageRangeL1);
            values.put(SameColumns.VoltageRangeL2, object.VoltageRangeL2);
            values.put(SameColumns.VoltageRangeL3, object.VoltageRangeL3);
            values.put(SameColumns.CurrentRangeL1, object.CurrentRangeL1);
            values.put(SameColumns.CurrentRangeL2, object.CurrentRangeL2);
            values.put(SameColumns.CurrentRangeL3, object.CurrentRangeL3);
            values.put("VoltageValueL1", object.VoltageValueL1Show.s_Value);
            values.put("VoltageValueL2", object.VoltageValueL2Show.s_Value);
            values.put("VoltageValueL3", object.VoltageValueL3Show.s_Value);
            values.put("VoltageVectorValueL1", object.VoltageValueL1Vector.s_Value);
            values.put("VoltageVectorValueL2", object.VoltageValueL2Vector.s_Value);
            values.put("VoltageVectorValueL3", object.VoltageValueL3Vector.s_Value);
            values.put("CurrentValueL1", object.CurrentValueL1.s_Value);
            values.put("CurrentValueL2", object.CurrentValueL2.s_Value);
            values.put("CurrentValueL3", object.CurrentValueL3.s_Value);
            values.put("VoltageAngleL1", object.VoltageAngleL1Show.s_Value);
            values.put("VoltageAngleL2", object.VoltageAngleL2Show.s_Value);
            values.put("VoltageAngleL3", object.VoltageAngleL3Show.s_Value);
            values.put("VoltageVectorAngleL1", object.VoltageAngleL1Vector.s_Value);
            values.put("VoltageVectorAngleL2", object.VoltageAngleL2Vector.s_Value);
            values.put("VoltageVectorAngleL3", object.VoltageAngleL3Vector.s_Value);
            values.put("CurrentAngleL1", object.CurrentAngleL1.s_Value);
            values.put("CurrentAngleL2", object.CurrentAngleL2.s_Value);
            values.put("CurrentAngleL3", object.CurrentAngleL3.s_Value);
            values.put("Frequency", object.Frequency.s_Value);
            values.put("VoltageCurrentAngleL1", object.VoltageCurrentAngleL1Show.s_Value);
            values.put("VoltageCurrentAngleL2", object.VoltageCurrentAngleL2Show.s_Value);
            values.put("VoltageCurrentAngleL3", object.VoltageCurrentAngleL3Show.s_Value);
            values.put("ActivePowerL1", object.ActivePowerL1.s_Value);
            values.put("ActivePowerL2", object.ActivePowerL2.s_Value);
            values.put("ActivePowerL3", object.ActivePowerL3.s_Value);
            values.put("ActivePowerTotal", object.ActivePowerTotal.s_Value);
            values.put("ReactivePowerL1", object.ReactivePowerL1.s_Value);
            values.put("ReactivePowerL2", object.ReactivePowerL2.s_Value);
            values.put("ReactivePowerL3", object.ReactivePowerL3.s_Value);
            values.put("ReactivePowerTotal", object.ReactivePowerTotal.s_Value);
            values.put("ApparentPowerL1", object.ApparentPowerL1.s_Value);
            values.put("ApparentPowerL2", object.ApparentPowerL2.s_Value);
            values.put("ApparentPowerL3", object.ApparentPowerL3.s_Value);
            values.put("ApparentPowerTotal", object.ApparentPowerTotal.s_Value);
            values.put("PowerFactorL1", object.PowerFactorL1.toString());
            values.put("PowerFactorL2", object.PowerFactorL2.toString());
            values.put("PowerFactorL3", object.PowerFactorL3.toString());
            values.put("PowerFactorTotal", object.PowerFactorTotal.s_Value);
            values.put("PulseSamplingMethod", object.Pulses);
            values.put("MeterConstant", object.MeterConstant);
            values.put("ConstantUnit", object.ConstantUnit);
            values.put("Pulses", object.Pulses);
            values.put("ErrorCount", Integer.valueOf(object.ErrorCount));
            values.put("RunTime", object.RunTime);
            values.put("Error1", object.getError(0));
            values.put("Error2", object.getError(1));
            values.put("Error3", object.getError(2));
            values.put("Error4", object.getError(3));
            values.put("Error5", object.getError(4));
            values.put("Error6", object.getError(5));
            values.put("Error7", object.getError(6));
            values.put("Error8", object.getError(7));
            values.put("Error9", object.getError(8));
            values.put("Error10", object.getError(9));
            values.put("ErrorMore", object.ErrorMore);
            values.put("ErrorAverage", object.ErrorAverage);
            values.put("ErrorStandard", object.ErrorStandard);
            values.put("Energy1", object.getEnergy(0));
            values.put("Energy2", object.getEnergy(1));
            values.put("Energy3", object.getEnergy(2));
            values.put("Energy4", object.getEnergy(3));
            values.put("Energy5", object.getEnergy(4));
            values.put("Energy6", object.getEnergy(5));
            values.put("Energy7", object.getEnergy(6));
            values.put("Energy8", object.getEnergy(7));
            values.put("Energy9", object.getEnergy(8));
            values.put("Energy10", object.getEnergy(9));
            values.put("Result", object.Result.s_Value);
            if (db.insert(TABLE_NAME, null, values) == -1) {
                z = false;
            }
        }
        return z;
    }

    /* access modifiers changed from: protected */
    @Override // com.clou.rs350.db.dao.BaseDao, com.clou.rs350.db.dao.BaseTestDao
    public Object parseData(Cursor cursor) {
        ErrorTest value = new ErrorTest();
        value.CustomerSerialNo = parseString(cursor.getString(cursor.getColumnIndex(this.m_TableKey)));
        value.MeterSerialNo = parseString(cursor.getString(cursor.getColumnIndex(SameColumns.MeterSerialNo)));
        value.MeterIndex = parseString(cursor.getString(cursor.getColumnIndex("MeterIndex")));
        value.WiringValue = parseString(cursor.getString(cursor.getColumnIndex(SameColumns.WiringType)));
        value.Wiring = cursor.getInt(cursor.getColumnIndex("IntWiringType"));
        value.ConnectionType = parseString(cursor.getString(cursor.getColumnIndex(SameColumns.ConnectionMode)));
        value.VoltageRangeL1 = parseString(cursor.getString(cursor.getColumnIndex(SameColumns.VoltageRangeL1)));
        value.VoltageRangeL2 = parseString(cursor.getString(cursor.getColumnIndex(SameColumns.VoltageRangeL2)));
        value.VoltageRangeL3 = parseString(cursor.getString(cursor.getColumnIndex(SameColumns.VoltageRangeL3)));
        value.CurrentRangeL1 = parseString(cursor.getString(cursor.getColumnIndex(SameColumns.CurrentRangeL1)));
        value.CurrentRangeL2 = parseString(cursor.getString(cursor.getColumnIndex(SameColumns.CurrentRangeL2)));
        value.CurrentRangeL3 = parseString(cursor.getString(cursor.getColumnIndex(SameColumns.CurrentRangeL3)));
        value.VoltageValueL1Show.s_Value = parseString(cursor.getString(cursor.getColumnIndex("VoltageValueL1")));
        value.VoltageValueL2Show.s_Value = parseString(cursor.getString(cursor.getColumnIndex("VoltageValueL2")));
        value.VoltageValueL3Show.s_Value = parseString(cursor.getString(cursor.getColumnIndex("VoltageValueL3")));
        value.CurrentValueL1.s_Value = parseString(cursor.getString(cursor.getColumnIndex("CurrentValueL1")));
        value.CurrentValueL2.s_Value = parseString(cursor.getString(cursor.getColumnIndex("CurrentValueL2")));
        value.CurrentValueL3.s_Value = parseString(cursor.getString(cursor.getColumnIndex("CurrentValueL3")));
        value.ActivePowerL1.s_Value = parseString(cursor.getString(cursor.getColumnIndex("ActivePowerL1")));
        value.ActivePowerL2.s_Value = parseString(cursor.getString(cursor.getColumnIndex("ActivePowerL2")));
        value.ActivePowerL3.s_Value = parseString(cursor.getString(cursor.getColumnIndex("ActivePowerL3")));
        value.ActivePowerTotal.s_Value = parseString(cursor.getString(cursor.getColumnIndex("ActivePowerTotal")));
        value.ReactivePowerL1.s_Value = parseString(cursor.getString(cursor.getColumnIndex("ReactivePowerL1")));
        value.ReactivePowerL2.s_Value = parseString(cursor.getString(cursor.getColumnIndex("ReactivePowerL2")));
        value.ReactivePowerL3.s_Value = parseString(cursor.getString(cursor.getColumnIndex("ReactivePowerL3")));
        value.ReactivePowerTotal.s_Value = parseString(cursor.getString(cursor.getColumnIndex("ReactivePowerTotal")));
        value.ApparentPowerL1.s_Value = parseString(cursor.getString(cursor.getColumnIndex("ApparentPowerL1")));
        value.ApparentPowerL2.s_Value = parseString(cursor.getString(cursor.getColumnIndex("ApparentPowerL2")));
        value.ApparentPowerL3.s_Value = parseString(cursor.getString(cursor.getColumnIndex("ApparentPowerL3")));
        value.ApparentPowerTotal.s_Value = parseString(cursor.getString(cursor.getColumnIndex("ApparentPowerTotal")));
        value.PowerFactorL1.s_Value = parseString(cursor.getString(cursor.getColumnIndex("PowerFactorL1")));
        value.PowerFactorL2.s_Value = parseString(cursor.getString(cursor.getColumnIndex("PowerFactorL2")));
        value.PowerFactorL3.s_Value = parseString(cursor.getString(cursor.getColumnIndex("PowerFactorL3")));
        value.PowerFactorTotal.s_Value = parseString(cursor.getString(cursor.getColumnIndex("PowerFactorTotal")));
        value.Pulses = parseString(cursor.getString(cursor.getColumnIndex("Pulses")));
        value.MeterConstant = parseString(cursor.getString(cursor.getColumnIndex("MeterConstant")));
        value.ConstantUnit = parseString(cursor.getString(cursor.getColumnIndex("ConstantUnit")));
        value.ErrorCount = OtherUtils.parseInt(parseString(cursor.getString(cursor.getColumnIndex("ErrorCount"))));
        value.hasErrorCount = value.ErrorCount;
        value.RunTime = parseString(cursor.getString(cursor.getColumnIndex("RunTime")));
        value.Error[0].s_Value = parseString(cursor.getString(cursor.getColumnIndex("Error1")));
        value.Error[1].s_Value = parseString(cursor.getString(cursor.getColumnIndex("Error2")));
        value.Error[2].s_Value = parseString(cursor.getString(cursor.getColumnIndex("Error3")));
        value.Error[3].s_Value = parseString(cursor.getString(cursor.getColumnIndex("Error4")));
        value.Error[4].s_Value = parseString(cursor.getString(cursor.getColumnIndex("Error5")));
        value.Error[5].s_Value = parseString(cursor.getString(cursor.getColumnIndex("Error6")));
        value.Error[6].s_Value = parseString(cursor.getString(cursor.getColumnIndex("Error7")));
        value.Error[7].s_Value = parseString(cursor.getString(cursor.getColumnIndex("Error8")));
        value.Error[8].s_Value = parseString(cursor.getString(cursor.getColumnIndex("Error9")));
        value.Error[9].s_Value = parseString(cursor.getString(cursor.getColumnIndex("Error10")));
        value.ErrorMore = parseString(cursor.getString(cursor.getColumnIndex("ErrorMore")));
        value.ErrorAverage = parseString(cursor.getString(cursor.getColumnIndex("ErrorAverage")));
        value.ErrorStandard = parseString(cursor.getString(cursor.getColumnIndex("ErrorStandard")));
        value.Energy[0].s_Value = parseString(cursor.getString(cursor.getColumnIndex("Energy1")));
        value.Energy[1].s_Value = parseString(cursor.getString(cursor.getColumnIndex("Energy2")));
        value.Energy[2].s_Value = parseString(cursor.getString(cursor.getColumnIndex("Energy3")));
        value.Energy[3].s_Value = parseString(cursor.getString(cursor.getColumnIndex("Energy4")));
        value.Energy[4].s_Value = parseString(cursor.getString(cursor.getColumnIndex("Energy5")));
        value.Energy[5].s_Value = parseString(cursor.getString(cursor.getColumnIndex("Energy6")));
        value.Energy[6].s_Value = parseString(cursor.getString(cursor.getColumnIndex("Energy7")));
        value.Energy[7].s_Value = parseString(cursor.getString(cursor.getColumnIndex("Energy8")));
        value.Energy[8].s_Value = parseString(cursor.getString(cursor.getColumnIndex("Energy9")));
        value.Energy[9].s_Value = parseString(cursor.getString(cursor.getColumnIndex("Energy10")));
        value.Result.s_Value = parseString(cursor.getString(cursor.getColumnIndex("Result")));
        return value;
    }
}
