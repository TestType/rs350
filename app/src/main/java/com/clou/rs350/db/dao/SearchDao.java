package com.clou.rs350.db.dao;

import android.content.Context;
import com.clou.rs350.db.model.DataBaseRecordItem;
import java.util.List;

public class SearchDao {
    private Context m_Context;
    BaseTestDao[] m_Daos = {new BasicMeasurementDao(this.m_Context), new WaveMeasurementDao(this.m_Context), new WiringCheckDao(this.m_Context), new HarmonicMeasurementDao(this.m_Context), new LTMDao(this.m_Context), new ErrorTestDao(this.m_Context), new DemandTestDao(this.m_Context), new EnergyTestDao(this.m_Context), new DailyTestDao(this.m_Context), new CTMeasurementDao(this.m_Context), new CTBurdenDao(this.m_Context), new PTBurdenDao(this.m_Context), new PTComparisonDao(this.m_Context), new SiteDataTestDao(this.m_Context), new VisualDao(this.m_Context), new SealDao(this.m_Context), new CameraDao(this.m_Context), new RegisterReadingDao(this.m_Context), new SignatureDao(this.m_Context), new ReadMeterDao(this.m_Context), new TestItemDao(this.m_Context)};

    public SearchDao(Context context) {
        this.m_Context = context;
    }

    public List<DataBaseRecordItem> queryBySerialBaseDataPage(String serialNo) {
        return queryBySerialBaseDataPage(serialNo, null, null);
    }

    public List<DataBaseRecordItem> queryBySerialBaseDataPage(String serialNo, String from, String to) {
        return new TestItemDao(this.m_Context).queryItemByKey(serialNo, from, to);
    }

    public void deleteRecordBySerialList(String serialNo, List<DataBaseRecordItem> list) {
        for (int i = 0; i < this.m_Daos.length; i++) {
            this.m_Daos[i].deleteRecordByKey(serialNo, list);
        }
    }
}
