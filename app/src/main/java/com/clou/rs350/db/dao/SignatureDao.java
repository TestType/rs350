package com.clou.rs350.db.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import com.clou.rs350.CLApplication;
import com.clou.rs350.db.model.DataBaseRecordItem;
import com.clou.rs350.db.model.sequence.SignatureData;
import com.clou.rs350.db.model.sequence.SignatureItem;
import com.clou.rs350.manager.DatabaseManager;
import com.clou.rs350.utils.BitmapUtils;
import com.itextpdf.text.pdf.PdfObject;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class SignatureDao extends BaseTestDao {
    private static final String TABLE_NAME = "TestSignature";

    private static final class TableColumns {
        public static final String Name = "Name";
        public static final String SignatureIndex = "SignatureIndex";
        public static final String SignaturePath = "SignaturePath";
        public static final String SignaturesIndex = "SignaturesIndex";
        public static final String Title = "Title";

        private TableColumns() {
        }
    }

    public SignatureDao(Context context) {
        super(context, TABLE_NAME);
    }

    public void createTable(SQLiteDatabase db) {
        this.m_TableName = TABLE_NAME;
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("create table if not exists ").append(this.m_TableName).append(" (");
        stringBuffer.append("intMyID").append(" integer primary key autoincrement,");
        stringBuffer.append(this.m_TableKey).append(" VARCHAR(50),");
        stringBuffer.append("CreateTime").append(" VARCHAR(50),");
        stringBuffer.append("IsRead").append(" integer default 0,");
        stringBuffer.append(TableColumns.Title).append(" VARCHAR(50),");
        stringBuffer.append(TableColumns.Name).append(" VARCHAR(50),");
        stringBuffer.append(TableColumns.SignaturesIndex).append(" integer,");
        stringBuffer.append(TableColumns.SignatureIndex).append(" integer,");
        stringBuffer.append(TableColumns.SignaturePath).append(" VARCHAR(50));");
        db.execSQL(stringBuffer.toString());
    }

    @Override // com.clou.rs350.db.dao.BaseDao, com.clou.rs350.db.dao.BaseTestDao
    public void upgradeTable(SQLiteDatabase db, int oldVersion, int newVersion) {
        if (oldVersion <= 3) {
            db.execSQL("alter table Signature rename to TestSignature");
        }
        if (oldVersion <= 2) {
            db.execSQL("alter table TestSignature add column SignatureIndex integer default 0");
        }
        if (oldVersion < 18) {
            db.execSQL("alter table TestSignature add column Title VARCHAR(50)");
            db.execSQL("alter table TestSignature add column Name VARCHAR(50)");
        }
        if (oldVersion < 19) {
            db.execSQL("alter table TestSignature add column SignaturesIndex integer default 0");
        }
    }

    @Override // com.clou.rs350.db.dao.BaseDao, com.clou.rs350.db.dao.BaseTestDao
    public synchronized boolean insertObject(Object Data, String Time) {
        boolean z;
        SQLiteDatabase db = DatabaseManager.getWriteableDatabase(this.m_Context);
        long status = 0;
        ContentValues values = new ContentValues();
        SignatureData objects = (SignatureData) Data;
        String imagePath = CLApplication.app.getImagePath();
        for (int i = 0; i < objects.Signatures.size(); i++) {
            SignatureItem object = objects.Signatures.get(i);
            values.put(this.m_TableKey, objects.SerialNo.toString());
            values.put("CreateTime", Time);
            values.put(TableColumns.SignaturesIndex, Integer.valueOf(objects.Index));
            values.put(TableColumns.SignatureIndex, Integer.valueOf(object.Index));
            values.put(TableColumns.Title, objects.Title);
            values.put(TableColumns.Name, object.Name);
            object.SignaturePath = (String.valueOf(objects.SerialNo) + "_" + Time + "Signature_" + objects.Index + "_" + object.Index + ".jpg").replace(" ", PdfObject.NOTHING).replace("/", PdfObject.NOTHING).replace(":", PdfObject.NOTHING);
            BitmapUtils.saveBitmapFile(BitmapUtils.getScaledBitmap(object.SignatureBmp, 0.4f, 0.4f), String.valueOf(imagePath) + File.separator + object.SignaturePath);
            values.put(TableColumns.SignaturePath, object.SignaturePath);
            status += db.insert(TABLE_NAME, null, values);
        }
        if (status != -1) {
            z = true;
        } else {
            z = false;
        }
        return z;
    }

    /* access modifiers changed from: protected */
    @Override // com.clou.rs350.db.dao.BaseDao, com.clou.rs350.db.dao.BaseTestDao
    public Object parseData(Cursor cursor) {
        SignatureData values = new SignatureData();
        values.SerialNo = cursor.getString(cursor.getColumnIndex(this.m_TableKey));
        values.Title = cursor.getString(cursor.getColumnIndex(TableColumns.Title));
        do {
            SignatureItem value = new SignatureItem();
            value.Index = cursor.getInt(cursor.getColumnIndex(TableColumns.SignatureIndex));
            value.SignaturePath = cursor.getString(cursor.getColumnIndex(TableColumns.SignaturePath));
            value.Name = cursor.getString(cursor.getColumnIndex(TableColumns.Name));
            values.Signatures.add(value);
        } while (cursor.moveToNext());
        return values;
    }

    @Override // com.clou.rs350.db.dao.BaseDao
    public List<Object> queryDataListByKeyAndTime(String serialNo, String time) {
        Cursor cursor = DatabaseManager.getWriteableDatabase(this.m_Context).query(this.m_TableName, null, String.valueOf(this.m_TableKey) + " =? and " + "CreateTime" + " =?", new String[]{serialNo, time}, null, null, null);
        List<Object> Datas = new ArrayList<>();
        Datas.add(new SignatureData());
        while (cursor.moveToNext()) {
            int Index = cursor.getInt(cursor.getColumnIndex(TableColumns.SignaturesIndex));
            int Size = Datas.size() - 1;
            if (Index != ((SignatureData) Datas.get(Size)).Index) {
                Datas.add(new SignatureData());
                Size++;
            }
            ((SignatureData) Datas.get(Size)).SerialNo = cursor.getString(cursor.getColumnIndex(this.m_TableKey));
            ((SignatureData) Datas.get(Size)).Title = cursor.getString(cursor.getColumnIndex(TableColumns.Title));
            ((SignatureData) Datas.get(Size)).Index = cursor.getInt(cursor.getColumnIndex(TableColumns.SignaturesIndex));
            SignatureItem value = new SignatureItem();
            value.Index = cursor.getInt(cursor.getColumnIndex(TableColumns.SignatureIndex));
            value.SignaturePath = cursor.getString(cursor.getColumnIndex(TableColumns.SignaturePath));
            value.Name = cursor.getString(cursor.getColumnIndex(TableColumns.Name));
            ((SignatureData) Datas.get(Size)).Signatures.add(value);
        }
        cursor.close();
        if (Datas.size() > 1 || ((SignatureData) Datas.get(0)).Signatures.size() > 0) {
            return Datas;
        }
        return null;
    }

    @Override // com.clou.rs350.db.dao.BaseDao, com.clou.rs350.db.dao.BaseTestDao
    public void deleteRecordByKey(String serialNo, List<DataBaseRecordItem> list) {
        List<Object> Datas;
        String imagePath = CLApplication.app.getImagePath();
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i).isSelect() && (Datas = queryDataListByKeyAndTime(serialNo, list.get(i).getDatetime())) != null) {
                for (int j = 0; j < Datas.size(); j++) {
                    SignatureData tmpData = (SignatureData) Datas.get(j);
                    for (int k = 0; k < tmpData.Signatures.size(); k++) {
                        File tmpImg = new File(String.valueOf(imagePath) + File.separator + tmpData.Signatures.get(k).SignaturePath);
                        if (tmpImg.exists()) {
                            tmpImg.delete();
                        }
                    }
                }
            }
        }
        super.deleteRecordByKey(serialNo, list);
    }
}
