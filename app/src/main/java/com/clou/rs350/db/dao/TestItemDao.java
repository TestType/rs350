package com.clou.rs350.db.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.text.TextUtils;
import com.clou.rs350.db.model.DataBaseRecordItem;
import com.clou.rs350.db.model.TestItem;
import com.clou.rs350.manager.DatabaseManager;
import java.util.ArrayList;
import java.util.List;

public class TestItemDao extends BaseTestDao {
    private static final String TABLE_TESTITEM = "TestItem";

    private static final class TableColumns {
        public static final String AngleDefinition = "AngleDefinition";
        public static final String Designation = "Designation";
        public static final String Humidity = "Humidity";
        public static final String Meter1SerialNo = "Meter1SerialNo";
        public static final String Meter2SerialNo = "Meter2SerialNo";
        public static final String Meter3SerialNo = "Meter3SerialNo";
        public static final String OperatorID = "OperatorID";
        public static final String OperatorName = "OperatorName";
        public static final String Remark = "Remark";
        public static final String StandardCalibValidDate = "StandardCalibValidDate";
        public static final String StandardCalibrationDate = "StandardCalibrationDate";
        public static final String StandardCertificateID = "StandardCertificateID";
        public static final String StandardSerialNo = "StandardSerialNo";
        public static final String Temperature = "Temperature";
        public static final String TestItem = "TestItem";

        private TableColumns() {
        }
    }

    public TestItemDao(Context context) {
        super(context, "TestItem");
    }

    public void createTable(SQLiteDatabase db) {
        new StringBuffer();
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("create table if not exists ").append("TestItem").append(" (");
        stringBuffer.append("intMyID").append(" integer primary key autoincrement,");
        stringBuffer.append(this.m_TableKey).append(" VARCHAR(50),");
        stringBuffer.append("CreateTime").append(" VARCHAR(50),");
        stringBuffer.append(TableColumns.Meter1SerialNo).append(" VARCHAR(50),");
        stringBuffer.append(TableColumns.Meter2SerialNo).append(" VARCHAR(50),");
        stringBuffer.append(TableColumns.Meter3SerialNo).append(" VARCHAR(50),");
        stringBuffer.append("TestItem").append(" VARCHAR(50),");
        stringBuffer.append("AngleDefinition").append(" integer default 0,");
        stringBuffer.append("OperatorID").append(" VARCHAR(50),");
        stringBuffer.append("OperatorName").append(" VARCHAR(50),");
        stringBuffer.append("Designation").append(" VARCHAR(50),");
        stringBuffer.append("StandardSerialNo").append(" VARCHAR(50),");
        stringBuffer.append(TableColumns.StandardCertificateID).append(" VARCHAR(50),");
        stringBuffer.append(TableColumns.StandardCalibrationDate).append(" VARCHAR(50),");
        stringBuffer.append(TableColumns.StandardCalibValidDate).append(" VARCHAR(50),");
        stringBuffer.append("Remark").append(" VARCHAR(500),");
        stringBuffer.append(TableColumns.Temperature).append(" VARCHAR(50),");
        stringBuffer.append(TableColumns.Humidity).append(" VARCHAR(50),");
        stringBuffer.append("IsRead").append(" integer default 0);");
        db.execSQL(stringBuffer.toString());
    }

    @Override // com.clou.rs350.db.dao.BaseDao, com.clou.rs350.db.dao.BaseTestDao
    public synchronized boolean insertObject(Object Data, String Time) {
        boolean z = true;
        synchronized (this) {
            SQLiteDatabase db = DatabaseManager.getWriteableDatabase(this.m_Context);
            ContentValues values = new ContentValues();
            TestItem object = (TestItem) Data;
            values.put(this.m_TableKey, object.CustomerSerialNo);
            values.put("CreateTime", Time);
            values.put(TableColumns.Meter1SerialNo, object.MeterSerialNo[0]);
            values.put(TableColumns.Meter2SerialNo, object.MeterSerialNo[1]);
            values.put(TableColumns.Meter3SerialNo, object.MeterSerialNo[2]);
            values.put("OperatorID", object.OperatorID);
            values.put("AngleDefinition", Integer.valueOf(object.AngleDefinition));
            values.put("TestItem", object.TestItem);
            values.put("OperatorName", object.OperatorName);
            values.put("Designation", object.Designation);
            values.put(TableColumns.Temperature, object.Temperature);
            values.put(TableColumns.Humidity, object.Humidity);
            values.put("StandardSerialNo", object.StandardSerialNo);
            values.put(TableColumns.StandardCertificateID, object.CertificateID);
            values.put(TableColumns.StandardCalibrationDate, object.Calib_Date);
            values.put(TableColumns.StandardCalibValidDate, object.Calib_Valid_Date);
            values.put("Remark", object.Remark);
            if (db.insert("TestItem", null, values) == -1) {
                z = false;
            }
        }
        return z;
    }

    /* access modifiers changed from: protected */
    @Override // com.clou.rs350.db.dao.BaseDao, com.clou.rs350.db.dao.BaseTestDao
    public Object parseData(Cursor cursor) {
        TestItem value = new TestItem();
        value.CustomerSerialNo = parseString(cursor.getString(cursor.getColumnIndex(this.m_TableKey)));
        value.TestTime = parseString(cursor.getString(cursor.getColumnIndex("CreateTime")));
        value.AngleDefinition = cursor.getInt(cursor.getColumnIndex("AngleDefinition"));
        value.TestItem = parseString(cursor.getString(cursor.getColumnIndex("TestItem")));
        value.MeterSerialNo[0] = parseString(cursor.getString(cursor.getColumnIndex(TableColumns.Meter1SerialNo)));
        value.MeterSerialNo[1] = parseString(cursor.getString(cursor.getColumnIndex(TableColumns.Meter2SerialNo)));
        value.MeterSerialNo[2] = parseString(cursor.getString(cursor.getColumnIndex(TableColumns.Meter3SerialNo)));
        value.OperatorID = parseString(cursor.getString(cursor.getColumnIndex("OperatorID")));
        value.OperatorName = parseString(cursor.getString(cursor.getColumnIndex("OperatorName")));
        value.Designation = parseString(cursor.getString(cursor.getColumnIndex("Designation")));
        value.Temperature = parseString(cursor.getString(cursor.getColumnIndex(TableColumns.Temperature)));
        value.Humidity = parseString(cursor.getString(cursor.getColumnIndex(TableColumns.Humidity)));
        value.StandardSerialNo = parseString(cursor.getString(cursor.getColumnIndex("StandardSerialNo")));
        value.CertificateID = parseString(cursor.getString(cursor.getColumnIndex(TableColumns.StandardCertificateID)));
        value.Calib_Date = parseString(cursor.getString(cursor.getColumnIndex(TableColumns.StandardCalibrationDate)));
        value.Calib_Valid_Date = parseString(cursor.getString(cursor.getColumnIndex(TableColumns.StandardCalibValidDate)));
        value.Remark = parseString(cursor.getString(cursor.getColumnIndex("Remark")));
        return value;
    }

    @Override // com.clou.rs350.db.dao.BaseDao, com.clou.rs350.db.dao.BaseTestDao
    public void upgradeTable(SQLiteDatabase db, int oldVersion, int newVersion) {
        if (oldVersion < 17) {
            db.execSQL("alter table TestItem add column Meter1SerialNo VARCHAR(50)");
            db.execSQL("alter table TestItem add column Meter2SerialNo VARCHAR(50)");
            db.execSQL("alter table TestItem add column Meter3SerialNo VARCHAR(50)");
        }
        if (oldVersion < 29) {
            db.execSQL("alter table TestItem add column StandardCalibrationDate VARCHAR(50)");
        }
    }

    public List<DataBaseRecordItem> queryItemByKey(String key, String from, String to) {
        List<DataBaseRecordItem> values = null;
        SQLiteDatabase db = DatabaseManager.getWriteableDatabase(this.m_Context);
        StringBuffer sql = new StringBuffer();
        sql.append("select intMyID, CreateTime, TestItem from " + this.m_TableName);
        if (!TextUtils.isEmpty(key)) {
            sql.append(" where " + this.m_TableKey + " = '" + key + "'");
        }
        if (TextUtils.isEmpty(from) || TextUtils.isEmpty(to)) {
            if (!TextUtils.isEmpty(from)) {
                if (sql.toString().contains("where")) {
                    sql.append(" and CreateTime > '" + from + "'");
                } else {
                    sql.append(" where CreateTime > '" + from + "'");
                }
            } else if (!TextUtils.isEmpty(to)) {
                if (sql.toString().contains("where")) {
                    sql.append(" and CreateTime < '" + to + "'");
                } else {
                    sql.append(" where CreateTime < '" + to + "'");
                }
            }
        } else if (sql.toString().contains("where")) {
            sql.append(" and CreateTime > '" + from + "'");
            sql.append(" and CreateTime < '" + to + "'");
        } else {
            sql.append(" where CreateTime > '" + from + "'");
            sql.append(" and CreateTime < '" + to + "'");
        }
        try {
            Cursor cursor = db.rawQuery(sql.toString(), null);
            values = new ArrayList<>();
            while (cursor.moveToNext()) {
                values.add(new DataBaseRecordItem(cursor.getInt(0), parseString(cursor.getString(cursor.getColumnIndex("CreateTime"))), parseString(cursor.getString(cursor.getColumnIndex("TestItem"))), false));
            }
            cursor.close();
        } catch (Exception e) {
        }
        return values;
    }

    public List<DataBaseRecordItem> queryItemByKey(String key) {
        SQLiteDatabase db = DatabaseManager.getWriteableDatabase(this.m_Context);
        List<DataBaseRecordItem> values = new ArrayList<>();
        Cursor cursor = db.rawQuery("select intMyID, CreateTime, TestItem from " + this.m_TableName + " where " + this.m_TableKey + " = '" + key + "'", null);
        while (cursor.moveToNext()) {
            values.add(new DataBaseRecordItem(cursor.getInt(0), parseString(cursor.getString(cursor.getColumnIndex("CreateTime"))), parseString(cursor.getString(cursor.getColumnIndex("TestItem"))), false));
        }
        cursor.close();
        return values;
    }
}
