package com.clou.rs350.db.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.clou.rs350.R;
import com.clou.rs350.constants.Constants;
import com.clou.rs350.db.model.MeterType;
import com.clou.rs350.manager.DatabaseManager;
import com.itextpdf.text.pdf.PdfObject;

import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

public class MeterTypeDao extends BaseDao {
    private static final String TABLE_NAME = "MeterType";

    private static final class TableColumns {
        public static final String ActiveAccuracy = "ActiveAccuracy";
        public static final String ActiveConstant = "ActiveConstant";
        public static final String ApparentAccuracy = "ApparentAccuracy";
        public static final String ApparentConstant = "ApparentConstant";
        public static final String BaseCurrent = "BaseCurrent";
        public static final String CreateTime = "CreateTime";
        public static final String ID = "intMyID";
        public static final String IntWiringType = "IntWiringType";
        public static final String IsRead = "IsRead";
        public static final String Manufacturer = "Manufacturer";
        public static final String MaxCurrent = "MaxCurrent";
        public static final String MeterType = "MeterType";
        public static final String NominalFrequency = "NominalFrequency";
        public static final String NominalVoltage = "NominalVoltage";
        public static final String ReactiveAccuracy = "ReactiveAccuracy";
        public static final String ReactiveConstant = "ReactiveConstant";

        private TableColumns() {
        }
    }

    public MeterTypeDao(Context context) {
        super(context, "MeterType", "MeterType");
    }

    public void createTable(SQLiteDatabase db) {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("create table if not exists ").append("MeterType").append(" (");
        stringBuffer.append("intMyID").append(" integer primary key autoincrement,");
        stringBuffer.append("MeterType").append(" VARCHAR(50),");
        stringBuffer.append("IntWiringType").append(" integer default 0,");
        stringBuffer.append(TableColumns.ActiveAccuracy).append(" VARCHAR(50),");
        stringBuffer.append(TableColumns.ReactiveAccuracy).append(" VARCHAR(50),");
        stringBuffer.append(TableColumns.ApparentAccuracy).append(" VARCHAR(50),");
        stringBuffer.append("Manufacturer").append(" VARCHAR(50),");
        stringBuffer.append(TableColumns.ActiveConstant).append(" VARCHAR(50),");
        stringBuffer.append(TableColumns.ReactiveConstant).append(" VARCHAR(50),");
        stringBuffer.append(TableColumns.ApparentConstant).append(" VARCHAR(50),");
        stringBuffer.append(TableColumns.MaxCurrent).append(" VARCHAR(10),");
        stringBuffer.append(TableColumns.BaseCurrent).append(" VARCHAR(10),");
        stringBuffer.append("NominalVoltage").append(" VARCHAR(10),");
        stringBuffer.append("NominalFrequency").append(" VARCHAR(10),");
        stringBuffer.append("CreateTime").append(" VARCHAR(50),");
        stringBuffer.append("IsRead").append(" integer default 0);");
        db.execSQL(stringBuffer.toString());
    }

    public synchronized void insertObject(MeterType object) {
        SQLiteDatabase db = DatabaseManager.getWriteableDatabase(this.m_Context);
        ContentValues values = new ContentValues();
        Date SaveTime = new Date(System.currentTimeMillis());
        SimpleDateFormat sDateFormat = new SimpleDateFormat(Constants.TIMEFORMAT);
        values.put("MeterType", object.Type);
        values.put("IntWiringType", Integer.valueOf(object.Wiring));
        values.put(TableColumns.ActiveAccuracy, object.ActiveAccuracy);
        values.put(TableColumns.ReactiveAccuracy, object.ReactiveAccuracy);
        values.put(TableColumns.ApparentAccuracy, object.ApparentAccuracy);
        values.put("Manufacturer", object.Manufacturer);
        values.put(TableColumns.ActiveConstant, object.ActiveConstant);
        values.put(TableColumns.ReactiveConstant, object.ReactiveConstant);
        values.put(TableColumns.ApparentConstant, object.ApparentConstant);
        values.put(TableColumns.MaxCurrent, object.MaxCurrent);
        values.put(TableColumns.BaseCurrent, object.BasicCurrent);
        values.put("NominalVoltage", object.NominalVoltage);
        values.put("NominalFrequency", object.NominalFrequency);
        values.put("CreateTime", sDateFormat.format(SaveTime));
        values.put("IsRead", (Integer) 0);
        db.insert("MeterType", null, values);
    }

    public MeterType queryObjectByType(String Name) {
        MeterType value = null;
        Cursor cursor = DatabaseManager.getWriteableDatabase(this.m_Context).rawQuery("select * from MeterType where MeterType = ?", new String[]{Name});
        if (cursor.moveToNext()) {
            value = (MeterType) parseData(cursor);
        }
        cursor.close();
        return value;
    }

    public void updateObjectByName(String Name, MeterType object) {
        SQLiteDatabase db = DatabaseManager.getWriteableDatabase(this.m_Context);
        ContentValues values = new ContentValues();
        values.put("MeterType", object.Type);
        values.put("IntWiringType", Integer.valueOf(object.Wiring));
        values.put(TableColumns.ActiveAccuracy, object.ActiveAccuracy);
        values.put(TableColumns.ReactiveAccuracy, object.ReactiveAccuracy);
        values.put(TableColumns.ApparentAccuracy, object.ApparentAccuracy);
        values.put("Manufacturer", object.Manufacturer);
        values.put(TableColumns.ActiveConstant, object.ActiveConstant);
        values.put(TableColumns.ReactiveConstant, object.ReactiveConstant);
        values.put(TableColumns.ApparentConstant, object.ApparentConstant);
        values.put(TableColumns.MaxCurrent, object.MaxCurrent);
        values.put(TableColumns.BaseCurrent, object.BasicCurrent);
        values.put("NominalVoltage", object.NominalVoltage);
        values.put("NominalFrequency", object.NominalFrequency);
        values.put("CreateTime", String.valueOf(System.currentTimeMillis()));
        values.put("IsRead", (Integer) 0);
        db.update("MeterType", values, "MeterType = ?", new String[]{Name});
    }

    public List<MeterType> queryAllInfo() {
        List<MeterType> values = new ArrayList<>();
        Cursor cursor = DatabaseManager.getWriteableDatabase(this.m_Context).query("MeterType", null, null, null, null, null, null);
        while (cursor.moveToNext()) {
            values.add((MeterType) parseData(cursor));
        }
        cursor.close();
        return values;
    }

    @Override // com.clou.rs350.db.dao.BaseDao
    public void upgradeTable(SQLiteDatabase db, int oldVersion, int newVersion) {
        if (oldVersion < 20) {
            db.execSQL("alter table MeterType add column IntWiringType integer default 0");
        }
    }

    /* access modifiers changed from: protected */
    @Override // com.clou.rs350.db.dao.BaseDao
    public Object parseData(Cursor cursor) {
        MeterType value = new MeterType();
        value.Type = parseString(cursor.getString(cursor.getColumnIndex("MeterType")));
        value.Wiring = cursor.getInt(cursor.getColumnIndex("IntWiringType"));
        value.WiringType = this.m_Context.getResources().getStringArray(R.array.type_of_connection)[value.Wiring];
        value.ActiveAccuracy = parseString(cursor.getString(cursor.getColumnIndex(TableColumns.ActiveAccuracy))).replace(" ", PdfObject.NOTHING);
        value.ReactiveAccuracy = parseString(cursor.getString(cursor.getColumnIndex(TableColumns.ReactiveAccuracy))).replace(" ", PdfObject.NOTHING);
        value.ApparentAccuracy = parseString(cursor.getString(cursor.getColumnIndex(TableColumns.ApparentAccuracy))).replace(" ", PdfObject.NOTHING);
        value.Manufacturer = parseString(cursor.getString(cursor.getColumnIndex("Manufacturer")));
        value.ActiveConstant = parseString(cursor.getString(cursor.getColumnIndex(TableColumns.ActiveConstant))).replace(" ", PdfObject.NOTHING);
        value.ReactiveConstant = parseString(cursor.getString(cursor.getColumnIndex(TableColumns.ReactiveConstant))).replace(" ", PdfObject.NOTHING);
        value.ApparentConstant = parseString(cursor.getString(cursor.getColumnIndex(TableColumns.ApparentConstant))).replace(" ", PdfObject.NOTHING);
        value.MaxCurrent = parseString(cursor.getString(cursor.getColumnIndex(TableColumns.MaxCurrent))).replace(" ", PdfObject.NOTHING);
        value.BasicCurrent = parseString(cursor.getString(cursor.getColumnIndex(TableColumns.BaseCurrent))).replace(" ", PdfObject.NOTHING);
        value.NominalVoltage = parseString(cursor.getString(cursor.getColumnIndex("NominalVoltage"))).replace(" ", PdfObject.NOTHING);
        value.NominalFrequency = parseString(cursor.getString(cursor.getColumnIndex("NominalFrequency"))).replace(" ", PdfObject.NOTHING);
        value.IsRead = cursor.getInt(cursor.getColumnIndex("IsRead"));
        value.isNew = false;
        return value;
    }
}
