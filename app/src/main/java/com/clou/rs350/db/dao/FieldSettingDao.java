package com.clou.rs350.db.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import com.clou.rs350.db.model.sequence.FieldItem;
import com.clou.rs350.db.model.sequence.FieldSetting;
import com.clou.rs350.manager.DatabaseManager;
import java.util.HashMap;

public class FieldSettingDao extends BaseDao {
    private static final String TABLE_NAME = "FieldSetting";

    private static final class TableColumns {
        public static final String Element = "Element";
        public static final String Item = "Item";
        public static final String RequiredField = "RequiredField";

        private TableColumns() {
        }
    }

    public FieldSettingDao(Context context) {
        super(context, TABLE_NAME, TableColumns.Element);
    }

    public void selectData(Context context) {
        DatabaseManager.getWriteableDatabase(context).query(TABLE_NAME, null, null, null, null, null, null).close();
    }

    public void createTable(SQLiteDatabase db) {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("create table if not exists ").append(this.m_TableName).append(" ( ").append("intMyID").append(" integer primary key autoincrement,");
        stringBuffer.append("CreateTime").append(" VARCHAR(50),");
        stringBuffer.append("IsRead").append(" integer default 0,");
        stringBuffer.append(TableColumns.Element).append(" VARCHAR(50),");
        stringBuffer.append(TableColumns.Item).append(" VARCHAR(50),");
        stringBuffer.append("RequiredField").append(" integer default 0);");
        db.execSQL(stringBuffer.toString());
    }

    @Override // com.clou.rs350.db.dao.BaseDao
    public synchronized boolean insertObject(Object Data, String Time) {
        boolean z;
        SQLiteDatabase db = DatabaseManager.getWriteableDatabase(this.m_Context);
        ContentValues values = new ContentValues();
        long status = 0;
        FieldSetting objects = (FieldSetting) Data;
        for (int i = 0; i < objects.Items.size(); i++) {
            FieldItem object = objects.Items.get(objects.ItemKeys.get(i));
            values.put(this.m_TableKey, objects.Element.toString());
            values.put("CreateTime", Time);
            values.put(TableColumns.Item, object.Item);
            values.put("RequiredField", Integer.valueOf(object.RequiredSetting));
            status += db.insert(TABLE_NAME, null, values);
        }
        if (status != -1) {
            z = true;
        } else {
            z = false;
        }
        return z;
    }

    /* access modifiers changed from: protected */
    @Override // com.clou.rs350.db.dao.BaseDao
    public Object parseData(Cursor cursor) {
        FieldSetting values = new FieldSetting();
        values.Element = cursor.getString(cursor.getColumnIndex(this.m_TableKey));
        values.Items = new HashMap();
        do {
            FieldItem value = new FieldItem();
            value.RequiredSetting = cursor.getInt(cursor.getColumnIndex("RequiredField"));
            value.Item = cursor.getString(cursor.getColumnIndex(TableColumns.Item));
            values.Items.put(value.Item, value);
        } while (cursor.moveToNext());
        return values;
    }

    @Override // com.clou.rs350.db.dao.BaseDao
    public void upgradeTable(SQLiteDatabase db, int oldVersion, int newVersion) {
        if (oldVersion < 27) {
            createTable(db);
        }
        super.upgradeTable(db, oldVersion, newVersion);
    }
}
