package com.clou.rs350.db.model.sequence;

import com.itextpdf.text.pdf.PdfObject;
import java.io.Serializable;

public class SequenceItem implements Serializable {
    public int Index;
    public String ItemFunction = PdfObject.NOTHING;
    public String ItemName = PdfObject.NOTHING;
    public int MustDo = 0;

    public SequenceItem() {
    }

    public SequenceItem(int index, String name, String function, int mustDo) {
        this.Index = index;
        this.ItemName = name;
        this.ItemFunction = function;
        this.MustDo = mustDo;
    }
}
