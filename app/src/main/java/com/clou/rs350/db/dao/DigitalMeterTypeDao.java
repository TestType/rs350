package com.clou.rs350.db.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import com.clou.rs350.constants.Constants;
import com.clou.rs350.db.model.DigitalMeterType;
import com.clou.rs350.db.model.MeterType;
import com.clou.rs350.manager.DatabaseManager;
import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

public class DigitalMeterTypeDao extends BaseDao {
    private static final String TABLE_NAME = "DigitalMeterType";

    private static final class TableColumns {
        public static final String ActiveAddress = "ActiveAddress";
        public static final String ActiveDataType = "ActiveDataType";
        public static final String ActiveLength = "ActiveLength";
        public static final String ActiveMultiple = "ActiveMultiple";
        public static final String ApparentAddress = "ApparentAddress";
        public static final String ApparentDataType = "ApparentDataType";
        public static final String ApparentLength = "ApparentLength";
        public static final String ApparentMultiple = "ApparentMultiple";
        public static final String CreateTime = "CreateTime";
        public static final String ID = "intMyID";
        public static final String IsRead = "IsRead";
        public static final String MeterType = "MeterType";
        public static final String ReactiveAddress = "ReactiveAddress";
        public static final String ReactiveDataType = "ReactiveDataType";
        public static final String ReactiveLength = "ReactiveLength";
        public static final String ReactiveMultiple = "ReactiveMultiple";

        private TableColumns() {
        }
    }

    public DigitalMeterTypeDao(Context context) {
        super(context, TABLE_NAME, "MeterType");
    }

    public void createTable(SQLiteDatabase db) {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("create table if not exists ").append(TABLE_NAME).append(" (");
        stringBuffer.append("intMyID").append(" integer primary key autoincrement,");
        stringBuffer.append("MeterType").append(" VARCHAR(50),");
        stringBuffer.append(TableColumns.ActiveAddress).append(" VARCHAR(50),");
        stringBuffer.append(TableColumns.ActiveLength).append(" VARCHAR(50),");
        stringBuffer.append(TableColumns.ActiveDataType).append(" VARCHAR(50),");
        stringBuffer.append(TableColumns.ActiveMultiple).append(" VARCHAR(50),");
        stringBuffer.append(TableColumns.ReactiveAddress).append(" VARCHAR(50),");
        stringBuffer.append(TableColumns.ReactiveLength).append(" VARCHAR(50),");
        stringBuffer.append(TableColumns.ReactiveDataType).append(" VARCHAR(50),");
        stringBuffer.append(TableColumns.ReactiveMultiple).append(" VARCHAR(50),");
        stringBuffer.append(TableColumns.ApparentAddress).append(" VARCHAR(50),");
        stringBuffer.append(TableColumns.ApparentLength).append(" VARCHAR(50),");
        stringBuffer.append(TableColumns.ApparentDataType).append(" VARCHAR(50),");
        stringBuffer.append(TableColumns.ApparentMultiple).append(" VARCHAR(50),");
        stringBuffer.append("CreateTime").append(" VARCHAR(50),");
        stringBuffer.append("IsRead").append(" integer default 0);");
        db.execSQL(stringBuffer.toString());
    }

    public synchronized void insertObject(DigitalMeterType object) {
        SQLiteDatabase db = DatabaseManager.getWriteableDatabase(this.m_Context);
        ContentValues values = new ContentValues();
        Date SaveTime = new Date(System.currentTimeMillis());
        SimpleDateFormat sDateFormat = new SimpleDateFormat(Constants.TIMEFORMAT);
        values.put("MeterType", object.Type);
        values.put(TableColumns.ActiveAddress, Integer.valueOf(object.DataType[0].Address));
        values.put(TableColumns.ActiveDataType, Integer.valueOf(object.DataType[0].DataType));
        values.put(TableColumns.ActiveLength, Integer.valueOf(object.DataType[0].Length));
        values.put(TableColumns.ActiveMultiple, Float.valueOf(object.DataType[0].Multiple));
        values.put(TableColumns.ReactiveAddress, Integer.valueOf(object.DataType[1].Address));
        values.put(TableColumns.ReactiveDataType, Integer.valueOf(object.DataType[1].DataType));
        values.put(TableColumns.ReactiveLength, Integer.valueOf(object.DataType[1].Length));
        values.put(TableColumns.ReactiveMultiple, Float.valueOf(object.DataType[1].Multiple));
        values.put(TableColumns.ApparentAddress, Integer.valueOf(object.DataType[2].Address));
        values.put(TableColumns.ApparentDataType, Integer.valueOf(object.DataType[2].DataType));
        values.put(TableColumns.ApparentLength, Integer.valueOf(object.DataType[2].Length));
        values.put(TableColumns.ApparentMultiple, Float.valueOf(object.DataType[2].Multiple));
        values.put("CreateTime", sDateFormat.format(SaveTime));
        values.put("IsRead", (Integer) 0);
        db.insert(TABLE_NAME, null, values);
    }

    public DigitalMeterType queryObjectByType(String Name) {
        DigitalMeterType value = null;
        Cursor cursor = DatabaseManager.getWriteableDatabase(this.m_Context).rawQuery("select * from DigitalMeterType where MeterType = ?", new String[]{Name});
        if (cursor.moveToNext()) {
            value = (DigitalMeterType) parseData(cursor);
        }
        cursor.close();
        return value;
    }

    public void updateObjectByName(String Name, DigitalMeterType object) {
        SQLiteDatabase db = DatabaseManager.getWriteableDatabase(this.m_Context);
        ContentValues values = new ContentValues();
        values.put("MeterType", object.Type);
        values.put(TableColumns.ActiveAddress, Integer.valueOf(object.DataType[0].Address));
        values.put(TableColumns.ActiveDataType, Integer.valueOf(object.DataType[0].DataType));
        values.put(TableColumns.ActiveLength, Integer.valueOf(object.DataType[0].Length));
        values.put(TableColumns.ActiveMultiple, Float.valueOf(object.DataType[0].Multiple));
        values.put(TableColumns.ReactiveAddress, Integer.valueOf(object.DataType[1].Address));
        values.put(TableColumns.ReactiveDataType, Integer.valueOf(object.DataType[1].DataType));
        values.put(TableColumns.ReactiveLength, Integer.valueOf(object.DataType[1].Length));
        values.put(TableColumns.ReactiveMultiple, Float.valueOf(object.DataType[1].Multiple));
        values.put(TableColumns.ApparentAddress, Integer.valueOf(object.DataType[2].Address));
        values.put(TableColumns.ApparentDataType, Integer.valueOf(object.DataType[2].DataType));
        values.put(TableColumns.ApparentLength, Integer.valueOf(object.DataType[2].Length));
        values.put(TableColumns.ApparentMultiple, Float.valueOf(object.DataType[2].Multiple));
        values.put("CreateTime", String.valueOf(System.currentTimeMillis()));
        values.put("IsRead", (Integer) 0);
        db.update(TABLE_NAME, values, "MeterType = ?", new String[]{Name});
    }

    public List<MeterType> queryAllInfo() {
        List<MeterType> values = new ArrayList<>();
        Cursor cursor = DatabaseManager.getWriteableDatabase(this.m_Context).query(TABLE_NAME, null, null, null, null, null, null);
        while (cursor.moveToNext()) {
            values.add((MeterType) parseData(cursor));
        }
        cursor.close();
        return values;
    }

    /* access modifiers changed from: protected */
    @Override // com.clou.rs350.db.dao.BaseDao
    public Object parseData(Cursor cursor) {
        DigitalMeterType value = new DigitalMeterType();
        value.Type = parseString(cursor.getString(cursor.getColumnIndex("MeterType")));
        value.DataType[0].Address = cursor.getInt(cursor.getColumnIndex(TableColumns.ActiveAddress));
        value.DataType[0].Length = cursor.getInt(cursor.getColumnIndex(TableColumns.ActiveLength));
        value.DataType[0].DataType = cursor.getInt(cursor.getColumnIndex(TableColumns.ActiveDataType));
        value.DataType[0].Multiple = cursor.getFloat(cursor.getColumnIndex(TableColumns.ActiveMultiple));
        value.DataType[1].Address = cursor.getInt(cursor.getColumnIndex(TableColumns.ReactiveAddress));
        value.DataType[1].Length = cursor.getInt(cursor.getColumnIndex(TableColumns.ReactiveLength));
        value.DataType[1].DataType = cursor.getInt(cursor.getColumnIndex(TableColumns.ReactiveDataType));
        value.DataType[1].Multiple = cursor.getFloat(cursor.getColumnIndex(TableColumns.ReactiveMultiple));
        value.DataType[2].Address = cursor.getInt(cursor.getColumnIndex(TableColumns.ApparentAddress));
        value.DataType[2].Length = cursor.getInt(cursor.getColumnIndex(TableColumns.ApparentLength));
        value.DataType[2].DataType = cursor.getInt(cursor.getColumnIndex(TableColumns.ApparentDataType));
        value.DataType[2].Multiple = cursor.getFloat(cursor.getColumnIndex(TableColumns.ApparentMultiple));
        value.IsRead = cursor.getInt(cursor.getColumnIndex("IsRead"));
        value.isNew = false;
        return value;
    }

    @Override // com.clou.rs350.db.dao.BaseDao
    public void upgradeTable(SQLiteDatabase db, int oldVersion, int newVersion) {
        if (oldVersion < 16) {
            createTable(db);
        }
    }
}
