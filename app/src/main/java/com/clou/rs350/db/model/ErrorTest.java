package com.clou.rs350.db.model;

import com.clou.rs350.device.MeterBaseDevice;
import com.clou.rs350.device.model.ExplainedModel;
import com.clou.rs350.model.ClouData;
import com.clou.rs350.utils.OtherUtils;
import com.itextpdf.text.pdf.PdfObject;

public class ErrorTest extends BaseErrorTest {
    public DBDataModel[] Energy;

    public ErrorTest() {
        this.Energy = new DBDataModel[]{new DBDataModel(), new DBDataModel(), new DBDataModel(), new DBDataModel(), new DBDataModel(), new DBDataModel(), new DBDataModel(), new DBDataModel(), new DBDataModel(), new DBDataModel()};
    }

    public ErrorTest(int meterIndex) {
        this.Energy = new DBDataModel[]{new DBDataModel(), new DBDataModel(), new DBDataModel(), new DBDataModel(), new DBDataModel(), new DBDataModel(), new DBDataModel(), new DBDataModel(), new DBDataModel(), new DBDataModel()};
        this.MeterIndex = new StringBuilder(String.valueOf(meterIndex)).toString();
    }

    public ErrorTest(MeterBaseDevice Device, int meterNo) {
        this.Energy = new DBDataModel[]{new DBDataModel(), new DBDataModel(), new DBDataModel(), new DBDataModel(), new DBDataModel(), new DBDataModel(), new DBDataModel(), new DBDataModel(), new DBDataModel(), new DBDataModel()};
        parseData(Device, meterNo);
    }

    public void parseData(MeterBaseDevice Device, int meterNo) {
        parseData(Device);
        ExplainedModel[] tmpValues = Device.getErrorsValues(meterNo);
        int digit = ClouData.getInstance().getSettingBasic().ErrorDigit;
        this.hasErrorCount = tmpValues[13].getIntValue();
        int tmpPQFlag = this.PQFlag;
        String[] tmpUnits = {"Wh", "varh", "VAh"};
        for (int i = 0; i < 10; i++) {
            this.Error[i] = OtherUtils.parseRound(Double.valueOf(tmpValues[i].getDoubleValue()), digit);
            this.Energy[i] = parse(Double.valueOf((((double) ((((float) this.iPulse) * 1.0f) / this.iMeterConstant)) / (1.0d + (this.Error[i].d_Value / 100.0d))) * 1000.0d), 4, tmpUnits[tmpPQFlag]);
        }
        this.ErrorAverage = OtherUtils.parseRound(Double.valueOf(tmpValues[10].getDoubleValue()), digit).s_Value;
        this.ErrorStandard = OtherUtils.parseRound(Double.valueOf(tmpValues[11].getDoubleValue()), digit).s_Value;
        this.remainPulses = tmpValues[12].getIntValue();
    }

    public void cleanError() {
        this.hasErrorCount = 0;
        this.Error = new DBDataModel[]{new DBDataModel(), new DBDataModel(), new DBDataModel(), new DBDataModel(), new DBDataModel(), new DBDataModel(), new DBDataModel(), new DBDataModel(), new DBDataModel(), new DBDataModel()};
        this.Energy = new DBDataModel[]{new DBDataModel(), new DBDataModel(), new DBDataModel(), new DBDataModel(), new DBDataModel(), new DBDataModel(), new DBDataModel(), new DBDataModel(), new DBDataModel(), new DBDataModel()};
        this.ErrorMore = PdfObject.NOTHING;
        this.ErrorAverage = "---";
        this.ErrorStandard = "---";
    }

    public String getEnergy(int index) {
        if (this.hasErrorCount <= index) {
            this.Energy[index].s_Value = "---";
        }
        return this.Energy[index].s_Value;
    }
}
