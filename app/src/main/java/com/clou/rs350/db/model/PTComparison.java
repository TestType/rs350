package com.clou.rs350.db.model;

import com.clou.rs350.device.MeterBaseDevice;
import com.clou.rs350.device.model.ExplainedModel;
import com.clou.rs350.model.ClouData;
import com.itextpdf.text.pdf.PdfObject;

public class PTComparison extends Range {
    public DBDataModel[] AngleError = {new DBDataModel(), new DBDataModel(), new DBDataModel()};
    public DBDataModel DeviceIsHost = new DBDataModel();
    public DBDataModel[] Location1Angle = {new DBDataModel(), new DBDataModel(), new DBDataModel()};
    public DBDataModel[] Location1Voltage = {new DBDataModel(), new DBDataModel(), new DBDataModel()};
    public DBDataModel[] Location2Angle = {new DBDataModel(), new DBDataModel(), new DBDataModel()};
    public DBDataModel[] Location2Voltage = {new DBDataModel(), new DBDataModel(), new DBDataModel()};
    public DBDataModel[] RatioError = {new DBDataModel(), new DBDataModel(), new DBDataModel()};
    public String SerialNo = PdfObject.NOTHING;
    public String Time = "---";
    public DBDataModel[] VoltageDifferent = {new DBDataModel(), new DBDataModel(), new DBDataModel()};

    public void parseData(MeterBaseDevice Device) {
        ExplainedModel[] tmpData = Device.getPTErrorValue();
        int digit = ClouData.getInstance().getSettingBasic().ErrorDigit;
        for (int index = 0; index < 3; index++) {
            this.Location1Voltage[index] = parse(tmpData[index]);
            this.Location1Angle[index] = parse(tmpData[index + 3]);
            this.Location2Voltage[index] = parse(tmpData[index + 6]);
            this.Location2Angle[index] = parse(tmpData[index + 9]);
            this.RatioError[index] = parse(Double.valueOf(this.Location2Voltage[index].d_Value - this.Location1Voltage[index].d_Value), "V");
            this.AngleError[index] = parse(tmpData[index + 15]);
            this.VoltageDifferent[index] = parseRound(Double.valueOf(this.RatioError[index].d_Value / this.Location1Voltage[index].d_Value), digit);
        }
        this.Time = tmpData[21].getStringValue();
    }

    public boolean isEmpty() {
        for (int i = 0; i < 3; i++) {
        }
        return true;
    }

    public void cleanData() {
        this.Location1Voltage = new DBDataModel[]{new DBDataModel(), new DBDataModel(), new DBDataModel()};
        this.Location1Angle = new DBDataModel[]{new DBDataModel(), new DBDataModel(), new DBDataModel()};
        this.Location2Voltage = new DBDataModel[]{new DBDataModel(), new DBDataModel(), new DBDataModel()};
        this.Location2Angle = new DBDataModel[]{new DBDataModel(), new DBDataModel(), new DBDataModel()};
        this.VoltageDifferent = new DBDataModel[]{new DBDataModel(), new DBDataModel(), new DBDataModel()};
        this.AngleError = new DBDataModel[]{new DBDataModel(), new DBDataModel(), new DBDataModel()};
        this.RatioError = new DBDataModel[]{new DBDataModel(), new DBDataModel(), new DBDataModel()};
    }
}
