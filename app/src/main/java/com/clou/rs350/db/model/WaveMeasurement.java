package com.clou.rs350.db.model;

import com.clou.rs350.device.MeterBaseDevice;
import com.clou.rs350.device.model.ExplainedModel;
import com.itextpdf.text.pdf.PdfObject;

public class WaveMeasurement extends BasicMeasurement {
    public DBDataModel CurrentValueL1 = new DBDataModel();
    public DBDataModel CurrentValueL2 = new DBDataModel();
    public DBDataModel CurrentValueL3 = new DBDataModel();
    public String CurrentWaveformL1 = PdfObject.NOTHING;
    public String CurrentWaveformL2 = PdfObject.NOTHING;
    public String CurrentWaveformL3 = PdfObject.NOTHING;
    public String PWaveformL1 = PdfObject.NOTHING;
    public String PWaveformL2 = PdfObject.NOTHING;
    public String PWaveformL3 = PdfObject.NOTHING;
    public String QWaveformL1 = PdfObject.NOTHING;
    public String QWaveformL2 = PdfObject.NOTHING;
    public String QWaveformL3 = PdfObject.NOTHING;
    public String SerialNo = PdfObject.NOTHING;
    public DBDataModel VoltageValueL1 = new DBDataModel();
    public DBDataModel VoltageValueL2 = new DBDataModel();
    public DBDataModel VoltageValueL3 = new DBDataModel();
    public String VoltageWaveformL1 = PdfObject.NOTHING;
    public String VoltageWaveformL2 = PdfObject.NOTHING;
    public String VoltageWaveformL3 = PdfObject.NOTHING;
    public WaveValue waveIa = new WaveValue();
    public WaveValue waveIb = new WaveValue();
    public WaveValue waveIc = new WaveValue();
    public WaveValue wavePa = new WaveValue();
    public WaveValue wavePb = new WaveValue();
    public WaveValue wavePc = new WaveValue();
    public WaveValue waveQa = new WaveValue();
    public WaveValue waveQb = new WaveValue();
    public WaveValue waveQc = new WaveValue();
    public WaveValue waveUa = new WaveValue();
    public WaveValue waveUb = new WaveValue();
    public WaveValue waveUc = new WaveValue();

    @Override // com.clou.rs350.db.model.BasicMeasurement
    public void parseData(MeterBaseDevice Device) {
        parseRangeData(Device);
        ExplainedModel[] TmpValues = Device.getLNVoltageValue();
        this.VoltageValueL1 = parse(TmpValues[0]);
        this.VoltageValueL2 = parse(TmpValues[1]);
        this.VoltageValueL3 = parse(TmpValues[2]);
        ExplainedModel[] TmpValues2 = Device.getCurrentValue();
        this.CurrentValueL1 = parse(TmpValues2[0]);
        this.CurrentValueL2 = parse(TmpValues2[1]);
        this.CurrentValueL3 = parse(TmpValues2[2]);
        ExplainedModel[] TmpUIPPValues = Device.getUIPPValue();
        ExplainedModel[] TmpPQPPValues = Device.getPQPPValue();
        this.waveUa = new WaveValue(parse(TmpUIPPValues[0]), parse(TmpUIPPValues[1]), Device.getUIWaveValue(0));
        this.waveIa = new WaveValue(parse(TmpUIPPValues[2]), parse(TmpUIPPValues[3]), Device.getUIWaveValue(3));
        this.wavePa = new WaveValue(parse(TmpPQPPValues[0]), parse(TmpPQPPValues[1]), Device.getPQWaveValue(0));
        this.waveQa = new WaveValue(parse(TmpPQPPValues[2]), parse(TmpPQPPValues[3]), Device.getPQWaveValue(3));
        if (this.Wiring == 0) {
            this.waveUb = new WaveValue(parse(TmpUIPPValues[4]), parse(TmpUIPPValues[5]), Device.getUIWaveValue(1));
            this.waveIb = new WaveValue(parse(TmpUIPPValues[6]), parse(TmpUIPPValues[7]), Device.getUIWaveValue(4));
            this.wavePb = new WaveValue(parse(TmpPQPPValues[4]), parse(TmpPQPPValues[5]), Device.getPQWaveValue(1));
            this.waveQb = new WaveValue(parse(TmpPQPPValues[6]), parse(TmpPQPPValues[7]), Device.getPQWaveValue(4));
        } else {
            this.waveUb = new WaveValue();
            this.waveIb = new WaveValue();
            this.wavePb = new WaveValue();
            this.waveQb = new WaveValue();
        }
        if (2 != this.Wiring) {
            this.waveUc = new WaveValue(parse(TmpUIPPValues[8]), parse(TmpUIPPValues[9]), Device.getUIWaveValue(2));
            this.waveIc = new WaveValue(parse(TmpUIPPValues[10]), parse(TmpUIPPValues[11]), Device.getUIWaveValue(5));
            this.wavePc = new WaveValue(parse(TmpPQPPValues[8]), parse(TmpPQPPValues[9]), Device.getPQWaveValue(2));
            this.waveQc = new WaveValue(parse(TmpPQPPValues[10]), parse(TmpPQPPValues[11]), Device.getPQWaveValue(5));
            return;
        }
        this.waveUc = new WaveValue();
        this.waveIc = new WaveValue();
        this.wavePc = new WaveValue();
        this.waveQc = new WaveValue();
    }

    public void buildStringData() {
        this.VoltageWaveformL1 = parseString(this.waveUa.Wave);
        this.VoltageWaveformL2 = parseString(this.waveUb.Wave);
        this.VoltageWaveformL3 = parseString(this.waveUc.Wave);
        this.CurrentWaveformL1 = parseString(this.waveIa.Wave);
        this.CurrentWaveformL2 = parseString(this.waveIb.Wave);
        this.CurrentWaveformL3 = parseString(this.waveIc.Wave);
        this.PWaveformL1 = parseString(this.wavePa.Wave);
        this.PWaveformL2 = parseString(this.wavePb.Wave);
        this.PWaveformL3 = parseString(this.wavePc.Wave);
        this.QWaveformL1 = parseString(this.waveQa.Wave);
        this.QWaveformL2 = parseString(this.waveQb.Wave);
        this.QWaveformL3 = parseString(this.waveQc.Wave);
    }

    private String parseString(Double[] dataArray) {
        if (dataArray == null) {
            return PdfObject.NOTHING;
        }
        StringBuffer sb = new StringBuffer();
        if (dataArray != null && dataArray.length > 0) {
            for (int i = 0; i < dataArray.length; i++) {
                if (i == 0) {
                    sb.append(dataArray[i].toString());
                } else {
                    sb.append("|");
                    sb.append(dataArray[i].toString());
                }
            }
        }
        return sb.toString();
    }
}
