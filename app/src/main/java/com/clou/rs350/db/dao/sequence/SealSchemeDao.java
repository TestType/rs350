package com.clou.rs350.db.dao.sequence;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import com.clou.rs350.db.dao.sequence.BaseSchemeDao;
import com.clou.rs350.db.model.sequence.SealData;
import com.clou.rs350.db.model.sequence.SealItem;
import com.clou.rs350.manager.DatabaseManager;

public class SealSchemeDao extends BaseSchemeDao {
    private static final String TABLE_NAME = "SealScheme";
    protected Context m_Context;

    private static final class TableColumns {
        public static final String ID = "intMyID";
        public static final String IsRead = "IsRead";
        public static final String RequiredField = "RequiredField";
        public static final String SchemeName = "SchemeName";
        public static final String SealLocation = "SealLocation";

        private TableColumns() {
        }
    }

    public SealSchemeDao(Context context) {
        super(context, TABLE_NAME, "SchemeName");
    }

    public void createTable(SQLiteDatabase db) {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("create table if not exists ").append(TABLE_NAME).append(" (");
        stringBuffer.append("intMyID").append(" integer primary key autoincrement,");
        stringBuffer.append("SchemeName").append(" VARCHAR(50),");
        stringBuffer.append("SealLocation").append(" VARCHAR(50),");
        stringBuffer.append("RequiredField").append(" integer default 0,");
        stringBuffer.append(SchemeTableColumns.Language).append(" integer,");
        stringBuffer.append("IsRead").append(" integer default 0);");
        db.execSQL(stringBuffer.toString());
    }

    public synchronized boolean insertObject(Object Data) {
        boolean z = false;
        synchronized (this) {
            SQLiteDatabase db = DatabaseManager.getWriteableDatabase(this.m_Context);
            ContentValues values = new ContentValues();
            long status = 0;
            SealData objects = (SealData) Data;
            for (int i = 0; i < objects.Seals.size(); i++) {
                SealItem object = objects.Seals.get(i);
                values.put("SchemeName", objects.SchemeName.toString());
                values.put(SchemeTableColumns.Language, Integer.valueOf(objects.Language));
                values.put("SealLocation", object.SealLocation);
                values.put("RequiredField", Integer.valueOf(object.RequiredField));
                values.put("IsRead", (Integer) 0);
                status += db.insert(TABLE_NAME, null, values);
            }
            if (status != -1) {
                z = true;
            }
        }
        return z;
    }

    /* access modifiers changed from: protected */
    @Override // com.clou.rs350.db.dao.BaseDao
    public SealData parseData(Cursor cursor) {
        SealData values = new SealData();
        values.SchemeName = parseString(cursor.getString(cursor.getColumnIndex("SchemeName")));
        values.Language = cursor.getInt(cursor.getColumnIndex(SchemeTableColumns.Language));
        do {
            SealItem value = new SealItem();
            value.SealLocation = parseString(cursor.getString(cursor.getColumnIndex("SealLocation")));
            value.RequiredField = cursor.getInt(cursor.getColumnIndex("RequiredField"));
            values.Seals.add(value);
        } while (cursor.moveToNext());
        return values;
    }

    @Override // com.clou.rs350.db.dao.BaseDao, com.clou.rs350.db.dao.sequence.BaseSchemeDao
    public void upgradeTable(SQLiteDatabase db, int oldVersion, int newVersion) {
        if (oldVersion < 25) {
            db.execSQL("alter table SealScheme add column RequiredField integer default 0");
        }
        super.upgradeTable(db, oldVersion, newVersion);
    }
}
