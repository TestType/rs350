package com.clou.rs350.db.model;

import com.clou.rs350.device.MeterBaseDevice;
import com.clou.rs350.device.model.ExplainedModel;
import com.clou.rs350.model.ClouData;
import com.clou.rs350.utils.OtherUtils;
import com.itextpdf.text.pdf.PdfObject;

public class DailyTest extends BaseErrorTest {
    public DailyTest() {
        this.iMeterConstant = 1.0f;
        this.MeterConstant = new StringBuilder(String.valueOf(this.iMeterConstant)).toString();
        this.Accuracy = 0.5d;
    }

    public DailyTest(int meterIndex) {
        this.MeterIndex = new StringBuilder(String.valueOf(meterIndex)).toString();
        this.iMeterConstant = 1.0f;
        this.MeterConstant = new StringBuilder(String.valueOf(this.iMeterConstant)).toString();
        this.Accuracy = 0.5d;
    }

    public DailyTest(MeterBaseDevice Device, int meterNo) {
        parseData(Device, meterNo, false);
    }

    public void parseData(MeterBaseDevice Device, int meterNo, boolean parseEnergy) {
        parseData(Device);
        ExplainedModel[] tmpValues = Device.getErrorsValues(meterNo);
        int digit = ClouData.getInstance().getSettingBasic().ErrorDigit;
        this.hasErrorCount = tmpValues[13].getIntValue();
        for (int i = 0; i < 10; i++) {
            this.Error[i] = OtherUtils.parseRound(Double.valueOf(tmpValues[i].getDoubleValue()), digit);
        }
        this.ErrorAverage = OtherUtils.parseRound(Double.valueOf(tmpValues[10].getDoubleValue()), digit).s_Value;
        this.ErrorStandard = OtherUtils.parseRound(Double.valueOf(tmpValues[11].getDoubleValue()), digit).s_Value;
        this.remainPulses = tmpValues[12].getIntValue();
    }

    public void cleanError() {
        this.hasErrorCount = 0;
        this.Error = new DBDataModel[]{new DBDataModel(), new DBDataModel(), new DBDataModel(), new DBDataModel(), new DBDataModel(), new DBDataModel(), new DBDataModel(), new DBDataModel(), new DBDataModel(), new DBDataModel()};
        this.ErrorMore = PdfObject.NOTHING;
        this.ErrorAverage = "---";
        this.ErrorStandard = "---";
    }
}
