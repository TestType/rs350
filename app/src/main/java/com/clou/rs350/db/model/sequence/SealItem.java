package com.clou.rs350.db.model.sequence;

import com.itextpdf.text.pdf.PdfObject;
import java.io.Serializable;

public class SealItem implements Serializable {
    public int Index;
    public int RequiredField = 0;
    public String SealLocation = PdfObject.NOTHING;
    public String SealSr = PdfObject.NOTHING;
}
