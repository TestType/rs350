package com.clou.rs350.db.model;

import com.itextpdf.text.pdf.PdfObject;
import java.io.Serializable;

public class OperatorInfo implements Serializable {
    public int Authority = 0;
    public String Designation = PdfObject.NOTHING;
    public String OperatorID = PdfObject.NOTHING;
    public String OperatorName = PdfObject.NOTHING;
    public String Password = PdfObject.NOTHING;
    public boolean isNew = true;
    private OperatorInfo meterBaseInfo = null;

    public OperatorInfo getCustomerInfo() {
        if (this.meterBaseInfo == null) {
            this.meterBaseInfo = new OperatorInfo();
        }
        return this.meterBaseInfo;
    }
}
