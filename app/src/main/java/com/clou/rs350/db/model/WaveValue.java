package com.clou.rs350.db.model;

import java.io.Serializable;

public class WaveValue implements Serializable {
    public DBDataModel MaxValue = new DBDataModel();
    public DBDataModel MinValue = new DBDataModel();
    public Double[] Wave = null;
    public DBDataModel max = new DBDataModel();

    public WaveValue() {
    }

    public WaveValue(DBDataModel maxValue, DBDataModel minValue, Double[] wave) {
        this.MaxValue = maxValue;
        this.MinValue = minValue;
        if (Math.abs(maxValue.d_Value) > Math.abs(minValue.d_Value)) {
            this.max.d_Value = Math.abs(maxValue.d_Value);
            this.max.s_Value = maxValue.s_Value;
        } else {
            this.max.d_Value = Math.abs(minValue.d_Value);
            this.max.s_Value = minValue.s_Value;
        }
        this.Wave = wave;
    }
}
