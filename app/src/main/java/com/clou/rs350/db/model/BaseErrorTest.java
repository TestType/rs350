package com.clou.rs350.db.model;

import android.text.TextUtils;
import com.clou.rs350.device.MeterBaseDevice;
import com.clou.rs350.utils.OtherUtils;
import com.itextpdf.text.pdf.ColumnText;
import com.itextpdf.text.pdf.PdfObject;

public class BaseErrorTest extends BasicMeasurement {
    public double Accuracy;
    public String ConstantUnit;
    public DBDataModel[] Error;
    public String ErrorAverage;
    public int ErrorCount;
    public String ErrorMore;
    public String ErrorStandard;
    public String MeterConstant;
    public String MeterIndex;
    public String MeterSerialNo;
    public String PulseSamplingMethod;
    public String Pulses;
    public DBDataModel Result;
    public String RunTime;
    public int hasErrorCount;
    public float iMeterConstant;
    public int iPulse;
    public int iPulseSamplingMethod;
    public int remainPulses;

    public BaseErrorTest() {
        this.MeterSerialNo = PdfObject.NOTHING;
        this.Accuracy = 0.0d;
        this.Pulses = PdfObject.NOTHING;
        this.Result = new DBDataModel(-1L, PdfObject.NOTHING);
        this.PulseSamplingMethod = PdfObject.NOTHING;
        this.iPulseSamplingMethod = 0;
        this.MeterConstant = PdfObject.NOTHING;
        this.ConstantUnit = PdfObject.NOTHING;
        this.iMeterConstant = ColumnText.GLOBAL_SPACE_CHAR_RATIO;
        this.iPulse = 0;
        this.MeterIndex = "0";
        this.ErrorCount = 5;
        this.RunTime = PdfObject.NOTHING;
        this.Error = new DBDataModel[]{new DBDataModel(), new DBDataModel(), new DBDataModel(), new DBDataModel(), new DBDataModel(), new DBDataModel(), new DBDataModel(), new DBDataModel(), new DBDataModel(), new DBDataModel()};
        this.ErrorMore = PdfObject.NOTHING;
        this.ErrorAverage = "---";
        this.ErrorStandard = "---";
        this.remainPulses = 0;
        this.hasErrorCount = 0;
    }

    public BaseErrorTest(int meterIndex) {
        this.MeterSerialNo = PdfObject.NOTHING;
        this.Accuracy = 0.0d;
        this.Pulses = PdfObject.NOTHING;
        this.Result = new DBDataModel(-1L, PdfObject.NOTHING);
        this.PulseSamplingMethod = PdfObject.NOTHING;
        this.iPulseSamplingMethod = 0;
        this.MeterConstant = PdfObject.NOTHING;
        this.ConstantUnit = PdfObject.NOTHING;
        this.iMeterConstant = ColumnText.GLOBAL_SPACE_CHAR_RATIO;
        this.iPulse = 0;
        this.MeterIndex = "0";
        this.ErrorCount = 5;
        this.RunTime = PdfObject.NOTHING;
        this.Error = new DBDataModel[]{new DBDataModel(), new DBDataModel(), new DBDataModel(), new DBDataModel(), new DBDataModel(), new DBDataModel(), new DBDataModel(), new DBDataModel(), new DBDataModel(), new DBDataModel()};
        this.ErrorMore = PdfObject.NOTHING;
        this.ErrorAverage = "---";
        this.ErrorStandard = "---";
        this.remainPulses = 0;
        this.hasErrorCount = 0;
        this.MeterIndex = new StringBuilder(String.valueOf(meterIndex)).toString();
    }

    public BaseErrorTest(MeterBaseDevice Device, int meterNo) {
        this.MeterSerialNo = PdfObject.NOTHING;
        this.Accuracy = 0.0d;
        this.Pulses = PdfObject.NOTHING;
        this.Result = new DBDataModel(-1L, PdfObject.NOTHING);
        this.PulseSamplingMethod = PdfObject.NOTHING;
        this.iPulseSamplingMethod = 0;
        this.MeterConstant = PdfObject.NOTHING;
        this.ConstantUnit = PdfObject.NOTHING;
        this.iMeterConstant = ColumnText.GLOBAL_SPACE_CHAR_RATIO;
        this.iPulse = 0;
        this.MeterIndex = "0";
        this.ErrorCount = 5;
        this.RunTime = PdfObject.NOTHING;
        this.Error = new DBDataModel[]{new DBDataModel(), new DBDataModel(), new DBDataModel(), new DBDataModel(), new DBDataModel(), new DBDataModel(), new DBDataModel(), new DBDataModel(), new DBDataModel(), new DBDataModel()};
        this.ErrorMore = PdfObject.NOTHING;
        this.ErrorAverage = "---";
        this.ErrorStandard = "---";
        this.remainPulses = 0;
        this.hasErrorCount = 0;
    }

    public int getPulsesProgress() {
        return this.remainPulses;
    }

    public String getError(int index) {
        if (this.hasErrorCount <= index) {
            this.Error[index].s_Value = "---";
        }
        return this.Error[index].s_Value;
    }

    public String getErrorStandard(int ErrorCount2) {
        if (this.hasErrorCount < ErrorCount2) {
            this.ErrorStandard = "---";
        }
        return this.ErrorStandard;
    }

    public String getErrorAverage(int ErrorCount2) {
        if (this.hasErrorCount < ErrorCount2) {
            this.ErrorAverage = "---";
        }
        return this.ErrorAverage;
    }

    public int getPass(int ErrorCount2) {
        return getPass(ErrorCount2, this.Accuracy);
    }

    public int getPass(int ErrorCount2, double Accuracy2) {
        this.Result = new DBDataModel(-1L, PdfObject.NOTHING);
        try {
            if (this.hasErrorCount >= ErrorCount2) {
                double absErrorAvg = Math.abs(Double.valueOf(this.ErrorAverage).doubleValue());
                if (Accuracy2 != 0.0d) {
                    if (absErrorAvg < Accuracy2) {
                        this.Result.l_Value = 0;
                    } else {
                        this.Result.l_Value = 1;
                    }
                }
            }
        } catch (Exception e) {
        }
        this.Result.s_Value = OtherUtils.getErrorResult((int) this.Result.l_Value);
        return (int) this.Result.l_Value;
    }

    @Override // com.clou.rs350.db.model.BasicMeasurement
    public boolean isEmpty() {
        return TextUtils.isEmpty(this.Error[0].s_Value) || "---".equals(this.Error[0].s_Value);
    }
}
