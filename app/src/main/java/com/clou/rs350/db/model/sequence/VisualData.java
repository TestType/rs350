package com.clou.rs350.db.model.sequence;

import com.itextpdf.text.pdf.PdfObject;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class VisualData implements Serializable {
    public String CreatTime = PdfObject.NOTHING;
    public int Index = 0;
    public int Language = 0;
    public String Remark = PdfObject.NOTHING;
    public String SchemeName = PdfObject.NOTHING;
    public String SerialNo = PdfObject.NOTHING;
    public List<VisualItem> Visuals = new ArrayList();

    public VisualData() {
    }

    public VisualData(int index) {
        this.Index = index;
    }
}
