package com.clou.rs350.db.model.sequence;

import com.itextpdf.text.pdf.PdfObject;
import java.io.Serializable;

public class RegisterReadingItem implements Serializable {
    public int Index;
    public String Register = PdfObject.NOTHING;
    public String RegisterValue = PdfObject.NOTHING;
    public int RequiredField = 0;
}
