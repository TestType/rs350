package com.clou.rs350.db.model;

import com.clou.rs350.device.model.Baudrate;
import com.clou.rs350.utils.OtherUtils;

public class ReadMeter extends BasicMeasurement {
    public String Address = "---";
    public String DateAndTime = "---";
    public String[] Energys = {"---", "---", "---", "---", "---", "---", "---", "---", "---", "---", "---", "---", "---", "---", "---", "---", "---", "---", "---", "---"};
    public String I1 = "---";
    public String I2 = "---";
    public String I3 = "---";
    public String MeterIndex = "0";
    public int Prorocol = 0;
    public String Psum = "---";
    public String Qsum = "---";
    public String StandardTime = "---";
    public String Time = "---";
    public String TimeError = "---";
    public String U1 = "---";
    public String U2 = "---";
    public String U3 = "---";
    public Baudrate bBaudrate = new Baudrate();

    public void CalTimeError() {
        String[] tmpStandardTime = this.StandardTime.split(":");
        String[] tmpTime = this.Time.split(":");
        if (tmpStandardTime.length == 3 && tmpTime.length == 3) {
            int iError = (((OtherUtils.parseInt(tmpTime[0]) * 3600) + (OtherUtils.parseInt(tmpTime[1]) * 60)) + OtherUtils.parseInt(tmpTime[2])) - (((OtherUtils.parseInt(tmpStandardTime[0]) * 3600) + (OtherUtils.parseInt(tmpStandardTime[1]) * 60)) + OtherUtils.parseInt(tmpStandardTime[2]));
            if (iError < -43200) {
                iError += 43200;
            } else if (iError > 43200) {
                iError -= 43200;
            }
            this.TimeError = String.valueOf(iError / 60) + ":" + Math.abs(iError % 60);
            return;
        }
        this.TimeError = "---";
    }

    @Override // com.clou.rs350.db.model.BasicMeasurement
    public boolean isEmpty() {
        return this.Address.equals("---");
    }
}
