package com.clou.rs350.db.model.sequence;

import com.itextpdf.text.pdf.PdfObject;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class SealData implements Serializable {
    public String CreatTime = PdfObject.NOTHING;
    public int Index = 0;
    public int Language = 0;
    public String SchemeName = PdfObject.NOTHING;
    public int SealCount = 0;
    public List<SealItem> Seals = new ArrayList();
    public String SerialNo = PdfObject.NOTHING;
    public int isAfter = 0;

    public SealData() {
    }

    public SealData(int index) {
        this.Index = index;
    }
}
