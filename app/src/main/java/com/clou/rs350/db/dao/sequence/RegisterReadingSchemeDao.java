package com.clou.rs350.db.dao.sequence;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import com.clou.rs350.db.dao.sequence.BaseSchemeDao;
import com.clou.rs350.db.model.sequence.RegisterReadingData;
import com.clou.rs350.db.model.sequence.RegisterReadingItem;
import com.clou.rs350.manager.DatabaseManager;

public class RegisterReadingSchemeDao extends BaseSchemeDao {
    private static final String TABLE_NAME = "RegisterReadingScheme";

    private static final class TableColumns {
        public static final String ID = "intMyID";
        public static final String IsRead = "IsRead";
        public static final String Register = "Register";
        public static final String RequiredField = "RequiredField";
        public static final String SchemeName = "SchemeName";

        private TableColumns() {
        }
    }

    public RegisterReadingSchemeDao(Context context) {
        super(context, TABLE_NAME, "SchemeName");
    }

    public void createTable(SQLiteDatabase db) {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("create table if not exists ").append(TABLE_NAME).append(" (");
        stringBuffer.append("intMyID").append(" integer primary key autoincrement,");
        stringBuffer.append("SchemeName").append(" VARCHAR(50),");
        stringBuffer.append("Register").append(" VARCHAR(50),");
        stringBuffer.append("RequiredField").append(" integer default 0,");
        stringBuffer.append(SchemeTableColumns.Language).append(" integer,");
        stringBuffer.append("IsRead").append(" integer default 0);");
        db.execSQL(stringBuffer.toString());
    }

    public synchronized boolean insertObject(Object Data) {
        boolean z = false;
        synchronized (this) {
            SQLiteDatabase db = DatabaseManager.getWriteableDatabase(this.m_Context);
            ContentValues values = new ContentValues();
            long status = 0;
            RegisterReadingData objects = (RegisterReadingData) Data;
            for (int i = 0; i < objects.Registers.size(); i++) {
                RegisterReadingItem object = objects.Registers.get(i);
                values.put("SchemeName", objects.SchemeName.toString());
                values.put(SchemeTableColumns.Language, Integer.valueOf(objects.Language));
                values.put("RequiredField", Integer.valueOf(object.RequiredField));
                values.put("Register", object.Register);
                values.put("IsRead", (Integer) 0);
                status += db.insert(TABLE_NAME, null, values);
            }
            if (status != -1) {
                z = true;
            }
        }
        return z;
    }

    /* access modifiers changed from: protected */
    @Override // com.clou.rs350.db.dao.BaseDao
    public RegisterReadingData parseData(Cursor cursor) {
        RegisterReadingData values = new RegisterReadingData();
        values.SchemeName = parseString(cursor.getString(cursor.getColumnIndex("SchemeName")));
        values.Language = cursor.getInt(cursor.getColumnIndex(SchemeTableColumns.Language));
        do {
            RegisterReadingItem value = new RegisterReadingItem();
            value.Register = parseString(cursor.getString(cursor.getColumnIndex("Register")));
            value.RequiredField = cursor.getInt(cursor.getColumnIndex("RequiredField"));
            values.Registers.add(value);
        } while (cursor.moveToNext());
        return values;
    }

    @Override // com.clou.rs350.db.dao.BaseDao, com.clou.rs350.db.dao.sequence.BaseSchemeDao
    public void upgradeTable(SQLiteDatabase db, int oldVersion, int newVersion) {
        if (oldVersion <= 1) {
            createTable(db);
            return;
        }
        if (oldVersion < 25) {
            db.execSQL("alter table RegisterReadingScheme add column RequiredField integer default 0");
        }
        super.upgradeTable(db, oldVersion, newVersion);
    }
}
