package com.clou.rs350.db.model;

import com.clou.rs350.device.MeterBaseDevice;
import com.clou.rs350.device.model.ExplainedModel;
import com.clou.rs350.utils.OtherUtils;
import com.itextpdf.text.pdf.PdfObject;

public class BasicMeasurement extends Range {
    public DBDataModel ActivePowerL1 = new DBDataModel();
    public DBDataModel ActivePowerL2 = new DBDataModel();
    public DBDataModel ActivePowerL3 = new DBDataModel();
    public DBDataModel ActivePowerTotal = new DBDataModel();
    public DBDataModel AngleU12I1 = new DBDataModel();
    public DBDataModel AngleU23I2 = new DBDataModel();
    public DBDataModel AngleU31I3 = new DBDataModel();
    public DBDataModel ApparentPowerL1 = new DBDataModel();
    public DBDataModel ApparentPowerL2 = new DBDataModel();
    public DBDataModel ApparentPowerL3 = new DBDataModel();
    public DBDataModel ApparentPowerTotal = new DBDataModel();
    public DBDataModel ApparentPowerType = new DBDataModel();
    public DBDataModel CFI1 = new DBDataModel();
    public DBDataModel CFI2 = new DBDataModel();
    public DBDataModel CFI3 = new DBDataModel();
    public DBDataModel CFU1 = new DBDataModel();
    public DBDataModel CFU2 = new DBDataModel();
    public DBDataModel CFU3 = new DBDataModel();
    public DBDataModel CurrentAngleL1 = new DBDataModel();
    public DBDataModel CurrentAngleL2 = new DBDataModel();
    public DBDataModel CurrentAngleL3 = new DBDataModel();
    public DBDataModel CurrentLN = new DBDataModel();
    public String CurrentPhaseSequence = PdfObject.NOTHING;
    public DBDataModel CurrentValueL1 = new DBDataModel();
    public DBDataModel CurrentValueL2 = new DBDataModel();
    public DBDataModel CurrentValueL3 = new DBDataModel();
    public String CustomerSerialNo = PdfObject.NOTHING;
    public DBDataModel Frequency = new DBDataModel();
    public String PhaseSequence = PdfObject.NOTHING;
    public DBDataModel PowerFactorL1 = new DBDataModel();
    public DBDataModel PowerFactorL2 = new DBDataModel();
    public DBDataModel PowerFactorL3 = new DBDataModel();
    public DBDataModel PowerFactorTotal = new DBDataModel();
    public DBDataModel ReactivePowerL1 = new DBDataModel();
    public DBDataModel ReactivePowerL2 = new DBDataModel();
    public DBDataModel ReactivePowerL3 = new DBDataModel();
    public DBDataModel ReactivePowerTotal = new DBDataModel();
    public DBDataModel THDI1 = new DBDataModel();
    public DBDataModel THDI2 = new DBDataModel();
    public DBDataModel THDI3 = new DBDataModel();
    public DBDataModel THDU1 = new DBDataModel();
    public DBDataModel THDU2 = new DBDataModel();
    public DBDataModel THDU3 = new DBDataModel();
    public DBDataModel VoltageAngleL1 = new DBDataModel();
    public DBDataModel VoltageAngleL1L2 = new DBDataModel();
    public DBDataModel VoltageAngleL1Show = new DBDataModel();
    public DBDataModel VoltageAngleL1Vector = new DBDataModel();
    public DBDataModel VoltageAngleL2 = new DBDataModel();
    public DBDataModel VoltageAngleL2L3 = new DBDataModel();
    public DBDataModel VoltageAngleL2Show = new DBDataModel();
    public DBDataModel VoltageAngleL2Vector = new DBDataModel();
    public DBDataModel VoltageAngleL3 = new DBDataModel();
    public DBDataModel VoltageAngleL3L1 = new DBDataModel();
    public DBDataModel VoltageAngleL3Show = new DBDataModel();
    public DBDataModel VoltageAngleL3Vector = new DBDataModel();
    public DBDataModel VoltageCurrentAngleL1 = new DBDataModel();
    public DBDataModel VoltageCurrentAngleL1Show = new DBDataModel();
    public DBDataModel VoltageCurrentAngleL2 = new DBDataModel();
    public DBDataModel VoltageCurrentAngleL2Show = new DBDataModel();
    public DBDataModel VoltageCurrentAngleL3 = new DBDataModel();
    public DBDataModel VoltageCurrentAngleL3Show = new DBDataModel();
    public DBDataModel VoltageLN = new DBDataModel();
    public String VoltagePhaseSequence = PdfObject.NOTHING;
    public DBDataModel VoltageValueL1 = new DBDataModel();
    public DBDataModel VoltageValueL1L2 = new DBDataModel();
    public DBDataModel VoltageValueL1Show = new DBDataModel();
    public DBDataModel VoltageValueL1Vector = new DBDataModel();
    public DBDataModel VoltageValueL2 = new DBDataModel();
    public DBDataModel VoltageValueL2L3 = new DBDataModel();
    public DBDataModel VoltageValueL2Show = new DBDataModel();
    public DBDataModel VoltageValueL2Vector = new DBDataModel();
    public DBDataModel VoltageValueL3 = new DBDataModel();
    public DBDataModel VoltageValueL3L1 = new DBDataModel();
    public DBDataModel VoltageValueL3Show = new DBDataModel();
    public DBDataModel VoltageValueL3Vector = new DBDataModel();

    public BasicMeasurement() {
    }

    public BasicMeasurement(MeterBaseDevice Device) {
        parseData(Device);
    }

    public void parseData(MeterBaseDevice Device) {
        this.Wiring = Device.getWiringValue();
        parseRangeData(Device);
        ExplainedModel[] TmpValues = Device.getLNVoltageValue();
        this.VoltageValueL1 = parse(TmpValues[0]);
        this.VoltageValueL2 = parse(TmpValues[1]);
        this.VoltageValueL3 = parse(TmpValues[2]);
        ExplainedModel[] TmpValues2 = Device.getLLVoltageValue();
        this.VoltageValueL1L2 = parse(TmpValues2[0]);
        this.VoltageValueL2L3 = parse(TmpValues2[1]);
        this.VoltageValueL3L1 = parse(TmpValues2[2]);
        ExplainedModel[] TmpValues3 = Device.getCurrentValue();
        this.CurrentValueL1 = parse(TmpValues3[0]);
        this.CurrentValueL2 = parse(TmpValues3[1]);
        this.CurrentValueL3 = parse(TmpValues3[2]);
        ExplainedModel[] TmpValues4 = Device.getLNVoltageCurrentAngleValue();
        this.VoltageCurrentAngleL1 = parse(TmpValues4[0]);
        this.VoltageCurrentAngleL2 = parse(TmpValues4[1]);
        this.VoltageCurrentAngleL3 = parse(TmpValues4[2]);
        ExplainedModel[] TmpValues5 = Device.getLLVoltageCurrentAngleValue();
        this.AngleU12I1 = parse(TmpValues5[0]);
        this.AngleU23I2 = parse(TmpValues5[1]);
        this.AngleU31I3 = parse(TmpValues5[2]);
        ExplainedModel[] TmpValues6 = Device.getPValue();
        this.ActivePowerL1 = parse(TmpValues6[0]);
        this.ActivePowerL2 = parse(TmpValues6[1]);
        this.ActivePowerL3 = parse(TmpValues6[2]);
        this.ActivePowerTotal = parse(TmpValues6[3]);
        ExplainedModel[] TmpValues7 = Device.getQValue();
        this.ReactivePowerL1 = parse(TmpValues7[0]);
        this.ReactivePowerL2 = parse(TmpValues7[1]);
        this.ReactivePowerL3 = parse(TmpValues7[2]);
        this.ReactivePowerTotal = parse(TmpValues7[3]);
        ExplainedModel[] TmpValues8 = Device.getSValue();
        this.ApparentPowerL1 = parse(TmpValues8[0]);
        this.ApparentPowerL2 = parse(TmpValues8[1]);
        this.ApparentPowerL3 = parse(TmpValues8[2]);
        this.ApparentPowerTotal = parse(TmpValues8[3]);
        ExplainedModel[] TmpValues9 = Device.getCosValue();
        this.PowerFactorL1 = parse(TmpValues9[0]);
        this.PowerFactorL2 = parse(TmpValues9[1]);
        this.PowerFactorL3 = parse(TmpValues9[2]);
        this.PowerFactorTotal = parse(TmpValues9[3]);
        this.Frequency = parse(Device.getFrequencyValue()[0]);
        ExplainedModel[] TmpValues10 = Device.getTHDValue();
        this.THDU1 = parse(TmpValues10[0]);
        this.THDU2 = parse(TmpValues10[1]);
        this.THDU3 = parse(TmpValues10[2]);
        this.THDI1 = parse(TmpValues10[3]);
        this.THDI2 = parse(TmpValues10[4]);
        this.THDI3 = parse(TmpValues10[5]);
        ExplainedModel[] TmpValues11 = Device.getVoltageCrestFactorValue();
        this.CFU1 = parse(TmpValues11[0]);
        this.CFU2 = parse(TmpValues11[1]);
        this.CFU3 = parse(TmpValues11[2]);
        ExplainedModel[] TmpValues12 = Device.getCurrentCrestFactorValue();
        this.CFI1 = parse(TmpValues12[0]);
        this.CFI2 = parse(TmpValues12[1]);
        this.CFI3 = parse(TmpValues12[2]);
        ExplainedModel[] TmpValues13 = Device.getNeutralValue();
        this.VoltageLN = parse(TmpValues13[0]);
        this.CurrentLN = parse(TmpValues13[1]);
        ExplainedModel[] TmpValues14 = Device.getLNVoltageAngleValue();
        this.VoltageAngleL1 = parse(TmpValues14[0]);
        this.VoltageAngleL2 = parse(TmpValues14[1]);
        this.VoltageAngleL3 = parse(TmpValues14[2]);
        ExplainedModel[] TmpValues15 = Device.getCurrentAngleValue();
        this.CurrentAngleL1 = parse(TmpValues15[0]);
        this.CurrentAngleL2 = parse(TmpValues15[1]);
        this.CurrentAngleL3 = parse(TmpValues15[2]);
        ExplainedModel[] TmpValues16 = Device.getLLVoltageAngleValue();
        this.VoltageAngleL1L2 = parse(TmpValues16[0]);
        this.VoltageAngleL2L3 = parse(TmpValues16[1]);
        if (1 != this.Wiring) {
            this.VoltageAngleL3L1 = parse(TmpValues16[2]);
        } else {
            this.VoltageAngleL3L1 = parse(TmpValues16[3]);
        }
        if (this.Wiring > 0) {
            this.VoltageAngleL2L3 = new DBDataModel();
            this.ActivePowerL2 = new DBDataModel();
            this.ReactivePowerL2 = new DBDataModel();
            this.ApparentPowerL2 = new DBDataModel();
            this.PowerFactorL2 = new DBDataModel();
            this.AngleU23I2 = new DBDataModel();
            this.VoltageValueL2L3 = new DBDataModel();
            this.VoltageAngleL2L3 = new DBDataModel();
            this.CurrentValueL2 = new DBDataModel();
            this.CurrentAngleL2 = new DBDataModel();
            this.THDU2 = new DBDataModel();
            this.THDI2 = new DBDataModel();
            this.CFU2 = new DBDataModel();
            this.CFI2 = new DBDataModel();
        }
        if (1 == this.Wiring) {
            this.VoltageValueL3L1 = parse(Device.getLLVoltageValue()[1]);
            this.AngleU31I3 = parse(Device.getLLVoltageCurrentAngleValue()[3]);
            this.PowerFactorL1 = new DBDataModel();
            this.PowerFactorL3 = new DBDataModel();
            this.ApparentPowerL1 = new DBDataModel();
            this.ApparentPowerL3 = new DBDataModel();
        } else if (2 == this.Wiring) {
            this.VoltageValueL2 = new DBDataModel();
            this.VoltageValueL3 = new DBDataModel();
            this.VoltageAngleL2 = new DBDataModel();
            this.VoltageAngleL3 = new DBDataModel();
            this.VoltageValueL3L1 = new DBDataModel();
            this.VoltageValueL2L3 = new DBDataModel();
            this.VoltageAngleL2L3 = new DBDataModel();
            this.VoltageCurrentAngleL2 = new DBDataModel();
            this.VoltageCurrentAngleL3 = new DBDataModel();
            this.AngleU23I2 = new DBDataModel();
            this.VoltageAngleL3L1 = new DBDataModel();
            this.AngleU31I3 = new DBDataModel();
            this.CurrentValueL3 = new DBDataModel();
            this.CurrentAngleL3 = new DBDataModel();
            this.THDU3 = new DBDataModel();
            this.THDI3 = new DBDataModel();
            this.CFU3 = new DBDataModel();
            this.CFI3 = new DBDataModel();
            this.PowerFactorL3 = new DBDataModel();
            this.ApparentPowerL3 = new DBDataModel();
            this.ReactivePowerL3 = new DBDataModel();
            this.ActivePowerL3 = new DBDataModel();
        }
        if (1 == this.Wiring) {
            this.VoltageValueL1Show = this.VoltageValueL1L2;
            this.VoltageValueL2Show = this.VoltageValueL2L3;
            this.VoltageValueL3Show = this.VoltageValueL3L1;
            this.VoltageAngleL1Show = this.VoltageAngleL1L2;
            this.VoltageAngleL2Show = this.VoltageAngleL2L3;
            this.VoltageAngleL3Show = this.VoltageAngleL3L1;
            this.VoltageCurrentAngleL1Show = this.AngleU12I1;
            this.VoltageCurrentAngleL2Show = this.AngleU23I2;
            this.VoltageCurrentAngleL3Show = this.AngleU31I3;
        } else {
            this.VoltageValueL1Show = this.VoltageValueL1;
            this.VoltageValueL2Show = this.VoltageValueL2;
            this.VoltageValueL3Show = this.VoltageValueL3;
            this.VoltageAngleL1Show = this.VoltageAngleL1;
            this.VoltageAngleL2Show = this.VoltageAngleL2;
            this.VoltageAngleL3Show = this.VoltageAngleL3;
            this.VoltageCurrentAngleL1Show = this.VoltageCurrentAngleL1;
            this.VoltageCurrentAngleL2Show = this.VoltageCurrentAngleL2;
            this.VoltageCurrentAngleL3Show = this.VoltageCurrentAngleL3;
        }
        if (1 == this.Wiring && 1 == Device.getVectorType()) {
            this.VoltageValueL1Vector = this.VoltageValueL1L2;
            this.VoltageValueL2Vector = this.VoltageValueL2L3;
            this.VoltageValueL3Vector = this.VoltageValueL3L1;
            this.VoltageAngleL1Vector = this.VoltageAngleL1L2;
            this.VoltageAngleL2Vector = this.VoltageAngleL2L3;
            this.VoltageAngleL3Vector = this.VoltageAngleL3L1;
        } else {
            this.VoltageValueL1Vector = this.VoltageValueL1;
            this.VoltageValueL2Vector = this.VoltageValueL2;
            this.VoltageValueL3Vector = this.VoltageValueL3;
            this.VoltageAngleL1Vector = this.VoltageAngleL1;
            this.VoltageAngleL2Vector = this.VoltageAngleL2;
            this.VoltageAngleL3Vector = this.VoltageAngleL3;
        }
        this.VoltagePhaseSequence = parsePhaseSequence(Device.getVoltagePhaseSequenceValue());
        this.CurrentPhaseSequence = parsePhaseSequence(Device.getCurrentPhaseSequenceValue());
    }

    private String parsePhaseSequence(int[] SequenceFlag) {
        return OtherUtils.parsePhaseSequence(SequenceFlag[0], SequenceFlag[1], SequenceFlag[2]);
    }

    public boolean isEmpty() {
        return false;
    }
}
