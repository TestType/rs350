package com.clou.rs350.ui.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.clou.rs350.R;
import com.clou.rs350.db.dao.MeterTypeDao;
import com.clou.rs350.db.model.MeterType;
import com.clou.rs350.ui.view.infoview.MeterTypeView;
import com.clou.rs350.utils.OtherUtils;

public class MeterTypeInfoDialog extends Dialog {
    Button btn_Close = null;
    Button btn_Modify = null;
    private View.OnClickListener cancleClick = null;
    TextView contentView = null;
    int[] m_Constants = new int[3];
    Context m_Context = null;
    MeterTypeDao m_Dao;
    MeterTypeView m_InfoView;
    boolean m_IsModefy = false;
    boolean m_IsNew = false;
    MeterType m_MeterType;
    View m_MeterViewLayout;
    private View.OnClickListener okClick = null;

    public MeterTypeInfoDialog(Context context, MeterType meterInfo, boolean isNew) {
        super(context, R.style.dialog);
        requestWindowFeature(1);
        setCanceledOnTouchOutside(false);
        this.m_Context = context;
        this.m_Dao = new MeterTypeDao(this.m_Context);
        this.m_MeterType = meterInfo;
        this.m_IsNew = isNew;
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_meter_type);
        this.m_MeterViewLayout = findViewById(R.id.layout_meter_type);
        this.m_InfoView = new MeterTypeView(this.m_Context, this.m_MeterViewLayout, this.m_MeterType, 2);
        this.btn_Close = (Button) findViewById(R.id.btn_close);
        this.btn_Close.setOnClickListener(new View.OnClickListener() {
            /* class com.clou.rs350.ui.dialog.MeterTypeInfoDialog.AnonymousClass1 */

            public void onClick(View v) {
                if (MeterTypeInfoDialog.this.m_IsNew) {
                    if (MeterTypeInfoDialog.this.cancleClick != null) {
                        MeterTypeInfoDialog.this.cancleClick.onClick(v);
                    }
                    MeterTypeInfoDialog.this.dismiss();
                } else if (MeterTypeInfoDialog.this.m_IsModefy) {
                    MeterTypeInfoDialog.this.StopModify();
                    MeterTypeInfoDialog.this.m_InfoView.refreshData(MeterTypeInfoDialog.this.m_MeterType);
                } else {
                    MeterTypeInfoDialog.this.dismiss();
                }
            }
        });
        this.btn_Modify = (Button) findViewById(R.id.btn_modify);
        this.btn_Modify.setOnClickListener(new View.OnClickListener() {
            /* class com.clou.rs350.ui.dialog.MeterTypeInfoDialog.AnonymousClass2 */

            public void onClick(View v) {
                if (MeterTypeInfoDialog.this.m_IsNew) {
                    int result = MeterTypeInfoDialog.this.Save();
                    if (result == 0) {
                        if (MeterTypeInfoDialog.this.okClick != null) {
                            v.setTag(MeterTypeInfoDialog.this.m_MeterType);
                            MeterTypeInfoDialog.this.okClick.onClick(v);
                        }
                        MeterTypeInfoDialog.this.dismiss();
                    } else if (1 == result) {
                        new HintDialog(MeterTypeInfoDialog.this.m_Context, (int) R.string.text_meter_type_empty).show();
                    }
                    if (2 == result) {
                        new HintDialog(MeterTypeInfoDialog.this.m_Context, (int) R.string.text_meter_type_exists).show();
                    }
                } else if (MeterTypeInfoDialog.this.m_IsModefy) {
                    MeterTypeInfoDialog.this.Save();
                } else {
                    MeterTypeInfoDialog.this.Modify();
                }
            }
        });
        if (this.m_IsNew) {
            Modify();
            this.btn_Modify.setText(R.string.text_ok);
        }
    }

    private void parseData() {
        this.m_MeterType = this.m_InfoView.getInfo();
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private void Modify() {
        this.m_InfoView.setFunction(3);
        this.btn_Modify.setText(R.string.text_save);
        this.btn_Close.setText(R.string.text_cancel);
        this.m_IsModefy = true;
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private int Save() {
        if (OtherUtils.isEmpty(this.m_InfoView.getKey())) {
            return 1;
        }
        if (!this.m_InfoView.hasSetParams()) {
            return 3;
        }
        parseData();
        if (!this.m_MeterType.isNew) {
            this.m_Dao.updateObjectByName(this.m_MeterType.Type, this.m_MeterType);
        } else if (this.m_Dao.queryObjectByType(this.m_MeterType.Type) != null) {
            return 2;
        } else {
            this.m_Dao.insertObject(this.m_MeterType);
            this.m_MeterType.isNew = false;
        }
        StopModify();
        return 0;
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private void StopModify() {
        this.m_InfoView.setFunction(0);
        this.btn_Modify.setText(R.string.text_modify);
        this.btn_Close.setText(R.string.text_close);
        this.m_IsModefy = false;
    }

    public void setOnClick(View.OnClickListener okClick2) {
        this.okClick = okClick2;
    }

    public void setOnClick(View.OnClickListener okClick2, View.OnClickListener cancleClick2) {
        this.okClick = okClick2;
        this.cancleClick = cancleClick2;
    }
}
