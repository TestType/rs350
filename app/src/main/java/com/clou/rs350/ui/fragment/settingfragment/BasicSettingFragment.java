package com.clou.rs350.ui.fragment.settingfragment;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Process;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.SeekBar;
import android.widget.TextView;

import com.clou.rs350.CLApplication;
import com.clou.rs350.R;
import com.clou.rs350.handler.HandlerSwitchView;
import com.clou.rs350.manager.MessageManager;
import com.clou.rs350.model.ClouData;
import com.clou.rs350.model.SettingBasic;
import com.clou.rs350.model.SettingFunction;
import com.clou.rs350.ui.dialog.HintDialog;
import com.clou.rs350.ui.dialog.NormalDialog;
import com.clou.rs350.ui.dialog.NumericDialog;
import com.clou.rs350.ui.fragment.BaseFragment;
import com.clou.rs350.ui.popwindow.ListPopWindow;
import com.clou.rs350.utils.FileUtils;
import com.itextpdf.text.pdf.PdfObject;
import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.view.annotation.ViewInject;

import java.util.ArrayList;
import java.util.List;

public class BasicSettingFragment extends BaseFragment implements View.OnClickListener, HandlerSwitchView.IOnSelectCallback, SeekBar.OnSeekBarChangeListener {
    @ViewInject(R.id.chk_auto)
    private RadioButton AutoDiscernConnectType;
    @ViewInject(R.id.setting_blue_edittext)
    private TextView BValueEt;
    @ViewInject(R.id.color_i1)
    private TextView ColorI1;
    @ViewInject(R.id.color_i2)
    private TextView ColorI2;
    @ViewInject(R.id.color_i3)
    private TextView ColorI3;
    @ViewInject(R.id.color_u1)
    private TextView ColorU1;
    @ViewInject(R.id.color_u2)
    private TextView ColorU2;
    @ViewInject(R.id.color_u3)
    private TextView ColorU3;
    @ViewInject(R.id.setting_green_edittext)
    private TextView GValueEt;
    @ViewInject(R.id.chk_manual)
    private RadioButton ManualDiscernConnectType;
    @ViewInject(R.id.setting_red_edittext)
    private TextView RValueEt;
    @ViewInject(R.id.seekbar_b)
    private SeekBar bSeekBar;
    @ViewInject(R.id.chk_harmonic_definition0)
    private RadioButton chk_Harmonic_Definition0;
    @ViewInject(R.id.chk_harmonic_definition1)
    private RadioButton chk_Harmonic_Definition1;
    @ViewInject(R.id.ss_ss_type_0)
    private RadioButton chk_S_Type0;
    @ViewInject(R.id.ss_ss_type_1)
    private RadioButton chk_S_Type1;
    @ViewInject(R.id.chk_vector0)
    private RadioButton chk_Vector_Definition0;
    @ViewInject(R.id.chk_vector1)
    private RadioButton chk_Vector_Definition1;
    @ViewInject(R.id.chk_vector_type0)
    private RadioButton chk_Vector_Type0;
    @ViewInject(R.id.chk_vector_type1)
    private RadioButton chk_Vector_Type1;
    private HandlerSwitchView colorSwitchView;
    @ViewInject(R.id.seekbar_g)
    private SeekBar gSeekBar;
    @ViewInject(R.id.layout_general)
    private LinearLayout layout_General;
    @ViewInject(R.id.layout_phase_color)
    private LinearLayout layout_Phase_Color;
    private TextView[] m_ColorViews;
    private int m_PhaseIndex;
    private int[] m_PhaseNameId = {R.string.text_u1, R.string.text_u2, R.string.text_u3, R.string.text_i1, R.string.text_i2, R.string.text_i3};
    @ViewInject(R.id.ss_basic_time_progressbar)
    private TextView progressbar_time;
    @ViewInject(R.id.seekbar_r)
    private SeekBar rSeekBar;
    @ViewInject(R.id.txt_selected)
    private TextView selectEt;
    private SettingBasic settingBasic;
    private HandlerSwitchView switchView;
    @ViewInject(R.id.color_u1)
    private TextView txtColorU1;
    @ViewInject(R.id.ss_basic_angle_view)
    private TextView txt_AngleImage;
    @ViewInject(R.id.ss_basic_device_model)
    private TextView txt_DeveiceModel;
    @ViewInject(R.id.ss_basic_double_click_delay)
    private TextView txt_DoubleClickDelay;
    @ViewInject(R.id.ss_error_digit)
    private TextView txt_ErrorDigit;
    @ViewInject(R.id.ss_export_template)
    private TextView txt_ExportTemplate;
    @ViewInject(R.id.ss_language)
    private TextView txt_Language;
    @ViewInject(R.id.ss_basic_refresh_time)
    private TextView txt_RefreshTime;
    @ViewInject(R.id.ss_basic_update_server)
    private TextView txt_UpdateServer;

    public void onClick(View v) {
        NumericDialog numDialog = new NumericDialog(this.m_Context);
        switch (v.getId()) {
            case R.id.ss_basic_angle_view:
                if (this.settingBasic.AngleDefinition == 0) {
                    this.txt_AngleImage.setBackgroundResource(R.drawable.btn_angeldefinition1);
                    this.settingBasic.AngleDefinition = 1;
                    return;
                }
                this.txt_AngleImage.setBackgroundResource(R.drawable.btn_angeldefinition0);
                this.settingBasic.AngleDefinition = 0;
                return;
            case R.id.ss_basic_refresh_time:
                numDialog.setMin(1.0d);
                numDialog.setMinus(false);
                numDialog.setDot(false);
                numDialog.showMyDialog((TextView) v);
                return;
            case R.id.ss_ss_type_0:
                this.settingBasic.SSType = 0;
                return;
            case R.id.ss_ss_type_1:
                this.settingBasic.SSType = 1;
                return;
            case R.id.ss_basic_time_progressbar:
                numDialog.setMin(1.0d);
                numDialog.setMinus(false);
                numDialog.setDot(false);
                numDialog.showMyDialog((TextView) v);
                return;
            case R.id.ss_error_digit:
                numDialog.setMin(0.0d);
                numDialog.setMax(3.0d);
                numDialog.setMinus(false);
                numDialog.setDot(false);
                numDialog.showMyDialog((TextView) v);
                return;
            case R.id.ss_export_template:
                List<String> tmpString = FileUtils.getFileDir(CLApplication.app.getTemplateXmlPath());
                ListPopWindow.showListPopwindow(this.m_Context, (String[]) tmpString.toArray(new String[tmpString.size()]), v, null);
                return;
            case R.id.ss_basic_double_click_delay:
                numDialog.setMin(100.0d);
                numDialog.setMax(2000.0d);
                numDialog.setMinus(false);
                numDialog.setDot(false);
                numDialog.showMyDialog((TextView) v);
                return;
            case R.id.ss_basic_device_model:
                String[] arrays = getResources().getStringArray(R.array.device_model);
                final int oldModel = Integer.parseInt(this.txt_DeveiceModel.getTag().toString());
                ListPopWindow.showListPopwindow(this.m_Context, arrays, v, new View.OnClickListener() {
                    /* class com.clou.rs350.ui.fragment.settingfragment.BasicSettingFragment.AnonymousClass1 */

                    public void onClick(View v) {
                        int model = Integer.parseInt(v.getTag().toString());
                        if (oldModel != model) {
                            BasicSettingFragment.this.txt_DeveiceModel.setTag(Integer.valueOf(model));
                            BasicSettingFragment.this.txt_DeveiceModel.setText(BasicSettingFragment.this.getDeviceModelText(model));
                        }
                    }
                });
                return;
            case R.id.ss_language:
                String[] arrlanguage = getResources().getStringArray(R.array.language);
                final int oldLanguage = Integer.parseInt(this.txt_Language.getTag().toString());
                ListPopWindow.showListPopwindow(this.m_Context, arrlanguage, v, new View.OnClickListener() {
                    /* class com.clou.rs350.ui.fragment.settingfragment.BasicSettingFragment.AnonymousClass2 */

                    public void onClick(View v) {
                        int model = Integer.parseInt(v.getTag().toString());
                        if (oldLanguage != model) {
                            BasicSettingFragment.this.settingBasic.Language = model;
                            BasicSettingFragment.this.settingBasic.ResetFlag = true;
                            BasicSettingFragment.this.settingBasic.saveSetting();
                            HintDialog tmpDialog = new HintDialog(BasicSettingFragment.this.m_Context);
                            tmpDialog.setOnClick(new View.OnClickListener() {
                                /* class com.clou.rs350.ui.fragment.settingfragment.BasicSettingFragment.AnonymousClass2.AnonymousClass1 */

                                public void onClick(View v) {
                                    BasicSettingFragment.this.restartApplication();
                                }
                            });
                            tmpDialog.show();
                            tmpDialog.setTitle(R.string.text_restart_app);
                            if (2 == model) {
                                NormalDialog tmpNormalDialog = new NormalDialog(BasicSettingFragment.this.m_Context);
                                tmpNormalDialog.setOnClick(new View.OnClickListener() {
                                    /* class com.clou.rs350.ui.fragment.settingfragment.BasicSettingFragment.AnonymousClass2.AnonymousClass2 */

                                    public void onClick(View v) {
                                        SettingFunction tmpFunction = ClouData.getInstance().getSettingFunction();
                                        tmpFunction.CNVersion = true;
                                        tmpFunction.saveSetting();
                                    }
                                }, null);
                                tmpNormalDialog.show();
                                tmpNormalDialog.setTitle(R.string.text_set_chinese_function);
                                return;
                            }
                            SettingFunction tmpFunction = ClouData.getInstance().getSettingFunction();
                            tmpFunction.CNVersion = false;
                            tmpFunction.saveSetting();
                        }
                    }
                });
                return;
            case R.id.btn_default:
                int[] rgb = getRGB(this.m_Context.getResources().getColor(new int[]{R.color.color_l1, R.color.color_l2, R.color.color_l3, R.color.color_l1, R.color.color_l2, R.color.color_l3}[this.m_PhaseIndex]));
                setProgressCount(rgb[0], rgb[1], rgb[2]);
                return;
            case R.id.setting_red_edittext:
            case R.id.setting_green_edittext:
            case R.id.setting_blue_edittext:
                numDialog.setMin(0.0d);
                numDialog.setMax(255.0d);
                numDialog.showMyDialog((TextView) v);
                numDialog.setOnDoneClick(new View.OnClickListener() {
                    /* class com.clou.rs350.ui.fragment.settingfragment.BasicSettingFragment.AnonymousClass3 */

                    public void onClick(View v) {
                        BasicSettingFragment.this.setProgressCount(Integer.parseInt(BasicSettingFragment.this.RValueEt.getText().toString().trim()), Integer.parseInt(BasicSettingFragment.this.GValueEt.getText().toString().trim()), Integer.parseInt(BasicSettingFragment.this.BValueEt.getText().toString().trim()));
                    }
                });
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private void restartApplication() {
        Intent intent = this.m_Context.getPackageManager().getLaunchIntentForPackage(this.m_Context.getPackageName());
        intent.setFlags(268468224);
        startActivity(intent);
        Process.killProcess(Process.myPid());
        System.exit(0);
    }

    public String getDeviceModelText(int deviceModel) {
        return getResources().getStringArray(R.array.device_model)[deviceModel];
    }

    @Override // android.support.v4.app.Fragment, com.clou.rs350.ui.fragment.BaseFragment
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initView();
        initData();
        initColorData();
    }

    private void initView() {
        this.txt_RefreshTime.setOnClickListener(this);
        this.txt_ErrorDigit.setOnClickListener(this);
        this.txt_DoubleClickDelay.setOnClickListener(this);
        this.chk_S_Type0.setOnClickListener(this);
        this.chk_S_Type1.setOnClickListener(this);
        this.txt_AngleImage.setOnClickListener(this);
        this.txt_DeveiceModel.setOnClickListener(this);
        this.txt_ExportTemplate.setOnClickListener(this);
        this.txt_Language.setOnClickListener(this);
        this.progressbar_time.setOnClickListener(this);
        List<View> tabViews = new ArrayList<>();
        tabViews.add(findViewById(R.id.btn_general));
        tabViews.add(findViewById(R.id.btn_phase_color));
        this.switchView = new HandlerSwitchView(this.m_Context);
        this.switchView.setTabViews(tabViews);
        this.switchView.boundClick();
        this.switchView.setOnTabChangedCallback(this);
        this.switchView.selectView(0);
        List<View> colorViews = new ArrayList<>();
        colorViews.add(findViewById(R.id.color_u1));
        colorViews.add(findViewById(R.id.color_u2));
        colorViews.add(findViewById(R.id.color_u3));
        colorViews.add(findViewById(R.id.color_i1));
        colorViews.add(findViewById(R.id.color_i2));
        colorViews.add(findViewById(R.id.color_i3));
        this.colorSwitchView = new HandlerSwitchView(this.m_Context);
        this.colorSwitchView.setChangeBackground(false);
        this.colorSwitchView.setTabViews(colorViews);
        this.colorSwitchView.boundClick();
        this.colorSwitchView.setOnTabChangedCallback(new HandlerSwitchView.IOnSelectCallback() {
            /* class com.clou.rs350.ui.fragment.settingfragment.BasicSettingFragment.AnonymousClass4 */

            @Override // com.clou.rs350.handler.HandlerSwitchView.IOnSelectCallback
            public void onSelectedChanged(int index) {
                BasicSettingFragment.this.m_PhaseIndex = index;
                BasicSettingFragment.this.selectEt.setText(BasicSettingFragment.this.m_PhaseNameId[BasicSettingFragment.this.m_PhaseIndex]);
                int[] rgb = BasicSettingFragment.this.getRGB(BasicSettingFragment.this.settingBasic.PhaseColors[index]);
                BasicSettingFragment.this.setProgressCount(rgb[0], rgb[1], rgb[2]);
            }

            @Override // com.clou.rs350.handler.HandlerSwitchView.IOnSelectCallback
            public void onClickView(View view) {
            }
        });
        this.colorSwitchView.selectView(0);
        this.m_ColorViews = new TextView[]{this.ColorU1, this.ColorU2, this.ColorU3, this.ColorI1, this.ColorI2, this.ColorI3};
    }

    @Override // android.support.v4.app.Fragment
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        this.fragmentView = inflater.inflate(R.layout.fragment_basic_setting, (ViewGroup) null);
        ViewUtils.inject(this, this.fragmentView);
        return this.fragmentView;
    }

    @Override // android.support.v4.app.Fragment, com.clou.rs350.ui.fragment.BaseFragment
    public void onPause() {
        super.onPause();
    }

    /* access modifiers changed from: protected */
    @Override // com.clou.rs350.ui.fragment.BaseFragment
    public void copyData() {
        int i;
        int i2;
        int i3;
        if (this.txt_RefreshTime != null) {
            this.settingBasic.RefreshTime = ((int) Double.valueOf(this.txt_RefreshTime.getText().toString().equals(PdfObject.NOTHING) ? "1" : this.txt_RefreshTime.getText().toString()).doubleValue()) * 1000;
            if (this.settingBasic.RefreshTime < 1000) {
                this.settingBasic.RefreshTime = 1000;
            }
            this.settingBasic.ProgressBarTime = (int) Double.valueOf(this.progressbar_time.getText().toString().equals(PdfObject.NOTHING) ? "1" : this.progressbar_time.getText().toString()).doubleValue();
            if (this.settingBasic.ProgressBarTime < 1) {
                this.settingBasic.ProgressBarTime = 1;
            }
            this.settingBasic.ErrorDigit = (int) Double.valueOf(this.txt_ErrorDigit.getText().toString().equals(PdfObject.NOTHING) ? "3" : this.txt_ErrorDigit.getText().toString()).doubleValue();
            this.settingBasic.DoubleClickDelay = (int) Double.valueOf(this.txt_DoubleClickDelay.getText().toString().equals(PdfObject.NOTHING) ? "200" : this.txt_DoubleClickDelay.getText().toString()).doubleValue();
            SettingBasic settingBasic2 = this.settingBasic;
            if (this.chk_Vector_Definition0.isChecked()) {
                i = 0;
            } else {
                i = 1;
            }
            settingBasic2.VectorDefinition = i;
            SettingBasic settingBasic3 = this.settingBasic;
            if (this.chk_Vector_Type0.isChecked()) {
                i2 = 0;
            } else {
                i2 = 1;
            }
            settingBasic3.VectorType = i2;
            int newValue = Integer.parseInt(this.txt_DeveiceModel.getTag().toString());
            if (newValue != this.settingBasic.DeviceModel) {
                this.settingBasic.DeviceModel = newValue;
                this.messageManager.freshDevice();
            }
            this.settingBasic.ExportTemplate = this.txt_ExportTemplate.getText().toString();
            this.settingBasic.UpdateServer = this.txt_UpdateServer.getText().toString();
            SettingBasic settingBasic4 = this.settingBasic;
            if (this.chk_Harmonic_Definition0.isChecked()) {
                i3 = 0;
            } else {
                i3 = 1;
            }
            settingBasic4.HarmonicDefinition = i3;
            this.settingBasic.AutoDiscernConnect = this.AutoDiscernConnectType.isChecked();
            this.messageManager.setDeviceAngleDefinition(true);
            this.messageManager.SetCalculateType(this.settingBasic.SSType, 0);
            this.settingBasic.saveSetting();
        }
    }

    private void initData() {
        boolean z = false;
        this.messageManager = MessageManager.getInstance(this.m_Context);
        this.settingBasic = ClouData.getInstance().getSettingBasic();
        this.txt_RefreshTime.setText(String.valueOf(this.settingBasic.RefreshTime / 1000));
        this.txt_DoubleClickDelay.setText(String.valueOf(this.settingBasic.DoubleClickDelay));
        this.txt_ErrorDigit.setText(String.valueOf(this.settingBasic.ErrorDigit));
        this.progressbar_time.setText(String.valueOf(this.settingBasic.ProgressBarTime));
        boolean checked = this.settingBasic.SSType == 0;
        this.chk_S_Type0.setChecked(checked);
        this.chk_S_Type1.setChecked(!checked);
        boolean checked2 = this.settingBasic.HarmonicDefinition == 0;
        this.chk_Harmonic_Definition0.setChecked(checked2);
        this.chk_Harmonic_Definition1.setChecked(!checked2);
        boolean checked3 = this.settingBasic.VectorDefinition == 0;
        this.chk_Vector_Definition0.setChecked(checked3);
        this.chk_Vector_Definition1.setChecked(!checked3);
        boolean checked4 = this.settingBasic.VectorType == 0;
        this.chk_Vector_Type0.setChecked(checked4);
        this.chk_Vector_Type1.setChecked(!checked4);
        this.txt_AngleImage.setBackgroundResource(this.settingBasic.AngleDefinition == 0 ? R.drawable.btn_angeldefinition0 : R.drawable.btn_angeldefinition1);
        this.txt_DeveiceModel.setTag(Integer.valueOf(this.settingBasic.DeviceModel));
        this.txt_DeveiceModel.setText(this.settingBasic.getDeviceModelText(this.m_Context));
        this.txt_ExportTemplate.setText(this.settingBasic.ExportTemplate);
        this.txt_UpdateServer.setText(this.settingBasic.UpdateServer);
        this.txt_Language.setTag(Integer.valueOf(this.settingBasic.Language));
        this.txt_Language.setText(this.settingBasic.getArrayText(this.m_Context, R.array.language, this.settingBasic.Language));
        this.AutoDiscernConnectType.setChecked(this.settingBasic.AutoDiscernConnect);
        RadioButton radioButton = this.ManualDiscernConnectType;
        if (!this.settingBasic.AutoDiscernConnect) {
            z = true;
        }
        radioButton.setChecked(z);
        for (int i = 0; i < this.m_ColorViews.length; i++) {
            this.m_ColorViews[i].setBackgroundColor(this.settingBasic.PhaseColors[i]);
        }
    }

    private void initColorData() {
        this.rSeekBar.setMax(255);
        this.rSeekBar.setOnSeekBarChangeListener(this);
        this.gSeekBar.setMax(255);
        this.gSeekBar.setOnSeekBarChangeListener(this);
        this.bSeekBar.setMax(255);
        this.bSeekBar.setOnSeekBarChangeListener(this);
        this.RValueEt.setOnClickListener(this);
        this.GValueEt.setOnClickListener(this);
        this.BValueEt.setOnClickListener(this);
        int[] rgb = getRGB(this.settingBasic.PhaseColors[0]);
        setProgressCount(rgb[0], rgb[1], rgb[2]);
        this.selectEt.setText(R.string.text_u1);
        findViewById(R.id.btn_default).setOnClickListener(this);
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private int[] getRGB(int color) {
        return new int[]{(color >> 16) & 255, (color >> 8) & 255, (color >> 0) & 255};
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private void setProgressCount(int r, int g, int b) {
        this.RValueEt.setText(new StringBuilder().append(r).toString());
        this.GValueEt.setText(new StringBuilder().append(g).toString());
        this.BValueEt.setText(new StringBuilder().append(b).toString());
        this.rSeekBar.setProgress(r);
        this.gSeekBar.setProgress(g);
        this.bSeekBar.setProgress(b);
        int color = Color.rgb(r, g, b);
        this.m_ColorViews[this.m_PhaseIndex].setBackgroundColor(color);
        this.settingBasic.PhaseColors[this.m_PhaseIndex] = color;
    }

    @Override // com.clou.rs350.handler.HandlerSwitchView.IOnSelectCallback
    public void onSelectedChanged(int index) {
    }

    @Override // com.clou.rs350.handler.HandlerSwitchView.IOnSelectCallback
    public void onClickView(View v) {
        switch (v.getId()) {
            case R.id.btn_general:
                this.layout_General.setVisibility(0);
                this.layout_Phase_Color.setVisibility(8);
                return;
            case R.id.btn_phase_color:
                this.layout_General.setVisibility(8);
                this.layout_Phase_Color.setVisibility(0);
                return;
            default:
                return;
        }
    }

    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
        setProgressCount(this.rSeekBar.getProgress(), this.gSeekBar.getProgress(), this.bSeekBar.getProgress());
    }

    public void onStartTrackingTouch(SeekBar seekBar) {
    }

    public void onStopTrackingTouch(SeekBar seekBar) {
    }
}
