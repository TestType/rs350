package com.clou.rs350.ui.fragment.recordfragment;

import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.clou.rs350.CLApplication;
import com.clou.rs350.R;
import com.clou.rs350.callback.ILoadCallback;
import com.clou.rs350.db.model.sequence.CameraData;
import com.clou.rs350.task.MyTask;
import com.clou.rs350.ui.fragment.BaseFragment;

import java.io.File;

public class CameraRecordFragment extends BaseFragment implements View.OnClickListener {
    private String TAG = "Photo";
    private Button btn_Next;
    private Button btn_Previous;
    private ImageView imageView;
    private CameraData m_Data;
    private int m_Index;
    private File m_PhotoFile;
    private TextView txt_Index;
    private TextView txt_Total;

    public CameraRecordFragment(CameraData data) {
        this.m_Data = data;
    }

    @Override // android.support.v4.app.Fragment
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        this.fragmentView = inflater.inflate(R.layout.fragment_record_camera, (ViewGroup) null);
        return this.fragmentView;
    }

    @Override // android.support.v4.app.Fragment, com.clou.rs350.ui.fragment.BaseFragment
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initView();
    }

    private void initView() {
        this.txt_Index = (TextView) findViewById(R.id.txt_index);
        this.txt_Total = (TextView) findViewById(R.id.txt_total);
        this.btn_Previous = (Button) findViewById(R.id.btn_previous);
        this.btn_Previous.setOnClickListener(this);
        this.btn_Next = (Button) findViewById(R.id.btn_next);
        this.btn_Next.setOnClickListener(this);
        this.imageView = (ImageView) findViewById(R.id.iv_show_photo);
    }

    @Override // android.support.v4.app.Fragment, com.clou.rs350.ui.fragment.BaseFragment
    public void onResume() {
        showData();
        super.onResume();
    }

    private void showData() {
        this.txt_Index.setText(new StringBuilder(String.valueOf(this.m_Index + 1)).toString());
        this.txt_Total.setText(new StringBuilder(String.valueOf(this.m_Data.Photos.size())).toString());
        setInitImageView(String.valueOf(CLApplication.app.getImagePath()) + File.separator + this.m_Data.Photos.get(this.m_Index).PhotoPath);
        setButton();
    }

    private void setInitImageView(final String Path) {
        new MyTask(new ILoadCallback() {
            /* class com.clou.rs350.ui.fragment.recordfragment.CameraRecordFragment.AnonymousClass1 */

            @Override // com.clou.rs350.callback.ILoadCallback
            public Object run() {
                CameraRecordFragment.this.m_PhotoFile = new File(Path);
                if (CameraRecordFragment.this.m_PhotoFile.exists()) {
                    return null;
                }
                System.out.println("init image-->1");
                return null;
            }

            @Override // com.clou.rs350.callback.ILoadCallback
            public void callback(Object result) {
                if (CameraRecordFragment.this.m_PhotoFile == null) {
                    CameraRecordFragment.this.imageView.setImageURI(null);
                } else {
                    CameraRecordFragment.this.imageView.setImageURI(Uri.fromFile(CameraRecordFragment.this.m_PhotoFile));
                }
            }
        }).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, new Integer[0]);
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_previous:
                this.m_Index--;
                showData();
                return;
            case R.id.btn_next:
                this.m_Index++;
                showData();
                return;
            default:
                return;
        }
    }

    private void setButton() {
        boolean z;
        boolean z2 = true;
        Button button = this.btn_Previous;
        if (this.m_Index != 0) {
            z = true;
        } else {
            z = false;
        }
        button.setEnabled(z);
        Button button2 = this.btn_Next;
        if (this.m_Index >= this.m_Data.Photos.size() - 1) {
            z2 = false;
        }
        button2.setEnabled(z2);
    }

    @Override // android.support.v4.app.Fragment, com.clou.rs350.ui.fragment.BaseFragment
    public void onPause() {
        super.onPause();
    }
}
