package com.clou.rs350.ui.fragment.recordfragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.clou.rs350.R;
import com.clou.rs350.db.model.SiteData;
import com.clou.rs350.ui.fragment.BaseFragment;
import com.clou.rs350.ui.view.infoview.SiteDataView;

public class SiteDataRecordFragment extends BaseFragment {
    SiteDataView m_InfoView;
    View m_InfoViewLayout;
    SiteData m_SiteData;

    public SiteDataRecordFragment(SiteData siteData) {
        this.m_SiteData = siteData;
    }

    @Override // android.support.v4.app.Fragment
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        this.fragmentView = inflater.inflate(R.layout.fragment_record_site_data, (ViewGroup) null);
        return this.fragmentView;
    }

    @Override // android.support.v4.app.Fragment, com.clou.rs350.ui.fragment.BaseFragment
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initView();
    }

    private void initView() {
        this.m_InfoViewLayout = findViewById(R.id.layout_site_data);
        this.m_InfoView = new SiteDataView(this.m_Context, this.m_InfoViewLayout, this.m_SiteData, 0);
    }

    @Override // android.support.v4.app.Fragment, com.clou.rs350.ui.fragment.BaseFragment
    public void onResume() {
        this.m_InfoView.refreshData(this.m_SiteData);
        super.onResume();
    }

    @Override // android.support.v4.app.Fragment, com.clou.rs350.ui.fragment.BaseFragment
    public void onPause() {
        super.onPause();
    }

    /* access modifiers changed from: protected */
    @Override // com.clou.rs350.ui.fragment.BaseFragment
    public void copyData() {
    }
}
