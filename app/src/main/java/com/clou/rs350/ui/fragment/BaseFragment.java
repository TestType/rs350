package com.clou.rs350.ui.fragment;

import android.app.Activity;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;
import com.clou.rs350.device.MeterBaseDevice;
import com.clou.rs350.manager.MessageManager;
import com.clou.rs350.task.TimerThread;
import com.clou.rs350.ui.dialog.HintDialog;

public class BaseFragment extends Fragment implements MessageManager.IMessageListener {
    public String Title;
    protected View fragmentView;
    protected Activity m_Context;
    protected int m_MustDo = 0;
    private Toast m_Toast;
    protected MessageManager messageManager;

    @Override // android.support.v4.app.Fragment
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRetainInstance(true);
    }

    @Override // android.support.v4.app.Fragment
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        this.m_Context = getActivity();
        this.messageManager = MessageManager.getInstance(this.m_Context);
    }

    @Override // android.support.v4.app.Fragment
    public void onResume() {
        super.onResume();
        if (this.messageManager != null) {
            this.messageManager.addMessageListener(this);
        }
    }

    @Override // android.support.v4.app.Fragment
    public void onPause() {
        super.onPause();
        if (this.messageManager != null) {
            this.messageManager.removeMessageListener(this);
        }
    }

    public void setMustDo(int flag) {
        this.m_MustDo = flag;
    }

    public void refreshData() {
    }

    /* access modifiers changed from: protected */
    public View findViewById(int id) {
        if (this.fragmentView != null) {
            return this.fragmentView.findViewById(id);
        }
        return this.m_Context.findViewById(id);
    }

    /* access modifiers changed from: protected */
    public void showToast(String toast) {
        if (this.m_Toast == null) {
            this.m_Toast = Toast.makeText(this.m_Context, toast, 0);
        }
        this.m_Toast.setText(toast);
        this.m_Toast.show();
    }

    /* access modifiers changed from: protected */
    public void showToast(int toast) {
        if (this.m_Toast == null) {
            this.m_Toast = Toast.makeText(this.m_Context, toast, 0);
        }
        this.m_Toast.setText(toast);
        this.m_Toast.show();
    }

    /* access modifiers changed from: protected */
    public void showHint(int title) {
        HintDialog hintDialog = new HintDialog(this.m_Context);
        hintDialog.show();
        hintDialog.setTitle(title);
    }

    /* access modifiers changed from: protected */
    public boolean isEmpty(TextView tv) {
        if (tv == null) {
            return true;
        }
        return TextUtils.isEmpty(tv.getText().toString().trim());
    }

    /* access modifiers changed from: protected */
    public boolean isEmpty(String text) {
        return TextUtils.isEmpty(text);
    }

    public synchronized void sendMessage(String message, int port, String server) {
    }

    @Override // com.clou.rs350.manager.MessageManager.IMessageListener
    public void onReceiveMessage(MeterBaseDevice Device) {
    }

    @Override // com.clou.rs350.manager.MessageManager.IMessageListener
    public void onReceiveMessage(byte[] buffer) {
    }

    @Override // com.clou.rs350.manager.MessageManager.IMessageListener
    public void onReceiveMessage(int strId) {
    }

    @Override // com.clou.rs350.manager.MessageManager.IMessageListener
    public void onReceiveMessage(String str) {
    }

    /* access modifiers changed from: protected */
    public void killThread(TimerThread thread) {
        if (thread != null && !thread.isStop()) {
            thread.stopTimer();
            thread.interrupt();
        }
    }

    /* access modifiers changed from: protected */
    public void postOnMainThread(Runnable callback) {
        if (this.m_Context != null) {
            this.m_Context.runOnUiThread(callback);
        }
    }

    /* access modifiers changed from: protected */
    public void copyData() {
    }

    public void SaveData() {
        try {
            copyData();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public int isFinish() {
        return 0;
    }
}
