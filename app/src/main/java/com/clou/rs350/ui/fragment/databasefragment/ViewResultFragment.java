package com.clou.rs350.ui.fragment.databasefragment;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.clou.rs350.R;
import com.clou.rs350.db.dao.CustomerInfoDao;
import com.clou.rs350.db.dao.SearchDao;
import com.clou.rs350.db.model.DataBaseRecordItem;
import com.clou.rs350.model.ClouData;
import com.clou.rs350.ui.activity.DataDetailActivity;
import com.clou.rs350.ui.adapter.CustomerSerialNoAdapter;
import com.clou.rs350.ui.adapter.RecordsAdapter;
import com.clou.rs350.ui.dialog.HintDialog;
import com.clou.rs350.ui.dialog.NormalDialog;
import com.clou.rs350.ui.fragment.BaseFragment;
import com.clou.rs350.ui.popwindow.DateAndTimeSettingPopWindow;
import com.clou.rs350.ui.popwindow.ListPopWindow;
import com.itextpdf.text.pdf.PdfObject;
import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.view.annotation.ViewInject;
import com.lidroid.xutils.view.annotation.event.OnClick;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class ViewResultFragment extends BaseFragment implements CompoundButton.OnCheckedChangeListener, AdapterView.OnItemClickListener {
    @ViewInject(R.id.list_customer_sr)
    private ListView CustomerSerialNoList;
    private Calendar calendar;
    @ViewInject(R.id.tv_serial_compare_style)
    private TextView compareTypeTextView;
    private int compare_type = -1;
    @ViewInject(R.id.checkbox_compare_date)
    private CheckBox dateCheckBox;
    @ViewInject(R.id.btn_delete)
    private Button deleteButton;
    @ViewInject(R.id.btn_detail)
    private Button detailButton;
    @ViewInject(R.id.tv_serial_date_begin)
    private TextView fromDate;
    private CustomerInfoDao m_CustomerDao;
    private CustomerSerialNoAdapter m_CustomerSerialNoAdapter;
    private String m_EndDate;
    private boolean m_FirstIn = true;
    private String m_FromDate;
    private RecordsAdapter m_RecordAdapter;
    @ViewInject(R.id.list_record)
    private ListView recordList;
    @ViewInject(R.id.search)
    private Button searchButton;
    private SearchDao searchDao;
    @ViewInject(R.id.checkbox_all_select)
    private CheckBox selectAllCheckBox;
    @ViewInject(R.id.checkbox_serialno)
    private CheckBox serialNoCheckBox;
    @ViewInject(R.id.edit_serial_compare_value)
    private EditText serialNoEditText;
    @ViewInject(R.id.tv_serial_date_to)
    private TextView toDate;

    @Override // android.support.v4.app.Fragment
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        this.fragmentView = inflater.inflate(R.layout.fragment_view_result, (ViewGroup) null);
        ViewUtils.inject(this, this.fragmentView);
        initData();
        return this.fragmentView;
    }

    @Override // android.support.v4.app.Fragment, com.clou.rs350.ui.fragment.BaseFragment
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initView();
    }

    private void initData() {
        this.searchDao = new SearchDao(this.m_Context);
        this.m_CustomerDao = new CustomerInfoDao(this.m_Context);
    }

    private void initView() {
        this.m_CustomerSerialNoAdapter = new CustomerSerialNoAdapter(this.m_Context);
        this.m_RecordAdapter = new RecordsAdapter(this.m_Context, new View.OnClickListener() {
            /* class com.clou.rs350.ui.fragment.databasefragment.ViewResultFragment.AnonymousClass1 */

            public void onClick(View v) {
                ViewResultFragment.this.setSelectCheck(ViewResultFragment.this.m_RecordAdapter.isSelectAll());
            }
        });
        this.CustomerSerialNoList.setAdapter((ListAdapter) this.m_CustomerSerialNoAdapter);
        this.recordList.setAdapter((ListAdapter) this.m_RecordAdapter);
        this.CustomerSerialNoList.setOnItemClickListener(this);
        this.recordList.setOnItemClickListener(this);
        this.selectAllCheckBox.setOnCheckedChangeListener(this);
        this.deleteButton.setEnabled(ClouData.getInstance().getOperatorInfo().Authority > 0);
    }

    @Override // android.support.v4.app.Fragment, com.clou.rs350.ui.fragment.BaseFragment
    public void onResume() {
        super.onResume();
        if (this.m_FirstIn) {
            searchForSerialNo();
            this.m_FirstIn = false;
        }
    }

    @OnClick({R.id.tv_serial_date_begin, R.id.tv_serial_date_to, R.id.search, R.id.btn_delete, R.id.btn_detail, R.id.tv_serial_compare_style})
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_delete:
                deleteRecord();
                return;
            case R.id.btn_detail:
                List<DataBaseRecordItem> list = this.m_RecordAdapter.getList();
                List<String> times = new ArrayList<>();
                if (list != null && list.size() > 0) {
                    for (int i = 0; i < list.size(); i++) {
                        if (list.get(i).isSelect()) {
                            times.add(list.get(i).getDatetime());
                        }
                    }
                }
                if (this.m_CustomerSerialNoAdapter.getCurrentMeterInfo() == null) {
                    new HintDialog(this.m_Context, (int) R.string.text_no_data).show();
                    return;
                }
                Intent intent = new Intent(this.m_Context, DataDetailActivity.class);
                intent.putExtra("CustomerSerialNo", this.m_CustomerSerialNoAdapter.getCurrentMeterInfo().CustomerSr);
                intent.putExtra("TestTime", (String[]) times.toArray(new String[times.size()]));
                startActivityForResult(intent, 0);
                return;
            case R.id.tv_serial_compare_style:
                final ArrayList<String> array = new ArrayList<>();
                array.add("=");
                array.add(">");
                array.add("<");
                array.add(">=");
                array.add("<=");
                ListPopWindow.showListPopwindow(this.m_Context, (String[]) array.toArray(new String[0]), (TextView) v, new View.OnClickListener() {
                    /* class com.clou.rs350.ui.fragment.databasefragment.ViewResultFragment.AnonymousClass2 */

                    public void onClick(View arg0) {
                        int index = Integer.parseInt(arg0.getTag().toString());
                        ViewResultFragment.this.compareTypeTextView.setText((CharSequence) array.get(index));
                        ViewResultFragment.this.compare_type = index;
                    }
                });
                return;
            case R.id.tv_serial_date_begin:
                popDateSelectorDialog(this.fromDate, true);
                return;
            case R.id.tv_serial_date_to:
                popDateSelectorDialog(this.toDate, false);
                return;
            case R.id.search:
                searchForSerialNo();
                return;
            default:
                return;
        }
    }

    private void deleteRecord() {
        NormalDialog dialog = new NormalDialog(this.m_Context);
        dialog.setOnClick(new View.OnClickListener() {
            /* class com.clou.rs350.ui.fragment.databasefragment.ViewResultFragment.AnonymousClass3 */

            public void onClick(View v) {
                List<DataBaseRecordItem> list = ViewResultFragment.this.m_RecordAdapter.getList();
                if (list != null && list.size() > 0 && ViewResultFragment.this.m_CustomerSerialNoAdapter.getCurrentMeterInfo() != null) {
                    ViewResultFragment.this.searchDao.deleteRecordBySerialList(ViewResultFragment.this.m_CustomerSerialNoAdapter.getCurrentMeterInfo().CustomerSr, list);
                    ViewResultFragment.this.refreshRecordData();
                }
            }
        }, null);
        dialog.show();
        dialog.setTitle(R.string.text_delete_selected_hint);
    }

    private void searchForSerialNo() {
        if (this.serialNoCheckBox.isChecked()) {
            searchSerialNoBaseUi();
        } else {
            searchAllSerialNo();
        }
    }

    private void searchAllSerialNo() {
        this.m_CustomerSerialNoAdapter.refreshData(this.m_CustomerDao.queryAllInfo());
        refreshRecordData();
    }

    private void searchSerialNoBaseUi() {
        if (this.compare_type == -1 || TextUtils.isEmpty(this.serialNoEditText.getText().toString())) {
            new HintDialog(this.m_Context, "MeterSerialNo info isn't complete").show();
            return;
        }
        this.m_CustomerSerialNoAdapter.refreshData(this.m_CustomerDao.queryForDataBaseOrderSerialNo(this.compare_type, this.serialNoEditText.getText().toString()));
        refreshRecordData();
    }

    private void popDateSelectorDialog(TextView textView, final boolean isFromData) {
        new DateAndTimeSettingPopWindow(this.m_Context).showPopWindow(textView, new View.OnClickListener() {
            /* class com.clou.rs350.ui.fragment.databasefragment.ViewResultFragment.AnonymousClass4 */

            public void onClick(View v) {
                String sDate = ((TextView) v).getText().toString();
                if (isFromData) {
                    ViewResultFragment.this.m_FromDate = String.valueOf(sDate) + ":00";
                    return;
                }
                ViewResultFragment.this.m_EndDate = String.valueOf(sDate) + ":59";
            }
        });
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private void refreshRecordData() {
        if (this.m_CustomerSerialNoAdapter.getCount() > 0) {
            String serialNo = this.m_CustomerSerialNoAdapter.getCurrentMeterInfo().CustomerSr;
            this.m_RecordAdapter.refreshData(serialNo, queryRecordData(serialNo));
        } else {
            this.m_RecordAdapter.refreshData(PdfObject.NOTHING, new ArrayList());
        }
        setSelectCheck(-1);
    }

    @Override // android.widget.AdapterView.OnItemClickListener
    public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
        switch (adapterView.getId()) {
            case R.id.list_customer_sr:
                if (this.m_CustomerSerialNoAdapter.setSerialNoIndex(position)) {
                    refreshRecordData();
                    return;
                }
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private void setSelectCheck(int isChecked) {
        boolean z;
        boolean z2;
        boolean z3 = true;
        this.selectAllCheckBox.setOnCheckedChangeListener(null);
        CheckBox checkBox = this.selectAllCheckBox;
        if (isChecked == 1) {
            z = true;
        } else {
            z = false;
        }
        checkBox.setChecked(z);
        this.selectAllCheckBox.setOnCheckedChangeListener(this);
        Button button = this.detailButton;
        if (isChecked != -1) {
            z2 = true;
        } else {
            z2 = false;
        }
        button.setEnabled(z2);
        Button button2 = this.deleteButton;
        if (isChecked == -1) {
            z3 = false;
        }
        button2.setEnabled(z3);
    }

    private List<DataBaseRecordItem> queryRecordData(String serialNo) {
        if (this.dateCheckBox.isChecked()) {
            return this.searchDao.queryBySerialBaseDataPage(serialNo, this.m_FromDate, this.m_EndDate);
        }
        return this.searchDao.queryBySerialBaseDataPage(serialNo);
    }

    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        this.m_RecordAdapter.selectAll(isChecked);
    }

    @Override // android.support.v4.app.Fragment
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        switch (requestCode) {
            case 0:
                if (resultCode == -1) {
                    refreshRecordData();
                    return;
                }
                return;
            default:
                return;
        }
    }
}
