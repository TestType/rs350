package com.clou.rs350.ui.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.clou.rs350.Preferences;
import com.clou.rs350.R;
import com.clou.rs350.utils.OtherUtils;

public class LimitDialog extends Dialog implements View.OnClickListener {
    private Button btn_Cancel = null;
    private Button btn_OK = null;
    View.OnClickListener mOnClick;
    private Context m_Context = null;
    private TextView txt_Current_Balance = null;
    private TextView txt_Current_Symmetry = null;
    private TextView txt_Current_THD = null;
    private TextView txt_Voltage_Balance = null;
    private TextView txt_Voltage_Lower_Limit = null;
    private TextView txt_Voltage_Nominal = null;
    private TextView txt_Voltage_Symmetry = null;
    private TextView txt_Voltage_THD = null;
    private TextView txt_Voltage_Upper_Limit = null;

    public LimitDialog(Context context, View.OnClickListener okClick) {
        super(context, R.style.dialog);
        requestWindowFeature(1);
        this.m_Context = context;
        this.mOnClick = okClick;
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_limit_setting);
        setCanceledOnTouchOutside(true);
        initView();
    }

    private void initView() {
        this.btn_OK = (Button) findViewById(R.id.btn_ok);
        this.btn_OK.setOnClickListener(this);
        this.btn_Cancel = (Button) findViewById(R.id.btn_cancel);
        this.btn_Cancel.setOnClickListener(this);
        this.txt_Voltage_Nominal = (TextView) findViewById(R.id.txt_voltage_nominal);
        this.txt_Voltage_Nominal.setOnClickListener(this);
        this.txt_Voltage_Upper_Limit = (TextView) findViewById(R.id.txt_voltage_upper_limit);
        this.txt_Voltage_Upper_Limit.setOnClickListener(this);
        this.txt_Voltage_Lower_Limit = (TextView) findViewById(R.id.txt_voltage_lower_limit);
        this.txt_Voltage_Lower_Limit.setOnClickListener(this);
        this.txt_Voltage_Balance = (TextView) findViewById(R.id.txt_voltage_balance_limit);
        this.txt_Voltage_Balance.setOnClickListener(this);
        this.txt_Voltage_Symmetry = (TextView) findViewById(R.id.txt_voltage_symmetry_limit);
        this.txt_Voltage_Symmetry.setOnClickListener(this);
        this.txt_Current_Balance = (TextView) findViewById(R.id.txt_current_balance_limit);
        this.txt_Current_Balance.setOnClickListener(this);
        this.txt_Current_Symmetry = (TextView) findViewById(R.id.txt_current_symmetry_limit);
        this.txt_Current_Symmetry.setOnClickListener(this);
        this.txt_Voltage_THD = (TextView) findViewById(R.id.txt_voltage_thd_limit);
        this.txt_Voltage_THD.setOnClickListener(this);
        this.txt_Current_THD = (TextView) findViewById(R.id.txt_current_thd_limit);
        this.txt_Current_THD.setOnClickListener(this);
        this.txt_Voltage_Nominal.setText(new StringBuilder(String.valueOf(Preferences.getFloat(Preferences.Key.VOLTAGENOMINAL, 230.0f))).toString());
        this.txt_Voltage_Upper_Limit.setText(new StringBuilder(String.valueOf(Preferences.getFloat(Preferences.Key.VOLTAGEUPPERLIMIT, 5.0f))).toString());
        this.txt_Voltage_Lower_Limit.setText(new StringBuilder(String.valueOf(Preferences.getFloat(Preferences.Key.VOLTAGELOWERLIMIT, 5.0f))).toString());
        this.txt_Voltage_Balance.setText(new StringBuilder(String.valueOf(Preferences.getFloat(Preferences.Key.VOLTAGEUNBALANCE, 0.2f))).toString());
        this.txt_Voltage_Symmetry.setText(new StringBuilder(String.valueOf(Preferences.getFloat(Preferences.Key.VOLTAGESYMMETRY, 0.5f))).toString());
        this.txt_Current_Balance.setText(new StringBuilder(String.valueOf(Preferences.getFloat(Preferences.Key.CURRENTUNBALANCE, 0.5f))).toString());
        this.txt_Current_Symmetry.setText(new StringBuilder(String.valueOf(Preferences.getFloat(Preferences.Key.CURRENTSYMMETRY, 3.0f))).toString());
        this.txt_Voltage_THD.setText(new StringBuilder(String.valueOf(Preferences.getFloat(Preferences.Key.VOLTAGEHARMONICLIMIT, 0.5f))).toString());
        this.txt_Current_THD.setText(new StringBuilder(String.valueOf(Preferences.getFloat(Preferences.Key.CURRENTHARMONICLIMIT, 0.5f))).toString());
    }

    private void parseData() {
        Preferences.putFloat(Preferences.Key.VOLTAGENOMINAL, (float) OtherUtils.parseDouble(this.txt_Voltage_Nominal));
        Preferences.putFloat(Preferences.Key.VOLTAGEUPPERLIMIT, (float) OtherUtils.parseDouble(this.txt_Voltage_Upper_Limit));
        Preferences.putFloat(Preferences.Key.VOLTAGELOWERLIMIT, (float) OtherUtils.parseDouble(this.txt_Voltage_Lower_Limit));
        Preferences.putFloat(Preferences.Key.VOLTAGEUNBALANCE, (float) OtherUtils.parseDouble(this.txt_Voltage_Balance));
        Preferences.putFloat(Preferences.Key.VOLTAGESYMMETRY, (float) OtherUtils.parseDouble(this.txt_Voltage_Symmetry));
        Preferences.putFloat(Preferences.Key.CURRENTUNBALANCE, (float) OtherUtils.parseDouble(this.txt_Current_Balance));
        Preferences.putFloat(Preferences.Key.CURRENTSYMMETRY, (float) OtherUtils.parseDouble(this.txt_Current_Symmetry));
        Preferences.putFloat(Preferences.Key.VOLTAGEHARMONICLIMIT, (float) OtherUtils.parseDouble(this.txt_Voltage_THD));
        Preferences.putFloat(Preferences.Key.CURRENTHARMONICLIMIT, (float) OtherUtils.parseDouble(this.txt_Current_THD));
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_cancel:
                if (isShowing()) {
                    dismiss();
                    return;
                }
                return;
            case R.id.btn_ok:
                parseData();
                if (this.mOnClick != null) {
                    this.mOnClick.onClick(v);
                }
                if (isShowing()) {
                    dismiss();
                    return;
                }
                return;
            case R.id.txt_voltage_nominal:
            case R.id.txt_voltage_upper_limit:
            case R.id.txt_voltage_lower_limit:
            case R.id.txt_voltage_balance_limit:
            case R.id.txt_voltage_symmetry_limit:
            case R.id.txt_current_balance_limit:
            case R.id.txt_current_symmetry_limit:
            case R.id.txt_voltage_thd_limit:
            case R.id.txt_current_thd_limit:
                NumericDialog numericDialog = new NumericDialog(this.m_Context);
                numericDialog.setMin(0.0d);
                numericDialog.showMyDialog((TextView) v);
                return;
            default:
                return;
        }
    }
}
