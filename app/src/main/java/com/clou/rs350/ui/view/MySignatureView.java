package com.clou.rs350.ui.view;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import com.itextpdf.text.pdf.ColumnText;

@SuppressLint({"DrawAllocation", "FloatMath"})
public class MySignatureView extends View {
    private static final String TAG = "SignatureView";
    private Bitmap mBitmap;
    private Paint mBitmapPaint;
    private Canvas mCanvas;
    private boolean mHasSign;
    private Paint mPathPaint;
    private float mX;
    private float mY;

    public MySignatureView(Context context) {
        super(context);
        init();
    }

    public MySignatureView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public MySignatureView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    private void init() {
        this.mBitmapPaint = new Paint(4);
        this.mPathPaint = new Paint();
        this.mPathPaint.setAntiAlias(true);
        this.mPathPaint.setDither(true);
        this.mPathPaint.setColor(-16777216);
        this.mPathPaint.setStyle(Paint.Style.STROKE);
        this.mPathPaint.setStrokeJoin(Paint.Join.ROUND);
        this.mPathPaint.setStrokeCap(Paint.Cap.ROUND);
        this.mPathPaint.setStrokeWidth(5.0f);
    }

    /* access modifiers changed from: protected */
    public void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
    }

    /* access modifiers changed from: protected */
    public void createBitmap() {
        if (this.mBitmap == null) {
            this.mBitmap = Bitmap.createBitmap(getMeasuredWidth(), getMeasuredHeight(), Bitmap.Config.ARGB_8888);
            this.mBitmap.eraseColor(-1);
            this.mCanvas = new Canvas(this.mBitmap);
        }
    }

    /* access modifiers changed from: protected */
    public void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        createBitmap();
        canvas.drawColor(0);
        canvas.drawBitmap(this.mBitmap, ColumnText.GLOBAL_SPACE_CHAR_RATIO, ColumnText.GLOBAL_SPACE_CHAR_RATIO, this.mBitmapPaint);
    }

    private void touch_start(float x, float y) {
        this.mCanvas.drawPoint(x, y, this.mPathPaint);
        this.mHasSign = true;
        this.mX = x;
        this.mY = y;
    }

    private void touch_move(float x, float y) {
        this.mCanvas.drawLine(this.mX, this.mY, x, y, this.mPathPaint);
        this.mHasSign = true;
        this.mX = x;
        this.mY = y;
    }

    private void touch_up() {
    }

    public boolean onTouchEvent(MotionEvent event) {
        float x = event.getX();
        float y = event.getY();
        switch (event.getAction()) {
            case 0:
                touch_start(x, y);
                invalidate();
                return true;
            case 1:
                touch_up();
                invalidate();
                return true;
            case 2:
                touch_move(x, y);
                invalidate();
                return true;
            default:
                return true;
        }
    }

    public void clearSignature() {
        this.mBitmap.eraseColor(-1);
        this.mHasSign = false;
        invalidate();
    }

    public void setSignatureBitmap(Bitmap bmp) {
        this.mBitmap = bmp;
        invalidate();
    }

    public Bitmap getSignatureBitmap() {
        return this.mBitmap;
    }

    public boolean getHasSign() {
        return this.mHasSign;
    }

    public void setHasSign(boolean flag) {
        this.mHasSign = flag;
    }
}
