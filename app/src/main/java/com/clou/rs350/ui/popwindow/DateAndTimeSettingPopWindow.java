package com.clou.rs350.ui.popwindow;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.TimePicker;

import com.clou.rs350.R;
import com.clou.rs350.constants.Constants;

import java.text.SimpleDateFormat;
import java.util.Date;

public class DateAndTimeSettingPopWindow extends PopupWindow implements View.OnClickListener, CompoundButton.OnCheckedChangeListener {
    TextView TargetView;
    private String TimeFormat = "yyyy/MM/dd HH:mm";
    DatePicker dpSetting;
    Context m_Context;
    View.OnClickListener m_OnClick;
    TimePicker tpSetting;

    public DateAndTimeSettingPopWindow(Context context) {
        super(context);
        this.m_Context = context;
        initView();
    }

    private void initView() {
        View conentView = ((LayoutInflater) this.m_Context.getSystemService("layout_inflater")).inflate(R.layout.pop_window_date_time_setting, (ViewGroup) null);
        conentView.findViewById(R.id.ok).setOnClickListener(this);
        conentView.findViewById(R.id.cancel).setOnClickListener(this);
        this.tpSetting = (TimePicker) conentView.findViewById(R.id.tp_setting);
        this.tpSetting.setIs24HourView(true);
        this.tpSetting.setDescendantFocusability(393216);
        this.dpSetting = (DatePicker) conentView.findViewById(R.id.dp_setting);
        this.dpSetting.setDescendantFocusability(393216);
        setContentView(conentView);
        setWidth(this.m_Context.getResources().getDimensionPixelSize(R.dimen.dp_320));
        setHeight(this.m_Context.getResources().getDimensionPixelSize(R.dimen.dp_150));
        setFocusable(true);
        setOutsideTouchable(true);
        update();
        setBackgroundDrawable(new ColorDrawable(0));
        setAnimationStyle(16973826);
    }

    public void showPopWindow(View currentClick, View.OnClickListener onclick) {
        showPopWindow(currentClick, null, onclick);
    }

    public void showPopWindow(View currentClick, View parentView, View.OnClickListener onclick) {
        this.m_OnClick = onclick;
        this.TargetView = (TextView) currentClick;
        setSoftInputMode(16);
        if (!isShowing()) {
            int[] location = new int[2];
            currentClick.getLocationOnScreen(location);
            if (parentView != null) {
                showAtLocation(parentView, 0, location[0] + ((currentClick.getWidth() - getWidth()) / 2), location[1] + currentClick.getHeight());
            } else {
                showAtLocation(currentClick, 0, location[0] + ((currentClick.getWidth() - getWidth()) / 2), location[1] + currentClick.getHeight());
            }
        } else {
            dismiss();
        }
    }

    public void setType(int type) {
        if (type == 0) {
            setWidth(this.m_Context.getResources().getDimensionPixelSize(R.dimen.dp_320));
            this.tpSetting.setVisibility(0);
            this.TimeFormat = "yyyy/MM/dd HH:mm";
        } else if (1 == type) {
            setWidth(this.m_Context.getResources().getDimensionPixelSize(R.dimen.dp_280));
            this.tpSetting.setVisibility(8);
            this.TimeFormat = Constants.CALIBARTTIMEFORMAT;
        }
    }

    private void praseValue() {
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ok:
                Date time = new Date(this.dpSetting.getYear() - 1900, this.dpSetting.getMonth(), this.dpSetting.getDayOfMonth(), this.tpSetting.getCurrentHour().intValue(), this.tpSetting.getCurrentMinute().intValue());
                this.TargetView.setText(new SimpleDateFormat(this.TimeFormat).format(time));
                this.TargetView.setTag(Long.valueOf(time.getTime()));
                if (this.m_OnClick != null) {
                    this.m_OnClick.onClick(this.TargetView);
                }
                if (isShowing()) {
                    dismiss();
                    return;
                }
                return;
            case R.id.cancel:
                if (isShowing()) {
                    dismiss();
                    return;
                }
                return;
            default:
                return;
        }
    }

    public void onCheckedChanged(CompoundButton arg0, boolean arg1) {
    }
}
