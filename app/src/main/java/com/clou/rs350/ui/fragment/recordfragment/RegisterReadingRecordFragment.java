package com.clou.rs350.ui.fragment.recordfragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.clou.rs350.R;
import com.clou.rs350.db.model.sequence.RegisterReadingData;
import com.clou.rs350.ui.adapter.RegisterItemAdapter;
import com.clou.rs350.ui.fragment.BaseFragment;
import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.view.annotation.ViewInject;

public class RegisterReadingRecordFragment extends BaseFragment implements View.OnClickListener {
    private static final String TAG = "RegisterReadingRecordFragment";
    private RegisterItemAdapter adapter;
    @ViewInject(R.id.list_register)
    private ListView list_Register;
    private RegisterReadingData m_Data;
    @ViewInject(R.id.txt_register_state)
    private TextView txt_Register_State;

    public RegisterReadingRecordFragment(RegisterReadingData data) {
        this.m_Data = data;
    }

    @Override // android.support.v4.app.Fragment
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        this.fragmentView = inflater.inflate(R.layout.fragment_record_register_reading, (ViewGroup) null);
        ViewUtils.inject(this, this.fragmentView);
        return this.fragmentView;
    }

    @Override // android.support.v4.app.Fragment, com.clou.rs350.ui.fragment.BaseFragment
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        this.adapter = new RegisterItemAdapter(this.m_Context, 0);
        this.adapter.setIsShow(true);
        this.list_Register.setAdapter((ListAdapter) this.adapter);
    }

    public void onClick(View v) {
        v.getId();
    }

    private void showData() {
        this.txt_Register_State.setText(this.m_Data.Index == 0 ? R.string.text_register_beforetest : R.string.text_register_aftertest);
        this.adapter.refresh(this.m_Data.Registers);
    }

    @Override // android.support.v4.app.Fragment, com.clou.rs350.ui.fragment.BaseFragment
    public void onResume() {
        showData();
        super.onResume();
    }

    /* access modifiers changed from: protected */
    @Override // com.clou.rs350.ui.fragment.BaseFragment
    public void copyData() {
    }

    @Override // android.support.v4.app.Fragment, com.clou.rs350.ui.fragment.BaseFragment
    public void onPause() {
        super.onPause();
    }
}
