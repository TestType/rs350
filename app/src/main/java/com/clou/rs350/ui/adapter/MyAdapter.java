package com.clou.rs350.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.clou.rs350.R;

import java.util.List;

public class MyAdapter extends BaseAdapter {
    private List<String> list = null;
    private Context mContext;

    public MyAdapter(List<String> list2, Context context) {
        this.list = list2;
        this.mContext = context;
    }

    public int getCount() {
        return this.list.size();
    }

    public Object getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return (long) position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = LayoutInflater.from(this.mContext).inflate(R.layout.adapter_list, (ViewGroup) null);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        holder.show(position);
        return convertView;
    }

    private class ViewHolder {
        private TextView dataTextView;

        ViewHolder(View view) {
            this.dataTextView = (TextView) view.findViewById(R.id.apt_list_textview);
        }

        /* access modifiers changed from: package-private */
        public void show(int position) {
            this.dataTextView.setText((String) MyAdapter.this.list.get(position));
        }
    }
}
