package com.clou.rs350.ui.view.control;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.FrameLayout;

public class SubContentRelativeLayout extends FrameLayout {
    public SubContentRelativeLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    /* access modifiers changed from: protected */
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
    }
}
