package com.clou.rs350.ui.fragment.recordfragment;

import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.clou.rs350.R;
import com.clou.rs350.db.model.WiringCheckMeasurement;
import com.clou.rs350.handler.HandlerSwitchView;
import com.clou.rs350.ui.fragment.BaseFragment;
import com.clou.rs350.ui.view.MyPowerQuadrantView;
import com.clou.rs350.ui.view.MyVectorView;
import com.clou.rs350.utils.AngleUtil;
import com.clou.rs350.utils.OtherUtils;
import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.view.annotation.ViewInject;

import java.util.ArrayList;
import java.util.List;

public class WiringCheckRecordFragment extends BaseFragment implements View.OnClickListener, HandlerSwitchView.IOnSelectCallback {
    @ViewInject(R.id.angle1)
    private TextView Angle1;
    @ViewInject(R.id.angle2)
    private TextView Angle2;
    @ViewInject(R.id.angle3)
    private TextView Angle3;
    @ViewInject(R.id.angle4)
    private TextView Angle4;
    @ViewInject(R.id.i1)
    private TextView I1;
    @ViewInject(R.id.i2)
    private TextView I2;
    @ViewInject(R.id.i3)
    private TextView I3;
    @ViewInject(R.id.i4)
    private TextView I4;
    @ViewInject(R.id.i)
    private TextView ITv;
    @ViewInject(R.id.l1anglei)
    private TextView L1AngleITv;
    @ViewInject(R.id.l1angleui)
    private TextView L1AngleUITv;
    @ViewInject(R.id.l1angleu)
    private TextView L1AngleUTv;
    @ViewInject(R.id.l1currentbalance)
    private TextView L1CurrentBalanceTv;
    @ViewInject(R.id.l1currentdirection)
    private TextView L1CurrentDirectionTv;
    @ViewInject(R.id.l1currentsymmetry)
    private TextView L1CurrentSymmetryTv;
    @ViewInject(R.id.l1currentthd)
    private TextView L1CurrentThdTv;
    @ViewInject(R.id.l1current)
    private TextView L1CurrentTv;
    @ViewInject(R.id.txt_il1)
    private TextView L1ITv;
    @ViewInject(R.id.l1p)
    private TextView L1PTv;
    @ViewInject(R.id.l1phasesequencevoltage)
    private TextView L1PhaseSequenceVoltageTv;
    @ViewInject(R.id.l1q)
    private TextView L1QTv;
    @ViewInject(R.id.txt_ul1)
    private TextView L1UTv;
    @ViewInject(R.id.l1voltagebalance)
    private TextView L1VoltageBalanceTv;
    @ViewInject(R.id.l1voltagesymmetry)
    private TextView L1VoltageSymmetryTv;
    @ViewInject(R.id.l1voltagethd)
    private TextView L1VoltageThdTv;
    @ViewInject(R.id.l1voltage)
    private TextView L1VoltageTv;
    @ViewInject(R.id.l2anglei)
    private TextView L2AngleITv;
    @ViewInject(R.id.l2angleui)
    private TextView L2AngleUITv;
    @ViewInject(R.id.l2angleu)
    private TextView L2AngleUTv;
    @ViewInject(R.id.l2currentbalance)
    private TextView L2CurrentBalanceTv;
    @ViewInject(R.id.l2currentdirection)
    private TextView L2CurrentDirectionTv;
    @ViewInject(R.id.l2currentsymmetry)
    private TextView L2CurrentSymmetryTv;
    @ViewInject(R.id.l2currentthd)
    private TextView L2CurrentThdTv;
    @ViewInject(R.id.l2current)
    private TextView L2CurrentTv;
    @ViewInject(R.id.txt_il2)
    private TextView L2ITv;
    @ViewInject(R.id.l2p)
    private TextView L2PTv;
    @ViewInject(R.id.l1phasesequencecurrent)
    private TextView L2PhaseSequenceCurrentTv;
    @ViewInject(R.id.l2q)
    private TextView L2QTv;
    @ViewInject(R.id.txt_ul2)
    private TextView L2UTv;
    @ViewInject(R.id.l2voltagebalance)
    private TextView L2VoltageBalanceTv;
    @ViewInject(R.id.l2voltagesymmetry)
    private TextView L2VoltageSymmetryTv;
    @ViewInject(R.id.l2voltagethd)
    private TextView L2VoltageThdTv;
    @ViewInject(R.id.l2voltage)
    private TextView L2VoltageTv;
    @ViewInject(R.id.l3anglei)
    private TextView L3AngleITv;
    @ViewInject(R.id.l3angleui)
    private TextView L3AngleUITv;
    @ViewInject(R.id.l3angleu)
    private TextView L3AngleUTv;
    @ViewInject(R.id.l3currentbalance)
    private TextView L3CurrentBalanceTv;
    @ViewInject(R.id.l3currentdirection)
    private TextView L3CurrentDirectionTv;
    @ViewInject(R.id.l3currentsymmetry)
    private TextView L3CurrentSymmetryTv;
    @ViewInject(R.id.l3currentthd)
    private TextView L3CurrentThdTv;
    @ViewInject(R.id.l3current)
    private TextView L3CurrentTv;
    @ViewInject(R.id.txt_il3)
    private TextView L3ITv;
    @ViewInject(R.id.l3p)
    private TextView L3PTv;
    @ViewInject(R.id.l3q)
    private TextView L3QTv;
    @ViewInject(R.id.txt_ul3)
    private TextView L3UTv;
    @ViewInject(R.id.l3voltagebalance)
    private TextView L3VoltageBalanceTv;
    @ViewInject(R.id.l3voltagesymmetry)
    private TextView L3VoltageSymmetryTv;
    @ViewInject(R.id.l3voltagethd)
    private TextView L3VoltageThdTv;
    @ViewInject(R.id.l3voltage)
    private TextView L3VoltageTv;
    @ViewInject(R.id.p1)
    private TextView P1;
    @ViewInject(R.id.p2)
    private TextView P2;
    @ViewInject(R.id.p3)
    private TextView P3;
    @ViewInject(R.id.p4)
    private TextView P4;
    @ViewInject(R.id.pf1)
    private TextView PF1;
    @ViewInject(R.id.pf2)
    private TextView PF2;
    @ViewInject(R.id.pf3)
    private TextView PF3;
    @ViewInject(R.id.pf4)
    private TextView PF4;
    @ViewInject(R.id.powerquadrant_view1)
    private MyPowerQuadrantView PowerQuadrantView1;
    @ViewInject(R.id.powerquadrant_view2)
    private MyPowerQuadrantView PowerQuadrantView2;
    @ViewInject(R.id.powerquadrant_view3)
    private MyPowerQuadrantView PowerQuadrantView3;
    @ViewInject(R.id.powerquadrant_view4)
    private MyPowerQuadrantView PowerQuadrantView4;
    @ViewInject(R.id.q1)
    private TextView Q1;
    @ViewInject(R.id.q2)
    private TextView Q2;
    @ViewInject(R.id.q3)
    private TextView Q3;
    @ViewInject(R.id.q4)
    private TextView Q4;
    @ViewInject(R.id.tvremark)
    private TextView RemarkTv;
    @ViewInject(R.id.u1)
    private TextView U1;
    @ViewInject(R.id.u2)
    private TextView U2;
    @ViewInject(R.id.u3)
    private TextView U3;
    @ViewInject(R.id.u4)
    private TextView U4;
    @ViewInject(R.id.u)
    private TextView UTv;
    private WiringCheckMeasurement m_WiringCheck;
    private HandlerSwitchView switchView;
    @ViewInject(R.id.realtime_vector_clockview)
    private MyVectorView vectorView;

    public WiringCheckRecordFragment(WiringCheckMeasurement data) {
        this.m_WiringCheck = data;
    }

    @Override // android.support.v4.app.Fragment
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        this.fragmentView = inflater.inflate(R.layout.fragment_record_wiring_check, (ViewGroup) null);
        ViewUtils.inject(this, this.fragmentView);
        this.fragmentView.findViewById(R.id.btn_wiring).setOnClickListener(this);
        this.fragmentView.findViewById(R.id.btn_analysis).setOnClickListener(this);
        this.fragmentView.findViewById(R.id.btn_powerquadrant).setOnClickListener(this);
        return this.fragmentView;
    }

    @Override // android.support.v4.app.Fragment, com.clou.rs350.ui.fragment.BaseFragment
    public void onResume() {
        super.onResume();
        ShowData();
    }

    @Override // android.support.v4.app.Fragment, com.clou.rs350.ui.fragment.BaseFragment
    public void onPause() {
        super.onPause();
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_wiring:
                findViewById(R.id.connection_check_wiringcheckonline).setVisibility(0);
                findViewById(R.id.connection_check_analysis).setVisibility(8);
                findViewById(R.id.connection_check_powerquadrant).setVisibility(8);
                return;
            case R.id.btn_powerquadrant:
                findViewById(R.id.connection_check_powerquadrant).setVisibility(0);
                findViewById(R.id.connection_check_analysis).setVisibility(8);
                findViewById(R.id.connection_check_wiringcheckonline).setVisibility(8);
                return;
            case R.id.btn_analysis:
                findViewById(R.id.connection_check_analysis).setVisibility(0);
                findViewById(R.id.connection_check_wiringcheckonline).setVisibility(8);
                findViewById(R.id.connection_check_powerquadrant).setVisibility(8);
                return;
            default:
                return;
        }
    }

    private void ShowData() {
        this.L1UTv.setText(this.m_WiringCheck.VoltageValueL1Show.s_Value);
        this.L2UTv.setText(this.m_WiringCheck.VoltageValueL2Show.s_Value);
        this.L3UTv.setText(this.m_WiringCheck.VoltageValueL3Show.s_Value);
        this.L1AngleUTv.setText(this.m_WiringCheck.VoltageAngleL1Show.s_Value);
        this.L2AngleUTv.setText(this.m_WiringCheck.VoltageAngleL2Show.s_Value);
        this.L3AngleUTv.setText(this.m_WiringCheck.VoltageAngleL3Show.s_Value);
        this.L1AngleUITv.setText(this.m_WiringCheck.VoltageCurrentAngleL1Show.s_Value);
        this.L2AngleUITv.setText(this.m_WiringCheck.VoltageCurrentAngleL2Show.s_Value);
        this.L3AngleUITv.setText(this.m_WiringCheck.VoltageCurrentAngleL3Show.s_Value);
        this.U1.setText(this.m_WiringCheck.VoltageValueL1Show.s_Value);
        this.U2.setText(this.m_WiringCheck.VoltageValueL2Show.s_Value);
        this.U3.setText(this.m_WiringCheck.VoltageValueL3Show.s_Value);
        this.Angle1.setText(this.m_WiringCheck.VoltageCurrentAngleL1Show.s_Value);
        this.Angle2.setText(this.m_WiringCheck.VoltageCurrentAngleL2Show.s_Value);
        this.Angle3.setText(this.m_WiringCheck.VoltageCurrentAngleL3Show.s_Value);
        this.L1ITv.setText(this.m_WiringCheck.CurrentValueL1.s_Value);
        this.L2ITv.setText(this.m_WiringCheck.CurrentValueL2.s_Value);
        this.L3ITv.setText(this.m_WiringCheck.CurrentValueL3.s_Value);
        this.L1AngleITv.setText(this.m_WiringCheck.CurrentAngleL1.s_Value);
        this.L2AngleITv.setText(this.m_WiringCheck.CurrentAngleL2.s_Value);
        this.L3AngleITv.setText(this.m_WiringCheck.CurrentAngleL3.s_Value);
        this.L1PTv.setText(this.m_WiringCheck.ActivePowerL1.s_Value);
        this.L2PTv.setText(this.m_WiringCheck.ActivePowerL2.s_Value);
        this.L3PTv.setText(this.m_WiringCheck.ActivePowerL3.s_Value);
        this.L1QTv.setText(this.m_WiringCheck.ReactivePowerL1.s_Value);
        this.L2QTv.setText(this.m_WiringCheck.ReactivePowerL2.s_Value);
        this.L3QTv.setText(this.m_WiringCheck.ReactivePowerL3.s_Value);
        this.UTv.setText(this.m_WiringCheck.VoltagePhaseSequence);
        this.ITv.setText(this.m_WiringCheck.CurrentPhaseSequence);
        this.RemarkTv.setText(this.m_WiringCheck.Remark);
        ShowVector(this.m_WiringCheck);
        ShowPowerQuadrant(this.m_WiringCheck);
        this.L1PhaseSequenceVoltageTv.setText(this.m_WiringCheck.VoltagePhaseSequence);
        this.L2PhaseSequenceCurrentTv.setText(this.m_WiringCheck.CurrentPhaseSequence);
        OtherUtils.setText(this.L1VoltageThdTv, this.m_WiringCheck.SiteHarmonic[0]);
        OtherUtils.setText(this.L2VoltageThdTv, this.m_WiringCheck.SiteHarmonic[1]);
        OtherUtils.setText(this.L3VoltageThdTv, this.m_WiringCheck.SiteHarmonic[2]);
        OtherUtils.setText(this.L1CurrentThdTv, this.m_WiringCheck.SiteHarmonic[3]);
        OtherUtils.setText(this.L2CurrentThdTv, this.m_WiringCheck.SiteHarmonic[4]);
        OtherUtils.setText(this.L3CurrentThdTv, this.m_WiringCheck.SiteHarmonic[5]);
        OtherUtils.setText(this.L1VoltageTv, this.m_WiringCheck.SiteState[0]);
        OtherUtils.setText(this.L2VoltageTv, this.m_WiringCheck.SiteState[1]);
        OtherUtils.setText(this.L3VoltageTv, this.m_WiringCheck.SiteState[2]);
        OtherUtils.setText(this.L1CurrentTv, this.m_WiringCheck.SiteState[3]);
        OtherUtils.setText(this.L2CurrentTv, this.m_WiringCheck.SiteState[4]);
        OtherUtils.setText(this.L3CurrentTv, this.m_WiringCheck.SiteState[5]);
        OtherUtils.setText(this.L1CurrentDirectionTv, this.m_WiringCheck.SiteCurrentDirection[0]);
        OtherUtils.setText(this.L2CurrentDirectionTv, this.m_WiringCheck.SiteCurrentDirection[1]);
        OtherUtils.setText(this.L3CurrentDirectionTv, this.m_WiringCheck.SiteCurrentDirection[2]);
        OtherUtils.setText(this.L1VoltageBalanceTv, this.m_WiringCheck.SiteVoltageBalance[0]);
        OtherUtils.setText(this.L2VoltageBalanceTv, this.m_WiringCheck.SiteVoltageBalance[1]);
        OtherUtils.setText(this.L3VoltageBalanceTv, this.m_WiringCheck.SiteVoltageBalance[2]);
        OtherUtils.setText(this.L1VoltageSymmetryTv, this.m_WiringCheck.SiteVoltageSymmetry[0]);
        OtherUtils.setText(this.L2VoltageSymmetryTv, this.m_WiringCheck.SiteVoltageSymmetry[1]);
        OtherUtils.setText(this.L3VoltageSymmetryTv, this.m_WiringCheck.SiteVoltageSymmetry[2]);
        OtherUtils.setText(this.L1CurrentBalanceTv, this.m_WiringCheck.SiteCurrentBalance[0]);
        OtherUtils.setText(this.L2CurrentBalanceTv, this.m_WiringCheck.SiteCurrentBalance[1]);
        OtherUtils.setText(this.L3CurrentBalanceTv, this.m_WiringCheck.SiteCurrentBalance[2]);
        OtherUtils.setText(this.L1CurrentSymmetryTv, this.m_WiringCheck.SiteCurrentSymmetry[0]);
        OtherUtils.setText(this.L2CurrentSymmetryTv, this.m_WiringCheck.SiteCurrentSymmetry[1]);
        OtherUtils.setText(this.L3CurrentSymmetryTv, this.m_WiringCheck.SiteCurrentSymmetry[2]);
        this.I1.setText(this.m_WiringCheck.CurrentValueL1.s_Value);
        this.I2.setText(this.m_WiringCheck.CurrentValueL2.s_Value);
        this.I3.setText(this.m_WiringCheck.CurrentValueL3.s_Value);
        this.PF1.setText(this.m_WiringCheck.PowerFactorL1.s_Value);
        this.PF2.setText(this.m_WiringCheck.PowerFactorL2.s_Value);
        this.PF3.setText(this.m_WiringCheck.PowerFactorL3.s_Value);
        this.PF4.setText(this.m_WiringCheck.PowerFactorTotal.s_Value);
        this.P1.setText(this.m_WiringCheck.ActivePowerL1.s_Value);
        this.P2.setText(this.m_WiringCheck.ActivePowerL2.s_Value);
        this.P3.setText(this.m_WiringCheck.ActivePowerL3.s_Value);
        this.P4.setText(this.m_WiringCheck.ActivePowerTotal.s_Value);
        this.Q1.setText(this.m_WiringCheck.ReactivePowerL1.s_Value);
        this.Q2.setText(this.m_WiringCheck.ReactivePowerL2.s_Value);
        this.Q3.setText(this.m_WiringCheck.ReactivePowerL3.s_Value);
        this.Q4.setText(this.m_WiringCheck.ReactivePowerTotal.s_Value);
    }

    private void ShowVector(WiringCheckMeasurement value) {
        this.vectorView.postInvalidate(new Double[]{Double.valueOf(value.VoltageValueL1Vector.d_Value), Double.valueOf(AngleUtil.adjustAngle(value.VoltageAngleL1Vector.d_Value)), Double.valueOf(value.VoltageValueL2Vector.d_Value), Double.valueOf(AngleUtil.adjustAngle(value.VoltageAngleL2Vector.d_Value)), Double.valueOf(value.VoltageValueL3Vector.d_Value), Double.valueOf(AngleUtil.adjustAngle(value.VoltageAngleL3Vector.d_Value)), Double.valueOf(value.CurrentValueL1.d_Value), Double.valueOf(AngleUtil.adjustAngle(value.CurrentAngleL1.d_Value)), Double.valueOf(value.CurrentValueL2.d_Value), Double.valueOf(AngleUtil.adjustAngle(value.CurrentAngleL2.d_Value)), Double.valueOf(value.CurrentValueL3.d_Value), Double.valueOf(AngleUtil.adjustAngle(value.CurrentAngleL3.d_Value))}, value.Wiring, value.AngleDefinition);
    }

    private void ShowPowerQuadrant(WiringCheckMeasurement value) {
        this.PowerQuadrantView1.postInvalidate(value.ActivePowerL1.d_Value, value.ReactivePowerL1.d_Value, value.ApparentPowerL1.d_Value);
        this.PowerQuadrantView2.postInvalidate(value.ActivePowerL2.d_Value, value.ReactivePowerL2.d_Value, value.ApparentPowerL2.d_Value);
        this.PowerQuadrantView3.postInvalidate(value.ActivePowerL3.d_Value, value.ReactivePowerL3.d_Value, value.ApparentPowerL3.d_Value);
        this.PowerQuadrantView4.postInvalidate(value.ActivePowerTotal.d_Value, value.ReactivePowerTotal.d_Value, value.ApparentPowerTotal.d_Value);
    }

    @Override // android.support.v4.app.Fragment, com.clou.rs350.ui.fragment.BaseFragment
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initView();
    }

    private void initView() {
        List<View> tabViews = new ArrayList<>();
        tabViews.add(findViewById(R.id.btn_wiring));
        tabViews.add(findViewById(R.id.btn_powerquadrant));
        tabViews.add(findViewById(R.id.btn_analysis));
        this.switchView = new HandlerSwitchView(this.m_Context);
        this.switchView.setTabViews(tabViews);
        this.switchView.boundClick();
        this.switchView.setOnTabChangedCallback(this);
        this.switchView.selectView(0);
        this.RemarkTv.setMovementMethod(ScrollingMovementMethod.getInstance());
    }

    @Override // com.clou.rs350.handler.HandlerSwitchView.IOnSelectCallback
    public void onSelectedChanged(int index) {
    }

    @Override // com.clou.rs350.handler.HandlerSwitchView.IOnSelectCallback
    public void onClickView(View v) {
        switch (v.getId()) {
            case R.id.btn_wiring:
                findViewById(R.id.connection_check_wiringcheckonline).setVisibility(0);
                findViewById(R.id.connection_check_analysis).setVisibility(8);
                findViewById(R.id.connection_check_powerquadrant).setVisibility(8);
                return;
            case R.id.btn_powerquadrant:
                findViewById(R.id.connection_check_powerquadrant).setVisibility(0);
                findViewById(R.id.connection_check_analysis).setVisibility(8);
                findViewById(R.id.connection_check_wiringcheckonline).setVisibility(8);
                return;
            case R.id.btn_analysis:
                findViewById(R.id.connection_check_analysis).setVisibility(0);
                findViewById(R.id.connection_check_wiringcheckonline).setVisibility(8);
                findViewById(R.id.connection_check_powerquadrant).setVisibility(8);
                return;
            default:
                return;
        }
    }
}
