package com.clou.rs350.ui.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.clou.rs350.R;
import com.clou.rs350.db.model.sequence.VisualItem;

import java.util.List;

public class VisualRecordItemAdapter extends BaseAdapter {
    private Context context;
    List<VisualItem> m_Data;

    public VisualRecordItemAdapter(Context context2) {
        this.context = context2;
    }

    public void refresh(List<VisualItem> data) {
        this.m_Data = data;
        notifyDataSetChanged();
    }

    public int getCount() {
        if (this.m_Data != null) {
            return this.m_Data.size();
        }
        return 0;
    }

    public Object getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return 0;
    }

    public List<VisualItem> getData() {
        return this.m_Data;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = View.inflate(this.context, R.layout.adapter_visual_record_item, null);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        holder.showValue(position, this.m_Data.get(position));
        return convertView;
    }

    class ViewHolder {
        int m_Index;
        TextView txt_Activites;
        TextView txt_Index;
        TextView txt_Result;

        ViewHolder(View v) {
            this.txt_Index = (TextView) v.findViewById(R.id.txt_index);
            this.txt_Activites = (TextView) v.findViewById(R.id.txt_activities);
            this.txt_Result = (TextView) v.findViewById(R.id.txt_result);
        }

        /* access modifiers changed from: package-private */
        public void showValue(int index, VisualItem data) {
            this.m_Index = index;
            this.txt_Index.setText(new StringBuilder(String.valueOf(index + 1)).toString());
            this.txt_Activites.setText(data.Activities);
            this.txt_Result.setText(data.Result.s_Value);
        }
    }
}
