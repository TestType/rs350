package com.clou.rs350.ui.view;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PathEffect;
import android.util.AttributeSet;

import com.clou.rs350.R;
import com.clou.rs350.db.model.DBDataModel;
import com.itextpdf.text.pdf.ColumnText;

public class MyRecordView extends MyZoomView {
    private static final String TAG = "MyRecordView";
    private RecordData[] Data;
    private PathEffect Effects = null;
    boolean isfirst = true;
    private Context mContext;
    private Paint myPaint = null;
    private Paint paint = null;
    private float x = 30.0f;
    private float x_length = ColumnText.GLOBAL_SPACE_CHAR_RATIO;
    private float y = 30.0f;
    private float y_height = ColumnText.GLOBAL_SPACE_CHAR_RATIO;

    public MyRecordView(Context context) {
        super(context);
    }

    public class RecordData {
        DBDataModel Data;
        String Mark;

        public RecordData() {
        }
    }

    public MyRecordView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        initData();
        this.mContext = context;
    }

    public MyRecordView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.mContext = context;
    }

    @Override // com.clou.rs350.ui.view.MyZoomView
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        float width = getScaleWidth();
        float height = getScaleHeight();
        this.x = width / 15.0f;
        this.y = height / 15.0f;
        this.y_height = height - (this.y * 2.0f);
        this.x_length = width - (this.x * 2.0f);
        if (this.isfirst) {
            this.myPaint = new Paint();
            this.myPaint.setColor(-1);
            this.myPaint.setAntiAlias(true);
            this.myPaint.setStrokeWidth(2.0f);
            this.Effects = this.myPaint.getPathEffect();
            this.isfirst = false;
        }
        drawVerticalLine();
    }

    private void initData() {
    }

    private void drawVerticalLine() {
        this.myPaint.setColor(-1);
        this.myPaint.setPathEffect(this.Effects);
        drawLine(this.x, this.y, this.x, this.y_height + this.y, this.myPaint);
        drawLine(this.x, (this.y_height / 2.0f) + this.y, this.x_length + this.x, (this.y_height / 2.0f) + this.y, this.myPaint);
        drawLine(this.x, this.y, this.x - 10.0f, this.y + 10.0f, this.myPaint);
        drawLine(this.x, this.y, this.x + 10.0f, this.y + 10.0f, this.myPaint);
        float textHight = getValue(R.dimen.sp_10, 17.0f);
        this.myPaint.setTextSize(textHight);
        drawText("t", (this.x + this.x_length) - 10.0f, this.y + (this.y_height / 2.0f) + textHight, this.myPaint);
    }

    private synchronized void drawLine(Canvas canvas, int color, PathEffect effects, float startX, float startY, float stopX, float stopY) {
        if (this.paint == null) {
            this.paint = new Paint();
            this.paint.setStyle(Paint.Style.STROKE);
            this.paint.setStrokeWidth(2.0f);
        }
        this.paint.setColor(color);
        Path path = new Path();
        path.moveTo(startX, startY);
        path.lineTo(stopX, stopY);
        this.paint.setPathEffect(effects);
        canvas.drawPath(path, this.paint);
    }

    private Double getMax(Double[] array) {
        Double max = Double.valueOf(Math.abs(array[0].doubleValue()));
        for (int i = 0; i < array.length - 1; i++) {
            if (Math.abs(array[i].doubleValue()) > max.doubleValue()) {
                max = Double.valueOf(Math.abs(array[i].doubleValue()));
            }
        }
        return max;
    }

    public void refreshData() {
        postInvalidate();
    }
}
