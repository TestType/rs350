package com.clou.rs350.ui.view.control;

import android.content.Context;
import android.graphics.Color;
import android.view.View;
import com.clou.rs350.Preferences;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ViewManager {
    private static final String TAG = "ViewManager";
    public static final int TYPE_BUTTON = 4;
    public static final int TYPE_CONTENT = 2;
    public static final int TYPE_FONT = 5;
    public static final int TYPE_STATUS = 1;
    public static final int TYPE_SUB_CONTENT = 3;
    private static ViewManager manager = new ViewManager();
    private Context context;
    private Map<Integer, List<View>> viewMap = new HashMap();

    public static ViewManager getInstance() {
        return manager;
    }

    private ViewManager() {
    }

    public void setContext(Context context2) {
        this.context = context2;
    }

    public void addControlView(int type, View view) {
        if (!this.viewMap.containsKey(Integer.valueOf(type))) {
            this.viewMap.put(Integer.valueOf(type), new ArrayList());
        }
        if (!this.viewMap.get(Integer.valueOf(type)).contains(view)) {
            this.viewMap.get(Integer.valueOf(type)).add(view);
        }
    }

    public void removeControlView(int type, View view) {
        if (this.viewMap.containsKey(Integer.valueOf(type)) && this.viewMap.get(Integer.valueOf(type)).contains(view)) {
            this.viewMap.get(Integer.valueOf(type)).remove(view);
        }
    }

    public void showBackground() {
        int defaultStatuColor = Color.parseColor("#F97A22");
        int defaultContentColor = Color.parseColor("#0080C0");
        int defaultSubContentColor = Color.parseColor("#1D2E39");
        int defaultButtonColor = Color.parseColor("#F97A22");
        int defaultFontColor = Color.parseColor("#FFFFFF");
        int statuColor = Preferences.getInt("statuColor", defaultStatuColor);
        int contentColor = Preferences.getInt("contentColor", defaultContentColor);
        int subContentColor = Preferences.getInt("subContentColor", defaultSubContentColor);
        int buttonColor = Preferences.getInt("buttonColor", defaultButtonColor);
        Preferences.getInt("fontColor", defaultFontColor);
        for (Integer type : this.viewMap.keySet()) {
            List<View> views = this.viewMap.get(type);
            if (type.intValue() == 1) {
                setColor(statuColor, views);
            } else if (type.intValue() == 2) {
                setColor(contentColor, views);
            } else if (type.intValue() == 3) {
                setColor(subContentColor, views);
            } else if (type.intValue() == 4) {
                setColor(buttonColor, views);
            }
        }
    }

    private void setColor(int color, List<View> views) {
        for (View view : views) {
            if (view instanceof MyButton) {
                ((MyButton) view).setShapColor(color);
            } else {
                view.setBackgroundColor(color);
            }
        }
    }
}
