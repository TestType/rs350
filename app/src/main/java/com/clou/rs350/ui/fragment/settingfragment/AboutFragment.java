package com.clou.rs350.ui.fragment.settingfragment;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.clou.rs350.CLApplication;
import com.clou.rs350.R;
import com.clou.rs350.callback.PasswordCallback;
import com.clou.rs350.db.model.StandardInfo;
import com.clou.rs350.device.MeterBaseDevice;
import com.clou.rs350.handler.HandlerSwitchView;
import com.clou.rs350.model.ClouData;
import com.clou.rs350.ui.dialog.HintDialog;
import com.clou.rs350.ui.dialog.HintDialog2;
import com.clou.rs350.ui.dialog.PasswordDialog;
import com.clou.rs350.ui.dialog.ProgressDialog;
import com.clou.rs350.ui.dialog.UpdateDialog;
import com.clou.rs350.ui.fragment.BaseFragment;
import com.clou.rs350.ui.view.infoview.StandardInfoView;
import com.clou.rs350.utils.DeviceUtil;
import com.clou.rs350.version.IUpdateNewVersionCallback;
import com.clou.rs350.version.UpdateUtils;
import com.clou.rs350.version.UpdateVersionManager;
import com.clou.rs350.version.VersionInfo;
import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.view.annotation.ViewInject;

import java.io.File;
import java.io.IOException;
import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

public class AboutFragment extends BaseFragment implements View.OnClickListener, HandlerSwitchView.IOnSelectCallback {
    @ViewInject(R.id.btn_history)
    private TextView btn_History;
    @ViewInject(R.id.btn_app_update)
    private TextView btn_app_update;
    @ViewInject(R.id.btn_hardware_update)
    private TextView btn_hardware_update;
    private IUpdateNewVersionCallback callback = new IUpdateNewVersionCallback() {
        /* class com.clou.rs350.ui.fragment.settingfragment.AboutFragment.AnonymousClass1 */

        @Override // com.clou.rs350.version.IUpdateNewVersionCallback
        public void onNewVersionCallback(VersionInfo newVersion) {
            int visible1;
            int visible2 = 0;
            AboutFragment.this.m_VersionInfo = newVersion;
            boolean isApkNewVersionExist = UpdateUtils.isApkNewVersionExist(AboutFragment.this.m_Context, newVersion);
            boolean isHardwareVersionExist = UpdateUtils.isHardwareNewVersionExist(newVersion);
            if (isApkNewVersionExist) {
                visible1 = 0;
            } else {
                visible1 = 8;
            }
            if (!isHardwareVersionExist) {
                visible2 = 8;
            }
            AboutFragment.this.red_apk_update.setVisibility(visible1);
            AboutFragment.this.red_hardware_update.setVisibility(visible2);
            AboutFragment.this.btn_app_update.setEnabled(isApkNewVersionExist);
            AboutFragment.this.btn_hardware_update.setEnabled(isHardwareVersionExist);
        }
    };
    @ViewInject(R.id.layout_basic_info)
    private LinearLayout layout_Basic_Info;
    @ViewInject(R.id.layout_history)
    private LinearLayout layout_History;
    @ViewInject(R.id.layout_standard_info)
    private LinearLayout layout_Standard_Info;
    private int m_LayoutIndex = 0;
    private StandardInfoView m_StandardInfoView;
    private VersionInfo m_VersionInfo;
    @ViewInject(R.id.red_apk_update)
    private ImageView red_apk_update;
    @ViewInject(R.id.red_hardware_update)
    private ImageView red_hardware_update;
    private HandlerSwitchView switchView;
    @ViewInject(R.id.about_appversion)
    private TextView txt_AppVersion;
    @ViewInject(R.id.about_calibratedate)
    private TextView txt_CalibrateDate;
    @ViewInject(R.id.about_firmwareversion)
    private TextView txt_FirmwareVersion;
    @ViewInject(R.id.about_hardwareclass)
    private TextView txt_HardwareClass;
    @ViewInject(R.id.about_hardwareserialno)
    private TextView txt_HardwareSerialNo;
    @ViewInject(R.id.about_hardware_version)
    private TextView txt_Hardware_Version;
    @ViewInject(R.id.about_manufacturer)
    private TextView txt_Manufacturer;
    private UpdateVersionManager updateVersionManager;

    @Override // android.support.v4.app.Fragment
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        this.fragmentView = inflater.inflate(R.layout.fragment_about, (ViewGroup) null);
        ViewUtils.inject(this, this.fragmentView);
        return this.fragmentView;
    }

    @Override // android.support.v4.app.Fragment, com.clou.rs350.ui.fragment.BaseFragment
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initView();
        initData();
        checkNewVersion();
    }

    private void initView() {
        List<View> tabViews = new ArrayList<>();
        tabViews.add(findViewById(R.id.btn_info));
        tabViews.add(findViewById(R.id.btn_standard_info));
        tabViews.add(findViewById(R.id.btn_history));
        this.switchView = new HandlerSwitchView(this.m_Context);
        this.switchView.setTabViews(tabViews);
        this.switchView.boundClick();
        this.switchView.setOnTabChangedCallback(this);
        this.switchView.selectView(0);
        this.m_StandardInfoView = new StandardInfoView(this.m_Context, this.layout_Standard_Info);
    }

    @Override // android.support.v4.app.Fragment, com.clou.rs350.ui.fragment.BaseFragment
    public void onResume() {
        super.onResume();
        showData();
    }

    @Override // android.support.v4.app.Fragment, com.clou.rs350.ui.fragment.BaseFragment
    public void onPause() {
        super.onPause();
    }

    private void showData() {
        StandardInfo data = ClouData.getInstance().getStandardInfo();
        this.txt_HardwareSerialNo.setText(data.StandardSerialNo);
        this.txt_FirmwareVersion.setText(data.FirmwareVersion);
        this.txt_Hardware_Version.setText(String.valueOf(data.MainBoardVersion) + "," + data.MeasureBoardVersion + "," + data.NeutralBoardVersion);
        this.txt_CalibrateDate.setText(data.getCalibrateDate());
        this.txt_AppVersion.setText(data.AppVersion);
        this.txt_HardwareClass.setText(data.Accuracy);
        this.txt_Manufacturer.setText(data.Manufacturer);
        this.m_StandardInfoView.showData();
    }

    private void initData() {
        this.btn_app_update.setOnClickListener(this);
        this.btn_app_update.setEnabled(false);
        this.btn_hardware_update.setOnClickListener(this);
        this.btn_hardware_update.setEnabled(false);
        this.txt_AppVersion.setOnClickListener(this);
    }

    private void checkNewVersion() {
        this.updateVersionManager = UpdateVersionManager.getInstance(this.m_Context);
        this.updateVersionManager.getNewVersion(this.callback);
    }

    private void appUpdate() {
        UpdateDialog builder = new UpdateDialog(getActivity());
        builder.setCancelable(false);
        builder.show();
        builder.setPositiveButton(R.string.text_ok, new View.OnClickListener() {
            /* class com.clou.rs350.ui.fragment.settingfragment.AboutFragment.AnonymousClass2 */

            public void onClick(View v) {
                final ProgressDialog pd = new ProgressDialog(AboutFragment.this.getActivity());
                pd.setCancelable(false);
                pd.show();
                pd.setTitle(R.string.text_start_download);
                new Thread() {
                    /* class com.clou.rs350.ui.fragment.settingfragment.AboutFragment.AnonymousClass2.AnonymousClass1 */

                    public void run() {
                        File file = UpdateUtils.download(AboutFragment.this.m_VersionInfo.getApkUrl(), new File(AboutFragment.this.getActivity().getCacheDir(), "temp.apk").getAbsolutePath(), pd);
                        if (file == null) {
                            AboutFragment.this.getActivity().runOnUiThread(new Runnable() {
                                /* class com.clou.rs350.ui.fragment.settingfragment.AboutFragment.AnonymousClass2.AnonymousClass1.AnonymousClass1 */

                                public void run() {
                                    new HintDialog(AboutFragment.this.getActivity(), (int) R.string.text_download_fail).show();
                                }
                            });
                        } else {
                            try {
                                Runtime.getRuntime().exec(new String[]{"chmod", "777", file.getPath()});
                            } catch (IOException e1) {
                                e1.printStackTrace();
                            }
                            try {
                                Thread.sleep(2000);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                            if (pd.isShowing()) {
                                UpdateUtils.installApplication(AboutFragment.this.getActivity(), file);
                                AboutFragment.this.getActivity().runOnUiThread(new Runnable() {
                                    /* class com.clou.rs350.ui.fragment.settingfragment.AboutFragment.AnonymousClass2.AnonymousClass1.AnonymousClass2 */

                                    public void run() {
                                        AboutFragment.this.resetRedPoint();
                                    }
                                });
                            } else {
                                return;
                            }
                        }
                        if (pd.isShowing()) {
                            pd.dismiss();
                        }
                    }
                }.start();
            }
        });
        builder.setNegativeButton(R.string.text_cancel, null);
        builder.setTitle(R.string.text_app_update);
        builder.setDescript(this.m_VersionInfo.getVersionDescript());
    }

    private void firmwareUpdate() {
        UpdateDialog builder = new UpdateDialog(getActivity());
        builder.setCancelable(false);
        builder.show();
        builder.setPositiveButton(R.string.text_ok, new View.OnClickListener() {
            /* class com.clou.rs350.ui.fragment.settingfragment.AboutFragment.AnonymousClass3 */

            public void onClick(View v) {
                final ProgressDialog pd = new ProgressDialog(AboutFragment.this.getActivity());
                pd.setCancelable(false);
                pd.show();
                pd.setTitle(R.string.text_start_download);
                new Thread() {
                    /* class com.clou.rs350.ui.fragment.settingfragment.AboutFragment.AnonymousClass3.AnonymousClass1 */

                    public void run() {
                        File file = UpdateUtils.download(AboutFragment.this.m_VersionInfo.getFirmwareUrl(), new File(String.valueOf(CLApplication.app.getDownloadPath()) + "/", "RS350.bin").getAbsolutePath(), pd);
                        if (file == null) {
                            AboutFragment.this.getActivity().runOnUiThread(new Runnable() {
                                /* class com.clou.rs350.ui.fragment.settingfragment.AboutFragment.AnonymousClass3.AnonymousClass1.AnonymousClass1 */

                                public void run() {
                                    new HintDialog(AboutFragment.this.getActivity(), (int) R.string.text_download_fail).show();
                                }
                            });
                        } else {
                            CLApplication.app.refreshPath(CLApplication.app.getDownloadPath());
                            try {
                                Runtime.getRuntime().exec(new String[]{"chmod", "777", file.getPath()});
                            } catch (IOException e1) {
                                e1.printStackTrace();
                            }
                            try {
                                Thread.sleep(2000);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                            if (pd.isShowing()) {
                                AboutFragment.this.getActivity().runOnUiThread(new Runnable() {
                                    /* class com.clou.rs350.ui.fragment.settingfragment.AboutFragment.AnonymousClass3.AnonymousClass1.AnonymousClass2 */

                                    public void run() {
                                        new HintDialog2(AboutFragment.this.getActivity(), R.string.text_firmware_update, R.string.text_update_firmware).show();
                                    }
                                });
                            } else {
                                return;
                            }
                        }
                        if (pd.isShowing()) {
                            pd.dismiss();
                        }
                    }
                }.start();
            }
        });
        builder.setNegativeButton(R.string.text_cancel, null);
        builder.setTitle(R.string.text_firmware_update);
        builder.setDescript(this.m_VersionInfo.getFirmwareDescript());
    }

    public void resetRedPoint() {
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.about_appversion:
                Toast.makeText(this.m_Context, String.valueOf(DeviceUtil.getWidthMaxDp(this.m_Context)) + "," + DeviceUtil.getHeightMaxDp(this.m_Context), 0).show();
                return;
            case R.id.about_hardwareclass:
            case R.id.about_manufacturer:
            case R.id.red_apk_update:
            default:
                return;
            case R.id.btn_app_update:
                appUpdate();
                return;
            case R.id.btn_hardware_update:
                firmwareUpdate();
                return;
        }
    }

    @Override // com.clou.rs350.ui.fragment.BaseFragment, com.clou.rs350.manager.MessageManager.IMessageListener
    public void onReceiveMessage(MeterBaseDevice Device) {
        showData();
    }

    @Override // com.clou.rs350.handler.HandlerSwitchView.IOnSelectCallback
    public void onSelectedChanged(final int index) {
        if (index != 1 || 101 == ClouData.getInstance().getOperatorInfo().Authority) {
            showView(index);
            return;
        }
        String sPassword = new SimpleDateFormat("yyyyMMdd").format(new Date(System.currentTimeMillis())).toString();
        this.messageManager.GetAllowAdjustFlag();
        PasswordDialog passwordDialog = new PasswordDialog(this.m_Context, sPassword, new PasswordCallback() {
            /* class com.clou.rs350.ui.fragment.settingfragment.AboutFragment.AnonymousClass4 */

            @Override // com.clou.rs350.callback.PasswordCallback
            public void onPasswordIsMistake() {
            }

            @Override // com.clou.rs350.callback.PasswordCallback
            public void onPasswordIsCorrect() {
                AboutFragment.this.showView(index);
            }
        });
        passwordDialog.setOnDismiss(new DialogInterface.OnDismissListener() {
            /* class com.clou.rs350.ui.fragment.settingfragment.AboutFragment.AnonymousClass5 */

            public void onDismiss(DialogInterface dialog) {
                AboutFragment.this.switchView.selectView(AboutFragment.this.m_LayoutIndex);
            }
        });
        passwordDialog.show();
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private void showView(int index) {
        int i;
        int i2;
        int i3 = 0;
        this.m_LayoutIndex = index;
        LinearLayout linearLayout = this.layout_Basic_Info;
        if (index == 0) {
            i = 0;
        } else {
            i = 8;
        }
        linearLayout.setVisibility(i);
        LinearLayout linearLayout2 = this.layout_Standard_Info;
        if (index == 1) {
            i2 = 0;
        } else {
            i2 = 8;
        }
        linearLayout2.setVisibility(i2);
        LinearLayout linearLayout3 = this.layout_History;
        if (index != 2) {
            i3 = 8;
        }
        linearLayout3.setVisibility(i3);
    }

    @Override // com.clou.rs350.handler.HandlerSwitchView.IOnSelectCallback
    public void onClickView(View view) {
    }
}
