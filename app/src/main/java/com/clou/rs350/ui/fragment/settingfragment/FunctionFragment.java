package com.clou.rs350.ui.fragment.settingfragment;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Process;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.clou.rs350.R;
import com.clou.rs350.callback.PasswordCallback;
import com.clou.rs350.db.model.StandardInfo;
import com.clou.rs350.device.MeterBaseDevice;
import com.clou.rs350.model.ClouData;
import com.clou.rs350.model.SettingFunction;
import com.clou.rs350.ui.dialog.HintDialog;
import com.clou.rs350.ui.dialog.PasswordDialog;
import com.clou.rs350.ui.fragment.BaseFragment;
import com.clou.rs350.ui.view.control.SwitchView;
import com.clou.rs350.utils.OtherUtils;
import com.lidroid.xutils.ViewUtils;

import java.text.DecimalFormat;

public class FunctionFragment extends BaseFragment implements View.OnClickListener {
    private int m_Authority = 0;
    private SwitchView m_CNVersionSwitch;
    private View m_DigitalMeterTestingLayout;
    private SwitchView m_DigitalMeterTestingSwitch;
    private SettingFunction m_SettingFunction;
    private StandardInfo m_StandardInfo;
    private SwitchView m_TestSequenceSwitch;

    public void onClick(View v) {
        v.getId();
    }

    @Override // android.support.v4.app.Fragment, com.clou.rs350.ui.fragment.BaseFragment
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initData();
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private void setTestSequence(boolean flag) {
        boolean z = true;
        this.m_StandardInfo = ClouData.getInstance().getStandardInfo();
        if (flag) {
            this.m_StandardInfo.Function |= 1;
        } else {
            this.m_StandardInfo.Function &= 0;
        }
        this.messageManager.SetStandardInfo(this.m_StandardInfo);
        SettingFunction settingFunction = this.m_SettingFunction;
        if (1 != (this.m_StandardInfo.Function & 1)) {
            z = false;
        }
        settingFunction.TestSequence = z;
        this.m_TestSequenceSwitch.show(flag);
    }

    @Override // android.support.v4.app.Fragment
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        this.fragmentView = inflater.inflate(R.layout.fragment_function, (ViewGroup) null);
        ViewUtils.inject(this, this.fragmentView);
        this.m_TestSequenceSwitch = new SwitchView(this.m_Context, findViewById(R.id.switch_test_sequence));
        this.m_TestSequenceSwitch.setOnSetFlag(new SwitchView.SetFlag() {
            /* class com.clou.rs350.ui.fragment.settingfragment.FunctionFragment.AnonymousClass1 */

            @Override // com.clou.rs350.ui.view.control.SwitchView.SetFlag
            public void setFlag(final boolean flag) {
                byte[] tmpPassword;
                if (!FunctionFragment.this.messageManager.isConnected()) {
                    FunctionFragment.this.m_TestSequenceSwitch.show(!flag);
                    new HintDialog(FunctionFragment.this.m_Context, (int) R.string.text_you_need_connect_device).show();
                } else if (101 == FunctionFragment.this.m_Authority) {
                    FunctionFragment.this.setTestSequence(flag);
                } else if (OtherUtils.isEmpty(FunctionFragment.this.m_StandardInfo.StandardSerialNo)) {
                    new HintDialog(FunctionFragment.this.m_Context, (int) R.string.text_you_need_connect_device).show();
                } else {
                    long lPassword = 1;
                    for (byte b : FunctionFragment.this.m_StandardInfo.StandardSerialNo.getBytes()) {
                        lPassword = ((lPassword * ((long) b)) % 100000000) + 11;
                    }
                    PasswordDialog passwordDialog = new PasswordDialog(FunctionFragment.this.m_Context, new DecimalFormat("00000000").format(lPassword), new PasswordCallback() {
                        /* class com.clou.rs350.ui.fragment.settingfragment.FunctionFragment.AnonymousClass1.AnonymousClass1 */

                        @Override // com.clou.rs350.callback.PasswordCallback
                        public void onPasswordIsMistake() {
                        }

                        @Override // com.clou.rs350.callback.PasswordCallback
                        public void onPasswordIsCorrect() {
                            FunctionFragment.this.setTestSequence(flag);
                        }
                    });
                    passwordDialog.setOnDismiss(new DialogInterface.OnDismissListener() {
                        /* class com.clou.rs350.ui.fragment.settingfragment.FunctionFragment.AnonymousClass1.AnonymousClass2 */

                        public void onDismiss(DialogInterface dialog) {
                            FunctionFragment.this.m_TestSequenceSwitch.show(!flag);
                        }
                    });
                    passwordDialog.show();
                }
            }
        });
        this.m_CNVersionSwitch = new SwitchView(this.m_Context, findViewById(R.id.switch_cn_version));
        this.m_CNVersionSwitch.setOnSetFlag(new SwitchView.SetFlag() {
            /* class com.clou.rs350.ui.fragment.settingfragment.FunctionFragment.AnonymousClass2 */

            @Override // com.clou.rs350.ui.view.control.SwitchView.SetFlag
            public void setFlag(boolean flag) {
                FunctionFragment.this.m_SettingFunction.CNVersion = flag;
                FunctionFragment.this.m_SettingFunction.saveSetting();
                HintDialog tmpDialog = new HintDialog(FunctionFragment.this.m_Context);
                tmpDialog.setOnClick(new View.OnClickListener() {
                    /* class com.clou.rs350.ui.fragment.settingfragment.FunctionFragment.AnonymousClass2.AnonymousClass1 */

                    public void onClick(View v) {
                        FunctionFragment.this.restartApplication();
                    }
                });
                tmpDialog.show();
                tmpDialog.setTitle(R.string.text_restart_app);
            }
        });
        this.m_DigitalMeterTestingSwitch = new SwitchView(this.m_Context, findViewById(R.id.switch_digital_meter_testing));
        this.m_DigitalMeterTestingSwitch.setOnSetFlag(new SwitchView.SetFlag() {
            /* class com.clou.rs350.ui.fragment.settingfragment.FunctionFragment.AnonymousClass3 */

            @Override // com.clou.rs350.ui.view.control.SwitchView.SetFlag
            public void setFlag(boolean flag) {
                FunctionFragment.this.m_SettingFunction.DigitalMeterTesting = flag;
                FunctionFragment.this.m_SettingFunction.saveSetting();
                HintDialog tmpDialog = new HintDialog(FunctionFragment.this.m_Context);
                tmpDialog.setOnClick(new View.OnClickListener() {
                    /* class com.clou.rs350.ui.fragment.settingfragment.FunctionFragment.AnonymousClass3.AnonymousClass1 */

                    public void onClick(View v) {
                        FunctionFragment.this.restartApplication();
                    }
                });
                tmpDialog.show();
                tmpDialog.setTitle(R.string.text_restart_app);
            }
        });
        this.m_DigitalMeterTestingLayout = findViewById(R.id.layout_digital_meter_testing);
        return this.fragmentView;
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private void restartApplication() {
        Intent intent = this.m_Context.getPackageManager().getLaunchIntentForPackage(this.m_Context.getPackageName());
        intent.setFlags(268468224);
        startActivity(intent);
        Process.killProcess(Process.myPid());
        System.exit(0);
    }

    @Override // android.support.v4.app.Fragment, com.clou.rs350.ui.fragment.BaseFragment
    public void onPause() {
        this.m_SettingFunction.saveSetting();
        super.onPause();
    }

    @Override // android.support.v4.app.Fragment, com.clou.rs350.ui.fragment.BaseFragment
    public void onResume() {
        this.m_StandardInfo = ClouData.getInstance().getStandardInfo();
        showData();
        super.onResume();
    }

    private void showData() {
        this.m_TestSequenceSwitch.show(this.m_SettingFunction.TestSequence);
        this.m_CNVersionSwitch.show(this.m_SettingFunction.CNVersion);
        this.m_DigitalMeterTestingSwitch.show(this.m_SettingFunction.DigitalMeterTesting);
        this.m_DigitalMeterTestingLayout.setVisibility(this.m_SettingFunction.CNVersion ? 0 : 8);
    }

    private void initData() {
        this.m_SettingFunction = ClouData.getInstance().getSettingFunction();
        this.m_Authority = ClouData.getInstance().getOperatorInfo().Authority;
    }

    @Override // com.clou.rs350.ui.fragment.BaseFragment, com.clou.rs350.manager.MessageManager.IMessageListener
    public void onReceiveMessage(MeterBaseDevice Device) {
        showData();
        super.onReceiveMessage(Device);
    }
}
