package com.clou.rs350.ui.view;

import android.content.Context;
import android.graphics.PointF;
import android.graphics.Rect;
import android.hardware.Camera;
import android.util.Log;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.ViewGroup;
import com.itextpdf.text.pdf.ColumnText;
import com.itextpdf.text.pdf.PdfObject;
import com.itextpdf.text.pdf.codec.wmf.MetaDo;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class CameraSurfacePreview extends SurfaceView implements SurfaceHolder.Callback, Camera.AutoFocusCallback {
    private Camera mCamera;
    private SurfaceHolder mHolder = getHolder();

    public CameraSurfacePreview(Context context) {
        super(context);
        this.mHolder.addCallback(this);
        this.mHolder.setType(3);
    }

    private PointF getLocation(MotionEvent event) {
        float x = ColumnText.GLOBAL_SPACE_CHAR_RATIO;
        float y = ColumnText.GLOBAL_SPACE_CHAR_RATIO;
        int count = event.getPointerCount();
        for (int i = 0; i < count; i++) {
            x += event.getX(i);
            y += event.getY(i);
        }
        return new PointF(x / ((float) count), y / ((float) count));
    }

    public boolean onTouchEvent(MotionEvent event) {
        switch (event.getAction()) {
            case 0:
            case 5:
            case MetaDo.META_SETRELABS /*{ENCODED_INT: 261}*/:
                PointF location = getLocation(event);
                focusOnTouch((int) location.x, (int) location.y);
                return true;
            case 3:
                Log.i(PdfObject.NOTHING, PdfObject.NOTHING);
                return true;
            default:
                return true;
        }
    }

    private void focusOnTouch(int x, int y) {
        Rect rect = new Rect(x - 100, y - 100, x + 100, y + 100);
        int left = ((rect.left * 2000) / getWidth()) - 1000;
        int top = ((rect.top * 2000) / getHeight()) - 1000;
        int right = ((rect.right * 2000) / getWidth()) - 1000;
        int bottom = ((rect.bottom * 2000) / getHeight()) - 1000;
        if (left < -1000) {
            left = -1000;
        }
        if (top < -1000) {
            top = -1000;
        }
        if (right > 1000) {
            right = 1000;
        }
        if (bottom > 1000) {
            bottom = 1000;
        }
        focusOnRect(new Rect(left, top, right, bottom));
    }

    private void focusOnRect(Rect rect) {
        if (this.mCamera != null) {
            Camera.Parameters parameters = this.mCamera.getParameters();
            parameters.setFocusMode("auto");
            if (parameters.getMaxNumFocusAreas() > 0) {
                List<Camera.Area> focusAreas = new ArrayList<>();
                focusAreas.add(new Camera.Area(rect, 1000));
                parameters.setFocusAreas(focusAreas);
            }
            this.mCamera.cancelAutoFocus();
            this.mCamera.setParameters(parameters);
            this.mCamera.autoFocus(this);
        }
    }

    public void surfaceCreated(SurfaceHolder holder) {
        try {
            this.mCamera = Camera.open();
            Camera.Size cSize = this.mCamera.getParameters().getPreviewSize();
            float scale = (((float) cSize.width) * 1.0f) / ((float) cSize.height);
            ViewGroup.LayoutParams vSize = getLayoutParams();
            int tmpWidth = getWidth();
            int tmpHeight = getHeight();
            if (((float) tmpWidth) / scale > ((float) tmpHeight)) {
                vSize.height = tmpHeight;
                vSize.width = (int) (((float) tmpHeight) * scale);
            } else {
                vSize.width = tmpWidth;
                vSize.height = (int) (((float) tmpWidth) / scale);
            }
            setLayoutParams(vSize);
            this.mCamera.setPreviewDisplay(holder);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void surfaceChanged(SurfaceHolder holder, int format, int w, int h) {
        try {
            this.mCamera.startPreview();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void surfaceDestroyed(SurfaceHolder holder) {
        if (this.mCamera != null) {
            this.mCamera.stopPreview();
            this.mCamera.release();
            this.mCamera = null;
        }
    }

    public void lightControl() {
        Camera.Parameters parameters = this.mCamera.getParameters();
        if (parameters.getFlashMode() != null) {
            if (parameters.getFlashMode().equals("torch")) {
                parameters.setFlashMode("off");
                this.mCamera.setParameters(parameters);
            } else if (parameters.getFlashMode().equals("off")) {
                parameters.setFlashMode("torch");
                this.mCamera.setParameters(parameters);
            }
        }
    }

    public void takePicture(Camera.PictureCallback imageCallback) {
        this.mCamera.takePicture(null, null, imageCallback);
    }

    public boolean hasFlash() {
        if (this.mCamera == null) {
            return false;
        }
        Camera.Parameters parameters = this.mCamera.getParameters();
        if (parameters.getFlashMode() == null) {
            return false;
        }
        List<String> supportedFlashModes = parameters.getSupportedFlashModes();
        return supportedFlashModes != null && !supportedFlashModes.isEmpty() && (supportedFlashModes.size() != 1 || !supportedFlashModes.get(0).equals("off"));
    }

    public void onAutoFocus(boolean success, Camera camera) {
    }
}
