package com.clou.rs350.ui.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.clou.rs350.R;
import com.clou.rs350.db.dao.BasicMeasurementDao;
import com.clou.rs350.db.dao.HarmonicMeasurementDao;
import com.clou.rs350.db.dao.LTMDao;
import com.clou.rs350.db.dao.WaveMeasurementDao;
import com.clou.rs350.db.dao.WiringCheckDao;
import com.clou.rs350.db.model.BasicMeasurement;
import com.clou.rs350.db.model.HarmonicMeasurement;
import com.clou.rs350.db.model.LTM;
import com.clou.rs350.db.model.WaveMeasurement;
import com.clou.rs350.db.model.WiringCheckMeasurement;
import com.clou.rs350.device.MeterBaseDevice;
import com.clou.rs350.handler.HandlerSwitchView;
import com.clou.rs350.model.ClouData;
import com.clou.rs350.task.ISleepCallback;
import com.clou.rs350.task.TimerThread;
import com.clou.rs350.ui.adapter.FragmentTabAdapter;
import com.clou.rs350.ui.dialog.SaveDialog;
import com.clou.rs350.ui.fragment.BaseFragment;
import com.clou.rs350.ui.fragment.realtimefragment.HarmonicFragment;
import com.clou.rs350.ui.fragment.realtimefragment.LTMFragment;
import com.clou.rs350.ui.fragment.realtimefragment.RealtimeBasicFragment;
import com.clou.rs350.ui.fragment.realtimefragment.WaveFragment;
import com.clou.rs350.ui.fragment.realtimefragment.WiringCheckCNFragment;
import com.clou.rs350.ui.fragment.realtimefragment.WiringCheckFragment;
import com.clou.rs350.ui.view.WiringAndRangeView;
import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.view.annotation.ViewInject;

import java.util.ArrayList;
import java.util.List;

public class RealTimeActivity extends BaseActivity implements View.OnClickListener, HandlerSwitchView.IOnSelectCallback {
    @ViewInject(R.id.btn_back)
    private Button backButtom;
    BasicMeasurement m_BasicValue = new BasicMeasurement();
    private boolean m_CNVersion;
    @ViewInject(R.id.setting_normal_linearlayout)
    private LinearLayout normalLayout;
    @ViewInject(R.id.btn_save)
    private Button saveButtom;
    private HandlerSwitchView switchView;
    private TimerThread timerThread;
    @ViewInject(R.id.title_textview)
    private TextView titleTextView;
    @ViewInject(R.id.layout_wiring)
    private View wiringLayout;
    private WiringAndRangeView wiringView;

    /* access modifiers changed from: protected */
    @Override // com.clou.rs350.ui.activity.BaseActivity, android.support.v4.app.FragmentActivity
    public void onCreate(Bundle savedInstanceState) {
        this.TAG = "RealTimeActivity";
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_realtime);
        setDefaultTitle(R.string.text_real_basic);
        ViewUtils.inject(this);
        initView();
        this.m_TabAdapter = new FragmentTabAdapter(this, getFragments(), R.id.content_view);
        this.m_TabAdapter.init();
    }

    private List<BaseFragment> getFragments() {
        List<BaseFragment> list = new ArrayList<>();
        list.add(new RealtimeBasicFragment(ClouData.getInstance().getBasicMeasurement()));
        list.add(new WaveFragment(ClouData.getInstance().getWaveMeasurement()));
        this.m_CNVersion = ClouData.getInstance().getSettingFunction().CNVersion;
        if (this.m_CNVersion) {
            list.add(new WiringCheckCNFragment(ClouData.getInstance().getWiringCheckMeasurement()));
        } else {
            list.add(new WiringCheckFragment(ClouData.getInstance().getWiringCheckMeasurement()));
        }
        list.add(new HarmonicFragment(ClouData.getInstance().getHarmonicMeasurement()));
        list.add(new LTMFragment(ClouData.getInstance().getLTM()));
        return list;
    }

    /* access modifiers changed from: protected */
    @Override // com.clou.rs350.ui.activity.BaseActivity, android.support.v4.app.FragmentActivity
    public void onResume() {
        super.onResume();
        startToReadRangeData();
    }

    /* access modifiers changed from: protected */
    @Override // com.clou.rs350.ui.activity.BaseActivity, android.support.v4.app.FragmentActivity
    public void onPause() {
        super.onPause();
        killThread(this.timerThread);
    }

    private void startToReadRangeData() {
        if (this.timerThread == null || this.timerThread.isStop()) {
            this.timerThread = new TimerThread(3000, new ISleepCallback() {
                /* class com.clou.rs350.ui.activity.RealTimeActivity.AnonymousClass1 */

                @Override // com.clou.rs350.task.ISleepCallback
                public void onSleepAfterToDo() {
                    RealTimeActivity.this.messageManager.getRanage();
                }
            }, "Read Range Thread");
            this.timerThread.start();
        }
    }

    private void initView() {
        List<View> tabViews = new ArrayList<>();
        tabViews.add(findViewById(R.id.basic_layout));
        tabViews.add(findViewById(R.id.wave_layout));
        tabViews.add(findViewById(R.id.wiring_check));
        tabViews.add(findViewById(R.id.harmonic_layout));
        tabViews.add(findViewById(R.id.ltm_layout));
        this.switchView = new HandlerSwitchView(this.m_Context);
        this.switchView.setTabViews(tabViews);
        this.switchView.boundClick();
        this.switchView.setOnTabChangedCallback(this);
        this.switchView.selectView(0);
        this.wiringView = new WiringAndRangeView(this, this.wiringLayout);
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_back:
                finish();
                return;
            case R.id.other_layout:
                this.normalLayout.setVisibility(View.VISIBLE);
                return;
            case R.id.btn_save:
                new SaveDialog(this.m_Context, getString(new int[]{R.string.text_realtime, R.string.text_wave, R.string.text_wiring_check, R.string.text_harmonic, R.string.text_ltm}[this.m_TabAdapter.getCurrentTab()]), new SaveDialog.ISave() {
                    /* class com.clou.rs350.ui.activity.RealTimeActivity.AnonymousClass2 */

                    @Override // com.clou.rs350.ui.dialog.SaveDialog.ISave
                    public boolean SaveMeasureData(String SerialNo, String Time) {
                        switch (RealTimeActivity.this.m_TabAdapter.getCurrentTab()) {
                            case 0:
                                BasicMeasurement basicData = ClouData.getInstance().getBasicMeasurement();
                                basicData.CustomerSerialNo = SerialNo;
                                return new BasicMeasurementDao(RealTimeActivity.this.m_Context).insertObject(basicData, Time);
                            case 1:
                                WaveMeasurement waveData = ClouData.getInstance().getWaveMeasurement();
                                waveData.SerialNo = SerialNo;
                                waveData.buildStringData();
                                return new WaveMeasurementDao(RealTimeActivity.this.m_Context).insertObject(waveData, Time);
                            case 2:
                                WiringCheckMeasurement wiringCheckData = ClouData.getInstance().getWiringCheckMeasurement();
                                wiringCheckData.CustomerSerialNo = SerialNo;
                                return new WiringCheckDao(RealTimeActivity.this.m_Context).insertObject(wiringCheckData, Time);
                            case 3:
                                HarmonicMeasurement harmonicData = ClouData.getInstance().getHarmonicMeasurement();
                                harmonicData.SerialNo = SerialNo;
                                harmonicData.buildStringData();
                                return new HarmonicMeasurementDao(RealTimeActivity.this.m_Context).insertObject(harmonicData, Time);
                            case 4:
                                LTM integrationData = ClouData.getInstance().getLTM();
                                integrationData.SerialNo = SerialNo;
                                return new LTMDao(RealTimeActivity.this.m_Context).insertObject(integrationData, Time);
                            default:
                                return false;
                        }
                    }
                }, null).show();
                return;
            case R.id.basic_layout:
                this.m_TabAdapter.checkedIndex(0);
                this.titleTextView.setText(R.string.text_real_basic);
                return;
            case R.id.wave_layout:
                this.m_TabAdapter.checkedIndex(1);
                this.titleTextView.setText(R.string.text_real_wave);
                return;
            case R.id.wiring_check:
                this.m_TabAdapter.checkedIndex(2);
                this.titleTextView.setText(R.string.text_real_wiring_check);
                return;
            case R.id.harmonic_layout:
                this.m_TabAdapter.checkedIndex(3);
                this.titleTextView.setText(R.string.text_real_harmonic);
                return;
            case R.id.ltm_layout:
                this.m_TabAdapter.checkedIndex(4);
                this.titleTextView.setText(R.string.text_real_ltm);
                return;
            default:
                return;
        }
    }

    @Override // com.clou.rs350.ui.activity.BaseActivity, com.clou.rs350.manager.MessageManager.IMessageListener
    public void onReceiveMessage(MeterBaseDevice Device) {
        this.m_BasicValue.parseRangeData(Device);
        this.wiringView.showData(this.m_BasicValue);
    }

    @Override // com.clou.rs350.handler.HandlerSwitchView.IOnSelectCallback
    public void onSelectedChanged(int index) {
    }

    @Override // com.clou.rs350.handler.HandlerSwitchView.IOnSelectCallback
    public void onClickView(View view) {
        onClick(view);
    }

    @Override // com.clou.rs350.ui.activity.BaseActivity
    public void startOrStopChangeViewStatus(int flag) {
        boolean hasStart;
        boolean z;
        boolean z2;
        boolean z3 = false;
        super.startOrStopChangeViewStatus(flag);
        if (flag == 0) {
            this.switchView.boundClick();
            this.switchView.selectCurrentView();
        } else {
            this.switchView.unBoundClick();
        }
        if (1 == flag) {
            hasStart = true;
        } else {
            hasStart = false;
        }
        Button button = this.backButtom;
        if (hasStart) {
            z = false;
        } else {
            z = true;
        }
        button.setEnabled(z);
        Button button2 = this.saveButtom;
        if (hasStart) {
            z2 = false;
        } else {
            z2 = true;
        }
        button2.setEnabled(z2);
        WiringAndRangeView wiringAndRangeView = this.wiringView;
        if (!hasStart) {
            z3 = true;
        }
        wiringAndRangeView.setEnabled(z3);
    }
}
