package com.clou.rs350.ui.view.control;

import android.content.Context;
import android.view.View;
import android.widget.LinearLayout;

import com.clou.rs350.R;

public class SwitchView implements View.OnClickListener {
    LinearLayout layout_Off;
    LinearLayout layout_On;
    Context m_Context;
    boolean m_Flag;
    SetFlag m_SetFlag;

    public interface SetFlag {
        void setFlag(boolean z);
    }

    public SwitchView(Context context, View view) {
        this.m_Context = context;
        this.layout_On = (LinearLayout) view.findViewById(R.id.layout_on);
        this.layout_On.setOnClickListener(this);
        this.layout_Off = (LinearLayout) view.findViewById(R.id.layout_off);
        this.layout_Off.setOnClickListener(this);
    }

    public void show(boolean flag) {
        int i;
        int i2 = 8;
        this.m_Flag = flag;
        LinearLayout linearLayout = this.layout_On;
        if (flag) {
            i = 0;
        } else {
            i = 8;
        }
        linearLayout.setVisibility(i);
        LinearLayout linearLayout2 = this.layout_Off;
        if (!flag) {
            i2 = 0;
        }
        linearLayout2.setVisibility(i2);
    }

    public void setOnSetFlag(SetFlag onSetFlag) {
        this.m_SetFlag = onSetFlag;
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.layout_on:
                this.m_Flag = false;
                break;
            case R.id.layout_off:
                this.m_Flag = true;
                break;
        }
        show(this.m_Flag);
        if (this.m_SetFlag != null) {
            this.m_SetFlag.setFlag(this.m_Flag);
        }
    }
}
