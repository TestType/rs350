package com.clou.rs350.ui.fragment.realtimefragment;

import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.TextView;

import com.clou.rs350.Preferences;
import com.clou.rs350.R;
import com.clou.rs350.callback.ILoadCallback;
import com.clou.rs350.db.model.HarmonicMeasurement;
import com.clou.rs350.device.MeterBaseDevice;
import com.clou.rs350.handler.HandlerSwitchView;
import com.clou.rs350.task.ISleepCallback;
import com.clou.rs350.task.MyTask;
import com.clou.rs350.task.TimerThread;
import com.clou.rs350.ui.adapter.HarmonicAdapter;
import com.clou.rs350.ui.adapter.PowerHarmonicListViewAdapter;
import com.clou.rs350.ui.adapter.VIHarmonicListViewAdapter;
import com.clou.rs350.ui.dialog.CountdownDialog;
import com.clou.rs350.ui.fragment.BaseFragment;
import com.clou.rs350.ui.view.MyHarmonicView;
import com.itextpdf.text.pdf.codec.TIFFConstants;
import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.view.annotation.ViewInject;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.ArrayList;
import java.util.List;

public class HarmonicFragment extends BaseFragment implements View.OnClickListener, HandlerSwitchView.IOnSelectCallback, CompoundButton.OnCheckedChangeListener {
    private static final String TAG = "HarmonicFragment";
    private HarmonicAdapter adapter;
    @ViewInject(R.id.harmonic_table_graph_button)
    private Button btnTabelGraph;
    @ViewInject(R.id.harmonic_table_button)
    private Button btnTable;
    @ViewInject(R.id.btn_hold)
    private Button btn_Hold;
    @ViewInject(R.id.btn_readall)
    private Button btn_Read_All;
    @ViewInject(R.id.btn_style)
    private Button btn_Style;
    @ViewInject(R.id.chk_harmonic_definition0)
    private RadioButton chk_Harmonic_Definition0;
    @ViewInject(R.id.chk_harmonic_definition1)
    private RadioButton chk_Harmonic_Definition1;
    @ViewInject(R.id.realtime_harmonic_tablelistview)
    private ListView dataListView;
    @ViewInject(R.id.harmonic_graph_layout)
    private View graphView;
    @ViewInject(R.id.harmonic_graph_view)
    private MyHarmonicView histogramView;
    @ViewInject(R.id.harmonic_graph_view1)
    private MyHarmonicView histogramView1;
    @ViewInject(R.id.harmonic_graph_view2)
    private MyHarmonicView histogramView2;
    boolean isTableFlag = false;
    private HarmonicMeasurement m_HarmonicMeasurement;
    private boolean m_Hold = false;
    CountdownDialog m_LoadingDialog;
    int m_Style = 0;
    private int m_Type = 257;
    private int m_TypeBefore = 0;
    private PowerHarmonicListViewAdapter power_adapter;
    private ListView power_listView;
    private TimerThread readDataThread;
    private HandlerSwitchView switchView;
    private HandlerSwitchView switchView_UIStyle1;
    private HandlerSwitchView switchView_UIStyle2;
    private HandlerSwitchView switchView_powerHarmonic;
    @ViewInject(R.id.harmonic_table_graph_button)
    private Button tableButton;
    @ViewInject(R.id.harmonic_layout_double)
    private View tableView;
    @ViewInject(R.id.harmonic_table_layout)
    private View tableViewUI;
    @ViewInject(R.id.realtime_harmonic_thd_text)
    private TextView textTHD;
    @ViewInject(R.id.realtime_harmonic_thd_text_current)
    private TextView textTHDCurrent;
    @ViewInject(R.id.realtime_harmonic_thd_text_voltage)
    private TextView textTHDVoltage;
    @ViewInject(R.id.harmonic_power_phase)
    private TextView titlePowerTextView;
    @ViewInject(R.id.realtime_harmonic_title)
    private TextView titleTextView;
    @ViewInject(R.id.realtime_harmonic_title_current)
    private TextView titleTextViewCurrent;
    @ViewInject(R.id.realtime_harmonic_title_voltage)
    private TextView titleTextViewVoltate;
    @ViewInject(R.id.harmonic_vi_phase)
    private TextView titleVITextView;
    private VIHarmonicListViewAdapter vi_adapter;
    private ListView vi_listView;

    public HarmonicFragment(HarmonicMeasurement Data) {
        this.m_HarmonicMeasurement = Data;
    }

    @Override // android.support.v4.app.Fragment
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        this.fragmentView = inflater.inflate(R.layout.fragment_harmonic, (ViewGroup) null);
        ViewUtils.inject(this, this.fragmentView);
        this.vi_listView = (ListView) this.fragmentView.findViewById(R.id.vi_listview);
        this.power_listView = (ListView) this.fragmentView.findViewById(R.id.power_listview);
        this.fragmentView.findViewById(R.id.btn_vi_harmonic).setOnClickListener(this);
        this.fragmentView.findViewById(R.id.btn_power_harmonic).setOnClickListener(this);
        this.fragmentView.findViewById(R.id.btn_hold).setOnClickListener(this);
        this.fragmentView.findViewById(R.id.btn_style).setOnClickListener(this);
        this.fragmentView.findViewById(R.id.btn_readall).setOnClickListener(this);
        this.fragmentView.findViewById(R.id.btn_power_graph).setOnClickListener(this);
        this.fragmentView.findViewById(R.id.harmonic_table_graph_button).setOnClickListener(this);
        this.fragmentView.findViewById(R.id.harmonic_table_button).setOnClickListener(this);
        this.fragmentView.findViewById(R.id.harmonic_voltagel1_button).setOnClickListener(this);
        this.fragmentView.findViewById(R.id.harmonic_voltagel2_button).setOnClickListener(this);
        this.fragmentView.findViewById(R.id.harmonic_voltagel3_button).setOnClickListener(this);
        this.fragmentView.findViewById(R.id.harmonic_currentl1_button).setOnClickListener(this);
        this.fragmentView.findViewById(R.id.harmonic_currentl2_button).setOnClickListener(this);
        this.fragmentView.findViewById(R.id.harmonic_currentl3_button).setOnClickListener(this);
        this.fragmentView.findViewById(R.id.harmonic_phasel1_button).setOnClickListener(this);
        this.fragmentView.findViewById(R.id.harmonic_phasel2_button).setOnClickListener(this);
        this.fragmentView.findViewById(R.id.harmonic_phasel3_button).setOnClickListener(this);
        this.chk_Harmonic_Definition0.setOnCheckedChangeListener(this);
        this.chk_Harmonic_Definition1.setOnCheckedChangeListener(this);
        return this.fragmentView;
    }

    @Override // android.support.v4.app.Fragment, com.clou.rs350.ui.fragment.BaseFragment
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        this.vi_adapter = new VIHarmonicListViewAdapter(getActivity());
        this.vi_listView.setAdapter((ListAdapter) this.vi_adapter);
        this.power_adapter = new PowerHarmonicListViewAdapter(getActivity());
        this.power_listView.setAdapter((ListAdapter) this.power_adapter);
        initView();
    }

    private void setTableOrGraph(boolean isTable) {
        if (isTable) {
            findViewById(R.id.harmonic_table_layout).setVisibility(0);
            findViewById(R.id.harmonic_graph_view).setVisibility(8);
            findViewById(R.id.harmonic_layout_double_table).setVisibility(0);
            findViewById(R.id.harmonic_layout_double_graph).setVisibility(8);
            this.btnTabelGraph.setText(getString(R.string.text_graph));
            this.btnTable.setText(getString(R.string.text_graph));
        } else {
            findViewById(R.id.harmonic_table_layout).setVisibility(8);
            findViewById(R.id.harmonic_graph_view).setVisibility(0);
            findViewById(R.id.harmonic_layout_double_table).setVisibility(8);
            findViewById(R.id.harmonic_layout_double_graph).setVisibility(0);
            this.btnTabelGraph.setText(getString(R.string.text_table));
            this.btnTable.setText(getString(R.string.text_table));
        }
        ShowValue(this.m_HarmonicMeasurement);
    }

    public void onClick(View v) {
        boolean z;
        boolean z2 = false;
        switch (v.getId()) {
            case R.id.harmonic_table_graph_button:
                if (!this.isTableFlag) {
                    z2 = true;
                }
                this.isTableFlag = z2;
                setTableOrGraph(this.isTableFlag);
                break;
            case R.id.harmonic_table_button:
                if (!this.isTableFlag) {
                    z2 = true;
                }
                this.isTableFlag = z2;
                setTableOrGraph(this.isTableFlag);
                break;
            case R.id.btn_hold:
                if (this.m_Hold) {
                    z = false;
                } else {
                    z = true;
                }
                this.m_Hold = z;
                this.btn_Hold.setText(this.m_Hold ? R.string.text_continue : R.string.text_hold);
                Button button = this.btn_Read_All;
                if (!this.m_Hold) {
                    z2 = true;
                }
                button.setEnabled(z2);
                break;
            case R.id.btn_readall:
                ReadAllValue();
                break;
            case R.id.btn_style:
                this.m_Style++;
                setStyle();
                break;
        }
        ShowValue(this.m_HarmonicMeasurement);
    }

    public void ReadAllValue() {
        killThread(this.readDataThread);
        this.messageManager.getData();
        this.messageManager.getHarmonic();
        this.m_LoadingDialog = new CountdownDialog(this.m_Context);
        this.m_LoadingDialog.showDialog(15, R.string.text_reading, new ISleepCallback() {
            /* class com.clou.rs350.ui.fragment.realtimefragment.HarmonicFragment.AnonymousClass1 */

            @Override // com.clou.rs350.task.ISleepCallback
            public void onSleepAfterToDo() {
                HarmonicFragment.this.m_LoadingDialog.dismiss();
                HarmonicFragment.this.initData();
            }
        });
    }

    private void setStyle() {
        switch (this.m_Style) {
            case 1:
                findViewById(R.id.harmonic_layout_double).setVisibility(8);
                findViewById(R.id.harmonic_graph_layout).setVisibility(0);
                this.m_Type &= 3841;
                onSelectedChanged((this.m_Type >> 8) - 1);
                break;
            default:
                findViewById(R.id.harmonic_layout_double).setVisibility(0);
                findViewById(R.id.harmonic_graph_layout).setVisibility(8);
                this.m_Style = 0;
                this.m_Type |= 3;
                break;
        }
        ShowValue(this.m_HarmonicMeasurement);
    }

    public void setType(int type) {
        this.m_Type = type;
    }

    private void initView() {
        this.adapter = new HarmonicAdapter(this.m_Context);
        this.dataListView.setAdapter((ListAdapter) this.adapter);
        List<View> tabViews = new ArrayList<>();
        tabViews.add(findViewById(R.id.btn_vi_harmonic));
        tabViews.add(findViewById(R.id.btn_power_harmonic));
        this.switchView = new HandlerSwitchView(this.m_Context);
        this.switchView.setTabViews(tabViews);
        this.switchView.boundClick();
        this.switchView.setOnTabChangedCallback(new HandlerSwitchView.IOnSelectCallback() {
            /* class com.clou.rs350.ui.fragment.realtimefragment.HarmonicFragment.AnonymousClass2 */

            @Override // com.clou.rs350.handler.HandlerSwitchView.IOnSelectCallback
            public void onSelectedChanged(int index) {
            }

            @Override // com.clou.rs350.handler.HandlerSwitchView.IOnSelectCallback
            public void onClickView(View v) {
                switch (v.getId()) {
                    case R.id.btn_vi_harmonic:
                        HarmonicFragment.this.findViewById(R.id.vi_harmonic).setVisibility(0);
                        HarmonicFragment.this.findViewById(R.id.power_harmonic).setVisibility(8);
                        HarmonicFragment.this.m_Type &= 3840;
                        if (1 == HarmonicFragment.this.m_Style) {
                            HarmonicFragment.this.m_Type++;
                        } else {
                            HarmonicFragment.this.m_Type += 3;
                        }
                        HarmonicFragment.this.m_TypeBefore = 0;
                        onSelectedChanged((HarmonicFragment.this.m_Type >> 8) - 1);
                        HarmonicFragment.this.btn_Style.setEnabled(true);
                        break;
                    case R.id.btn_power_harmonic:
                        HarmonicFragment.this.findViewById(R.id.power_harmonic).setVisibility(0);
                        HarmonicFragment.this.findViewById(R.id.vi_harmonic).setVisibility(8);
                        HarmonicFragment.this.m_TypeBefore = HarmonicFragment.this.m_Type;
                        HarmonicFragment.this.m_Type &= 3840;
                        HarmonicFragment.this.m_Type |= 28;
                        HarmonicFragment.this.btn_Style.setEnabled(false);
                        break;
                }
                HarmonicFragment.this.ShowValue(HarmonicFragment.this.m_HarmonicMeasurement);
            }
        });
        this.switchView.selectView(0);
        List<View> UIStyle1 = new ArrayList<>();
        UIStyle1.add(findViewById(R.id.harmonic_voltagel1_button));
        UIStyle1.add(findViewById(R.id.harmonic_voltagel2_button));
        UIStyle1.add(findViewById(R.id.harmonic_voltagel3_button));
        UIStyle1.add(findViewById(R.id.harmonic_currentl1_button));
        UIStyle1.add(findViewById(R.id.harmonic_currentl2_button));
        UIStyle1.add(findViewById(R.id.harmonic_currentl3_button));
        this.switchView_UIStyle1 = new HandlerSwitchView(this.m_Context);
        this.switchView_UIStyle1.setTabViews(UIStyle1);
        this.switchView_UIStyle1.boundClick();
        this.switchView_UIStyle1.setOnTabChangedCallback(this);
        this.switchView_UIStyle1.selectView(0);
        List<View> UIStyle2 = new ArrayList<>();
        UIStyle2.add(findViewById(R.id.harmonic_phasel1_button));
        UIStyle2.add(findViewById(R.id.harmonic_phasel2_button));
        UIStyle2.add(findViewById(R.id.harmonic_phasel3_button));
        this.switchView_UIStyle2 = new HandlerSwitchView(this.m_Context);
        this.switchView_UIStyle2.setTabViews(UIStyle2);
        this.switchView_UIStyle2.boundClick();
        this.switchView_UIStyle2.setOnTabChangedCallback(this);
        this.switchView_UIStyle2.selectView(0);
        List<View> powerHarmonic = new ArrayList<>();
        powerHarmonic.add(findViewById(R.id.btn_power_l1));
        powerHarmonic.add(findViewById(R.id.btn_power_l2));
        powerHarmonic.add(findViewById(R.id.btn_power_l3));
        this.switchView_powerHarmonic = new HandlerSwitchView(this.m_Context);
        this.switchView_powerHarmonic.setTabViews(powerHarmonic);
        this.switchView_powerHarmonic.boundClick();
        this.switchView_powerHarmonic.setOnTabChangedCallback(this);
        this.switchView_powerHarmonic.selectView(0);
    }

    @Override // android.support.v4.app.Fragment, com.clou.rs350.ui.fragment.BaseFragment
    public void onResume() {
        super.onResume();
        this.m_Style = Preferences.getInt(Preferences.Key.REALHARMONICSTYLE, 0);
        this.m_HarmonicMeasurement.HarmonicDefination = Preferences.getInt(Preferences.Key.HARMONICDEFINITION, 0);
        if (this.m_HarmonicMeasurement.HarmonicDefination == 0) {
            this.chk_Harmonic_Definition0.setChecked(true);
        } else {
            this.chk_Harmonic_Definition1.setChecked(true);
        }
        initData();
        setStyle();
        setTableOrGraph(this.isTableFlag);
    }

    @Override // android.support.v4.app.Fragment, com.clou.rs350.ui.fragment.BaseFragment
    public void onPause() {
        Preferences.putInt(Preferences.Key.REALHARMONICSTYLE, this.m_Style);
        Preferences.putInt(Preferences.Key.HARMONICDEFINITION, this.m_HarmonicMeasurement.HarmonicDefination);
        if (this.readDataThread != null && !this.readDataThread.isStop()) {
            this.readDataThread.stopTimer();
            this.readDataThread.interrupt();
            this.readDataThread = null;
        }
        super.onPause();
    }

    @Override // com.clou.rs350.handler.HandlerSwitchView.IOnSelectCallback
    public void onSelectedChanged(int index) {
        int smallIndex = index % 3;
        this.switchView_UIStyle1.selectView(index);
        this.switchView_UIStyle2.selectView(smallIndex);
        this.switchView_powerHarmonic.selectView(smallIndex);
        String[] TitleText = {getString(R.string.text_voltage_l1), getString(R.string.text_voltage_l2), getString(R.string.text_voltage_l3), getString(R.string.text_current_l1), getString(R.string.text_current_l2), getString(R.string.text_current_l3)};
        String[] TitleTextPhase = {getString(R.string.text_phase_l1), getString(R.string.text_phase_l2), getString(R.string.text_phase_l3)};
        String[] TitleTextVoltage = {getString(R.string.text_voltage_l1), getString(R.string.text_voltage_l2), getString(R.string.text_voltage_l3)};
        String[] TitleTextCurrent = {getString(R.string.text_current_l1), getString(R.string.text_current_l2), getString(R.string.text_current_l3)};
        this.titleTextView.setText(TitleText[index]);
        this.titleVITextView.setText(TitleTextPhase[smallIndex]);
        this.titlePowerTextView.setText(TitleTextPhase[smallIndex]);
        this.titleTextViewVoltate.setText(TitleTextVoltage[smallIndex]);
        this.titleTextViewCurrent.setText(TitleTextCurrent[smallIndex]);
        ShowValue(this.m_HarmonicMeasurement);
    }

    @Override // com.clou.rs350.handler.HandlerSwitchView.IOnSelectCallback
    public void onClickView(View v) {
        switch (v.getId()) {
            case R.id.btn_power_l1:
                if (284 != this.m_Type) {
                    setType(TIFFConstants.TIFFTAG_PLANARCONFIG);
                    break;
                }
                break;
            case R.id.btn_power_l2:
                if (540 != this.m_Type) {
                    setType(540);
                    break;
                }
                break;
            case R.id.btn_power_l3:
                if (796 != this.m_Type) {
                    setType(796);
                    break;
                }
                break;
            case R.id.harmonic_voltagel1_button:
                if (257 != this.m_Type) {
                    setType(257);
                    break;
                }
                break;
            case R.id.harmonic_voltagel2_button:
                if (513 != this.m_Type) {
                    setType(513);
                    break;
                }
                break;
            case R.id.harmonic_voltagel3_button:
                if (769 != this.m_Type) {
                    setType(769);
                    break;
                }
                break;
            case R.id.harmonic_currentl1_button:
                if (258 != this.m_Type) {
                    setType(258);
                    break;
                }
                break;
            case R.id.harmonic_currentl2_button:
                if (514 != this.m_Type) {
                    setType(TIFFConstants.TIFFTAG_JPEGIFBYTECOUNT);
                    break;
                }
                break;
            case R.id.harmonic_currentl3_button:
                if (770 != this.m_Type) {
                    setType(770);
                    break;
                }
                break;
            case R.id.harmonic_phasel1_button:
                if (259 != this.m_Type) {
                    setType(259);
                    break;
                }
                break;
            case R.id.harmonic_phasel2_button:
                if (515 != this.m_Type) {
                    setType(TIFFConstants.TIFFTAG_JPEGRESTARTINTERVAL);
                    break;
                }
                break;
            case R.id.harmonic_phasel3_button:
                if (771 != this.m_Type) {
                    setType(771);
                    break;
                }
                break;
        }
        ShowValue(this.m_HarmonicMeasurement);
    }

    @Override // com.clou.rs350.ui.fragment.BaseFragment, com.clou.rs350.manager.MessageManager.IMessageListener
    public void onReceiveMessage(final MeterBaseDevice Device) {
        new MyTask(new ILoadCallback() {
            /* class com.clou.rs350.ui.fragment.realtimefragment.HarmonicFragment.AnonymousClass3 */

            @Override // com.clou.rs350.callback.ILoadCallback
            public Object run() {
                HarmonicFragment.this.m_HarmonicMeasurement.parseData(Device);
                return null;
            }

            @Override // com.clou.rs350.callback.ILoadCallback
            public void callback(Object result) {
                HarmonicFragment.this.ShowValue(HarmonicFragment.this.m_HarmonicMeasurement);
            }
        }).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, new Integer[0]);
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private void ShowValue(HarmonicMeasurement harmonicMeasurement) {
        DecimalFormat df = new DecimalFormat("#.##");
        DecimalFormatSymbols symbols = new DecimalFormatSymbols();
        symbols.setDecimalSeparator('.');
        df.setDecimalFormatSymbols(symbols);
        int Phase = (this.m_Type >> 8) - 1;
        if (Phase < 0 || Phase > 2) {
            Phase = 0;
        }
        if (this.m_TypeBefore == 0) {
            switch (this.m_Style) {
                case 1:
                    if (2 == (this.m_Type & 2)) {
                        this.textTHD.setText(df.format(harmonicMeasurement.arrTHD[Phase + 3]));
                        this.adapter.refresh(harmonicMeasurement.arrCurrentValue[Phase], harmonicMeasurement.arrCurrentContent[Phase], harmonicMeasurement.arrCurrentAngle[Phase]);
                        this.histogramView.refreshData(harmonicMeasurement.arrCurrentContent[Phase]);
                    }
                    if (1 == (this.m_Type & 1)) {
                        this.textTHD.setText(df.format(harmonicMeasurement.arrTHD[Phase]));
                        this.adapter.refresh(harmonicMeasurement.arrVoltageValue[Phase], harmonicMeasurement.arrVoltageContent[Phase], harmonicMeasurement.arrVoltageAngle[Phase]);
                        this.histogramView.refreshData(harmonicMeasurement.arrVoltageContent[Phase]);
                        return;
                    }
                    return;
                default:
                    if (this.isTableFlag) {
                        this.vi_adapter.refreshData(harmonicMeasurement.arrVoltageValue[Phase], harmonicMeasurement.arrVoltageContent[Phase], harmonicMeasurement.arrVoltageAngle[Phase], harmonicMeasurement.arrCurrentValue[Phase], harmonicMeasurement.arrCurrentContent[Phase], harmonicMeasurement.arrCurrentAngle[Phase]);
                        return;
                    }
                    this.textTHDVoltage.setText(df.format(harmonicMeasurement.arrTHD[Phase]));
                    this.textTHDCurrent.setText(df.format(harmonicMeasurement.arrTHD[Phase + 3]));
                    this.histogramView1.refreshData(harmonicMeasurement.arrVoltageContent[Phase]);
                    this.histogramView2.refreshData(harmonicMeasurement.arrCurrentContent[Phase]);
                    return;
            }
        } else {
            this.power_adapter.refreshData(harmonicMeasurement.arrPValue[Phase], harmonicMeasurement.arrQValue[Phase], harmonicMeasurement.arrSValue[Phase]);
        }
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private void initData() {
        int refreshTime = Preferences.getInt(Preferences.Key.REFRESHTIME, 2500);
        if (refreshTime < 2500) {
            refreshTime = 2500;
        }
        if (this.readDataThread == null || this.readDataThread.isStop()) {
            this.readDataThread = new TimerThread((long) refreshTime, new ISleepCallback() {
                /* class com.clou.rs350.ui.fragment.realtimefragment.HarmonicFragment.AnonymousClass4 */
                int index = 0;

                @Override // com.clou.rs350.task.ISleepCallback
                public void onSleepAfterToDo() {
                    if (HarmonicFragment.this.m_Hold) {
                        return;
                    }
                    if (HarmonicFragment.this.m_TypeBefore == 0) {
                        switch (this.index) {
                            case 1:
                                HarmonicFragment.this.messageManager.getData();
                                HarmonicFragment.this.messageManager.getHarmonicValue(HarmonicFragment.this.m_Type);
                                break;
                            case 2:
                                HarmonicFragment.this.messageManager.getHarmonicTHD();
                                if (!HarmonicFragment.this.isTableFlag) {
                                    this.index = 0;
                                    break;
                                }
                                break;
                            default:
                                HarmonicFragment.this.messageManager.getHarmonicAngle(HarmonicFragment.this.m_Type);
                                this.index = 0;
                                break;
                        }
                        this.index++;
                        return;
                    }
                    switch (this.index) {
                        case 1:
                            HarmonicFragment.this.messageManager.getHarmonicValue(HarmonicFragment.this.m_Type >> 8, 4);
                            break;
                        case 2:
                            HarmonicFragment.this.messageManager.getHarmonicValue(HarmonicFragment.this.m_Type >> 8, 8);
                            break;
                        default:
                            HarmonicFragment.this.messageManager.getHarmonicValue(HarmonicFragment.this.m_Type >> 8, 16);
                            this.index = 0;
                            break;
                    }
                    this.index++;
                }
            }, "Get Data Thread");
            this.readDataThread.start();
        }
    }

    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        if (isChecked) {
            switch (buttonView.getId()) {
                case R.id.chk_harmonic_definition0:
                    this.m_HarmonicMeasurement.HarmonicDefination = 0;
                    return;
                case R.id.chk_harmonic_definition1:
                    this.m_HarmonicMeasurement.HarmonicDefination = 1;
                    return;
                default:
                    return;
            }
        }
    }
}
