package com.clou.rs350.ui.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.clou.rs350.R;
import com.clou.rs350.db.dao.ReadMeterDao;
import com.clou.rs350.db.model.BasicMeasurement;
import com.clou.rs350.db.model.ReadMeter;
import com.clou.rs350.device.MeterBaseDevice;
import com.clou.rs350.handler.HandlerSwitchView;
import com.clou.rs350.model.ClouData;
import com.clou.rs350.ui.adapter.FragmentTabAdapter;
import com.clou.rs350.ui.dialog.SaveDialog;
import com.clou.rs350.ui.fragment.BaseFragment;
import com.clou.rs350.ui.fragment.readmeterfragment.ReadMeterFragment;
import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.view.annotation.ViewInject;
import com.lidroid.xutils.view.annotation.event.OnClick;

import java.util.ArrayList;
import java.util.List;

public class ReadMeterActivity extends BaseActivity implements View.OnClickListener, HandlerSwitchView.IOnSelectCallback {
    @ViewInject(R.id.btn_back)
    private Button backButtom;
    BasicMeasurement m_BasicValue = new BasicMeasurement();
    @ViewInject(R.id.setting_normal_linearlayout)
    private LinearLayout normalLayout;
    @ViewInject(R.id.btn_save)
    private Button saveButtom;
    private HandlerSwitchView switchView;
    @ViewInject(R.id.title_textview)
    private TextView titleTextView;

    /* access modifiers changed from: protected */
    @Override // com.clou.rs350.ui.activity.BaseActivity, android.support.v4.app.FragmentActivity
    public void onCreate(Bundle savedInstanceState) {
        this.TAG = "ReadMeterActivity";
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_read_meter);
        setDefaultTitle(R.string.text_read_meter);
        ViewUtils.inject(this);
        initView();
        this.m_TabAdapter = new FragmentTabAdapter(this, getFragments(), R.id.content_view);
        this.m_TabAdapter.init();
    }

    private List<BaseFragment> getFragments() {
        List<BaseFragment> list = new ArrayList<>();
        list.add(new ReadMeterFragment(ClouData.getInstance().getReadMeter()));
        return list;
    }

    /* access modifiers changed from: protected */
    @Override // com.clou.rs350.ui.activity.BaseActivity, android.support.v4.app.FragmentActivity
    public void onResume() {
        super.onResume();
    }

    /* access modifiers changed from: protected */
    @Override // com.clou.rs350.ui.activity.BaseActivity, android.support.v4.app.FragmentActivity
    public void onPause() {
        super.onPause();
    }

    private void initView() {
    }

    @OnClick({R.id.btn_back, R.id.btn_save})
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_back:
                finish();
                return;
            case R.id.other_layout:
                this.normalLayout.setVisibility(View.GONE);
                return;
            case R.id.btn_save:
                new SaveDialog(this.m_Context, getString(new int[]{R.string.text_read_meter}[this.m_TabAdapter.getCurrentTab()]), new SaveDialog.ISave() {
                    /* class com.clou.rs350.ui.activity.ReadMeterActivity.AnonymousClass1 */

                    @Override // com.clou.rs350.ui.dialog.SaveDialog.ISave
                    public boolean SaveMeasureData(String SerialNo, String Time) {
                        switch (ReadMeterActivity.this.m_TabAdapter.getCurrentTab()) {
                            case 0:
                                ReadMeter basicData = ClouData.getInstance().getReadMeter();
                                basicData.CustomerSerialNo = SerialNo;
                                return new ReadMeterDao(ReadMeterActivity.this.m_Context).insertObject(basicData, Time);
                            default:
                                return false;
                        }
                    }
                }, null).show();
                return;
            default:
                return;
        }
    }

    @Override // com.clou.rs350.ui.activity.BaseActivity, com.clou.rs350.manager.MessageManager.IMessageListener
    public void onReceiveMessage(MeterBaseDevice Device) {
        this.m_BasicValue.parseRangeData(Device);
    }

    @Override // com.clou.rs350.handler.HandlerSwitchView.IOnSelectCallback
    public void onSelectedChanged(int index) {
    }

    @Override // com.clou.rs350.handler.HandlerSwitchView.IOnSelectCallback
    public void onClickView(View view) {
        onClick(view);
    }

    @Override // com.clou.rs350.ui.activity.BaseActivity
    public void startOrStopChangeViewStatus(int flag) {
        boolean hasStart;
        boolean z;
        boolean z2 = false;
        super.startOrStopChangeViewStatus(flag);
        if (flag == 0) {
            this.switchView.boundClick();
            this.switchView.selectCurrentView();
        } else {
            this.switchView.unBoundClick();
        }
        if (1 == flag) {
            hasStart = true;
        } else {
            hasStart = false;
        }
        Button button = this.backButtom;
        if (hasStart) {
            z = false;
        } else {
            z = true;
        }
        button.setEnabled(z);
        Button button2 = this.saveButtom;
        if (!hasStart) {
            z2 = true;
        }
        button2.setEnabled(z2);
    }
}
