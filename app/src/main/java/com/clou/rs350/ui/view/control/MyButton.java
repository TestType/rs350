package com.clou.rs350.ui.view.control;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.StateListDrawable;
import android.util.AttributeSet;
import android.widget.Button;

import com.clou.rs350.Preferences;
import com.clou.rs350.R;
import com.clou.rs350.utils.ColorUtils;

public class MyButton extends Button {
    public MyButton(Context context) {
        this(context, null);
    }

    public MyButton(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    /* access modifiers changed from: protected */
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        ViewManager.getInstance().addControlView(4, this);
    }

    private void setShapColor() {
        setShapColor(Preferences.getInt("buttonColor", Color.parseColor("#F97A22")));
    }

    public void setShapColor(int color) {
        Resources res = getResources();
        if (res != null && getContext() != null) {
            try {
                float corner = (float) res.getDimensionPixelSize(R.dimen.dp_7);
                StateListDrawable myGrad = new StateListDrawable();
                GradientDrawable d1 = new GradientDrawable();
                d1.setCornerRadius(corner);
                d1.setColor(ColorUtils.changeColorAlpha(color, 255));
                d1.setStroke(res.getDimensionPixelSize(R.dimen.dp_2), -1);
                GradientDrawable d2 = new GradientDrawable();
                d2.setCornerRadius(corner);
                d2.setColor(ColorUtils.changeColorAlpha(color, 127));
                d2.setStroke(res.getDimensionPixelSize(R.dimen.dp_2), -1);
                myGrad.addState(new int[]{16842910, 16842908}, d1);
                myGrad.addState(new int[]{16842919, 16842910}, d2);
                myGrad.addState(new int[]{16842908}, d1);
                myGrad.addState(new int[]{16842919}, d2);
                myGrad.addState(new int[]{16842910}, d1);
                myGrad.addState(new int[0], d1);
            } catch (Exception e) {
            }
        }
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        ViewManager.getInstance().removeControlView(4, this);
    }
}
