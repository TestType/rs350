package com.clou.rs350.ui.fragment.realtimefragment;

import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.clou.rs350.Preferences;
import com.clou.rs350.R;
import com.clou.rs350.callback.ILoadCallback;
import com.clou.rs350.db.model.WaveMeasurement;
import com.clou.rs350.device.MeterBaseDevice;
import com.clou.rs350.handler.HandlerSwitchView;
import com.clou.rs350.model.ClouData;
import com.clou.rs350.task.ISleepCallback;
import com.clou.rs350.task.MyTask;
import com.clou.rs350.task.TimerThread;
import com.clou.rs350.ui.fragment.BaseFragment;
import com.clou.rs350.ui.view.MyWaveView;
import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.view.annotation.ViewInject;
import com.lidroid.xutils.view.annotation.event.OnClick;

import java.util.ArrayList;
import java.util.List;

public class WaveFragment extends BaseFragment implements CompoundButton.OnCheckedChangeListener, View.OnClickListener, HandlerSwitchView.IOnSelectCallback {
    private static final String TAG = "WaveFragment";
    private CheckBox I1Cbox;
    private CheckBox I2Cbox;
    private CheckBox I3Cbox;
    private CheckBox IL1Cbox;
    private CheckBox IL2Cbox;
    private CheckBox IL3Cbox;
    private CheckBox U1Cbox;
    private CheckBox U2Cbox;
    private CheckBox U3Cbox;
    private CheckBox UL1Cbox;
    private CheckBox UL2Cbox;
    private CheckBox UL3Cbox;
    @ViewInject(R.id.btn_hold)
    private Button btn_Hold;
    boolean flag = true;
    private boolean m_Hold = false;
    int m_Style = 0;
    WaveMeasurement m_WaveMeasurement;
    private TimerThread readDataThread;
    private int readFlag = 0;
    private HandlerSwitchView switchView;
    private int tmpWiring = 0;
    @ViewInject(R.id.realtime_wave_view)
    private MyWaveView waveView;
    @ViewInject(R.id.realtime_wave_view_table1)
    private MyWaveView waveViewTable1;
    @ViewInject(R.id.realtime_wave_view_table2)
    private MyWaveView waveViewTable2;
    @ViewInject(R.id.realtime_wave_view_table3)
    private MyWaveView waveViewTable3;

    public WaveFragment(WaveMeasurement Data) {
        this.m_WaveMeasurement = Data;
    }

    @Override // android.support.v4.app.Fragment
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        this.fragmentView = inflater.inflate(R.layout.fragment_wave, (ViewGroup) null);
        ViewUtils.inject(this, this.fragmentView);
        return this.fragmentView;
    }

    @Override // android.support.v4.app.Fragment, com.clou.rs350.ui.fragment.BaseFragment
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initView();
    }

    private void setStyle() {
        switch (this.m_Style) {
            case 1:
                findViewById(R.id.realtime_waveconent_view).setVisibility(8);
                findViewById(R.id.realtime_waveconent_newtable_view).setVisibility(0);
                return;
            default:
                findViewById(R.id.realtime_waveconent_view).setVisibility(0);
                findViewById(R.id.realtime_waveconent_newtable_view).setVisibility(8);
                this.m_Style = 0;
                return;
        }
    }

    @Override // android.support.v4.app.Fragment, com.clou.rs350.ui.fragment.BaseFragment
    public void onResume() {
        super.onResume();
        this.m_Style = Preferences.getInt(Preferences.Key.REALWAVESTYLE, 0);
        setStyle();
        initData();
    }

    private void initData() {
        int refreshTime = Preferences.getInt(Preferences.Key.REFRESHTIME, 4000);
        if (refreshTime < 4000) {
            refreshTime = 4000;
        }
        if (this.readDataThread == null || this.readDataThread.isStop()) {
            this.readDataThread = new TimerThread((long) refreshTime, new ISleepCallback() {
                /* class com.clou.rs350.ui.fragment.realtimefragment.WaveFragment.AnonymousClass1 */

                @Override // com.clou.rs350.task.ISleepCallback
                public void onSleepAfterToDo() {
                    if (!WaveFragment.this.m_Hold) {
                        switch (WaveFragment.this.readFlag) {
                            case 1:
                                WaveFragment.this.messageManager.getPQWave();
                                return;
                            default:
                                WaveFragment.this.messageManager.getUIWave();
                                return;
                        }
                    }
                }
            }, "Get Data Thread");
            this.readDataThread.start();
        }
    }

    private void initView() {
        int[] Colors = ClouData.getInstance().getSettingBasic().PhaseColors;
        this.U1Cbox = (CheckBox) findViewById(R.id.cb_ul1);
        this.U1Cbox.setOnCheckedChangeListener(this);
        this.U1Cbox.setTextColor(Colors[0]);
        ((TextView) findViewById(R.id.txt_u1)).setTextColor(Colors[0]);
        findViewById(R.id.view_u1).setBackgroundColor(Colors[0]);
        this.I1Cbox = (CheckBox) findViewById(R.id.cb_il1);
        this.I1Cbox.setOnCheckedChangeListener(this);
        this.I1Cbox.setTextColor(Colors[3]);
        ((TextView) findViewById(R.id.txt_i1)).setTextColor(Colors[3]);
        ((TextView) findViewById(R.id.view_i1)).setTextColor(Colors[3]);
        this.U2Cbox = (CheckBox) findViewById(R.id.cb_ul2);
        this.U2Cbox.setOnCheckedChangeListener(this);
        this.U2Cbox.setTextColor(Colors[1]);
        ((TextView) findViewById(R.id.txt_u2)).setTextColor(Colors[1]);
        findViewById(R.id.view_u2).setBackgroundColor(Colors[1]);
        this.I2Cbox = (CheckBox) findViewById(R.id.cb_il2);
        this.I2Cbox.setOnCheckedChangeListener(this);
        this.I2Cbox.setTextColor(Colors[4]);
        ((TextView) findViewById(R.id.txt_i2)).setTextColor(Colors[4]);
        ((TextView) findViewById(R.id.view_i2)).setTextColor(Colors[4]);
        this.U3Cbox = (CheckBox) findViewById(R.id.cb_ul3);
        this.U3Cbox.setOnCheckedChangeListener(this);
        this.U3Cbox.setTextColor(Colors[2]);
        ((TextView) findViewById(R.id.txt_u3)).setTextColor(Colors[2]);
        findViewById(R.id.view_u3).setBackgroundColor(Colors[2]);
        this.I3Cbox = (CheckBox) findViewById(R.id.cb_il3);
        this.I3Cbox.setOnCheckedChangeListener(this);
        this.I3Cbox.setTextColor(Colors[5]);
        ((TextView) findViewById(R.id.txt_i3)).setTextColor(Colors[5]);
        ((TextView) findViewById(R.id.view_i3)).setTextColor(Colors[5]);
        this.UL1Cbox = (CheckBox) findViewById(R.id.cb_ul1_right);
        this.UL1Cbox.setOnCheckedChangeListener(this);
        this.UL1Cbox.setTextColor(Colors[0]);
        findViewById(R.id.view_ul1).setBackgroundColor(Colors[0]);
        this.IL1Cbox = (CheckBox) findViewById(R.id.cb_il1_right);
        this.IL1Cbox.setOnCheckedChangeListener(this);
        this.IL1Cbox.setTextColor(Colors[3]);
        ((TextView) findViewById(R.id.view_il1)).setTextColor(Colors[3]);
        this.UL2Cbox = (CheckBox) findViewById(R.id.cb_ul2_right);
        this.UL2Cbox.setOnCheckedChangeListener(this);
        this.UL2Cbox.setTextColor(Colors[1]);
        findViewById(R.id.view_ul2).setBackgroundColor(Colors[1]);
        this.IL2Cbox = (CheckBox) findViewById(R.id.cb_il2_right);
        this.IL2Cbox.setOnCheckedChangeListener(this);
        this.IL2Cbox.setTextColor(Colors[4]);
        ((TextView) findViewById(R.id.view_il2)).setTextColor(Colors[4]);
        this.UL3Cbox = (CheckBox) findViewById(R.id.cb_ul3_right);
        this.UL3Cbox.setOnCheckedChangeListener(this);
        this.UL3Cbox.setTextColor(Colors[2]);
        findViewById(R.id.view_ul3).setBackgroundColor(Colors[2]);
        this.IL3Cbox = (CheckBox) findViewById(R.id.cb_il3_right);
        this.IL3Cbox.setOnCheckedChangeListener(this);
        this.IL3Cbox.setTextColor(Colors[5]);
        ((TextView) findViewById(R.id.view_il3)).setTextColor(Colors[5]);
        this.waveView.showLines(this.U1Cbox.isChecked(), this.I1Cbox.isChecked(), this.U2Cbox.isChecked(), this.I2Cbox.isChecked(), this.U3Cbox.isChecked(), this.I3Cbox.isChecked());
        this.waveView.setLayerType(1, null);
        this.waveViewTable1.showLines(this.UL1Cbox.isChecked(), this.IL1Cbox.isChecked(), false, false, false, false);
        this.waveViewTable1.setLayerType(1, null);
        this.waveViewTable2.showLines(false, false, this.UL2Cbox.isChecked(), this.IL2Cbox.isChecked(), false, false);
        this.waveViewTable2.setLayerType(1, null);
        this.waveViewTable3.showLines(false, false, false, false, this.UL3Cbox.isChecked(), this.IL3Cbox.isChecked());
        this.waveViewTable3.setLayerType(1, null);
        List<View> tabViews = new ArrayList<>();
        tabViews.add(findViewById(R.id.btn_ui));
        tabViews.add(findViewById(R.id.btn_power));
        this.switchView = new HandlerSwitchView(this.m_Context);
        this.switchView.setTabViews(tabViews);
        this.switchView.boundClick();
        this.switchView.setOnTabChangedCallback(this);
        this.switchView.selectView(0);
    }

    public void onCheckedChanged(CompoundButton v, boolean isChecked) {
        this.waveView.showLines(this.U1Cbox.isChecked(), this.I1Cbox.isChecked(), this.U2Cbox.isChecked(), this.I2Cbox.isChecked(), this.U3Cbox.isChecked(), this.I3Cbox.isChecked());
        this.waveViewTable1.showLines(this.UL1Cbox.isChecked(), this.IL1Cbox.isChecked(), false, false, false, false);
        this.waveViewTable2.showLines(false, false, this.UL2Cbox.isChecked(), this.IL2Cbox.isChecked(), false, false);
        this.waveViewTable3.showLines(false, false, false, false, this.UL3Cbox.isChecked(), this.IL3Cbox.isChecked());
        switch (v.getId()) {
            case R.id.cb_ul1:
            case R.id.cb_ul1_right:
                this.U1Cbox.setChecked(isChecked);
                this.UL1Cbox.setChecked(isChecked);
                return;
            case R.id.cb_il1:
            case R.id.cb_il1_right:
                this.I1Cbox.setChecked(isChecked);
                this.IL1Cbox.setChecked(isChecked);
                return;
            case R.id.cb_ul2:
            case R.id.cb_ul2_right:
                this.U2Cbox.setChecked(isChecked);
                this.UL2Cbox.setChecked(isChecked);
                return;
            case R.id.cb_il2:
            case R.id.cb_il2_right:
                this.I2Cbox.setChecked(isChecked);
                this.IL2Cbox.setChecked(isChecked);
                return;
            case R.id.cb_ul3:
            case R.id.cb_ul3_right:
                this.U3Cbox.setChecked(isChecked);
                this.UL3Cbox.setChecked(isChecked);
                return;
            case R.id.cb_il3:
            case R.id.cb_il3_right:
                this.I3Cbox.setChecked(isChecked);
                this.IL3Cbox.setChecked(isChecked);
                return;
            case R.id.realtime_waveconent_newtable_view:
            case R.id.realtime_waveconent_newtableleft_view:
            case R.id.realtime_wave_view_table1:
            case R.id.realtime_wave_view_table2:
            case R.id.realtime_wave_view_table3:
            default:
                return;
        }
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private void setButtonEnable(int flag2) {
        boolean z;
        boolean z2;
        boolean z3;
        boolean z4;
        boolean z5;
        boolean z6;
        boolean z7;
        boolean z8;
        boolean z9;
        boolean z10;
        boolean z11;
        boolean z12;
        boolean z13;
        boolean z14;
        boolean z15 = true;
        this.I2Cbox.setChecked(flag2 < 1);
        CheckBox checkBox = this.U2Cbox;
        if (flag2 < 1) {
            z = true;
        } else {
            z = false;
        }
        checkBox.setChecked(z);
        CheckBox checkBox2 = this.I2Cbox;
        if (flag2 < 1) {
            z2 = true;
        } else {
            z2 = false;
        }
        checkBox2.setEnabled(z2);
        CheckBox checkBox3 = this.U2Cbox;
        if (flag2 < 1) {
            z3 = true;
        } else {
            z3 = false;
        }
        checkBox3.setEnabled(z3);
        CheckBox checkBox4 = this.IL2Cbox;
        if (flag2 < 1) {
            z4 = true;
        } else {
            z4 = false;
        }
        checkBox4.setChecked(z4);
        CheckBox checkBox5 = this.UL2Cbox;
        if (flag2 < 1) {
            z5 = true;
        } else {
            z5 = false;
        }
        checkBox5.setChecked(z5);
        CheckBox checkBox6 = this.IL2Cbox;
        if (flag2 < 1) {
            z6 = true;
        } else {
            z6 = false;
        }
        checkBox6.setEnabled(z6);
        CheckBox checkBox7 = this.UL2Cbox;
        if (flag2 < 1) {
            z7 = true;
        } else {
            z7 = false;
        }
        checkBox7.setEnabled(z7);
        CheckBox checkBox8 = this.I3Cbox;
        if (flag2 < 2) {
            z8 = true;
        } else {
            z8 = false;
        }
        checkBox8.setChecked(z8);
        CheckBox checkBox9 = this.U3Cbox;
        if (flag2 < 2) {
            z9 = true;
        } else {
            z9 = false;
        }
        checkBox9.setChecked(z9);
        CheckBox checkBox10 = this.I3Cbox;
        if (flag2 < 2) {
            z10 = true;
        } else {
            z10 = false;
        }
        checkBox10.setEnabled(z10);
        CheckBox checkBox11 = this.U3Cbox;
        if (flag2 < 2) {
            z11 = true;
        } else {
            z11 = false;
        }
        checkBox11.setEnabled(z11);
        CheckBox checkBox12 = this.IL3Cbox;
        if (flag2 < 2) {
            z12 = true;
        } else {
            z12 = false;
        }
        checkBox12.setChecked(z12);
        CheckBox checkBox13 = this.UL3Cbox;
        if (flag2 < 2) {
            z13 = true;
        } else {
            z13 = false;
        }
        checkBox13.setChecked(z13);
        CheckBox checkBox14 = this.IL3Cbox;
        if (flag2 < 2) {
            z14 = true;
        } else {
            z14 = false;
        }
        checkBox14.setEnabled(z14);
        CheckBox checkBox15 = this.UL3Cbox;
        if (flag2 >= 2) {
            z15 = false;
        }
        checkBox15.setEnabled(z15);
    }

    @Override // com.clou.rs350.ui.fragment.BaseFragment, com.clou.rs350.manager.MessageManager.IMessageListener
    public void onReceiveMessage(final MeterBaseDevice Device) {
        super.onReceiveMessage(Device);
        new MyTask(new ILoadCallback() {
            /* class com.clou.rs350.ui.fragment.realtimefragment.WaveFragment.AnonymousClass2 */

            @Override // com.clou.rs350.callback.ILoadCallback
            public Object run() {
                WaveFragment.this.m_WaveMeasurement.parseData(Device);
                return null;
            }

            @Override // com.clou.rs350.callback.ILoadCallback
            public void callback(Object result) {
                int wiring = WaveFragment.this.messageManager.getMeter().getWiringValue();
                if (wiring != WaveFragment.this.tmpWiring) {
                    WaveFragment.this.tmpWiring = wiring;
                    WaveFragment.this.setButtonEnable(wiring);
                }
                switch (WaveFragment.this.readFlag) {
                    case 1:
                        WaveFragment.this.waveView.refreshData(1, WaveFragment.this.m_WaveMeasurement.wavePa, WaveFragment.this.m_WaveMeasurement.waveQa, WaveFragment.this.m_WaveMeasurement.wavePb, WaveFragment.this.m_WaveMeasurement.waveQb, WaveFragment.this.m_WaveMeasurement.wavePc, WaveFragment.this.m_WaveMeasurement.waveQc);
                        WaveFragment.this.waveViewTable1.refreshData(1, WaveFragment.this.m_WaveMeasurement.wavePa, WaveFragment.this.m_WaveMeasurement.waveQa, null, null, null, null);
                        WaveFragment.this.waveViewTable2.refreshData(1, null, null, WaveFragment.this.m_WaveMeasurement.wavePb, WaveFragment.this.m_WaveMeasurement.waveQb, null, null);
                        WaveFragment.this.waveViewTable3.refreshData(1, null, null, null, null, WaveFragment.this.m_WaveMeasurement.wavePc, WaveFragment.this.m_WaveMeasurement.waveQc);
                        return;
                    default:
                        WaveFragment.this.waveView.refreshData(0, WaveFragment.this.m_WaveMeasurement.waveUa, WaveFragment.this.m_WaveMeasurement.waveIa, WaveFragment.this.m_WaveMeasurement.waveUb, WaveFragment.this.m_WaveMeasurement.waveIb, WaveFragment.this.m_WaveMeasurement.waveUc, WaveFragment.this.m_WaveMeasurement.waveIc);
                        WaveFragment.this.waveViewTable1.refreshData(0, WaveFragment.this.m_WaveMeasurement.waveUa, WaveFragment.this.m_WaveMeasurement.waveIa, null, null, null, null);
                        WaveFragment.this.waveViewTable2.refreshData(0, null, null, WaveFragment.this.m_WaveMeasurement.waveUb, WaveFragment.this.m_WaveMeasurement.waveIb, null, null);
                        WaveFragment.this.waveViewTable3.refreshData(0, null, null, null, null, WaveFragment.this.m_WaveMeasurement.waveUc, WaveFragment.this.m_WaveMeasurement.waveIc);
                        return;
                }
            }
        }).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, new Integer[0]);
    }

    @Override // android.support.v4.app.Fragment, com.clou.rs350.ui.fragment.BaseFragment
    public void onPause() {
        super.onPause();
        Preferences.putInt(Preferences.Key.REALWAVESTYLE, this.m_Style);
        if (this.readDataThread != null && !this.readDataThread.isStop()) {
            this.readDataThread.stopTimer();
            this.readDataThread.interrupt();
            this.readDataThread = null;
        }
    }

    @OnClick({R.id.btn_style, R.id.btn_hold})
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_hold:
                this.m_Hold = !this.m_Hold;
                this.btn_Hold.setText(this.m_Hold ? R.string.text_continue : R.string.text_hold);
                return;
            case R.id.btn_readall:
            default:
                return;
            case R.id.btn_style:
                this.m_Style++;
                setStyle();
                return;
        }
    }

    @Override // com.clou.rs350.handler.HandlerSwitchView.IOnSelectCallback
    public void onSelectedChanged(int index) {
        this.readFlag = index;
        String[] UP = {getActivity().getResources().getString(R.string.text_u), getActivity().getResources().getString(R.string.text_p)};
        String[] IQ = {getActivity().getResources().getString(R.string.text_i), getActivity().getResources().getString(R.string.text_q)};
        this.U1Cbox.setText(UP[index]);
        this.UL1Cbox.setText(UP[index]);
        this.U2Cbox.setText(UP[index]);
        this.UL2Cbox.setText(UP[index]);
        this.U3Cbox.setText(UP[index]);
        this.UL3Cbox.setText(UP[index]);
        this.I1Cbox.setText(IQ[index]);
        this.IL1Cbox.setText(IQ[index]);
        this.I2Cbox.setText(IQ[index]);
        this.IL2Cbox.setText(IQ[index]);
        this.I3Cbox.setText(IQ[index]);
        this.IL3Cbox.setText(IQ[index]);
    }

    @Override // com.clou.rs350.handler.HandlerSwitchView.IOnSelectCallback
    public void onClickView(View view) {
    }
}
