package com.clou.rs350.ui.popwindow;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.clou.rs350.R;
import com.clou.rs350.db.model.DBDataModel;
import com.clou.rs350.device.model.Baudrate;
import com.clou.rs350.utils.OtherUtils;

public class SetBaudratePopWindow extends PopupWindow implements View.OnClickListener {
    private TextView Cancel = null;
    private TextView Save = null;
    private TextView baudrateEt = null;
    private String[] baudrateList;
    private View contentView;
    private TextView dataBitEt = null;
    private String[] dataBitList;
    View.OnClickListener mOnClick;
    private Context m_Context = null;
    View m_ParentView;
    private TextView parityBitEt = null;
    private String[] parityBitList;
    private TextView stopBitEt = null;
    private String[] stopBitList;
    private Baudrate tempData;

    public SetBaudratePopWindow(Context context) {
        this.m_Context = context;
        initView();
        initData();
    }

    public void showPopWindow(View currentClick, Baudrate baudrate, View.OnClickListener onclick) {
        this.mOnClick = onclick;
        this.m_ParentView = currentClick;
        this.tempData = baudrate;
        showValue();
        if (!isShowing()) {
            int[] location = new int[2];
            currentClick.getLocationOnScreen(location);
            showAtLocation(currentClick, 0, location[0], location[1] + currentClick.getHeight());
            return;
        }
        dismiss();
    }

    private void initData() {
        this.baudrateList = new String[]{"Auto", "600", "1200", "2400", "4800", "9600", "14400", "19200", "38400", "56000", "57600", "115200"};
        this.dataBitList = new String[]{"7", "8", "9"};
        this.parityBitList = this.m_Context.getResources().getStringArray(R.array.parity_bits);
        this.stopBitList = new String[]{"0.5", "1", "1.5", "2"};
    }

    private void initView() {
        View conentView = ((LayoutInflater) this.m_Context.getSystemService("layout_inflater")).inflate(R.layout.pop_window_set_baudrate, (ViewGroup) null);
        this.Save = (TextView) conentView.findViewById(R.id.ok);
        this.Save.setOnClickListener(this);
        this.Cancel = (TextView) conentView.findViewById(R.id.cancel);
        this.Cancel.setOnClickListener(this);
        this.baudrateEt = (TextView) conentView.findViewById(R.id.txt_baudrate);
        this.stopBitEt = (TextView) conentView.findViewById(R.id.txt_stop_bits);
        this.dataBitEt = (TextView) conentView.findViewById(R.id.txt_data_bits);
        this.parityBitEt = (TextView) conentView.findViewById(R.id.txt_parity_bits);
        this.baudrateEt.setFocusable(false);
        this.stopBitEt.setFocusable(false);
        this.dataBitEt.setFocusable(false);
        this.parityBitEt.setFocusable(false);
        this.baudrateEt.setOnClickListener(this);
        this.stopBitEt.setOnClickListener(this);
        this.dataBitEt.setOnClickListener(this);
        this.parityBitEt.setOnClickListener(this);
        setContentView(conentView);
        setWidth(this.m_Context.getResources().getDimensionPixelSize(R.dimen.dp_280));
        setHeight(this.m_Context.getResources().getDimensionPixelSize(R.dimen.dp_180));
        setFocusable(true);
        setOutsideTouchable(true);
        update();
        setBackgroundDrawable(new ColorDrawable(0));
        setAnimationStyle(16973826);
    }

    private void showValue() {
        this.baudrateEt.setText(this.baudrateList[((int) this.tempData.baudrate.l_Value) + 1]);
        this.baudrateEt.setTag(Long.valueOf(this.tempData.baudrate.l_Value + 1));
        this.dataBitEt.setText(this.dataBitList[(int) this.tempData.dataBits.l_Value]);
        this.dataBitEt.setTag(Long.valueOf(this.tempData.dataBits.l_Value));
        this.parityBitEt.setText(this.parityBitList[(int) this.tempData.parityBits.l_Value]);
        this.parityBitEt.setTag(Long.valueOf(this.tempData.parityBits.l_Value));
        this.stopBitEt.setText(this.stopBitList[(int) this.tempData.stopBits.l_Value]);
        this.stopBitEt.setTag(Long.valueOf(this.tempData.stopBits.l_Value));
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.txt_baudrate:
                ListPopWindow.showListPopwindow(this.m_Context, this.baudrateList, v, null, 0, true, this.m_ParentView);
                return;
            case R.id.txt_data_bits:
                ListPopWindow.showListPopwindow(this.m_Context, this.dataBitList, v, null, 0, true, this.m_ParentView);
                return;
            case R.id.txt_parity_bits:
                ListPopWindow.showListPopwindow(this.m_Context, this.parityBitList, v, null, 0, true, this.m_ParentView);
                return;
            case R.id.txt_stop_bits:
                ListPopWindow.showListPopwindow(this.m_Context, this.stopBitList, v, null, 0, true, this.m_ParentView);
                return;
            case R.id.ok:
                if (validate()) {
                    this.tempData.baudrate.l_Value = (long) (((int) OtherUtils.convertToLong(this.baudrateEt.getTag())) - 1);
                    this.tempData.dataBits.l_Value = (long) ((int) OtherUtils.convertToLong(this.dataBitEt.getTag()));
                    this.tempData.parityBits.l_Value = (long) ((int) OtherUtils.convertToLong(this.parityBitEt.getTag()));
                    this.tempData.stopBits.l_Value = (long) ((int) OtherUtils.convertToLong(this.stopBitEt.getTag()));
                    parseBaudrate(this.tempData);
                    if (this.mOnClick != null) {
                        v.setTag(this.tempData);
                        this.mOnClick.onClick(v);
                    }
                    dismiss();
                    return;
                }
                return;
            case R.id.cancel:
                dismiss();
                return;
            default:
                return;
        }
    }

    private boolean validate() {
        if (OtherUtils.isEmpty(this.baudrateEt) || OtherUtils.isEmpty(this.dataBitEt) || OtherUtils.isEmpty(this.parityBitEt) || OtherUtils.isEmpty(this.stopBitEt)) {
            return false;
        }
        return true;
    }

    public void parseBaudrate(Baudrate tmpData) {
        tmpData.baudrate.s_Value = this.baudrateList[((int) tmpData.baudrate.l_Value) + 1];
        tmpData.dataBits.s_Value = this.dataBitList[(int) tmpData.dataBits.l_Value];
        DBDataModel dBDataModel = tmpData.parityBits;
        dBDataModel.s_Value = new String[]{"n", "e", "o"}[(int) tmpData.parityBits.l_Value];
        tmpData.stopBits.s_Value = this.stopBitList[(int) tmpData.stopBits.l_Value];
    }
}
