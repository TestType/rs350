package com.clou.rs350.ui.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.clou.rs350.R;

public class NormalDialog extends Dialog implements View.OnClickListener {
    private Button btn_Cancel = null;
    private Button btn_Ok = null;
    private View.OnClickListener cancleClick = null;
    private Context m_Context = null;
    private int m_TitleId = 0;
    private View.OnClickListener okClick = null;
    private TextView txt_Title = null;

    public NormalDialog(Context context) {
        super(context, R.style.dialog);
        requestWindowFeature(1);
        setCanceledOnTouchOutside(true);
        this.m_Context = context;
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_normal);
        this.txt_Title = (TextView) findViewById(R.id.txt_title);
        this.txt_Title.setMovementMethod(ScrollingMovementMethod.getInstance());
        if (this.m_TitleId != 0) {
            this.txt_Title.setText(this.m_TitleId);
        }
        this.btn_Ok = (Button) findViewById(R.id.btn_ok);
        this.btn_Ok.setOnClickListener(this);
        this.btn_Cancel = (Button) findViewById(R.id.btn_cancel);
        this.btn_Cancel.setOnClickListener(this);
    }

    @Override // android.app.Dialog
    public void setTitle(CharSequence title) {
        this.txt_Title.setText(title.toString());
    }

    @Override // android.app.Dialog
    public void setTitle(int titleId) {
        this.txt_Title.setText(titleId);
    }

    public void setOkText(CharSequence text) {
        this.btn_Ok.setText(text);
    }

    public void setOkText(int id) {
        this.btn_Ok.setText(id);
    }

    public void setPositiveButton(int textId, View.OnClickListener listener) {
        this.btn_Ok.setText(textId);
        this.okClick = listener;
    }

    public void setNegativeButton(int textId, View.OnClickListener listener) {
        this.btn_Cancel.setText(textId);
        this.cancleClick = listener;
    }

    public void setCancleText(CharSequence text) {
        this.btn_Cancel.setText(text);
    }

    public void onClick(View v) {
        dismiss();
        switch (v.getId()) {
            case R.id.btn_cancel:
                if (this.cancleClick != null) {
                    this.cancleClick.onClick(v);
                }
                dismiss();
                return;
            case R.id.btn_ok:
                if (this.okClick != null) {
                    this.okClick.onClick(v);
                }
                dismiss();
                return;
            default:
                return;
        }
    }

    public void setOnClick(View.OnClickListener onClickListener, View.OnClickListener cancleClick2) {
        this.okClick = onClickListener;
        this.cancleClick = cancleClick2;
    }

    public void show(int txtId) {
        this.m_TitleId = txtId;
        show();
    }
}
