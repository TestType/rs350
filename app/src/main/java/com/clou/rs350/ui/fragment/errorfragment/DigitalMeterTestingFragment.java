package com.clou.rs350.ui.fragment.errorfragment;

import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.clou.rs350.R;
import com.clou.rs350.db.dao.DigitalMeterTypeDao;
import com.clou.rs350.db.model.BasicMeasurement;
import com.clou.rs350.db.model.DigitalMeterTest;
import com.clou.rs350.db.model.DigitalMeterType;
import com.clou.rs350.device.MeterBaseDevice;
import com.clou.rs350.handler.HandlerSwitchView;
import com.clou.rs350.task.ISleepCallback;
import com.clou.rs350.task.TimerThread;
import com.clou.rs350.ui.activity.BaseActivity;
import com.clou.rs350.ui.dialog.NumericDialog;
import com.clou.rs350.ui.popwindow.ListPopWindow;
import com.clou.rs350.ui.popwindow.SetBaudratePopWindow;
import com.clou.rs350.ui.view.infoview.DigitalMeterTypeView;
import com.clou.rs350.utils.DoubleClick;
import com.clou.rs350.utils.OtherUtils;
import com.itextpdf.text.pdf.PdfObject;
import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.view.annotation.ViewInject;
import com.lidroid.xutils.view.annotation.event.OnClick;

import java.util.ArrayList;
import java.util.List;

public class DigitalMeterTestingFragment extends BaseErrorFragment implements View.OnClickListener, HandlerSwitchView.IOnSelectCallback {
    private static final String TAG = "DigitalMeterErrorFragment";
    private boolean autoStop = false;
    @ViewInject(R.id.btn_select)
    LinearLayout btn_Select;
    @ViewInject(R.id.btn_set)
    private Button btn_Set;
    @ViewInject(R.id.btn_setting)
    private Button btn_Setting;
    @ViewInject(R.id.btn_start)
    private Button btn_Start;
    @ViewInject(R.id.btn_test)
    private Button btn_Test;
    private TextView[] e11Tv = new TextView[3];
    private TextView[] e21Tv = new TextView[3];
    private TextView[] e31Tv = new TextView[3];
    private TextView[] e41Tv = new TextView[3];
    private TextView[] e51Tv = new TextView[3];
    private TextView[] eA1Tv = new TextView[3];
    private TextView[] es1Tv = new TextView[3];
    private boolean hasStart = false;
    private BasicMeasurement m_BasicData;
    private String[] m_BaudrateList = {"600", "1200", "2400", "4800", "9600", "14400", "19200", "38400", "56000", "57600", "115200"};
    DigitalMeterTypeDao m_Dao;
    private String[] m_DataBitList;
    private int m_ErrorCount = 0;
    private DigitalMeterTest m_ErrorTest;
    private int m_ErrorTime = 5;
    DigitalMeterTypeView m_InfoView;
    private boolean m_IsBaseTime = false;
    private String[] m_ParityList;
    private String[] m_StopBitList;
    @ViewInject(R.id.layout_digital_meter_type)
    View m_ViewLayout;
    private int[] measurementType = new int[3];
    private int[] orderMeterState = new int[3];
    private TimerThread readDataThread;
    private DoubleClick selectClick = null;
    private HandlerSwitchView switchView;
    @ViewInject(R.id.txt_address)
    private TextView txt_Address;
    @ViewInject(R.id.txt_baudrate)
    private TextView txt_Baudrate;
    @ViewInject(R.id.txt_ct)
    private TextView txt_CT;
    @ViewInject(R.id.txt_data_bits)
    private TextView txt_Data_Bits;
    @ViewInject(R.id.txt_meterp)
    private TextView txt_MeterP;
    @ViewInject(R.id.txt_meterq)
    private TextView txt_MeterQ;
    @ViewInject(R.id.txt_meters)
    private TextView txt_MeterS;
    @ViewInject(R.id.txt_pt)
    private TextView txt_PT;
    @ViewInject(R.id.txt_parity_bits)
    private TextView txt_Parity;
    @ViewInject(R.id.txt_stop_bits)
    private TextView txt_Stop_Bits;
    @ViewInject(R.id.txt_type)
    EditText txt_Type;

    public DigitalMeterTestingFragment(DigitalMeterTest Data) {
        this.m_ErrorTest = Data;
        this.m_BasicData = new BasicMeasurement();
    }

    @Override // android.support.v4.app.Fragment
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        this.fragmentView = inflater.inflate(R.layout.fragment_digital_meter_testing, (ViewGroup) null);
        ViewUtils.inject(this, this.fragmentView);
        return this.fragmentView;
    }

    @Override // android.support.v4.app.Fragment, com.clou.rs350.ui.fragment.BaseFragment
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initView();
        initData();
    }

    private void initData() {
        this.m_DataBitList = new String[]{"7", "8", "9"};
        this.m_ParityList = this.m_Context.getResources().getStringArray(R.array.parity_bits);
        this.m_StopBitList = new String[]{"0.5", "1", "1.5", "2"};
    }

    private void initView() {
        this.m_Dao = new DigitalMeterTypeDao(this.m_Context);
        List<View> tabViews = new ArrayList<>();
        tabViews.add(findViewById(R.id.btn_test));
        tabViews.add(findViewById(R.id.btn_setting));
        this.switchView = new HandlerSwitchView(this.m_Context);
        this.switchView.setTabViews(tabViews);
        this.switchView.boundClick();
        this.switchView.setOnTabChangedCallback(this);
        this.switchView.selectView(0);
        this.txt_Type.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            /* class com.clou.rs350.ui.fragment.errorfragment.DigitalMeterTestingFragment.AnonymousClass1 */

            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == 6 || actionId == 5) {
                    String id = DigitalMeterTestingFragment.this.txt_Type.getText().toString();
                    DigitalMeterTestingFragment.this.searchData(id);
                    DigitalMeterTestingFragment.this.txt_Type.setSelection(id.length());
                }
                Log.i("actionId", new StringBuilder(String.valueOf(actionId)).toString());
                return false;
            }
        });
        this.btn_Select.setOnClickListener(this);
        this.selectClick = new DoubleClick(new DoubleClick.OnDoubleClick() {
            /* class com.clou.rs350.ui.fragment.errorfragment.DigitalMeterTestingFragment.AnonymousClass2 */

            private void searchKey(String condition) {
                List<String> tmpNames = DigitalMeterTestingFragment.this.m_Dao.queryAllKey(condition);
                ListPopWindow.showListPopwindow(DigitalMeterTestingFragment.this.m_Context, (String[]) tmpNames.toArray(new String[tmpNames.size()]), DigitalMeterTestingFragment.this.txt_Type, new View.OnClickListener() {
                    /* class com.clou.rs350.ui.fragment.errorfragment.DigitalMeterTestingFragment.AnonymousClass2.AnonymousClass1 */

                    public void onClick(View v) {
                        DigitalMeterTestingFragment.this.searchData(((TextView) v).getText().toString());
                    }
                });
            }

            @Override // com.clou.rs350.utils.DoubleClick.OnDoubleClick
            public void doubleClick() {
                searchKey(DigitalMeterTestingFragment.this.txt_Type.getText().toString());
            }

            @Override // com.clou.rs350.utils.DoubleClick.OnDoubleClick
            public void singleClick() {
                searchKey(PdfObject.NOTHING);
            }
        });
        this.m_InfoView = new DigitalMeterTypeView(this.m_Context, this.m_ViewLayout);
        int[] e1id = {R.id.txt_pe1, R.id.txt_qe1, R.id.txt_se1};
        int[] e2id = {R.id.txt_pe2, R.id.txt_qe2, R.id.txt_se2};
        int[] e3id = {R.id.txt_pe3, R.id.txt_qe3, R.id.txt_se3};
        int[] e4id = {R.id.txt_pe4, R.id.txt_qe4, R.id.txt_se4};
        int[] e5id = {R.id.txt_pe5, R.id.txt_qe5, R.id.txt_se5};
        int[] eavgid = {R.id.txt_peavg, R.id.txt_qeavg, R.id.txt_seavg};
        int[] esid = {R.id.txt_pes, R.id.txt_qes, R.id.txt_ses};
        for (int i = 0; i < 3; i++) {
            this.e11Tv[i] = (TextView) findViewById(e1id[i]);
            this.e21Tv[i] = (TextView) findViewById(e2id[i]);
            this.e31Tv[i] = (TextView) findViewById(e3id[i]);
            this.e41Tv[i] = (TextView) findViewById(e4id[i]);
            this.e51Tv[i] = (TextView) findViewById(e5id[i]);
            this.eA1Tv[i] = (TextView) findViewById(eavgid[i]);
            this.es1Tv[i] = (TextView) findViewById(esid[i]);
        }
    }

    @Override // android.support.v4.app.Fragment, com.clou.rs350.ui.fragment.BaseFragment
    public void onResume() {
        super.onResume();
        refreshData();
        if (this.hasStart) {
            getDatas();
        }
    }

    @Override // com.clou.rs350.ui.fragment.BaseFragment
    public void refreshData() {
        new SetBaudratePopWindow(this.m_Context).parseBaudrate(this.m_ErrorTest.DeviceBaudrate);
        this.m_ErrorTest.DeviceBaudrate.parityBits.s_Value = this.m_ParityList[(int) this.m_ErrorTest.DeviceBaudrate.parityBits.l_Value];
        this.txt_Address.setText(new StringBuilder(String.valueOf(this.m_ErrorTest.Address)).toString());
        this.txt_Baudrate.setText(this.m_ErrorTest.DeviceBaudrate.baudrate.s_Value);
        this.txt_Data_Bits.setText(this.m_ErrorTest.DeviceBaudrate.dataBits.s_Value);
        this.txt_Parity.setText(this.m_ErrorTest.DeviceBaudrate.parityBits.s_Value);
        this.txt_Stop_Bits.setText(this.m_ErrorTest.DeviceBaudrate.stopBits.s_Value);
        this.txt_PT.setText(new StringBuilder(String.valueOf(this.m_ErrorTest.PT)).toString());
        this.txt_CT.setText(new StringBuilder(String.valueOf(this.m_ErrorTest.CT)).toString());
        this.txt_Type.setText(this.m_ErrorTest.Type);
        this.messageManager.setDigitalParameter((int) this.m_ErrorTest.DeviceBaudrate.baudrate.l_Value, (int) this.m_ErrorTest.DeviceBaudrate.dataBits.l_Value, (int) this.m_ErrorTest.DeviceBaudrate.parityBits.l_Value, (int) this.m_ErrorTest.DeviceBaudrate.stopBits.l_Value, this.m_ErrorTest.Address, this.m_ErrorTest.DataType, this.m_ErrorTest.PT, this.m_ErrorTest.CT);
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private void searchData(String type) {
        DigitalMeterType info = this.m_Dao.queryObjectByType(type);
        if (info != null) {
            this.m_ErrorTest.CopyData(info);
        } else {
            this.m_ErrorTest.Type = PdfObject.NOTHING;
        }
    }

    private void getDatas() {
        int refreshTime = this.m_ErrorTime * 1000;
        if (this.readDataThread == null || this.readDataThread.isStop()) {
            this.readDataThread = new TimerThread((long) refreshTime, new ISleepCallback() {
                /* class com.clou.rs350.ui.fragment.errorfragment.DigitalMeterTestingFragment.AnonymousClass3 */

                @Override // com.clou.rs350.task.ISleepCallback
                public void onSleepAfterToDo() {
                    DigitalMeterTestingFragment.this.messageManager.getDigitalError();
                }
            }, "Get Data Thread");
            this.readDataThread.start();
        }
    }

    private void setParameters() {
        this.m_ErrorTest.Address = getValue(this.txt_Address);
        this.m_ErrorTest.PT = getValue(this.txt_PT);
        this.m_ErrorTest.CT = getValue(this.txt_CT);
        this.messageManager.setDigitalParameter((int) this.m_ErrorTest.DeviceBaudrate.baudrate.l_Value, (int) this.m_ErrorTest.DeviceBaudrate.dataBits.l_Value, (int) this.m_ErrorTest.DeviceBaudrate.parityBits.l_Value, (int) this.m_ErrorTest.DeviceBaudrate.stopBits.l_Value, this.m_ErrorTest.Address, this.m_ErrorTest.DataType, this.m_ErrorTest.PT, this.m_ErrorTest.CT);
    }

    @Override // com.clou.rs350.ui.fragment.BaseFragment, com.clou.rs350.manager.MessageManager.IMessageListener
    public void onReceiveMessage(MeterBaseDevice Device) {
        this.m_BasicData.parseData(Device);
        if (this.hasStart) {
            this.m_ErrorTest.parseData(Device, 0);
        } else if (this.m_IsBaseTime) {
            for (int i = 0; i < this.measurementType.length; i++) {
            }
        }
        showData(this.m_ErrorTest);
    }

    @Override // android.support.v4.app.Fragment, com.clou.rs350.ui.fragment.BaseFragment
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState != null && savedInstanceState.containsKey("constants")) {
            this.measurementType = (int[]) savedInstanceState.getSerializable("measurementType");
            this.orderMeterState = (int[]) savedInstanceState.getSerializable("orderMeterState");
        }
    }

    /* JADX DEBUG: Multi-variable search result rejected for r3v0, resolved type: android.os.Bundle */
    /* JADX WARN: Multi-variable type inference failed */
    /* JADX WARN: Type inference failed for: r1v0, types: [int[], java.io.Serializable] */
    /* JADX WARN: Type inference failed for: r1v1, types: [int[], java.io.Serializable] */
    /* JADX WARNING: Unknown variable types count: 2 */
    @Override // android.support.v4.app.Fragment
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void onSaveInstanceState(Bundle outState) {
        if (outState.size() != 0) {
            outState.putSerializable("measurementType", this.measurementType);
            outState.putSerializable("orderMeterState", this.orderMeterState);
        }
        super.onSaveInstanceState(outState);
    }


    @Override // android.support.v4.app.Fragment, com.clou.rs350.ui.fragment.BaseFragment
    public void onPause() {
        this.messageManager.setBaudrate(1, 7, 1, 0, 1);
        killThread();
        super.onPause();
    }

    private void killThread() {
        if (this.readDataThread != null && !this.readDataThread.isStop()) {
            this.readDataThread.stopTimer();
            this.readDataThread.interrupt();
            this.readDataThread = null;
        }
    }

    @Override // com.clou.rs350.ui.fragment.errorfragment.BaseErrorFragment
    public boolean start() {
        this.messageManager.setDigitalStartFlag(1);
        this.m_ErrorTest.cleanError();
        getDatas();
        setStartButton(1);
        this.hasStart = true;
        return true;
    }

    @Override // com.clou.rs350.ui.fragment.errorfragment.BaseErrorFragment
    public void doContinue() {
        getDatas();
        this.hasStart = true;
    }

    @Override // com.clou.rs350.ui.fragment.errorfragment.BaseErrorFragment
    public boolean stop() {
        this.messageManager.setDigitalStartFlag(0);
        killThread();
        setStartButton(0);
        this.hasStart = false;
        copyData();
        return true;
    }

    private void setStartButton(int flag) {
        this.btn_Start.setText(getString(flag == 0 ? R.string.text_start : R.string.text_stop));
        this.btn_Setting.setEnabled(flag == 0);
        if (flag == 0) {
            this.switchView.boundClick();
            this.switchView.selectCurrentView();
        } else {
            this.switchView.unBoundClick();
        }
        if (this.m_Context instanceof BaseActivity) {
            ((BaseActivity) this.m_Context).startOrStopChangeViewStatus(flag);
        }
    }

    private void showData(DigitalMeterTest errors) {
        for (int i = 0; i < 3; i++) {
            this.e11Tv[i].setText(errors.getError1(i));
            this.e21Tv[i].setText(errors.getError2(i));
            this.e31Tv[i].setText(errors.getError3(i));
            this.e41Tv[i].setText(errors.getError4(i));
            this.e51Tv[i].setText(errors.getError5(i));
            this.eA1Tv[i].setText(errors.getErrorAverage(i, this.m_ErrorCount));
            this.es1Tv[i].setText(errors.getErrorStandard(i, this.m_ErrorCount));
        }
        this.txt_MeterP.setText(errors.MeterP.s_Value);
        this.txt_MeterQ.setText(errors.MeterQ.s_Value);
        this.txt_MeterS.setText(errors.MeterS.s_Value);
    }

    @OnClick({R.id.btn_start, R.id.btn_set, R.id.txt_address, R.id.txt_baudrate, R.id.txt_stop_bits, R.id.txt_data_bits, R.id.txt_parity_bits, R.id.txt_pt, R.id.txt_ct, R.id.btn_select})
    public void onClick(View v) {
        NumericDialog numeric = new NumericDialog(this.m_Context);
        numeric.setMinus(false);
        switch (v.getId()) {
            case R.id.btn_select:
                this.selectClick.click();
                return;
            case R.id.btn_start:
                if (OtherUtils.isEmpty(this.m_ErrorTest.Type)) {
                    showHint(R.string.text_meter_type_wrong);
                    return;
                } else if (!this.hasStart) {
                    start();
                    return;
                } else {
                    stop();
                    return;
                }
            case R.id.txt_baudrate:
                ListPopWindow.showListPopwindow(this.m_Context, this.m_BaudrateList, this.txt_Baudrate, new View.OnClickListener() {
                    /* class com.clou.rs350.ui.fragment.errorfragment.DigitalMeterTestingFragment.AnonymousClass4 */

                    public void onClick(View v) {
                        DigitalMeterTestingFragment.this.m_ErrorTest.DeviceBaudrate.baudrate.l_Value = (long) ((Integer) DigitalMeterTestingFragment.this.txt_Baudrate.getTag()).intValue();
                        DigitalMeterTestingFragment.this.m_ErrorTest.DeviceBaudrate.baudrate.s_Value = DigitalMeterTestingFragment.this.txt_Baudrate.getText().toString();
                    }
                });
                return;
            case R.id.txt_data_bits:
                ListPopWindow.showListPopwindow(this.m_Context, this.m_DataBitList, this.txt_Data_Bits, new View.OnClickListener() {
                    /* class com.clou.rs350.ui.fragment.errorfragment.DigitalMeterTestingFragment.AnonymousClass5 */

                    public void onClick(View v) {
                        DigitalMeterTestingFragment.this.m_ErrorTest.DeviceBaudrate.dataBits.l_Value = (long) ((Integer) DigitalMeterTestingFragment.this.txt_Data_Bits.getTag()).intValue();
                        DigitalMeterTestingFragment.this.m_ErrorTest.DeviceBaudrate.dataBits.s_Value = DigitalMeterTestingFragment.this.txt_Data_Bits.getText().toString();
                    }
                });
                return;
            case R.id.txt_parity_bits:
                ListPopWindow.showListPopwindow(this.m_Context, this.m_ParityList, this.txt_Parity, new View.OnClickListener() {
                    /* class com.clou.rs350.ui.fragment.errorfragment.DigitalMeterTestingFragment.AnonymousClass7 */

                    public void onClick(View v) {
                        DigitalMeterTestingFragment.this.m_ErrorTest.DeviceBaudrate.parityBits.l_Value = (long) ((Integer) DigitalMeterTestingFragment.this.txt_Parity.getTag()).intValue();
                        DigitalMeterTestingFragment.this.m_ErrorTest.DeviceBaudrate.parityBits.s_Value = DigitalMeterTestingFragment.this.txt_Parity.getText().toString();
                    }
                });
                return;
            case R.id.txt_stop_bits:
                ListPopWindow.showListPopwindow(this.m_Context, this.m_StopBitList, this.txt_Stop_Bits, new View.OnClickListener() {
                    /* class com.clou.rs350.ui.fragment.errorfragment.DigitalMeterTestingFragment.AnonymousClass6 */

                    public void onClick(View v) {
                        DigitalMeterTestingFragment.this.m_ErrorTest.DeviceBaudrate.stopBits.l_Value = (long) ((Integer) DigitalMeterTestingFragment.this.txt_Stop_Bits.getTag()).intValue();
                        DigitalMeterTestingFragment.this.m_ErrorTest.DeviceBaudrate.stopBits.s_Value = DigitalMeterTestingFragment.this.txt_Stop_Bits.getText().toString();
                    }
                });
                return;
            case R.id.txt_address:
                numeric.setDot(false);
                numeric.showMyDialog(this.txt_Address);
                return;
            case R.id.txt_pt:
                numeric.setDot(false);
                numeric.showMyDialog(this.txt_PT);
                return;
            case R.id.txt_ct:
                numeric.setDot(false);
                numeric.showMyDialog(this.txt_CT);
                return;
            case R.id.btn_set:
                setParameters();
                return;
            default:
                return;
        }
    }

    private int getValue(TextView tv) {
        String text = tv.getText().toString().trim();
        if (TextUtils.isEmpty(text)) {
            return 0;
        }
        return OtherUtils.convertToInt(Double.valueOf(OtherUtils.parseDouble(text)));
    }

    @Override // com.clou.rs350.ui.fragment.errorfragment.BaseErrorFragment
    public boolean hasSetParams() {
        return true;
    }

    @Override // com.clou.rs350.handler.HandlerSwitchView.IOnSelectCallback
    public void onSelectedChanged(int index) {
        setView(index);
    }

    @Override // com.clou.rs350.handler.HandlerSwitchView.IOnSelectCallback
    public void onClickView(View v) {
    }

    private void setView(int index) {
        int i;
        int i2 = 8;
        boolean z = false;
        View findViewById = findViewById(R.id.layout_test);
        if (index == 0) {
            i = 0;
        } else {
            i = 8;
        }
        findViewById.setVisibility(i);
        View findViewById2 = findViewById(R.id.layout_digital_meter_type);
        if (index == 1) {
            i2 = 0;
        }
        findViewById2.setVisibility(i2);
        Button button = this.btn_Start;
        if (index == 0) {
            z = true;
        }
        button.setEnabled(z);
    }
}
