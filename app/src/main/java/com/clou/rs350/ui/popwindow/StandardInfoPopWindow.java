package com.clou.rs350.ui.popwindow;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.PopupWindow;

import com.clou.rs350.R;
import com.clou.rs350.db.model.StandardInfo;

public class StandardInfoPopWindow extends PopupWindow implements View.OnClickListener {
    View.OnClickListener mOnClick;
    Context m_Context;
    StandardInfo m_Info;
    EditText txt_MainBoardVersion;
    EditText txt_MeasureBoardVersion;
    EditText txt_NeutralBoardVersion;
    EditText txt_StandardSr;

    public StandardInfoPopWindow(Context context) {
        super(context);
        this.m_Context = context;
        initView();
    }

    private void initView() {
        View conentView = ((LayoutInflater) this.m_Context.getSystemService("layout_inflater")).inflate(R.layout.pop_window_standardinfo, (ViewGroup) null);
        conentView.findViewById(R.id.ok).setOnClickListener(this);
        conentView.findViewById(R.id.cancel).setOnClickListener(this);
        this.txt_StandardSr = (EditText) conentView.findViewById(R.id.txt_standard_serial_no);
        this.txt_MainBoardVersion = (EditText) conentView.findViewById(R.id.txt_main_board);
        this.txt_MeasureBoardVersion = (EditText) conentView.findViewById(R.id.txt_measure_board);
        this.txt_NeutralBoardVersion = (EditText) conentView.findViewById(R.id.txt_neutral_board);
        setContentView(conentView);
        setWidth(this.m_Context.getResources().getDimensionPixelSize(R.dimen.dp_400));
        setHeight(this.m_Context.getResources().getDimensionPixelSize(R.dimen.dp_200));
        setFocusable(true);
        setOutsideTouchable(true);
        update();
        setBackgroundDrawable(new ColorDrawable(0));
        setAnimationStyle(16973826);
    }

    private void showValue() {
        this.txt_StandardSr.setText(this.m_Info.StandardSerialNo);
        this.txt_MainBoardVersion.setText(this.m_Info.MainBoardVersion);
        this.txt_MeasureBoardVersion.setText(this.m_Info.MeasureBoardVersion);
        this.txt_NeutralBoardVersion.setText(this.m_Info.NeutralBoardVersion);
    }

    private StandardInfo praseValue() {
        StandardInfo tmpStandardInfo = new StandardInfo(this.m_Context);
        tmpStandardInfo.StandardSerialNo = this.txt_StandardSr.getText().toString();
        tmpStandardInfo.MainBoardVersion = this.txt_MainBoardVersion.getText().toString().toUpperCase();
        tmpStandardInfo.MeasureBoardVersion = this.txt_MeasureBoardVersion.getText().toString().toUpperCase();
        tmpStandardInfo.NeutralBoardVersion = this.txt_NeutralBoardVersion.getText().toString().toUpperCase();
        return tmpStandardInfo;
    }

    public void showPopWindow(View currentClick, StandardInfo Info, View.OnClickListener onclick) {
        this.mOnClick = onclick;
        this.m_Info = Info;
        showValue();
        if (!isShowing()) {
            int[] location = new int[2];
            currentClick.getLocationOnScreen(location);
            showAtLocation(currentClick, 0, location[0], location[1] + currentClick.getHeight());
            return;
        }
        dismiss();
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ok:
                v.setTag(praseValue());
                if (this.mOnClick != null) {
                    this.mOnClick.onClick(v);
                }
                if (isShowing()) {
                    dismiss();
                    return;
                }
                return;
            case R.id.cancel:
                if (isShowing()) {
                    dismiss();
                    return;
                }
                return;
            default:
                return;
        }
    }
}
