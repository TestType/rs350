package com.clou.rs350.ui.activity;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.TextView;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.clou.rs350.CLApplication;
import com.clou.rs350.Preferences;
import com.clou.rs350.R;
import com.clou.rs350.callback.ILoadCallback;
import com.clou.rs350.db.dao.OperatorInfoDao;
import com.clou.rs350.db.model.OperatorInfo;
import com.clou.rs350.model.ClouData;
import com.clou.rs350.service.CLService;
import com.clou.rs350.ui.dialog.LoadingDialog;
import com.clou.rs350.ui.popwindow.ListPopWindow;
import com.clou.rs350.utils.DoubleClick;
import com.clou.rs350.utils.OtherUtils;
import com.itextpdf.text.pdf.PdfObject;
import com.itextpdf.text.xml.xmp.XmpMMProperties;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.List;

public class StartActivity extends BaseActivity implements View.OnClickListener {
    private boolean initOver = false;
    final private int REQUEST_CODE_STORAGE_PERMISSION = 990;
    OperatorInfoDao m_Dao;
    DoubleClick m_DoubleClick;
    TextView txt_ID;
    TextView txt_Password;

    /* access modifiers changed from: protected */
    @Override // com.clou.rs350.ui.activity.BaseActivity, android.support.v4.app.FragmentActivity
    public void onCreate(Bundle savedInstanceState) {
        this.TAG = "StartActivity";
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_start);
        solicitarPermisos();
    }

    private void adjustStartNext() {
        if (this.initOver) {
            startTo(MainActivity.class);
            finish();
        }
    }

    private void initView() {
        this.txt_ID = (TextView) findViewById(R.id.txt_id);
        this.txt_Password = (TextView) findViewById(R.id.txt_password);
        findViewById(R.id.btn_login).setOnClickListener(this);
        findViewById(R.id.btn_close).setOnClickListener(this);
        findViewById(R.id.btn_select).setOnClickListener(this);
        this.m_DoubleClick = new DoubleClick(new DoubleClick.OnDoubleClick() {
            /* class com.clou.rs350.ui.activity.StartActivity.AnonymousClass1 */

            private void searchKey(String condition) {
                List<String> tmpNames = StartActivity.this.m_Dao.queryAllKey(condition);
                ListPopWindow.showListPopwindow(StartActivity.this.m_Context, (String[]) tmpNames.toArray(new String[tmpNames.size()]), StartActivity.this.txt_ID, new View.OnClickListener() {
                    /* class com.clou.rs350.ui.activity.StartActivity.AnonymousClass1.AnonymousClass1 */

                    public void onClick(View v) {
                    }
                });
            }

            @Override // com.clou.rs350.utils.DoubleClick.OnDoubleClick
            public void doubleClick() {
                searchKey(StartActivity.this.txt_ID.getText().toString());
            }

            @Override // com.clou.rs350.utils.DoubleClick.OnDoubleClick
            public void singleClick() {
                searchKey(PdfObject.NOTHING);
            }
        });
    }

    private void initData() {
        boolean isFirstLunchApp = Preferences.getBoolean(Preferences.Key.IS_FIRST_LUNCHAPP, true);
        this.m_Dao = new OperatorInfoDao(this.m_Context);
        if (isFirstLunchApp) {
            this.initOver = true;
        } else {
            this.initOver = true;
        }
        this.txt_ID.setText(Preferences.getString(Preferences.Key.OPERATOR_ID, XmpMMProperties.MANAGER));
        if (Preferences.getBoolean(Preferences.Key.HAS_CRASH, false)) {
            new LoadingDialog(this.m_Context, new ILoadCallback() {
                /* class com.clou.rs350.ui.activity.StartActivity.AnonymousClass2 */

                @Override // com.clou.rs350.callback.ILoadCallback
                public Object run() {
                    ClouData.getInstance(StartActivity.this.m_Context).loadDeserialization();
                    return null;
                }

                @Override // com.clou.rs350.callback.ILoadCallback
                public void callback(Object result) {
                }
            }).show();
        }
        Preferences.putBoolean(Preferences.Key.HAS_CRASH, false);
    }

    private void initFolder(){
        File rootPath = new File(CLApplication.ROOTPATH);
        if (!rootPath.exists()) {
            rootPath.mkdirs();
        }
        File downloadPath = new File(CLApplication.DOWNLOADPATH);
        if (!downloadPath.exists()) {
            downloadPath.mkdirs();
        }
        File imagePath = new File(CLApplication.IMAGEPATH);
        if (!imagePath.exists()) {
            imagePath.mkdirs();
        }
        File templetXmlPath = new File(CLApplication.TEMPLETXMLPATH);
        if (!templetXmlPath.exists()) {
            templetXmlPath.mkdirs();
        }
        File templetLogoPath = new File(CLApplication.TEMPLATELOGOPATH);
        if (!templetLogoPath.exists()) {
            templetLogoPath.mkdirs();
        }
        File simulaterPath = new File(CLApplication.SIMULATORPATH);
        if (!simulaterPath.exists()) {
            simulaterPath.mkdirs();
        }
        File reportPath = new File(CLApplication.REPORTPATH);
        if (!reportPath.exists()) {
            reportPath.mkdirs();
        }
        File inportPath = new File(CLApplication.IMPORTPATH);
        if (!inportPath.exists()) {
            inportPath.mkdirs();
        }
        File exportPath = new File(CLApplication.EXPORTPATH);
        if (!exportPath.exists()) {
            exportPath.mkdirs();
        }
        File ltmExportPath = new File(CLApplication.LTMEXPORTPATH);
        if (!ltmExportPath.exists()) {
            ltmExportPath.mkdirs();
        }
        File backupPath = new File(CLApplication.BACKUPPATH);
        if (!backupPath.exists()) {
            backupPath.mkdirs();
        }
        File logPath = new File(CLApplication.LOGPATH);
        if (!logPath.exists()) {
            logPath.mkdirs();
        }
    }

    private void initFiles(){
        PackageInfo packInfo = null;
        try {
            packInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        int version = packInfo.versionCode;
        if (version > Preferences.getInt(Preferences.Key.VERSIONBEFORE, 0)) {
            Assets2Sd(this.m_Context, "Template.xml", CLApplication.TEMPLETXMLPATH);
            Assets2Sd(this, "TemplateCN.xml", CLApplication.TEMPLETXMLPATH);
            Assets2Sd(this, "Template-rpp.xml", CLApplication.TEMPLETXMLPATH);
            Assets2Sd(this, "logo.png", CLApplication.TEMPLATELOGOPATH);
            Assets2Sd(this, "RPP.png", CLApplication.TEMPLATELOGOPATH);
            Assets2Sd(this, "index.html", CLApplication.SIMULATORPATH);
            Assets2Sd(this, "cas.html", CLApplication.SIMULATORPATH);
            Assets2Sd(this, "readRS.js", CLApplication.SIMULATORPATH);
            Preferences.putInt(Preferences.Key.VERSIONBEFORE, version);
            File tmpCNTemplate = new File(CLApplication.TEMPLETXMLPATH + "/检定报告.xml");
            if (tmpCNTemplate.exists()) {
                tmpCNTemplate.delete();
            }
            new File(CLApplication.TEMPLETXMLPATH + "/TemplateCN.xml").renameTo(new File(CLApplication.TEMPLETXMLPATH + "/检定报告.xml"));
        }

    }

    private void Assets2Sd(Context context, String fileAssetFile, String fileSdPath) {
        String fileSdPath2 = fileSdPath + File.separator + fileAssetFile;
        if (!new File(fileSdPath2).exists()) {
            try {
                copyBigDataToSD(context, fileAssetFile, fileSdPath2);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void copyBigDataToSD(Context context, String fileAssetPath, String strOutFileName) throws IOException {
        InputStream myInput = context.getAssets().open(fileAssetPath);
        OutputStream myOutput = new FileOutputStream(strOutFileName);
        byte[] buffer = new byte[1024];
        for (int length = myInput.read(buffer); length > 0; length = myInput.read(buffer)) {
            myOutput.write(buffer, 0, length);
        }
        myOutput.flush();
        myInput.close();
        myOutput.close();
    }

    /* access modifiers changed from: protected */
    @Override // com.clou.rs350.ui.activity.BaseActivity
    public void adjustAtBackground() {
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_select:
                this.m_DoubleClick.click();
                return;
            case R.id.txt_password:
            default:
                return;
            case R.id.btn_login:
                if (OtherUtils.isEmpty(this.txt_ID)) {
                    showHint(R.string.text_operator_id_empty);
                    return;
                }
                OperatorInfo info = (OperatorInfo) this.m_Dao.queryByKey(this.txt_ID.getText().toString());
                if (info == null) {
                    showHint(R.string.text_operator_id_wrong);
                    return;
                } else if (info.Password.equals(this.txt_Password.getText().toString())) {
                    Preferences.putString(Preferences.Key.OPERATOR_ID, info.OperatorID);
                    ClouData.getInstance().setOperatorInfo(info);
                    if (2 == info.Authority) {
                        ClouData.getInstance().setIsDemo(true);
                        this.messageManager.freshDevice();
                    }
                    adjustStartNext();
                    return;
                } else {
                    showHint(R.string.text_password_wrong);
                    return;
                }
            case R.id.btn_close:
                exitDo();
                return;
        }
    }

    @Override // android.support.v4.app.FragmentActivity
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode != 4) {
            return false;
        }
        exitDo();
        return false;
    }

    private void exitDo() {
        stopService(new Intent(this, CLService.class));
        finish();
    }

    public void solicitarPermisos(){
        int permisosEscritura = ContextCompat.checkSelfPermission(m_Context, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        int permisosLectura = ContextCompat.checkSelfPermission(m_Context, Manifest.permission.READ_EXTERNAL_STORAGE);
        int permissionsBluetooth = ContextCompat.checkSelfPermission(m_Context, Manifest.permission.BLUETOOTH);
        int permissionsBluetoothAdm = ContextCompat.checkSelfPermission(m_Context, Manifest.permission.BLUETOOTH_ADMIN);
        int permissionsCamara = ContextCompat.checkSelfPermission(m_Context, Manifest.permission.CAMERA);
        int permissionsTelephone = ContextCompat.checkSelfPermission(m_Context, Manifest.permission.READ_PHONE_STATE);
        int permissionsInternet = ContextCompat.checkSelfPermission(m_Context, Manifest.permission.INTERNET);

        if((permisosEscritura!= PackageManager.PERMISSION_GRANTED)||(permisosLectura!= PackageManager.PERMISSION_GRANTED)
        || (permissionsBluetooth != PackageManager.PERMISSION_GRANTED) || (permissionsBluetoothAdm != PackageManager.PERMISSION_GRANTED)
        || (permissionsCamara != PackageManager.PERMISSION_GRANTED) || (permissionsTelephone != PackageManager.PERMISSION_GRANTED)
        || (permissionsInternet != PackageManager.PERMISSION_GRANTED))
        {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                ActivityCompat.requestPermissions(StartActivity.this, new String[]{
                        Manifest.permission.READ_EXTERNAL_STORAGE,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        Manifest.permission.BLUETOOTH,
                        Manifest.permission.BLUETOOTH_ADMIN,
                        Manifest.permission.CAMERA,
                        Manifest.permission.READ_PHONE_STATE,
                        Manifest.permission.INTERNET,
                },REQUEST_CODE_STORAGE_PERMISSION);
            }
        }
        else{
            initView();
            initData();
            initFolder();
            initFiles();
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case REQUEST_CODE_STORAGE_PERMISSION: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    //Aquí lo que se hace si aceptan el permiso
                    initView();
                    initData();
                    initFolder();
                    initFiles();
                } else {
                    //Aquí lo que se hace si no lo aceptan
                }
                return;
            }
        }
    }
}
