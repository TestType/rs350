package com.clou.rs350.ui.fragment.realtimefragment;

import android.content.res.Resources;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.clou.rs350.Preferences;
import com.clou.rs350.R;
import com.clou.rs350.callback.ILoadCallback;
import com.clou.rs350.db.model.BasicMeasurement;
import com.clou.rs350.device.MeterBaseDevice;
import com.clou.rs350.handler.HandlerSwitchView;
import com.clou.rs350.model.ClouData;
import com.clou.rs350.task.ISleepCallback;
import com.clou.rs350.task.MyTask;
import com.clou.rs350.task.SleepTask;
import com.clou.rs350.task.TimerThread;
import com.clou.rs350.ui.fragment.BaseFragment;
import com.clou.rs350.ui.popwindow.TrxRatioPopWindow;
import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.view.annotation.ViewInject;
import com.lidroid.xutils.view.annotation.event.OnClick;

public class RealtimeBasicFragment extends BaseFragment implements View.OnClickListener, HandlerSwitchView.IOnSelectCallback {
    @ViewInject(R.id.f)
    private TextView FTv;
    @ViewInject(R.id.in)
    private TextView INTv;
    @ViewInject(R.id.l1anglei)
    private TextView L1AngleITv;
    @ViewInject(R.id.l1angleui)
    private TextView L1AngleUITv;
    @ViewInject(R.id.l1angleu)
    private TextView L1AngleUTv;
    @ViewInject(R.id.l1cfi)
    private TextView L1CFITv;
    @ViewInject(R.id.l1cfu)
    private TextView L1CFUTv;
    @ViewInject(R.id.txt_il1)
    private TextView L1ITv;
    @ViewInject(R.id.l1pf)
    private TextView L1PFTv;
    @ViewInject(R.id.l1p)
    private TextView L1PTv;
    @ViewInject(R.id.l1q)
    private TextView L1QTv;
    @ViewInject(R.id.l1s)
    private TextView L1STv;
    @ViewInject(R.id.l1thdi)
    private TextView L1THDITv;
    @ViewInject(R.id.l1thdu)
    private TextView L1THDUTv;
    @ViewInject(R.id.l1ull)
    private TextView L1ULLTv;
    @ViewInject(R.id.l1uln)
    private TextView L1ULNTv;
    @ViewInject(R.id.l2anglei)
    private TextView L2AngleITv;
    @ViewInject(R.id.l2angleui)
    private TextView L2AngleUITv;
    @ViewInject(R.id.l2angleu)
    private TextView L2AngleUTv;
    @ViewInject(R.id.l2cfi)
    private TextView L2CFITv;
    @ViewInject(R.id.l2cfu)
    private TextView L2CFUTv;
    @ViewInject(R.id.txt_il2)
    private TextView L2ITv;
    @ViewInject(R.id.l2pf)
    private TextView L2PFTv;
    @ViewInject(R.id.l2p)
    private TextView L2PTv;
    @ViewInject(R.id.l2q)
    private TextView L2QTv;
    @ViewInject(R.id.l2s)
    private TextView L2STv;
    @ViewInject(R.id.l2thdi)
    private TextView L2THDITv;
    @ViewInject(R.id.l2thdu)
    private TextView L2THDUTv;
    @ViewInject(R.id.l2ull)
    private TextView L2ULLTv;
    @ViewInject(R.id.l2uln)
    private TextView L2ULNTv;
    @ViewInject(R.id.l3anglei)
    private TextView L3AngleITv;
    @ViewInject(R.id.l3angleui)
    private TextView L3AngleUITv;
    @ViewInject(R.id.l3angleu)
    private TextView L3AngleUTv;
    @ViewInject(R.id.l3cfi)
    private TextView L3CFITv;
    @ViewInject(R.id.l3cfu)
    private TextView L3CFUTv;
    @ViewInject(R.id.txt_il3)
    private TextView L3ITv;
    @ViewInject(R.id.l3pf)
    private TextView L3PFTv;
    @ViewInject(R.id.l3p)
    private TextView L3PTv;
    @ViewInject(R.id.l3q)
    private TextView L3QTv;
    @ViewInject(R.id.l3s)
    private TextView L3STv;
    @ViewInject(R.id.l3thdi)
    private TextView L3THDITv;
    @ViewInject(R.id.l3thdu)
    private TextView L3THDUTv;
    @ViewInject(R.id.l3ull)
    private TextView L3ULLTv;
    @ViewInject(R.id.l3uln)
    private TextView L3ULNTv;
    @ViewInject(R.id.pseqi)
    private TextView PSEQITv;
    @ViewInject(R.id.psequ)
    private TextView PSEQUTv;
    @ViewInject(R.id.real_basic_f_textview)
    private TextView RbFTv;
    @ViewInject(R.id.real_basic_i_12_textview)
    private TextView RbL1AngleITv;
    @ViewInject(R.id.real_basic_un_13_textview)
    private TextView RbL1AngleUINTv;
    @ViewInject(R.id.real_basic_up_13_textview)
    private TextView RbL1AngleUITv;
    @ViewInject(R.id.real_basic_un_12_textview)
    private TextView RbL1AngleUNTv;
    @ViewInject(R.id.real_basic_up_12_textview)
    private TextView RbL1AngleUTv;
    @ViewInject(R.id.real_basic_i_11_textview)
    private TextView RbL1ITv;
    @ViewInject(R.id.real_basic_p_1_textview)
    private TextView RbL1PFTv;
    @ViewInject(R.id.real_basic_w_11_textview)
    private TextView RbL1PTv;
    @ViewInject(R.id.real_basic_w_12_textview)
    private TextView RbL1QTv;
    @ViewInject(R.id.real_basic_w_13_textview)
    private TextView RbL1STv;
    @ViewInject(R.id.real_basic_un_11_textview)
    private TextView RbL1UNTv;
    @ViewInject(R.id.real_basic_up_11_textview)
    private TextView RbL1UTv;
    @ViewInject(R.id.real_basic_i_22_textview)
    private TextView RbL2AngleITv;
    @ViewInject(R.id.real_basic_un_23_textview)
    private TextView RbL2AngleUINTv;
    @ViewInject(R.id.real_basic_up_23_textview)
    private TextView RbL2AngleUITv;
    @ViewInject(R.id.real_basic_un_22_textview)
    private TextView RbL2AngleUNTv;
    @ViewInject(R.id.real_basic_up_22_textview)
    private TextView RbL2AngleUTv;
    @ViewInject(R.id.real_basic_i_21_textview)
    private TextView RbL2ITv;
    @ViewInject(R.id.real_basic_p_2_textview)
    private TextView RbL2PFTv;
    @ViewInject(R.id.real_basic_w_21_textview)
    private TextView RbL2PTv;
    @ViewInject(R.id.real_basic_w_22_textview)
    private TextView RbL2QTv;
    @ViewInject(R.id.real_basic_w_23_textview)
    private TextView RbL2STv;
    @ViewInject(R.id.real_basic_un_21_textview)
    private TextView RbL2UNTv;
    @ViewInject(R.id.real_basic_up_21_textview)
    private TextView RbL2UTv;
    @ViewInject(R.id.real_basic_i_32_textview)
    private TextView RbL3AngleITv;
    @ViewInject(R.id.real_basic_un_33_textview)
    private TextView RbL3AngleUINTv;
    @ViewInject(R.id.real_basic_up_33_textview)
    private TextView RbL3AngleUITv;
    @ViewInject(R.id.real_basic_un_32_textview)
    private TextView RbL3AngleUNTv;
    @ViewInject(R.id.real_basic_up_32_textview)
    private TextView RbL3AngleUTv;
    @ViewInject(R.id.real_basic_i_31_textview)
    private TextView RbL3ITv;
    @ViewInject(R.id.real_basic_p_3_textview)
    private TextView RbL3PFTv;
    @ViewInject(R.id.real_basic_w_31_textview)
    private TextView RbL3PTv;
    @ViewInject(R.id.real_basic_w_32_textview)
    private TextView RbL3QTv;
    @ViewInject(R.id.real_basic_w_33_textview)
    private TextView RbL3STv;
    @ViewInject(R.id.real_basic_un_31_textview)
    private TextView RbL3UNTv;
    @ViewInject(R.id.real_basic_up_31_textview)
    private TextView RbL3UTv;
    @ViewInject(R.id.real_basic_iii_textview)
    private TextView RbPSeqITv;
    @ViewInject(R.id.real_basic_uuu_textview)
    private TextView RbPSeqUTv;
    @ViewInject(R.id.real_basic_p_4_textview)
    private TextView RbSumPFTv;
    @ViewInject(R.id.real_basic_w_41_textview)
    private TextView RbSumPTv;
    @ViewInject(R.id.real_basic_w_42_textview)
    private TextView RbSumQTv;
    @ViewInject(R.id.real_basic_w_43_textview)
    private TextView RbSumSTv;
    @ViewInject(R.id.sumpf)
    private TextView SumPFTv;
    @ViewInject(R.id.sump)
    private TextView SumPTv;
    @ViewInject(R.id.sumq)
    private TextView SumQTv;
    @ViewInject(R.id.sums)
    private TextView SumSTv;
    @ViewInject(R.id.un)
    private TextView UNTv;
    @ViewInject(R.id.btn_hold)
    private Button btn_Hold;
    @ViewInject(R.id.real_basic_title_i_2_textview)
    private TextView i2TitleTv;
    private boolean m_Hold = false;
    BasicMeasurement m_MeasureValue;
    int m_Style = 0;
    private int m_Wiring;
    private boolean m_initFinish = false;
    @ViewInject(R.id.real_basic_p_n_button)
    private Button pnButton;
    @ViewInject(R.id.basic_p_n_linearlayout)
    private LinearLayout pnLinearlayout;
    @ViewInject(R.id.real_basic_p_p_button)
    private Button ppButton;
    @ViewInject(R.id.basic_p_p_linearlayout)
    private LinearLayout ppLinearlayout;
    private TimerThread readDataThread;
    private HandlerSwitchView switchView;
    @ViewInject(R.id.real_basic_title_21_textview)
    private TextView u23TitleTv;
    @ViewInject(R.id.real_basic_title_31_textview)
    private TextView u31TitleTv;

    public RealtimeBasicFragment(BasicMeasurement Data) {
        this.m_MeasureValue = Data;
    }

    @Override // android.support.v4.app.Fragment
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        this.fragmentView = inflater.inflate(R.layout.fragment_realtime_basic, (ViewGroup) null);
        ViewUtils.inject(this, this.fragmentView);
        return this.fragmentView;
    }

    @Override // android.support.v4.app.Fragment, com.clou.rs350.ui.fragment.BaseFragment
    public void onResume() {
        super.onResume();
        this.m_Style = Preferences.getInt(Preferences.Key.REALBASICSTYLE, 0);
        initData();
        new SleepTask(50, new ISleepCallback() {
            /* class com.clou.rs350.ui.fragment.realtimefragment.RealtimeBasicFragment.AnonymousClass1 */

            @Override // com.clou.rs350.task.ISleepCallback
            public void onSleepAfterToDo() {
                RealtimeBasicFragment.this.setStyle();
                RealtimeBasicFragment.this.m_initFinish = true;
            }
        }).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, new Integer[0]);
    }

    @Override // android.support.v4.app.Fragment, com.clou.rs350.ui.fragment.BaseFragment
    public void onPause() {
        super.onPause();
        Preferences.putInt(Preferences.Key.REALBASICSTYLE, this.m_Style);
        if (this.readDataThread != null && !this.readDataThread.isStop()) {
            this.readDataThread.stopTimer();
            this.readDataThread.interrupt();
            this.readDataThread = null;
        }
    }

    @Override // com.clou.rs350.ui.fragment.BaseFragment, com.clou.rs350.manager.MessageManager.IMessageListener
    public void onReceiveMessage(final MeterBaseDevice Device) {
        super.onReceiveMessage(Device);
        if (!this.m_initFinish) {
            setStyle();
            this.m_initFinish = true;
            showValue(this.m_MeasureValue);
        }
        new MyTask(new ILoadCallback() {
            /* class com.clou.rs350.ui.fragment.realtimefragment.RealtimeBasicFragment.AnonymousClass2 */

            @Override // com.clou.rs350.callback.ILoadCallback
            public Object run() {
                RealtimeBasicFragment.this.m_MeasureValue.parseData(Device);
                return null;
            }

            @Override // com.clou.rs350.callback.ILoadCallback
            public void callback(Object result) {
                boolean z;
                if (RealtimeBasicFragment.this.m_Wiring != Device.getWiringValue()) {
                    RealtimeBasicFragment.this.m_Wiring = Device.getWiringValue();
                    RealtimeBasicFragment.this.setLayout(1 == RealtimeBasicFragment.this.m_Wiring);
                    RealtimeBasicFragment realtimeBasicFragment = RealtimeBasicFragment.this;
                    if (1 == RealtimeBasicFragment.this.m_Wiring) {
                        z = true;
                    } else {
                        z = false;
                    }
                    realtimeBasicFragment.setTitle(z);
                    if (2 == RealtimeBasicFragment.this.m_Wiring) {
                        RealtimeBasicFragment.this.pnButton.setEnabled(false);
                    } else {
                        RealtimeBasicFragment.this.pnButton.setEnabled(true);
                    }
                }
                RealtimeBasicFragment.this.showValue(RealtimeBasicFragment.this.m_MeasureValue);
            }
        }).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, new Integer[0]);
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private void showValue(BasicMeasurement tmpMeasureValue) {
        this.L1ULNTv.setText(tmpMeasureValue.VoltageValueL1.s_Value);
        this.L2ULNTv.setText(tmpMeasureValue.VoltageValueL2.s_Value);
        this.L3ULNTv.setText(tmpMeasureValue.VoltageValueL3.s_Value);
        this.L1ULLTv.setText(tmpMeasureValue.VoltageValueL1L2.s_Value);
        this.L2ULLTv.setText(tmpMeasureValue.VoltageValueL2L3.s_Value);
        this.L3ULLTv.setText(tmpMeasureValue.VoltageValueL3L1.s_Value);
        if (1 != this.m_Wiring) {
            this.L1AngleUTv.setText(tmpMeasureValue.VoltageAngleL1.s_Value);
            this.L2AngleUTv.setText(tmpMeasureValue.VoltageAngleL2.s_Value);
            this.L3AngleUTv.setText(tmpMeasureValue.VoltageAngleL3.s_Value);
            this.L1AngleUITv.setText(tmpMeasureValue.VoltageCurrentAngleL1.s_Value);
            this.L2AngleUITv.setText(tmpMeasureValue.VoltageCurrentAngleL2.s_Value);
            this.L3AngleUITv.setText(tmpMeasureValue.VoltageCurrentAngleL3.s_Value);
        } else {
            this.L1AngleUTv.setText(tmpMeasureValue.VoltageAngleL1L2.s_Value);
            this.L2AngleUTv.setText(tmpMeasureValue.VoltageAngleL2L3.s_Value);
            this.L3AngleUTv.setText(tmpMeasureValue.VoltageAngleL3L1.s_Value);
            this.L1AngleUITv.setText(tmpMeasureValue.AngleU12I1.s_Value);
            this.L2AngleUITv.setText(tmpMeasureValue.AngleU23I2.s_Value);
            this.L3AngleUITv.setText(tmpMeasureValue.AngleU31I3.s_Value);
        }
        this.L1THDUTv.setText(tmpMeasureValue.THDU1.s_Value);
        this.L2THDUTv.setText(tmpMeasureValue.THDU2.s_Value);
        this.L3THDUTv.setText(tmpMeasureValue.THDU3.s_Value);
        this.L1CFUTv.setText(tmpMeasureValue.CFU1.s_Value);
        this.L2CFUTv.setText(tmpMeasureValue.CFU2.s_Value);
        this.L3CFUTv.setText(tmpMeasureValue.CFU3.s_Value);
        this.L1ITv.setText(tmpMeasureValue.CurrentValueL1.s_Value);
        this.L2ITv.setText(tmpMeasureValue.CurrentValueL2.s_Value);
        this.L3ITv.setText(tmpMeasureValue.CurrentValueL3.s_Value);
        this.L1AngleITv.setText(tmpMeasureValue.CurrentAngleL1.s_Value);
        this.L2AngleITv.setText(tmpMeasureValue.CurrentAngleL2.s_Value);
        this.L3AngleITv.setText(tmpMeasureValue.CurrentAngleL3.s_Value);
        this.L1THDITv.setText(tmpMeasureValue.THDI1.s_Value);
        this.L2THDITv.setText(tmpMeasureValue.THDI2.s_Value);
        this.L3THDITv.setText(tmpMeasureValue.THDI3.s_Value);
        this.L1CFITv.setText(tmpMeasureValue.CFI1.s_Value);
        this.L2CFITv.setText(tmpMeasureValue.CFI2.s_Value);
        this.L3CFITv.setText(tmpMeasureValue.CFI3.s_Value);
        this.L1PFTv.setText(tmpMeasureValue.PowerFactorL1.s_Value);
        this.L2PFTv.setText(tmpMeasureValue.PowerFactorL2.s_Value);
        this.L3PFTv.setText(tmpMeasureValue.PowerFactorL3.s_Value);
        this.SumPFTv.setText(tmpMeasureValue.PowerFactorTotal.s_Value);
        this.L1PTv.setText(tmpMeasureValue.ActivePowerL1.s_Value);
        this.L2PTv.setText(tmpMeasureValue.ActivePowerL2.s_Value);
        this.L3PTv.setText(tmpMeasureValue.ActivePowerL3.s_Value);
        this.SumPTv.setText(tmpMeasureValue.ActivePowerTotal.s_Value);
        this.L1QTv.setText(tmpMeasureValue.ReactivePowerL1.s_Value);
        this.L2QTv.setText(tmpMeasureValue.ReactivePowerL2.s_Value);
        this.L3QTv.setText(tmpMeasureValue.ReactivePowerL3.s_Value);
        this.SumQTv.setText(tmpMeasureValue.ReactivePowerTotal.s_Value);
        this.L1STv.setText(tmpMeasureValue.ApparentPowerL1.s_Value);
        this.L2STv.setText(tmpMeasureValue.ApparentPowerL2.s_Value);
        this.L3STv.setText(tmpMeasureValue.ApparentPowerL3.s_Value);
        this.SumSTv.setText(tmpMeasureValue.ApparentPowerTotal.s_Value);
        this.UNTv.setText(tmpMeasureValue.VoltageLN.s_Value);
        this.INTv.setText(tmpMeasureValue.CurrentLN.s_Value);
        this.FTv.setText(tmpMeasureValue.Frequency.s_Value);
        this.PSEQUTv.setText(tmpMeasureValue.VoltagePhaseSequence);
        this.PSEQITv.setText(tmpMeasureValue.CurrentPhaseSequence);
        this.RbL1UTv.setText(tmpMeasureValue.VoltageValueL1L2.s_Value);
        this.RbL2UTv.setText(tmpMeasureValue.VoltageValueL2L3.s_Value);
        this.RbL3UTv.setText(tmpMeasureValue.VoltageValueL3L1.s_Value);
        this.RbL1AngleUTv.setText(tmpMeasureValue.VoltageAngleL1L2.s_Value);
        this.RbL2AngleUTv.setText(tmpMeasureValue.VoltageAngleL2L3.s_Value);
        this.RbL3AngleUTv.setText(tmpMeasureValue.VoltageAngleL3L1.s_Value);
        this.RbL1AngleUITv.setText(tmpMeasureValue.AngleU12I1.s_Value);
        this.RbL2AngleUITv.setText(tmpMeasureValue.AngleU23I2.s_Value);
        this.RbL3AngleUITv.setText(tmpMeasureValue.AngleU31I3.s_Value);
        this.RbL1UNTv.setText(tmpMeasureValue.VoltageValueL1.s_Value);
        this.RbL2UNTv.setText(tmpMeasureValue.VoltageValueL2.s_Value);
        this.RbL3UNTv.setText(tmpMeasureValue.VoltageValueL3.s_Value);
        this.RbL1AngleUNTv.setText(tmpMeasureValue.VoltageAngleL1.s_Value);
        this.RbL2AngleUNTv.setText(tmpMeasureValue.VoltageAngleL2.s_Value);
        this.RbL3AngleUNTv.setText(tmpMeasureValue.VoltageAngleL3.s_Value);
        this.RbL1AngleUINTv.setText(tmpMeasureValue.VoltageCurrentAngleL1.s_Value);
        this.RbL2AngleUINTv.setText(tmpMeasureValue.VoltageCurrentAngleL2.s_Value);
        this.RbL3AngleUINTv.setText(tmpMeasureValue.VoltageCurrentAngleL3.s_Value);
        this.RbL1ITv.setText(tmpMeasureValue.CurrentValueL1.s_Value);
        this.RbL2ITv.setText(tmpMeasureValue.CurrentValueL2.s_Value);
        this.RbL3ITv.setText(tmpMeasureValue.CurrentValueL3.s_Value);
        this.RbL1AngleITv.setText(tmpMeasureValue.CurrentAngleL1.s_Value);
        this.RbL2AngleITv.setText(tmpMeasureValue.CurrentAngleL2.s_Value);
        this.RbL3AngleITv.setText(tmpMeasureValue.CurrentAngleL3.s_Value);
        this.RbL1PTv.setText(tmpMeasureValue.ActivePowerL1.s_Value);
        this.RbL2PTv.setText(tmpMeasureValue.ActivePowerL2.s_Value);
        this.RbL3PTv.setText(tmpMeasureValue.ActivePowerL3.s_Value);
        this.RbSumPTv.setText(tmpMeasureValue.ActivePowerTotal.s_Value);
        this.RbL1QTv.setText(tmpMeasureValue.ReactivePowerL1.s_Value);
        this.RbL2QTv.setText(tmpMeasureValue.ReactivePowerL2.s_Value);
        this.RbL3QTv.setText(tmpMeasureValue.ReactivePowerL3.s_Value);
        this.RbSumQTv.setText(tmpMeasureValue.ReactivePowerTotal.s_Value);
        this.RbL1STv.setText(tmpMeasureValue.ApparentPowerL1.s_Value);
        this.RbL2STv.setText(tmpMeasureValue.ApparentPowerL2.s_Value);
        this.RbL3STv.setText(tmpMeasureValue.ApparentPowerL3.s_Value);
        this.RbSumSTv.setText(tmpMeasureValue.ApparentPowerTotal.s_Value);
        this.RbL1PFTv.setText(tmpMeasureValue.PowerFactorL1.s_Value);
        this.RbL2PFTv.setText(tmpMeasureValue.PowerFactorL2.s_Value);
        this.RbL3PFTv.setText(tmpMeasureValue.PowerFactorL3.s_Value);
        this.RbSumPFTv.setText(tmpMeasureValue.PowerFactorTotal.s_Value);
        this.RbFTv.setText(tmpMeasureValue.Frequency.s_Value);
        this.RbPSeqUTv.setText(tmpMeasureValue.VoltagePhaseSequence);
        this.RbPSeqITv.setText(tmpMeasureValue.CurrentPhaseSequence);
    }

    private void initData() {
        this.m_Wiring = -1;
        int refreshTime = Preferences.getInt(Preferences.Key.REFRESHTIME, 1000);
        if (this.readDataThread != null) {
            killThread(this.readDataThread);
        }
        this.readDataThread = new TimerThread((long) refreshTime, new ISleepCallback() {
            /* class com.clou.rs350.ui.fragment.realtimefragment.RealtimeBasicFragment.AnonymousClass3 */
            int count = 0;

            @Override // com.clou.rs350.task.ISleepCallback
            public void onSleepAfterToDo() {
                if (!RealtimeBasicFragment.this.m_Hold) {
                    RealtimeBasicFragment.this.messageManager.getData();
                    switch (this.count) {
                        case 3:
                            RealtimeBasicFragment.this.messageManager.getSpecialData();
                            break;
                        case 6:
                            RealtimeBasicFragment.this.messageManager.getWiringCheck();
                            this.count = 0;
                            break;
                    }
                    this.count++;
                }
            }
        }, "Get Data Thread");
        this.readDataThread.start();
    }

    @OnClick({R.id.btn_style, R.id.real_basic_p_p_button, R.id.real_basic_p_n_button, R.id.btn_txr_ratio, R.id.btn_hold})
    public void onClick(View v) {
        boolean z = false;
        switch (v.getId()) {
            case R.id.btn_txr_ratio:
                new TrxRatioPopWindow(this.m_Context).showRatioPopWindow(v, ClouData.getInstance().getSiteData(), null);
                return;
            case R.id.btn_hold:
                if (!this.m_Hold) {
                    z = true;
                }
                this.m_Hold = z;
                this.btn_Hold.setText(this.m_Hold ? R.string.text_continue : R.string.text_hold);
                return;
            case R.id.btn_style:
                this.m_Style++;
                setStyle();
                return;
            case R.id.real_basic_p_p_button:
                setLayout(false);
                return;
            case R.id.real_basic_p_n_button:
                setLayout(true);
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private void setStyle() {
        switch (this.m_Style) {
            case 1:
                findViewById(R.id.realtimebasic).setVisibility(8);
                findViewById(R.id.layout_whole).setVisibility(0);
                return;
            default:
                findViewById(R.id.realtimebasic).setVisibility(0);
                findViewById(R.id.layout_whole).setVisibility(8);
                this.m_Style = 0;
                return;
        }
    }

    private boolean isPPLayout() {
        return this.ppLinearlayout.getVisibility() == 0;
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private void setLayout(boolean is3P) {
        int i;
        int i2 = 8;
        LinearLayout linearLayout = this.ppLinearlayout;
        if (is3P) {
            i = 0;
        } else {
            i = 8;
        }
        linearLayout.setVisibility(i);
        LinearLayout linearLayout2 = this.pnLinearlayout;
        if (!is3P) {
            i2 = 0;
        }
        linearLayout2.setVisibility(i2);
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private void setTitle(boolean is3P) {
        int i = R.string.text_phase_null;
        this.u23TitleTv.setText(this.m_Context.getResources().getString(is3P ? R.string.text_phase_null : R.string.text_l23));
        this.u31TitleTv.setText(this.m_Context.getResources().getString(is3P ? R.string.text_l32 : R.string.text_l31));
        Resources resources = this.m_Context.getResources();
        if (!is3P) {
            i = R.string.text_phase_2;
        }
        this.i2TitleTv.setText(resources.getString(i));
    }

    @Override // android.support.v4.app.Fragment, com.clou.rs350.ui.fragment.BaseFragment
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initView();
    }

    private void initView() {
    }

    @Override // com.clou.rs350.handler.HandlerSwitchView.IOnSelectCallback
    public void onSelectedChanged(int index) {
    }

    @Override // com.clou.rs350.handler.HandlerSwitchView.IOnSelectCallback
    public void onClickView(View view) {
    }
}
