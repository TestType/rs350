package com.clou.rs350.ui.view.infoview;

import android.content.Context;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;

import com.clou.rs350.R;
import com.clou.rs350.db.dao.MeterTypeDao;
import com.clou.rs350.db.model.MeterType;
import com.clou.rs350.db.model.sequence.FieldItem;
import com.clou.rs350.db.model.sequence.FieldSetting;
import com.clou.rs350.ui.dialog.HintDialog;
import com.clou.rs350.ui.dialog.NumericDialog;
import com.clou.rs350.ui.popwindow.ListPopWindow;
import com.clou.rs350.utils.DoubleClick;
import com.clou.rs350.utils.FieldSettingUtil;
import com.clou.rs350.utils.OtherUtils;
import com.itextpdf.text.pdf.PdfObject;

import java.util.ArrayList;
import java.util.List;

public class MeterTypeView implements View.OnClickListener {
    Button btn_ActiveConstantUnit;
    Button btn_ReactiveConstantUnit;
    LinearLayout btn_Select;
    RadioButton[] chk_WiringTypes;
    TextView contentView = null;
    int[] m_Constants = new int[3];
    Context m_Context = null;
    MeterTypeDao m_Dao;
    FieldSetting m_FieldSetting;
    int m_Function = 0;
    boolean m_HasSearch = false;
    MeterType m_Info;
    private DoubleClick selectClick = null;
    List<TextView[]> txtList;
    TextView txt_ActiveAccuracy;
    TextView txt_ActiveConstant;
    TextView txt_BasicCurrent;
    TextView txt_MaximumCurrent;
    TextView txt_MeterManufacturer;
    TextView txt_NominalFrequency;
    TextView txt_NominalVoltage;
    TextView txt_ReactiveAccuracy;
    TextView txt_ReactiveConstant;
    EditText txt_Type;
    View view_ModifyEnable;

    public MeterTypeView(Context context, View view, MeterType typeInfo, int Function) {
        this.m_Context = context;
        this.m_Dao = new MeterTypeDao(this.m_Context);
        this.m_Info = typeInfo;
        this.txt_Type = (EditText) view.findViewById(R.id.txt_type);
        this.txt_Type.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            /* class com.clou.rs350.ui.view.infoview.MeterTypeView.AnonymousClass1 */

            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if ((actionId == 6 || actionId == 5) && 3 != MeterTypeView.this.m_Function) {
                    String id = MeterTypeView.this.txt_Type.getText().toString();
                    MeterTypeView.this.searchData(id);
                    MeterTypeView.this.txt_Type.setSelection(id.length());
                }
                Log.i("actionId", new StringBuilder(String.valueOf(actionId)).toString());
                return false;
            }
        });
        this.btn_Select = (LinearLayout) view.findViewById(R.id.btn_select);
        this.btn_Select.setOnClickListener(this);
        this.txt_MeterManufacturer = (TextView) view.findViewById(R.id.txt_meter_manufacturer);
        this.chk_WiringTypes = new RadioButton[]{(RadioButton) view.findViewById(R.id.chk_wiring1), (RadioButton) view.findViewById(R.id.chk_wiring6), (RadioButton) view.findViewById(R.id.chk_wiring2), (RadioButton) view.findViewById(R.id.chk_wiring3), (RadioButton) view.findViewById(R.id.chk_wiring4), (RadioButton) view.findViewById(R.id.chk_wiring5)};
        this.txt_BasicCurrent = (TextView) view.findViewById(R.id.txt_basic_current);
        this.txt_BasicCurrent.setOnClickListener(this);
        this.txt_MaximumCurrent = (TextView) view.findViewById(R.id.txt_maximum_current);
        this.txt_MaximumCurrent.setOnClickListener(this);
        this.txt_NominalVoltage = (TextView) view.findViewById(R.id.txt_nominal_voltage);
        this.txt_NominalVoltage.setOnClickListener(this);
        this.txt_NominalFrequency = (TextView) view.findViewById(R.id.txt_nominal_frequency);
        this.txt_NominalFrequency.setOnClickListener(this);
        this.txt_ActiveAccuracy = (TextView) view.findViewById(R.id.txt_active_accuracy);
        this.txt_ActiveAccuracy.setOnClickListener(this);
        this.txt_ReactiveAccuracy = (TextView) view.findViewById(R.id.txt_reactive_accuracy);
        this.txt_ReactiveAccuracy.setOnClickListener(this);
        this.txt_ActiveConstant = (TextView) view.findViewById(R.id.txt_active_constant);
        this.txt_ActiveConstant.setOnClickListener(this);
        this.btn_ActiveConstantUnit = (Button) view.findViewById(R.id.btn_active_constant_unit);
        this.btn_ActiveConstantUnit.setOnClickListener(this);
        this.txt_ReactiveConstant = (TextView) view.findViewById(R.id.txt_reactive_constant);
        this.txt_ReactiveConstant.setOnClickListener(this);
        this.btn_ReactiveConstantUnit = (Button) view.findViewById(R.id.btn_reactive_constant_unit);
        this.btn_ReactiveConstantUnit.setOnClickListener(this);
        this.view_ModifyEnable = view.findViewById(R.id.txt_enable);
        setFunction(Function);
        this.selectClick = new DoubleClick(new DoubleClick.OnDoubleClick() {
            /* class com.clou.rs350.ui.view.infoview.MeterTypeView.AnonymousClass2 */

            private void searchKey(String condition) {
                List<String> tmpNames = MeterTypeView.this.m_Dao.queryAllKey(condition);
                ListPopWindow.showListPopwindow(MeterTypeView.this.m_Context, (String[]) tmpNames.toArray(new String[tmpNames.size()]), MeterTypeView.this.txt_Type, new View.OnClickListener() {
                    /* class com.clou.rs350.ui.view.infoview.MeterTypeView.AnonymousClass2.AnonymousClass1 */

                    public void onClick(View v) {
                        MeterTypeView.this.searchData(((TextView) v).getText().toString());
                    }
                });
            }

            @Override // com.clou.rs350.utils.DoubleClick.OnDoubleClick
            public void doubleClick() {
                searchKey(MeterTypeView.this.txt_Type.getText().toString());
            }

            @Override // com.clou.rs350.utils.DoubleClick.OnDoubleClick
            public void singleClick() {
                searchKey(PdfObject.NOTHING);
            }
        });
        initTxtList();
        showData();
    }

    private void initTxtList() {
        this.txtList = new ArrayList();
        this.txtList.add(new TextView[]{this.txt_MeterManufacturer});
        this.txtList.add(new TextView[]{this.txt_BasicCurrent});
        this.txtList.add(new TextView[]{this.txt_MaximumCurrent});
        this.txtList.add(new TextView[]{this.txt_NominalVoltage});
        this.txtList.add(new TextView[]{this.txt_NominalFrequency});
        this.txtList.add(new TextView[]{this.txt_ActiveAccuracy, this.txt_ReactiveAccuracy});
    }

    public void refreshFieldSetting() {
        this.m_FieldSetting = new FieldSettingUtil(this.m_Context).getFieldSetting("MeterType");
        for (int i = 0; i < this.txtList.size(); i++) {
            FieldItem tmpItem = this.m_FieldSetting.Items.get(this.m_FieldSetting.ItemKeys.get(i));
            TextView[] tmpTxtView = this.txtList.get(i);
            for (int k = 0; k < tmpTxtView.length; k++) {
                if (1 == tmpItem.RequiredSetting) {
                    tmpTxtView[k].setHint(R.string.text_required_field);
                } else {
                    tmpTxtView[k].setHint(PdfObject.NOTHING);
                }
            }
        }
        if (!OtherUtils.isEmpty(this.txt_ActiveAccuracy)) {
            this.txt_ActiveConstant.setHint(R.string.text_required_field);
            this.txt_ReactiveAccuracy.setHint(PdfObject.NOTHING);
        } else {
            this.txt_ActiveConstant.setHint(PdfObject.NOTHING);
        }
        if (!OtherUtils.isEmpty(this.txt_ReactiveAccuracy)) {
            this.txt_ReactiveConstant.setHint(R.string.text_required_field);
            this.txt_ActiveAccuracy.setHint(PdfObject.NOTHING);
            return;
        }
        this.txt_ReactiveConstant.setHint(PdfObject.NOTHING);
    }

    public void setFunction(int function) {
        boolean z;
        boolean z2;
        boolean z3;
        boolean z4 = true;
        this.m_Function = function;
        this.view_ModifyEnable.setVisibility(function < 2 ? 0 : 8);
        Button button = this.btn_ActiveConstantUnit;
        if (function > 1) {
            z = true;
        } else {
            z = false;
        }
        button.setEnabled(z);
        Button button2 = this.btn_ReactiveConstantUnit;
        if (function > 1) {
            z2 = true;
        } else {
            z2 = false;
        }
        button2.setEnabled(z2);
        EditText editText = this.txt_Type;
        if (function == 1 || function == 3) {
            z3 = true;
        } else {
            z3 = false;
        }
        editText.setEnabled(z3);
        LinearLayout linearLayout = this.btn_Select;
        if (function != 1) {
            z4 = false;
        }
        linearLayout.setEnabled(z4);
    }

    public void refreshData(MeterType info) {
        this.m_Info = info;
        showData();
    }

    public MeterType getInfo() {
        parseData();
        return this.m_Info;
    }

    private void showData() {
        if (this.m_Info != null) {
            this.txt_Type.setText(this.m_Info.Type);
            this.txt_MeterManufacturer.setText(this.m_Info.Manufacturer);
            this.chk_WiringTypes[this.m_Info.Wiring].setChecked(true);
            this.txt_BasicCurrent.setText(this.m_Info.BasicCurrent.replace("A", PdfObject.NOTHING));
            this.txt_MaximumCurrent.setText(this.m_Info.MaxCurrent.replace("A", PdfObject.NOTHING));
            this.txt_NominalVoltage.setText(this.m_Info.NominalVoltage.replace("V", PdfObject.NOTHING));
            this.txt_NominalFrequency.setText(this.m_Info.NominalFrequency.replace("Hz", PdfObject.NOTHING));
            this.txt_ActiveAccuracy.setText(this.m_Info.ActiveAccuracy);
            this.txt_ReactiveAccuracy.setText(this.m_Info.ReactiveAccuracy);
            setConstantsText(this.txt_ActiveConstant, this.btn_ActiveConstantUnit, this.m_Info.ActiveConstant, 0);
            setConstantsText(this.txt_ReactiveConstant, this.btn_ReactiveConstantUnit, this.m_Info.ReactiveConstant, 1);
            refreshFieldSetting();
        }
    }

    private void parseData() {
        this.m_Info.Type = this.txt_Type.getText().toString();
        this.m_Info.Manufacturer = this.txt_MeterManufacturer.getText().toString();
        int i = 0;
        while (true) {
            if (i >= this.chk_WiringTypes.length) {
                break;
            } else if (this.chk_WiringTypes[i].isChecked()) {
                this.m_Info.WiringType = this.chk_WiringTypes[i].getText().toString();
                this.m_Info.Wiring = i;
                break;
            } else {
                i++;
            }
        }
        this.m_Info.BasicCurrent = String.valueOf(this.txt_BasicCurrent.getText().toString()) + "A";
        this.m_Info.MaxCurrent = String.valueOf(this.txt_MaximumCurrent.getText().toString()) + "A";
        this.m_Info.NominalVoltage = String.valueOf(this.txt_NominalVoltage.getText().toString()) + "V";
        this.m_Info.NominalFrequency = String.valueOf(this.txt_NominalFrequency.getText().toString()) + "Hz";
        this.m_Info.ActiveAccuracy = this.txt_ActiveAccuracy.getText().toString();
        this.m_Info.ReactiveAccuracy = this.txt_ReactiveAccuracy.getText().toString();
        this.m_Info.ActiveConstant = String.valueOf(this.txt_ActiveConstant.getText().toString()) + this.btn_ActiveConstantUnit.getText().toString();
        this.m_Info.ReactiveConstant = String.valueOf(this.txt_ReactiveConstant.getText().toString()) + this.btn_ReactiveConstantUnit.getText().toString();
    }

    public String getKey() {
        return this.txt_Type.getText().toString();
    }

    public boolean getHasSearch() {
        return this.m_HasSearch;
    }

    public boolean hasSetParams() {
        boolean result = true;
        String strHint = PdfObject.NOTHING;
        for (int i = 0; i < this.txtList.size(); i++) {
            FieldItem tmpItem = this.m_FieldSetting.Items.get(this.m_FieldSetting.ItemKeys.get(i));
            if (1 == tmpItem.RequiredSetting) {
                boolean tmpResult = false;
                TextView[] tmpTxtView = this.txtList.get(i);
                int k = 0;
                while (true) {
                    if (k >= tmpTxtView.length) {
                        break;
                    } else if (!OtherUtils.isEmpty(tmpTxtView[k])) {
                        tmpResult = true;
                        break;
                    } else {
                        k++;
                    }
                }
                if (!tmpResult) {
                    result = false;
                    strHint = String.valueOf(strHint) + this.m_Context.getResources().getString(tmpItem.Id) + " " + this.m_Context.getResources().getString(R.string.text_is_empty) + "\r\n";
                }
            }
        }
        if (!OtherUtils.isEmpty(this.txt_ActiveAccuracy) && OtherUtils.isEmpty(this.txt_ActiveConstant)) {
            result = false;
            strHint = String.valueOf(strHint) + this.m_Context.getResources().getString(R.string.text_active_constant) + " " + this.m_Context.getResources().getString(R.string.text_is_empty) + "\r\n";
        }
        if (!OtherUtils.isEmpty(this.txt_ReactiveAccuracy) && OtherUtils.isEmpty(this.txt_ReactiveConstant)) {
            result = false;
            strHint = strHint + this.m_Context.getResources().getString(R.string.text_reactive_constant) + " " + this.m_Context.getResources().getString(R.string.text_is_empty) + "\r\n";
        }
        if (!result) {
            new HintDialog(this.m_Context, strHint.substring(0, strHint.length() - 2)).show();
        }
        return result;
    }

    private void setConstantsText(TextView constantValue, Button constantBtn, String constant, int power) {
        String value = PdfObject.NOTHING;
        String unit = PdfObject.NOTHING;
        if (!TextUtils.isEmpty(constant)) {
            int index = OtherUtils.getnumberCount(constant);
            value = constant.substring(0, index);
            unit = constant.substring(index);
        }
        if (!TextUtils.isEmpty(value)) {
            constantValue.setText(value);
            constantBtn.setText(unit);
            this.m_Constants[power] = getConstantsImp(value, unit);
        }
    }

    private int getConstantsImp(String value, String unit) {
        double data = OtherUtils.parseDouble(value);
        if (isStartWithImp(unit)) {
            return OtherUtils.convertToInt(Double.valueOf(data));
        }
        return OtherUtils.convertToInt(Double.valueOf(calculateConstant(data)));
    }

    private boolean isStartWithImp(String text) {
        if (TextUtils.isEmpty(text)) {
            return false;
        }
        return text.startsWith("imp");
    }

    private double calculateConstant(double constant) {
        if (0.0d == constant) {
            return 0.0d;
        }
        return (1.0d / Double.valueOf(constant).doubleValue()) * 1000.0d;
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private void searchData(String type) {
        this.m_Info = this.m_Dao.queryObjectByType(type);
        if (this.m_Info != null) {
            this.m_HasSearch = true;
        } else {
            this.m_HasSearch = false;
            this.m_Info = new MeterType();
            this.m_Info.Type = type;
        }
        refreshData(this.m_Info);
    }

    public void onClick(View v) {
        String text;
        String text2;
        switch (v.getId()) {
            case R.id.btn_select:
                this.selectClick.click();
                return;
            case R.id.btn_active_constant_unit:
                Button btn = (Button) v;
                String text3 = btn.getText().toString();
                this.m_Constants[0] = getConstantsImp(this.txt_ActiveConstant.getText().toString(), this.btn_ActiveConstantUnit.getText().toString());
                String text4 = text3.startsWith("imp") ? "Wh/imp" : "imp/kWh";
                btn.setText(text4);
                if (text4.startsWith("imp")) {
                    text2 = new StringBuilder(String.valueOf(this.m_Constants[0])).toString();
                } else {
                    text2 = new StringBuilder(String.valueOf(calculateConstant((double) this.m_Constants[0]))).toString();
                }
                this.txt_ActiveConstant.setText(text2);
                return;
            case R.id.btn_reactive_constant_unit:
                Button btn2 = (Button) v;
                String text5 = btn2.getText().toString();
                this.m_Constants[1] = getConstantsImp(this.txt_ReactiveConstant.getText().toString(), this.btn_ReactiveConstantUnit.getText().toString());
                String text6 = text5.startsWith("imp") ? "varh/imp" : "imp/kvarh";
                btn2.setText(text6);
                if (text6.startsWith("imp")) {
                    text = new StringBuilder(String.valueOf(this.m_Constants[1])).toString();
                } else {
                    text = new StringBuilder(String.valueOf(calculateConstant((double) this.m_Constants[1]))).toString();
                }
                this.txt_ReactiveConstant.setText(text);
                return;
            default:
                NumericDialog numDialog = new NumericDialog(this.m_Context);
                numDialog.setMinus(false);
                numDialog.showMyDialog((TextView) v);
                numDialog.setOnDoneClick(new View.OnClickListener() {
                    /* class com.clou.rs350.ui.view.infoview.MeterTypeView.AnonymousClass3 */

                    public void onClick(View v) {
                        MeterTypeView.this.refreshFieldSetting();
                    }
                });
                return;
        }
    }
}
