package com.clou.rs350.ui.fragment.errorfragment;

import android.view.View;
import android.widget.TextView;
import com.clou.rs350.manager.MessageManager;
import com.clou.rs350.ui.dialog.NumericDialog;
import com.clou.rs350.ui.fragment.BaseFragment;

public class BaseErrorFragment extends BaseFragment implements MessageManager.IMessageListener {
    private int flag = 0;
    private int func_Mstate = 0;

    public int getFunc_Mstate() {
        return this.func_Mstate;
    }

    public void rememberFunc_Mstate(int func_Mstate2) {
        this.func_Mstate = func_Mstate2;
    }

    public int getFlag() {
        return this.flag;
    }

    public void setFlag(int flag2) {
        this.flag = flag2;
    }

    public boolean start() {
        return true;
    }

    public void doContinue() {
    }

    public boolean stop() {
        return true;
    }

    public boolean hasSetParams() {
        return true;
    }

    public void showInputDialog(TextView editText, View.OnClickListener doneClick) {
        showInputDialog(editText, doneClick, true);
    }

    public void showInputDialog(TextView editText, View.OnClickListener doneClick, boolean minus) {
        NumericDialog numeric = new NumericDialog(this.m_Context);
        numeric.setMinus(minus);
        numeric.showMyDialog(editText);
        numeric.setCancelable(false);
        numeric.setCanceledOnTouchOutside(false);
        numeric.setOnDoneClick(doneClick);
    }

    public void showInputDialog(TextView editText, View.OnClickListener doneClick, double min) {
        NumericDialog numeric = new NumericDialog(this.m_Context);
        numeric.setMin(min);
        numeric.showMyDialog(editText);
        numeric.setCancelable(false);
        numeric.setCanceledOnTouchOutside(false);
        numeric.setOnDoneClick(doneClick);
    }

    public void showInputDialog(TextView editText, View.OnClickListener doneClick, double min, double max) {
        NumericDialog numeric = new NumericDialog(this.m_Context);
        numeric.setMin(min);
        numeric.setMax(max);
        numeric.showMyDialog(editText);
        numeric.setCancelable(false);
        numeric.setCanceledOnTouchOutside(false);
        numeric.setOnDoneClick(doneClick);
    }
}
