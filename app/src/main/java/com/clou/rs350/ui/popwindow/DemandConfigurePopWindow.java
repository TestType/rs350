package com.clou.rs350.ui.popwindow;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.RadioButton;
import android.widget.TextView;

import com.clou.rs350.Preferences;
import com.clou.rs350.R;
import com.clou.rs350.db.model.DemandTest;
import com.clou.rs350.model.ClouData;
import com.clou.rs350.ui.dialog.NumericDialog;

public class DemandConfigurePopWindow extends BasePopWindow implements View.OnClickListener, CompoundButton.OnCheckedChangeListener {
    String Meter1Accuracy;
    String Meter2Accuracy;
    String Meter3Accuracy;
    RadioButton chk_MW;
    CheckBox chk_Meter2Enable;
    CheckBox chk_Meter3Enable;
    RadioButton chk_StartStopSwitch;
    RadioButton chk_Tablet;
    RadioButton chk_W;
    RadioButton chk_kW;
    Context mContext;
    View.OnClickListener mOnClick;
    DemandTest[] m_Demand;
    int temp_PQ_Flag;
    int testMeterCount;
    TextView txt_Meter1Accuracy;
    TextView txt_Meter2Accuracy;
    TextView txt_Meter3Accuracy;

    public DemandConfigurePopWindow(Context context) {
        super(context);
        this.mContext = context;
        initView();
    }

    private void initView() {
        View conentView = ((LayoutInflater) this.mContext.getSystemService("layout_inflater")).inflate(R.layout.pop_window_demandconfigure, (ViewGroup) null);
        conentView.findViewById(R.id.ok).setOnClickListener(this);
        conentView.findViewById(R.id.cancel).setOnClickListener(this);
        this.txt_Meter1Accuracy = (TextView) conentView.findViewById(R.id.txt_meter1_accuracy);
        this.txt_Meter1Accuracy.setOnClickListener(this);
        this.txt_Meter2Accuracy = (TextView) conentView.findViewById(R.id.txt_meter2_accuracy);
        this.txt_Meter2Accuracy.setOnClickListener(this);
        this.txt_Meter3Accuracy = (TextView) conentView.findViewById(R.id.txt_meter3_accuracy);
        this.txt_Meter3Accuracy.setOnClickListener(this);
        this.chk_Meter2Enable = (CheckBox) conentView.findViewById(R.id.chk_meter2);
        this.chk_Meter3Enable = (CheckBox) conentView.findViewById(R.id.chk_meter3);
        this.chk_W = (RadioButton) conentView.findViewById(R.id.chk_w);
        this.chk_kW = (RadioButton) conentView.findViewById(R.id.chk_kw);
        this.chk_MW = (RadioButton) conentView.findViewById(R.id.chk_mw);
        this.chk_Tablet = (RadioButton) conentView.findViewById(R.id.chk_tablet);
        this.chk_StartStopSwitch = (RadioButton) conentView.findViewById(R.id.chk_start_stop_switch);
        setContentView(conentView);
        setWidth(this.mContext.getResources().getDimensionPixelSize(R.dimen.dp_450));
        setHeight(this.mContext.getResources().getDimensionPixelSize(R.dimen.dp_230));
        setFocusable(true);
        setOutsideTouchable(true);
        update();
        setBackgroundDrawable(new ColorDrawable(0));
        setAnimationStyle(16973826);
    }

    private void praseTmpValue() {
        boolean z;
        boolean z2;
        switch (this.temp_PQ_Flag) {
            case 0:
                this.Meter1Accuracy = ClouData.getInstance().getMeterBaseInfo()[0].ActiveAccuracy.trim();
                this.Meter2Accuracy = ClouData.getInstance().getMeterBaseInfo()[1].ActiveAccuracy.trim();
                this.Meter3Accuracy = ClouData.getInstance().getMeterBaseInfo()[2].ActiveAccuracy.trim();
                break;
            case 1:
                this.Meter1Accuracy = ClouData.getInstance().getMeterBaseInfo()[0].ReactiveAccuracy.trim();
                this.Meter2Accuracy = ClouData.getInstance().getMeterBaseInfo()[1].ReactiveAccuracy.trim();
                this.Meter3Accuracy = ClouData.getInstance().getMeterBaseInfo()[2].ReactiveAccuracy.trim();
                break;
            case 2:
                this.Meter1Accuracy = ClouData.getInstance().getMeterBaseInfo()[0].ApparentAccuracy.trim();
                this.Meter2Accuracy = ClouData.getInstance().getMeterBaseInfo()[1].ApparentAccuracy.trim();
                this.Meter3Accuracy = ClouData.getInstance().getMeterBaseInfo()[2].ApparentAccuracy.trim();
                break;
        }
        CheckBox checkBox = this.chk_Meter2Enable;
        if ((this.testMeterCount & 2) == 2) {
            z = true;
        } else {
            z = false;
        }
        checkBox.setChecked(z);
        CheckBox checkBox2 = this.chk_Meter3Enable;
        if ((this.testMeterCount & 4) == 4) {
            z2 = true;
        } else {
            z2 = false;
        }
        checkBox2.setChecked(z2);
        switch (this.m_Demand[0].Unit) {
            case 0:
                this.chk_W.setChecked(true);
                break;
            case 1:
                this.chk_kW.setChecked(true);
                break;
            case 2:
                this.chk_MW.setChecked(true);
                break;
        }
        if (Preferences.getInt(Preferences.Key.DEMANDTESTMODE, 0) == 0) {
            this.chk_Tablet.setChecked(true);
        } else {
            this.chk_StartStopSwitch.setChecked(true);
        }
        showValue();
    }

    private void showValue() {
        this.txt_Meter1Accuracy.setText(this.Meter1Accuracy);
        this.txt_Meter2Accuracy.setText(this.Meter2Accuracy);
        this.txt_Meter3Accuracy.setText(this.Meter3Accuracy);
    }

    private void praseValue() {
        int i = 2;
        int i2 = 0;
        switch (this.temp_PQ_Flag) {
            case 0:
                ClouData.getInstance().getMeterBaseInfo()[0].ActiveAccuracy = this.txt_Meter1Accuracy.getText().toString();
                ClouData.getInstance().getMeterBaseInfo()[1].ActiveAccuracy = this.txt_Meter2Accuracy.getText().toString();
                ClouData.getInstance().getMeterBaseInfo()[2].ActiveAccuracy = this.txt_Meter3Accuracy.getText().toString();
                break;
            case 1:
                ClouData.getInstance().getMeterBaseInfo()[0].ReactiveAccuracy = this.txt_Meter1Accuracy.getText().toString();
                ClouData.getInstance().getMeterBaseInfo()[1].ReactiveAccuracy = this.txt_Meter2Accuracy.getText().toString();
                ClouData.getInstance().getMeterBaseInfo()[2].ReactiveAccuracy = this.txt_Meter3Accuracy.getText().toString();
                break;
            case 2:
                ClouData.getInstance().getMeterBaseInfo()[0].ApparentAccuracy = this.txt_Meter1Accuracy.getText().toString();
                ClouData.getInstance().getMeterBaseInfo()[1].ApparentAccuracy = this.txt_Meter2Accuracy.getText().toString();
                ClouData.getInstance().getMeterBaseInfo()[2].ApparentAccuracy = this.txt_Meter3Accuracy.getText().toString();
                break;
        }
        int tmpUnit = 1;
        if (this.chk_W.isChecked()) {
            tmpUnit = 0;
        } else if (this.chk_MW.isChecked()) {
            tmpUnit = 2;
        }
        this.m_Demand[0].Unit = tmpUnit;
        this.m_Demand[1].Unit = tmpUnit;
        this.m_Demand[2].Unit = tmpUnit;
        if (!this.chk_Meter2Enable.isChecked()) {
            i = 0;
        }
        this.testMeterCount = (this.chk_Meter3Enable.isChecked() ? 4 : 0) | i | 1;
        ClouData.getInstance().setTestMeterCount(this.testMeterCount);
        if (!this.chk_Tablet.isChecked()) {
            i2 = 1;
        }
        Preferences.putInt(Preferences.Key.DEMANDTESTMODE, i2);
    }

    public void showPopWindow(View currentClick, int PQFlag, DemandTest[] Data, View.OnClickListener onclick) {
        this.mOnClick = onclick;
        this.temp_PQ_Flag = PQFlag;
        this.m_Demand = Data;
        this.testMeterCount = ClouData.getInstance().getTestMeterCount();
        praseTmpValue();
        if (!isShowing()) {
            int[] location = new int[2];
            currentClick.getLocationOnScreen(location);
            showAtLocation(currentClick, 0, location[0] - getWidth(), location[1]);
            setSoftInputMode(16);
            return;
        }
        dismiss();
    }

    public void onClick(View v) {
        NumericDialog nd = new NumericDialog(this.mContext);
        switch (v.getId()) {
            case R.id.ok:
                praseValue();
                if (this.mOnClick != null) {
                    this.mOnClick.onClick(v);
                }
                if (isShowing()) {
                    dismiss();
                    return;
                }
                return;
            case R.id.cancel:
                if (isShowing()) {
                    dismiss();
                    return;
                }
                return;
            case R.id.dp_setting:
            case R.id.tp_setting:
            case R.id.layout_test_3meter:
            default:
                return;
            case R.id.txt_meter1_accuracy:
            case R.id.txt_meter2_accuracy:
            case R.id.txt_meter3_accuracy:
                nd.txtLimit = 4;
                nd.showMyDialog((TextView) v);
                return;
        }
    }

    public void onCheckedChanged(CompoundButton v, boolean isChecked) {
        v.getId();
    }
}
