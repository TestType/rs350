package com.clou.rs350.ui.fragment.recordfragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.clou.rs350.R;
import com.clou.rs350.db.model.DigitalMeterTest;
import com.clou.rs350.ui.fragment.BaseFragment;
import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.view.annotation.ViewInject;

public class DigitalMeterTestingRecordFragment extends BaseFragment {
    private static final String TAG = "ErrorFragment";
    @ViewInject(R.id.btn_start)
    private Button btn_Start;
    private TextView[] e11Tv = new TextView[3];
    private TextView[] e21Tv = new TextView[3];
    private TextView[] e31Tv = new TextView[3];
    private TextView[] e41Tv = new TextView[3];
    private TextView[] e51Tv = new TextView[3];
    private TextView[] eA1Tv = new TextView[3];
    private TextView[] es1Tv = new TextView[3];
    private DigitalMeterTest m_ErrorTests;
    @ViewInject(R.id.txt_meterp)
    private TextView txt_MeterP;
    @ViewInject(R.id.txt_meterq)
    private TextView txt_MeterQ;
    @ViewInject(R.id.txt_meters)
    private TextView txt_MeterS;

    public DigitalMeterTestingRecordFragment(DigitalMeterTest Data) {
        this.m_ErrorTests = Data;
    }

    @Override // android.support.v4.app.Fragment
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        this.fragmentView = inflater.inflate(R.layout.fragment_record_digital_meter_testing, (ViewGroup) null);
        ViewUtils.inject(this, this.fragmentView);
        return this.fragmentView;
    }

    @Override // android.support.v4.app.Fragment, com.clou.rs350.ui.fragment.BaseFragment
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initView();
    }

    private void initView() {
        int[] e1id = {R.id.txt_pe1, R.id.txt_qe1, R.id.txt_se1};
        int[] e2id = {R.id.txt_pe2, R.id.txt_qe2, R.id.txt_se2};
        int[] e3id = {R.id.txt_pe3, R.id.txt_qe3, R.id.txt_se3};
        int[] e4id = {R.id.txt_pe4, R.id.txt_qe4, R.id.txt_se4};
        int[] e5id = {R.id.txt_pe5, R.id.txt_qe5, R.id.txt_se5};
        int[] eavgid = {R.id.txt_peavg, R.id.txt_qeavg, R.id.txt_seavg};
        int[] esid = {R.id.txt_pes, R.id.txt_qes, R.id.txt_ses};
        for (int i = 0; i < 3; i++) {
            this.e11Tv[i] = (TextView) findViewById(e1id[i]);
            this.e21Tv[i] = (TextView) findViewById(e2id[i]);
            this.e31Tv[i] = (TextView) findViewById(e3id[i]);
            this.e41Tv[i] = (TextView) findViewById(e4id[i]);
            this.e51Tv[i] = (TextView) findViewById(e5id[i]);
            this.eA1Tv[i] = (TextView) findViewById(eavgid[i]);
            this.es1Tv[i] = (TextView) findViewById(esid[i]);
        }
    }

    @Override // android.support.v4.app.Fragment, com.clou.rs350.ui.fragment.BaseFragment
    public void onResume() {
        super.onResume();
        showData(this.m_ErrorTests);
    }

    @Override // android.support.v4.app.Fragment, com.clou.rs350.ui.fragment.BaseFragment
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override // android.support.v4.app.Fragment
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    private void showData(DigitalMeterTest errors) {
        for (int i = 0; i < 3; i++) {
            this.e11Tv[i].setText(errors.getError1(i));
            this.e21Tv[i].setText(errors.getError2(i));
            this.e31Tv[i].setText(errors.getError3(i));
            this.e41Tv[i].setText(errors.getError4(i));
            this.e51Tv[i].setText(errors.getError5(i));
            this.eA1Tv[i].setText(errors.getErrorAverage(i, errors.hasErrorCount));
            this.es1Tv[i].setText(errors.getErrorStandard(i, errors.hasErrorCount));
        }
        this.txt_MeterP.setText(errors.MeterP.s_Value);
        this.txt_MeterQ.setText(errors.MeterQ.s_Value);
        this.txt_MeterS.setText(errors.MeterS.s_Value);
    }
}
