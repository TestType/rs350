package com.clou.rs350.ui.view;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.DashPathEffect;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PathEffect;
import android.util.AttributeSet;

import com.clou.rs350.R;
import com.clou.rs350.db.model.WaveValue;
import com.clou.rs350.model.ClouData;
import com.itextpdf.text.pdf.ColumnText;
import com.itextpdf.text.pdf.PdfObject;

public class MyWaveView extends MyZoomView {
    private static final String TAG = "MyWaveView";
    private int[] Colors;
    private float arrow = ColumnText.GLOBAL_SPACE_CHAR_RATIO;
    boolean isfirst = true;
    private Context m_Context;
    private int m_LineColor = -1;
    private float m_StrokeWidth = 3.0f;
    private int m_Type = 0;
    private Paint myPaint = null;
    private PathEffect newEffects = null;
    private PathEffect oldEffects = null;
    private Paint paint = null;
    private boolean showI1;
    private boolean showI2;
    private boolean showI3;
    private boolean showU1;
    private boolean showU2;
    private boolean showU3;
    private float textSize = ColumnText.GLOBAL_SPACE_CHAR_RATIO;
    private WaveValue valDouble_il1 = new WaveValue();
    private WaveValue valDouble_il2 = new WaveValue();
    private WaveValue valDouble_il3 = new WaveValue();
    private WaveValue valDouble_ul1 = new WaveValue();
    private WaveValue valDouble_ul2 = new WaveValue();
    private WaveValue valDouble_ul3 = new WaveValue();
    private float x = 30.0f;
    private float x_length = ColumnText.GLOBAL_SPACE_CHAR_RATIO;
    private float y = 30.0f;
    private float y_height = ColumnText.GLOBAL_SPACE_CHAR_RATIO;

    public MyWaveView(Context context) {
        super(context);
        this.m_Context = context;
        initData();
    }

    public MyWaveView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        initData();
        this.m_Context = context;
    }

    public MyWaveView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.m_Context = context;
        initData();
    }

    @Override // com.clou.rs350.ui.view.MyZoomView
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        float width = getScaleWidth();
        float height = getScaleHeight();
        this.x = width / 15.0f;
        this.y = height / 15.0f;
        this.y_height = height - (this.y * 2.0f);
        this.x_length = width - (this.x * 2.0f);
        this.arrow = this.y_height / 20.0f;
        if (this.isfirst) {
            this.myPaint = new Paint();
            this.myPaint.setColor(this.m_LineColor);
            this.myPaint.setAntiAlias(false);
            this.myPaint.setStrokeWidth(3.0f);
            this.oldEffects = this.myPaint.getPathEffect();
            this.newEffects = new DashPathEffect(new float[]{1.0f, 2.0f, 4.0f, 8.0f}, 1.0f);
            this.isfirst = false;
        }
        this.textSize = this.y_height / 13.0f;
        if (this.textSize > getValue(R.dimen.sp_10, 17.0f)) {
            this.textSize = getValue(R.dimen.sp_10, 17.0f);
        }
        this.myPaint.setTextSize(this.textSize);
        drawVerticalLine();
        draw_6_line();
    }

    private void initData() {
        this.showU1 = true;
        this.showI1 = true;
        this.showU2 = true;
        this.showI2 = true;
        this.showU3 = true;
        this.showI3 = true;
        this.Colors = ClouData.getInstance().getSettingBasic().PhaseColors;
    }

    private void drawVerticalLine() {
        this.myPaint.setColor(this.m_LineColor);
        this.myPaint.setPathEffect(this.oldEffects);
        drawLine(this.x, this.y, this.x, this.y_height + this.y, this.myPaint);
        drawLine(this.x, (this.y_height / 2.0f) + this.y, this.x_length + this.x, (this.y_height / 2.0f) + this.y, this.myPaint);
        drawLine(this.x, this.y, this.x - (this.arrow / 1.5f), this.arrow + this.y, this.myPaint);
        drawLine(this.x, this.y, (this.arrow / 1.5f) + this.x, this.arrow + this.y, this.myPaint);
        drawLine(this.x_length + this.x, this.y, this.x_length + this.x, this.y_height + this.y, this.myPaint);
        drawLine(this.x_length + this.x, this.y, (this.x + this.x_length) - (this.arrow / 1.5f), this.arrow + this.y, this.myPaint);
        drawLine(this.x_length + this.x, this.y, (this.arrow / 1.5f) + this.x + this.x_length, this.arrow + this.y, this.myPaint);
        this.myPaint.setColor(-1426063361);
        this.myPaint.setPathEffect(this.newEffects);
        draw_new_line(new Double[]{Double.valueOf(1.0d), Double.valueOf(1.0d)}, 1.0d);
        draw_new_line(new Double[]{Double.valueOf(-1.0d), Double.valueOf(-1.0d)}, 1.0d);
    }

    private synchronized void drawLine(Canvas canvas, int color, PathEffect effects, float startX, float startY, float stopX, float stopY) {
        if (this.paint == null) {
            this.paint = new Paint();
            this.paint.setStyle(Paint.Style.STROKE);
            this.paint.setStrokeWidth(this.m_StrokeWidth);
        }
        this.paint.setColor(color);
        Path path = new Path();
        path.moveTo(startX, startY);
        path.lineTo(stopX, stopY);
        this.paint.setPathEffect(effects);
        canvas.drawPath(path, this.paint);
    }

    private void draw_6_line() {
        if (this.m_Type == 0) {
            double max_0 = 0.0d;
            double max_1 = 0.0d;
            String strMax_0 = PdfObject.NOTHING;
            String strMax_1 = PdfObject.NOTHING;
            if (this.valDouble_ul1 != null && this.showU1) {
                max_0 = this.valDouble_ul1.max.d_Value;
                strMax_0 = this.valDouble_ul1.max.s_Value.replace("-", PdfObject.NOTHING);
            }
            if (this.valDouble_ul2 != null && this.showU2 && max_0 < this.valDouble_ul2.max.d_Value) {
                max_0 = this.valDouble_ul2.max.d_Value;
                strMax_0 = this.valDouble_ul2.max.s_Value.replace("-", PdfObject.NOTHING);
            }
            if (this.valDouble_ul3 != null && this.showU3 && max_0 < this.valDouble_ul3.max.d_Value) {
                max_0 = this.valDouble_ul3.max.d_Value;
                strMax_0 = this.valDouble_ul3.max.s_Value.replace("-", PdfObject.NOTHING);
            }
            if (this.valDouble_il1 != null && this.showI1) {
                max_1 = this.valDouble_il1.max.d_Value;
                strMax_1 = this.valDouble_il1.max.s_Value.replace("-", PdfObject.NOTHING);
            }
            if (this.valDouble_il2 != null && this.showI2 && max_1 < this.valDouble_il2.max.d_Value) {
                max_1 = this.valDouble_il2.max.d_Value;
                strMax_1 = this.valDouble_il2.max.s_Value.replace("-", PdfObject.NOTHING);
            }
            if (this.valDouble_il3 != null && this.showI3 && max_1 < this.valDouble_il3.max.d_Value) {
                max_1 = this.valDouble_il3.max.d_Value;
                strMax_1 = this.valDouble_il3.max.s_Value.replace("-", PdfObject.NOTHING);
            }
            if (max_0 != 0.0d) {
                this.myPaint.setColor(this.m_LineColor);
                this.myPaint.setTextAlign(Paint.Align.LEFT);
                drawText(strMax_0, this.x + this.arrow, (this.y + (this.y_height * 0.1f)) - 5.0f, this.myPaint);
                drawText("-" + strMax_0, this.x + this.arrow, this.y + (this.y_height * 0.9f) + this.textSize, this.myPaint);
            }
            if (max_1 != 0.0d) {
                this.myPaint.setColor(this.m_LineColor);
                this.myPaint.setTextAlign(Paint.Align.RIGHT);
                drawText(strMax_1, (this.x + this.x_length) - this.arrow, (this.y + (this.y_height * 0.1f)) - 5.0f, this.myPaint);
                drawText("-" + strMax_1, (this.x + this.x_length) - this.arrow, this.y + (this.y_height * 0.9f) + this.textSize, this.myPaint);
            }
            if (this.showU1) {
                this.myPaint.setColor(this.Colors[0]);
                this.myPaint.setPathEffect(this.oldEffects);
                if (0.0d != max_0) {
                    draw_new_line(this.valDouble_ul1.Wave, this.valDouble_ul1.max.d_Value / max_0);
                } else {
                    draw_new_line(this.valDouble_ul1.Wave, 1.0d);
                }
            }
            if (this.showI1) {
                this.myPaint.setColor(this.Colors[3]);
                this.myPaint.setPathEffect(this.newEffects);
                if (0.0d != max_1) {
                    draw_new_line(this.valDouble_il1.Wave, this.valDouble_il1.max.d_Value / max_1);
                } else {
                    draw_new_line(this.valDouble_il1.Wave, 1.0d);
                }
            }
            if (this.showU2) {
                this.myPaint.setColor(this.Colors[1]);
                this.myPaint.setPathEffect(this.oldEffects);
                if (0.0d != max_0) {
                    draw_new_line(this.valDouble_ul2.Wave, this.valDouble_ul2.max.d_Value / max_0);
                } else {
                    draw_new_line(this.valDouble_ul2.Wave, 1.0d);
                }
            }
            if (this.showI2) {
                this.myPaint.setColor(this.Colors[4]);
                this.myPaint.setPathEffect(this.newEffects);
                if (0.0d != max_1) {
                    draw_new_line(this.valDouble_il2.Wave, this.valDouble_il2.max.d_Value / max_1);
                } else {
                    draw_new_line(this.valDouble_il2.Wave, 1.0d);
                }
            }
            if (this.showU3) {
                this.myPaint.setColor(this.Colors[2]);
                this.myPaint.setPathEffect(this.oldEffects);
                if (0.0d != max_0) {
                    draw_new_line(this.valDouble_ul3.Wave, this.valDouble_ul3.max.d_Value / max_0);
                } else {
                    draw_new_line(this.valDouble_ul3.Wave, 1.0d);
                }
            }
            if (this.showI3) {
                this.myPaint.setColor(this.Colors[5]);
                this.myPaint.setPathEffect(this.newEffects);
                if (0.0d != max_1) {
                    draw_new_line(this.valDouble_il3.Wave, this.valDouble_il3.max.d_Value / max_1);
                } else {
                    draw_new_line(this.valDouble_il3.Wave, 1.0d);
                }
            }
        } else {
            double max_02 = 0.0d;
            String strMax_02 = PdfObject.NOTHING;
            if (this.valDouble_ul1 != null && this.showU1) {
                max_02 = this.valDouble_ul1.max.d_Value;
                strMax_02 = this.valDouble_ul1.max.s_Value.replace("-", PdfObject.NOTHING);
            }
            if (this.valDouble_ul2 != null && this.showU2 && max_02 < this.valDouble_ul2.max.d_Value) {
                max_02 = this.valDouble_ul2.max.d_Value;
                strMax_02 = this.valDouble_ul2.max.s_Value.replace("-", PdfObject.NOTHING);
            }
            if (this.valDouble_ul3 != null && this.showU3 && max_02 < this.valDouble_ul3.max.d_Value) {
                max_02 = this.valDouble_ul3.max.d_Value;
                strMax_02 = this.valDouble_ul3.max.s_Value.replace("-", PdfObject.NOTHING);
            }
            if (this.valDouble_il1 != null && this.showI1 && max_02 < this.valDouble_il1.max.d_Value) {
                max_02 = this.valDouble_il1.max.d_Value;
                strMax_02 = this.valDouble_il1.max.s_Value.replace("-", PdfObject.NOTHING);
            }
            if (this.valDouble_il2 != null && this.showI2 && max_02 < this.valDouble_il2.max.d_Value) {
                max_02 = this.valDouble_il2.max.d_Value;
                strMax_02 = this.valDouble_il2.max.s_Value.replace("-", PdfObject.NOTHING);
            }
            if (this.valDouble_il3 != null && this.showI3 && max_02 < this.valDouble_il3.max.d_Value) {
                max_02 = this.valDouble_il3.max.d_Value;
                strMax_02 = this.valDouble_il3.max.s_Value.replace("-", PdfObject.NOTHING);
            }
            if (max_02 != 0.0d) {
                this.myPaint.setColor(this.m_LineColor);
                this.myPaint.setTextAlign(Paint.Align.LEFT);
                drawText(strMax_02.replace("var", "W"), this.x + this.arrow, (this.y + (this.y_height * 0.1f)) - 5.0f, this.myPaint);
                drawText("-" + strMax_02.replace("var", "W"), this.x + this.arrow, this.y + (this.y_height * 0.9f) + this.textSize, this.myPaint);
                this.myPaint.setTextAlign(Paint.Align.RIGHT);
                drawText(strMax_02.replace("W", "var"), (this.x + this.x_length) - this.arrow, (this.y + (this.y_height * 0.1f)) - 5.0f, this.myPaint);
                drawText("-" + strMax_02.replace("W", "var"), (this.x + this.x_length) - this.arrow, this.y + (this.y_height * 0.9f) + this.textSize, this.myPaint);
            }
            if (this.showU1) {
                this.myPaint.setColor(this.Colors[0]);
                this.myPaint.setPathEffect(this.oldEffects);
                if (0.0d != max_02) {
                    draw_new_line(this.valDouble_ul1.Wave, this.valDouble_ul1.max.d_Value / max_02);
                } else {
                    draw_new_line(this.valDouble_ul1.Wave, 1.0d);
                }
            }
            if (this.showI1) {
                this.myPaint.setColor(this.Colors[3]);
                this.myPaint.setPathEffect(this.newEffects);
                if (0.0d != max_02) {
                    draw_new_line(this.valDouble_il1.Wave, this.valDouble_il1.max.d_Value / max_02);
                } else {
                    draw_new_line(this.valDouble_il1.Wave, 1.0d);
                }
            }
            if (this.showU2) {
                this.myPaint.setColor(this.Colors[1]);
                this.myPaint.setPathEffect(this.oldEffects);
                if (0.0d != max_02) {
                    draw_new_line(this.valDouble_ul2.Wave, this.valDouble_ul2.max.d_Value / max_02);
                } else {
                    draw_new_line(this.valDouble_ul2.Wave, 1.0d);
                }
            }
            if (this.showI2) {
                this.myPaint.setColor(this.Colors[4]);
                this.myPaint.setPathEffect(this.newEffects);
                if (0.0d != max_02) {
                    draw_new_line(this.valDouble_il2.Wave, this.valDouble_il2.max.d_Value / max_02);
                } else {
                    draw_new_line(this.valDouble_il2.Wave, 1.0d);
                }
            }
            if (this.showU3) {
                this.myPaint.setColor(this.Colors[2]);
                this.myPaint.setPathEffect(this.oldEffects);
                if (0.0d != max_02) {
                    draw_new_line(this.valDouble_ul3.Wave, this.valDouble_ul3.max.d_Value / max_02);
                } else {
                    draw_new_line(this.valDouble_ul3.Wave, 1.0d);
                }
            }
            if (this.showI3) {
                this.myPaint.setColor(this.Colors[5]);
                this.myPaint.setPathEffect(this.newEffects);
                if (0.0d != max_02) {
                    draw_new_line(this.valDouble_il3.Wave, this.valDouble_il3.max.d_Value / max_02);
                } else {
                    draw_new_line(this.valDouble_il3.Wave, 1.0d);
                }
            }
        }
    }

    private void draw_new_line(Double[] y_Doubles, double scale) {
        if (!(y_Doubles == null || y_Doubles[0] == null)) {
            Double oldx = Double.valueOf((double) this.x);
            Double maxValue = getMax(y_Doubles);
            if (0.0d == scale) {
                scale = 1.0d;
            }
            double scale2 = scale * (((double) ((int) (((double) this.y_height) / 2.5d))) / maxValue.doubleValue());
            float point_length = this.x_length / ((float) (y_Doubles.length - 1));
            for (int i = 0; i < y_Doubles.length - 1; i++) {
                drawLine((float) oldx.doubleValue(), (float) ((-(y_Doubles[i].doubleValue() * scale2)) + ((double) (this.y + (this.y_height / 2.0f)))), (float) (oldx.doubleValue() + ((double) point_length)), (float) ((-(y_Doubles[i + 1].doubleValue() * scale2)) + ((double) (this.y + (this.y_height / 2.0f)))), this.myPaint);
                oldx = Double.valueOf(oldx.doubleValue() + ((double) point_length));
            }
        }
    }

    private Double getMax(Double[] array) {
        Double max = Double.valueOf(Math.abs(array[0].doubleValue()));
        for (int i = 0; i < array.length - 1; i++) {
            if (Math.abs(array[i].doubleValue()) > max.doubleValue()) {
                max = Double.valueOf(Math.abs(array[i].doubleValue()));
            }
        }
        return max;
    }

    public void showLines(boolean showU12, boolean showI12, boolean showU22, boolean showI22, boolean showU32, boolean showI32) {
        this.showU1 = showU12;
        this.showU2 = showU22;
        this.showU3 = showU32;
        this.showI1 = showI12;
        this.showI2 = showI22;
        this.showI3 = showI32;
        postInvalidate();
    }

    public void refreshData(int type, WaveValue valDouble_ul12, WaveValue valDouble_il12, WaveValue valDouble_ul22, WaveValue valDouble_il22, WaveValue valDouble_ul32, WaveValue valDouble_il32) {
        this.m_Type = type;
        this.valDouble_ul1 = valDouble_ul12;
        this.valDouble_il1 = valDouble_il12;
        this.valDouble_ul2 = valDouble_ul22;
        this.valDouble_il2 = valDouble_il22;
        this.valDouble_ul3 = valDouble_ul32;
        this.valDouble_il3 = valDouble_il32;
        postInvalidate();
    }

    public Bitmap getBitmap(int type, WaveValue valDouble_ul12, WaveValue valDouble_il12, WaveValue valDouble_ul22, WaveValue valDouble_il22, WaveValue valDouble_ul32, WaveValue valDouble_il32) {
        this.m_LineColor = -16777216;
        this.m_Width = 900;
        this.m_Height = 380;
        this.m_StrokeWidth = 2.0f;
        Bitmap mBitmap = Bitmap.createBitmap(this.m_Width, this.m_Height, Bitmap.Config.ARGB_8888);
        mBitmap.eraseColor(-1);
        Canvas mCanvas = new Canvas(mBitmap);
        this.m_Type = type;
        this.valDouble_ul1 = valDouble_ul12;
        this.valDouble_il1 = valDouble_il12;
        this.valDouble_ul2 = valDouble_ul22;
        this.valDouble_il2 = valDouble_il22;
        this.valDouble_ul3 = valDouble_ul32;
        this.valDouble_il3 = valDouble_il32;
        draw(mCanvas);
        return mBitmap;
    }
}
