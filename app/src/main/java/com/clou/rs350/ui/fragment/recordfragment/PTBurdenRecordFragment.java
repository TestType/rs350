package com.clou.rs350.ui.fragment.recordfragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.clou.rs350.R;
import com.clou.rs350.db.model.PTBurden;
import com.clou.rs350.ui.fragment.BaseFragment;
import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.view.annotation.ViewInject;

public class PTBurdenRecordFragment extends BaseFragment {
    @ViewInject(R.id.l1vtburdenva)
    private TextView L1VtBurdenVATv;
    @ViewInject(R.id.l1vtburdenw)
    private TextView L1VtBurdenWTv;
    @ViewInject(R.id.l1vtcurrent)
    private TextView L1VtCurrentTv;
    @ViewInject(R.id.l1vtnominal)
    private TextView L1VtNominalTv;
    @ViewInject(R.id.l1vtphaseangle)
    private TextView L1VtPhaseAngleTv;
    @ViewInject(R.id.l1vtsecondaryvoltage)
    private TextView L1VtSecondaryVoltageTv;
    @ViewInject(R.id.l2vtburdenva)
    private TextView L2VtBurdenVATv;
    @ViewInject(R.id.l2vtburdenw)
    private TextView L2VtBurdenWTv;
    @ViewInject(R.id.l2vtcurrent)
    private TextView L2VtCurrentTv;
    @ViewInject(R.id.l2vtnominal)
    private TextView L2VtNominalTv;
    @ViewInject(R.id.l2vtphaseangle)
    private TextView L2VtPhaseAngleTv;
    @ViewInject(R.id.l2vtsecondaryvoltage)
    private TextView L2VtSecondaryVoltageTv;
    @ViewInject(R.id.l3vtburdenva)
    private TextView L3VtBurdenVATv;
    @ViewInject(R.id.l3vtburdenw)
    private TextView L3VtBurdenWTv;
    @ViewInject(R.id.l3vtcurrent)
    private TextView L3VtCurrentTv;
    @ViewInject(R.id.l3vtnominal)
    private TextView L3VtNominalTv;
    @ViewInject(R.id.l3vtphaseangle)
    private TextView L3VtPhaseAngleTv;
    @ViewInject(R.id.l3vtsecondaryvoltage)
    private TextView L3VtSecondaryVoltageTv;
    @ViewInject(R.id.txt_vs)
    private TextView VSTv;
    @ViewInject(R.id.txt_va)
    private TextView VaTv;
    private PTBurden m_PTBurden;

    public PTBurdenRecordFragment(PTBurden data) {
        this.m_PTBurden = data;
    }

    @Override // android.support.v4.app.Fragment
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        this.fragmentView = inflater.inflate(R.layout.fragment_record_pt_burden, (ViewGroup) null);
        ViewUtils.inject(this, this.fragmentView);
        return this.fragmentView;
    }

    @Override // android.support.v4.app.Fragment, com.clou.rs350.ui.fragment.BaseFragment
    public void onResume() {
        super.onResume();
        showSavedData();
    }

    private void showSavedData() {
        this.VSTv.setText(new StringBuilder(String.valueOf(this.m_PTBurden.VoltageValueNormalSecondary.s_Value)).toString());
        this.VaTv.setText(new StringBuilder(String.valueOf(this.m_PTBurden.PTVA.s_Value)).toString());
        showData();
    }

    private void showData() {
        this.L1VtSecondaryVoltageTv.setText(this.m_PTBurden.VoltageValue[0].s_Value);
        this.L2VtSecondaryVoltageTv.setText(this.m_PTBurden.VoltageValue[1].s_Value);
        this.L3VtSecondaryVoltageTv.setText(this.m_PTBurden.VoltageValue[2].s_Value);
        this.L1VtCurrentTv.setText(this.m_PTBurden.CurrentValue[0].s_Value);
        this.L2VtCurrentTv.setText(this.m_PTBurden.CurrentValue[1].s_Value);
        this.L3VtCurrentTv.setText(this.m_PTBurden.CurrentValue[2].s_Value);
        this.L1VtPhaseAngleTv.setText(this.m_PTBurden.VoltageCurrentAngle[0].s_Value);
        this.L2VtPhaseAngleTv.setText(this.m_PTBurden.VoltageCurrentAngle[1].s_Value);
        this.L3VtPhaseAngleTv.setText(this.m_PTBurden.VoltageCurrentAngle[2].s_Value);
        this.L1VtBurdenWTv.setText(this.m_PTBurden.BurdenW[0].s_Value);
        this.L2VtBurdenWTv.setText(this.m_PTBurden.BurdenW[1].s_Value);
        this.L3VtBurdenWTv.setText(this.m_PTBurden.BurdenW[2].s_Value);
        this.L1VtBurdenVATv.setText(this.m_PTBurden.BurdenVA[0].s_Value);
        this.L2VtBurdenVATv.setText(this.m_PTBurden.BurdenVA[1].s_Value);
        this.L3VtBurdenVATv.setText(this.m_PTBurden.BurdenVA[2].s_Value);
        this.L1VtNominalTv.setText(this.m_PTBurden.BurdenPersents[0].s_Value);
        this.L2VtNominalTv.setText(this.m_PTBurden.BurdenPersents[1].s_Value);
        this.L3VtNominalTv.setText(this.m_PTBurden.BurdenPersents[2].s_Value);
    }

    @Override // android.support.v4.app.Fragment, com.clou.rs350.ui.fragment.BaseFragment
    public void onPause() {
        super.onPause();
    }
}
