package com.clou.rs350.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.clou.rs350.R;
import com.clou.rs350.device.model.ExplainedModel;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;

public class VIHarmonicListViewAdapter extends BaseAdapter {
    private Context context;
    private Double[] currentAngles;
    private Double[] currentContents;
    private ExplainedModel[] currentValues;
    private Double[] voltageAngles;
    private Double[] voltageContents;
    private ExplainedModel[] voltageValues;

    public VIHarmonicListViewAdapter(Context context2) {
        this.context = context2;
    }

    public void refreshData(ExplainedModel[] voltageValues2, Double[] voltageContents2, Double[] voltageAngles2, ExplainedModel[] currentValues2, Double[] currentContents2, Double[] currentAngles2) {
        this.voltageValues = voltageValues2;
        this.voltageContents = voltageContents2;
        this.voltageAngles = voltageAngles2;
        this.currentValues = currentValues2;
        this.currentContents = currentContents2;
        this.currentAngles = currentAngles2;
        notifyDataSetChanged();
    }

    public int getCount() {
        if (this.voltageContents != null) {
            return this.voltageContents.length;
        }
        return 0;
    }

    public Object getItem(int arg0) {
        return this.voltageContents[arg0];
    }

    public long getItemId(int arg0) {
        return 0;
    }

    public View getView(int position, View view, ViewGroup arg2) {
        ViewHolder holder;
        if (view == null) {
            view = LayoutInflater.from(this.context).inflate(R.layout.adapter_vi_harmonic, (ViewGroup) null);
            holder = new ViewHolder(view);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }
        holder.showData(position + 1, this.voltageValues[position], this.voltageContents[position], this.voltageAngles[position], this.currentValues[position], this.currentContents[position], this.currentAngles[position]);
        return view;
    }

    class ViewHolder {
        TextView currentAngleTv;
        TextView currentContentTv;
        TextView currentValueTv;
        TextView itemTv;
        TextView voltageAngleTv;
        TextView voltageContentTv;
        TextView voltageValueTv;

        ViewHolder(View v) {
            this.itemTv = (TextView) v.findViewById(R.id.vi_itemText);
            this.voltageValueTv = (TextView) v.findViewById(R.id.vi_voltage_value);
            this.voltageContentTv = (TextView) v.findViewById(R.id.vi_voltage_percent);
            this.voltageAngleTv = (TextView) v.findViewById(R.id.vi_voltage_phase);
            this.currentValueTv = (TextView) v.findViewById(R.id.vi_current_value);
            this.currentContentTv = (TextView) v.findViewById(R.id.vi_current_percent);
            this.currentAngleTv = (TextView) v.findViewById(R.id.vi_current_phase);
        }

        public void showData(int index, ExplainedModel voltageValues, Double voltageContents, Double voltageAngles, ExplainedModel currentValues, Double currentContents, Double currentAngles) {
            this.itemTv.setText(new StringBuilder(String.valueOf(index)).toString());
            this.voltageValueTv.setText(voltageValues == null ? "---" : voltageValues.getStringValue());
            DecimalFormat df = new DecimalFormat("0.00");
            DecimalFormatSymbols symbols = new DecimalFormatSymbols();
            symbols.setDecimalSeparator('.');
            df.setDecimalFormatSymbols(symbols);
            this.voltageContentTv.setText(df.format(voltageContents));
            DecimalFormat df2 = new DecimalFormat("0.000");
            df2.setDecimalFormatSymbols(symbols);
            this.voltageAngleTv.setText(df2.format(voltageAngles));
            this.currentValueTv.setText(currentValues == null ? "---" : currentValues.getStringValue());
            DecimalFormat df3 = new DecimalFormat("0.00");
            df3.setDecimalFormatSymbols(symbols);
            this.currentContentTv.setText(df3.format(currentContents));
            DecimalFormat df4 = new DecimalFormat("0.000");
            df4.setDecimalFormatSymbols(symbols);
            this.currentAngleTv.setText(df4.format(currentAngles));
        }
    }
}
