package com.clou.rs350.ui.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.TextView;

import com.clou.rs350.CLApplication;
import com.clou.rs350.R;
import com.clou.rs350.model.ClouData;
import com.clou.rs350.report.ExportReport;
import com.clou.rs350.report.LoadExportDataTool;
import com.clou.rs350.utils.DoubleClick;
import com.clou.rs350.utils.FileUtils;
import com.clou.rs350.utils.OtherUtils;
import com.itextpdf.text.pdf.PdfObject;

import java.io.File;

public class BaseSaveDialog extends Dialog {
    protected Button CancelBtn = null;
    protected TextView CustomerSerialNoTextView = null;
    protected TextView HumidityTextView = null;
    protected TextView OperatorTextView = null;
    protected TextView RemarkTextView = null;
    protected Button SaveBtn = null;
    protected TextView TemperatureTextView = null;
    protected boolean isSave = false;
    protected Context m_Context = null;
    protected DoubleClick m_DoubleClick = null;
    protected LoadingDialog m_LoadingDialog;

    public BaseSaveDialog(Context context) {
        super(context, R.style.dialog);
        requestWindowFeature(1);
        this.m_Context = context;
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    /* access modifiers changed from: protected */
    public boolean validate(TextView v, int resid) {
        boolean tmpResult = true;
        if (OtherUtils.isEmpty(v) || OtherUtils.isEmpty(v.getText().toString().trim())) {
            new HintDialog(this.m_Context, resid).show();
            v.requestFocus();
            ((InputMethodManager) this.m_Context.getSystemService("input_method")).showSoftInput(v, 0);
            tmpResult = false;
        }
        String templatePath = ClouData.getInstance().getSettingBasic().ExportTemplate;
        File tmpFile = new File(String.valueOf(CLApplication.app.getTemplateXmlPath()) + File.separator + templatePath);
        if (!OtherUtils.isEmpty(templatePath) && tmpFile.exists()) {
            return tmpResult;
        }
        SelectValueDialog tmpDialog = new SelectValueDialog(this.m_Context, FileUtils.getFileDir(CLApplication.app.getTemplateXmlPath()));
        tmpDialog.setWarning(R.string.text_select_template);
        tmpDialog.show();
        tmpDialog.setOnClick(new View.OnClickListener() {
            /* class com.clou.rs350.ui.dialog.BaseSaveDialog.AnonymousClass1 */

            public void onClick(View v) {
                ClouData.getInstance().getSettingBasic().ExportTemplate = v.getTag().toString();
                ClouData.getInstance().getSettingBasic().saveSetting();
                BaseSaveDialog.this.save();
            }
        }, null);
        tmpDialog.setTitle(R.string.text_select_template);
        return false;
    }

    /* access modifiers changed from: protected */
    public void save() {
    }

    /* access modifiers changed from: protected */
    public String Export(String CustomerSerialNo, String Times) {
        String reportPath = (String.valueOf(CustomerSerialNo) + "_" + Times).replace(" ", PdfObject.NOTHING).replace("/", PdfObject.NOTHING).replace(":", PdfObject.NOTHING);
        String templet = ClouData.getInstance().getSettingBasic().ExportTemplate;
        return new ExportReport(this.m_Context).exportPdf(templet, reportPath, new LoadExportDataTool(this.m_Context).loadData(CustomerSerialNo, new String[]{Times}));
    }

    public void dismiss() {
        super.dismiss();
    }
}
