package com.clou.rs350.ui.view;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PointF;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;

import com.clou.rs350.R;
import com.itextpdf.text.pdf.ColumnText;
import com.itextpdf.text.pdf.PdfObject;
import com.itextpdf.text.pdf.codec.wmf.MetaDo;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;

@SuppressLint({"DrawAllocation", "FloatMath"})
public class MyHarmonicView extends View {
    private static final String TAG = "HistogramView";
    private float emptyPesent = 0.25f;
    private float harmonicTimes = 63.0f;
    private Context mContext;
    private float m_TextSize = 17.0f;
    private float m_ZoomLimit = 30.0f;
    private Paint myPaint = null;
    private float numberHeight = 20.0f;
    private float numberWidth = 15.0f;
    private float oldDistx;
    private float oldDisty;
    private float pllarPesent = 0.75f;
    private int pointCount;
    private float rectHigh = ColumnText.GLOBAL_SPACE_CHAR_RATIO;
    private float rectLeftX = ColumnText.GLOBAL_SPACE_CHAR_RATIO;
    private float rectTopY = ColumnText.GLOBAL_SPACE_CHAR_RATIO;
    private float rectWidth = ColumnText.GLOBAL_SPACE_CHAR_RATIO;
    private float scalex = 1.0f;
    private float scaley = 1.0f;
    private PointF start = new PointF();
    private Double[] values = {Double.valueOf(0.5d), Double.valueOf(1.0d), Double.valueOf(0.3d), Double.valueOf(0.01d), Double.valueOf(5.0E-4d), Double.valueOf(0.5d), Double.valueOf(1.0d), Double.valueOf(0.3d), Double.valueOf(0.01d), Double.valueOf(0.26d), Double.valueOf(0.5d), Double.valueOf(1.0d), Double.valueOf(0.3d), Double.valueOf(0.01d), Double.valueOf(0.26d), Double.valueOf(0.5d), Double.valueOf(1.0d), Double.valueOf(0.3d), Double.valueOf(0.01d), Double.valueOf(0.26d), Double.valueOf(0.5d), Double.valueOf(1.0d), Double.valueOf(0.3d), Double.valueOf(0.01d), Double.valueOf(0.26d), Double.valueOf(0.5d), Double.valueOf(1.0d), Double.valueOf(0.3d), Double.valueOf(0.01d), Double.valueOf(0.26d), Double.valueOf(0.5d), Double.valueOf(1.0d), Double.valueOf(0.3d), Double.valueOf(0.01d), Double.valueOf(0.26d), Double.valueOf(0.5d), Double.valueOf(1.0d), Double.valueOf(0.3d), Double.valueOf(0.01d), Double.valueOf(0.26d), Double.valueOf(0.5d), Double.valueOf(1.0d), Double.valueOf(0.3d), Double.valueOf(0.01d), Double.valueOf(0.26d), Double.valueOf(0.5d), Double.valueOf(1.0d), Double.valueOf(0.3d), Double.valueOf(0.01d), Double.valueOf(0.26d), Double.valueOf(0.5d), Double.valueOf(1.0d), Double.valueOf(0.3d), Double.valueOf(0.01d), Double.valueOf(0.26d), Double.valueOf(0.5d), Double.valueOf(1.0d), Double.valueOf(0.3d), Double.valueOf(0.01d), Double.valueOf(0.26d), Double.valueOf(0.5d), Double.valueOf(1.0d), Double.valueOf(0.3d)};
    private PointF zoomLocation = new PointF();

    public MyHarmonicView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.mContext = context;
        init();
        this.values = null;
    }

    private void init() {
        this.myPaint = new Paint();
        this.myPaint.setAntiAlias(true);
        this.myPaint.setStrokeWidth(2.0f);
        this.myPaint.setColor(-1);
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private float getValue(int resId, float defaultValue) {
        return this.mContext == null ? defaultValue : this.mContext.getResources().getDimension(resId);
    }

    /* access modifiers changed from: protected */
    public void onDraw1(Canvas canvas) {
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        float width = (float) getWidth();
        float height = (float) getHeight();
        this.m_TextSize = height / 10.0f;
        if (this.m_TextSize > getValue(R.dimen.sp_10, 17.0f)) {
            this.m_TextSize = getValue(R.dimen.sp_10, 17.0f);
        }
        this.numberWidth = (this.m_TextSize * 0.75f) + getValue(R.dimen.dp_2, 17.0f);
        this.numberHeight = this.m_TextSize + getValue(R.dimen.dp_2, 17.0f);
        this.myPaint.setTextSize(this.m_TextSize);
        this.rectWidth = this.scalex * width;
        this.rectHigh = this.scaley * height;
        checkXY();
        Axis axis = new Axis(new MyLine(this.rectLeftX, (this.rectTopY + this.rectHigh) - this.numberHeight, this.rectWidth, false), new MyLine((this.numberWidth * 2.0f) + this.rectLeftX, this.rectTopY, this.rectHigh, true));
        axis.drawXY(canvas, this.myPaint);
        axis.drawNumber(canvas, this.myPaint);
        axis.drawAllPillar(canvas, this.myPaint);
    }

    public void refreshData(Double[] values2) {
        this.values = values2;
        postInvalidate();
    }

    /* access modifiers changed from: package-private */
    public class MyPoint {
        float x;
        float y;

        MyPoint(float x2, float y2) {
            this.x = x2;
            this.y = y2;
        }
    }

    /* access modifiers changed from: package-private */
    public class MyLine {
        float arrowOffset = 7.0f;
        boolean isVertical;
        float startX;
        float startY;
        float stopX;
        float stopY;

        MyLine(float startX2, float startY2, float length, boolean isVertical2) {
            this.startX = startX2;
            this.startY = startY2;
            this.isVertical = isVertical2;
            if (isVertical2) {
                this.stopX = startX2;
                this.stopY = startY2 + length;
                return;
            }
            this.stopX = startX2 + length;
            this.stopY = startY2;
        }

        /* access modifiers changed from: package-private */
        public void draw(Canvas canvas, Paint paint) {
            canvas.drawLine(this.startX, this.startY, this.stopX, this.stopY, paint);
            if (this.isVertical) {
                canvas.drawLine(this.startX, this.startY, this.arrowOffset + this.startX, this.arrowOffset + this.startY, paint);
                canvas.drawLine(this.startX, this.startY, this.startX - this.arrowOffset, this.arrowOffset + this.startY, paint);
                return;
            }
            canvas.drawLine(this.stopX, this.stopY, this.stopX - this.arrowOffset, this.stopY - this.arrowOffset, paint);
            canvas.drawLine(this.stopX, this.stopY, this.stopX - this.arrowOffset, this.arrowOffset + this.stopY, paint);
        }
    }

    class Axis {
        MyLine xLine;
        MyLine yLine;

        Axis(MyLine xLine2, MyLine yLine2) {
            this.xLine = xLine2;
            this.yLine = yLine2;
        }

        /* access modifiers changed from: package-private */
        public void drawXY(Canvas canvas, Paint paint) {
            if (this.xLine != null && this.yLine != null) {
                paint.setColor(-1);
                this.xLine.draw(canvas, paint);
                this.yLine.draw(canvas, paint);
            }
        }

        /* access modifiers changed from: package-private */
        public void drawNumberX1(Canvas canvas, Paint paint) {
            MyPoint originPoint = new MyPoint(this.yLine.startX + 2.0f, this.xLine.startY);
            float width = ((this.xLine.stopX - originPoint.x) - this.xLine.arrowOffset) / MyHarmonicView.this.harmonicTimes;
            float emptyWidth = width * MyHarmonicView.this.emptyPesent;
            float pllarWidth = width * MyHarmonicView.this.pllarPesent;
            float textX = originPoint.x;
            float textY = originPoint.y + MyHarmonicView.this.numberHeight;
            for (int i = 1; i <= MyHarmonicView.this.values.length; i++) {
                if (i == 1) {
                    textX += (pllarWidth / 2.0f) + emptyWidth;
                } else {
                    textX += width;
                }
                if (i % 5 == 0) {
                    String text = String.valueOf(i);
                    if (text.length() == 1) {
                        text = " " + text;
                    }
                    canvas.drawText(text, textX - (MyHarmonicView.this.numberWidth / 1.5f), textY, paint);
                }
                if (i == 5) {
                    Log.i(MyHarmonicView.TAG, "i == 5 width :" + ((textX - (MyHarmonicView.this.numberWidth / 1.5f)) - originPoint.x));
                }
            }
        }

        /* access modifiers changed from: package-private */
        public void drawNumber(Canvas canvas, Paint paint) {
            paint.setColor(-1);
            paint.setTextAlign(Paint.Align.CENTER);
            MyPoint originPoint = new MyPoint(this.yLine.startX + 2.0f, this.xLine.startY);
            canvas.drawText("0", originPoint.x - (MyHarmonicView.this.numberWidth * 0.75f), originPoint.y + (MyHarmonicView.this.numberHeight * 0.85f), paint);
            float xLength = (this.xLine.stopX - originPoint.x) - this.xLine.arrowOffset;
            float yWidth = ((originPoint.y - this.yLine.startY) - MyHarmonicView.this.numberHeight) / 4.0f;
            float x1 = originPoint.x - (MyHarmonicView.this.numberWidth + 3.0f);
            float y1 = originPoint.y + MyHarmonicView.this.numberHeight;
            String[] texts = {"0.01", "0.1", "1", "10", "100"};
            MyHarmonicView.this.getValue(R.dimen.dp_14, 25.0f);
            for (int i = 1; i <= 4; i++) {
                y1 -= yWidth;
                canvas.drawText(texts[i], x1, y1 - (MyHarmonicView.this.numberHeight / 1.6f), paint);
                paint.setStrokeWidth(1.0f);
                canvas.drawLine(originPoint.x - 2.0f, y1 - MyHarmonicView.this.numberHeight, originPoint.x + xLength, y1 - MyHarmonicView.this.numberHeight, paint);
                paint.setStrokeWidth(2.0f);
            }
            canvas.drawText("%", x1, this.yLine.startY + (MyHarmonicView.this.numberHeight * 0.75f), paint);
        }

        /* access modifiers changed from: package-private */
        public void drawAllPillar(Canvas canvas, Paint paint) {
            if (!(MyHarmonicView.this.values == null || MyHarmonicView.this.values.length == 0)) {
                MyPoint originPoint = new MyPoint(this.yLine.startX + 2.0f, this.xLine.startY);
                float xLength = (this.xLine.stopX - originPoint.x) - this.xLine.arrowOffset;
                float yHeight = originPoint.y - this.yLine.startY;
                float width = xLength / MyHarmonicView.this.harmonicTimes;
                float emptyWidth = width * MyHarmonicView.this.emptyPesent;
                float pllarWidth = width * MyHarmonicView.this.pllarPesent;
                MyPoint leftTop = new MyPoint(originPoint.x, originPoint.y - (yHeight / 2.0f));
                MyPoint rightBottom = new MyPoint(originPoint.x, originPoint.y);
                for (int i = 0; i < MyHarmonicView.this.values.length; i++) {
                    if (i == 0) {
                        leftTop.x += emptyWidth;
                        rightBottom.x = leftTop.x + pllarWidth;
                    } else {
                        leftTop.x += width;
                        rightBottom.x = leftTop.x + pllarWidth;
                    }
                    if ((i + 1) % 5 == 0) {
                        paint.setColor(-7829368);
                        String text = new StringBuilder().append(i + 1).toString();
                        if (text.length() == 1) {
                            text = " " + text;
                        }
                        drawBottomText(canvas, leftTop, rightBottom, paint, text);
                    } else {
                        paint.setColor(-1);
                    }
                    leftTop.y = originPoint.y - getValueByPersent(MyHarmonicView.this.values[i].doubleValue());
                    canvas.drawRect(leftTop.x, leftTop.y, rightBottom.x, rightBottom.y, paint);
                    DecimalFormat df = new DecimalFormat("#.##");
                    DecimalFormatSymbols symbols = new DecimalFormatSymbols();
                    symbols.setDecimalSeparator('.');
                    df.setDecimalFormatSymbols(symbols);
                    drawTopText(canvas, leftTop, rightBottom, paint, String.valueOf(df.format(MyHarmonicView.this.values[i])) + "%", pllarWidth / 2.0f);
                }
            }
        }

        /* access modifiers changed from: package-private */
        public void drawBottomText(Canvas canvas, MyPoint leftTop, MyPoint rightBottom, Paint paint, String text) {
            paint.setColor(-1);
            paint.setTextSize(MyHarmonicView.this.m_TextSize);
            paint.setTextAlign(Paint.Align.CENTER);
            canvas.drawText(text, (rightBottom.x + leftTop.x) / 2.0f, rightBottom.y + (MyHarmonicView.this.numberHeight * 0.85f), paint);
            paint.setColor(-7829368);
        }

        /* access modifiers changed from: package-private */
        public void drawTopText(Canvas canvas, MyPoint leftTop, MyPoint rightBottom, Paint paint, String text, float varTextSize) {
            paint.setColor(-1);
            if (varTextSize > MyHarmonicView.this.m_TextSize) {
                varTextSize = MyHarmonicView.this.m_TextSize;
            }
            paint.setTextSize(varTextSize);
            paint.setTextAlign(Paint.Align.CENTER);
            canvas.drawText(text, (rightBottom.x + leftTop.x) / 2.0f, leftTop.y - (0.2f * varTextSize), paint);
            paint.setColor(-7829368);
            paint.setTextSize(MyHarmonicView.this.m_TextSize);
        }

        /* access modifiers changed from: package-private */
        public float getValueByPersent(double persent) {
            float yItem = ((new MyPoint(this.yLine.startX + 3.0f, this.xLine.startY).y - this.yLine.startY) - MyHarmonicView.this.numberHeight) / 4.0f;
            if (persent > 0.0d && persent <= 0.1d) {
                return yItem * ((((float) persent) - ColumnText.GLOBAL_SPACE_CHAR_RATIO) / 0.1f);
            }
            if (persent > 0.1d && persent <= 1.0d) {
                return yItem + (((((float) persent) - 0.1f) / 0.9f) * yItem);
            }
            if (persent > 1.0d && persent <= 10.0d) {
                return (2.0f * yItem) + (((((float) persent) - 1.0f) / 9.0f) * yItem);
            }
            if (persent > 10.0d) {
                return (3.0f * yItem) + (((((float) persent) - 10.0f) / 90.0f) * yItem);
            }
            return 0.9f;
        }
    }

    private void checkXY() {
        if (this.rectLeftX > ColumnText.GLOBAL_SPACE_CHAR_RATIO) {
            this.rectLeftX = ColumnText.GLOBAL_SPACE_CHAR_RATIO;
        }
        float rightX = (float) getWidth();
        if (this.rectLeftX + this.rectWidth < rightX) {
            this.rectLeftX = rightX - this.rectWidth;
        }
        if (this.rectTopY > ColumnText.GLOBAL_SPACE_CHAR_RATIO) {
            this.rectTopY = ColumnText.GLOBAL_SPACE_CHAR_RATIO;
        }
        float bottomY = (float) getHeight();
        if (this.rectTopY + this.rectHigh < bottomY) {
            this.rectTopY = bottomY - this.rectHigh;
        }
    }

    public boolean onTouchEvent(MotionEvent event) {
        if (this.pointCount > event.getPointerCount()) {
            this.start = getLocation(event);
        }
        this.pointCount = event.getPointerCount();
        if (this.pointCount == 1) {
            this.oldDistx = ColumnText.GLOBAL_SPACE_CHAR_RATIO;
        }
        switch (event.getAction()) {
            case 0:
            case 5:
            case MetaDo.META_SETRELABS /*{ENCODED_INT: 261}*/:
                if (this.pointCount >= 2) {
                    this.oldDistx = Math.abs(event.getX(0) - event.getX(1));
                    this.oldDisty = Math.abs(event.getY(0) - event.getY(1));
                    if (this.m_ZoomLimit > this.oldDistx) {
                        this.oldDistx = this.m_ZoomLimit;
                    }
                    if (this.m_ZoomLimit > this.oldDisty) {
                        this.oldDisty = this.m_ZoomLimit;
                    }
                    this.zoomLocation = getLocation(event);
                }
                this.start = getLocation(event);
                break;
            case 2:
                doZoom(event);
                doMove(event);
                break;
            case 3:
                Log.i(PdfObject.NOTHING, PdfObject.NOTHING);
                break;
        }
        return true;
    }

    private PointF getLocation(MotionEvent event) {
        float x = ColumnText.GLOBAL_SPACE_CHAR_RATIO;
        float y = ColumnText.GLOBAL_SPACE_CHAR_RATIO;
        int count = event.getPointerCount();
        for (int i = 0; i < count; i++) {
            x += event.getX(i);
            y += event.getY(i);
        }
        return new PointF(x / ((float) count), y / ((float) count));
    }

    private void doMove(MotionEvent event) {
        PointF stop = getLocation(event);
        this.rectLeftX += stop.x - this.start.x;
        this.rectTopY += stop.y - this.start.y;
        postInvalidate();
        this.start = stop;
    }

    private void doZoom(MotionEvent event) {
        if (event.getPointerCount() >= 2) {
            float x = Math.abs(event.getX(0) - event.getX(1));
            if (this.m_ZoomLimit < x) {
                this.scalex *= x / this.oldDistx;
                if (this.scalex > 500.0f) {
                    this.scalex = 500.0f;
                }
                if (this.scalex < 1.0f) {
                    this.scalex = 1.0f;
                }
                this.oldDistx = x;
            }
            float y = Math.abs(event.getY(0) - event.getY(1));
            if (this.m_ZoomLimit < y) {
                this.scaley *= y / this.oldDisty;
                if (this.scaley > 500.0f) {
                    this.scaley = 500.0f;
                }
                if (this.scaley < 1.0f) {
                    this.scaley = 1.0f;
                }
                this.oldDisty = y;
            }
            computerMoveByZoom(this.scalex, this.scaley);
            postInvalidate();
        }
    }

    private void computerMoveByZoom(float scalex2, float scaley2) {
        float rectWidth2 = ((float) getWidth()) * scalex2;
        float rectHigh2 = ((float) getHeight()) * scaley2;
        float trans_x = (this.rectWidth - rectWidth2) * ((this.zoomLocation.x - this.rectLeftX) / this.rectWidth);
        float trans_y = (this.rectHigh - rectHigh2) * ((this.zoomLocation.y - this.rectTopY) / this.rectHigh);
        this.rectLeftX += trans_x;
        this.rectTopY += trans_y;
    }
}
