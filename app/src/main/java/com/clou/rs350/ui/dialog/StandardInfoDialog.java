package com.clou.rs350.ui.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;

import com.clou.rs350.R;
import com.clou.rs350.ui.view.infoview.StandardInfoView;

public class StandardInfoDialog extends Dialog {
    private View.OnClickListener cancleClick = null;
    private Context m_Context = null;
    StandardInfoView m_InfoView;
    private View.OnClickListener okClick = null;

    public StandardInfoDialog(Context context) {
        super(context, R.style.dialog);
        this.m_Context = context;
        requestWindowFeature(1);
        setCanceledOnTouchOutside(true);
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_standard_info);
        WindowManager.LayoutParams lp = getWindow().getAttributes();
        lp.width = getContext().getResources().getDisplayMetrics().widthPixels;
        lp.height = getContext().getResources().getDisplayMetrics().heightPixels;
        getWindow().setAttributes(lp);
        this.m_InfoView = new StandardInfoView(this.m_Context, findViewById(R.id.layout_standard_info));
        this.m_InfoView.setOnClick(R.string.text_close, new View.OnClickListener() {
            /* class com.clou.rs350.ui.dialog.StandardInfoDialog.AnonymousClass1 */

            public void onClick(View v) {
                StandardInfoDialog.this.dismiss();
            }
        });
    }

    public void setOnClick(View.OnClickListener onClickListener, View.OnClickListener cancleClick2) {
        this.okClick = onClickListener;
        this.cancleClick = cancleClick2;
    }
}
