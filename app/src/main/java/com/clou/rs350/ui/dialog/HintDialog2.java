package com.clou.rs350.ui.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.clou.rs350.R;

public class HintDialog2 extends Dialog implements View.OnClickListener {
    private Button btn_OK = null;
    private Context m_Context = null;
    private int m_DescriptId = 0;
    private int m_TitleId = 0;
    private View.OnClickListener okClick = null;
    private TextView txt_Descript = null;
    private TextView txt_Title = null;

    public HintDialog2(Context context) {
        super(context, R.style.dialog);
        requestWindowFeature(1);
        setCanceledOnTouchOutside(true);
        this.m_Context = context;
    }

    public HintDialog2(Context context, int txt_Id, int txt_DescribeId) {
        super(context, R.style.dialog);
        requestWindowFeature(1);
        setCanceledOnTouchOutside(true);
        this.m_Context = context;
        this.m_TitleId = txt_Id;
        this.m_DescriptId = txt_DescribeId;
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_hint2);
        this.txt_Title = (TextView) findViewById(R.id.txt_title);
        this.txt_Descript = (TextView) findViewById(R.id.txt_describe);
        this.txt_Descript.setMovementMethod(ScrollingMovementMethod.getInstance());
        if (this.m_TitleId != 0) {
            this.txt_Title.setText(this.m_TitleId);
        }
        if (this.m_DescriptId != 0) {
            this.txt_Descript.setText(this.m_DescriptId);
        }
        this.btn_OK = (Button) findViewById(R.id.btn_ok);
        this.btn_OK.setOnClickListener(this);
    }

    @Override // android.app.Dialog
    public void setTitle(CharSequence title) {
        this.txt_Title.setText(title.toString());
    }

    @Override // android.app.Dialog
    public void setTitle(int titleId) {
        this.txt_Title.setText(titleId);
    }

    public void setDescript(CharSequence descript) {
        this.txt_Descript.setText(descript.toString());
    }

    public void setOkText(CharSequence text) {
        this.btn_OK.setText(text);
    }

    public void setOkText(int id) {
        this.btn_OK.setText(id);
    }

    public void setPositiveButton(int textId, View.OnClickListener listener) {
        this.btn_OK.setText(textId);
        this.okClick = listener;
    }

    public void onClick(View v) {
        dismiss();
        switch (v.getId()) {
            case R.id.btn_ok:
                if (this.okClick != null) {
                    this.okClick.onClick(v);
                }
                dismiss();
                return;
            default:
                return;
        }
    }

    public void setOnClick(View.OnClickListener onClickListener) {
        this.okClick = onClickListener;
    }
}
