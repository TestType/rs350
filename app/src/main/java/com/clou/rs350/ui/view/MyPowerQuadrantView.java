package com.clou.rs350.ui.view;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.AttributeSet;

import com.clou.rs350.R;
import com.clou.rs350.model.ClouData;
import com.clou.rs350.utils.AngleUtil;
import com.itextpdf.text.pdf.ColumnText;
import com.itextpdf.text.pdf.codec.TIFFConstants;

public class MyPowerQuadrantView extends MyZoomView {
    protected int CircleBackground = getResources().getColor(R.color.show_title_color);
    protected int CircleLine = -1;
    private double a_radio = 0.0d;
    private float center_x = ColumnText.GLOBAL_SPACE_CHAR_RATIO;
    private float center_y = ColumnText.GLOBAL_SPACE_CHAR_RATIO;
    private float height = ColumnText.GLOBAL_SPACE_CHAR_RATIO;
    private boolean isfirst = true;
    protected LineModel[] lines = null;
    private float m_Radius = ColumnText.GLOBAL_SPACE_CHAR_RATIO;
    protected float m_StrokeWidth = 3.0f;
    private int m_VectorDefinition = 0;
    private Paint myPaint = null;
    private float width = ColumnText.GLOBAL_SPACE_CHAR_RATIO;

    /* access modifiers changed from: package-private */
    public class LineModel {
        public double angle;
        public int color;
        public double length;
        public String name;
        public float ratio;

        public LineModel(float ratio2, double angle2, double length2, String name2, int color2) {
            this.ratio = ratio2;
            this.angle = angle2;
            this.length = length2;
            this.name = name2;
            this.color = color2;
        }
    }

    public MyPowerQuadrantView(Context context) {
        super(context);
        this.m_ZoomType = 1;
        this.m_VectorDefinition = ClouData.getInstance().getSettingBasic().VectorDefinition;
    }

    public MyPowerQuadrantView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        this.m_ZoomType = 1;
        this.m_VectorDefinition = ClouData.getInstance().getSettingBasic().VectorDefinition;
    }

    public MyPowerQuadrantView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.m_ZoomType = 1;
        this.m_VectorDefinition = ClouData.getInstance().getSettingBasic().VectorDefinition;
    }

    public void setVectorDefinition(int def) {
        this.m_VectorDefinition = def;
    }

    @Override // com.clou.rs350.ui.view.MyZoomView
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        this.width = getScaleWidth();
        this.height = getScaleHeight();
        this.center_x = this.width / 2.0f;
        this.center_y = this.height / 2.0f;
        if (this.isfirst) {
            this.myPaint = new Paint();
            this.myPaint.setAntiAlias(true);
            this.myPaint.setTextAlign(Paint.Align.CENTER);
            this.isfirst = false;
            this.myPaint.setStrokeWidth(this.m_StrokeWidth);
        }
        this.m_Radius = (this.width < this.height ? this.width / 2.0f : this.height / 2.0f) * 0.8f;
        this.a_radio = (double) (this.m_Radius * 0.08f);
        drawXY(this.width, this.height);
        this.myPaint.setColor(-1);
        if (this.lines != null) {
            for (LineModel line : this.lines) {
                drawLine(line);
            }
        }
    }

    private void drawXY(float width2, float height2) {
        float f;
        float tmpHalfWidth = width2 / 2.0f;
        float tmpHalfHeight = height2 / 2.0f;
        if (width2 < height2) {
            f = tmpHalfWidth;
        } else {
            f = tmpHalfHeight;
        }
        float tmpLineLength = f * 0.95f;
        float tmpTextLocation = tmpLineLength / 3.0f;
        Paint xyPaint = new Paint();
        xyPaint.setColor(-1);
        xyPaint.setStrokeWidth(this.m_StrokeWidth);
        float tmpTextHeight = tmpLineLength / 5.0f;
        xyPaint.setTextSize(tmpTextHeight);
        float tmpTextHeight2 = (tmpTextHeight * 2.0f) / 5.0f;
        xyPaint.setTextAlign(Paint.Align.CENTER);
        drawCircle(this.center_x, this.center_y, this.m_Radius, this.CircleLine, this.CircleBackground, this.m_StrokeWidth, xyPaint);
        drawLine(tmpHalfWidth - tmpLineLength, tmpHalfHeight, tmpHalfWidth + tmpLineLength, tmpHalfHeight, xyPaint);
        drawLine(tmpHalfWidth, tmpHalfHeight - tmpLineLength, tmpHalfWidth, tmpHalfHeight + tmpLineLength, xyPaint);
        drawText("I", tmpHalfWidth + tmpTextLocation, (tmpHalfHeight - tmpTextLocation) + tmpTextHeight2, xyPaint);
        drawText(this.m_VectorDefinition == 0 ? "II" : "IV", tmpHalfWidth + tmpTextLocation, tmpHalfHeight + tmpTextLocation + tmpTextHeight2, xyPaint);
        drawText("III", tmpHalfWidth - tmpTextLocation, tmpHalfHeight + tmpTextLocation + tmpTextHeight2, xyPaint);
        drawText(this.m_VectorDefinition == 0 ? "IV" : "II", tmpHalfWidth - tmpTextLocation, (tmpHalfHeight - tmpTextLocation) + tmpTextHeight2, xyPaint);
    }

    public void postInvalidate(LineModel[] lines2) {
        this.lines = lines2;
        super.postInvalidate();
    }

    public void postInvalidate(double ActivePower, double ReactivePower, double ApparentPower) {
        LineModel ActivePowerLine;
        LineModel ReactivePowerLine;
        LineModel ApparentPowerLine;
        if (0.0d == ApparentPower) {
            ApparentPower = Math.sqrt((ActivePower * ActivePower) + (ReactivePower * ReactivePower));
        }
        float ActiveRatio = (float) Math.abs(ActivePower / ApparentPower);
        float ReactiveRatio = (float) Math.abs(ReactivePower / ApparentPower);
        if (this.m_VectorDefinition == 0) {
            ActivePowerLine = new LineModel(ActiveRatio, (double) (ActivePower > 0.0d ? 0 : 180), ActivePower, "P", -16776961);
            ReactivePowerLine = new LineModel(ReactiveRatio, (double) (ReactivePower > 0.0d ? TIFFConstants.TIFFTAG_IMAGEDESCRIPTION : 90), ReactivePower, "Q", -65536);
            ApparentPowerLine = new LineModel(1.0f, 360.0d - AngleUtil.adjustAngle((Math.atan2(ReactivePower, ActivePower) / 3.141592653589793d) * 180.0d), ApparentPower, "S", -256);
        } else {
            ActivePowerLine = new LineModel(ActiveRatio, (double) (ActivePower > 0.0d ? TIFFConstants.TIFFTAG_IMAGEDESCRIPTION : 90), ActivePower, "P", -16776961);
            ReactivePowerLine = new LineModel(ReactiveRatio, (double) (ReactivePower > 0.0d ? 0 : 180), ReactivePower, "Q", -65536);
            ApparentPowerLine = new LineModel(1.0f, AngleUtil.adjustAngle((Math.atan2(ReactivePower, ActivePower) / 3.141592653589793d) * 180.0d) + 270.0d, ApparentPower, "S", -256);
        }
        postInvalidate(new LineModel[]{ActivePowerLine, ReactivePowerLine, ApparentPowerLine});
    }

    public void drawLine(LineModel line) {
        line.angle = AngleUtil.adjustAngle(line.angle);
        double x_offset = ((double) (line.ratio * this.m_Radius)) * Math.sin(0.017453292519943295d * line.angle);
        double y_offset = ((double) (line.ratio * this.m_Radius)) * Math.cos(0.017453292519943295d * line.angle);
        double new_x_coordinate = ((double) this.center_x) - x_offset;
        double new_y_coordinate = ((double) this.center_y) - y_offset;
        AngleUtil.adjustAngle(line.angle + 165.0d);
        double offsetAngle = AngleUtil.adjustAngle(line.angle + 155.0d);
        double arrow_left_x_offect = this.a_radio * Math.sin(0.017453292519943295d * offsetAngle);
        double arrow_left_y_offect = this.a_radio * Math.cos(0.017453292519943295d * offsetAngle);
        double offsetAngle2 = AngleUtil.adjustAngle(line.angle + 205.0d);
        double arrow_left_coordinate_x = new_x_coordinate - arrow_left_x_offect;
        double arrow_left_coordinate_y = new_y_coordinate - arrow_left_y_offect;
        double arrow_right_coordinate_x = new_x_coordinate - (this.a_radio * Math.sin(0.017453292519943295d * offsetAngle2));
        double arrow_right_coordinate_y = new_y_coordinate - (this.a_radio * Math.cos(0.017453292519943295d * offsetAngle2));
        double offsetAngle3 = AngleUtil.adjustAngle(line.angle + 225.0d);
        double character_x_coordinate = arrow_right_coordinate_x - (15.0d * Math.sin(0.017453292519943295d * offsetAngle3));
        double character_y_coordinate = (arrow_right_coordinate_y - (15.0d * Math.cos(0.017453292519943295d * offsetAngle3))) + 7.0d;
        this.myPaint.setColor(line.color);
        if (line.length != 0.0d) {
            drawLine(this.center_x, this.center_y, (float) new_x_coordinate, (float) new_y_coordinate, this.myPaint);
            drawLine((float) new_x_coordinate, (float) new_y_coordinate, (float) arrow_left_coordinate_x, (float) arrow_left_coordinate_y, this.myPaint);
            drawLine((float) new_x_coordinate, (float) new_y_coordinate, (float) arrow_right_coordinate_x, (float) arrow_right_coordinate_y, this.myPaint);
            float old_textSize = this.myPaint.getTextSize();
            this.myPaint.setTextSize(20.0f);
            drawText(line.name, (float) character_x_coordinate, (float) character_y_coordinate, this.myPaint);
            this.myPaint.setTextSize(old_textSize);
        }
    }
}
