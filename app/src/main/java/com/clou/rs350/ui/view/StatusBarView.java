package com.clou.rs350.ui.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.clou.rs350.R;
import com.clou.rs350.manager.StatusBarManager;
import com.clou.rs350.ui.dialog.HintDialog;

public class StatusBarView extends LinearLayout {
    private TextView connectStateTextView;
    private Context m_Context;
    private MyBatteryView m_DeviceBatteryPictureView;
    private TextView m_DeviceBatteryTextView;
    private MyBatteryView m_TabBatteryPictureView;
    private TextView m_TabletBatteryTextView;
    private StatusBarManager manager;

    public StatusBarView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initView(context);
    }

    public StatusBarView(Context context) {
        super(context);
        initView(context);
    }

    private void initView(Context context) {
        if (context != null) {
            this.m_Context = context;
            ((LayoutInflater) context.getSystemService("layout_inflater")).inflate(R.layout.status_bar_layout, this);
            this.m_DeviceBatteryPictureView = (MyBatteryView) findViewById(R.id.device_battery_percent);
            this.m_TabBatteryPictureView = (MyBatteryView) findViewById(R.id.tab_battery_percent);
            this.m_DeviceBatteryTextView = (TextView) findViewById(R.id.device_battery_textview);
            this.m_TabletBatteryTextView = (TextView) findViewById(R.id.tab_battery_textview);
            this.connectStateTextView = (TextView) findViewById(R.id.connect_status_textview);
            this.manager = StatusBarManager.getInstance();
        }
    }

    public void showHint(int id) {
        new HintDialog(this.m_Context, id).show();
    }

    public void setDeviceBattery(int Power, int state) {
        if (this.m_DeviceBatteryPictureView != null) {
            this.m_DeviceBatteryPictureView.setBattery((float) Power);
        }
        if (this.m_DeviceBatteryTextView != null) {
            this.m_DeviceBatteryTextView.setText(String.valueOf(Power) + "%");
        }
    }

    public void setTabBattery(int Power, int state) {
        if (this.m_TabBatteryPictureView != null) {
            this.m_TabBatteryPictureView.setBattery((float) Power);
        }
        if (this.m_TabletBatteryTextView != null) {
            this.m_TabletBatteryTextView.setText(String.valueOf(Power) + "%");
        }
    }

    public void setConnectState(String state) {
        if (this.connectStateTextView != null) {
            this.connectStateTextView.setText(state);
        }
    }

    public void setSignal(float signal) {
    }

    /* access modifiers changed from: protected */
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        if (this.manager != null) {
            this.manager.addStatusView(this);
        }
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        if (this.manager != null) {
            this.manager.removeStatusView(this);
        }
    }
}
