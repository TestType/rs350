package com.clou.rs350.ui.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import com.clou.rs350.R;
import com.clou.rs350.db.dao.BaseDao;
import com.clou.rs350.utils.OtherUtils;

public class SaveAsDialog extends Dialog implements View.OnClickListener {
    private Button btn_Cancel = null;
    private Button btn_Ok = null;
    private View.OnClickListener cancleClick = null;
    private Context m_Context = null;
    private BaseDao m_Dao = null;
    private int m_ExistId = 0;
    private int m_WarningId = 0;
    private View.OnClickListener okClick = null;
    private TextView txt_Value = null;

    public SaveAsDialog(Context context, BaseDao dao) {
        super(context, R.style.dialog);
        requestWindowFeature(1);
        setCanceledOnTouchOutside(true);
        this.m_Context = context;
        this.m_Dao = dao;
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_save_as);
        WindowManager.LayoutParams lp = getWindow().getAttributes();
        lp.width = getContext().getResources().getDisplayMetrics().widthPixels;
        lp.height = getContext().getResources().getDisplayMetrics().heightPixels;
        getWindow().setAttributes(lp);
        this.txt_Value = (TextView) findViewById(R.id.txt_save_name);
        this.btn_Ok = (Button) findViewById(R.id.btn_ok);
        this.btn_Ok.setOnClickListener(this);
        this.btn_Cancel = (Button) findViewById(R.id.btn_cancel);
        this.btn_Cancel.setOnClickListener(this);
    }

    public void setSelectionValue(String value) {
        this.txt_Value.setText(value);
    }

    public void setWarning(int id) {
        this.m_WarningId = id;
    }

    public void setExist(int id) {
        this.m_ExistId = id;
    }

    public void setOkText(CharSequence text) {
        this.btn_Ok.setText(text);
    }

    public void setOkText(int id) {
        this.btn_Ok.setText(id);
    }

    public void setCancleText(CharSequence text) {
        this.btn_Cancel.setText(text);
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_cancel:
                if (this.cancleClick != null) {
                    this.cancleClick.onClick(v);
                }
                dismiss();
                return;
            case R.id.btn_ok:
                if (OtherUtils.isEmpty(this.txt_Value)) {
                    if (this.m_WarningId != 0) {
                        new HintDialog(this.m_Context, this.m_WarningId).show();
                        return;
                    }
                    return;
                } else if (this.m_Dao == null || !this.m_Dao.queryIfExistByKey(this.txt_Value.getText().toString())) {
                    v.setTag(this.txt_Value.getText().toString());
                    if (this.okClick != null) {
                        this.okClick.onClick(v);
                    }
                    dismiss();
                    return;
                } else if (this.m_ExistId != 0) {
                    new HintDialog(this.m_Context, this.m_ExistId).show();
                    return;
                } else {
                    return;
                }
            default:
                return;
        }
    }

    public void setOnClick(View.OnClickListener onClickListener, View.OnClickListener cancleClick2) {
        this.okClick = onClickListener;
        this.cancleClick = cancleClick2;
    }
}
