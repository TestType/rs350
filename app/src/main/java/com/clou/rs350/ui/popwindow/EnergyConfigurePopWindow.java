package com.clou.rs350.ui.popwindow;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.PopupWindow;
import android.widget.RadioButton;
import android.widget.TextView;

import com.clou.rs350.Preferences;
import com.clou.rs350.R;
import com.clou.rs350.db.model.EnergyTest;
import com.clou.rs350.model.ClouData;
import com.clou.rs350.ui.dialog.NumericDialog;

public class EnergyConfigurePopWindow extends PopupWindow implements View.OnClickListener, CompoundButton.OnCheckedChangeListener {
    String Meter1Accuracy;
    String Meter1ApparentAccuracy;
    String Meter1ReactiveAccruacy;
    String Meter2Accuracy;
    String Meter3Accuracy;
    Button btn_MeterSelection;
    CheckBox chk_Active;
    CheckBox chk_Apparent;
    RadioButton chk_MWh;
    CheckBox chk_Meter2Enable;
    CheckBox chk_Meter3Enable;
    CheckBox chk_Reactive;
    RadioButton chk_StartStopSwitch;
    RadioButton chk_Tablet;
    RadioButton chk_Wh;
    RadioButton chk_kWh;
    View layout_1Meter;
    View layout_3Meter;
    Context mContext;
    View.OnClickListener mOnClick;
    EnergyTest[] m_Data;
    int temp_PQ_Flag;
    int testMeterCount;
    TextView txt_Meter1Accuracy;
    TextView txt_Meter2Accuracy;
    TextView txt_Meter3Accuracy;

    public EnergyConfigurePopWindow(Context context) {
        super(context);
        this.mContext = context;
        initView();
    }

    private void initView() {
        View conentView = ((LayoutInflater) this.mContext.getSystemService("layout_inflater")).inflate(R.layout.pop_window_energyconfigure, (ViewGroup) null);
        conentView.findViewById(R.id.ok).setOnClickListener(this);
        conentView.findViewById(R.id.cancel).setOnClickListener(this);
        this.btn_MeterSelection = (Button) conentView.findViewById(R.id.btn_meter_select);
        this.btn_MeterSelection.setOnClickListener(this);
        this.layout_3Meter = conentView.findViewById(R.id.layout_test_3meter);
        this.layout_1Meter = conentView.findViewById(R.id.layout_test_1meter);
        this.txt_Meter1Accuracy = (TextView) conentView.findViewById(R.id.txt_meter1_accuracy);
        this.txt_Meter1Accuracy.setOnClickListener(this);
        this.txt_Meter2Accuracy = (TextView) conentView.findViewById(R.id.txt_meter2_accuracy);
        this.txt_Meter2Accuracy.setOnClickListener(this);
        this.txt_Meter3Accuracy = (TextView) conentView.findViewById(R.id.txt_meter3_accuracy);
        this.txt_Meter3Accuracy.setOnClickListener(this);
        this.chk_Meter2Enable = (CheckBox) conentView.findViewById(R.id.chk_meter2);
        this.chk_Meter3Enable = (CheckBox) conentView.findViewById(R.id.chk_meter3);
        this.chk_Wh = (RadioButton) conentView.findViewById(R.id.chk_wh);
        this.chk_kWh = (RadioButton) conentView.findViewById(R.id.chk_kwh);
        this.chk_MWh = (RadioButton) conentView.findViewById(R.id.chk_mwh);
        this.chk_Active = (CheckBox) conentView.findViewById(R.id.chk_active);
        this.chk_Reactive = (CheckBox) conentView.findViewById(R.id.chk_reactive);
        this.chk_Apparent = (CheckBox) conentView.findViewById(R.id.chk_appreance);
        this.chk_Tablet = (RadioButton) conentView.findViewById(R.id.chk_tablet);
        this.chk_StartStopSwitch = (RadioButton) conentView.findViewById(R.id.chk_start_stop_switch);
        setContentView(conentView);
        setWidth(this.mContext.getResources().getDimensionPixelSize(R.dimen.dp_450));
        setHeight(this.mContext.getResources().getDimensionPixelSize(R.dimen.dp_230));
        setFocusable(true);
        setOutsideTouchable(true);
        update();
        setBackgroundDrawable(new ColorDrawable(0));
        setAnimationStyle(16973826);
    }

    private void praseTmpValue() {
        boolean z;
        boolean z2;
        switch (this.temp_PQ_Flag) {
            case 0:
                this.Meter1Accuracy = ClouData.getInstance().getMeterBaseInfo()[0].ActiveAccuracy.trim();
                this.Meter2Accuracy = ClouData.getInstance().getMeterBaseInfo()[1].ActiveAccuracy.trim();
                this.Meter3Accuracy = ClouData.getInstance().getMeterBaseInfo()[2].ActiveAccuracy.trim();
                break;
            case 1:
                this.Meter1Accuracy = ClouData.getInstance().getMeterBaseInfo()[0].ReactiveAccuracy.trim();
                this.Meter2Accuracy = ClouData.getInstance().getMeterBaseInfo()[1].ReactiveAccuracy.trim();
                this.Meter3Accuracy = ClouData.getInstance().getMeterBaseInfo()[2].ReactiveAccuracy.trim();
                break;
            case 2:
                this.Meter1Accuracy = ClouData.getInstance().getMeterBaseInfo()[0].ApparentAccuracy.trim();
                this.Meter2Accuracy = ClouData.getInstance().getMeterBaseInfo()[1].ApparentAccuracy.trim();
                this.Meter3Accuracy = ClouData.getInstance().getMeterBaseInfo()[2].ApparentAccuracy.trim();
                break;
        }
        this.Meter1Accuracy = ClouData.getInstance().getMeterBaseInfo()[0].ActiveAccuracy.trim();
        this.Meter1ReactiveAccruacy = ClouData.getInstance().getMeterBaseInfo()[0].ReactiveAccuracy.trim();
        this.Meter1ApparentAccuracy = ClouData.getInstance().getMeterBaseInfo()[0].ApparentAccuracy.trim();
        CheckBox checkBox = this.chk_Meter2Enable;
        if ((this.testMeterCount & 2) == 2) {
            z = true;
        } else {
            z = false;
        }
        checkBox.setChecked(z);
        CheckBox checkBox2 = this.chk_Meter3Enable;
        if ((this.testMeterCount & 4) == 4) {
            z2 = true;
        } else {
            z2 = false;
        }
        checkBox2.setChecked(z2);
        EnergyTest tmpEnergy = this.m_Data[0];
        this.chk_Active.setChecked(tmpEnergy.testFlag[0]);
        this.chk_Reactive.setChecked(tmpEnergy.testFlag[1]);
        this.chk_Apparent.setChecked(tmpEnergy.testFlag[2]);
        switch (tmpEnergy.Unit) {
            case 0:
                this.chk_Wh.setChecked(true);
                break;
            case 1:
                this.chk_kWh.setChecked(true);
                break;
            case 2:
                this.chk_MWh.setChecked(true);
                break;
        }
        if (Preferences.getInt(Preferences.Key.ENERGYTESTMODE, 0) == 0) {
            this.chk_Tablet.setChecked(true);
        } else {
            this.chk_StartStopSwitch.setChecked(true);
        }
        showValue();
    }

    private void showValue() {
        if (1 != this.testMeterCount) {
            this.txt_Meter1Accuracy.setText(this.Meter1Accuracy);
            this.txt_Meter2Accuracy.setText(this.Meter2Accuracy);
            this.txt_Meter3Accuracy.setText(this.Meter3Accuracy);
            return;
        }
        this.txt_Meter1Accuracy.setText(this.Meter1Accuracy);
        this.txt_Meter2Accuracy.setText(this.Meter1ReactiveAccruacy);
        this.txt_Meter3Accuracy.setText(this.Meter1ApparentAccuracy);
    }

    private void praseValue() {
        int i = 2;
        int i2 = 0;
        if (1 != this.testMeterCount) {
            switch (this.temp_PQ_Flag) {
                case 0:
                    ClouData.getInstance().getMeterBaseInfo()[0].ActiveAccuracy = this.txt_Meter1Accuracy.getText().toString();
                    ClouData.getInstance().getMeterBaseInfo()[1].ActiveAccuracy = this.txt_Meter2Accuracy.getText().toString();
                    ClouData.getInstance().getMeterBaseInfo()[2].ActiveAccuracy = this.txt_Meter3Accuracy.getText().toString();
                    break;
                case 1:
                    ClouData.getInstance().getMeterBaseInfo()[0].ReactiveAccuracy = this.txt_Meter1Accuracy.getText().toString();
                    ClouData.getInstance().getMeterBaseInfo()[1].ReactiveAccuracy = this.txt_Meter2Accuracy.getText().toString();
                    ClouData.getInstance().getMeterBaseInfo()[2].ReactiveAccuracy = this.txt_Meter3Accuracy.getText().toString();
                    break;
                case 2:
                    ClouData.getInstance().getMeterBaseInfo()[0].ApparentAccuracy = this.txt_Meter1Accuracy.getText().toString();
                    ClouData.getInstance().getMeterBaseInfo()[1].ApparentAccuracy = this.txt_Meter2Accuracy.getText().toString();
                    ClouData.getInstance().getMeterBaseInfo()[2].ApparentAccuracy = this.txt_Meter3Accuracy.getText().toString();
                    break;
            }
            int tmpUnit = 1;
            if (this.chk_Wh.isChecked()) {
                tmpUnit = 0;
            } else if (this.chk_MWh.isChecked()) {
                tmpUnit = 2;
            }
            this.m_Data[0].Unit = tmpUnit;
            this.m_Data[1].Unit = tmpUnit;
            this.m_Data[2].Unit = tmpUnit;
            this.m_Data[0].testFlag = new boolean[]{true, true, true};
            this.m_Data[1].testFlag = new boolean[]{true, true, true};
            this.m_Data[2].testFlag = new boolean[]{true, true, true};
        } else {
            ClouData.getInstance().getMeterBaseInfo()[0].ActiveAccuracy = this.txt_Meter1Accuracy.getText().toString();
            ClouData.getInstance().getMeterBaseInfo()[0].ReactiveAccuracy = this.txt_Meter2Accuracy.getText().toString();
            ClouData.getInstance().getMeterBaseInfo()[0].ApparentAccuracy = this.txt_Meter3Accuracy.getText().toString();
            EnergyTest tmpEnergy = this.m_Data[0];
            tmpEnergy.testFlag[0] = this.chk_Active.isChecked();
            tmpEnergy.testFlag[1] = this.chk_Reactive.isChecked();
            tmpEnergy.testFlag[2] = this.chk_Apparent.isChecked();
            if (this.chk_Wh.isChecked()) {
                tmpEnergy.Unit = 0;
            } else if (this.chk_MWh.isChecked()) {
                tmpEnergy.Unit = 2;
            } else {
                tmpEnergy.Unit = 1;
            }
        }
        if (1 != this.testMeterCount) {
            if (!this.chk_Meter2Enable.isChecked()) {
                i = 0;
            }
            this.testMeterCount = (this.chk_Meter3Enable.isChecked() ? 4 : 0) | i | 1;
        }
        ClouData.getInstance().setTestMeterCount(this.testMeterCount);
        if (!this.chk_Tablet.isChecked()) {
            i2 = 1;
        }
        Preferences.putInt(Preferences.Key.ENERGYTESTMODE, i2);
    }

    public void showPopWindow(View currentClick, int PQFlag, EnergyTest[] Data, View.OnClickListener onclick) {
        this.mOnClick = onclick;
        this.temp_PQ_Flag = PQFlag;
        this.testMeterCount = ClouData.getInstance().getTestMeterCount();
        this.m_Data = Data;
        showMeterLayout(this.testMeterCount);
        praseTmpValue();
        if (!isShowing()) {
            int[] location = new int[2];
            currentClick.getLocationOnScreen(location);
            showAtLocation(currentClick, 0, location[0] - getWidth(), location[1]);
            return;
        }
        dismiss();
    }

    private void praseScreenToTmpValue() {
        if (1 != this.testMeterCount) {
            this.Meter1Accuracy = this.txt_Meter1Accuracy.getText().toString();
            this.Meter2Accuracy = this.txt_Meter2Accuracy.getText().toString();
            this.Meter3Accuracy = this.txt_Meter3Accuracy.getText().toString();
            return;
        }
        this.Meter1Accuracy = this.txt_Meter1Accuracy.getText().toString();
        this.Meter1ReactiveAccruacy = this.txt_Meter2Accuracy.getText().toString();
        this.Meter1ApparentAccuracy = this.txt_Meter3Accuracy.getText().toString();
    }

    public void onClick(View v) {
        NumericDialog nd = new NumericDialog(this.mContext);
        switch (v.getId()) {
            case R.id.ok:
                praseValue();
                if (this.mOnClick != null) {
                    this.mOnClick.onClick(v);
                }
                if (isShowing()) {
                    dismiss();
                    return;
                }
                return;
            case R.id.cancel:
                if (isShowing()) {
                    dismiss();
                    return;
                }
                return;
            case R.id.txt_meter1_accuracy:
            case R.id.txt_meter2_accuracy:
            case R.id.txt_meter3_accuracy:
                nd.txtLimit = 4;
                nd.showMyDialog((TextView) v);
                return;
            case R.id.btn_meter_select:
                praseScreenToTmpValue();
                if (1 == this.testMeterCount) {
                    this.testMeterCount = 3;
                } else {
                    this.testMeterCount = 1;
                }
                showMeterLayout(this.testMeterCount);
                showValue();
                return;
            default:
                return;
        }
    }

    private void showMeterLayout(int MeterCount) {
        if (1 == MeterCount) {
            this.btn_MeterSelection.setText(R.string.text_3meter);
            this.layout_1Meter.setVisibility(0);
            this.layout_3Meter.setVisibility(8);
            return;
        }
        this.btn_MeterSelection.setText(R.string.text_1meter);
        this.layout_1Meter.setVisibility(8);
        this.layout_3Meter.setVisibility(0);
    }

    public void onCheckedChanged(CompoundButton v, boolean isChecked) {
        v.getId();
    }
}
