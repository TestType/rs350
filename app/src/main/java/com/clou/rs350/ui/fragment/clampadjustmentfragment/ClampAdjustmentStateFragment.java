package com.clou.rs350.ui.fragment.clampadjustmentfragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.clou.rs350.Preferences;
import com.clou.rs350.R;
import com.clou.rs350.db.model.BasicMeasurement;
import com.clou.rs350.device.MeterBaseDevice;
import com.clou.rs350.task.ISleepCallback;
import com.clou.rs350.task.TimerThread;
import com.clou.rs350.ui.activity.BaseActivity;
import com.clou.rs350.ui.fragment.BaseFragment;
import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.view.annotation.ViewInject;

import java.lang.reflect.Array;

public class ClampAdjustmentStateFragment extends BaseFragment {
    private static final int STATE_ADJUST_STATE_DATA = 1;
    private static final int STATE_READ_MEASURE_DATA = 0;
    @ViewInject(R.id.ss_calibration_har_l1_ang)
    private TextView l1AngleTextView;
    @ViewInject(R.id.ss_calibration_har_l1_rms)
    private TextView l1RmsTextView;
    @ViewInject(R.id.ss_calibration_har_l2_ang)
    private TextView l2AngleTextView;
    @ViewInject(R.id.ss_calibration_har_l2_rms)
    private TextView l2RmsTextView;
    @ViewInject(R.id.ss_calibration_har_l3_ang)
    private TextView l3AngleTextView;
    @ViewInject(R.id.ss_calibration_har_l3_rms)
    private TextView l3RmsTextView;
    BasicMeasurement m_BasicMeasurement = new BasicMeasurement();
    private TimerThread readDataThread = null;
    private TextView[][] txtStates;

    @Override // android.support.v4.app.Fragment
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        this.fragmentView = inflater.inflate(R.layout.fragment_clamp_adjustment_state, (ViewGroup) null);
        ViewUtils.inject(this, this.fragmentView);
        return this.fragmentView;
    }

    @Override // android.support.v4.app.Fragment, com.clou.rs350.ui.fragment.BaseFragment
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initView();
        initData();
    }

    private void initView() {
        this.txtStates = (TextView[][]) Array.newInstance(TextView.class, 7, 3);
        int[][] txt_ID = {new int[]{R.id.txt_clamp_adjustment_1_1, R.id.txt_clamp_adjustment_2_1, R.id.txt_clamp_adjustment_3_1}, new int[]{R.id.txt_clamp_adjustment_1_2, R.id.txt_clamp_adjustment_2_2, R.id.txt_clamp_adjustment_3_2}, new int[]{R.id.txt_clamp_adjustment_1_3, R.id.txt_clamp_adjustment_2_3, R.id.txt_clamp_adjustment_3_3}, new int[]{R.id.txt_clamp_adjustment_1_4, R.id.txt_clamp_adjustment_2_4, R.id.txt_clamp_adjustment_3_4}, new int[]{R.id.txt_clamp_adjustment_1_5, R.id.txt_clamp_adjustment_2_5, R.id.txt_clamp_adjustment_3_5}, new int[]{R.id.txt_clamp_adjustment_1_6, R.id.txt_clamp_adjustment_2_6, R.id.txt_clamp_adjustment_3_6}, new int[]{R.id.txt_clamp_adjustment_1_7, R.id.txt_clamp_adjustment_2_7, R.id.txt_clamp_adjustment_3_7}};
        for (int i = 0; i < txt_ID.length; i++) {
            int[] ids = txt_ID[i];
            for (int j = 0; j < ids.length; j++) {
                this.txtStates[i][j] = (TextView) findViewById(ids[j]);
            }
        }
    }

    @Override // android.support.v4.app.Fragment, com.clou.rs350.ui.fragment.BaseFragment
    public void onResume() {
        super.onResume();
        this.messageManager.SetRatio(0.0d, 0.0d, 0);
        readTestData();
    }

    private void initData() {
        this.m_BasicMeasurement = new BasicMeasurement();
    }

    public void onClick(View v) {
    }

    private void readTestData() {
        int refreshTime = Preferences.getInt(Preferences.Key.REFRESHTIME, 1500);
        if (this.readDataThread == null || this.readDataThread.isStop()) {
            this.readDataThread = new TimerThread((long) refreshTime, new ISleepCallback() {
                /* class com.clou.rs350.ui.fragment.clampadjustmentfragment.ClampAdjustmentStateFragment.AnonymousClass1 */

                @Override // com.clou.rs350.task.ISleepCallback
                public void onSleepAfterToDo() {
                    ClampAdjustmentStateFragment.this.messageManager.getData();
                    ClampAdjustmentStateFragment.this.messageManager.GetClampAdjustmentFlag();
                }
            }, "readDataThread");
            this.readDataThread.start();
        }
    }

    @Override // com.clou.rs350.ui.fragment.BaseFragment, com.clou.rs350.manager.MessageManager.IMessageListener
    public void onReceiveMessage(MeterBaseDevice Device) {
        showData(Device);
    }

    private String getResult(int Flag) {
        int ResultID;
        switch (Flag) {
            case 1:
                ResultID = R.string.text_adjusting;
                break;
            case 2:
                ResultID = R.string.text_finish;
                break;
            default:
                ResultID = R.string.text_waiting;
                break;
        }
        return this.m_Context.getResources().getString(ResultID);
    }

    private void showData(MeterBaseDevice Device) {
        String Ia = Device.getCurrentValue()[0].getStringValue();
        String Ib = Device.getCurrentValue()[1].getStringValue();
        String Ic = Device.getCurrentValue()[2].getStringValue();
        String sIa = Device.getCurrentAngleValue()[0].getStringValue();
        String sIb = Device.getCurrentAngleValue()[1].getStringValue();
        String sIc = Device.getCurrentAngleValue()[2].getStringValue();
        this.l1RmsTextView.setText(Ia);
        this.l1AngleTextView.setText(sIa);
        this.l2RmsTextView.setText(Ib);
        this.l2AngleTextView.setText(sIb);
        this.l3RmsTextView.setText(Ic);
        this.l3AngleTextView.setText(sIc);
        int[] tmpStateFlag = Device.getClampAdjustmentFlagValue();
        if (tmpStateFlag[0] == 0) {
            if (this.m_Context instanceof BaseActivity) {
                ((BaseActivity) this.m_Context).startOrStopChangeViewStatus(0);
            }
        } else if (this.m_Context instanceof BaseActivity) {
            ((BaseActivity) this.m_Context).startOrStopChangeViewStatus(1);
        }
        for (int i = 0; i < tmpStateFlag.length - 1; i++) {
            if (tmpStateFlag[i + 1] == 0) {
                for (int j = 0; j < 3; j++) {
                    this.txtStates[i][j].setText(getResult((tmpStateFlag[i + 1] >> (j * 2)) & 3));
                }
            }
        }
    }

    @Override // android.support.v4.app.Fragment, com.clou.rs350.ui.fragment.BaseFragment
    public void onPause() {
        super.onPause();
        killThread(this.readDataThread);
    }

    @Override // android.support.v4.app.Fragment
    public void onDestroy() {
        this.messageManager.setFunc_Mstate(0);
        super.onDestroy();
    }
}
