package com.clou.rs350.ui.view;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.AttributeSet;

import com.clou.rs350.R;
import com.clou.rs350.db.model.DBDataModel;
import com.clou.rs350.ui.model.LTMData;
import com.itextpdf.text.pdf.ColumnText;

import java.util.ArrayList;
import java.util.List;

public class LTMGraphView extends MyZoomView {
    private static final String TAG = "LTMRecordView";
    private float arrow = ColumnText.GLOBAL_SPACE_CHAR_RATIO;
    boolean isfirst = true;
    private DBDataModel m_Max = new DBDataModel();
    private DBDataModel m_Min = new DBDataModel();
    private int m_Style = 0;
    private List<String> m_Times = new ArrayList();
    private List<LTMData> m_Values = null;
    private Paint myPaint = null;
    private float textSize = ColumnText.GLOBAL_SPACE_CHAR_RATIO;
    private float x = ColumnText.GLOBAL_SPACE_CHAR_RATIO;
    private float x_length = ColumnText.GLOBAL_SPACE_CHAR_RATIO;
    private float y = ColumnText.GLOBAL_SPACE_CHAR_RATIO;
    private float y_height = ColumnText.GLOBAL_SPACE_CHAR_RATIO;

    public LTMGraphView(Context context) {
        super(context);
        initData();
    }

    public LTMGraphView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        initData();
        this.m_Context = context;
    }

    public LTMGraphView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initData();
        this.m_Context = context;
    }

    private void getMaxMin() {
        boolean startFlag = true;
        if (this.m_Values != null) {
            for (int i = 0; i < this.m_Values.size(); i++) {
                for (int j = 0; j < this.m_Values.get(i).dataList.size(); j++) {
                    DBDataModel tmpValue = this.m_Values.get(i).dataList.get(j);
                    if (startFlag) {
                        startFlag = false;
                        this.m_Min.d_Value = tmpValue.d_Value;
                        this.m_Min.s_Value = tmpValue.s_Value;
                        this.m_Max.d_Value = tmpValue.d_Value;
                        this.m_Max.s_Value = tmpValue.s_Value;
                    } else {
                        if (this.m_Min.d_Value > tmpValue.d_Value) {
                            this.m_Min.d_Value = tmpValue.d_Value;
                            this.m_Min.s_Value = tmpValue.s_Value;
                        }
                        if (this.m_Max.d_Value < tmpValue.d_Value) {
                            this.m_Max.d_Value = tmpValue.d_Value;
                            this.m_Max.s_Value = tmpValue.s_Value;
                        }
                    }
                }
            }
        }
        if (1 == this.m_Style) {
            if (this.m_Max.d_Value < 0.0d) {
                this.m_Max = new DBDataModel(0.0d, "0");
            }
            if (this.m_Min.d_Value > 0.0d) {
                this.m_Min = new DBDataModel(0.0d, "0");
            }
        }
    }

    @Override // com.clou.rs350.ui.view.MyZoomView
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        float width = getScaleWidth();
        float height = getScaleHeight();
        getMaxMin();
        this.textSize = width / 60.0f;
        if (this.textSize > getValue(R.dimen.sp_10, 17.0f)) {
            this.textSize = getValue(R.dimen.sp_10, 17.0f);
        }
        this.x = this.textSize * 6.5f;
        this.y = (float) (getHeight() / 20);
        this.arrow = this.y / 2.0f;
        this.x_length = width - (this.x * 1.5f);
        this.y_height = (height - this.y) - (this.textSize * 2.6f);
        if (this.isfirst) {
            this.myPaint = new Paint();
            this.myPaint.setColor(-1);
            this.myPaint.setAntiAlias(true);
            this.myPaint.setStrokeWidth(3.0f);
            this.isfirst = false;
        }
        this.myPaint.setTextSize(this.textSize);
        drawVerticalLine();
        draw_lines();
    }

    private void initData() {
    }

    private void drawVerticalLine() {
        this.myPaint.setColor(-1);
        drawLine(this.x, this.y, this.x, this.y + this.y_height, this.myPaint);
        drawLine(this.x, this.y, this.x - (this.arrow / 1.5f), this.y + this.arrow, this.myPaint);
        drawLine(this.x, this.y, (this.arrow / 1.5f) + this.x, this.y + this.arrow, this.myPaint);
        this.y_height -= this.y;
        this.y += this.y;
        this.myPaint.setTextAlign(Paint.Align.RIGHT);
        if (this.m_Max.d_Value <= 0.0d) {
            drawLine(this.x, this.y, this.x_length + this.x, this.y, this.myPaint);
        } else if (this.m_Min.d_Value >= 0.0d) {
            drawLine(this.x, this.y + this.y_height, this.x_length + this.x, this.y + this.y_height, this.myPaint);
        } else {
            float tmpy = this.y + (this.y_height * ((float) Math.abs(this.m_Max.d_Value / (Math.abs(this.m_Min.d_Value) + this.m_Max.d_Value))));
            drawLine(this.x, tmpy, this.x_length + this.x, tmpy, this.myPaint);
            drawText("0", this.x - 5.0f, (this.textSize / 3.0f) + tmpy, this.myPaint);
        }
        drawText(this.m_Max.s_Value, this.x - 5.0f, this.y + (this.textSize / 3.0f), this.myPaint);
        drawText(this.m_Min.s_Value, this.x - 5.0f, this.y + this.y_height + (this.textSize / 3.0f), this.myPaint);
        this.myPaint.setTextAlign(Paint.Align.CENTER);
        int interval = (int) ((this.textSize * 10.0f) / (this.x_length / ((float) (this.m_Times.size() - 1))));
        if (interval == 0) {
            interval = 1;
        }
        float point_length = this.x_length / ((float) (this.m_Times.size() - 1));
        float oldx = this.x;
        int textCount = (this.m_Times.size() / interval) + (this.m_Times.size() % interval > 0 ? 1 : 0);
        for (int i = 0; i < textCount; i++) {
            String[] time = this.m_Times.get(i * interval).split(" ");
            float tmpy2 = this.y + this.y_height + (this.textSize * 1.2f);
            this.myPaint.setColor(-1);
            drawText(time[0], oldx, tmpy2, this.myPaint);
            drawText(time[1], oldx, this.textSize + tmpy2, this.myPaint);
            this.myPaint.setColor(-1862270977);
            drawLine(oldx, this.y, oldx, this.y + this.y_height, this.myPaint);
            oldx += ((float) interval) * point_length;
        }
        this.myPaint.setColor(-1862270977);
        drawLine(this.x, this.y, this.x + this.x_length, this.y, this.myPaint);
        drawLine(this.x, this.y + this.y_height, this.x + this.x_length, this.y + this.y_height, this.myPaint);
    }

    private void draw_lines() {
        double scale = (this.m_Max.d_Value - this.m_Min.d_Value) / ((double) this.y_height);
        if (this.m_Values != null) {
            for (int i = 0; i < this.m_Values.size(); i++) {
                draw_new_line(this.m_Values.get(i), scale);
            }
        }
    }

    private void draw_new_line(LTMData data, double scale) {
        if (data != null) {
            this.myPaint.setColor(data.color);
            double oldx = (double) this.x;
            List<DBDataModel> dots = data.dataList;
            float point_length = this.x_length / ((float) (dots.size() - 1));
            boolean setText = ((double) point_length) > 6.5d * ((double) this.textSize);
            this.myPaint.setTextAlign(Paint.Align.CENTER);
            if (setText) {
                drawText(dots.get(0).s_Value, (float) oldx, (float) ((((-(dots.get(0).d_Value - this.m_Min.d_Value)) / scale) + ((double) (this.y + this.y_height))) - 5.0d), this.myPaint);
            }
            for (int i = 0; i < dots.size() - 1; i++) {
                float startX = (float) oldx;
                float startY = (float) (((-(dots.get(i).d_Value - this.m_Min.d_Value)) / scale) + ((double) (this.y + this.y_height)));
                float stopX = (float) (((double) point_length) + oldx);
                float stopY = (float) (((-(dots.get(i + 1).d_Value - this.m_Min.d_Value)) / scale) + ((double) (this.y + this.y_height)));
                if (setText) {
                    drawText(dots.get(i + 1).s_Value, stopX, stopY - 5.0f, this.myPaint);
                }
                drawLine(startX, startY, stopX, stopY, this.myPaint);
                oldx += (double) point_length;
            }
        }
    }

    public void refreshData(List<LTMData> data, List<String> time) {
        this.m_Values = data;
        this.m_Times = time;
        postInvalidate();
    }

    public void setStyle(int style) {
        this.m_Style = style;
        postInvalidate();
    }
}
