package com.clou.rs350.ui.view.control;

import android.content.Context;
import android.graphics.Color;
import android.util.AttributeSet;
import android.widget.LinearLayout;
import com.clou.rs350.Preferences;

public class SubContentLinearLayout extends LinearLayout {
    public SubContentLinearLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    /* access modifiers changed from: protected */
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        int subContentColor = Preferences.getInt("subContentColor", Color.parseColor("#1D2E39"));
        if (subContentColor > 0) {
            setBackgroundColor(subContentColor);
        }
        ViewManager.getInstance().addControlView(3, this);
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        ViewManager.getInstance().removeControlView(3, this);
    }
}
