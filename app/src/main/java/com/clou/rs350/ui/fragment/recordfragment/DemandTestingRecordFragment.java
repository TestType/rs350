package com.clou.rs350.ui.fragment.recordfragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.clou.rs350.R;
import com.clou.rs350.db.model.DemandTest;
import com.clou.rs350.ui.fragment.BaseFragment;
import com.clou.rs350.utils.CountDown;
import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.view.annotation.ViewInject;

public class DemandTestingRecordFragment extends BaseFragment {
    @ViewInject(R.id.meter1_demand)
    private TextView Meter1DemandTv;
    @ViewInject(R.id.meter1_error)
    private TextView Meter1ErrorTv;
    @ViewInject(R.id.meter1_ref)
    private TextView Meter1RefTv;
    @ViewInject(R.id.meter2_demand)
    private TextView Meter2DemandTv;
    @ViewInject(R.id.meter2_error)
    private TextView Meter2ErrorTv;
    @ViewInject(R.id.meter2_ref)
    private TextView Meter2RefTv;
    @ViewInject(R.id.meter3_demand)
    private TextView Meter3DemandTv;
    @ViewInject(R.id.meter3_error)
    private TextView Meter3ErrorTv;
    @ViewInject(R.id.meter3_ref)
    private TextView Meter3RefTv;
    private TextView[] MeterDemandTvs;
    private TextView[] MeterErrorTvs;
    private TextView[] MeterRefTvs;
    @ViewInject(R.id.testtime_edit)
    private TextView TestingTimeTv;
    private DemandTest m_DemandMeasurement;
    @ViewInject(R.id.txt_meter_sr)
    private TextView txt_Meter_Sr;

    public DemandTestingRecordFragment(DemandTest data) {
        this.m_DemandMeasurement = data;
    }

    @Override // android.support.v4.app.Fragment
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        this.fragmentView = inflater.inflate(R.layout.fragment_record_demand_testing, (ViewGroup) null);
        ViewUtils.inject(this, this.fragmentView);
        return this.fragmentView;
    }

    @Override // android.support.v4.app.Fragment, com.clou.rs350.ui.fragment.BaseFragment
    public void onActivityCreated(Bundle savedInstanceState) {
        new CountDown();
        super.onActivityCreated(savedInstanceState);
        initView();
    }

    private void initView() {
        this.MeterDemandTvs = new TextView[]{this.Meter1DemandTv, this.Meter2DemandTv, this.Meter3DemandTv};
        this.MeterRefTvs = new TextView[]{this.Meter1RefTv, this.Meter2RefTv, this.Meter3RefTv};
        this.MeterErrorTvs = new TextView[]{this.Meter1ErrorTv, this.Meter2ErrorTv, this.Meter3ErrorTv};
    }

    @Override // android.support.v4.app.Fragment, com.clou.rs350.ui.fragment.BaseFragment
    public void onResume() {
        showData();
        super.onResume();
    }

    @Override // android.support.v4.app.Fragment, com.clou.rs350.ui.fragment.BaseFragment
    public void onPause() {
        super.onPause();
    }

    private void showData() {
        this.txt_Meter_Sr.setText(this.m_DemandMeasurement.MeterSerialNo);
        this.TestingTimeTv.setText(this.m_DemandMeasurement.RunningTime);
        for (int i = 0; i < 3; i++) {
            this.MeterDemandTvs[i].setText(new StringBuilder(String.valueOf(this.m_DemandMeasurement.EndPower[i].s_Value)).toString());
            this.MeterRefTvs[i].setText(new StringBuilder(String.valueOf(this.m_DemandMeasurement.StandardPower[i].s_Value)).toString());
            this.MeterErrorTvs[i].setText(this.m_DemandMeasurement.Error[i].s_Value);
        }
    }
}
