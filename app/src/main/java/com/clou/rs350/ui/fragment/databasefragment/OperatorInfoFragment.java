package com.clou.rs350.ui.fragment.databasefragment;

import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.clou.rs350.R;
import com.clou.rs350.callback.PasswordCallback;
import com.clou.rs350.db.dao.OperatorInfoDao;
import com.clou.rs350.db.model.OperatorInfo;
import com.clou.rs350.model.ClouData;
import com.clou.rs350.ui.dialog.NormalDialog;
import com.clou.rs350.ui.dialog.PasswordDialog;
import com.clou.rs350.ui.fragment.BaseFragment;
import com.clou.rs350.ui.popwindow.ListPopWindow;
import com.clou.rs350.utils.DoubleClick;
import com.clou.rs350.utils.OtherUtils;
import com.itextpdf.text.pdf.PdfObject;
import com.lidroid.xutils.ViewUtils;

import java.util.List;

public class OperatorInfoFragment extends BaseFragment implements View.OnClickListener {
    Button btn_Cancel = null;
    Button btn_Delete = null;
    Button btn_Modify = null;
    Button btn_New = null;
    Button btn_Save = null;
    LinearLayout btn_Select = null;
    int m_Authority = 0;
    String[] m_AuthorityStrings;
    String[] m_AuthorityStringsShow;
    OperatorInfoDao m_Dao;
    DoubleClick m_DoubleClick;
    boolean m_HasSearch = true;
    OperatorInfo m_Info;
    boolean m_IsModefy = false;
    boolean m_IsNew = false;
    TextView txt_Authority = null;
    TextView txt_Designation = null;
    EditText txt_ID = null;
    TextView txt_Name = null;
    TextView txt_Password = null;

    @Override // android.support.v4.app.Fragment
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        this.fragmentView = inflater.inflate(R.layout.fragment_operator_info, (ViewGroup) null);
        ViewUtils.inject(this, this.fragmentView);
        return this.fragmentView;
    }

    @Override // android.support.v4.app.Fragment, com.clou.rs350.ui.fragment.BaseFragment
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initView();
    }

    private void initView() {
        this.m_Info = ClouData.getInstance().getOperatorInfo();
        this.m_Authority = this.m_Info.Authority;
        this.m_Dao = new OperatorInfoDao(this.m_Context);
        this.m_AuthorityStrings = getResources().getStringArray(this.m_Authority == 3 ? R.array.authority_admin : R.array.authority);
        this.m_AuthorityStringsShow = getResources().getStringArray(R.array.authority_show);
        initTextView();
        this.btn_New = (Button) findViewById(R.id.btn_new);
        this.btn_New.setOnClickListener(this);
        this.btn_Modify = (Button) findViewById(R.id.btn_modify);
        this.btn_Modify.setOnClickListener(this);
        this.btn_Delete = (Button) findViewById(R.id.btn_delete);
        this.btn_Delete.setOnClickListener(this);
        this.btn_Save = (Button) findViewById(R.id.btn_save);
        this.btn_Save.setOnClickListener(new View.OnClickListener() {
            /* class com.clou.rs350.ui.fragment.databasefragment.OperatorInfoFragment.AnonymousClass1 */

            public void onClick(View v) {
                OperatorInfoFragment.this.Save();
            }
        });
        this.btn_Cancel = (Button) findViewById(R.id.btn_cancel);
        this.btn_Cancel.setOnClickListener(new View.OnClickListener() {
            /* class com.clou.rs350.ui.fragment.databasefragment.OperatorInfoFragment.AnonymousClass2 */

            public void onClick(View v) {
                if (OperatorInfoFragment.this.m_IsModefy) {
                    OperatorInfoFragment.this.setEditButton(false, false);
                    OperatorInfoFragment.this.refreshData(OperatorInfoFragment.this.m_Info);
                }
            }
        });
        searchData(this.m_Info.OperatorID);
        setEditButton(false, false);
        this.m_DoubleClick = new DoubleClick(new DoubleClick.OnDoubleClick() {
            /* class com.clou.rs350.ui.fragment.databasefragment.OperatorInfoFragment.AnonymousClass3 */

            private void searchKey(String condition) {
                List<String> tmpNames = OperatorInfoFragment.this.m_Dao.queryAllKey(condition);
                ListPopWindow.showListPopwindow(OperatorInfoFragment.this.m_Context, (String[]) tmpNames.toArray(new String[tmpNames.size()]), OperatorInfoFragment.this.txt_ID, new View.OnClickListener() {
                    /* class com.clou.rs350.ui.fragment.databasefragment.OperatorInfoFragment.AnonymousClass3.AnonymousClass1 */

                    public void onClick(View v) {
                        OperatorInfoFragment.this.searchData(((TextView) v).getText().toString());
                    }
                });
            }

            @Override // com.clou.rs350.utils.DoubleClick.OnDoubleClick
            public void doubleClick() {
                searchKey(OperatorInfoFragment.this.txt_ID.getText().toString());
            }

            @Override // com.clou.rs350.utils.DoubleClick.OnDoubleClick
            public void singleClick() {
                searchKey(PdfObject.NOTHING);
            }
        });
    }

    private void initTextView() {
        this.txt_ID = (EditText) findViewById(R.id.txt_id);
        this.txt_ID.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            /* class com.clou.rs350.ui.fragment.databasefragment.OperatorInfoFragment.AnonymousClass4 */

            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if ((actionId == 6 || actionId == 5) && !OperatorInfoFragment.this.m_IsNew) {
                    String id = OperatorInfoFragment.this.txt_ID.getText().toString();
                    OperatorInfoFragment.this.searchData(id);
                    OperatorInfoFragment.this.txt_ID.setSelection(id.length());
                }
                Log.i("actionId", new StringBuilder(String.valueOf(actionId)).toString());
                return false;
            }
        });
        this.btn_Select = (LinearLayout) findViewById(R.id.btn_select);
        this.btn_Select.setOnClickListener(this);
        this.txt_Name = (TextView) findViewById(R.id.txt_name);
        this.txt_Password = (TextView) findViewById(R.id.txt_password);
        this.txt_Authority = (TextView) findViewById(R.id.txt_authority);
        this.txt_Authority.setOnClickListener(this);
        this.txt_Designation = (TextView) findViewById(R.id.txt_designation);
    }

    private void parseData() {
        this.m_Info.OperatorID = this.txt_ID.getText().toString();
        this.m_Info.OperatorName = this.txt_Name.getText().toString();
        this.m_Info.Authority = OtherUtils.convertToInt(this.txt_Authority.getTag());
        this.m_Info.Password = this.txt_Password.getText().toString();
        this.m_Info.Designation = this.txt_Designation.getText().toString();
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private void Save() {
        if (!this.m_IsNew || !OtherUtils.isEmpty(this.txt_ID)) {
            parseData();
            String Name = this.m_Info.OperatorID;
            if (!this.m_IsNew) {
                this.m_Dao.deleteRecordByKey(Name);
            } else if (this.m_Dao.queryByKey(Name) != null) {
                showHint(R.string.text_operator_id_exists);
                return;
            }
            OperatorInfoDao.insertObject(this.m_Info, this.m_Context);
            setEditButton(false, false);
            showHint(R.string.text_save_success);
            return;
        }
        showHint(R.string.text_operator_id_empty);
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private void setEditButton(boolean Edit, boolean isNew) {
        boolean z;
        boolean z2;
        boolean z3 = true;
        boolean z4 = false;
        this.m_IsNew = isNew;
        this.m_IsModefy = Edit;
        this.btn_New.setEnabled((this.m_Authority == 0 || 2 == this.m_Authority) ? false : !Edit || !isNew);
        Button button = this.btn_Modify;
        if (Edit || 2 == this.m_Authority || 2 == ((Integer) this.txt_Authority.getTag()).intValue()) {
            z = false;
        } else {
            z = true;
        }
        button.setEnabled(z);
        this.btn_Delete.setEnabled((this.m_Authority == 0 || 2 == this.m_Authority || 2 == ((Integer) this.txt_Authority.getTag()).intValue() || this.txt_ID.getText().toString().equals(ClouData.getInstance().getOperatorInfo().OperatorID)) ? false : !isNew);
        this.btn_Save.setEnabled(Edit);
        this.btn_Cancel.setEnabled(Edit);
        EditText editText = this.txt_ID;
        if (this.m_Authority == 0 || 2 == this.m_Authority) {
            z2 = false;
        } else {
            z2 = !Edit || isNew;
        }
        editText.setEnabled(z2);
        LinearLayout linearLayout = this.btn_Select;
        if (this.m_Authority == 0 || 2 == this.m_Authority) {
            z3 = false;
        } else if (Edit) {
            z3 = false;
        }
        linearLayout.setEnabled(z3);
        this.txt_Name.setEnabled(Edit);
        this.txt_Password.setEnabled(Edit);
        TextView textView = this.txt_Authority;
        if (!(this.m_Authority == 0 || 2 == this.m_Authority)) {
            z4 = Edit;
        }
        textView.setEnabled(z4);
        this.txt_Designation.setEnabled(Edit);
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private void refreshData(OperatorInfo info) {
        this.txt_ID.setText(info.OperatorID);
        this.txt_Name.setText(info.OperatorName);
        this.txt_Password.setText(info.Password);
        if (info.Authority < this.m_AuthorityStringsShow.length) {
            this.txt_Authority.setText(this.m_AuthorityStringsShow[info.Authority]);
        }
        this.txt_Authority.setTag(Integer.valueOf(info.Authority));
        this.txt_Designation.setText(info.Designation);
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private void searchData(String ID) {
        this.m_Info = (OperatorInfo) this.m_Dao.queryByKey(ID);
        if (this.m_Info != null) {
            this.m_HasSearch = true;
        } else {
            this.m_HasSearch = false;
            this.m_Info = new OperatorInfo();
            this.m_Info.OperatorID = ID;
        }
        refreshData(this.m_Info);
        setEditButton(false, false);
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_delete:
                if (!this.m_HasSearch) {
                    showHint(R.string.text_operator_id_wrong);
                    return;
                } else if (OtherUtils.isEmpty(this.txt_ID)) {
                    showHint(R.string.text_operator_id_empty);
                    return;
                } else {
                    NormalDialog dialog = new NormalDialog(this.m_Context);
                    dialog.setOnClick(new View.OnClickListener() {
                        /* class com.clou.rs350.ui.fragment.databasefragment.OperatorInfoFragment.AnonymousClass6 */

                        public void onClick(View v) {
                            OperatorInfoFragment.this.m_Dao.deleteRecordByKey(OperatorInfoFragment.this.m_Info.OperatorID);
                            OperatorInfoFragment.this.m_Info = new OperatorInfo();
                            OperatorInfoFragment.this.refreshData(OperatorInfoFragment.this.m_Info);
                        }
                    }, null);
                    dialog.show();
                    dialog.setTitle(R.string.text_operator_delete);
                    return;
                }
            case R.id.btn_select:
                this.m_DoubleClick.click();
                return;
            case R.id.btn_new:
                setEditButton(true, true);
                refreshData(new OperatorInfo());
                return;
            case R.id.btn_modify:
                if (!this.m_HasSearch) {
                    showHint(R.string.text_operator_id_wrong);
                    return;
                } else if (OtherUtils.isEmpty(this.txt_ID)) {
                    showHint(R.string.text_operator_id_empty);
                    return;
                } else if (this.m_Authority > 0) {
                    setEditButton(true, false);
                    return;
                } else {
                    new PasswordDialog(this.m_Context, this.m_Info.Password, new PasswordCallback() {
                        /* class com.clou.rs350.ui.fragment.databasefragment.OperatorInfoFragment.AnonymousClass5 */

                        @Override // com.clou.rs350.callback.PasswordCallback
                        public void onPasswordIsMistake() {
                        }

                        @Override // com.clou.rs350.callback.PasswordCallback
                        public void onPasswordIsCorrect() {
                            OperatorInfoFragment.this.setEditButton(true, false);
                        }
                    }).show();
                    return;
                }
            case R.id.txt_authority:
                ListPopWindow.showListPopwindow(this.m_Context, this.m_AuthorityStrings, v, new View.OnClickListener() {
                    /* class com.clou.rs350.ui.fragment.databasefragment.OperatorInfoFragment.AnonymousClass7 */

                    public void onClick(View v) {
                        int model = Integer.parseInt(v.getTag().toString());
                        if (1 < model) {
                            v.setTag(Integer.valueOf(model + 1));
                        }
                    }
                });
                return;
            default:
                return;
        }
    }

    @Override // android.support.v4.app.Fragment, com.clou.rs350.ui.fragment.BaseFragment
    public void onPause() {
        setEditButton(false, false);
        refreshData(this.m_Info);
        super.onPause();
    }
}
