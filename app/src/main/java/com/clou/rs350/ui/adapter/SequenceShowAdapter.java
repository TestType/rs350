package com.clou.rs350.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.clou.rs350.R;
import com.clou.rs350.db.model.sequence.SequenceItem;

import java.util.ArrayList;
import java.util.List;

public class SequenceShowAdapter extends BaseAdapter {
    private Context mContext;
    private List<SequenceItem> mDataList;

    public SequenceShowAdapter(List<SequenceItem> list, Context context) {
        this.mDataList = list;
        this.mContext = context;
    }

    public int getCount() {
        if (this.mDataList != null) {
            return this.mDataList.size();
        }
        return 0;
    }

    public Object getItem(int position) {
        try {
            if (this.mDataList != null) {
                return this.mDataList.get(position);
            }
            return null;
        } catch (Exception e) {
            return null;
        }
    }

    public long getItemId(int position) {
        return 0;
    }

    public View getView(int position, View view, ViewGroup parent) {
        ViewHolder holder;
        if (view == null) {
            view = LayoutInflater.from(this.mContext).inflate(R.layout.adapter_sequence_show_item, (ViewGroup) null);
            holder = new ViewHolder(view);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }
        holder.show(position);
        return view;
    }

    public void refreshData(List<SequenceItem> list) {
        if (this.mDataList == null) {
            this.mDataList = new ArrayList();
        }
        this.mDataList.clear();
        this.mDataList.addAll(list);
        notifyDataSetChanged();
    }

    class ViewHolder {
        int m_Index;
        TextView txt_Function;
        TextView txt_Index;
        TextView txt_Name;

        ViewHolder(View view) {
            this.txt_Index = (TextView) view.findViewById(R.id.txt_index);
            this.txt_Name = (TextView) view.findViewById(R.id.txt_sequence_name);
            this.txt_Function = (TextView) view.findViewById(R.id.txt_sequence_item);
        }

        /* access modifiers changed from: package-private */
        public void show(int Index) {
            if (SequenceShowAdapter.this.mDataList != null) {
                SequenceItem item = (SequenceItem) SequenceShowAdapter.this.mDataList.get(Index);
                this.m_Index = Index;
                this.txt_Index.setText(new StringBuilder(String.valueOf(this.m_Index + 1)).toString());
                this.txt_Name.setText(item.ItemName);
                this.txt_Function.setText(item.ItemFunction);
            }
        }
    }
}
