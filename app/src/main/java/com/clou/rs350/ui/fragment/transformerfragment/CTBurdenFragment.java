package com.clou.rs350.ui.fragment.transformerfragment;

import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.TextView;

import com.clou.rs350.R;
import com.clou.rs350.db.model.CTBurden;
import com.clou.rs350.db.model.DBDataModel;
import com.clou.rs350.db.model.SiteData;
import com.clou.rs350.device.MeterBaseDevice;
import com.clou.rs350.handler.HandlerSwitchView;
import com.clou.rs350.model.ClouData;
import com.clou.rs350.task.ISleepCallback;
import com.clou.rs350.task.SleepTask;
import com.clou.rs350.ui.dialog.HintDialog;
import com.clou.rs350.ui.dialog.LoadingDialog;
import com.clou.rs350.ui.dialog.NumericDialog;
import com.clou.rs350.ui.fragment.BaseFragment;
import com.clou.rs350.utils.OtherUtils;
import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.view.annotation.ViewInject;
import com.lidroid.xutils.view.annotation.event.OnClick;

import java.util.ArrayList;
import java.util.List;

public class CTBurdenFragment extends BaseFragment implements HandlerSwitchView.IOnSelectCallback {
    @ViewInject(R.id.txt_accuracyclass)
    private TextView AccuracyclassTv;
    @ViewInject(R.id.chk_ctterminal)
    private RadioButton CTTerminalCkBox;
    @ViewInject(R.id.txt_ip)
    private TextView IpTv;
    @ViewInject(R.id.txt_is)
    private TextView IsTv;
    @ViewInject(R.id.l1ctburden)
    private TextView L1CtBurdenTv;
    @ViewInject(R.id.l1ctcurrent)
    private TextView L1CtCurrentTv;
    @ViewInject(R.id.l1ctnominal)
    private TextView L1CtNominalTv;
    @ViewInject(R.id.l1ctvoltage)
    private TextView L1CtVoltageTv;
    @ViewInject(R.id.l1)
    private TextView L1ResultTv;
    @ViewInject(R.id.l2ctburden)
    private TextView L2CtBurdenTv;
    @ViewInject(R.id.l2ctcurrent)
    private TextView L2CtCurrentTv;
    @ViewInject(R.id.l2ctnominal)
    private TextView L2CtNominalTv;
    @ViewInject(R.id.l2ctvoltage)
    private TextView L2CtVoltageTv;
    @ViewInject(R.id.l2)
    private TextView L2ResultTv;
    @ViewInject(R.id.l3ctburden)
    private TextView L3CtBurdenTv;
    @ViewInject(R.id.l3ctcurrent)
    private TextView L3CtCurrentTv;
    @ViewInject(R.id.l3ctnominal)
    private TextView L3CtNominalTv;
    @ViewInject(R.id.l3ctvoltage)
    private TextView L3CtVoltageTv;
    @ViewInject(R.id.l3)
    private TextView L3ResultTv;
    @ViewInject(R.id.txt_length)
    private TextView LengthTv;
    @ViewInject(R.id.chk_meterterminal)
    private RadioButton MeterTerminalCkBox;
    @ViewInject(R.id.txt_size)
    private TextView SizeTv;
    @ViewInject(R.id.txt_va)
    private TextView VaTv;
    private int i_Index = 0;
    private int i_Time = 2;
    private CTBurden m_CTBurden;
    LoadingDialog m_LoadingDialog;
    private HandlerSwitchView m_SwitchView;

    public CTBurdenFragment(CTBurden Data) {
        this.m_CTBurden = Data;
    }

    @Override // android.support.v4.app.Fragment
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        this.fragmentView = inflater.inflate(R.layout.fragment_ct_burden, (ViewGroup) null);
        ViewUtils.inject(this, this.fragmentView);
        return this.fragmentView;
    }

    @Override // android.support.v4.app.Fragment, com.clou.rs350.ui.fragment.BaseFragment
    public void onResume() {
        super.onResume();
        refreshData();
        initData();
    }

    private void initData() {
        this.messageManager.setFunc_Mstate(65);
    }

    @Override // com.clou.rs350.ui.fragment.BaseFragment
    public void refreshData() {
        SiteData baseInfo = ClouData.getInstance().getSiteData();
        this.IpTv.setText(new StringBuilder(String.valueOf(baseInfo.CTPrimary.d_Value)).toString());
        this.IsTv.setText(new StringBuilder(String.valueOf(baseInfo.CTSecondary.d_Value)).toString());
        this.m_CTBurden.setVAValue(baseInfo.CTBurden.d_Value);
        this.AccuracyclassTv.setText(baseInfo.CTAccuracyClass);
        this.VaTv.setText(new StringBuilder(String.valueOf(baseInfo.CTBurden.d_Value)).toString());
        setWireEnable(this.m_CTBurden.CTTerminal != 0);
        this.LengthTv.setText(new StringBuilder(String.valueOf(this.m_CTBurden.LengthOfWire.d_Value)).toString());
        this.SizeTv.setText(new StringBuilder(String.valueOf(this.m_CTBurden.SizeOfWire.d_Value)).toString());
        showData();
    }

    @Override // android.support.v4.app.Fragment, com.clou.rs350.ui.fragment.BaseFragment
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initView();
    }

    private void initView() {
        List<View> tabViews = new ArrayList<>();
        tabViews.add(findViewById(R.id.btn_l1));
        tabViews.add(findViewById(R.id.btn_l2));
        tabViews.add(findViewById(R.id.btn_l3));
        this.m_SwitchView = new HandlerSwitchView(this.m_Context);
        this.m_SwitchView.setTabViews(tabViews);
        this.m_SwitchView.boundClick();
        this.m_SwitchView.setOnTabChangedCallback(this);
        this.m_SwitchView.selectView(0);
    }

    @Override // com.clou.rs350.handler.HandlerSwitchView.IOnSelectCallback
    public void onSelectedChanged(int index) {
        this.i_Index = index;
    }

    @Override // com.clou.rs350.handler.HandlerSwitchView.IOnSelectCallback
    public void onClickView(View view) {
        onClick(view);
    }

    private void setWireEnable(boolean Flag) {
        boolean z = false;
        this.m_CTBurden.CTTerminal = Flag ? 1 : 0;
        this.MeterTerminalCkBox.setChecked(Flag);
        RadioButton radioButton = this.CTTerminalCkBox;
        if (!Flag) {
            z = true;
        }
        radioButton.setChecked(z);
        this.LengthTv.setEnabled(Flag);
        this.SizeTv.setEnabled(Flag);
    }

    @OnClick({R.id.txt_ip, R.id.txt_is, R.id.txt_va, R.id.txt_accuracyclass, R.id.txt_length, R.id.txt_size, R.id.btn_read, R.id.chk_ctterminal, R.id.chk_meterterminal})
    public void onClick(View view) {
        NumericDialog nd = new NumericDialog(this.m_Context);
        switch (view.getId()) {
            case R.id.txt_ip:
            case R.id.txt_is:
            case R.id.txt_accuracyclass:
                nd.txtLimit = 6;
                nd.showMyDialog((TextView) view);
                return;
            case R.id.txt_va:
                nd.showMyDialog((TextView) view);
                nd.setOnDoneClick(new View.OnClickListener() {
                    /* class com.clou.rs350.ui.fragment.transformerfragment.CTBurdenFragment.AnonymousClass1 */

                    public void onClick(View v) {
                        CTBurdenFragment.this.m_CTBurden.setVAValue(OtherUtils.parseDouble(CTBurdenFragment.this.VaTv.getText().toString().trim()));
                        CTBurdenFragment.this.showData();
                    }
                });
                return;
            case R.id.chk_ctterminal:
                setWireEnable(false);
                this.m_CTBurden.CalcVA(0);
                this.m_CTBurden.CalcVA(1);
                this.m_CTBurden.CalcVA(2);
                showData();
                return;
            case R.id.chk_meterterminal:
                setWireEnable(true);
                this.m_CTBurden.CalcVA(0);
                this.m_CTBurden.CalcVA(1);
                this.m_CTBurden.CalcVA(2);
                showData();
                return;
            case R.id.txt_length:
            case R.id.txt_size:
                nd.showMyDialog((TextView) view);
                nd.setOnDoneClick(new View.OnClickListener() {
                    /* class com.clou.rs350.ui.fragment.transformerfragment.CTBurdenFragment.AnonymousClass2 */

                    public void onClick(View v) {
                        CTBurdenFragment.this.m_CTBurden.setWireValue(OtherUtils.parseDouble(CTBurdenFragment.this.LengthTv.getText().toString().trim()), OtherUtils.parseDouble(CTBurdenFragment.this.SizeTv.getText().toString().trim()));
                        CTBurdenFragment.this.m_CTBurden.CalcVA(0);
                        CTBurdenFragment.this.m_CTBurden.CalcVA(1);
                        CTBurdenFragment.this.m_CTBurden.CalcVA(2);
                        CTBurdenFragment.this.showData();
                    }
                });
                return;
            case R.id.btn_read:
                ReadClick();
                return;
            default:
                return;
        }
    }

    public void ReadClick() {
        this.i_Time = 1;
        this.messageManager.getCTValue();
        this.m_LoadingDialog = new LoadingDialog(this.m_Context);
        this.m_LoadingDialog.show();
        new SleepTask(1500, new ISleepCallback() {
            /* class com.clou.rs350.ui.fragment.transformerfragment.CTBurdenFragment.AnonymousClass3 */

            @Override // com.clou.rs350.task.ISleepCallback
            public void onSleepAfterToDo() {
                CTBurdenFragment.this.i_Time = 0;
                if (CTBurdenFragment.this.m_LoadingDialog != null) {
                    CTBurdenFragment.this.m_LoadingDialog.dismiss();
                }
            }
        }).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, new Integer[0]);
    }

    @Override // com.clou.rs350.ui.fragment.BaseFragment, com.clou.rs350.manager.MessageManager.IMessageListener
    public void onReceiveMessage(MeterBaseDevice Device) {
        super.onReceiveMessage(Device);
        if (1 == this.i_Time) {
            this.m_CTBurden.parseData(Device, this.i_Index);
            showData();
        }
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private void showData() {
        this.L1CtCurrentTv.setText(this.m_CTBurden.CurrentValueSecondary[0].s_Value);
        this.L2CtCurrentTv.setText(this.m_CTBurden.CurrentValueSecondary[1].s_Value);
        this.L3CtCurrentTv.setText(this.m_CTBurden.CurrentValueSecondary[2].s_Value);
        this.L1CtVoltageTv.setText(this.m_CTBurden.VoltageValueSecondary[0].s_Value);
        this.L2CtVoltageTv.setText(this.m_CTBurden.VoltageValueSecondary[1].s_Value);
        this.L3CtVoltageTv.setText(this.m_CTBurden.VoltageValueSecondary[2].s_Value);
        this.L1CtBurdenTv.setText(this.m_CTBurden.Burden[0].s_Value);
        this.L2CtBurdenTv.setText(this.m_CTBurden.Burden[1].s_Value);
        this.L3CtBurdenTv.setText(this.m_CTBurden.Burden[2].s_Value);
        this.L1CtNominalTv.setText(this.m_CTBurden.BurdenPersents[0].s_Value);
        this.L2CtNominalTv.setText(this.m_CTBurden.BurdenPersents[1].s_Value);
        this.L3CtNominalTv.setText(this.m_CTBurden.BurdenPersents[2].s_Value);
        OtherUtils.setText(this.L1ResultTv, this.m_CTBurden.Result[0]);
        OtherUtils.setText(this.L2ResultTv, this.m_CTBurden.Result[1]);
        OtherUtils.setText(this.L3ResultTv, this.m_CTBurden.Result[2]);
    }

    @Override // android.support.v4.app.Fragment, com.clou.rs350.ui.fragment.BaseFragment
    public void onPause() {
        super.onPause();
    }

    /* access modifiers changed from: protected */
    @Override // com.clou.rs350.ui.fragment.BaseFragment
    public void copyData() {
        SiteData baseInfo = ClouData.getInstance().getSiteData();
        baseInfo.CTAccuracyClass = this.AccuracyclassTv.getText().toString();
        baseInfo.CTBurden = this.m_CTBurden.CTVA;
        String TmpValue = this.IpTv.getText().toString();
        baseInfo.CTPrimary = new DBDataModel(OtherUtils.parseDouble(TmpValue), String.valueOf(TmpValue) + "A");
        String TmpValue2 = this.IsTv.getText().toString();
        baseInfo.CTSecondary = new DBDataModel(OtherUtils.parseDouble(TmpValue2), String.valueOf(TmpValue2) + "A");
    }

    @Override // com.clou.rs350.ui.fragment.BaseFragment
    public int isFinish() {
        if (!this.m_CTBurden.isEmpty()) {
            return 0;
        }
        new HintDialog(this.m_Context, (int) R.string.text_do_test).show();
        return 1;
    }
}
