package com.clou.rs350.ui.activity;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;

import com.clou.rs350.CLApplication;
import com.clou.rs350.R;
import com.clou.rs350.callback.ILoadCallback;
import com.clou.rs350.constants.ConstantsFileName;
import com.clou.rs350.db.dao.FieldSettingDao;
import com.clou.rs350.db.dao.sequence.RegisterReadingSchemeDao;
import com.clou.rs350.db.dao.sequence.SealSchemeDao;
import com.clou.rs350.db.dao.sequence.SequenceDao;
import com.clou.rs350.db.dao.sequence.VisualSchemeDao;
import com.clou.rs350.handler.HandlerSwitchView;
import com.clou.rs350.ui.adapter.FragmentTabAdapter;
import com.clou.rs350.ui.dialog.HintDialog;
import com.clou.rs350.ui.dialog.LoadingDialog;
import com.clou.rs350.ui.fragment.BaseFragment;
import com.clou.rs350.ui.fragment.databasefragment.CustomerInfoFragment;
import com.clou.rs350.ui.fragment.databasefragment.MeterTypeFragment;
import com.clou.rs350.ui.fragment.databasefragment.OperatorInfoFragment;
import com.clou.rs350.ui.fragment.databasefragment.RegisterReadingSettingFragment;
import com.clou.rs350.ui.fragment.databasefragment.SealSettingFragment;
import com.clou.rs350.ui.fragment.databasefragment.SiteDataFragment;
import com.clou.rs350.ui.fragment.databasefragment.ViewResultFragment;
import com.clou.rs350.ui.fragment.databasefragment.VisualSettingFragment;
import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.view.annotation.ViewInject;
import com.lidroid.xutils.view.annotation.event.OnClick;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

@SuppressLint({"SimpleDateFormat"})
public class DataBaseActivity extends BaseActivity implements View.OnClickListener, HandlerSwitchView.IOnSelectCallback {
    @ViewInject(R.id.layout_data_manager)
    private LinearLayout layout_Data_Manager;
    @ViewInject(R.id.layout_scheme_setting)
    private LinearLayout layout_Scheme_Setting;
    private HandlerSwitchView switchView;

    /* access modifiers changed from: protected */
    @Override // com.clou.rs350.ui.activity.BaseActivity, android.support.v4.app.FragmentActivity
    public void onCreate(Bundle savedInstanceState) {
        this.TAG = "DataBaseActivity";
        super.onCreate(savedInstanceState);
        getWindow().setFlags(1024, 1024);
        requestWindowFeature(1);
        setContentView(R.layout.activity_data_base);
        setDefaultTitle(R.string.text_data_base);
        ViewUtils.inject(this);
        initView();
        initData();
    }

    private void initData() {
        this.m_StartFlag = 1;
    }

    private void initView() {
        List<View> tabViews = new ArrayList<>();
        tabViews.add(findViewById(R.id.btn_view_result));
        tabViews.add(findViewById(R.id.btn_customer_info));
        tabViews.add(findViewById(R.id.btn_meter_type));
        tabViews.add(findViewById(R.id.btn_site_data));
        tabViews.add(findViewById(R.id.btn_operator_info));
        tabViews.add(findViewById(R.id.btn_visual_setting));
        tabViews.add(findViewById(R.id.btn_seal_setting));
        tabViews.add(findViewById(R.id.btn_register_reading_setting));
        this.switchView = new HandlerSwitchView(this.m_Context);
        this.switchView.setTabViews(tabViews);
        this.switchView.boundClick();
        this.switchView.setOnTabChangedCallback(this);
        this.switchView.selectView(0);
        this.m_TabAdapter = new FragmentTabAdapter(this, getFragments(), R.id.content_view);
        this.m_TabAdapter.init();
    }

    private List<BaseFragment> getFragments() {
        List<BaseFragment> list = new ArrayList<>();
        list.add(new ViewResultFragment());
        list.add(new CustomerInfoFragment());
        list.add(new MeterTypeFragment());
        list.add(new SiteDataFragment());
        list.add(new OperatorInfoFragment());
        list.add(new VisualSettingFragment());
        list.add(new SealSettingFragment());
        list.add(new RegisterReadingSettingFragment());
        return list;
    }

    @OnClick({R.id.btn_back, R.id.btn_scheme_setting, R.id.btn_data_manager, R.id.btn_export_scheme})
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_back:
                finish();
                return;
            case R.id.btn_scheme_setting:
                this.layout_Data_Manager.setVisibility(View.GONE);
                this.layout_Scheme_Setting.setVisibility(View.VISIBLE);
                return;
            case R.id.btn_export_scheme:
                new LoadingDialog(this.m_Context, new ILoadCallback() {
                    /* class com.clou.rs350.ui.activity.DataBaseActivity.AnonymousClass1 */

                    @Override // com.clou.rs350.callback.ILoadCallback
                    public Object run() {
                        String exportPath = CLApplication.app.getExportPath();
                        new SequenceDao(DataBaseActivity.this.m_Context).exportDataToCSV(String.valueOf(exportPath) + File.separator + ConstantsFileName.SEQUENCESCHEMECSVNAME);
                        new VisualSchemeDao(DataBaseActivity.this.m_Context).exportDataToCSV(String.valueOf(exportPath) + File.separator + ConstantsFileName.VISUALSCHEMECSVNAME);
                        new SealSchemeDao(DataBaseActivity.this.m_Context).exportDataToCSV(String.valueOf(exportPath) + File.separator + ConstantsFileName.SEALSCHEMECSVNAME);
                        new RegisterReadingSchemeDao(DataBaseActivity.this.m_Context).exportDataToCSV(String.valueOf(exportPath) + File.separator + ConstantsFileName.REGISTERREADINGSCHEMECSVNAME);
                        new FieldSettingDao(DataBaseActivity.this.m_Context).exportDataToCSV(String.valueOf(exportPath) + File.separator + ConstantsFileName.FIELDSETTINGCSVNAME);
                        CLApplication.app.refreshPath(exportPath);
                        return null;
                    }

                    @Override // com.clou.rs350.callback.ILoadCallback
                    public void callback(Object result) {
                        new HintDialog(DataBaseActivity.this.m_Context, (int) R.string.text_export_finish).show();
                    }
                }).show();
                return;
            case R.id.btn_data_manager:
                this.layout_Data_Manager.setVisibility(View.VISIBLE);
                this.layout_Scheme_Setting.setVisibility(View.GONE);
                return;
            default:
                return;
        }
    }

    @Override // com.clou.rs350.handler.HandlerSwitchView.IOnSelectCallback
    public void onSelectedChanged(int index) {
        this.m_TabAdapter.checkedIndex(index);
    }

    @Override // com.clou.rs350.handler.HandlerSwitchView.IOnSelectCallback
    public void onClickView(View view) {
    }
}
