package com.clou.rs350.ui.popwindow;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.PopupWindow;

import com.clou.rs350.R;
import com.clou.rs350.handler.HandlerSwitchView;

import java.util.ArrayList;
import java.util.List;

public class WiringPopWindow extends PopupWindow implements View.OnClickListener {
    HandlerSwitchView definitionSwitchView;
    Context mContext;
    View.OnClickListener mOnClick;
    int[] mWiring = new int[3];
    HandlerSwitchView powerSwitchView;
    HandlerSwitchView wiringSwitchView;

    public WiringPopWindow(Context context) {
        super(context);
        this.mContext = context;
        initView();
    }

    private void initView() {
        View conentView = ((LayoutInflater) this.mContext.getSystemService("layout_inflater")).inflate(R.layout.pop_window_wiring, (ViewGroup) null);
        conentView.findViewById(R.id.ok).setOnClickListener(this);
        conentView.findViewById(R.id.cancel).setOnClickListener(this);
        setContentView(conentView);
        setWidth(this.mContext.getResources().getDimensionPixelSize(R.dimen.dp_300));
        setHeight(this.mContext.getResources().getDimensionPixelSize(R.dimen.dp_170));
        setFocusable(true);
        setOutsideTouchable(true);
        update();
        setBackgroundDrawable(new ColorDrawable(0));
        setAnimationStyle(16973826);
        initWiringSelection(conentView);
        initPowerSelection(conentView);
        initDefinitionSelection(conentView);
    }

    private void initWiringSelection(View conentView) {
        List<View> tabViews = new ArrayList<>();
        tabViews.add(conentView.findViewById(R.id.wiring3p4w));
        tabViews.add(conentView.findViewById(R.id.wiring3p3w));
        tabViews.add(conentView.findViewById(R.id.wiring1p2w));
        this.wiringSwitchView = new HandlerSwitchView(this.mContext);
        this.wiringSwitchView.setTabViews(tabViews);
        this.wiringSwitchView.boundClick();
        this.wiringSwitchView.setOnTabChangedCallback(new HandlerSwitchView.IOnSelectCallback() {
            /* class com.clou.rs350.ui.popwindow.WiringPopWindow.AnonymousClass1 */

            @Override // com.clou.rs350.handler.HandlerSwitchView.IOnSelectCallback
            public void onSelectedChanged(int index) {
                WiringPopWindow.this.mWiring[0] = index;
            }

            @Override // com.clou.rs350.handler.HandlerSwitchView.IOnSelectCallback
            public void onClickView(View view) {
            }
        });
    }

    private void initPowerSelection(View conentView) {
        List<View> tabViews = new ArrayList<>();
        tabViews.add(conentView.findViewById(R.id.active));
        tabViews.add(conentView.findViewById(R.id.reactive));
        tabViews.add(conentView.findViewById(R.id.apparent));
        this.powerSwitchView = new HandlerSwitchView(this.mContext);
        this.powerSwitchView.setTabViews(tabViews);
        this.powerSwitchView.boundClick();
        this.powerSwitchView.setOnTabChangedCallback(new HandlerSwitchView.IOnSelectCallback() {
            /* class com.clou.rs350.ui.popwindow.WiringPopWindow.AnonymousClass2 */

            @Override // com.clou.rs350.handler.HandlerSwitchView.IOnSelectCallback
            public void onSelectedChanged(int index) {
                WiringPopWindow.this.mWiring[1] = index;
            }

            @Override // com.clou.rs350.handler.HandlerSwitchView.IOnSelectCallback
            public void onClickView(View view) {
            }
        });
    }

    private void initDefinitionSelection(View conentView) {
        List<View> tabViews = new ArrayList<>();
        tabViews.add(conentView.findViewById(R.id.total));
        tabViews.add(conentView.findViewById(R.id.fundamental));
        this.definitionSwitchView = new HandlerSwitchView(this.mContext);
        this.definitionSwitchView.setTabViews(tabViews);
        this.definitionSwitchView.boundClick();
        this.definitionSwitchView.setOnTabChangedCallback(new HandlerSwitchView.IOnSelectCallback() {
            /* class com.clou.rs350.ui.popwindow.WiringPopWindow.AnonymousClass3 */

            @Override // com.clou.rs350.handler.HandlerSwitchView.IOnSelectCallback
            public void onSelectedChanged(int index) {
                WiringPopWindow.this.mWiring[2] = index;
            }

            @Override // com.clou.rs350.handler.HandlerSwitchView.IOnSelectCallback
            public void onClickView(View view) {
            }
        });
    }

    public void showWiringPopWindow(View currentClick, int[] wiring, View.OnClickListener onclick) {
        this.mOnClick = onclick;
        if (!isShowing()) {
            int[] location = new int[2];
            currentClick.getLocationOnScreen(location);
            this.wiringSwitchView.selectView(wiring[0]);
            this.powerSwitchView.selectView(wiring[1]);
            this.definitionSwitchView.selectView(wiring[2]);
            this.mWiring = wiring;
            showAtLocation(currentClick, 0, location[0], location[1] + currentClick.getHeight());
            return;
        }
        dismiss();
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ok:
                v.setTag(this.mWiring);
                if (this.mOnClick != null) {
                    this.mOnClick.onClick(v);
                }
                if (isShowing()) {
                    dismiss();
                    return;
                }
                return;
            case R.id.cancel:
                if (isShowing()) {
                    dismiss();
                    return;
                }
                return;
            default:
                return;
        }
    }
}
