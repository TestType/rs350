package com.clou.rs350.ui.popwindow;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.PopupWindow;
import android.widget.RadioButton;
import android.widget.TextView;

import com.clou.rs350.CLApplication;
import com.clou.rs350.R;
import com.clou.rs350.manager.MessageManager;

public class PTConnectionPopWindow extends PopupWindow implements View.OnClickListener {
    View ConnectTypeLayout;
    private Button btn_Close;
    private Button btn_Connect;
    RadioButton chk_Location1;
    RadioButton chk_Location2;
    private String[] m_ChannelString;
    Context m_Context;
    private MessageManager m_MessageManager = MessageManager.getInstance(this.m_Context);
    private View parentView;
    private TextView tv_Channel;

    public PTConnectionPopWindow(Context context) {
        super(context);
        this.m_Context = context;
        initView();
    }

    private void initView() {
        View conentView = ((LayoutInflater) this.m_Context.getSystemService("layout_inflater")).inflate(R.layout.pop_window_pt_connection, (ViewGroup) null);
        this.ConnectTypeLayout = conentView.findViewById(R.id.layout_connecttype);
        this.chk_Location1 = (RadioButton) conentView.findViewById(R.id.radio_location1);
        this.chk_Location2 = (RadioButton) conentView.findViewById(R.id.radio_location2);
        this.tv_Channel = (TextView) conentView.findViewById(R.id.txt_channel);
        this.btn_Connect = (Button) conentView.findViewById(R.id.ok);
        this.btn_Close = (Button) conentView.findViewById(R.id.cancel);
        this.tv_Channel.setOnClickListener(this);
        this.btn_Connect.setOnClickListener(this);
        this.btn_Close.setOnClickListener(this);
        this.m_ChannelString = CLApplication.app.getResources().getStringArray(R.array.pt_channel);
        setContentView(conentView);
        setWidth(this.m_Context.getResources().getDimensionPixelSize(R.dimen.dp_280));
        setHeight(this.m_Context.getResources().getDimensionPixelSize(R.dimen.dp_150));
        setFocusable(true);
        setOutsideTouchable(true);
        update();
        setBackgroundDrawable(new ColorDrawable(0));
        setAnimationStyle(16973826);
    }

    public void showPopWindow(View currentClick, int[] flags) {
        this.parentView = currentClick;
        setState(flags);
        if (!isShowing()) {
            int[] location = new int[2];
            currentClick.getLocationOnScreen(location);
            showAtLocation(currentClick, 0, location[0] - getWidth(), location[1]);
            return;
        }
        dismiss();
    }

    public void setState(int[] flags) {
        if (1 == flags[0]) {
            this.chk_Location1.setChecked(true);
        } else {
            this.chk_Location2.setChecked(true);
        }
        if (flags[1] < 1) {
            flags[1] = 1;
        } else if (flags[1] > this.m_ChannelString.length) {
            flags[1] = this.m_ChannelString.length;
        }
        this.tv_Channel.setTag(Integer.valueOf(flags[1] - 1));
        this.tv_Channel.setText(this.m_ChannelString[flags[1] - 1]);
    }

    public void onClick(View v) {
        int i = 1;
        switch (v.getId()) {
            case R.id.ok:
                int channel = Integer.parseInt(this.tv_Channel.getTag().toString());
                MessageManager messageManager = this.m_MessageManager;
                if (!this.chk_Location1.isChecked()) {
                    i = 0;
                }
                messageManager.SetPTAddress(i, channel + 1);
                this.m_MessageManager.setPTConnect();
                if (isShowing()) {
                    dismiss();
                    return;
                }
                return;
            case R.id.cancel:
                if (isShowing()) {
                    dismiss();
                    return;
                }
                return;
            case R.id.txt_channel:
                ListPopWindow.showListPopwindow(this.m_Context, this.m_ChannelString, v, null, 0, true, this.parentView);
                return;
            default:
                return;
        }
    }
}
