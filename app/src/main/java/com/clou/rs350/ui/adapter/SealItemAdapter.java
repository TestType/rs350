package com.clou.rs350.ui.adapter;

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.clou.rs350.R;
import com.clou.rs350.db.model.sequence.SealItem;
import com.itextpdf.text.pdf.PdfObject;

import java.util.List;

import google.zxing.camera.CaptureDialog;

public class SealItemAdapter extends BaseAdapter {
    private boolean isShow;
    private Context m_Context;
    List<SealItem> m_Data;
    private boolean m_EnableScan;
    private int m_MustDo;

    public SealItemAdapter(Context context, boolean enableScan, int flag) {
        this.m_Context = context;
        this.m_EnableScan = enableScan;
        this.m_MustDo = flag;
    }

    public void setIsShow(boolean flag) {
        this.isShow = flag;
    }

    public void refresh(List<SealItem> data) {
        this.m_Data = data;
        notifyDataSetChanged();
    }

    public int getCount() {
        if (this.m_Data != null) {
            return this.m_Data.size();
        }
        return 0;
    }

    public Object getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return 0;
    }

    public List<SealItem> getData() {
        return this.m_Data;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = View.inflate(this.m_Context, R.layout.adapter_seal_item, null);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        holder.showValue(position, this.m_Data.get(position));
        return convertView;
    }

    class ViewHolder {
        LinearLayout btn_Scan;
        LinearLayout layout_Scan;
        int m_Index;
        TextView txt_Index;
        TextView txt_Location;
        TextView txt_Sr;

        ViewHolder(View v) {
            this.txt_Index = (TextView) v.findViewById(R.id.txt_index);
            this.txt_Location = (TextView) v.findViewById(R.id.txt_seal_location);
            this.txt_Sr = (TextView) v.findViewById(R.id.txt_serial_number);
            this.txt_Sr.addTextChangedListener(new TextWatcher() {
                /* class com.clou.rs350.ui.adapter.SealItemAdapter.ViewHolder.AnonymousClass1 */

                public void onTextChanged(CharSequence s, int start, int before, int count) {
                }

                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                }

                public void afterTextChanged(Editable s) {
                    SealItemAdapter.this.m_Data.get(ViewHolder.this.m_Index).SealSr = ViewHolder.this.txt_Sr.getText().toString();
                }
            });
            this.layout_Scan = (LinearLayout) v.findViewById(R.id.layout_scan);
            this.btn_Scan = (LinearLayout) v.findViewById(R.id.btn_scan);
            this.btn_Scan.setOnClickListener(new View.OnClickListener() {
                /* class com.clou.rs350.ui.adapter.SealItemAdapter.ViewHolder.AnonymousClass2 */

                public void onClick(View v) {
                    new CaptureDialog(SealItemAdapter.this.m_Context).show(ViewHolder.this.txt_Sr);
                }
            });
            this.layout_Scan.setVisibility(SealItemAdapter.this.m_EnableScan ? View.VISIBLE : View.GONE);
            if (SealItemAdapter.this.isShow) {
                this.txt_Sr.setBackgroundResource(R.drawable.tableshowtext);
                this.txt_Sr.setTextColor(SealItemAdapter.this.m_Context.getResources().getColor(R.color.showtextcolor));
                this.txt_Sr.setEnabled(false);
                this.txt_Location.setBackgroundResource(R.drawable.tableshowtext);
                this.txt_Location.setTextColor(SealItemAdapter.this.m_Context.getResources().getColor(R.color.showtextcolor));
                this.txt_Location.setEnabled(false);
            }
        }

        /* access modifiers changed from: package-private */
        public void showValue(int index, SealItem data) {
            this.m_Index = index;
            this.txt_Index.setText(new StringBuilder(String.valueOf(index + 1)).toString());
            this.txt_Location.setText(data.SealLocation);
            this.txt_Sr.setText(data.SealSr);
            if (1 == data.RequiredField && 1 == SealItemAdapter.this.m_MustDo) {
                this.txt_Index.setText(String.valueOf(index + 1) + " *");
                this.txt_Sr.setHint(R.string.text_required_field);
                return;
            }
            this.txt_Index.setText(new StringBuilder(String.valueOf(index + 1)).toString());
            this.txt_Sr.setHint(PdfObject.NOTHING);
        }
    }
}
