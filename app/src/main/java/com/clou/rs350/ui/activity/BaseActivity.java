package com.clou.rs350.ui.activity;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentManager;

import com.clou.rs350.R;
import com.clou.rs350.device.MeterBaseDevice;
import com.clou.rs350.manager.MessageManager;
import com.clou.rs350.service.CLService;
import com.clou.rs350.task.TimerThread;
import com.clou.rs350.ui.adapter.FragmentTabAdapter;
import com.clou.rs350.ui.dialog.HintDialog;
import com.clou.rs350.utils.LanguageUtil;

public class BaseActivity extends FragmentActivity implements MessageManager.IMessageListener {
    protected String TAG = "BaseActivity";
    protected View contentView;
    private Intent extrasData;
    protected FragmentManager fragmentManager;
    protected Context m_Context;
    protected int m_StartFlag;
    protected FragmentTabAdapter m_TabAdapter;
    protected MessageManager messageManager;

    private ServiceConnection conn = new ServiceConnection() {
        /* class com.clou.rs350.ui.activity.BaseActivity.AnonymousClass1 */

        public void onServiceDisconnected(ComponentName name) {
        }

        public void onServiceConnected(ComponentName name, IBinder service) {
        }
    };


    /* access modifiers changed from: protected */
    @Override // android.support.v4.app.FragmentActivity
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.i(this.TAG, this.TAG + " onCreate.");
        this.m_Context = this;
        this.fragmentManager = getSupportFragmentManager();
        this.extrasData = getIntent();
        this.messageManager = MessageManager.getInstance(this);
        LanguageUtil.changeLanguage(this.m_Context);
        bindService(new Intent(this, CLService.class), this.conn, BIND_AUTO_CREATE);
    }

    private void HideDecorView() {
        if (Build.VERSION.SDK_INT > 11 && Build.VERSION.SDK_INT < 19) {
            getWindow().getDecorView().setSystemUiVisibility(8);
        } else if (Build.VERSION.SDK_INT >= 19) {
            getWindow().getDecorView().setSystemUiVisibility(4102);
        }
    }

    @Override // android.app.Activity
    public void setContentView(int layoutResID) {
        this.contentView = getLayoutInflater().inflate(layoutResID, (ViewGroup) null);
        super.setContentView(this.contentView);
    }

    /* access modifiers changed from: protected */
    @Override // android.support.v4.app.FragmentActivity
    public void onResume() {
        super.onResume();
        Log.i(this.TAG, String.valueOf(this.TAG) + " onResume.");
        if (this.m_TabAdapter != null) {
            this.m_TabAdapter.onResume();
        }
        if (this.messageManager != null) {
            this.messageManager.addMessageListener(this);
        }
    }

    /* access modifiers changed from: protected */
    @Override // android.support.v4.app.FragmentActivity
    public void onPause() {
        Log.i(this.TAG, String.valueOf(this.TAG) + " onPause.");
        if (this.m_StartFlag == 0) {
            if (this.m_TabAdapter != null) {
                this.m_TabAdapter.onPause();
            }
        } else if (this.m_TabAdapter != null) {
            this.m_TabAdapter.saveFragmentData();
        }
        if (this.messageManager != null) {
            this.messageManager.removeMessageListener(this);
        }
        super.onPause();
    }

    public void startOrStopChangeViewStatus(int flag) {
        this.m_StartFlag = flag;
    }

    /* access modifiers changed from: protected */
    public String getString(String key) {
        if (this.extrasData != null) {
            return this.extrasData.getExtras().getString(key);
        }
        return null;
    }

    /* access modifiers changed from: protected */
    public int getInt(String key) {
        Bundle data;
        if (this.extrasData == null || (data = this.extrasData.getExtras()) == null) {
            return 0;
        }
        return data.getInt(key);
    }

    /* access modifiers changed from: protected */
    public boolean getBoolean(String key) {
        if (this.extrasData != null) {
            return this.extrasData.getExtras().getBoolean(key);
        }
        return false;
    }

    /* access modifiers changed from: protected */
    public void startTo(Class activity) {
        startActivity(new Intent(this, activity));
    }

    /* access modifiers changed from: protected */
    public void startTo(Class activity, Bundle data) {
        Intent intent = new Intent(this, activity);
        intent.putExtras(data);
        startActivity(intent);
    }

    /* access modifiers changed from: protected */
    public void startToMain() {
        Intent intent = new Intent(this.m_Context, MainActivity.class);
        //603979776
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP+Intent.FLAG_ACTIVITY_SINGLE_TOP);
        startActivity(intent);
    }

    @Override // com.clou.rs350.manager.MessageManager.IMessageListener
    public void onReceiveMessage(MeterBaseDevice Device) {
    }

    @Override // com.clou.rs350.manager.MessageManager.IMessageListener
    public void onReceiveMessage(byte[] buffer) {
    }

    @Override // com.clou.rs350.manager.MessageManager.IMessageListener
    public void onReceiveMessage(int strId) {
        HintDialog tmpDialog = new HintDialog(this.m_Context);
        tmpDialog.show();
        tmpDialog.setTitle(strId);
    }

    @Override // com.clou.rs350.manager.MessageManager.IMessageListener
    public void onReceiveMessage(String str) {
        HintDialog tmpDialog = new HintDialog(this.m_Context);
        tmpDialog.show();
        tmpDialog.setTitle(str);
    }

    /* access modifiers changed from: protected */
    @Override // android.support.v4.app.FragmentActivity
    public void onStart() {
        Log.i(this.TAG, String.valueOf(this.TAG) + " onStart.");
        super.onStart();
    }

    /* access modifiers changed from: protected */
    @Override // android.support.v4.app.FragmentActivity
    public void onStop() {
        Log.i(this.TAG, String.valueOf(this.TAG) + " onStop.");
        adjustAtBackground();
        super.onStop();
    }

    /* access modifiers changed from: protected */
    public void adjustAtBackground() {
    }

    /* access modifiers changed from: protected */
    public boolean isEmpty(TextView tv) {
        if (tv == null) {
            return true;
        }
        return TextUtils.isEmpty(tv.getText().toString().trim());
    }

    /* access modifiers changed from: protected */
    public boolean isEmpty(String text) {
        return TextUtils.isEmpty(text);
    }

    /* access modifiers changed from: protected */
    public void killThread(TimerThread thread) {
        if (thread != null && !thread.isStop()) {
            thread.stopTimer();
            thread.interrupt();
        }
    }

    /* access modifiers changed from: protected */
    public void setDefaultTitle(int text) {
        ((TextView) findViewById(R.id.title_textview)).setText(text);
    }

    /* access modifiers changed from: protected */
    public void setDefaultTitle(String text) {
        ((TextView) findViewById(R.id.title_textview)).setText(text);
    }

    /* access modifiers changed from: protected */
    public void showHint(int title) {
        HintDialog hintDialog = new HintDialog(this.m_Context);
        hintDialog.show();
        hintDialog.setTitle(title);
    }

    /* access modifiers changed from: protected */
    @Override // android.support.v4.app.FragmentActivity
    public void onDestroy() {
        unbindService(this.conn);
        Log.i(this.TAG, String.valueOf(this.TAG) + " onDestroy.");
        super.onDestroy();
    }

}
