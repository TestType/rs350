package com.clou.rs350.ui.adapter;

import android.os.Handler;
import android.os.Message;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.fragment.app.FragmentTransaction;
import com.clou.rs350.ui.fragment.BaseFragment;
import java.util.List;

public class FragmentTabAdapter {
    private int currentIndex = 0;
    private int currentTab;
    private FragmentActivity fragmentActivity;
    private int fragmentContentId;
    private List<BaseFragment> fragments;
    private Handler handler = new Handler() {
        /* class com.clou.rs350.ui.adapter.FragmentTabAdapter.AnonymousClass1 */

        public void handleMessage(Message msg) {
            try {
                FragmentTransaction ft = FragmentTabAdapter.this.fragmentActivity.getSupportFragmentManager().beginTransaction();
                ft.add(FragmentTabAdapter.this.fragmentContentId, (Fragment) FragmentTabAdapter.this.fragments.get(0));
                ft.commit();
            } catch (Exception e) {
            }
        }
    };
    private boolean isPause = false;

    public void saveFragmentData() {
        BaseFragment tmpFragment = getCurrentFragment();
        if (tmpFragment != null) {
            tmpFragment.SaveData();
        }
    }

    public void onResume() {
        try {
            checkedIndex(this.currentIndex);
        } catch (Exception e) {
        }
        this.isPause = false;
    }

    public void onPause() {
        FragmentTransaction ft = this.fragmentActivity.getSupportFragmentManager().beginTransaction();
        BaseFragment tmpFragment = getCurrentFragment();
        if (tmpFragment != null) {
            tmpFragment.SaveData();
            tmpFragment.onPause();
        }
        for (int i = 0; i < this.fragments.size(); i++) {
            if (i != this.currentTab) {
                ft.remove(this.fragments.get(i));
            }
        }
        ft.commit();
        this.isPause = true;
    }

    public void cleanFragment() {
        FragmentTransaction ft = this.fragmentActivity.getSupportFragmentManager().beginTransaction();
        if (this.fragments.size() > 0) {
            this.fragments.get(this.currentTab).SaveData();
            while (0 < this.fragments.size()) {
                this.fragments.get(0).onPause();
                ft.remove(this.fragments.get(0));
                this.fragments.remove(0);
            }
            ft.commit();
            this.currentIndex = 0;
        }
    }

    public FragmentTabAdapter(FragmentActivity fragmentActivity2, List<BaseFragment> fragments2, int fragmentContentId2) {
        this.fragments = fragments2;
        this.fragmentActivity = fragmentActivity2;
        this.fragmentContentId = fragmentContentId2;
    }

    public void init() {
        FragmentTransaction ft = this.fragmentActivity.getSupportFragmentManager().beginTransaction();
        ft.add(this.fragmentContentId, this.fragments.get(0));
        ft.commit();
    }

    public void checkedIndex(int checkedIndex) {
        if (this.currentIndex != checkedIndex || this.isPause) {
            this.currentIndex = checkedIndex;
            if (this.fragments.size() > this.currentIndex) {
                Fragment fragment = this.fragments.get(this.currentIndex);
                FragmentTransaction ft = obtainFragmentTransaction(this.currentIndex);
                getCurrentFragment().SaveData();
                getCurrentFragment().onPause();
                if (fragment.isAdded()) {
                    fragment.onResume();
                } else {
                    ft.add(this.fragmentContentId, fragment);
                }
                showTab(this.currentIndex);
                ft.commit();
            }
        }
    }

    private void showTab(int idx) {
        for (int i = 0; i < this.fragments.size(); i++) {
            BaseFragment fragment = this.fragments.get(i);
            FragmentTransaction ft = obtainFragmentTransaction(idx);
            if (idx == i) {
                ft.show(fragment);
            } else {
                ft.hide(fragment);
            }
            ft.commit();
        }
        this.currentTab = idx;
    }

    private FragmentTransaction obtainFragmentTransaction(int index) {
        return this.fragmentActivity.getSupportFragmentManager().beginTransaction();
    }

    public int getCount() {
        return this.fragments.size();
    }

    public int getCurrentTab() {
        return this.currentTab;
    }

    public BaseFragment getCurrentFragment() {
        if (this.fragments.size() > 0) {
            return this.fragments.get(this.currentTab);
        }
        return null;
    }

    public BaseFragment getFragment(int index) {
        if (index < this.fragments.size()) {
            return this.fragments.get(index);
        }
        return null;
    }
}
