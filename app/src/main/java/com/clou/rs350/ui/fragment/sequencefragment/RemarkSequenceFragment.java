package com.clou.rs350.ui.fragment.sequencefragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.clou.rs350.R;
import com.clou.rs350.db.model.TestItem;
import com.clou.rs350.ui.dialog.NumericDialog;
import com.clou.rs350.ui.fragment.BaseFragment;
import com.itextpdf.text.pdf.PdfObject;
import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.view.annotation.ViewInject;
import com.lidroid.xutils.view.annotation.event.OnClick;

public class RemarkSequenceFragment extends BaseFragment implements View.OnClickListener {
    @ViewInject(R.id.txt_humidity)
    private TextView HumidityTextView;
    @ViewInject(R.id.txt_temperature)
    private TextView TemperatureTextView;
    TestItem m_Data;
    @ViewInject(R.id.txt_remark)
    private TextView txt_Remark;

    public RemarkSequenceFragment(TestItem data) {
        this.m_Data = data;
    }

    @Override // android.support.v4.app.Fragment
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        this.fragmentView = inflater.inflate(R.layout.fragment_sequence_remark, (ViewGroup) null);
        ViewUtils.inject(this, this.fragmentView);
        return this.fragmentView;
    }

    @Override // android.support.v4.app.Fragment, com.clou.rs350.ui.fragment.BaseFragment
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initView();
    }

    private void initView() {
    }

    private void showData() {
        this.txt_Remark.setText(this.m_Data.Remark);
        this.TemperatureTextView.setText(this.m_Data.Temperature.replace("℃", PdfObject.NOTHING));
        this.HumidityTextView.setText(this.m_Data.Humidity.replace("%", PdfObject.NOTHING));
    }

    @Override // android.support.v4.app.Fragment, com.clou.rs350.ui.fragment.BaseFragment
    public void onResume() {
        showData();
        super.onResume();
    }

    private void parseData() {
        this.m_Data.Remark = this.txt_Remark.getText().toString();
        this.m_Data.Temperature = String.valueOf(this.TemperatureTextView.getText().toString()) + "℃";
        this.m_Data.Humidity = String.valueOf(this.HumidityTextView.getText().toString()) + "%";
    }

    /* access modifiers changed from: protected */
    @Override // com.clou.rs350.ui.fragment.BaseFragment
    public void copyData() {
        parseData();
    }

    @OnClick({R.id.txt_temperature, R.id.txt_humidity})
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.txt_temperature:
                NumericDialog numericDialog = new NumericDialog(this.m_Context);
                numericDialog.txtLimit = 5;
                numericDialog.setMax(60.0d);
                numericDialog.setMin(-40.0d);
                numericDialog.showMyDialog(this.TemperatureTextView);
                return;
            case R.id.txt_humidity:
                NumericDialog numericDialog2 = new NumericDialog(this.m_Context);
                numericDialog2.setMax(100.0d);
                numericDialog2.setMin(0.0d);
                numericDialog2.setMinus(false);
                numericDialog2.txtLimit = 4;
                numericDialog2.showMyDialog(this.HumidityTextView);
                return;
            default:
                return;
        }
    }
}
