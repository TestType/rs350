package com.clou.rs350.ui.activity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.os.Process;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.clou.rs350.Preferences;
import com.clou.rs350.R;
import com.clou.rs350.constants.ConstExtras;
import com.clou.rs350.device.MeterBaseDevice;
import com.clou.rs350.model.ClouData;
import com.clou.rs350.task.ISleepCallback;
import com.clou.rs350.task.TimerThread;
import com.clou.rs350.version.IUpdateNewVersionCallback;
import com.clou.rs350.version.UpdateUtils;
import com.clou.rs350.version.UpdateVersionManager;
import com.clou.rs350.version.VersionInfo;
import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.view.annotation.ViewInject;
import com.lidroid.xutils.view.annotation.event.OnClick;

import java.util.Timer;
import java.util.TimerTask;

@SuppressLint({"NewApi"})
public class MainActivity extends BaseActivity {
    private static Boolean isExit = false;
    private IUpdateNewVersionCallback callback = new IUpdateNewVersionCallback() {
        /* class com.clou.rs350.ui.activity.MainActivity.AnonymousClass1 */

        @Override // com.clou.rs350.version.IUpdateNewVersionCallback
        public void onNewVersionCallback(VersionInfo newVersion) {
            MainActivity.this.img_system_update.setVisibility(UpdateUtils.isNewVersionExist(newVersion, MainActivity.this.m_Context) ? View.VISIBLE : View.GONE);
        }
    };
    @ViewInject(R.id.database_img)
    private ImageView img_data;
    @ViewInject(R.id.error_img)
    private ImageView img_error;
    @ViewInject(R.id.realtime_img)
    private ImageView img_real;
    @ViewInject(R.id.system_img)
    private ImageView img_system;
    @ViewInject(R.id.red_point_update)
    private ImageView img_system_update;
    @ViewInject(R.id.ts_layout)
    private LinearLayout layout_TestSequence;
    private boolean m_CNVersion;
    private TimerThread readDataThread;
    private UpdateVersionManager updateVersionManager;

    /* access modifiers changed from: protected */
    @Override // com.clou.rs350.ui.activity.BaseActivity, android.support.v4.app.FragmentActivity
    public void onCreate(Bundle savedInstanceState) {
        this.TAG = "MainActivity";
        super.onCreate(savedInstanceState);
        this.m_CNVersion = ClouData.getInstance().getSettingFunction().CNVersion;
        if (this.m_CNVersion) {
            setContentView(R.layout.activity_main_cn);
        } else {
            setContentView(R.layout.activity_main);
        }
        ViewUtils.inject(this);
        checkToSystemSettingActivity();
        checkNewVersion();
        initView();
        initData();
    }

    private void initView() {
    }

    private void initData() {
        if (Preferences.getBoolean(Preferences.Key.HAS_CRASH, true)) {
            Intent intent = this.m_Context.getPackageManager().getLaunchIntentForPackage(this.m_Context.getPackageName());
            //268468224
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK );
            startActivity(intent);
            Process.killProcess(Process.myPid());
            System.exit(0);
        }
        Preferences.putBoolean(Preferences.Key.HAS_CRASH, true);
    }

    /* access modifiers changed from: protected */
    @Override // com.clou.rs350.ui.activity.BaseActivity, android.support.v4.app.FragmentActivity
    public void onResume() {
        super.onResume();
        initFunction();
        readVersion();
    }

    private void readVersion() {
        int refreshTime = Preferences.getInt(Preferences.Key.REFRESHTIME, 5000);
        if (refreshTime < 5000) {
            refreshTime = 5000;
        }
        if (this.readDataThread == null || this.readDataThread.isStop()) {
            this.readDataThread = new TimerThread((long) refreshTime, new ISleepCallback() {
                /* class com.clou.rs350.ui.activity.MainActivity.AnonymousClass2 */

                @Override // com.clou.rs350.task.ISleepCallback
                public void onSleepAfterToDo() {
                    if (MainActivity.this.messageManager.isConnected()) {
                        MainActivity.this.messageManager.getSerialNumber();
                    }
                }
            }, "Get Data Thread");
            this.readDataThread.start();
        }
    }

    private void initFunction() {
        this.layout_TestSequence.setVisibility(View.VISIBLE);
    }

    /* access modifiers changed from: protected */
    @Override // com.clou.rs350.ui.activity.BaseActivity, android.support.v4.app.FragmentActivity
    public void onPause() {
        super.onPause();
        if (this.readDataThread != null && !this.readDataThread.isStop()) {
            this.readDataThread.stopTimer();
            this.readDataThread.interrupt();
            this.readDataThread = null;
        }
    }

    private void checkNewVersion() {
        this.updateVersionManager = UpdateVersionManager.getInstance(this);
        this.updateVersionManager.checkVersion();
        this.updateVersionManager.registerNewVersionExistBroadcast();
        this.updateVersionManager.getNewVersion(this.callback);
    }

    private void checkToSystemSettingActivity() {
        if (!ClouData.getInstance().isDemo() && TextUtils.isEmpty(Preferences.getString(Preferences.Key.BLUETOOTH_MAC))) {
            Intent intent = new Intent(this, SystemSettingActivity.class);
            intent.putExtra(ConstExtras.SETTING_TAG, 2);
            startActivity(intent);
        }
    }

    @OnClick({R.id.realtime_img, R.id.error_img, R.id.transformer_img, R.id.ts_img, R.id.read_img, R.id.database_img, R.id.system_img})
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.realtime_img:
                startTo(RealTimeActivity.class);
                return;
            case R.id.error_layout:
            case R.id.transformer_layout:
            case R.id.ts_layout:
            case R.id.read_layout:
            case R.id.power_layout:
            case R.id.pt_layout:
            case R.id.ct_layout:
            case R.id.database_layout:
            case R.id.system_layout:
            default:
                return;
            case R.id.error_img:
                if (this.m_CNVersion) {
                    startTo(ErrorMeasurementCNActivity.class);
                    return;
                } else {
                    startTo(ErrorMeasurementActivity.class);
                    return;
                }
            case R.id.transformer_img:
                startTo(TransformerTestActivity.class);
                return;
            case R.id.ts_img:
                startTo(TestSequenceActivity.class);
                return;
            case R.id.read_img:
                startTo(ReadMeterActivity.class);
                return;
            case R.id.database_img:
                startTo(DataBaseActivity.class);
                return;
            case R.id.system_img:
                startTo(SystemSettingActivity.class);
                return;
        }
    }

    private void exitDo() {
        Preferences.putBoolean(Preferences.Key.HAS_CRASH, false);
        finish();
    }

    @Override // android.support.v4.app.FragmentActivity
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode != 4) {
            return false;
        }
        exitBy2Click();
        return false;
    }

    private void exitBy2Click() {
        if (!isExit.booleanValue()) {
            isExit = true;
            Toast.makeText(this, (int) R.string.text_double_click_to_exit, Toast.LENGTH_SHORT).show();
            new Timer().schedule(new TimerTask() {
                /* class com.clou.rs350.ui.activity.MainActivity.AnonymousClass3 */

                public void run() {
                    MainActivity.isExit = false;
                }
            }, 2000);
            return;
        }
        exitDo();
    }

    @Override // com.clou.rs350.ui.activity.BaseActivity, com.clou.rs350.manager.MessageManager.IMessageListener
    public void onReceiveMessage(MeterBaseDevice Device) {
        initFunction();
    }

    /* access modifiers changed from: protected */
    @Override // com.clou.rs350.ui.activity.BaseActivity, android.support.v4.app.FragmentActivity
    public void onDestroy() {
        this.updateVersionManager.unregisterNewVersionExistBroadcast();
        this.updateVersionManager.removeCallback(this.callback);
        super.onDestroy();
    }
}
