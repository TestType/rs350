package com.clou.rs350.ui.fragment.sequencefragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.clou.rs350.Preferences;
import com.clou.rs350.R;
import com.clou.rs350.db.dao.MeterTypeDao;
import com.clou.rs350.db.dao.SiteDataManagerDao;
import com.clou.rs350.db.model.CustomerInfo;
import com.clou.rs350.db.model.MeterType;
import com.clou.rs350.db.model.SiteData;
import com.clou.rs350.model.ClouData;
import com.clou.rs350.ui.fragment.BaseFragment;
import com.clou.rs350.ui.view.infoview.SiteDataView;
import com.clou.rs350.utils.InfoUtil;
import com.clou.rs350.utils.OtherUtils;
import com.itextpdf.text.pdf.PdfObject;

public class SiteDataSequenceFragment extends BaseFragment {
    CustomerInfo m_CustomerInfo;
    SiteDataView m_InfoView;
    View m_InfoViewLayout;
    SiteData m_SiteData;

    @Override // android.support.v4.app.Fragment
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        this.fragmentView = inflater.inflate(R.layout.fragment_sequence_site_data, (ViewGroup) null);
        return this.fragmentView;
    }

    @Override // android.support.v4.app.Fragment, com.clou.rs350.ui.fragment.BaseFragment
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initView();
    }

    private void initView() {
        this.m_InfoViewLayout = findViewById(R.id.layout_site_data);
        this.m_InfoView = new SiteDataView(this.m_Context, this.m_InfoViewLayout, this.m_SiteData, 4);
        this.m_InfoView.setRequiredField(this.m_MustDo);
    }

    @Override // android.support.v4.app.Fragment, com.clou.rs350.ui.fragment.BaseFragment
    public void onResume() {
        this.m_CustomerInfo = ClouData.getInstance().getCustomerInfo();
        this.m_SiteData = ClouData.getInstance().getSiteData();
        if (!this.m_SiteData.CustomerSr.equals(this.m_CustomerInfo.CustomerSr)) {
            this.m_SiteData = new SiteData();
            this.m_SiteData.CustomerSr = this.m_CustomerInfo.CustomerSr;
            SiteData tmpData = (SiteData) new SiteDataManagerDao(this.m_Context).queryObjectBySerialNo(this.m_SiteData.CustomerSr);
            if (tmpData != null) {
                this.m_SiteData = tmpData;
            }
        }
        this.m_InfoView.refreshData(this.m_SiteData);
        super.onResume();
    }

    @Override // android.support.v4.app.Fragment, com.clou.rs350.ui.fragment.BaseFragment
    public void onPause() {
        super.onPause();
    }

    /* access modifiers changed from: protected */
    @Override // com.clou.rs350.ui.fragment.BaseFragment
    public void copyData() {
        this.m_SiteData = this.m_InfoView.getInfo();
        int tmpCount = 1;
        MeterTypeDao tmpTypeDao = new MeterTypeDao(this.m_Context);
        ClouData.getInstance().setSiteData(this.m_SiteData);
        if (!OtherUtils.isEmpty(this.m_SiteData.MeterInfo[0].NominalVoltage)) {
            Preferences.putFloat(Preferences.Key.VOLTAGENOMINAL, (float) OtherUtils.parseDouble(this.m_SiteData.MeterInfo[0].NominalVoltage.replace("V", PdfObject.NOTHING)));
        }
        for (int i = 0; i < this.m_SiteData.Meter_Count; i++) {
            MeterType tmpType = tmpTypeDao.queryObjectByType(this.m_SiteData.MeterInfo[i].Type);
            if (tmpType != null) {
                this.m_SiteData.MeterInfo[i].CopyTypeData(tmpType);
            }
            tmpCount |= 1 << i;
        }
        ClouData.getInstance().setTestMeterCount(tmpCount);
        InfoUtil.setRatio(this.m_SiteData, this.m_Context);
    }

    @Override // com.clou.rs350.ui.fragment.BaseFragment
    public int isFinish() {
        if (!this.m_InfoView.hasSetParams()) {
            return 1;
        }
        return 0;
    }
}
