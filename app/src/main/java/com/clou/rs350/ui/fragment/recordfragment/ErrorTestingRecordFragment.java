package com.clou.rs350.ui.fragment.recordfragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.clou.rs350.R;
import com.clou.rs350.db.model.ErrorTest;
import com.clou.rs350.ui.adapter.ErrorAdapter;
import com.clou.rs350.ui.fragment.BaseFragment;
import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.view.annotation.ViewInject;

public class ErrorTestingRecordFragment extends BaseFragment {
    @ViewInject(R.id.list_errors)
    private ListView lstError;
    private ErrorTest m_ErrorTest;
    @ViewInject(R.id.txt_constant)
    private TextView txt_Constant;
    @ViewInject(R.id.txt_eavg)
    private TextView txt_Eavg;
    @ViewInject(R.id.txt_es)
    private TextView txt_Es;
    @ViewInject(R.id.txt_meter_sr)
    private TextView txt_Meter_Sr;
    @ViewInject(R.id.txt_pulses)
    private TextView txt_Pulse;
    @ViewInject(R.id.txt_i1)
    private TextView txt_i1;
    @ViewInject(R.id.txt_i2)
    private TextView txt_i2;
    @ViewInject(R.id.txt_i3)
    private TextView txt_i3;
    @ViewInject(R.id.txt_cos1)
    private TextView txt_pf1;
    @ViewInject(R.id.txt_cos2)
    private TextView txt_pf2;
    @ViewInject(R.id.txt_cos3)
    private TextView txt_pf3;
    @ViewInject(R.id.txt_psum)
    private TextView txt_sump;
    @ViewInject(R.id.txt_pfsum)
    private TextView txt_sumpf;
    @ViewInject(R.id.txt_qsum)
    private TextView txt_sumq;
    @ViewInject(R.id.txt_sums)
    private TextView txt_sums;
    @ViewInject(R.id.txt_u1)
    private TextView txt_u1;
    @ViewInject(R.id.txt_u2)
    private TextView txt_u2;
    @ViewInject(R.id.txt_u3)
    private TextView txt_u3;

    public ErrorTestingRecordFragment(ErrorTest data) {
        this.m_ErrorTest = data;
    }

    @Override // android.support.v4.app.Fragment
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        this.fragmentView = inflater.inflate(R.layout.fragment_record_error_testing, (ViewGroup) null);
        ViewUtils.inject(this, this.fragmentView);
        return this.fragmentView;
    }

    @Override // android.support.v4.app.Fragment, com.clou.rs350.ui.fragment.BaseFragment
    public void onResume() {
        super.onResume();
        showData();
    }

    @Override // android.support.v4.app.Fragment, com.clou.rs350.ui.fragment.BaseFragment
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initView();
    }

    private void initView() {
    }

    private void showData() {
        this.txt_Meter_Sr.setText(this.m_ErrorTest.MeterSerialNo);
        this.txt_u1.setText(this.m_ErrorTest.VoltageValueL1Show.s_Value);
        this.txt_u2.setText(this.m_ErrorTest.VoltageValueL2Show.s_Value);
        this.txt_u3.setText(this.m_ErrorTest.VoltageValueL3Show.s_Value);
        this.txt_i1.setText(this.m_ErrorTest.CurrentValueL1.s_Value);
        this.txt_i2.setText(this.m_ErrorTest.CurrentValueL2.s_Value);
        this.txt_i3.setText(this.m_ErrorTest.CurrentValueL3.s_Value);
        this.txt_pf1.setText(this.m_ErrorTest.PowerFactorL1.s_Value);
        this.txt_pf2.setText(this.m_ErrorTest.PowerFactorL2.s_Value);
        this.txt_pf3.setText(this.m_ErrorTest.PowerFactorL3.s_Value);
        this.txt_sumpf.setText(this.m_ErrorTest.PowerFactorTotal.s_Value);
        this.txt_sump.setText(this.m_ErrorTest.ActivePowerTotal.s_Value);
        this.txt_sumq.setText(this.m_ErrorTest.ReactivePowerTotal.s_Value);
        this.txt_sums.setText(this.m_ErrorTest.ApparentPowerTotal.s_Value);
        this.txt_Constant.setText(String.valueOf(this.m_ErrorTest.MeterConstant) + this.m_ErrorTest.ConstantUnit);
        this.txt_Pulse.setText(this.m_ErrorTest.Pulses);
        this.txt_Eavg.setText(this.m_ErrorTest.ErrorAverage);
        this.txt_Es.setText(this.m_ErrorTest.ErrorStandard);
        this.lstError.setAdapter((ListAdapter) new ErrorAdapter(this.m_ErrorTest, this.m_Context));
    }

    @Override // android.support.v4.app.Fragment, com.clou.rs350.ui.fragment.BaseFragment
    public void onPause() {
        super.onPause();
    }
}
