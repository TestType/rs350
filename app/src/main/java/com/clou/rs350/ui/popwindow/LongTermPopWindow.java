package com.clou.rs350.ui.popwindow;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.clou.rs350.R;
import com.clou.rs350.db.model.DBDataModel;
import com.clou.rs350.db.model.LTM;
import com.clou.rs350.manager.MessageManager;
import com.clou.rs350.ui.dialog.NumericDialog;
import com.clou.rs350.utils.OtherUtils;

public class LongTermPopWindow extends PopupWindow implements View.OnClickListener, CompoundButton.OnCheckedChangeListener {
    CheckBox chk_start_at_defined_time;
    Context mContext;
    View.OnClickListener mOnClick;
    LTM m_Data;
    View m_ParentView;
    TextView txt_interval;
    TextView txt_period;
    TextView txt_start;
    TextView txt_stop;

    public LongTermPopWindow(Context context) {
        super(context);
        this.mContext = context;
        initView();
    }

    private void initView() {
        View conentView = ((LayoutInflater) this.mContext.getSystemService("layout_inflater")).inflate(R.layout.pop_window_longtime, (ViewGroup) null);
        conentView.findViewById(R.id.ok).setOnClickListener(this);
        conentView.findViewById(R.id.cancel).setOnClickListener(this);
        this.txt_interval = (TextView) conentView.findViewById(R.id.txt_interval);
        this.txt_interval.setOnClickListener(this);
        this.txt_period = (TextView) conentView.findViewById(R.id.txt_period);
        this.txt_period.setOnClickListener(this);
        this.chk_start_at_defined_time = (CheckBox) conentView.findViewById(R.id.chk_start_at_defined_time);
        this.chk_start_at_defined_time.setOnCheckedChangeListener(this);
        this.txt_start = (TextView) conentView.findViewById(R.id.txt_start_time);
        this.txt_start.setOnClickListener(this);
        this.txt_stop = (TextView) conentView.findViewById(R.id.txt_stop_time);
        this.txt_stop.setOnClickListener(this);
        setContentView(conentView);
        setWidth(this.mContext.getResources().getDimensionPixelSize(R.dimen.dp_420));
        setHeight(this.mContext.getResources().getDimensionPixelSize(R.dimen.dp_220));
        setFocusable(true);
        setOutsideTouchable(true);
        update();
        setBackgroundDrawable(new ColorDrawable(0));
        setAnimationStyle(16973826);
    }

    public void showPopWindow(View currentClick, LTM Data, View.OnClickListener onclick) {
        this.mOnClick = onclick;
        this.m_Data = Data;
        this.m_ParentView = currentClick;
        showValue();
        if (!isShowing()) {
            int[] location = new int[2];
            currentClick.getLocationOnScreen(location);
            showAtLocation(currentClick, 0, location[0] - getWidth(), location[1]);
            return;
        }
        dismiss();
    }

    private void showValue() {
        this.txt_interval.setText(this.m_Data.Interval.s_Value);
        this.txt_period.setText(this.m_Data.Period.s_Value);
        this.chk_start_at_defined_time.setChecked(this.m_Data.WithDefindTime);
        SetButton(this.m_Data.WithDefindTime);
        this.txt_start.setText(this.m_Data.DefindStartTime.s_Value);
        this.txt_start.setTag(Long.valueOf(this.m_Data.DefindStartTime.l_Value));
        this.txt_stop.setText(this.m_Data.DefindStopTime.s_Value);
        this.txt_stop.setTag(Long.valueOf(this.m_Data.DefindStopTime.l_Value));
    }

    private void praseValue() {
        boolean z;
        praseIntegrationValue(0);
        MessageManager.getInstance(this.mContext).setIntegrationParameter((int) this.m_Data.Interval.l_Value, (int) this.m_Data.Period.l_Value);
        this.m_Data.DefindStartTime = new DBDataModel(((Number) this.txt_start.getTag()).longValue(), this.txt_start.getText().toString());
        this.m_Data.DefindStopTime = new DBDataModel(((Number) this.txt_stop.getTag()).longValue(), this.txt_stop.getText().toString());
        LTM ltm = this.m_Data;
        if (!this.chk_start_at_defined_time.isChecked() || (this.m_Data.DefindStartTime.l_Value == 0 && this.m_Data.DefindStopTime.l_Value == 0)) {
            z = false;
        } else {
            z = true;
        }
        ltm.WithDefindTime = z;
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private void praseIntegrationValue(int flag) {
        int interval = 1;
        int period = 1;
        if (!OtherUtils.isEmpty(this.txt_interval)) {
            interval = OtherUtils.parseInt(this.txt_interval.getText().toString());
        }
        if (!OtherUtils.isEmpty(this.txt_period)) {
            period = OtherUtils.parseInt(this.txt_period.getText().toString());
        }
        if (1 > period) {
            period = 1;
        }
        if (99 < period) {
            period = 99;
        }
        if (1 > interval) {
            interval = 1;
        }
        if (99 < interval) {
            interval = 99;
        }
        this.m_Data.Interval = new DBDataModel((long) interval, new StringBuilder(String.valueOf(interval)).toString());
        this.m_Data.Period = new DBDataModel((long) period, new StringBuilder(String.valueOf(period)).toString());
    }

    private void onIntegrationValueClick(View v, final int flag) {
        NumericDialog nd = new NumericDialog(this.mContext);
        nd.m_Dot = false;
        nd.setMinus(false);
        nd.setOnDoneClick(new View.OnClickListener() {
            /* class com.clou.rs350.ui.popwindow.LongTermPopWindow.AnonymousClass1 */

            public void onClick(View v) {
                LongTermPopWindow.this.praseIntegrationValue(flag);
                LongTermPopWindow.this.txt_interval.setText(LongTermPopWindow.this.m_Data.Interval.s_Value);
                LongTermPopWindow.this.txt_period.setText(LongTermPopWindow.this.m_Data.Period.s_Value);
            }
        });
        nd.showMyDialog((TextView) v);
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ok:
                praseValue();
                v.setTag(this.m_Data.Interval);
                if (this.mOnClick != null) {
                    this.mOnClick.onClick(v);
                }
                if (isShowing()) {
                    dismiss();
                    return;
                }
                return;
            case R.id.cancel:
                if (isShowing()) {
                    dismiss();
                    return;
                }
                return;
            case R.id.txt_interval:
                onIntegrationValueClick(v, 1);
                return;
            case R.id.txt_period:
                onIntegrationValueClick(v, 0);
                return;
            case R.id.txt_start_time:
            case R.id.txt_stop_time:
                new DateAndTimeSettingPopWindow(this.mContext).showPopWindow(v, this.m_ParentView, null);
                return;
            default:
                return;
        }
    }

    private void SetButton(boolean flag) {
        this.txt_start.setEnabled(flag);
        this.txt_stop.setEnabled(flag);
    }

    public void onCheckedChanged(CompoundButton v, boolean isChecked) {
        switch (v.getId()) {
            case R.id.chk_start_at_defined_time:
                SetButton(isChecked);
                return;
            default:
                return;
        }
    }
}
