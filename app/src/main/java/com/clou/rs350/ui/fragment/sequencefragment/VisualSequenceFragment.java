package com.clou.rs350.ui.fragment.sequencefragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.clou.rs350.R;
import com.clou.rs350.db.dao.sequence.VisualSchemeDao;
import com.clou.rs350.db.model.sequence.VisualData;
import com.clou.rs350.db.model.sequence.VisualItem;
import com.clou.rs350.model.ClouData;
import com.clou.rs350.ui.adapter.VisualItemAdapter;
import com.clou.rs350.ui.dialog.HintDialog;
import com.clou.rs350.ui.fragment.BaseFragment;
import com.clou.rs350.ui.popwindow.ListPopWindow;
import com.clou.rs350.utils.OtherUtils;
import com.itextpdf.text.pdf.PdfObject;
import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.view.annotation.ViewInject;
import com.lidroid.xutils.view.annotation.event.OnClick;

import java.util.List;

public class VisualSequenceFragment extends BaseFragment implements View.OnClickListener {
    private static final String TAG = "VisualRecordFragment";
    @ViewInject(R.id.list_visual)
    private ListView list_Visual;
    private VisualItemAdapter m_Adapter;
    private VisualData m_Data;
    @ViewInject(R.id.txt_remark)
    private TextView txt_Remark;
    @ViewInject(R.id.txt_scheme)
    private TextView txt_Scheme;

    public VisualSequenceFragment(VisualData data) {
        this.m_Data = data;
    }

    @Override // android.support.v4.app.Fragment
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        this.fragmentView = inflater.inflate(R.layout.fragment_sequence_visualinspection, (ViewGroup) null);
        ViewUtils.inject(this, this.fragmentView);
        return this.fragmentView;
    }

    @Override // android.support.v4.app.Fragment, com.clou.rs350.ui.fragment.BaseFragment
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        this.m_Adapter = new VisualItemAdapter(this.m_Context, this.m_MustDo);
        this.list_Visual.setAdapter((ListAdapter) this.m_Adapter);
    }

    @OnClick({R.id.txt_scheme})
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.txt_scheme:
                List<String> tmpNames = new VisualSchemeDao(this.m_Context).queryAllKey(ClouData.getInstance(this.m_Context).getSettingBasic().Language);
                ListPopWindow.showListPopwindow(this.m_Context, (String[]) tmpNames.toArray(new String[tmpNames.size()]), v, new View.OnClickListener() {
                    /* class com.clou.rs350.ui.fragment.sequencefragment.VisualSequenceFragment.AnonymousClass1 */

                    public void onClick(View v) {
                        VisualData tmpScheme = (VisualData) new VisualSchemeDao(VisualSequenceFragment.this.m_Context).queryByKey(((TextView) v).getText().toString());
                        VisualSequenceFragment.this.m_Data.SchemeName = tmpScheme.SchemeName;
                        VisualSequenceFragment.this.m_Data.Visuals = tmpScheme.Visuals;
                        VisualSequenceFragment.this.m_Adapter.refresh(VisualSequenceFragment.this.m_Data.Visuals);
                    }
                });
                return;
            default:
                return;
        }
    }

    private void showData() {
        this.txt_Scheme.setText(this.m_Data.SchemeName);
        this.m_Adapter.refresh(this.m_Data.Visuals);
        this.txt_Remark.setText(this.m_Data.Remark);
    }

    @Override // android.support.v4.app.Fragment, com.clou.rs350.ui.fragment.BaseFragment
    public void onResume() {
        showData();
        super.onResume();
    }

    private void parseData() {
        this.m_Data.Visuals = this.m_Adapter.getData();
        this.m_Data.Remark = this.txt_Remark.getText().toString();
    }

    /* access modifiers changed from: protected */
    @Override // com.clou.rs350.ui.fragment.BaseFragment
    public void copyData() {
        parseData();
    }

    @Override // android.support.v4.app.Fragment, com.clou.rs350.ui.fragment.BaseFragment
    public void onPause() {
        super.onPause();
    }

    @Override // com.clou.rs350.ui.fragment.BaseFragment
    public int isFinish() {
        int result = 0;
        if (1 != this.m_MustDo) {
            return 0;
        }
        String strHint = PdfObject.NOTHING;
        parseData();
        if (OtherUtils.isEmpty(this.m_Data.SchemeName)) {
            new HintDialog(this.m_Context, (int) R.string.text_select_scheme).show();
            return 1;
        }
        for (int i = 0; i < this.m_Data.Visuals.size(); i++) {
            VisualItem tmpItem = this.m_Data.Visuals.get(i);
            if (1 == tmpItem.RequiredField && -1 == tmpItem.Result.l_Value) {
                result = 1;
                strHint = String.valueOf(strHint) + tmpItem.Activities + " " + this.m_Context.getResources().getString(R.string.text_is_empty) + "\r\n";
            }
        }
        if (result == 0) {
            return result;
        }
        new HintDialog(this.m_Context, strHint.substring(0, strHint.length() - 2)).show();
        return result;
    }
}
