package com.clou.rs350.ui.model;

import com.clou.rs350.db.model.DBDataModel;
import com.itextpdf.text.pdf.PdfObject;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class LTMData implements Serializable {
    public int color = 16777215;
    public List<DBDataModel> dataList = new ArrayList();
    public String name = PdfObject.NOTHING;

    public LTMData() {
    }

    public LTMData(String name2) {
        this.name = name2;
    }
}
