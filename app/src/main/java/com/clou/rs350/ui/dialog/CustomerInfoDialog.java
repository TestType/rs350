package com.clou.rs350.ui.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.clou.rs350.R;
import com.clou.rs350.db.dao.CustomerInfoDao;
import com.clou.rs350.db.model.CustomerInfo;
import com.clou.rs350.model.ClouData;
import com.clou.rs350.ui.view.infoview.CustomerInfoView;

public class CustomerInfoDialog extends Dialog {
    Button btn_Close = null;
    Button btn_Modify = null;
    Button btn_SiteData = null;
    TextView contentView = null;
    boolean isModefy = false;
    Context m_Context = null;
    CustomerInfo m_Info;
    CustomerInfoView m_InfoView;
    View m_InfoViewLayout;
    private View.OnClickListener okClick = null;

    public CustomerInfoDialog(Context context) {
        super(context, R.style.dialog);
        requestWindowFeature(1);
        setCanceledOnTouchOutside(true);
        this.m_Context = context;
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.m_Info = ClouData.getInstance().getCustomerInfo();
        setContentView(R.layout.dialog_customer_info);
        this.m_InfoViewLayout = findViewById(R.id.layout_customer_info);
        this.m_InfoView = new CustomerInfoView(this.m_Context, this.m_InfoViewLayout, this.m_Info, 0);
        this.btn_Close = (Button) findViewById(R.id.btn_close);
        this.btn_Close.setOnClickListener(new View.OnClickListener() {
            /* class com.clou.rs350.ui.dialog.CustomerInfoDialog.AnonymousClass1 */

            public void onClick(View v) {
                if (CustomerInfoDialog.this.isModefy) {
                    CustomerInfoDialog.this.StopModify();
                    CustomerInfoDialog.this.m_InfoView.refreshData(CustomerInfoDialog.this.m_Info);
                    return;
                }
                CustomerInfoDialog.this.dismiss();
            }
        });
        this.btn_Modify = (Button) findViewById(R.id.btn_modify);
        this.btn_Modify.setOnClickListener(new View.OnClickListener() {
            /* class com.clou.rs350.ui.dialog.CustomerInfoDialog.AnonymousClass2 */

            public void onClick(View v) {
                if (CustomerInfoDialog.this.isModefy) {
                    CustomerInfoDialog.this.Save();
                } else {
                    CustomerInfoDialog.this.Modify();
                }
            }
        });
        this.btn_SiteData = (Button) findViewById(R.id.btn_site_data);
        this.btn_SiteData.setOnClickListener(new View.OnClickListener() {
            /* class com.clou.rs350.ui.dialog.CustomerInfoDialog.AnonymousClass3 */

            public void onClick(View v) {
                new SiteDataInfoDialog(CustomerInfoDialog.this.m_Context).show();
            }
        });
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private void Modify() {
        this.m_InfoView.setFunction(2);
        this.btn_Modify.setText(R.string.text_save);
        this.btn_Close.setText(R.string.text_cancel);
        this.isModefy = true;
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private void Save() {
        StopModify();
        this.m_Info = this.m_InfoView.getInfo();
        if (this.m_Info.isNew) {
            new CustomerInfoDao(this.m_Context).insertObject(this.m_Info);
            this.m_Info.isNew = false;
            return;
        }
        new CustomerInfoDao(this.m_Context).updateObjectByKey(this.m_Info.CustomerSr, this.m_Info);
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private void StopModify() {
        this.m_InfoView.setFunction(0);
        this.btn_Modify.setText(R.string.text_modify);
        this.btn_Close.setText(R.string.text_close);
        this.isModefy = false;
    }

    public void setOnClick(View.OnClickListener okClick2) {
        this.okClick = okClick2;
    }
}
