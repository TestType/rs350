package com.clou.rs350.ui.dialog;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;

import com.clou.rs350.Preferences;
import com.clou.rs350.R;
import com.clou.rs350.callback.ILoadCallback;
import com.clou.rs350.constants.Constants;
import com.clou.rs350.db.dao.CustomerInfoDao;
import com.clou.rs350.db.dao.SiteDataManagerDao;
import com.clou.rs350.db.dao.SiteDataTestDao;
import com.clou.rs350.db.dao.TestItemDao;
import com.clou.rs350.db.model.CustomerInfo;
import com.clou.rs350.db.model.SiteData;
import com.clou.rs350.db.model.TestItem;
import com.clou.rs350.model.ClouData;
import com.clou.rs350.task.MyTask;
import com.clou.rs350.ui.popwindow.ListPopWindow;
import com.clou.rs350.utils.DoubleClick;
import com.clou.rs350.utils.InfoUtil;
import com.clou.rs350.utils.OtherUtils;
import com.itextpdf.text.pdf.PdfObject;

import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.List;

import google.zxing.camera.CaptureDialog;

public class SaveErrorDialog extends BaseSaveDialog implements View.OnClickListener {
    private TextView CustomerSerialNoTextView = null;
    private TextView Meter1SerialNoTextView = null;
    private TextView Meter2SerialNoTextView = null;
    private TextView Meter3SerialNoTextView = null;
    private TextView[] MeterSerialNoTextView = null;
    private CheckBox chk_Meter1Enable;
    private CheckBox chk_Meter2Enable;
    private CheckBox chk_Meter3Enable;
    private CheckBox[] chk_MeterEnables;
    private IAfterSave m_AfterSave;
    private ISave m_SaveData;
    private String m_TestItem = PdfObject.NOTHING;

    public interface IAfterSave {
        void AfterSave(boolean z);
    }

    public interface ISave {
        boolean SaveMeasureData(String[] strArr, String str);
    }

    public SaveErrorDialog(Context context, String testItem, ISave SaveData, IAfterSave AfterSave) {
        super(context);
        this.m_TestItem = testItem;
        this.m_SaveData = SaveData;
        this.m_AfterSave = AfterSave;
    }

    /* access modifiers changed from: protected */
    @Override // com.clou.rs350.ui.dialog.BaseSaveDialog
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_save_error);
        setCanceledOnTouchOutside(true);
        WindowManager.LayoutParams lp = getWindow().getAttributes();
        lp.width = getContext().getResources().getDisplayMetrics().widthPixels;
        lp.height = getContext().getResources().getDisplayMetrics().heightPixels;
        getWindow().setAttributes(lp);
        initView();
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private void parseSiteData() {
        SiteData tmpData = ClouData.getInstance().getSiteData();
        if (OtherUtils.isEmpty(tmpData.CustomerSr)) {
            tmpData = (SiteData) new SiteDataManagerDao(this.m_Context).queryObjectBySerialNo(this.CustomerSerialNoTextView.getText().toString());
        } else {
            tmpData.CustomerSr = this.CustomerSerialNoTextView.getText().toString();
        }
        if (tmpData != null) {
            ClouData.getInstance().setSiteData(tmpData);
            if (!OtherUtils.isEmpty(tmpData.MeterInfo[0].NominalVoltage)) {
                Preferences.putFloat(Preferences.Key.VOLTAGENOMINAL, (float) OtherUtils.parseDouble(tmpData.MeterInfo[0].NominalVoltage.replace("V", PdfObject.NOTHING)));
            }
        }
    }

    private void initView() {
        boolean z;
        boolean z2;
        findViewById(R.id.btn_scan).setOnClickListener(this);
        findViewById(R.id.btn_select).setOnClickListener(this);
        findViewById(R.id.btn_scan_0).setOnClickListener(this);
        findViewById(R.id.btn_scan_1).setOnClickListener(this);
        findViewById(R.id.btn_scan_2).setOnClickListener(this);
        this.SaveBtn = (Button) findViewById(R.id.btn_save);
        this.SaveBtn.setOnClickListener(this);
        this.SaveBtn.setFocusable(true);
        this.CancelBtn = (Button) findViewById(R.id.btn_cancel);
        this.CancelBtn.setOnClickListener(this);
        findViewById(R.id.btn_customer_detail).setOnClickListener(this);
        findViewById(R.id.btn_meter1_detail).setOnClickListener(this);
        findViewById(R.id.btn_meter2_detail).setOnClickListener(this);
        findViewById(R.id.btn_meter3_detail).setOnClickListener(this);
        this.CustomerSerialNoTextView = (TextView) findViewById(R.id.txt_customer_sr);
        this.CustomerSerialNoTextView.requestFocus();
        this.CustomerSerialNoTextView.clearFocus();
        this.CustomerSerialNoTextView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            /* class com.clou.rs350.ui.dialog.SaveErrorDialog.AnonymousClass1 */

            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == 6 || actionId == 5) {
                    SaveErrorDialog.this.parseSiteData();
                }
                Log.i("actionId", new StringBuilder(String.valueOf(actionId)).toString());
                return false;
            }
        });
        this.Meter1SerialNoTextView = (TextView) findViewById(R.id.txt_meter1_sr);
        this.Meter2SerialNoTextView = (TextView) findViewById(R.id.txt_meter2_sr);
        this.Meter3SerialNoTextView = (TextView) findViewById(R.id.txt_meter3_sr);
        this.MeterSerialNoTextView = new TextView[]{this.Meter1SerialNoTextView, this.Meter2SerialNoTextView, this.Meter3SerialNoTextView};
        this.TemperatureTextView = (TextView) findViewById(R.id.txt_temperature);
        this.TemperatureTextView.setOnClickListener(this);
        this.HumidityTextView = (TextView) findViewById(R.id.txt_humidity);
        this.HumidityTextView.setOnClickListener(this);
        this.RemarkTextView = (TextView) findViewById(R.id.txt_remark);
        this.chk_Meter1Enable = (CheckBox) findViewById(R.id.chk_meter1);
        this.chk_Meter2Enable = (CheckBox) findViewById(R.id.chk_meter2);
        this.chk_Meter3Enable = (CheckBox) findViewById(R.id.chk_meter3);
        ClouData datas = ClouData.getInstance();
        this.CustomerSerialNoTextView.setText(datas.getCustomerInfo().CustomerSr);
        this.Meter1SerialNoTextView.setText(datas.getMeterBaseInfo()[0].SerialNo);
        this.Meter2SerialNoTextView.setText(datas.getMeterBaseInfo()[1].SerialNo);
        this.Meter3SerialNoTextView.setText(datas.getMeterBaseInfo()[2].SerialNo);
        this.TemperatureTextView.setText(datas.getTestItem().Temperature.replace("℃", PdfObject.NOTHING));
        this.HumidityTextView.setText(datas.getTestItem().Humidity.replace("%", PdfObject.NOTHING));
        this.RemarkTextView.setText(datas.getTestItem().Remark);
        int testMeterCount = ClouData.getInstance().getTestMeterCount();
        this.chk_Meter1Enable.setChecked((testMeterCount & 1) == 1);
        CheckBox checkBox = this.chk_Meter2Enable;
        if ((testMeterCount & 2) == 2) {
            z = true;
        } else {
            z = false;
        }
        checkBox.setChecked(z);
        CheckBox checkBox2 = this.chk_Meter3Enable;
        if ((testMeterCount & 4) == 4) {
            z2 = true;
        } else {
            z2 = false;
        }
        checkBox2.setChecked(z2);
        this.chk_MeterEnables = new CheckBox[]{this.chk_Meter1Enable, this.chk_Meter2Enable, this.chk_Meter3Enable};
        this.m_DoubleClick = new DoubleClick(new DoubleClick.OnDoubleClick() {
            /* class com.clou.rs350.ui.dialog.SaveErrorDialog.AnonymousClass2 */

            private void searchKey(String condition) {
                List<String> tmpSerials = new CustomerInfoDao(SaveErrorDialog.this.m_Context).queryAllKey(condition);
                ListPopWindow.showListPopwindow(SaveErrorDialog.this.m_Context, (String[]) tmpSerials.toArray(new String[tmpSerials.size()]), SaveErrorDialog.this.CustomerSerialNoTextView, new View.OnClickListener() {
                    /* class com.clou.rs350.ui.dialog.SaveErrorDialog.AnonymousClass2.AnonymousClass1 */

                    public void onClick(View v) {
                        SaveErrorDialog.this.parseData();
                    }
                });
            }

            @Override // com.clou.rs350.utils.DoubleClick.OnDoubleClick
            public void doubleClick() {
                searchKey(SaveErrorDialog.this.CustomerSerialNoTextView.getText().toString());
            }

            @Override // com.clou.rs350.utils.DoubleClick.OnDoubleClick
            public void singleClick() {
                searchKey(PdfObject.NOTHING);
            }
        });
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private void parseData() {
        ClouData datas = ClouData.getInstance();
        String tmpData = this.CustomerSerialNoTextView.getText().toString();
        CustomerInfo customerInfo = new CustomerInfoDao(this.m_Context).queryObjectBySerialNo(tmpData);
        if (customerInfo != null) {
            ClouData.getInstance().setCustomerInfo(customerInfo);
        } else {
            ClouData.getInstance().getCustomerInfo().isNew = true;
        }
        datas.getCustomerInfo().CustomerSr = tmpData;
        datas.getTestItem().CustomerSerialNo = tmpData;
        datas.getTestItem().Temperature = String.valueOf(this.TemperatureTextView.getText().toString()) + "℃";
        datas.getTestItem().Humidity = String.valueOf(this.HumidityTextView.getText().toString()) + "%";
        datas.getTestItem().Remark = this.RemarkTextView.getText().toString();
        datas.getTestItem().parseData(this.m_TestItem);
        parseSiteData();
        datas.getMeterBaseInfo()[0].CustomerSerialNo = tmpData;
        datas.getMeterBaseInfo()[1].CustomerSerialNo = tmpData;
        datas.getMeterBaseInfo()[2].CustomerSerialNo = tmpData;
        datas.getMeterBaseInfo()[0].SerialNo = tmpData;
        if (this.chk_Meter2Enable.isChecked()) {
            datas.getMeterBaseInfo()[1].SerialNo = tmpData;
        }
        if (this.chk_Meter3Enable.isChecked()) {
            datas.getMeterBaseInfo()[2].SerialNo = tmpData;
        }
        if (!OtherUtils.isEmpty(this.Meter1SerialNoTextView)) {
            datas.getMeterBaseInfo()[0].SerialNo = this.Meter1SerialNoTextView.getText().toString();
        }
        if (!OtherUtils.isEmpty(this.Meter2SerialNoTextView)) {
            datas.getMeterBaseInfo()[1].SerialNo = this.Meter2SerialNoTextView.getText().toString();
        }
        if (!OtherUtils.isEmpty(this.Meter3SerialNoTextView)) {
            datas.getMeterBaseInfo()[2].SerialNo = this.Meter3SerialNoTextView.getText().toString();
        }
    }

    private void scanMeterSr(final int index) {
        CaptureDialog tmpDialog = new CaptureDialog(this.m_Context);
        tmpDialog.setScanResult(new CaptureDialog.ScanResult() {
            /* class com.clou.rs350.ui.dialog.SaveErrorDialog.AnonymousClass3 */

            @Override // google.zxing.camera.CaptureDialog.ScanResult
            public void getScanResult(String result) {
                SaveErrorDialog.this.MeterSerialNoTextView[index].setText(result);
            }
        });
        tmpDialog.show();
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_save:
                save();
                return;
            case R.id.btn_select:
                this.m_DoubleClick.click();
                return;
            case R.id.btn_cancel:
                dismiss();
                return;
            case R.id.btn_scan:
                CaptureDialog tmpDialog = new CaptureDialog(this.m_Context);
                tmpDialog.setScanResult(new CaptureDialog.ScanResult() {
                    /* class com.clou.rs350.ui.dialog.SaveErrorDialog.AnonymousClass4 */

                    @Override // google.zxing.camera.CaptureDialog.ScanResult
                    public void getScanResult(String result) {
                        SaveErrorDialog.this.CustomerSerialNoTextView.setText(result);
                        SaveErrorDialog.this.parseSiteData();
                    }
                });
                tmpDialog.show();
                return;
            case R.id.btn_customer_detail:
                if (validate(this.CustomerSerialNoTextView, R.string.text_customer_serial_no_is_empty)) {
                    parseData();
                    new CustomerInfoDialog(this.m_Context).show();
                    return;
                }
                return;
            case R.id.txt_temperature:
                NumericDialog numericDialog = new NumericDialog(this.m_Context);
                numericDialog.txtLimit = 5;
                numericDialog.setMax(60.0d);
                numericDialog.setMin(-40.0d);
                numericDialog.showMyDialog(this.TemperatureTextView);
                return;
            case R.id.txt_humidity:
                NumericDialog numericDialog2 = new NumericDialog(this.m_Context);
                numericDialog2.setMax(100.0d);
                numericDialog2.setMin(0.0d);
                numericDialog2.setMinus(false);
                numericDialog2.txtLimit = 4;
                numericDialog2.showMyDialog(this.HumidityTextView);
                return;
            case R.id.btn_scan_0:
                scanMeterSr(0);
                return;
            case R.id.btn_scan_1:
                scanMeterSr(1);
                return;
            case R.id.btn_scan_2:
                scanMeterSr(2);
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: protected */
    @Override // com.clou.rs350.ui.dialog.BaseSaveDialog
    public void save() {
        if (validate(this.CustomerSerialNoTextView, R.string.text_customer_serial_no_is_empty)) {
            this.m_LoadingDialog = new LoadingDialog(this.m_Context);
            this.m_LoadingDialog.setCancelable(false);
            this.m_LoadingDialog.show();
            new MyTask(new ILoadCallback() {
                /* class com.clou.rs350.ui.dialog.SaveErrorDialog.AnonymousClass5 */

                @Override // com.clou.rs350.callback.ILoadCallback
                public Object run() {
                    int Result = 1;
                    String Time = new SimpleDateFormat(Constants.TIMEFORMAT).format(new Date(System.currentTimeMillis())).toString();
                    SaveErrorDialog.this.parseData();
                    TestItem tmpTestItem = ClouData.getInstance().getTestItem();
                    String[] SerialNos = new String[3];
                    for (int i = 0; i < 3; i++) {
                        if (SaveErrorDialog.this.chk_MeterEnables[i].isChecked()) {
                            SerialNos[i] = ClouData.getInstance().getMeterBaseInfo()[i].SerialNo;
                            tmpTestItem.MeterSerialNo[i] = SerialNos[i];
                        }
                    }
                    if (SaveErrorDialog.this.m_SaveData != null && SaveErrorDialog.this.m_SaveData.SaveMeasureData(SerialNos, Time)) {
                        Result = 0;
                        CustomerInfoDao tmpDao = new CustomerInfoDao(SaveErrorDialog.this.m_Context);
                        if (!tmpDao.queryIfExistByKey(ClouData.getInstance().getCustomerInfo().CustomerSr)) {
                            tmpDao.insertObject(ClouData.getInstance().getCustomerInfo());
                        }
                        new TestItemDao(SaveErrorDialog.this.m_Context).insertObject(tmpTestItem, Time);
                        SiteData tmpSiteData = ClouData.getInstance().getSiteData();
                        tmpSiteData.CustomerSr = tmpTestItem.CustomerSerialNo;
                        new SiteDataTestDao(SaveErrorDialog.this.m_Context).insertObject(tmpSiteData, Time);
                    }
                    SaveErrorDialog.this.Export(ClouData.getInstance().getCustomerInfo().CustomerSr, Time);
                    return Integer.valueOf(Result);
                }

                @Override // com.clou.rs350.callback.ILoadCallback
                public void callback(Object result) {
                    SaveErrorDialog.this.m_LoadingDialog.dismiss();
                    if (((Number) result).intValue() == 0) {
                        SaveErrorDialog.this.SaveBtn.setVisibility(4);
                        SaveErrorDialog.this.isSave = true;
                        SaveErrorDialog.this.CancelBtn.setText(R.string.text_close);
                        new HintDialog(SaveErrorDialog.this.m_Context, (int) R.string.text_save_success).show();
                        if (SaveErrorDialog.this.m_AfterSave != null) {
                            SaveErrorDialog.this.m_AfterSave.AfterSave(true);
                            return;
                        }
                        return;
                    }
                    new HintDialog(SaveErrorDialog.this.m_Context, (int) R.string.text_save_fail).show();
                }
            }).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, new Integer[0]);
        }
    }

    @Override // com.clou.rs350.ui.dialog.BaseSaveDialog
    public void dismiss() {
        parseData();
        if (this.isSave) {
            ClouData.getInstance().getTestItem().Remark = PdfObject.NOTHING;
        }
        InfoUtil.setRatio(ClouData.getInstance().getSiteData(), this.m_Context);
        super.dismiss();
    }
}
