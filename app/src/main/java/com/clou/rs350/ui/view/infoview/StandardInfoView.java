package com.clou.rs350.ui.view.infoview;

import android.content.Context;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.clou.rs350.R;
import com.clou.rs350.db.model.StandardInfo;
import com.clou.rs350.manager.MessageManager;
import com.clou.rs350.model.ClouData;
import com.clou.rs350.ui.dialog.HintDialog;
import com.clou.rs350.ui.popwindow.DateAndTimeSettingPopWindow;
import com.clou.rs350.ui.popwindow.StandardInfoPopWindow;
import com.clou.rs350.utils.OtherUtils;

import java.util.Date;

public class StandardInfoView implements View.OnClickListener {
    Button btn_Cancel;
    Button btn_Modify;
    int id_Cancel;
    boolean isModify = false;
    int m_Authority = 0;
    private View.OnClickListener m_CancelClick = null;
    Context m_Context;
    StandardInfo m_Info;
    TextView txt_Calibrated_By;
    TextView txt_Calibration_Date;
    TextView txt_Calibration_Valid_Date;
    TextView txt_Certificate_ID;
    TextView txt_StandardSr;

    public StandardInfoView(Context context, View view) {
        this.m_Context = context;
        this.m_Authority = ClouData.getInstance().getOperatorInfo().Authority;
        this.txt_StandardSr = (TextView) view.findViewById(R.id.txt_standard_serial_no);
        this.txt_StandardSr.setOnClickListener(this);
        this.txt_Calibration_Date = (TextView) view.findViewById(R.id.txt_calibrate_date);
        this.txt_Calibration_Date.setOnClickListener(this);
        this.txt_Calibration_Valid_Date = (TextView) view.findViewById(R.id.txt_calibrate_valid_date);
        this.txt_Calibration_Valid_Date.setOnClickListener(this);
        this.txt_Certificate_ID = (TextView) view.findViewById(R.id.txt_certificate_id);
        this.txt_Calibrated_By = (TextView) view.findViewById(R.id.txt_calibrated_by);
        this.btn_Modify = (Button) view.findViewById(R.id.btn_modify);
        this.btn_Modify.setOnClickListener(this);
        this.btn_Cancel = (Button) view.findViewById(R.id.btn_cancel);
        this.btn_Cancel.setOnClickListener(this);
        showData();
    }

    public void setOnClick(int id, View.OnClickListener cancleClick) {
        if (cancleClick != null) {
            this.m_CancelClick = cancleClick;
            this.id_Cancel = id;
            this.btn_Cancel.setEnabled(true);
        }
        setButton(false);
    }

    public void showData() {
        if (!this.isModify) {
            this.m_Info = ClouData.getInstance().getStandardInfo();
            this.txt_StandardSr.setText(this.m_Info.StandardSerialNo);
            this.txt_Calibration_Date.setText(this.m_Info.getCalibrateDate());
            if (this.m_Info.CalibrateDate != null) {
                this.txt_Calibration_Date.setTag(Long.valueOf(this.m_Info.CalibrateDate.getTime()));
            }
            this.txt_Calibration_Valid_Date.setText(this.m_Info.getCalib_Valid_Date());
            if (this.m_Info.Calib_Valid_Date != null) {
                this.txt_Calibration_Valid_Date.setTag(Long.valueOf(this.m_Info.Calib_Valid_Date.getTime()));
            }
            this.txt_StandardSr.setText(this.m_Info.StandardSerialNo);
            this.txt_StandardSr.setTag(this.m_Info);
            this.txt_Certificate_ID.setText(this.m_Info.CertificateID);
            this.txt_Calibrated_By.setText(this.m_Info.Calibrated_By);
            setButton(false);
        }
    }

    private void setButton(boolean flag) {
        this.isModify = flag;
        this.txt_Calibration_Date.setEnabled(flag);
        this.txt_Calibration_Valid_Date.setEnabled(flag);
        this.txt_Certificate_ID.setEnabled(flag);
        this.txt_Calibrated_By.setEnabled(flag);
        this.btn_Modify.setText(flag ? R.string.text_save : R.string.text_modify);
        if (this.m_CancelClick != null) {
            this.btn_Cancel.setText(this.id_Cancel);
        } else {
            this.btn_Cancel.setEnabled(flag);
        }
        if (101 == this.m_Authority) {
            this.txt_StandardSr.setEnabled(flag);
        }
    }

    private void parseData() {
    }

    public void onClick(View v) {
        boolean z = false;
        switch (v.getId()) {
            case R.id.btn_cancel:
                setButton(false);
                if (!this.isModify && this.m_CancelClick != null) {
                    this.m_CancelClick.onClick(v);
                }
                showData();
                return;
            case R.id.btn_modify:
                if (!MessageManager.getInstance().isConnected()) {
                    new HintDialog(this.m_Context, (int) R.string.text_you_need_connect_device).show();
                    return;
                } else if (!OtherUtils.isEmpty(this.txt_StandardSr) || 101 == this.m_Authority) {
                    if (this.isModify) {
                        this.m_Info.StandardSerialNo = this.txt_StandardSr.getText().toString();
                        StandardInfo tmpStandardInfo = (StandardInfo) this.txt_StandardSr.getTag();
                        this.m_Info.MainBoardVersion = tmpStandardInfo.MainBoardVersion;
                        this.m_Info.MeasureBoardVersion = tmpStandardInfo.MeasureBoardVersion;
                        this.m_Info.NeutralBoardVersion = tmpStandardInfo.NeutralBoardVersion;
                        this.m_Info.CalibrateDate = new Date(OtherUtils.convertToLong(this.txt_Calibration_Date.getTag()));
                        this.m_Info.Calib_Valid_Date = new Date(OtherUtils.convertToLong(this.txt_Calibration_Valid_Date.getTag()));
                        this.m_Info.CertificateID = this.txt_Certificate_ID.getText().toString();
                        this.m_Info.Calibrated_By = this.txt_Calibrated_By.getText().toString();
                        MessageManager.getInstance().SetStandardInfo(this.m_Info);
                    }
                    if (!this.isModify) {
                        z = true;
                    }
                    setButton(z);
                    return;
                } else {
                    new HintDialog(this.m_Context, (int) R.string.text_no_standard_sr).show();
                    return;
                }
            case R.id.txt_standard_serial_no:
                new StandardInfoPopWindow(this.m_Context).showPopWindow(v, (StandardInfo) this.txt_StandardSr.getTag(), new View.OnClickListener() {
                    /* class com.clou.rs350.ui.view.infoview.StandardInfoView.AnonymousClass3 */

                    public void onClick(View v) {
                        Object tag = v.getTag();
                        StandardInfoView.this.txt_StandardSr.setText(((StandardInfo) tag).StandardSerialNo);
                        StandardInfoView.this.txt_StandardSr.setTag(tag);
                    }
                });
                return;
            case R.id.txt_calibrate_date:
                DateAndTimeSettingPopWindow popWindow = new DateAndTimeSettingPopWindow(this.m_Context);
                popWindow.setType(1);
                popWindow.showPopWindow(v, new View.OnClickListener() {
                    /* class com.clou.rs350.ui.view.infoview.StandardInfoView.AnonymousClass1 */

                    public void onClick(View v) {
                        StandardInfoView.this.txt_Calibration_Date.setTag(v.getTag());
                    }
                });
                return;
            case R.id.txt_calibrate_valid_date:
                DateAndTimeSettingPopWindow popWindow2 = new DateAndTimeSettingPopWindow(this.m_Context);
                popWindow2.setType(1);
                popWindow2.showPopWindow(v, new View.OnClickListener() {
                    /* class com.clou.rs350.ui.view.infoview.StandardInfoView.AnonymousClass2 */

                    public void onClick(View v) {
                        StandardInfoView.this.txt_Calibration_Valid_Date.setTag(v.getTag());
                    }
                });
                return;
            default:
                return;
        }
    }
}
