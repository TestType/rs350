package com.clou.rs350.ui.fragment.recordfragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.clou.rs350.R;
import com.clou.rs350.db.model.DailyTest;
import com.clou.rs350.ui.adapter.DailyAdapter;
import com.clou.rs350.ui.fragment.BaseFragment;
import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.view.annotation.ViewInject;

public class DailyTestingRecordFragment extends BaseFragment {
    @ViewInject(R.id.list_errors)
    private ListView lstError;
    private DailyTest m_ErrorTest;
    @ViewInject(R.id.txt_constant)
    private TextView txt_Constant;
    @ViewInject(R.id.txt_eavg)
    private TextView txt_Eavg;
    @ViewInject(R.id.txt_es)
    private TextView txt_Es;
    @ViewInject(R.id.txt_meter_sr)
    private TextView txt_Meter_Sr;
    @ViewInject(R.id.txt_pulses)
    private TextView txt_Pulse;

    public DailyTestingRecordFragment(DailyTest data) {
        this.m_ErrorTest = data;
    }

    @Override // android.support.v4.app.Fragment
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        this.fragmentView = inflater.inflate(R.layout.fragment_record_daily_testing, (ViewGroup) null);
        ViewUtils.inject(this, this.fragmentView);
        return this.fragmentView;
    }

    @Override // android.support.v4.app.Fragment, com.clou.rs350.ui.fragment.BaseFragment
    public void onResume() {
        super.onResume();
        showData();
    }

    @Override // android.support.v4.app.Fragment, com.clou.rs350.ui.fragment.BaseFragment
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initView();
    }

    private void initView() {
    }

    private void showData() {
        this.txt_Meter_Sr.setText(this.m_ErrorTest.MeterSerialNo);
        this.txt_Constant.setText(String.valueOf(this.m_ErrorTest.MeterConstant) + this.m_ErrorTest.ConstantUnit);
        this.txt_Pulse.setText(this.m_ErrorTest.Pulses);
        this.txt_Eavg.setText(this.m_ErrorTest.ErrorAverage);
        this.txt_Es.setText(this.m_ErrorTest.ErrorStandard);
        this.lstError.setAdapter((ListAdapter) new DailyAdapter(this.m_ErrorTest, this.m_Context));
    }

    @Override // android.support.v4.app.Fragment, com.clou.rs350.ui.fragment.BaseFragment
    public void onPause() {
        super.onPause();
    }
}
