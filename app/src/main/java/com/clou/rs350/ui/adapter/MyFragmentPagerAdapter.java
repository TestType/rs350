package com.clou.rs350.ui.adapter;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import java.util.ArrayList;

class MyFragmentPagerAdapter extends FragmentStatePagerAdapter {
    private ArrayList<Fragment> fragmentsList;

    public MyFragmentPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    public MyFragmentPagerAdapter(FragmentManager fm, ArrayList<Fragment> fragments) {
        super(fm);
        this.fragmentsList = fragments;
    }

    @Override // android.support.v4.view.PagerAdapter
    public int getCount() {
        return this.fragmentsList.size();
    }

    @Override // android.support.v4.app.FragmentStatePagerAdapter
    public Fragment getItem(int arg0) {
        return this.fragmentsList.get(arg0);
    }

    @Override // android.support.v4.view.PagerAdapter
    public int getItemPosition(Object object) {
        return super.getItemPosition(object);
    }
}
