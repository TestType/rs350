package com.clou.rs350.ui.view.control;

import android.content.Context;
import android.graphics.Color;
import android.util.AttributeSet;
import android.widget.RelativeLayout;
import com.clou.rs350.Preferences;

public class StatusRelativeLayout extends RelativeLayout {
    public StatusRelativeLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    /* access modifiers changed from: protected */
    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        int statuColor = Preferences.getInt("statuColor", Color.parseColor("#F97A22"));
        if (statuColor > 0) {
            setBackgroundColor(statuColor);
        }
        ViewManager.getInstance().addControlView(1, this);
    }

    /* access modifiers changed from: protected */
    public void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        ViewManager.getInstance().removeControlView(1, this);
    }
}
