package com.clou.rs350.ui.fragment.recordfragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;

import com.clou.rs350.Preferences;
import com.clou.rs350.R;
import com.clou.rs350.db.model.WaveMeasurement;
import com.clou.rs350.handler.HandlerSwitchView;
import com.clou.rs350.ui.fragment.BaseFragment;
import com.clou.rs350.ui.view.MyWaveView;
import com.clou.rs350.utils.OtherUtils;
import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.view.annotation.ViewInject;
import com.lidroid.xutils.view.annotation.event.OnClick;

import java.util.ArrayList;
import java.util.List;

public class WaveRecordFragment extends BaseFragment implements CompoundButton.OnCheckedChangeListener, View.OnClickListener, HandlerSwitchView.IOnSelectCallback {
    private static final String TAG = "WaveFragment";
    private CheckBox I1Cbox;
    private CheckBox I2Cbox;
    private CheckBox I3Cbox;
    private CheckBox IL1Cbox;
    private CheckBox IL2Cbox;
    private CheckBox IL3Cbox;
    private CheckBox U1Cbox;
    private CheckBox U2Cbox;
    private CheckBox U3Cbox;
    private CheckBox UL1Cbox;
    private CheckBox UL2Cbox;
    private CheckBox UL3Cbox;
    boolean flag = true;
    int m_Style = 0;
    WaveMeasurement m_WaveMeasurement;
    private int readFlag = 0;
    private HandlerSwitchView switchView;
    @ViewInject(R.id.realtime_wave_view)
    private MyWaveView waveView;
    @ViewInject(R.id.realtime_wave_view_table1)
    private MyWaveView waveViewTable1;
    @ViewInject(R.id.realtime_wave_view_table2)
    private MyWaveView waveViewTable2;
    @ViewInject(R.id.realtime_wave_view_table3)
    private MyWaveView waveViewTable3;

    public WaveRecordFragment(WaveMeasurement data) {
        this.m_WaveMeasurement = data;
    }

    @Override // android.support.v4.app.Fragment
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        this.fragmentView = inflater.inflate(R.layout.fragment_record_wave, (ViewGroup) null);
        ViewUtils.inject(this, this.fragmentView);
        return this.fragmentView;
    }

    @Override // android.support.v4.app.Fragment, com.clou.rs350.ui.fragment.BaseFragment
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initView();
    }

    private void setStyle() {
        switch (this.m_Style) {
            case 1:
                findViewById(R.id.realtime_waveconent_view).setVisibility(8);
                findViewById(R.id.realtime_waveconent_newtable_view).setVisibility(0);
                return;
            default:
                findViewById(R.id.realtime_waveconent_view).setVisibility(0);
                findViewById(R.id.realtime_waveconent_newtable_view).setVisibility(8);
                this.m_Style = 0;
                return;
        }
    }

    @Override // android.support.v4.app.Fragment, com.clou.rs350.ui.fragment.BaseFragment
    public void onResume() {
        super.onResume();
        this.m_Style = Preferences.getInt(Preferences.Key.REALWAVESTYLE, 0);
        setStyle();
        initData();
    }

    private void initData() {
        ShowData();
    }

    private void initView() {
        this.U1Cbox = (CheckBox) findViewById(R.id.cb_ul1);
        this.U1Cbox.setOnCheckedChangeListener(this);
        this.I1Cbox = (CheckBox) findViewById(R.id.cb_il1);
        this.I1Cbox.setOnCheckedChangeListener(this);
        this.U2Cbox = (CheckBox) findViewById(R.id.cb_ul2);
        this.U2Cbox.setOnCheckedChangeListener(this);
        this.I2Cbox = (CheckBox) findViewById(R.id.cb_il2);
        this.I2Cbox.setOnCheckedChangeListener(this);
        this.U3Cbox = (CheckBox) findViewById(R.id.cb_ul3);
        this.U3Cbox.setOnCheckedChangeListener(this);
        this.I3Cbox = (CheckBox) findViewById(R.id.cb_il3);
        this.I3Cbox.setOnCheckedChangeListener(this);
        this.UL1Cbox = (CheckBox) findViewById(R.id.cb_ul1_right);
        this.UL1Cbox.setOnCheckedChangeListener(this);
        this.IL1Cbox = (CheckBox) findViewById(R.id.cb_il1_right);
        this.IL1Cbox.setOnCheckedChangeListener(this);
        this.UL2Cbox = (CheckBox) findViewById(R.id.cb_ul2_right);
        this.UL2Cbox.setOnCheckedChangeListener(this);
        this.IL2Cbox = (CheckBox) findViewById(R.id.cb_il2_right);
        this.IL2Cbox.setOnCheckedChangeListener(this);
        this.UL3Cbox = (CheckBox) findViewById(R.id.cb_ul3_right);
        this.UL3Cbox.setOnCheckedChangeListener(this);
        this.IL3Cbox = (CheckBox) findViewById(R.id.cb_il3_right);
        this.IL3Cbox.setOnCheckedChangeListener(this);
        this.waveView.showLines(this.U1Cbox.isChecked(), this.I1Cbox.isChecked(), this.U2Cbox.isChecked(), this.I2Cbox.isChecked(), this.U3Cbox.isChecked(), this.I3Cbox.isChecked());
        this.waveView.setLayerType(1, null);
        this.waveViewTable1.showLines(this.UL1Cbox.isChecked(), this.IL1Cbox.isChecked(), false, false, false, false);
        this.waveViewTable1.setLayerType(1, null);
        this.waveViewTable2.showLines(false, false, this.UL2Cbox.isChecked(), this.IL2Cbox.isChecked(), false, false);
        this.waveViewTable2.setLayerType(1, null);
        this.waveViewTable3.showLines(false, false, false, false, this.UL3Cbox.isChecked(), this.IL3Cbox.isChecked());
        this.waveViewTable3.setLayerType(1, null);
        List<View> tabViews = new ArrayList<>();
        tabViews.add(findViewById(R.id.btn_ui));
        tabViews.add(findViewById(R.id.btn_power));
        this.switchView = new HandlerSwitchView(this.m_Context);
        this.switchView.setTabViews(tabViews);
        this.switchView.boundClick();
        this.switchView.setOnTabChangedCallback(this);
        this.switchView.selectView(0);
    }

    public void onCheckedChanged(CompoundButton v, boolean isChecked) {
        this.waveView.showLines(this.U1Cbox.isChecked(), this.I1Cbox.isChecked(), this.U2Cbox.isChecked(), this.I2Cbox.isChecked(), this.U3Cbox.isChecked(), this.I3Cbox.isChecked());
        this.waveViewTable1.showLines(this.UL1Cbox.isChecked(), this.IL1Cbox.isChecked(), false, false, false, false);
        this.waveViewTable2.showLines(false, false, this.UL2Cbox.isChecked(), this.IL2Cbox.isChecked(), false, false);
        this.waveViewTable3.showLines(false, false, false, false, this.UL3Cbox.isChecked(), this.IL3Cbox.isChecked());
        switch (v.getId()) {
            case R.id.cb_ul1:
            case R.id.cb_ul1_right:
                this.U1Cbox.setChecked(isChecked);
                this.UL1Cbox.setChecked(isChecked);
                return;
            case R.id.cb_il1:
            case R.id.cb_il1_right:
                this.I1Cbox.setChecked(isChecked);
                this.IL1Cbox.setChecked(isChecked);
                return;
            case R.id.cb_ul2:
            case R.id.cb_ul2_right:
                this.U2Cbox.setChecked(isChecked);
                this.UL2Cbox.setChecked(isChecked);
                return;
            case R.id.cb_il2:
            case R.id.cb_il2_right:
                this.I2Cbox.setChecked(isChecked);
                this.IL2Cbox.setChecked(isChecked);
                return;
            case R.id.cb_ul3:
            case R.id.cb_ul3_right:
                this.U3Cbox.setChecked(isChecked);
                this.UL3Cbox.setChecked(isChecked);
                return;
            case R.id.cb_il3:
            case R.id.cb_il3_right:
                this.I3Cbox.setChecked(isChecked);
                this.IL3Cbox.setChecked(isChecked);
                return;
            case R.id.realtime_waveconent_newtable_view:
            case R.id.realtime_waveconent_newtableleft_view:
            case R.id.realtime_wave_view_table1:
            case R.id.realtime_wave_view_table2:
            case R.id.realtime_wave_view_table3:
            default:
                return;
        }
    }

    private void setButtonEnable(boolean flag2) {
        this.I2Cbox.setChecked(flag2);
        this.U2Cbox.setChecked(flag2);
        this.I2Cbox.setEnabled(flag2);
        this.U2Cbox.setEnabled(flag2);
        this.IL2Cbox.setChecked(flag2);
        this.UL2Cbox.setChecked(flag2);
        this.IL2Cbox.setEnabled(flag2);
        this.UL2Cbox.setEnabled(flag2);
    }

    private void ShowData() {
        setButtonEnable(!OtherUtils.is3P(this.m_WaveMeasurement.Wiring));
        switch (this.readFlag) {
            case 1:
                this.waveView.refreshData(1, this.m_WaveMeasurement.wavePa, this.m_WaveMeasurement.waveQa, this.m_WaveMeasurement.wavePb, this.m_WaveMeasurement.waveQb, this.m_WaveMeasurement.wavePc, this.m_WaveMeasurement.waveQc);
                this.waveViewTable1.refreshData(1, this.m_WaveMeasurement.wavePa, this.m_WaveMeasurement.waveQa, null, null, null, null);
                this.waveViewTable2.refreshData(1, null, null, this.m_WaveMeasurement.wavePb, this.m_WaveMeasurement.waveQb, null, null);
                this.waveViewTable3.refreshData(1, null, null, null, null, this.m_WaveMeasurement.wavePc, this.m_WaveMeasurement.waveQc);
                return;
            default:
                this.waveView.refreshData(0, this.m_WaveMeasurement.waveUa, this.m_WaveMeasurement.waveIa, this.m_WaveMeasurement.waveUb, this.m_WaveMeasurement.waveIb, this.m_WaveMeasurement.waveUc, this.m_WaveMeasurement.waveIc);
                this.waveViewTable1.refreshData(0, this.m_WaveMeasurement.waveUa, this.m_WaveMeasurement.waveIa, null, null, null, null);
                this.waveViewTable2.refreshData(0, null, null, this.m_WaveMeasurement.waveUb, this.m_WaveMeasurement.waveIb, null, null);
                this.waveViewTable3.refreshData(0, null, null, null, null, this.m_WaveMeasurement.waveUc, this.m_WaveMeasurement.waveIc);
                return;
        }
    }

    @Override // android.support.v4.app.Fragment, com.clou.rs350.ui.fragment.BaseFragment
    public void onPause() {
        super.onPause();
        Preferences.putInt(Preferences.Key.REALWAVESTYLE, this.m_Style);
    }

    @OnClick({R.id.btn_style, R.id.btn_hold})
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_style:
                this.m_Style++;
                setStyle();
                ShowData();
                return;
            default:
                return;
        }
    }

    @Override // com.clou.rs350.handler.HandlerSwitchView.IOnSelectCallback
    public void onSelectedChanged(int index) {
        this.readFlag = index;
        String[] UP = {getActivity().getResources().getString(R.string.text_u), getActivity().getResources().getString(R.string.text_p)};
        String[] IQ = {getActivity().getResources().getString(R.string.text_i), getActivity().getResources().getString(R.string.text_q)};
        this.U1Cbox.setText(UP[index]);
        this.UL1Cbox.setText(UP[index]);
        this.U2Cbox.setText(UP[index]);
        this.UL2Cbox.setText(UP[index]);
        this.U3Cbox.setText(UP[index]);
        this.UL3Cbox.setText(UP[index]);
        this.I1Cbox.setText(IQ[index]);
        this.IL1Cbox.setText(IQ[index]);
        this.I2Cbox.setText(IQ[index]);
        this.IL2Cbox.setText(IQ[index]);
        this.I3Cbox.setText(IQ[index]);
        this.IL3Cbox.setText(IQ[index]);
        ShowData();
    }

    @Override // com.clou.rs350.handler.HandlerSwitchView.IOnSelectCallback
    public void onClickView(View view) {
    }
}
