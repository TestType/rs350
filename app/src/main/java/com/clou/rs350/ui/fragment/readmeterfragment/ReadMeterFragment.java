package com.clou.rs350.ui.fragment.readmeterfragment;

import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.clou.rs350.R;
import com.clou.rs350.db.model.ReadMeter;
import com.clou.rs350.device.MeterBaseDevice;
import com.clou.rs350.device.model.Baudrate;
import com.clou.rs350.electricmeter.DLT645_1997;
import com.clou.rs350.electricmeter.DLT645_2007;
import com.clou.rs350.electricmeter.IElectricMeter;
import com.clou.rs350.handler.HandlerSwitchView;
import com.clou.rs350.task.ISleepCallback;
import com.clou.rs350.task.SleepTask;
import com.clou.rs350.ui.dialog.HintDialog;
import com.clou.rs350.ui.dialog.LoadingDialog;
import com.clou.rs350.ui.fragment.BaseFragment;
import com.clou.rs350.ui.popwindow.ListPopWindow;
import com.clou.rs350.ui.popwindow.SetBaudratePopWindow;
import com.clou.rs350.ui.popwindow.TimeSettingPopWindow;
import com.clou.rs350.utils.OtherUtils;
import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.view.annotation.ViewInject;
import com.lidroid.xutils.view.annotation.event.OnClick;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class ReadMeterFragment extends BaseFragment implements View.OnClickListener, HandlerSwitchView.IOnSelectCallback {
    private boolean isAuto;
    private Baudrate m_AutoBaudrate;
    private int m_BufferIndex;
    List<byte[]> m_Buffers;
    ReadMeter m_Data;
    private LoadingDialog m_LoadingDialog;
    private SleepTask m_LoadingTask;
    private IElectricMeter m_Meter = new DLT645_2007();
    int m_ReadStep = 0;
    private int m_ResendCount;
    String[] sProtocol = {"DLT645-2007", "DLT645-1997"};
    private HandlerSwitchView switchView;
    @ViewInject(R.id.txt_address)
    TextView txt_Address;
    @ViewInject(R.id.txt_baudrate)
    TextView txt_Baudrate;
    TextView[] txt_Energys;
    @ViewInject(R.id.txt_flat_export_active)
    TextView txt_Flat_Export_Active_Energy;
    @ViewInject(R.id.txt_flat_export_reactive)
    TextView txt_Flat_Export_Reactive_Energy;
    @ViewInject(R.id.txt_flat_import_active)
    TextView txt_Flat_Import_Active_Energy;
    @ViewInject(R.id.txt_flat_import_reactive)
    TextView txt_Flat_Import_Reactive_Energy;
    @ViewInject(R.id.txt_il1)
    TextView txt_IL1;
    @ViewInject(R.id.txt_il2)
    TextView txt_IL2;
    @ViewInject(R.id.txt_il3)
    TextView txt_IL3;
    @ViewInject(R.id.txt_meter_index)
    TextView txt_MeterIndex;
    @ViewInject(R.id.txt_peak_export_active)
    TextView txt_Peak_Export_Active_Energy;
    @ViewInject(R.id.txt_peak_export_reactive)
    TextView txt_Peak_Export_Reactive_Energy;
    @ViewInject(R.id.txt_peak_import_active)
    TextView txt_Peak_Import_Active_Energy;
    @ViewInject(R.id.txt_peak_import_reactive)
    TextView txt_Peak_Import_Reactive_Energy;
    @ViewInject(R.id.txt_protocol)
    TextView txt_Protocol;
    @ViewInject(R.id.txt_psum)
    TextView txt_Psum;
    @ViewInject(R.id.txt_qsum)
    TextView txt_Qsum;
    @ViewInject(R.id.txt_standard_time)
    TextView txt_Standard_Time;
    @ViewInject(R.id.txt_time)
    TextView txt_Time;
    @ViewInject(R.id.txt_time_error)
    TextView txt_Time_Error;
    @ViewInject(R.id.txt_tip_export_active)
    TextView txt_Tip_Export_Active_Energy;
    @ViewInject(R.id.txt_tip_export_reactive)
    TextView txt_Tip_Export_Reactive_Energy;
    @ViewInject(R.id.txt_tip_import_active)
    TextView txt_Tip_Import_Active_Energy;
    @ViewInject(R.id.txt_tip_import_reactive)
    TextView txt_Tip_Import_Reactive_Energy;
    @ViewInject(R.id.txt_total_export_active)
    TextView txt_Total_Export_Active_Energy;
    @ViewInject(R.id.txt_total_export_reactive)
    TextView txt_Total_Export_Reactive_Energy;
    @ViewInject(R.id.txt_total_import_active)
    TextView txt_Total_Import_Active_Energy;
    @ViewInject(R.id.txt_total_import_reactive)
    TextView txt_Total_Import_Reactive_Energy;
    @ViewInject(R.id.txt_ul1)
    TextView txt_UL1;
    @ViewInject(R.id.txt_ul2)
    TextView txt_UL2;
    @ViewInject(R.id.txt_ul3)
    TextView txt_UL3;
    @ViewInject(R.id.txt_valley_export_active)
    TextView txt_Valley_Export_Active_Energy;
    @ViewInject(R.id.txt_valley_export_reactive)
    TextView txt_Valley_Export_Reactive_Energy;
    @ViewInject(R.id.txt_valley_import_active)
    TextView txt_Valley_Import_Active_Energy;
    @ViewInject(R.id.txt_valley_import_reactive)
    TextView txt_Valley_Import_Reactive_Energy;

    public ReadMeterFragment(ReadMeter data) {
        this.m_Data = data;
    }

    @Override // android.support.v4.app.Fragment
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        this.fragmentView = inflater.inflate(R.layout.fragment_read_meter, (ViewGroup) null);
        ViewUtils.inject(this, this.fragmentView);
        initView();
        return this.fragmentView;
    }

    private void initView() {
        this.txt_Energys = new TextView[]{this.txt_Total_Import_Active_Energy, this.txt_Tip_Import_Active_Energy, this.txt_Peak_Import_Active_Energy, this.txt_Flat_Import_Active_Energy, this.txt_Valley_Import_Active_Energy, this.txt_Total_Export_Active_Energy, this.txt_Tip_Export_Active_Energy, this.txt_Peak_Export_Active_Energy, this.txt_Flat_Export_Active_Energy, this.txt_Valley_Export_Active_Energy, this.txt_Total_Import_Reactive_Energy, this.txt_Tip_Import_Reactive_Energy, this.txt_Peak_Import_Reactive_Energy, this.txt_Flat_Import_Reactive_Energy, this.txt_Valley_Import_Reactive_Energy, this.txt_Total_Export_Reactive_Energy, this.txt_Tip_Export_Reactive_Energy, this.txt_Peak_Export_Reactive_Energy, this.txt_Flat_Export_Reactive_Energy, this.txt_Valley_Export_Reactive_Energy};
        List<View> tabViews = new ArrayList<>();
        tabViews.add(findViewById(R.id.btn_basic));
        tabViews.add(findViewById(R.id.btn_more));
        this.switchView = new HandlerSwitchView(this.m_Context);
        this.switchView.setTabViews(tabViews);
        this.switchView.boundClick();
        this.switchView.setOnTabChangedCallback(this);
        this.switchView.selectView(0);
    }

    @Override // android.support.v4.app.Fragment, com.clou.rs350.ui.fragment.BaseFragment
    public void onResume() {
        super.onResume();
        initData();
        refreshData();
        setData();
        showData();
    }

    @Override // com.clou.rs350.ui.fragment.BaseFragment
    public void refreshData() {
    }

    /* access modifiers changed from: protected */
    @Override // com.clou.rs350.ui.fragment.BaseFragment
    public void copyData() {
    }

    @Override // android.support.v4.app.Fragment, com.clou.rs350.ui.fragment.BaseFragment
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initView();
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private void initMeter(int index) {
        switch (index) {
            case 1:
                this.m_Meter = new DLT645_1997();
                return;
            default:
                this.m_Meter = new DLT645_2007();
                return;
        }
    }

    @OnClick({R.id.btn_read, R.id.txt_baudrate, R.id.txt_protocol, R.id.txt_meter_index, R.id.txt_standard_time, R.id.btn_test})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_read:
                ReadClick();
                return;
            case R.id.txt_baudrate:
                new SetBaudratePopWindow(this.m_Context).showPopWindow(view, this.m_Data.bBaudrate, new View.OnClickListener() {
                    /* class com.clou.rs350.ui.fragment.readmeterfragment.ReadMeterFragment.AnonymousClass2 */

                    public void onClick(View v) {
                        ReadMeterFragment.this.m_Data.bBaudrate = (Baudrate) v.getTag();
                        if (-1 != ReadMeterFragment.this.m_Data.bBaudrate.baudrate.l_Value) {
                            Baudrate tmpBaudrate = ReadMeterFragment.this.m_Data.bBaudrate;
                            ReadMeterFragment.this.messageManager.setBaudrate(1, (int) tmpBaudrate.baudrate.l_Value, (int) tmpBaudrate.dataBits.l_Value, (int) tmpBaudrate.parityBits.l_Value, (int) tmpBaudrate.stopBits.l_Value);
                        }
                        ReadMeterFragment.this.setData();
                    }
                });
                return;
            case R.id.btn_test:
            default:
                return;
            case R.id.txt_meter_index:
                ListPopWindow.showListPopwindow(this.m_Context, new String[]{"1", "2", "3"}, view, new View.OnClickListener() {
                    /* class com.clou.rs350.ui.fragment.readmeterfragment.ReadMeterFragment.AnonymousClass1 */

                    public void onClick(View v) {
                        ReadMeterFragment.this.m_Data.MeterIndex = v.getTag().toString();
                    }
                }, 0, true, null);
                return;
            case R.id.txt_protocol:
                ListPopWindow.showListPopwindow(this.m_Context, this.sProtocol, view, new View.OnClickListener() {
                    /* class com.clou.rs350.ui.fragment.readmeterfragment.ReadMeterFragment.AnonymousClass3 */

                    public void onClick(View v) {
                        ReadMeterFragment.this.m_Data.Prorocol = OtherUtils.convertToInt(v.getTag());
                        ReadMeterFragment.this.initMeter(ReadMeterFragment.this.m_Data.Prorocol);
                    }
                }, 0, true, null);
                return;
            case R.id.txt_standard_time:
                new TimeSettingPopWindow(this.m_Context).showPopWindow(view, null, new View.OnClickListener() {
                    /* class com.clou.rs350.ui.fragment.readmeterfragment.ReadMeterFragment.AnonymousClass4 */

                    public void onClick(View v) {
                        ReadMeterFragment.this.m_Data.StandardTime = ReadMeterFragment.this.txt_Standard_Time.getText().toString();
                        ReadMeterFragment.this.m_Data.CalTimeError();
                        ReadMeterFragment.this.showData();
                    }
                });
                return;
        }
    }

    private void getReadBuffer() {
        this.m_Buffers.addAll(this.m_Meter.ReadTime());
        this.m_Buffers.addAll(this.m_Meter.ReadBasicData());
    }

    @Override // com.clou.rs350.ui.fragment.BaseFragment, com.clou.rs350.manager.MessageManager.IMessageListener
    public void onReceiveMessage(byte[] buffer) {
        super.onReceiveMessage(buffer);
        switch (this.m_Meter.Unpacket(buffer)) {
            case 1:
                if (this.isAuto) {
                    this.isAuto = false;
                    new SetBaudratePopWindow(this.m_Context).parseBaudrate(this.m_AutoBaudrate);
                    this.m_Data.bBaudrate = this.m_AutoBaudrate;
                    showBaudrate();
                    InitRead();
                    return;
                }
                if (this.m_ReadStep == 0) {
                    this.m_ReadStep = 1;
                    getReadBuffer();
                }
                if (1 == this.m_BufferIndex) {
                    Calendar cal = Calendar.getInstance();
                    this.m_Data.StandardTime = String.valueOf(cal.get(11)) + ":" + String.format("%1$02d", Integer.valueOf(cal.get(12))) + ":" + String.format("%1$02d", Integer.valueOf(cal.get(13)));
                }
                this.m_BufferIndex++;
                if (this.m_BufferIndex < this.m_Buffers.size()) {
                    ResetLoadingTask();
                    ReadMeter();
                } else {
                    this.m_LoadingDialog.dismiss();
                    this.m_LoadingTask.stop();
                    new HintDialog(this.m_Context, (int) R.string.text_read_success);
                }
                parseMeterData();
                this.m_Data.CalTimeError();
                showData();
                return;
            case 2:
            default:
                return;
            case 3:
                ResetLoadingTask();
                this.messageManager.sendBuffer(1, this.m_Meter.ReadContinue());
                return;
        }
    }

    private void parseMeterData() {
        this.m_Data.Address = this.m_Meter.GetAddress();
        this.m_Data.DateAndTime = this.m_Meter.GetDateAndTime();
        this.m_Data.Time = this.m_Meter.GetTime();
        String[] tmpValue = this.m_Meter.getBasicData();
        this.m_Data.U1 = tmpValue[0];
        this.m_Data.U2 = tmpValue[1];
        this.m_Data.U3 = tmpValue[2];
        this.m_Data.I1 = tmpValue[3];
        this.m_Data.I2 = tmpValue[4];
        this.m_Data.I3 = tmpValue[5];
        this.m_Data.Psum = tmpValue[6];
        this.m_Data.Qsum = tmpValue[7];
        for (int i = 0; i < 20; i++) {
            this.m_Data.Energys[i] = tmpValue[i + 8];
        }
    }

    @Override // com.clou.rs350.ui.fragment.BaseFragment, com.clou.rs350.manager.MessageManager.IMessageListener
    public void onReceiveMessage(MeterBaseDevice Device) {
        super.onReceiveMessage(Device);
    }

    private void showBaudrate() {
        if (-1 == this.m_Data.bBaudrate.baudrate.l_Value) {
            this.txt_Baudrate.setText(this.m_Data.bBaudrate.baudrate.s_Value);
        } else {
            this.txt_Baudrate.setText(String.valueOf(this.m_Data.bBaudrate.baudrate.s_Value) + "," + this.m_Data.bBaudrate.parityBits.s_Value + "," + this.m_Data.bBaudrate.dataBits.s_Value + "," + this.m_Data.bBaudrate.stopBits.s_Value);
        }
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private void setData() {
        this.txt_MeterIndex.setText(new StringBuilder(String.valueOf(OtherUtils.parseInt(this.m_Data.MeterIndex) + 1)).toString());
        showBaudrate();
        this.txt_Protocol.setText(this.sProtocol[this.m_Data.Prorocol]);
        initMeter(this.m_Data.Prorocol);
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private void showData() {
        this.txt_Address.setText(this.m_Data.Address);
        this.txt_Time.setText(this.m_Data.DateAndTime);
        this.txt_Standard_Time.setText(this.m_Data.StandardTime);
        this.txt_Time_Error.setText(this.m_Data.TimeError);
        this.txt_UL1.setText(this.m_Data.U1);
        this.txt_UL2.setText(this.m_Data.U2);
        this.txt_UL3.setText(this.m_Data.U3);
        this.txt_IL1.setText(this.m_Data.I1);
        this.txt_IL2.setText(this.m_Data.I2);
        this.txt_IL3.setText(this.m_Data.I3);
        this.txt_Psum.setText(this.m_Data.Psum);
        this.txt_Qsum.setText(this.m_Data.Qsum);
        for (int i = 0; i < 20; i++) {
            this.txt_Energys[i].setText(this.m_Data.Energys[i]);
        }
    }

    @Override // android.support.v4.app.Fragment, com.clou.rs350.ui.fragment.BaseFragment
    public void onPause() {
        this.messageManager.setBaudrate(1, 7, 1, 0, 1);
        super.onPause();
    }

    public void ReadClick() {
        this.m_LoadingDialog = new LoadingDialog(this.m_Context);
        this.m_LoadingDialog.show();
        this.m_ResendCount = 0;
        SetLoadingTask();
        if (-1 != this.m_Data.bBaudrate.baudrate.l_Value) {
            Baudrate tmpBaudrate = this.m_Data.bBaudrate;
            this.messageManager.setBaudrate(1, (int) tmpBaudrate.baudrate.l_Value, (int) tmpBaudrate.dataBits.l_Value, (int) tmpBaudrate.parityBits.l_Value, (int) tmpBaudrate.stopBits.l_Value);
            InitRead();
            return;
        }
        AutoBaudrate();
    }

    private void ResetLoadingTask() {
        this.m_LoadingTask.restart();
        this.m_ResendCount = 0;
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private void SetLoadingTask() {
        this.m_LoadingTask = new SleepTask(1000, new ISleepCallback() {
            /* class com.clou.rs350.ui.fragment.readmeterfragment.ReadMeterFragment.AnonymousClass5 */

            @Override // com.clou.rs350.task.ISleepCallback
            public void onSleepAfterToDo() {
                if (3 > ReadMeterFragment.this.m_ResendCount) {
                    ReadMeterFragment.this.m_ResendCount++;
                    ReadMeterFragment.this.SetLoadingTask();
                    ReadMeterFragment.this.ReadMeter();
                } else if (ReadMeterFragment.this.isAuto) {
                    ReadMeterFragment.this.m_AutoBaudrate.parityBits.l_Value++;
                    ReadMeterFragment.this.m_AutoBaudrate.baudrate.l_Value += ReadMeterFragment.this.m_AutoBaudrate.parityBits.l_Value / 3;
                    ReadMeterFragment.this.m_AutoBaudrate.parityBits.l_Value %= 3;
                    if (ReadMeterFragment.this.m_AutoBaudrate.baudrate.l_Value <= 10) {
                        ReadMeterFragment.this.messageManager.setBaudrate(1, ReadMeterFragment.this.m_AutoBaudrate);
                        new SleepTask(200, new ISleepCallback() {
                            /* class com.clou.rs350.ui.fragment.readmeterfragment.ReadMeterFragment.AnonymousClass5.AnonymousClass1 */

                            @Override // com.clou.rs350.task.ISleepCallback
                            public void onSleepAfterToDo() {
                                ReadMeterFragment.this.ReadMeter();
                            }
                        }).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, new Integer[0]);
                        ReadMeterFragment.this.SetLoadingTask();
                        ReadMeterFragment.this.m_ResendCount = 0;
                        return;
                    }
                    ReadMeterFragment.this.m_LoadingDialog.dismiss();
                    new HintDialog(ReadMeterFragment.this.m_Context, (int) R.string.text_read_failure).show();
                } else {
                    ReadMeterFragment.this.m_LoadingDialog.dismiss();
                    new HintDialog(ReadMeterFragment.this.m_Context, (int) R.string.text_read_failure).show();
                }
            }
        });
        this.m_LoadingTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, new Integer[0]);
    }

    private void AutoBaudrate() {
        this.isAuto = true;
        this.m_Buffers = this.m_Meter.ReadAddress();
        this.m_BufferIndex = 0;
        this.m_AutoBaudrate = new Baudrate();
        this.m_AutoBaudrate.baudrate.l_Value = 0;
        this.messageManager.setBaudrate(1, this.m_AutoBaudrate);
        new SleepTask(200, new ISleepCallback() {
            /* class com.clou.rs350.ui.fragment.readmeterfragment.ReadMeterFragment.AnonymousClass6 */

            @Override // com.clou.rs350.task.ISleepCallback
            public void onSleepAfterToDo() {
                ReadMeterFragment.this.ReadMeter();
            }
        }).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, new Integer[0]);
    }

    private void InitRead() {
        this.m_Buffers = this.m_Meter.ReadAddress();
        this.m_ReadStep = 0;
        this.m_LoadingTask.restart();
        this.m_BufferIndex = 0;
        ReadMeter();
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private void ReadMeter() {
        this.messageManager.sendBuffer(1, this.m_Buffers.get(this.m_BufferIndex));
    }

    private void initData() {
    }

    @Override // com.clou.rs350.ui.fragment.BaseFragment
    public int isFinish() {
        if (!this.m_Data.isEmpty()) {
            return 0;
        }
        new HintDialog(this.m_Context, (int) R.string.text_do_test).show();
        return 1;
    }

    @Override // com.clou.rs350.handler.HandlerSwitchView.IOnSelectCallback
    public void onSelectedChanged(int index) {
        setView(index);
    }

    @Override // com.clou.rs350.handler.HandlerSwitchView.IOnSelectCallback
    public void onClickView(View view) {
    }

    private void setView(int index) {
        int i;
        int i2 = 0;
        View findViewById = findViewById(R.id.layout_basic);
        if (index == 0) {
            i = 0;
        } else {
            i = 8;
        }
        findViewById.setVisibility(i);
        View findViewById2 = findViewById(R.id.layout_more);
        if (index != 1) {
            i2 = 8;
        }
        findViewById2.setVisibility(i2);
    }
}
