package com.clou.rs350.ui.view;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PointF;
import android.util.AttributeSet;
import android.util.FloatMath;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import com.itextpdf.text.pdf.ColumnText;
import com.itextpdf.text.pdf.PdfObject;
import com.itextpdf.text.pdf.codec.wmf.MetaDo;

@SuppressLint({"DrawAllocation", "FloatMath"})
public class MyZoomView extends View {
    private static final String TAG = "ZoomView";
    protected Context m_Context;
    protected int m_Height = 0;
    protected int m_Width = 0;
    private float m_ZoomLimit = 30.0f;
    protected int m_ZoomType = 0;
    private Canvas myCanvas = null;
    private Paint myPaint = null;
    private float oldDistx;
    private float oldDisty;
    private int pointCount;
    protected float rectHigh = ColumnText.GLOBAL_SPACE_CHAR_RATIO;
    protected float rectLeftX = ColumnText.GLOBAL_SPACE_CHAR_RATIO;
    protected float rectTopY = ColumnText.GLOBAL_SPACE_CHAR_RATIO;
    protected float rectWidth = ColumnText.GLOBAL_SPACE_CHAR_RATIO;
    private float scalex = 1.0f;
    private float scaley = 1.0f;
    private PointF start = new PointF();
    private PointF zoomLocation = new PointF();

    public MyZoomView(Context context) {
        super(context);
        this.m_Context = context;
    }

    public MyZoomView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        this.m_Context = context;
    }

    public MyZoomView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.m_Context = context;
    }

    /* access modifiers changed from: protected */
    public float getScaleWidth() {
        return this.rectWidth;
    }

    /* access modifiers changed from: protected */
    public float getScaleHeight() {
        return this.rectHigh;
    }

    private void init() {
    }

    /* access modifiers changed from: protected */
    public float getValue(int resId, float defaultValue) {
        return this.m_Context == null ? defaultValue : this.m_Context.getResources().getDimension(resId);
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        this.myCanvas = canvas;
        if (this.m_Width == 0) {
            this.m_Width = getWidth();
        }
        if (this.m_Height == 0) {
            this.m_Height = getHeight();
        }
        this.rectWidth = ((float) this.m_Width) * this.scalex;
        this.rectHigh = ((float) this.m_Height) * this.scaley;
        checkXY();
    }

    /* access modifiers changed from: protected */
    public void drawLine(float startX, float startY, float stopX, float stopY, Paint paint) {
        if (this.rectLeftX + startX < ColumnText.GLOBAL_SPACE_CHAR_RATIO && this.rectLeftX + stopX < ColumnText.GLOBAL_SPACE_CHAR_RATIO) {
            return;
        }
        if (this.rectTopY + startY < ColumnText.GLOBAL_SPACE_CHAR_RATIO && this.rectTopY + stopY < ColumnText.GLOBAL_SPACE_CHAR_RATIO) {
            return;
        }
        if (this.rectLeftX + startX > ((float) this.m_Width) && this.rectLeftX + stopX > ((float) this.m_Width)) {
            return;
        }
        if (this.rectTopY + startY <= ((float) this.m_Height) || this.rectTopY + stopY <= ((float) this.m_Height)) {
            this.myCanvas.drawLine(this.rectLeftX + startX, this.rectTopY + startY, this.rectLeftX + stopX, this.rectTopY + stopY, paint);
        }
    }

    /* access modifiers changed from: protected */
    public void drawText(String text, float x, float y, Paint paint) {
        if (this.rectLeftX + x >= -50.0f && this.rectTopY + y >= -50.0f && this.rectLeftX + x <= ((float) (this.m_Width + 50)) && this.rectTopY + y <= ((float) (this.m_Height + 50))) {
            this.myCanvas.drawText(text, this.rectLeftX + x, this.rectTopY + y, paint);
        }
    }

    /* access modifiers changed from: protected */
    public void drawCircle(float x, float y, float radius, int colorStroke, int colorBackground, float width, Paint paint) {
        int color = paint.getColor();
        paint.setColor(colorStroke);
        this.myCanvas.drawCircle(this.rectLeftX + x, this.rectTopY + y, radius, paint);
        paint.setColor(colorBackground);
        this.myCanvas.drawCircle(this.rectLeftX + x, this.rectTopY + y, radius - width, paint);
        paint.setColor(color);
    }

    class MyPoint {
        float x;
        float y;

        MyPoint(float x2, float y2) {
            this.x = x2;
            this.y = y2;
        }
    }

    private void checkXY() {
        if (this.rectLeftX > ColumnText.GLOBAL_SPACE_CHAR_RATIO) {
            this.rectLeftX = ColumnText.GLOBAL_SPACE_CHAR_RATIO;
        }
        float rightX = (float) this.m_Width;
        if (this.rectLeftX + this.rectWidth < rightX) {
            this.rectLeftX = rightX - this.rectWidth;
        }
        if (this.rectTopY > ColumnText.GLOBAL_SPACE_CHAR_RATIO) {
            this.rectTopY = ColumnText.GLOBAL_SPACE_CHAR_RATIO;
        }
        float bottomY = (float) this.m_Height;
        if (this.rectTopY + this.rectHigh < bottomY) {
            this.rectTopY = bottomY - this.rectHigh;
        }
    }

    public boolean onTouchEvent(MotionEvent event) {
        if (this.pointCount > event.getPointerCount()) {
            this.start = getLocation(event);
        }
        this.pointCount = event.getPointerCount();
        if (this.pointCount == 1) {
            this.oldDistx = ColumnText.GLOBAL_SPACE_CHAR_RATIO;
            this.oldDisty = ColumnText.GLOBAL_SPACE_CHAR_RATIO;
        }
        switch (event.getAction()) {
            case 0:
            case 5:
            case MetaDo.META_SETRELABS /*{ENCODED_INT: 261}*/:
                if (this.pointCount >= 2) {
                    if (1 == this.m_ZoomType) {
                        this.oldDistx = spacing(event);
                    } else {
                        this.oldDistx = Math.abs(event.getX(0) - event.getX(1));
                        this.oldDisty = Math.abs(event.getY(0) - event.getY(1));
                        if (this.m_ZoomLimit > this.oldDistx) {
                            this.oldDistx = this.m_ZoomLimit;
                        }
                        if (this.m_ZoomLimit > this.oldDisty) {
                            this.oldDisty = this.m_ZoomLimit;
                        }
                    }
                    this.zoomLocation = getLocation(event);
                }
                this.start = getLocation(event);
                break;
            case 2:
                doZoom(event);
                doMove(event);
                break;
            case 3:
                Log.i(PdfObject.NOTHING, PdfObject.NOTHING);
                break;
        }
        return true;
    }

    private PointF getLocation(MotionEvent event) {
        float x = ColumnText.GLOBAL_SPACE_CHAR_RATIO;
        float y = ColumnText.GLOBAL_SPACE_CHAR_RATIO;
        int count = event.getPointerCount();
        for (int i = 0; i < count; i++) {
            x += event.getX(i);
            y += event.getY(i);
        }
        return new PointF(x / ((float) count), y / ((float) count));
    }

    private void doMove(MotionEvent event) {
        PointF stop = getLocation(event);
        this.rectLeftX += stop.x - this.start.x;
        this.rectTopY += stop.y - this.start.y;
        postInvalidate();
        this.start = stop;
    }

    private void doZoom(MotionEvent event) {
        if (1 == this.m_ZoomType) {
            float newDistx = spacing(event);
            if (ColumnText.GLOBAL_SPACE_CHAR_RATIO != newDistx) {
                this.scalex *= newDistx / this.oldDistx;
                if (this.scalex < 1.0f) {
                    this.scalex = 1.0f;
                }
                this.scaley = this.scalex;
                this.oldDistx = newDistx;
            } else {
                return;
            }
        } else if (event.getPointerCount() >= 2) {
            float x = Math.abs(event.getX(0) - event.getX(1));
            if (this.m_ZoomLimit < x) {
                this.scalex *= x / this.oldDistx;
                if (this.scalex < 1.0f) {
                    this.scalex = 1.0f;
                }
                this.oldDistx = x;
            }
            float y = Math.abs(event.getY(0) - event.getY(1));
            if (this.m_ZoomLimit < y) {
                this.scaley *= y / this.oldDisty;
                if (this.scaley < 1.0f) {
                    this.scaley = 1.0f;
                }
                this.oldDisty = y;
            }
        } else {
            return;
        }
        computerMoveByZoom(this.scalex, this.scaley);
        postInvalidate();
    }

    private void computerMoveByZoom(float scalex2, float scaley2) {
        float rectWidth2 = ((float) this.m_Width) * scalex2;
        float rectHigh2 = ((float) this.m_Height) * scaley2;
        float trans_x = (this.rectWidth - rectWidth2) * ((this.zoomLocation.x - this.rectLeftX) / this.rectWidth);
        float trans_y = (this.rectHigh - rectHigh2) * ((this.zoomLocation.y - this.rectTopY) / this.rectHigh);
        this.rectLeftX += trans_x;
        this.rectTopY += trans_y;
    }

    private float spacing(MotionEvent event) {
        if (event.getPointerCount() < 2) {
            return ColumnText.GLOBAL_SPACE_CHAR_RATIO;
        }
        float x = event.getX(0) - event.getX(1);
        float y = event.getY(0) - event.getY(1);
        return (float) Math.sqrt((x * x) + (y * y));
    }
}
