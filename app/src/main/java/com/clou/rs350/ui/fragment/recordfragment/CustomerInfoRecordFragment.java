package com.clou.rs350.ui.fragment.recordfragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.clou.rs350.R;
import com.clou.rs350.db.dao.CustomerInfoDao;
import com.clou.rs350.db.model.CustomerInfo;
import com.clou.rs350.ui.fragment.BaseFragment;
import com.clou.rs350.ui.view.infoview.CustomerInfoView;

public class CustomerInfoRecordFragment extends BaseFragment {
    Button btn_Cancel = null;
    Button btn_Modify = null;
    TextView contentView = null;
    boolean isModefy = false;
    CustomerInfo m_Info;
    CustomerInfoView m_InfoView;
    View m_InfoViewLayout;

    public CustomerInfoRecordFragment(CustomerInfo data) {
        this.m_Info = data;
    }

    @Override // android.support.v4.app.Fragment
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        this.fragmentView = inflater.inflate(R.layout.fragment_record_customer_info, (ViewGroup) null);
        return this.fragmentView;
    }

    @Override // android.support.v4.app.Fragment, com.clou.rs350.ui.fragment.BaseFragment
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initView();
    }

    private void initView() {
        this.m_InfoViewLayout = findViewById(R.id.layout_customer_info);
        this.m_InfoView = new CustomerInfoView(this.m_Context, this.m_InfoViewLayout, this.m_Info, 0);
        this.btn_Cancel = (Button) findViewById(R.id.btn_cancel);
        this.btn_Cancel.setOnClickListener(new View.OnClickListener() {
            /* class com.clou.rs350.ui.fragment.recordfragment.CustomerInfoRecordFragment.AnonymousClass1 */

            public void onClick(View v) {
                if (CustomerInfoRecordFragment.this.isModefy) {
                    CustomerInfoRecordFragment.this.StopModify();
                    CustomerInfoRecordFragment.this.m_InfoView.refreshData(CustomerInfoRecordFragment.this.m_Info);
                }
            }
        });
        this.btn_Modify = (Button) findViewById(R.id.btn_modify);
        this.btn_Modify.setOnClickListener(new View.OnClickListener() {
            /* class com.clou.rs350.ui.fragment.recordfragment.CustomerInfoRecordFragment.AnonymousClass2 */

            public void onClick(View v) {
                if (CustomerInfoRecordFragment.this.isModefy) {
                    CustomerInfoRecordFragment.this.Save();
                } else {
                    CustomerInfoRecordFragment.this.Modify();
                }
            }
        });
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private void Modify() {
        this.m_InfoView.setFunction(2);
        this.btn_Modify.setText(R.string.text_save);
        this.btn_Cancel.setEnabled(true);
        this.isModefy = true;
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private void Save() {
        StopModify();
        this.m_Info = this.m_InfoView.getInfo();
        if (this.m_Info.isNew) {
            new CustomerInfoDao(this.m_Context).insertObject(this.m_Info);
            this.m_Info.isNew = false;
        } else {
            new CustomerInfoDao(this.m_Context).updateObjectByKey(this.m_Info.CustomerSr, this.m_Info);
        }
        showHint(R.string.text_save_success);
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private void StopModify() {
        this.m_InfoView.setFunction(0);
        this.btn_Modify.setText(R.string.text_modify);
        this.btn_Cancel.setEnabled(false);
        this.isModefy = false;
    }
}
