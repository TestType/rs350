package com.clou.rs350.ui.popwindow;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.PopupWindow;
import android.widget.RadioButton;
import android.widget.TextView;

import com.clou.rs350.Preferences;
import com.clou.rs350.R;
import com.clou.rs350.model.ClouData;
import com.clou.rs350.ui.dialog.NumericDialog;
import com.clou.rs350.utils.OtherUtils;

public class ErrorConfigurePopWindow extends PopupWindow implements View.OnClickListener, CompoundButton.OnCheckedChangeListener {
    RadioButton chk_AutoStop;
    CheckBox chk_IsBaseTime;
    RadioButton chk_ManualStop;
    CheckBox chk_Meter2Enable;
    CheckBox chk_Meter3Enable;
    RadioButton chk_ScanningHead;
    RadioButton chk_StartStopSwitch;
    Context mContext;
    View.OnClickListener mOnClick;
    int temp_PQ_Flag;
    int testMeterCount;
    TextView txt_ErrorCount;
    TextView txt_ErrorTime;
    TextView txt_Meter1Accuracy;
    TextView txt_Meter2Accuracy;
    TextView txt_Meter3Accuracy;
    TextView txt_WaitLimit;

    public ErrorConfigurePopWindow(Context context) {
        super(context);
        this.mContext = context;
        initView();
    }

    private void initView() {
        View conentView = ((LayoutInflater) this.mContext.getSystemService("layout_inflater")).inflate(R.layout.pop_window_errorconfigure, (ViewGroup) null);
        conentView.findViewById(R.id.ok).setOnClickListener(this);
        conentView.findViewById(R.id.cancel).setOnClickListener(this);
        this.txt_ErrorCount = (TextView) conentView.findViewById(R.id.txt_error_count);
        this.txt_ErrorCount.setOnClickListener(this);
        this.txt_ErrorTime = (TextView) conentView.findViewById(R.id.txt_time_basic);
        this.txt_ErrorTime.setOnClickListener(this);
        this.txt_WaitLimit = (TextView) conentView.findViewById(R.id.txt_wait_limit);
        this.txt_WaitLimit.setOnClickListener(this);
        this.txt_Meter1Accuracy = (TextView) conentView.findViewById(R.id.txt_meter1_accuracy);
        this.txt_Meter1Accuracy.setOnClickListener(this);
        this.txt_Meter2Accuracy = (TextView) conentView.findViewById(R.id.txt_meter2_accuracy);
        this.txt_Meter2Accuracy.setOnClickListener(this);
        this.txt_Meter3Accuracy = (TextView) conentView.findViewById(R.id.txt_meter3_accuracy);
        this.txt_Meter3Accuracy.setOnClickListener(this);
        this.chk_Meter2Enable = (CheckBox) conentView.findViewById(R.id.chk_meter2);
        this.chk_Meter3Enable = (CheckBox) conentView.findViewById(R.id.chk_meter3);
        this.chk_IsBaseTime = (CheckBox) conentView.findViewById(R.id.chk_is_base_time);
        this.chk_AutoStop = (RadioButton) conentView.findViewById(R.id.chk_auto_stop);
        this.chk_ManualStop = (RadioButton) conentView.findViewById(R.id.chk_manual_stop);
        this.chk_ScanningHead = (RadioButton) conentView.findViewById(R.id.chk_scanning_head);
        this.chk_StartStopSwitch = (RadioButton) conentView.findViewById(R.id.chk_start_stop_switch);
        setContentView(conentView);
        setWidth(this.mContext.getResources().getDimensionPixelSize(R.dimen.dp_450));
        setHeight(this.mContext.getResources().getDimensionPixelSize(R.dimen.dp_240));
        setFocusable(true);
        setOutsideTouchable(true);
        update();
        setBackgroundDrawable(new ColorDrawable(0));
        setAnimationStyle(16973826);
    }

    private void showValue() {
        boolean z;
        boolean z2;
        switch (this.temp_PQ_Flag) {
            case 0:
                this.txt_Meter1Accuracy.setText(ClouData.getInstance().getMeterBaseInfo()[0].ActiveAccuracy.trim());
                this.txt_Meter2Accuracy.setText(ClouData.getInstance().getMeterBaseInfo()[1].ActiveAccuracy.trim());
                this.txt_Meter3Accuracy.setText(ClouData.getInstance().getMeterBaseInfo()[2].ActiveAccuracy.trim());
                break;
            case 1:
                this.txt_Meter1Accuracy.setText(ClouData.getInstance().getMeterBaseInfo()[0].ReactiveAccuracy.trim());
                this.txt_Meter2Accuracy.setText(ClouData.getInstance().getMeterBaseInfo()[1].ReactiveAccuracy.trim());
                this.txt_Meter3Accuracy.setText(ClouData.getInstance().getMeterBaseInfo()[2].ReactiveAccuracy.trim());
                break;
            case 2:
                this.txt_Meter1Accuracy.setText(ClouData.getInstance().getMeterBaseInfo()[0].ApparentAccuracy.trim());
                this.txt_Meter2Accuracy.setText(ClouData.getInstance().getMeterBaseInfo()[1].ApparentAccuracy.trim());
                this.txt_Meter3Accuracy.setText(ClouData.getInstance().getMeterBaseInfo()[2].ApparentAccuracy.trim());
                break;
        }
        CheckBox checkBox = this.chk_Meter2Enable;
        if ((this.testMeterCount & 2) == 2) {
            z = true;
        } else {
            z = false;
        }
        checkBox.setChecked(z);
        CheckBox checkBox2 = this.chk_Meter3Enable;
        if ((this.testMeterCount & 4) == 4) {
            z2 = true;
        } else {
            z2 = false;
        }
        checkBox2.setChecked(z2);
        if (Preferences.getInt(Preferences.Key.ERRORTESTMODE, 0) == 0) {
            this.chk_ScanningHead.setChecked(true);
        } else {
            this.chk_StartStopSwitch.setChecked(true);
        }
        this.txt_ErrorCount.setText(new StringBuilder(String.valueOf(Preferences.getInt("ErrorCount", 5))).toString());
        this.txt_ErrorTime.setText(new StringBuilder(String.valueOf(Preferences.getInt(Preferences.Key.TIMEBASEFORERROR, 5))).toString());
        this.chk_IsBaseTime.setChecked(Preferences.getBoolean(Preferences.Key.ERRORISBASETIME, false));
        this.txt_WaitLimit.setText(new StringBuilder(String.valueOf(Preferences.getInt(Preferences.Key.WAITLIMIT, 3))).toString());
        if (Preferences.getBoolean(Preferences.Key.ERRORAUTOSTOP, true)) {
            this.chk_AutoStop.setChecked(true);
        } else {
            this.chk_ManualStop.setChecked(true);
        }
    }

    private void praseValue() {
        int i = 2;
        int i2 = 0;
        switch (this.temp_PQ_Flag) {
            case 0:
                ClouData.getInstance().getMeterBaseInfo()[0].ActiveAccuracy = this.txt_Meter1Accuracy.getText().toString();
                ClouData.getInstance().getMeterBaseInfo()[1].ActiveAccuracy = this.txt_Meter2Accuracy.getText().toString();
                ClouData.getInstance().getMeterBaseInfo()[2].ActiveAccuracy = this.txt_Meter3Accuracy.getText().toString();
                break;
            case 1:
                ClouData.getInstance().getMeterBaseInfo()[0].ReactiveAccuracy = this.txt_Meter1Accuracy.getText().toString();
                ClouData.getInstance().getMeterBaseInfo()[1].ReactiveAccuracy = this.txt_Meter2Accuracy.getText().toString();
                ClouData.getInstance().getMeterBaseInfo()[2].ReactiveAccuracy = this.txt_Meter3Accuracy.getText().toString();
                break;
            case 2:
                ClouData.getInstance().getMeterBaseInfo()[0].ApparentAccuracy = this.txt_Meter1Accuracy.getText().toString();
                ClouData.getInstance().getMeterBaseInfo()[1].ApparentAccuracy = this.txt_Meter2Accuracy.getText().toString();
                ClouData.getInstance().getMeterBaseInfo()[2].ApparentAccuracy = this.txt_Meter3Accuracy.getText().toString();
                break;
        }
        if (!this.chk_Meter2Enable.isChecked()) {
            i = 0;
        }
        this.testMeterCount = (this.chk_Meter3Enable.isChecked() ? 4 : 0) | i | 1;
        ClouData.getInstance().setTestMeterCount(this.testMeterCount);
        if (!this.chk_ScanningHead.isChecked()) {
            i2 = 1;
        }
        Preferences.putInt(Preferences.Key.ERRORTESTMODE, i2);
        Preferences.putInt("ErrorCount", OtherUtils.getIntValue(this.txt_ErrorCount));
        Preferences.putBoolean(Preferences.Key.ERRORISBASETIME, this.chk_IsBaseTime.isChecked());
        Preferences.putInt(Preferences.Key.TIMEBASEFORERROR, OtherUtils.getIntValue(this.txt_ErrorTime));
        Preferences.putInt(Preferences.Key.WAITLIMIT, OtherUtils.getIntValue(this.txt_WaitLimit));
        Preferences.putBoolean(Preferences.Key.ERRORAUTOSTOP, this.chk_AutoStop.isChecked());
    }

    public void showPopWindow(View currentClick, int PQFlag, View.OnClickListener onclick) {
        this.mOnClick = onclick;
        this.temp_PQ_Flag = PQFlag;
        this.testMeterCount = ClouData.getInstance().getTestMeterCount();
        showValue();
        if (!isShowing()) {
            int[] location = new int[2];
            currentClick.getLocationOnScreen(location);
            showAtLocation(currentClick, 0, location[0] - getWidth(), location[1]);
            return;
        }
        dismiss();
    }

    public void onClick(View v) {
        NumericDialog nd = new NumericDialog(this.mContext);
        switch (v.getId()) {
            case R.id.txt_error_count:
                nd.setMax(10.0d);
                nd.setMin(3.0d);
                nd.setMinus(false);
                nd.setDot(false);
                nd.showMyDialog((TextView) v);
                return;
            case R.id.chk_is_base_time:
            case R.id.dp_setting:
            case R.id.tp_setting:
            case R.id.layout_test_3meter:
            default:
                return;
            case R.id.txt_time_basic:
                nd.txtLimit = 3;
                nd.setMin(1.0d);
                nd.setDot(false);
                nd.setMinus(false);
                nd.showMyDialog((TextView) v);
                return;
            case R.id.txt_wait_limit:
                nd.txtLimit = 3;
                nd.setMin(2.0d);
                nd.setDot(false);
                nd.setMinus(false);
                nd.showMyDialog((TextView) v);
                return;
            case R.id.ok:
                praseValue();
                if (this.mOnClick != null) {
                    this.mOnClick.onClick(v);
                }
                if (isShowing()) {
                    dismiss();
                    return;
                }
                return;
            case R.id.cancel:
                if (isShowing()) {
                    dismiss();
                    return;
                }
                return;
            case R.id.txt_meter1_accuracy:
            case R.id.txt_meter2_accuracy:
            case R.id.txt_meter3_accuracy:
                nd.txtLimit = 4;
                nd.setMinus(false);
                nd.showMyDialog((TextView) v);
                return;
        }
    }

    public void onCheckedChanged(CompoundButton v, boolean isChecked) {
    }
}
