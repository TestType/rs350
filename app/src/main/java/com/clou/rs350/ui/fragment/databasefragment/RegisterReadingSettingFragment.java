package com.clou.rs350.ui.fragment.databasefragment;

import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.clou.rs350.R;
import com.clou.rs350.db.dao.sequence.RegisterReadingSchemeDao;
import com.clou.rs350.db.model.sequence.RegisterReadingData;
import com.clou.rs350.db.model.sequence.RegisterReadingItem;
import com.clou.rs350.model.ClouData;
import com.clou.rs350.ui.adapter.RegisterReadingEditItemAdapter;
import com.clou.rs350.ui.dialog.HintDialog;
import com.clou.rs350.ui.dialog.NormalDialog;
import com.clou.rs350.ui.dialog.NumericDialog;
import com.clou.rs350.ui.dialog.SaveAsDialog;
import com.clou.rs350.ui.fragment.BaseFragment;
import com.clou.rs350.ui.popwindow.ListPopWindow;
import com.clou.rs350.utils.DoubleClick;
import com.clou.rs350.utils.OtherUtils;
import com.itextpdf.text.pdf.PdfObject;
import com.lidroid.xutils.ViewUtils;

import java.util.List;

public class RegisterReadingSettingFragment extends BaseFragment implements View.OnClickListener {
    Button btn_Cancel = null;
    Button btn_Delete = null;
    Button btn_Modify = null;
    LinearLayout btn_Name_Select = null;
    Button btn_New = null;
    Button btn_Save = null;
    TextView contentView = null;
    View layout_ActivitiesCount = null;
    ListView lst_List = null;
    RegisterReadingEditItemAdapter m_Adapter = null;
    int m_Authority = 0;
    boolean m_HasSearch = false;
    boolean m_IsModefy = false;
    boolean m_IsNew = false;
    RegisterReadingData m_Scheme;
    private RegisterReadingSchemeDao m_SettingDao;
    private DoubleClick selectClick = null;
    TextView txt_ActivitiesCount = null;
    EditText txt_Name_Edit = null;

    @Override // android.support.v4.app.Fragment
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        this.fragmentView = inflater.inflate(R.layout.fragment_register_reading_setting, (ViewGroup) null);
        ViewUtils.inject(this, this.fragmentView);
        return this.fragmentView;
    }

    @Override // android.support.v4.app.Fragment, com.clou.rs350.ui.fragment.BaseFragment
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initView();
    }

    public void initView() {
        this.m_Authority = ClouData.getInstance().getOperatorInfo().Authority;
        this.m_SettingDao = new RegisterReadingSchemeDao(this.m_Context);
        this.m_Scheme = new RegisterReadingData();
        initTextView();
        this.btn_New = (Button) findViewById(R.id.btn_new);
        this.btn_New.setOnClickListener(this);
        this.btn_Modify = (Button) findViewById(R.id.btn_modify);
        this.btn_Modify.setOnClickListener(this);
        this.btn_Delete = (Button) findViewById(R.id.btn_delete);
        this.btn_Delete.setOnClickListener(this);
        this.btn_Save = (Button) findViewById(R.id.btn_save);
        this.btn_Save.setOnClickListener(this);
        this.btn_Cancel = (Button) findViewById(R.id.btn_cancel);
        this.btn_Cancel.setOnClickListener(this);
        setEditButton(false, false);
    }

    private void initTextView() {
        this.layout_ActivitiesCount = findViewById(R.id.layout_activities_count);
        this.lst_List = (ListView) findViewById(R.id.list_scheme_edit);
        this.m_Adapter = new RegisterReadingEditItemAdapter(this.m_Context);
        this.lst_List.setAdapter((ListAdapter) this.m_Adapter);
        this.txt_ActivitiesCount = (TextView) findViewById(R.id.txt_items_count);
        this.txt_ActivitiesCount.setOnClickListener(new View.OnClickListener() {
            /* class com.clou.rs350.ui.fragment.databasefragment.RegisterReadingSettingFragment.AnonymousClass1 */

            public void onClick(View v) {
                NumericDialog nd = new NumericDialog(RegisterReadingSettingFragment.this.m_Context);
                nd.txtLimit = 2;
                nd.setOnDoneClick(new View.OnClickListener() {
                    /* class com.clou.rs350.ui.fragment.databasefragment.RegisterReadingSettingFragment.AnonymousClass1.AnonymousClass1 */

                    public void onClick(View v) {
                        int count = OtherUtils.parseInt(RegisterReadingSettingFragment.this.txt_ActivitiesCount.getText().toString());
                        List<RegisterReadingItem> tmpList = RegisterReadingSettingFragment.this.m_Adapter.getDatas();
                        if (count > tmpList.size()) {
                            while (count > tmpList.size()) {
                                tmpList.add(new RegisterReadingItem());
                            }
                        } else if (count < tmpList.size()) {
                            while (count < tmpList.size()) {
                                tmpList.remove(count);
                            }
                        }
                        RegisterReadingSettingFragment.this.txt_ActivitiesCount.setText(new StringBuilder(String.valueOf(tmpList.size())).toString());
                        RegisterReadingSettingFragment.this.m_Adapter.refresh(tmpList);
                    }
                });
                nd.showMyDialog((TextView) v);
            }
        });
        this.txt_Name_Edit = (EditText) findViewById(R.id.txt_name_edit);
        this.txt_Name_Edit.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            /* class com.clou.rs350.ui.fragment.databasefragment.RegisterReadingSettingFragment.AnonymousClass2 */

            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if ((actionId == 6 || actionId == 5) && !RegisterReadingSettingFragment.this.m_IsModefy) {
                    RegisterReadingSettingFragment.this.searchData(RegisterReadingSettingFragment.this.txt_Name_Edit.getText().toString());
                }
                Log.i("actionId", new StringBuilder(String.valueOf(actionId)).toString());
                return false;
            }
        });
        this.btn_Name_Select = (LinearLayout) findViewById(R.id.btn_name_select);
        this.btn_Name_Select.setOnClickListener(new View.OnClickListener() {
            /* class com.clou.rs350.ui.fragment.databasefragment.RegisterReadingSettingFragment.AnonymousClass3 */

            public void onClick(View v) {
                RegisterReadingSettingFragment.this.selectClick.click();
            }
        });
        this.selectClick = new DoubleClick(new DoubleClick.OnDoubleClick() {
            /* class com.clou.rs350.ui.fragment.databasefragment.RegisterReadingSettingFragment.AnonymousClass4 */

            private void searchKey(String condition) {
                List<String> tmpSerials = RegisterReadingSettingFragment.this.m_SettingDao.queryAllKey(condition);
                ListPopWindow.showListPopwindow(RegisterReadingSettingFragment.this.m_Context, (String[]) tmpSerials.toArray(new String[tmpSerials.size()]), RegisterReadingSettingFragment.this.txt_Name_Edit, new View.OnClickListener() {
                    /* class com.clou.rs350.ui.fragment.databasefragment.RegisterReadingSettingFragment.AnonymousClass4.AnonymousClass1 */

                    public void onClick(View v) {
                        RegisterReadingSettingFragment.this.searchData(RegisterReadingSettingFragment.this.txt_Name_Edit.getText().toString());
                    }
                });
            }

            @Override // com.clou.rs350.utils.DoubleClick.OnDoubleClick
            public void doubleClick() {
                searchKey(RegisterReadingSettingFragment.this.txt_Name_Edit.getText().toString());
            }

            @Override // com.clou.rs350.utils.DoubleClick.OnDoubleClick
            public void singleClick() {
                searchKey(PdfObject.NOTHING);
            }
        });
    }

    private void showData() {
        refreshAdapter(this.m_Scheme);
    }

    private void parseData() {
        this.m_Scheme.SchemeName = this.txt_Name_Edit.getText().toString();
        this.m_Scheme.Registers = this.m_Adapter.getDatas();
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private void Save() {
        if (!this.m_IsNew || !OtherUtils.isEmpty(this.txt_Name_Edit)) {
            parseData();
            String Name = this.m_Scheme.SchemeName;
            if (!this.m_IsNew) {
                this.m_SettingDao.deleteRecordByKey(Name);
            } else if (this.m_SettingDao.queryByKey(Name) != null) {
                new HintDialog(this.m_Context, (int) R.string.text_scheme_name_exists).show();
                return;
            }
            this.m_Scheme.Language = ClouData.getInstance(this.m_Context).getSettingBasic().Language;
            this.m_SettingDao.insertObject(this.m_Scheme);
            setEditButton(false, false);
            refreshAdapter(this.m_Scheme);
            showHint(R.string.text_save_success);
            return;
        }
        new HintDialog(this.m_Context, (int) R.string.text_scheme_name_empty).show();
    }

    private void setEditButton(boolean Edit, boolean isNew) {
        boolean z;
        boolean z2;
        boolean z3;
        boolean z4;
        boolean z5 = true;
        this.layout_ActivitiesCount.setVisibility(Edit ? 0 : 8);
        this.m_IsNew = isNew;
        this.m_IsModefy = Edit;
        EditText editText = this.txt_Name_Edit;
        if (!Edit || isNew) {
            z = true;
        } else {
            z = false;
        }
        editText.setEnabled(z);
        LinearLayout linearLayout = this.btn_Name_Select;
        if (Edit) {
            z2 = false;
        } else {
            z2 = true;
        }
        linearLayout.setEnabled(z2);
        Button button = this.btn_New;
        if ((!Edit || !isNew) && 2 < this.m_Authority) {
            z3 = true;
        } else {
            z3 = false;
        }
        button.setEnabled(z3);
        Button button2 = this.btn_Modify;
        if (Edit || 2 >= this.m_Authority) {
            z4 = false;
        } else {
            z4 = true;
        }
        button2.setEnabled(z4);
        this.btn_Cancel.setEnabled(Edit);
        Button button3 = this.btn_Delete;
        if (isNew || 2 >= this.m_Authority) {
            z5 = false;
        }
        button3.setEnabled(z5);
        this.btn_Delete.setText(Edit ? R.string.text_save_as : R.string.text_delete);
        this.btn_Save.setEnabled(Edit);
        this.m_Adapter.setEnable(Edit);
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private void refreshAdapter(RegisterReadingData data) {
        this.txt_Name_Edit.setText(data.SchemeName);
        this.txt_ActivitiesCount.setText(new StringBuilder(String.valueOf(data.Registers.size())).toString());
        this.m_Adapter.refresh(data.Registers);
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private void searchData(String schemeName) {
        this.m_Scheme = (RegisterReadingData) this.m_SettingDao.queryByKey(schemeName);
        if (this.m_Scheme != null) {
            this.m_HasSearch = true;
            showData();
            return;
        }
        this.m_HasSearch = false;
        this.m_Scheme = new RegisterReadingData();
        this.m_Scheme.SchemeName = schemeName;
        showData();
        this.txt_Name_Edit.setSelection(schemeName.length());
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_delete:
                if (this.m_IsModefy) {
                    SaveAsDialog tmpDialog = new SaveAsDialog(this.m_Context, this.m_SettingDao);
                    tmpDialog.setWarning(R.string.text_scheme_name_empty);
                    tmpDialog.setExist(R.string.text_scheme_name_exists);
                    tmpDialog.setOnClick(new View.OnClickListener() {
                        /* class com.clou.rs350.ui.fragment.databasefragment.RegisterReadingSettingFragment.AnonymousClass5 */

                        public void onClick(View v) {
                            RegisterReadingSettingFragment.this.m_Scheme.SchemeName = v.getTag().toString();
                            RegisterReadingSettingFragment.this.txt_Name_Edit.setText(RegisterReadingSettingFragment.this.m_Scheme.SchemeName);
                            RegisterReadingSettingFragment.this.Save();
                        }
                    }, null);
                    tmpDialog.show();
                    tmpDialog.setSelectionValue(this.m_Scheme.SchemeName);
                    return;
                } else if (!this.m_HasSearch) {
                    new HintDialog(this.m_Context, (int) R.string.text_scheme_name_wrong).show();
                    return;
                } else if (OtherUtils.isEmpty(this.txt_Name_Edit)) {
                    new HintDialog(this.m_Context, (int) R.string.text_scheme_name_empty).show();
                    return;
                } else {
                    NormalDialog dialog = new NormalDialog(this.m_Context);
                    dialog.setOnClick(new View.OnClickListener() {
                        /* class com.clou.rs350.ui.fragment.databasefragment.RegisterReadingSettingFragment.AnonymousClass6 */

                        public void onClick(View v) {
                            RegisterReadingSettingFragment.this.m_SettingDao.deleteRecordByKey(RegisterReadingSettingFragment.this.m_Scheme.SchemeName);
                            RegisterReadingSettingFragment.this.m_Scheme = new RegisterReadingData();
                            RegisterReadingSettingFragment.this.refreshAdapter(RegisterReadingSettingFragment.this.m_Scheme);
                        }
                    }, null);
                    dialog.show();
                    dialog.setTitle(R.string.text_scheme_delete);
                    return;
                }
            case R.id.btn_save:
                Save();
                return;
            case R.id.btn_cancel:
                setEditButton(false, false);
                searchData(this.m_Scheme.SchemeName);
                return;
            case R.id.btn_new:
                setEditButton(true, true);
                refreshAdapter(new RegisterReadingData());
                return;
            case R.id.btn_modify:
                if (!this.m_HasSearch) {
                    new HintDialog(this.m_Context, (int) R.string.text_scheme_name_wrong).show();
                    return;
                } else if (OtherUtils.isEmpty(this.txt_Name_Edit)) {
                    new HintDialog(this.m_Context, (int) R.string.text_scheme_name_empty).show();
                    return;
                } else {
                    setEditButton(true, false);
                    refreshAdapter(this.m_Scheme);
                    return;
                }
            default:
                return;
        }
    }

    @Override // android.support.v4.app.Fragment, com.clou.rs350.ui.fragment.BaseFragment
    public void onPause() {
        setEditButton(false, false);
        refreshAdapter(this.m_Scheme);
        super.onPause();
    }
}
