package com.clou.rs350.ui.fragment.recordfragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.TextView;

import com.clou.rs350.R;
import com.clou.rs350.db.model.CTBurden;
import com.clou.rs350.ui.fragment.BaseFragment;
import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.view.annotation.ViewInject;

public class CTBurdenRecordFragment extends BaseFragment {
    @ViewInject(R.id.chk_ctterminal)
    private RadioButton CTTerminalCkBox;
    @ViewInject(R.id.l1ctburden)
    private TextView L1CtBurdenTv;
    @ViewInject(R.id.l1ctcurrent)
    private TextView L1CtCurrentTv;
    @ViewInject(R.id.l1ctnominal)
    private TextView L1CtNominalTv;
    @ViewInject(R.id.l1ctvoltage)
    private TextView L1CtVoltageTv;
    @ViewInject(R.id.l1)
    private TextView L1ResultTv;
    @ViewInject(R.id.l2ctburden)
    private TextView L2CtBurdenTv;
    @ViewInject(R.id.l2ctcurrent)
    private TextView L2CtCurrentTv;
    @ViewInject(R.id.l2ctnominal)
    private TextView L2CtNominalTv;
    @ViewInject(R.id.l2ctvoltage)
    private TextView L2CtVoltageTv;
    @ViewInject(R.id.l2)
    private TextView L2ResultTv;
    @ViewInject(R.id.l3ctburden)
    private TextView L3CtBurdenTv;
    @ViewInject(R.id.l3ctcurrent)
    private TextView L3CtCurrentTv;
    @ViewInject(R.id.l3ctnominal)
    private TextView L3CtNominalTv;
    @ViewInject(R.id.l3ctvoltage)
    private TextView L3CtVoltageTv;
    @ViewInject(R.id.l3)
    private TextView L3ResultTv;
    @ViewInject(R.id.txt_length)
    private TextView LengthTv;
    @ViewInject(R.id.chk_meterterminal)
    private RadioButton MeterTerminalCkBox;
    @ViewInject(R.id.txt_size)
    private TextView SizeTv;
    @ViewInject(R.id.txt_va)
    private TextView VaTv;
    private CTBurden m_CTBurden;

    public CTBurdenRecordFragment(CTBurden data) {
        this.m_CTBurden = data;
    }

    @Override // android.support.v4.app.Fragment
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        this.fragmentView = inflater.inflate(R.layout.fragment_record_ct_burden, (ViewGroup) null);
        ViewUtils.inject(this, this.fragmentView);
        return this.fragmentView;
    }

    @Override // android.support.v4.app.Fragment, com.clou.rs350.ui.fragment.BaseFragment
    public void onResume() {
        super.onResume();
        showSavedData();
        initData();
    }

    private void initData() {
    }

    private void showSavedData() {
        this.VaTv.setText(new StringBuilder(String.valueOf(this.m_CTBurden.CTVA.s_Value)).toString());
        setWireEnable(this.m_CTBurden.CTTerminal != 0);
        this.LengthTv.setText(new StringBuilder(String.valueOf(this.m_CTBurden.LengthOfWire.s_Value)).toString());
        this.SizeTv.setText(new StringBuilder(String.valueOf(this.m_CTBurden.SizeOfWire.s_Value)).toString());
        showData();
    }

    @Override // android.support.v4.app.Fragment, com.clou.rs350.ui.fragment.BaseFragment
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initView();
    }

    private void initView() {
    }

    private void setWireEnable(boolean Flag) {
        boolean z = false;
        this.m_CTBurden.CTTerminal = Flag ? 1 : 0;
        this.MeterTerminalCkBox.setChecked(Flag);
        RadioButton radioButton = this.CTTerminalCkBox;
        if (!Flag) {
            z = true;
        }
        radioButton.setChecked(z);
        this.LengthTv.setEnabled(Flag);
        this.SizeTv.setEnabled(Flag);
    }

    private void showData() {
        this.L1CtCurrentTv.setText(this.m_CTBurden.CurrentValueSecondary[0].s_Value);
        this.L2CtCurrentTv.setText(this.m_CTBurden.CurrentValueSecondary[1].s_Value);
        this.L3CtCurrentTv.setText(this.m_CTBurden.CurrentValueSecondary[2].s_Value);
        this.L1CtVoltageTv.setText(this.m_CTBurden.VoltageValueSecondary[0].s_Value);
        this.L2CtVoltageTv.setText(this.m_CTBurden.VoltageValueSecondary[1].s_Value);
        this.L3CtVoltageTv.setText(this.m_CTBurden.VoltageValueSecondary[2].s_Value);
        this.L1CtBurdenTv.setText(this.m_CTBurden.Burden[0].s_Value);
        this.L2CtBurdenTv.setText(this.m_CTBurden.Burden[1].s_Value);
        this.L3CtBurdenTv.setText(this.m_CTBurden.Burden[2].s_Value);
        this.L1CtNominalTv.setText(this.m_CTBurden.BurdenPersents[0].s_Value);
        this.L2CtNominalTv.setText(this.m_CTBurden.BurdenPersents[1].s_Value);
        this.L3CtNominalTv.setText(this.m_CTBurden.BurdenPersents[2].s_Value);
    }

    @Override // android.support.v4.app.Fragment, com.clou.rs350.ui.fragment.BaseFragment
    public void onPause() {
        super.onPause();
    }
}
