package com.clou.rs350.ui.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CompoundButton;
import android.widget.RadioButton;
import android.widget.TextView;

import com.clou.rs350.R;
import com.clou.rs350.db.model.sequence.VisualItem;

import java.util.List;

public class VisualItemAdapter extends BaseAdapter {
    private Context context;
    List<VisualItem> m_Data;
    private int m_MustDo;

    public VisualItemAdapter(Context context2, int flag) {
        this.context = context2;
        this.m_MustDo = flag;
    }

    public void refresh(List<VisualItem> data) {
        this.m_Data = data;
        notifyDataSetChanged();
    }

    public int getCount() {
        if (this.m_Data != null) {
            return this.m_Data.size();
        }
        return 0;
    }

    public Object getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return 0;
    }

    public List<VisualItem> getData() {
        return this.m_Data;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = View.inflate(this.context, R.layout.adapter_visual_item, null);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        holder.showValue(position, this.m_Data.get(position));
        return convertView;
    }

    class ViewHolder implements CompoundButton.OnCheckedChangeListener {
        RadioButton[] chk_Results;
        int m_Index;
        TextView txt_Activites;
        TextView txt_Index;

        ViewHolder(View v) {
            this.txt_Index = (TextView) v.findViewById(R.id.txt_index);
            this.txt_Activites = (TextView) v.findViewById(R.id.txt_activities);
            this.chk_Results = new RadioButton[]{(RadioButton) v.findViewById(R.id.chk_na), (RadioButton) v.findViewById(R.id.chk_pass), (RadioButton) v.findViewById(R.id.chk_fail)};
            for (int i = 0; i < this.chk_Results.length; i++) {
                this.chk_Results[i].setOnCheckedChangeListener(this);
            }
        }

        /* access modifiers changed from: package-private */
        public void showValue(int index, VisualItem data) {
            this.m_Index = index;
            this.txt_Activites.setText(data.Activities);
            this.chk_Results[((int) data.Result.l_Value) + 1].setChecked(true);
            if (1 == data.RequiredField && 1 == VisualItemAdapter.this.m_MustDo) {
                this.txt_Index.setText(String.valueOf(index + 1) + " *");
            } else {
                this.txt_Index.setText(new StringBuilder(String.valueOf(index + 1)).toString());
            }
        }

        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            switch (buttonView.getId()) {
                case R.id.chk_pass:
                    if (isChecked) {
                        VisualItemAdapter.this.m_Data.get(this.m_Index).Result.l_Value = 0;
                        break;
                    }
                    break;
                case R.id.chk_fail:
                    if (isChecked) {
                        VisualItemAdapter.this.m_Data.get(this.m_Index).Result.l_Value = 1;
                        break;
                    }
                    break;
                case R.id.chk_na:
                    if (isChecked) {
                        VisualItemAdapter.this.m_Data.get(this.m_Index).Result.l_Value = -1;
                        break;
                    }
                    break;
            }
            if (isChecked) {
                VisualItemAdapter.this.m_Data.get(this.m_Index).Result.s_Value = buttonView.getText().toString();
            }
        }
    }
}
