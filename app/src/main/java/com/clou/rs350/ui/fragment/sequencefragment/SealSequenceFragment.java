package com.clou.rs350.ui.fragment.sequencefragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.clou.rs350.R;
import com.clou.rs350.db.dao.sequence.SealSchemeDao;
import com.clou.rs350.db.model.sequence.SealData;
import com.clou.rs350.db.model.sequence.SealItem;
import com.clou.rs350.model.ClouData;
import com.clou.rs350.ui.adapter.SealItemAdapter;
import com.clou.rs350.ui.dialog.HintDialog;
import com.clou.rs350.ui.fragment.BaseFragment;
import com.clou.rs350.ui.popwindow.ListPopWindow;
import com.clou.rs350.utils.OtherUtils;
import com.itextpdf.text.pdf.PdfObject;
import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.view.annotation.ViewInject;

import java.util.List;

public class SealSequenceFragment extends BaseFragment implements View.OnClickListener {
    private static final String TAG = "SealRecordFragment";
    @ViewInject(R.id.list_seal)
    private ListView list_Seal;
    private SealItemAdapter m_Adapter;
    private SealData m_Data;
    @ViewInject(R.id.txt_scheme)
    private TextView txt_Scheme;
    @ViewInject(R.id.txt_seal_state)
    private TextView txt_Seal_State;

    public SealSequenceFragment(SealData data) {
        this.m_Data = data;
    }

    @Override // android.support.v4.app.Fragment
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        this.fragmentView = inflater.inflate(R.layout.fragment_sequence_seal, (ViewGroup) null);
        ViewUtils.inject(this, this.fragmentView);
        this.fragmentView.findViewById(R.id.txt_scheme).setOnClickListener(this);
        return this.fragmentView;
    }

    @Override // android.support.v4.app.Fragment, com.clou.rs350.ui.fragment.BaseFragment
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        this.m_Adapter = new SealItemAdapter(this.m_Context, true, this.m_MustDo);
        this.list_Seal.setAdapter((ListAdapter) this.m_Adapter);
        this.txt_Seal_State.setText(this.m_Data.Index == 0 ? R.string.text_seal_beforetest : R.string.text_seal_aftertest);
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.txt_scheme:
                List<String> tmpNames = new SealSchemeDao(this.m_Context).queryAllKey(ClouData.getInstance(this.m_Context).getSettingBasic().Language);
                ListPopWindow.showListPopwindow(this.m_Context, (String[]) tmpNames.toArray(new String[tmpNames.size()]), v, new View.OnClickListener() {
                    /* class com.clou.rs350.ui.fragment.sequencefragment.SealSequenceFragment.AnonymousClass1 */

                    public void onClick(View v) {
                        SealSequenceFragment.this.refreshScheme(((TextView) v).getText().toString());
                    }
                });
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private void refreshScheme(String scheme) {
        SealData tmpScheme = (SealData) new SealSchemeDao(this.m_Context).queryByKey(scheme);
        this.m_Data.SchemeName = tmpScheme.SchemeName;
        this.m_Data.Seals = tmpScheme.Seals;
        this.m_Adapter.refresh(this.m_Data.Seals);
    }

    private void showData() {
        if (!OtherUtils.isEmpty(this.m_Data.SchemeName) || OtherUtils.isEmpty(ClouData.getInstance().getSealScheme())) {
            this.m_Adapter.refresh(this.m_Data.Seals);
        } else {
            this.m_Data.SchemeName = ClouData.getInstance().getSealScheme();
            refreshScheme(this.m_Data.SchemeName);
        }
        this.txt_Scheme.setText(this.m_Data.SchemeName);
    }

    @Override // android.support.v4.app.Fragment, com.clou.rs350.ui.fragment.BaseFragment
    public void onResume() {
        showData();
        super.onResume();
    }

    private void parseData() {
        this.m_Data.SchemeName = this.txt_Scheme.getText().toString();
        this.m_Data.Seals = this.m_Adapter.getData();
        ClouData.getInstance().setSealScheme(this.m_Data.SchemeName);
    }

    /* access modifiers changed from: protected */
    @Override // com.clou.rs350.ui.fragment.BaseFragment
    public void copyData() {
        parseData();
    }

    @Override // android.support.v4.app.Fragment, com.clou.rs350.ui.fragment.BaseFragment
    public void onPause() {
        super.onPause();
    }

    @Override // com.clou.rs350.ui.fragment.BaseFragment
    public int isFinish() {
        int result = 0;
        if (1 != this.m_MustDo) {
            return 0;
        }
        String strHint = PdfObject.NOTHING;
        parseData();
        if (OtherUtils.isEmpty(this.m_Data.SchemeName)) {
            new HintDialog(this.m_Context, (int) R.string.text_select_scheme).show();
            return 1;
        }
        for (int i = 0; i < this.m_Data.Seals.size(); i++) {
            SealItem tmpItem = this.m_Data.Seals.get(i);
            if (1 == tmpItem.RequiredField && OtherUtils.isEmpty(tmpItem.SealSr)) {
                result = 1;
                strHint = String.valueOf(strHint) + tmpItem.SealLocation + " " + this.m_Context.getResources().getString(R.string.text_is_empty) + "\r\n";
            }
        }
        if (result == 0) {
            return result;
        }
        new HintDialog(this.m_Context, strHint.substring(0, strHint.length() - 2)).show();
        return result;
    }
}
