package com.clou.rs350.ui.fragment.errorfragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.clou.rs350.Preferences;
import com.clou.rs350.R;
import com.clou.rs350.db.model.EnergyTest;
import com.clou.rs350.device.MeterBaseDevice;
import com.clou.rs350.manager.MessageManager;
import com.clou.rs350.model.ClouData;
import com.clou.rs350.task.ISleepCallback;
import com.clou.rs350.task.TimerThread;
import com.clou.rs350.ui.activity.BaseActivity;
import com.clou.rs350.ui.dialog.HintDialog;
import com.clou.rs350.ui.popwindow.EnergyConfigurePopWindow;
import com.clou.rs350.ui.popwindow.TrxRatioPopWindow;
import com.clou.rs350.utils.OtherUtils;
import com.itextpdf.text.pdf.PdfObject;
import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.view.annotation.ViewInject;
import com.lidroid.xutils.view.annotation.event.OnClick;

public class EnergyTestingFragment extends BaseErrorFragment implements View.OnClickListener {
    @ViewInject(R.id.active_end)
    private TextView ActiveEndTv;
    @ViewInject(R.id.active_end_unit)
    private TextView ActiveEndUnitTv;
    @ViewInject(R.id.active_error)
    private TextView ActiveErrorTv;
    @ViewInject(R.id.active_initial)
    private TextView ActiveInitialTv;
    @ViewInject(R.id.active_initial_unit)
    private TextView ActiveInitialUnitTv;
    @ViewInject(R.id.active_ref)
    private TextView ActiveRefTv;
    @ViewInject(R.id.apparent_end)
    private TextView ApparentEndTv;
    @ViewInject(R.id.apparent_end_unit)
    private TextView ApparentEndUnitTv;
    @ViewInject(R.id.apparent_error)
    private TextView ApparentErrorTv;
    @ViewInject(R.id.apparent_initial)
    private TextView ApparentInitialTv;
    @ViewInject(R.id.apparent_initial_unit)
    private TextView ApparentInitialUnitTv;
    @ViewInject(R.id.apparent_ref)
    private TextView ApparentRefTv;
    @ViewInject(R.id.meter1_end)
    private TextView Meter1EndTv;
    @ViewInject(R.id.meter1_end_unit)
    private TextView Meter1EndUnitTv;
    @ViewInject(R.id.meter1_error)
    private TextView Meter1ErrorTv;
    @ViewInject(R.id.meter1_initial)
    private TextView Meter1InitialTv;
    @ViewInject(R.id.meter1_initial_unit)
    private TextView Meter1InitialUnitTv;
    @ViewInject(R.id.meter1_pulse)
    private TextView Meter1PulseTv;
    @ViewInject(R.id.meter1_ref)
    private TextView Meter1RefTv;
    @ViewInject(R.id.meter2_end)
    private TextView Meter2EndTv;
    @ViewInject(R.id.meter2_end_unit)
    private TextView Meter2EndUnitTv;
    @ViewInject(R.id.meter2_error)
    private TextView Meter2ErrorTv;
    @ViewInject(R.id.meter2_initial)
    private TextView Meter2InitialTv;
    @ViewInject(R.id.meter2_initial_unit)
    private TextView Meter2InitialUnitTv;
    @ViewInject(R.id.meter2_pulse)
    private TextView Meter2PulseTv;
    @ViewInject(R.id.meter2_ref)
    private TextView Meter2RefTv;
    @ViewInject(R.id.meter3_end)
    private TextView Meter3EndTv;
    @ViewInject(R.id.meter3_end_unit)
    private TextView Meter3EndUnitTv;
    @ViewInject(R.id.meter3_error)
    private TextView Meter3ErrorTv;
    @ViewInject(R.id.meter3_initial)
    private TextView Meter3InitialTv;
    @ViewInject(R.id.meter3_initial_unit)
    private TextView Meter3InitialUnitTv;
    @ViewInject(R.id.meter3_pulse)
    private TextView Meter3PulseTv;
    @ViewInject(R.id.meter3_ref)
    private TextView Meter3RefTv;
    @ViewInject(R.id.reactive_end)
    private TextView ReactiveEndTv;
    @ViewInject(R.id.reactive_end_unit)
    private TextView ReactiveEndUnitTv;
    @ViewInject(R.id.reactive_error)
    private TextView ReactiveErrorTv;
    @ViewInject(R.id.reactive_initial)
    private TextView ReactiveInitialTv;
    @ViewInject(R.id.reactive_initial_unit)
    private TextView ReactiveInitialUnitTv;
    @ViewInject(R.id.reactive_ref)
    private TextView ReactiveRefTv;
    @ViewInject(R.id.btn_configure)
    private Button configureBtn;
    private boolean hasStart;
    @ViewInject(R.id.energy_register_testing_active)
    private LinearLayout layout1Meter;
    private EnergyTest[] m_EnergyTest;
    private int m_PQ_Flag = 0;
    private long m_StartTime;
    private int m_TestType = 0;
    @ViewInject(R.id.btn_txr_ratio)
    private Button ratioBtn;
    private TimerThread readDataThread;
    @ViewInject(R.id.btn_start)
    private Button startBtn;
    private boolean[] testMeter;
    @ViewInject(R.id.txt_test_time)
    private TextView txt_TestTime;

    public EnergyTestingFragment(EnergyTest[] Data) {
        boolean[] zArr = new boolean[3];
        zArr[0] = true;
        this.testMeter = zArr;
        this.hasStart = false;
        this.m_EnergyTest = Data;
    }

    public EnergyTestingFragment() {
        boolean[] zArr = new boolean[3];
        zArr[0] = true;
        this.testMeter = zArr;
        this.hasStart = false;
        this.m_EnergyTest = new EnergyTest[0];
    }

    @Override // android.support.v4.app.Fragment
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        this.fragmentView = inflater.inflate(R.layout.fragment_energy_register_testing, (ViewGroup) null);
        ViewUtils.inject(this, this.fragmentView);
        return this.fragmentView;
    }

    private void initData() {
        if (this.messageManager.getMeter().getStartStateValue() == 0) {
            this.messageManager.setFunc_Mstate(35);
        }
        int refreshTime = Preferences.getInt(Preferences.Key.REFRESHTIME, 1000);
        if (this.readDataThread == null || this.readDataThread.isStop()) {
            this.readDataThread = new TimerThread((long) refreshTime, new ISleepCallback() {
                /* class com.clou.rs350.ui.fragment.errorfragment.EnergyTestingFragment.AnonymousClass1 */

                @Override // com.clou.rs350.task.ISleepCallback
                public void onSleepAfterToDo() {
                    if (EnergyTestingFragment.this.hasStart) {
                        EnergyTestingFragment.this.messageManager.getEnergy();
                    }
                    if (1 == EnergyTestingFragment.this.m_TestType) {
                        EnergyTestingFragment.this.messageManager.getStartState();
                    }
                }
            }, "Get Data Thread");
            this.readDataThread.start();
        }
    }

    private void killThread() {
        if (this.readDataThread != null && !this.readDataThread.isStop()) {
            this.readDataThread.stopTimer();
            this.readDataThread.interrupt();
            this.readDataThread = null;
        }
    }

    private void updateAccuracy() {
        String tmpString = ClouData.getInstance().getMeterBaseInfo()[0].ActiveAccuracy.trim();
        if (!OtherUtils.isEmpty(tmpString)) {
            this.m_EnergyTest[0].Accuracy[0] = OtherUtils.parseDouble(tmpString);
        }
        String tmpString2 = ClouData.getInstance().getMeterBaseInfo()[0].ReactiveAccuracy.trim();
        if (!OtherUtils.isEmpty(tmpString2)) {
            this.m_EnergyTest[0].Accuracy[1] = OtherUtils.parseDouble(tmpString2);
        }
        String tmpString3 = ClouData.getInstance().getMeterBaseInfo()[0].ApparentAccuracy.trim();
        if (!OtherUtils.isEmpty(tmpString3)) {
            this.m_EnergyTest[0].Accuracy[2] = OtherUtils.parseDouble(tmpString3);
        }
    }

    private void updateAccuracys(int meterIndex) {
        String tmpString = PdfObject.NOTHING;
        switch (this.m_PQ_Flag) {
            case 0:
                tmpString = ClouData.getInstance().getMeterBaseInfo()[meterIndex].ActiveAccuracy.trim();
                break;
            case 1:
                tmpString = ClouData.getInstance().getMeterBaseInfo()[meterIndex].ReactiveAccuracy.trim();
                break;
            case 2:
                tmpString = ClouData.getInstance().getMeterBaseInfo()[meterIndex].ApparentAccuracy.trim();
                break;
        }
        if (!OtherUtils.isEmpty(tmpString)) {
            this.m_EnergyTest[meterIndex].Accuracy[this.m_PQ_Flag] = OtherUtils.parseDouble(tmpString);
        }
    }

    @Override // com.clou.rs350.ui.fragment.BaseFragment
    public void refreshData() {
        this.m_TestType = Preferences.getInt(Preferences.Key.ENERGYTESTMODE, 0);
        this.messageManager.setTestMode(this.m_TestType);
        int tmpValue = ClouData.getInstance().getTestMeterCount();
        if (1 == tmpValue) {
            refresh1MeterUnit();
            updateAccuracy();
            findViewById(R.id.energy_register_testing_meter).setVisibility(View.GONE);
            findViewById(R.id.energy_register_testing_active).setVisibility(View.VISIBLE);
        } else {
            refresh3MeterUnit(this.m_PQ_Flag);
            updateAccuracys(0);
            updateAccuracys(1);
            updateAccuracys(2);
            findViewById(R.id.energy_register_testing_meter).setVisibility(View.VISIBLE);
            findViewById(R.id.energy_register_testing_active).setVisibility(View.GONE);
        }
        for (int i = 0; i < 3; i++) {
            this.testMeter[i] = false;
            if ((tmpValue & 1) == 1) {
                this.testMeter[i] = true;
            }
            tmpValue >>= 1;
        }
        if (this.layout1Meter.getVisibility() == View.VISIBLE) {
            refreshInput(this.m_EnergyTest[0]);
            showData(this.m_EnergyTest[0]);
            return;
        }
        refreshInput(this.m_EnergyTest);
        showData(this.m_EnergyTest);
    }

    private void refresh1MeterUnit() {
        int tmpUnit = this.m_EnergyTest[0].Unit;
        String sUnit = PdfObject.NOTHING;
        switch (tmpUnit) {
            case 1:
                sUnit = "k" + sUnit;
                break;
            case 2:
                sUnit = "M" + sUnit;
                break;
        }
        this.ActiveInitialUnitTv.setText(String.valueOf(sUnit) + "Wh");
        this.ReactiveInitialUnitTv.setText(String.valueOf(sUnit) + "varh");
        this.ApparentInitialUnitTv.setText(String.valueOf(sUnit) + "VAh");
        this.ActiveEndUnitTv.setText(String.valueOf(sUnit) + "Wh");
        this.ReactiveEndUnitTv.setText(String.valueOf(sUnit) + "varh");
        this.ApparentEndUnitTv.setText(String.valueOf(sUnit) + "VAh");
    }

    private void refresh3MeterUnit(int PQFlag) {
        int tmpUnit = this.m_EnergyTest[0].Unit;
        String sUnit = OtherUtils.parseUnitByWiring(PQFlag, "Wh");
        switch (tmpUnit) {
            case 1:
                sUnit = "k" + sUnit;
                break;
            case 2:
                sUnit = "M" + sUnit;
                break;
        }
        this.Meter1InitialUnitTv.setText(sUnit);
        this.Meter2InitialUnitTv.setText(sUnit);
        this.Meter3InitialUnitTv.setText(sUnit);
        this.Meter1EndUnitTv.setText(sUnit);
        this.Meter2EndUnitTv.setText(sUnit);
        this.Meter3EndUnitTv.setText(sUnit);
    }

    @Override // android.support.v4.app.Fragment, com.clou.rs350.ui.fragment.BaseFragment
    public void onResume() {
        refreshData();
        initData();
        super.onResume();
    }

    @Override // android.support.v4.app.Fragment, com.clou.rs350.ui.fragment.BaseFragment
    public void onPause() {
        killThread();
        super.onPause();
    }

    @OnClick({R.id.btn_configure, R.id.btn_txr_ratio, R.id.btn_start, R.id.active_initial, R.id.reactive_initial, R.id.apparent_initial, R.id.active_end, R.id.reactive_end, R.id.apparent_end, R.id.meter1_initial, R.id.meter1_end, R.id.meter2_initial, R.id.meter2_end, R.id.meter3_initial, R.id.meter3_end})
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_start:
                if (!this.hasStart) {
                    start();
                    return;
                } else {
                    stop();
                    return;
                }
            case R.id.btn_configure:
                new EnergyConfigurePopWindow(this.m_Context).showPopWindow(v, MessageManager.getInstance().getMeter().getPQFlagValue(), this.m_EnergyTest, new View.OnClickListener() {
                    /* class com.clou.rs350.ui.fragment.errorfragment.EnergyTestingFragment.AnonymousClass2 */

                    public void onClick(View v) {
                        EnergyTestingFragment.this.refreshData();
                    }
                });
                return;
            case R.id.btn_txr_ratio:
                new TrxRatioPopWindow(this.m_Context).showRatioPopWindow(v, ClouData.getInstance().getSiteData(), null);
                return;
            case R.id.meter1_initial:
            case R.id.meter1_end:
                showInputDialog((TextView) v, (View.OnClickListener) new View.OnClickListener() {
                    /* class com.clou.rs350.ui.fragment.errorfragment.EnergyTestingFragment.AnonymousClass6 */

                    public void onClick(View view) {
                        EnergyTestingFragment.this.m_EnergyTest[0].setEnergyValue(EnergyTestingFragment.this.m_PQ_Flag, OtherUtils.parseDouble(EnergyTestingFragment.this.Meter1InitialTv.getText().toString().trim()), OtherUtils.parseDouble(EnergyTestingFragment.this.Meter1EndTv.getText().toString().trim()));
                        EnergyTestingFragment.this.refreshInput(EnergyTestingFragment.this.m_EnergyTest);
                        EnergyTestingFragment.this.showData(EnergyTestingFragment.this.m_EnergyTest);
                    }
                }, false);
                return;
            case R.id.meter2_initial:
            case R.id.meter2_end:
                showInputDialog((TextView) v, (View.OnClickListener) new View.OnClickListener() {
                    /* class com.clou.rs350.ui.fragment.errorfragment.EnergyTestingFragment.AnonymousClass7 */

                    public void onClick(View view) {
                        EnergyTestingFragment.this.m_EnergyTest[1].setEnergyValue(EnergyTestingFragment.this.m_PQ_Flag, OtherUtils.parseDouble(EnergyTestingFragment.this.Meter2InitialTv.getText().toString().trim()), OtherUtils.parseDouble(EnergyTestingFragment.this.Meter2EndTv.getText().toString().trim()));
                        EnergyTestingFragment.this.refreshInput(EnergyTestingFragment.this.m_EnergyTest);
                        EnergyTestingFragment.this.showData(EnergyTestingFragment.this.m_EnergyTest);
                    }
                }, false);
                return;
            case R.id.meter3_initial:
            case R.id.meter3_end:
                showInputDialog((TextView) v, (View.OnClickListener) new View.OnClickListener() {
                    /* class com.clou.rs350.ui.fragment.errorfragment.EnergyTestingFragment.AnonymousClass8 */

                    public void onClick(View view) {
                        EnergyTestingFragment.this.m_EnergyTest[2].setEnergyValue(EnergyTestingFragment.this.m_PQ_Flag, OtherUtils.parseDouble(EnergyTestingFragment.this.Meter3InitialTv.getText().toString().trim()), OtherUtils.parseDouble(EnergyTestingFragment.this.Meter3EndTv.getText().toString().trim()));
                        EnergyTestingFragment.this.refreshInput(EnergyTestingFragment.this.m_EnergyTest);
                        EnergyTestingFragment.this.showData(EnergyTestingFragment.this.m_EnergyTest);
                    }
                }, false);
                return;
            case R.id.active_initial:
            case R.id.active_end:
                showInputDialog((TextView) v, (View.OnClickListener) new View.OnClickListener() {
                    /* class com.clou.rs350.ui.fragment.errorfragment.EnergyTestingFragment.AnonymousClass3 */

                    public void onClick(View view) {
                        EnergyTestingFragment.this.m_EnergyTest[0].setEnergyValue(0, OtherUtils.parseDouble(EnergyTestingFragment.this.ActiveInitialTv.getText().toString().trim()), OtherUtils.parseDouble(EnergyTestingFragment.this.ActiveEndTv.getText().toString().trim()));
                        EnergyTestingFragment.this.refreshInput(EnergyTestingFragment.this.m_EnergyTest[0]);
                        EnergyTestingFragment.this.showData(EnergyTestingFragment.this.m_EnergyTest[0]);
                    }
                }, false);
                return;
            case R.id.reactive_initial:
            case R.id.reactive_end:
                showInputDialog((TextView) v, (View.OnClickListener) new View.OnClickListener() {
                    /* class com.clou.rs350.ui.fragment.errorfragment.EnergyTestingFragment.AnonymousClass4 */

                    public void onClick(View view) {
                        EnergyTestingFragment.this.m_EnergyTest[0].setEnergyValue(1, OtherUtils.parseDouble(EnergyTestingFragment.this.ReactiveInitialTv.getText().toString().trim()), OtherUtils.parseDouble(EnergyTestingFragment.this.ReactiveEndTv.getText().toString().trim()));
                        EnergyTestingFragment.this.refreshInput(EnergyTestingFragment.this.m_EnergyTest[0]);
                        EnergyTestingFragment.this.showData(EnergyTestingFragment.this.m_EnergyTest[0]);
                    }
                }, false);
                return;
            case R.id.apparent_initial:
            case R.id.apparent_end:
                showInputDialog((TextView) v, (View.OnClickListener) new View.OnClickListener() {
                    /* class com.clou.rs350.ui.fragment.errorfragment.EnergyTestingFragment.AnonymousClass5 */

                    public void onClick(View view) {
                        EnergyTestingFragment.this.m_EnergyTest[0].setEnergyValue(2, OtherUtils.parseDouble(EnergyTestingFragment.this.ApparentInitialTv.getText().toString().trim()), OtherUtils.parseDouble(EnergyTestingFragment.this.ApparentEndTv.getText().toString().trim()));
                        EnergyTestingFragment.this.refreshInput( EnergyTestingFragment.this.m_EnergyTest[0]);
                        EnergyTestingFragment.this.showData(EnergyTestingFragment.this.m_EnergyTest[0]);
                    }
                }, false);
                return;
            default:
                return;
        }
    }

    private void setButton(int flag) {
        this.startBtn.setText(getString(flag == 0 ? R.string.text_start : R.string.text_stop));
        boolean enabled = flag == 0;
        this.ratioBtn.setEnabled(enabled);
        this.configureBtn.setEnabled(enabled);
        if (this.m_Context instanceof BaseActivity) {
            ((BaseActivity) this.m_Context).startOrStopChangeViewStatus(flag);
        }
    }

    private void cleanEndEnergy() {
        if (this.layout1Meter.getVisibility() == View.VISIBLE) {
            this.m_EnergyTest[0].setEnergyValue(0, OtherUtils.parseDouble(this.ActiveInitialTv.getText().toString().trim()), 0.0d);
            this.m_EnergyTest[0].setEnergyValue(1, OtherUtils.parseDouble(this.ReactiveInitialTv.getText().toString().trim()), 0.0d);
            this.m_EnergyTest[0].setEnergyValue(2, OtherUtils.parseDouble(this.ApparentInitialTv.getText().toString().trim()), 0.0d);
            refreshInput(this.m_EnergyTest[0]);
            showData(this.m_EnergyTest[0]);
            this.ActiveEndTv.setText(PdfObject.NOTHING);
            this.ReactiveEndTv.setText(PdfObject.NOTHING);
            this.ApparentEndTv.setText(PdfObject.NOTHING);
            return;
        }
        this.m_EnergyTest[0].setEnergyValue(this.m_PQ_Flag, OtherUtils.parseDouble(this.Meter1InitialTv.getText().toString().trim()), 0.0d);
        this.m_EnergyTest[1].setEnergyValue(this.m_PQ_Flag, OtherUtils.parseDouble(this.Meter2InitialTv.getText().toString().trim()), 0.0d);
        this.m_EnergyTest[2].setEnergyValue(this.m_PQ_Flag, OtherUtils.parseDouble(this.Meter3InitialTv.getText().toString().trim()), 0.0d);
        refreshInput(this.m_EnergyTest);
        showData(this.m_EnergyTest);
        this.Meter1EndTv.setText(PdfObject.NOTHING);
        this.Meter2EndTv.setText(PdfObject.NOTHING);
        this.Meter3EndTv.setText(PdfObject.NOTHING);
    }

    private void showTime(long millis) {
        this.txt_TestTime.setText(OtherUtils.getTime(millis / 1000));
    }

    @Override // com.clou.rs350.ui.fragment.errorfragment.BaseErrorFragment
    public boolean start() {
        this.messageManager.setFunc_Mstate_Start(35, 1);
        this.m_StartTime = System.currentTimeMillis();
        cleanEndEnergy();
        this.m_EnergyTest[0].TestStartTime = System.currentTimeMillis();
        this.m_EnergyTest[0].RunningTime = 0;
        showTime(0);
        setButton(1);
        this.hasStart = true;
        return true;
    }

    @Override // com.clou.rs350.ui.fragment.errorfragment.BaseErrorFragment
    public void doContinue() {
        initData();
        this.hasStart = true;
    }

    @Override // com.clou.rs350.ui.fragment.errorfragment.BaseErrorFragment
    public boolean stop() {
        setButton(0);
        this.messageManager.setStart(0);
        this.messageManager.getEnergy();
        this.m_EnergyTest[0].TestEndTime = System.currentTimeMillis();
        this.m_EnergyTest[0].RunningTime = this.m_EnergyTest[0].TestEndTime - this.m_EnergyTest[0].TestStartTime;
        this.hasStart = false;
        return true;
    }

    @Override // android.support.v4.app.Fragment, com.clou.rs350.ui.fragment.BaseFragment
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initView();
    }

    private void initView() {
    }

    @Override // com.clou.rs350.ui.fragment.BaseFragment, com.clou.rs350.manager.MessageManager.IMessageListener
    public void onReceiveMessage(MeterBaseDevice Device) {
        boolean z = true;
        int tmpPQ_Flag = Device.getPQFlagValue();
        if (this.m_PQ_Flag != tmpPQ_Flag) {
            this.m_PQ_Flag = tmpPQ_Flag;
            refresh3MeterUnit(this.m_PQ_Flag);
            updateAccuracys(0);
            updateAccuracys(1);
            updateAccuracys(2);
            showData(this.m_EnergyTest);
        }
        if (this.hasStart) {
            this.m_EnergyTest[0].RunningTime = System.currentTimeMillis() - this.m_EnergyTest[0].TestStartTime;
        }
        if (this.layout1Meter.getVisibility() == View.VISIBLE) {
            this.m_EnergyTest[0].parseStandardEnergy(Device, true);
            showData(this.m_EnergyTest[0]);
        } else {
            for (int i = 0; i < 3; i++) {
                if (this.testMeter[i]) {
                    this.m_EnergyTest[i].parseStandardEnergy(Device, false);
                }
            }
            showData(this.m_EnergyTest);
        }
        boolean z2 = this.hasStart;
        if (1 != Device.getStartStateValue()) {
            z = false;
        }
        if (!(z ^ z2)) {
            return;
        }
        if (this.hasStart) {
            stop();
        } else {
            start();
        }
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private void refreshInput(EnergyTest energyMeasurement) {
        this.ActiveInitialTv.setText(new StringBuilder(String.valueOf(energyMeasurement.BeginEnergy[0].d_Value)).toString());
        this.ReactiveInitialTv.setText(new StringBuilder(String.valueOf(energyMeasurement.BeginEnergy[1].d_Value)).toString());
        this.ApparentInitialTv.setText(new StringBuilder(String.valueOf(energyMeasurement.BeginEnergy[2].d_Value)).toString());
        this.ActiveEndTv.setText(new StringBuilder(String.valueOf(energyMeasurement.EndEnergy[0].d_Value)).toString());
        this.ReactiveEndTv.setText(new StringBuilder(String.valueOf(energyMeasurement.EndEnergy[1].d_Value)).toString());
        this.ApparentEndTv.setText(new StringBuilder(String.valueOf(energyMeasurement.EndEnergy[2].d_Value)).toString());
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private void showData(EnergyTest energyMeasurement) {
        this.ActiveRefTv.setText(energyMeasurement.StandardEnergy[0].s_Value);
        this.ReactiveRefTv.setText(energyMeasurement.StandardEnergy[1].s_Value);
        this.ApparentRefTv.setText(energyMeasurement.StandardEnergy[2].s_Value);
        this.ActiveErrorTv.setText(energyMeasurement.Error[0].s_Value);
        this.ReactiveErrorTv.setText(energyMeasurement.Error[1].s_Value);
        this.ApparentErrorTv.setText(energyMeasurement.Error[2].s_Value);
        this.ActiveErrorTv.setTextColor(OtherUtils.getColor((int) energyMeasurement.Result[0].l_Value));
        this.ReactiveErrorTv.setTextColor(OtherUtils.getColor((int) energyMeasurement.Result[1].l_Value));
        this.ApparentErrorTv.setTextColor(OtherUtils.getColor((int) energyMeasurement.Result[2].l_Value));
        showTime(this.m_EnergyTest[0].RunningTime);
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private void refreshInput(EnergyTest[] energyMeasurement) {
        this.Meter1InitialTv.setText(new StringBuilder(String.valueOf(energyMeasurement[0].BeginEnergy[this.m_PQ_Flag].d_Value)).toString());
        this.Meter2InitialTv.setText(new StringBuilder(String.valueOf(energyMeasurement[1].BeginEnergy[this.m_PQ_Flag].d_Value)).toString());
        this.Meter3InitialTv.setText(new StringBuilder(String.valueOf(energyMeasurement[2].BeginEnergy[this.m_PQ_Flag].d_Value)).toString());
        this.Meter1EndTv.setText(new StringBuilder(String.valueOf(energyMeasurement[0].EndEnergy[this.m_PQ_Flag].d_Value)).toString());
        this.Meter2EndTv.setText(new StringBuilder(String.valueOf(energyMeasurement[1].EndEnergy[this.m_PQ_Flag].d_Value)).toString());
        this.Meter3EndTv.setText(new StringBuilder(String.valueOf(energyMeasurement[2].EndEnergy[this.m_PQ_Flag].d_Value)).toString());
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private void showData(EnergyTest[] energyMeasurement) {
        this.Meter1RefTv.setText(energyMeasurement[0].StandardEnergy[this.m_PQ_Flag].s_Value);
        this.Meter2RefTv.setText(energyMeasurement[1].StandardEnergy[this.m_PQ_Flag].s_Value);
        this.Meter3RefTv.setText(energyMeasurement[2].StandardEnergy[this.m_PQ_Flag].s_Value);
        this.Meter1ErrorTv.setText(energyMeasurement[0].Error[this.m_PQ_Flag].s_Value);
        this.Meter2ErrorTv.setText(energyMeasurement[1].Error[this.m_PQ_Flag].s_Value);
        this.Meter3ErrorTv.setText(energyMeasurement[2].Error[this.m_PQ_Flag].s_Value);
        this.Meter1ErrorTv.setTextColor(OtherUtils.getColor((int) energyMeasurement[0].Result[this.m_PQ_Flag].l_Value));
        this.Meter2ErrorTv.setTextColor(OtherUtils.getColor((int) energyMeasurement[1].Result[this.m_PQ_Flag].l_Value));
        this.Meter3ErrorTv.setTextColor(OtherUtils.getColor((int) energyMeasurement[2].Result[this.m_PQ_Flag].l_Value));
        showTime(this.m_EnergyTest[0].RunningTime);
    }

    @Override // com.clou.rs350.ui.fragment.BaseFragment
    public int isFinish() {
        int result = 0;
        for (int i = 0; i < 3; i++) {
            if (this.testMeter[i] && this.m_EnergyTest[i].isEmpty()) {
                result = 1;
                new HintDialog(this.m_Context, (int) R.string.text_do_test).show();
            }
        }
        return result;
    }
}
