package com.clou.rs350.ui.dialog;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import com.clou.rs350.Preferences;
import com.clou.rs350.R;
import com.clou.rs350.callback.ILoadCallback;
import com.clou.rs350.constants.Constants;
import com.clou.rs350.db.dao.CustomerInfoDao;
import com.clou.rs350.db.dao.MeterTypeDao;
import com.clou.rs350.db.dao.SiteDataManagerDao;
import com.clou.rs350.db.dao.TestItemDao;
import com.clou.rs350.db.model.CustomerInfo;
import com.clou.rs350.db.model.MeterType;
import com.clou.rs350.db.model.SiteData;
import com.clou.rs350.model.ClouData;
import com.clou.rs350.task.MyTask;
import com.clou.rs350.ui.popwindow.ListPopWindow;
import com.clou.rs350.utils.DoubleClick;
import com.clou.rs350.utils.InfoUtil;
import com.clou.rs350.utils.OtherUtils;
import com.itextpdf.text.pdf.PdfObject;

import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.List;

import google.zxing.camera.CaptureDialog;

public class SaveDialog extends BaseSaveDialog implements View.OnClickListener {
    private TextView CustomerSerialNoTextView = null;
    private IAfterSave m_AfterSave;
    private ISave m_SaveData;
    private String m_TestItem = PdfObject.NOTHING;

    public interface IAfterSave {
        void AfterSave(boolean z);
    }

    public interface ISave {
        boolean SaveMeasureData(String str, String str2);
    }

    public SaveDialog(Context context, String testItem, ISave SaveData, IAfterSave AfterSave) {
        super(context);
        this.m_TestItem = testItem;
        this.m_SaveData = SaveData;
        this.m_AfterSave = AfterSave;
    }

    /* access modifiers changed from: protected */
    @Override // com.clou.rs350.ui.dialog.BaseSaveDialog
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_save);
        setCanceledOnTouchOutside(true);
        WindowManager.LayoutParams lp = getWindow().getAttributes();
        lp.width = getContext().getResources().getDisplayMetrics().widthPixels;
        lp.height = getContext().getResources().getDisplayMetrics().heightPixels;
        getWindow().setAttributes(lp);
        initView();
    }

    private void initView() {
        findViewById(R.id.btn_scan).setOnClickListener(this);
        findViewById(R.id.btn_select).setOnClickListener(this);
        this.SaveBtn = (Button) findViewById(R.id.btn_save);
        this.SaveBtn.setOnClickListener(this);
        this.SaveBtn.setFocusable(true);
        this.CancelBtn = (Button) findViewById(R.id.btn_cancel);
        this.CancelBtn.setOnClickListener(this);
        this.CustomerSerialNoTextView = (TextView) findViewById(R.id.txt_customer_sr);
        this.CustomerSerialNoTextView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            /* class com.clou.rs350.ui.dialog.SaveDialog.AnonymousClass1 */

            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                SiteData tmpData;
                if ((actionId == 6 || actionId == 5) && (tmpData = (SiteData) new SiteDataManagerDao(SaveDialog.this.m_Context).queryObjectBySerialNo(SaveDialog.this.CustomerSerialNoTextView.getText().toString())) != null) {
                    int tmpCount = ClouData.getInstance().getTestMeterCount();
                    MeterTypeDao tmpTypeDao = new MeterTypeDao(SaveDialog.this.m_Context);
                    ClouData.getInstance().setSiteData(tmpData);
                    if (!OtherUtils.isEmpty(tmpData.MeterInfo[0].NominalVoltage)) {
                        Preferences.putFloat(Preferences.Key.VOLTAGENOMINAL, (float) OtherUtils.parseDouble(tmpData.MeterInfo[0].NominalVoltage.replace("V", PdfObject.NOTHING)));
                    }
                    for (int i = 0; i < tmpData.Meter_Count; i++) {
                        if (OtherUtils.isEmpty(tmpData.MeterInfo[i].SerialNo)) {
                            MeterType tmpType = tmpTypeDao.queryObjectByType(tmpData.MeterInfo[i].Type);
                            if (tmpType != null) {
                                tmpData.MeterInfo[i].CopyTypeData(tmpType);
                            }
                            tmpCount |= 1 << i;
                        }
                    }
                    ClouData.getInstance().setTestMeterCount(tmpCount);
                }
                Log.i("actionId", new StringBuilder(String.valueOf(actionId)).toString());
                return false;
            }
        });
        this.TemperatureTextView = (TextView) findViewById(R.id.txt_temperature);
        this.TemperatureTextView.setOnClickListener(this);
        this.RemarkTextView = (TextView) findViewById(R.id.txt_remark);
        this.HumidityTextView = (TextView) findViewById(R.id.txt_humidity);
        this.HumidityTextView.setOnClickListener(this);
        findViewById(R.id.btn_customer_detail).setOnClickListener(this);
        findViewById(R.id.btn_meter_detail).setOnClickListener(this);
        ClouData datas = ClouData.getInstance();
        this.CustomerSerialNoTextView.setText(datas.getCustomerInfo().CustomerSr);
        this.TemperatureTextView.setText(datas.getTestItem().Temperature.replace("℃", PdfObject.NOTHING));
        this.HumidityTextView.setText(datas.getTestItem().Humidity.replace("%", PdfObject.NOTHING));
        this.RemarkTextView.setText(datas.getTestItem().Remark);
        this.m_DoubleClick = new DoubleClick(new DoubleClick.OnDoubleClick() {
            /* class com.clou.rs350.ui.dialog.SaveDialog.AnonymousClass2 */

            private void searchKey(String condition) {
                List<String> tmpSerials = new CustomerInfoDao(SaveDialog.this.m_Context).queryAllKey(condition);
                ListPopWindow.showListPopwindow(SaveDialog.this.m_Context, (String[]) tmpSerials.toArray(new String[tmpSerials.size()]), SaveDialog.this.CustomerSerialNoTextView, new View.OnClickListener() {
                    /* class com.clou.rs350.ui.dialog.SaveDialog.AnonymousClass2.AnonymousClass1 */

                    public void onClick(View v) {
                        SaveDialog.this.parseData();
                    }
                });
            }

            @Override // com.clou.rs350.utils.DoubleClick.OnDoubleClick
            public void doubleClick() {
                searchKey(SaveDialog.this.CustomerSerialNoTextView.getText().toString());
            }

            @Override // com.clou.rs350.utils.DoubleClick.OnDoubleClick
            public void singleClick() {
                searchKey(PdfObject.NOTHING);
            }
        });
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private void parseData() {
        ClouData datas = ClouData.getInstance();
        String tmpData = this.CustomerSerialNoTextView.getText().toString();
        CustomerInfo customerInfo = new CustomerInfoDao(this.m_Context).queryObjectBySerialNo(tmpData);
        if (customerInfo != null) {
            ClouData.getInstance().setCustomerInfo(customerInfo);
        } else {
            ClouData.getInstance().getCustomerInfo().isNew = true;
        }
        SiteData siteData = ClouData.getInstance().getSiteData();
        if (OtherUtils.isEmpty(siteData.CustomerSr)) {
            siteData = (SiteData) new SiteDataManagerDao(this.m_Context).queryObjectBySerialNo(tmpData);
        } else {
            siteData.CustomerSr = tmpData;
        }
        if (siteData != null) {
            int tmpCount = 1;
            for (int i = 0; i < siteData.Meter_Count; i++) {
                tmpCount |= 1 << i;
            }
            ClouData.getInstance().setTestMeterCount(tmpCount);
            ClouData.getInstance().setSiteData(siteData);
            if (!OtherUtils.isEmpty(siteData.MeterInfo[0].NominalVoltage)) {
                Preferences.putFloat(Preferences.Key.VOLTAGENOMINAL, (float) OtherUtils.parseDouble(siteData.MeterInfo[0].NominalVoltage.replace("V", PdfObject.NOTHING)));
            }
        } else {
            SiteData siteData2 = new SiteData();
            siteData2.CustomerSr = tmpData;
            ClouData.getInstance().setSiteData(siteData2);
        }
        datas.getCustomerInfo().CustomerSr = tmpData;
        datas.getTestItem().CustomerSerialNo = tmpData;
        datas.getTestItem().Temperature = String.valueOf(this.TemperatureTextView.getText().toString()) + "℃";
        datas.getTestItem().Humidity = String.valueOf(this.HumidityTextView.getText().toString()) + "%";
        datas.getTestItem().Remark = this.RemarkTextView.getText().toString();
        datas.getTestItem().parseData(this.m_TestItem);
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_save:
                save();
                return;
            case R.id.btn_select:
                this.m_DoubleClick.click();
                return;
            case R.id.btn_cancel:
                dismiss();
                return;
            case R.id.btn_scan:
                CaptureDialog tmpDialog = new CaptureDialog(this.m_Context);
                tmpDialog.setScanResult(new CaptureDialog.ScanResult() {
                    /* class com.clou.rs350.ui.dialog.SaveDialog.AnonymousClass3 */

                    @Override // google.zxing.camera.CaptureDialog.ScanResult
                    public void getScanResult(String result) {
                        SaveDialog.this.CustomerSerialNoTextView.setText(result);
                        SaveDialog.this.parseData();
                    }
                });
                tmpDialog.show();
                return;
            case R.id.btn_customer_detail:
                if (validate(this.CustomerSerialNoTextView, R.string.text_customer_serial_no_is_empty)) {
                    parseData();
                    new CustomerInfoDialog(this.m_Context).show();
                    return;
                }
                return;
            case R.id.txt_temperature:
                NumericDialog numericDialog = new NumericDialog(this.m_Context);
                numericDialog.txtLimit = 5;
                numericDialog.setMax(60.0d);
                numericDialog.setMin(-40.0d);
                numericDialog.showMyDialog(this.TemperatureTextView);
                return;
            case R.id.txt_humidity:
                NumericDialog numericDialog2 = new NumericDialog(this.m_Context);
                numericDialog2.setMax(100.0d);
                numericDialog2.setMin(0.0d);
                numericDialog2.setMinus(false);
                numericDialog2.txtLimit = 4;
                numericDialog2.showMyDialog(this.HumidityTextView);
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: protected */
    @Override // com.clou.rs350.ui.dialog.BaseSaveDialog
    public void save() {
        if (validate(this.CustomerSerialNoTextView, R.string.text_customer_serial_no_is_empty)) {
            this.m_LoadingDialog = new LoadingDialog(this.m_Context);
            this.m_LoadingDialog.setCancelable(false);
            this.m_LoadingDialog.show();
            new MyTask(new ILoadCallback() {
                /* class com.clou.rs350.ui.dialog.SaveDialog.AnonymousClass4 */

                @Override // com.clou.rs350.callback.ILoadCallback
                public Object run() {
                    int Result = 1;
                    String Time = new SimpleDateFormat(Constants.TIMEFORMAT).format(new Date(System.currentTimeMillis()));
                    SaveDialog.this.parseData();
                    if (SaveDialog.this.m_SaveData != null && SaveDialog.this.m_SaveData.SaveMeasureData(ClouData.getInstance().getCustomerInfo().CustomerSr, Time)) {
                        Result = 0;
                        CustomerInfoDao tmpDao = new CustomerInfoDao(SaveDialog.this.m_Context);
                        if (!tmpDao.queryIfExistByKey(ClouData.getInstance().getCustomerInfo().CustomerSr)) {
                            tmpDao.insertObject(ClouData.getInstance().getCustomerInfo());
                        }
                        new TestItemDao(SaveDialog.this.m_Context).insertObject(ClouData.getInstance().getTestItem(), Time);
                    }
                    SaveDialog.this.Export(ClouData.getInstance().getCustomerInfo().CustomerSr, Time);
                    return Integer.valueOf(Result);
                }

                @Override // com.clou.rs350.callback.ILoadCallback
                public void callback(Object result) {
                    SaveDialog.this.m_LoadingDialog.dismiss();
                    if (((Number) result).intValue() == 0) {
                        SaveDialog.this.SaveBtn.setVisibility(4);
                        SaveDialog.this.isSave = true;
                        SaveDialog.this.CancelBtn.setText(R.string.text_close);
                        new HintDialog(SaveDialog.this.m_Context, (int) R.string.text_save_success).show();
                        if (SaveDialog.this.m_AfterSave != null) {
                            SaveDialog.this.m_AfterSave.AfterSave(true);
                            return;
                        }
                        return;
                    }
                    new HintDialog(SaveDialog.this.m_Context, (int) R.string.text_save_fail).show();
                }
            }).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, new Integer[0]);
        }
    }

    @Override // com.clou.rs350.ui.dialog.BaseSaveDialog
    public void dismiss() {
        parseData();
        if (this.isSave) {
            ClouData.getInstance().getTestItem().Remark = PdfObject.NOTHING;
        }
        InfoUtil.setRatio(ClouData.getInstance().getSiteData(), this.m_Context);
        super.dismiss();
    }
}
