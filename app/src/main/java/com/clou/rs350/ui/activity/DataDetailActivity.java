package com.clou.rs350.ui.activity;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.clou.rs350.CLApplication;
import com.clou.rs350.Preferences;
import com.clou.rs350.R;
import com.clou.rs350.callback.ILoadCallback;
import com.clou.rs350.db.dao.BaseDao;
import com.clou.rs350.db.dao.BasicMeasurementDao;
import com.clou.rs350.db.dao.CTBurdenDao;
import com.clou.rs350.db.dao.CTMeasurementDao;
import com.clou.rs350.db.dao.CameraDao;
import com.clou.rs350.db.dao.CustomerInfoDao;
import com.clou.rs350.db.dao.DailyTestDao;
import com.clou.rs350.db.dao.DemandTestDao;
import com.clou.rs350.db.dao.DigitalMeterTestDao;
import com.clou.rs350.db.dao.EnergyTestDao;
import com.clou.rs350.db.dao.ErrorTestDao;
import com.clou.rs350.db.dao.HarmonicMeasurementDao;
import com.clou.rs350.db.dao.LTMDao;
import com.clou.rs350.db.dao.PTBurdenDao;
import com.clou.rs350.db.dao.PTComparisonDao;
import com.clou.rs350.db.dao.ReadMeterDao;
import com.clou.rs350.db.dao.RegisterReadingDao;
import com.clou.rs350.db.dao.SealDao;
import com.clou.rs350.db.dao.SearchDao;
import com.clou.rs350.db.dao.SignatureDao;
import com.clou.rs350.db.dao.SiteDataTestDao;
import com.clou.rs350.db.dao.TestItemDao;
import com.clou.rs350.db.dao.VisualDao;
import com.clou.rs350.db.dao.WaveMeasurementDao;
import com.clou.rs350.db.dao.WiringCheckDao;
import com.clou.rs350.db.model.BasicMeasurement;
import com.clou.rs350.db.model.CTBurden;
import com.clou.rs350.db.model.CTMeasurement;
import com.clou.rs350.db.model.CustomerInfo;
import com.clou.rs350.db.model.DailyTest;
import com.clou.rs350.db.model.DataBaseRecordItem;
import com.clou.rs350.db.model.DemandTest;
import com.clou.rs350.db.model.DigitalMeterTest;
import com.clou.rs350.db.model.EnergyTest;
import com.clou.rs350.db.model.ErrorTest;
import com.clou.rs350.db.model.HarmonicMeasurement;
import com.clou.rs350.db.model.LTM;
import com.clou.rs350.db.model.PTBurden;
import com.clou.rs350.db.model.PTComparison;
import com.clou.rs350.db.model.ReadMeter;
import com.clou.rs350.db.model.SiteData;
import com.clou.rs350.db.model.TestItem;
import com.clou.rs350.db.model.WaveMeasurement;
import com.clou.rs350.db.model.WiringCheckMeasurement;
import com.clou.rs350.db.model.report.DataItem;
import com.clou.rs350.db.model.report.ReportDatas;
import com.clou.rs350.db.model.sequence.CameraData;
import com.clou.rs350.db.model.sequence.RegisterReadingData;
import com.clou.rs350.db.model.sequence.SealData;
import com.clou.rs350.db.model.sequence.SignatureData;
import com.clou.rs350.db.model.sequence.VisualData;
import com.clou.rs350.handler.HandlerSwitchView;
import com.clou.rs350.model.ClouData;
import com.clou.rs350.report.ExportReport;
import com.clou.rs350.task.MyTask;
import com.clou.rs350.ui.adapter.FragmentTabAdapter;
import com.clou.rs350.ui.dialog.HintDialog;
import com.clou.rs350.ui.dialog.LoadingDialog;
import com.clou.rs350.ui.dialog.NormalDialog;
import com.clou.rs350.ui.dialog.SelectValueDialog;
import com.clou.rs350.ui.fragment.BaseFragment;
import com.clou.rs350.ui.fragment.recordfragment.CTBurdenRecordFragment;
import com.clou.rs350.ui.fragment.recordfragment.CTErrorRecordFragment;
import com.clou.rs350.ui.fragment.recordfragment.CameraRecordFragment;
import com.clou.rs350.ui.fragment.recordfragment.CustomerInfoRecordFragment;
import com.clou.rs350.ui.fragment.recordfragment.DailyTestingRecordFragment;
import com.clou.rs350.ui.fragment.recordfragment.DemandTestingRecordFragment;
import com.clou.rs350.ui.fragment.recordfragment.DigitalMeterTestingRecordFragment;
import com.clou.rs350.ui.fragment.recordfragment.EnergyTestingRecordFragment;
import com.clou.rs350.ui.fragment.recordfragment.ErrorTestingRecordFragment;
import com.clou.rs350.ui.fragment.recordfragment.HarmonicRecordFragment;
import com.clou.rs350.ui.fragment.recordfragment.LTMRecordFragment;
import com.clou.rs350.ui.fragment.recordfragment.PTBurdenRecordFragment;
import com.clou.rs350.ui.fragment.recordfragment.PTComparisonRecordFragment;
import com.clou.rs350.ui.fragment.recordfragment.ReadMeterRecordFragment;
import com.clou.rs350.ui.fragment.recordfragment.RealtimeBasicRecordFragment;
import com.clou.rs350.ui.fragment.recordfragment.RegisterReadingRecordFragment;
import com.clou.rs350.ui.fragment.recordfragment.RemarkRecordFragment;
import com.clou.rs350.ui.fragment.recordfragment.SealRecordFragment;
import com.clou.rs350.ui.fragment.recordfragment.SignatureRecordFragment;
import com.clou.rs350.ui.fragment.recordfragment.SiteDataRecordFragment;
import com.clou.rs350.ui.fragment.recordfragment.VisualRecordFragment;
import com.clou.rs350.ui.fragment.recordfragment.WaveRecordFragment;
import com.clou.rs350.ui.fragment.recordfragment.WiringCheckRecordFragment;
import com.clou.rs350.ui.view.control.HorizontalList;
import com.clou.rs350.utils.FileUtils;
import com.itextpdf.text.pdf.PdfObject;
import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.view.annotation.ViewInject;

import java.util.ArrayList;
import java.util.List;

public class DataDetailActivity extends BaseActivity implements View.OnClickListener, HandlerSwitchView.IOnSelectCallback {
    @ViewInject(R.id.horizontallist)
    private View HorizontalList;
    @ViewInject(R.id.btn_back)
    private Button backButtom;
    @ViewInject(R.id.btn_delete)
    private Button deleteButton;
    private CustomerInfo m_CustomerInfo;
    private HorizontalList m_HList;
    List<BaseFragment> m_ListFragment = new ArrayList();
    List<Integer> m_ListRecordName = new ArrayList();
    private ReportDatas m_ReportDatas;
    private List<TestItem> m_TestItems;
    @ViewInject(R.id.title_textview)
    private TextView titleTextView;

    /* access modifiers changed from: protected */
    @Override // com.clou.rs350.ui.activity.BaseActivity, android.support.v4.app.FragmentActivity
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_data_detail);
        setDefaultTitle(R.string.text_data_detail);
        ViewUtils.inject(this);
        this.m_HList = new HorizontalList(this.m_Context, this.HorizontalList);
        this.deleteButton.setEnabled(ClouData.getInstance().getOperatorInfo().Authority > 0);
        initData();
    }

    private Object inputData(BaseDao dao, String key1, String key2) {
        List<DataItem> tmpData = dao.queryReportDataByKeyAndTime(key1, key2);
        if (tmpData != null) {
            if (this.m_ReportDatas.Datas.containsKey(dao.getTableName())) {
                this.m_ReportDatas.Datas.get(dao.getTableName()).addAll(tmpData);
            } else {
                this.m_ReportDatas.Datas.put(dao.getTableName(), tmpData);
            }
        }
        return dao.queryDataByKeyAndTime(key1, key2);
    }

    private List<Object> inputDatas(BaseDao dao, String key1, String key2) {
        return inputDatas(dao, key1, key2, true);
    }

    private List<Object> inputDatas(BaseDao dao, String key1, String key2, boolean setReportModel) {
        List<DataItem> tmpData;
        if (setReportModel && (tmpData = dao.queryReportDataByKeyAndTime(key1, key2)) != null) {
            if (this.m_ReportDatas.Datas.containsKey(dao.getTableName())) {
                this.m_ReportDatas.Datas.get(dao.getTableName()).addAll(tmpData);
            } else {
                this.m_ReportDatas.Datas.put(dao.getTableName(), tmpData);
            }
        }
        return dao.queryDataListByKeyAndTime(key1, key2);
    }

    private void initData() {
        final LoadingDialog load = new LoadingDialog(this.m_Context);
        load.show();
        new MyTask(new ILoadCallback() {
            /* class com.clou.rs350.ui.activity.DataDetailActivity.AnonymousClass1 */

            @Override // com.clou.rs350.callback.ILoadCallback
            public Object run() {
                Intent intent = DataDetailActivity.this.getIntent();
                DataDetailActivity.this.loadData(intent.getStringExtra("CustomerSerialNo"), intent.getStringArrayExtra("TestTime"));
                return null;
            }

            @Override // com.clou.rs350.callback.ILoadCallback
            public void callback(Object result) {
                load.dismiss();
                DataDetailActivity.this.m_HList.setList(DataDetailActivity.this.m_ListRecordName);
                DataDetailActivity.this.m_HList.setOnClick(new HorizontalList.onItemClick() {
                    /* class com.clou.rs350.ui.activity.DataDetailActivity.AnonymousClass1.AnonymousClass1 */

                    @Override // com.clou.rs350.ui.view.control.HorizontalList.onItemClick
                    public void onItemClick(int index) {
                        DataDetailActivity.this.m_TabAdapter.checkedIndex(index);
                    }
                });
                DataDetailActivity.this.initView(DataDetailActivity.this.m_ListFragment);
            }
        }).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, new Integer[0]);
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private void loadData(String CustomerSerialNo, String[] Times) {
        this.m_ReportDatas = new ReportDatas();
        this.m_ListRecordName.add((int) R.string.text_customer_info);
        this.m_CustomerInfo = new CustomerInfoDao(this.m_Context).queryObjectBySerialNo(CustomerSerialNo);
        this.m_ListFragment.add(new CustomerInfoRecordFragment(this.m_CustomerInfo));
        this.m_TestItems = new ArrayList();
        for (int i = 0; i < Times.length; i++) {
            this.m_TestItems.add((TestItem) inputData(new TestItemDao(this.m_Context), CustomerSerialNo, Times[i]));
            List<Object> Datas = inputDatas(new SiteDataTestDao(this.m_Context), CustomerSerialNo, Times[i]);
            if (Datas != null && Datas.size() > 0) {
                for (int j = 0; j < Datas.size(); j++) {
                    this.m_ListRecordName.add((int) R.string.text_site_data);
                    this.m_ListFragment.add(new SiteDataRecordFragment((SiteData) Datas.get(j)));
                }
                SiteData siteData = (SiteData) Datas.get(0);
            }
            List<Object> Datas2 = inputDatas(new CameraDao(this.m_Context), CustomerSerialNo, Times[i]);
            if (Datas2 != null && Datas2.size() > 0) {
                for (int j2 = 0; j2 < Datas2.size(); j2++) {
                    this.m_ListRecordName.add((int) R.string.text_camera);
                    this.m_ListFragment.add(new CameraRecordFragment((CameraData) Datas2.get(j2)));
                }
            }
            List<Object> Datas3 = inputDatas(new VisualDao(this.m_Context), CustomerSerialNo, Times[i]);
            if (Datas3 != null && Datas3.size() > 0) {
                for (int j3 = 0; j3 < Datas3.size(); j3++) {
                    this.m_ListRecordName.add((int) R.string.text_visual);
                    this.m_ListFragment.add(new VisualRecordFragment((VisualData) Datas3.get(j3)));
                }
            }
            List<Object> Datas4 = inputDatas(new SealDao(this.m_Context), CustomerSerialNo, Times[i]);
            if (Datas4 != null && Datas4.size() > 0) {
                for (int j4 = 0; j4 < Datas4.size(); j4++) {
                    this.m_ListRecordName.add((int) R.string.text_seal);
                    this.m_ListFragment.add(new SealRecordFragment((SealData) Datas4.get(j4)));
                }
            }
            List<Object> Datas5 = inputDatas(new RegisterReadingDao(this.m_Context), CustomerSerialNo, Times[i]);
            if (Datas5 != null && Datas5.size() > 0) {
                for (int j5 = 0; j5 < Datas5.size(); j5++) {
                    this.m_ListRecordName.add((int) R.string.text_register_reading);
                    this.m_ListFragment.add(new RegisterReadingRecordFragment((RegisterReadingData) Datas5.get(j5)));
                }
            }
            List<Object> Datas6 = inputDatas(new BasicMeasurementDao(this.m_Context), CustomerSerialNo, Times[i]);
            if (Datas6 != null && Datas6.size() > 0) {
                for (int j6 = 0; j6 < Datas6.size(); j6++) {
                    this.m_ListRecordName.add((int) R.string.text_inst_parameters);
                    this.m_ListFragment.add(new RealtimeBasicRecordFragment((BasicMeasurement) Datas6.get(j6)));
                }
            }
            List<Object> Datas7 = inputDatas(new WaveMeasurementDao(this.m_Context), CustomerSerialNo, Times[i]);
            if (Datas7 != null && Datas7.size() > 0) {
                for (int j7 = 0; j7 < Datas7.size(); j7++) {
                    this.m_ListRecordName.add((int) R.string.text_wave);
                    this.m_ListFragment.add(new WaveRecordFragment((WaveMeasurement) Datas7.get(j7)));
                }
            }
            List<Object> Datas8 = inputDatas(new WiringCheckDao(this.m_Context), CustomerSerialNo, Times[i]);
            if (Datas8 != null && Datas8.size() > 0) {
                for (int j8 = 0; j8 < Datas8.size(); j8++) {
                    this.m_ListRecordName.add((int) R.string.text_wiring_check);
                    ((WiringCheckMeasurement) Datas8.get(j8)).AngleDefinition = this.m_TestItems.get(i).AngleDefinition;
                    this.m_ListFragment.add(new WiringCheckRecordFragment((WiringCheckMeasurement) Datas8.get(j8)));
                }
            }
            List<Object> Datas9 = inputDatas(new HarmonicMeasurementDao(this.m_Context), CustomerSerialNo, Times[i]);
            if (Datas9 != null && Datas9.size() > 0) {
                for (int j9 = 0; j9 < Datas9.size(); j9++) {
                    this.m_ListRecordName.add((int) R.string.text_harmonic);
                    this.m_ListFragment.add(new HarmonicRecordFragment((HarmonicMeasurement) Datas9.get(j9)));
                }
            }
            List<Object> Datas10 = inputDatas(new LTMDao(this.m_Context), CustomerSerialNo, Times[i], false);
            if (Datas10 != null && Datas10.size() > 0) {
                for (int j10 = 0; j10 < Datas10.size(); j10++) {
                    this.m_ListRecordName.add((int) R.string.text_ltm);
                    this.m_ListFragment.add(new LTMRecordFragment((LTM) Datas10.get(j10)));
                }
            }
            List<Object> Datas11 = inputDatas(new ErrorTestDao(this.m_Context), CustomerSerialNo, Times[i]);
            if (Datas11 != null && Datas11.size() > 0) {
                for (int j11 = 0; j11 < Datas11.size(); j11++) {
                    this.m_ListRecordName.add((int) R.string.text_error);
                    this.m_ListFragment.add(new ErrorTestingRecordFragment((ErrorTest) Datas11.get(j11)));
                }
            }
            List<Object> Datas12 = inputDatas(new DemandTestDao(this.m_Context), CustomerSerialNo, Times[i]);
            if (Datas12 != null && Datas12.size() > 0) {
                for (int j12 = 0; j12 < Datas12.size(); j12++) {
                    this.m_ListRecordName.add((int) R.string.text_demand_testing);
                    this.m_ListFragment.add(new DemandTestingRecordFragment((DemandTest) Datas12.get(j12)));
                }
            }
            List<Object> Datas13 = inputDatas(new EnergyTestDao(this.m_Context), CustomerSerialNo, Times[i]);
            if (Datas13 != null && Datas13.size() > 0) {
                for (int j13 = 0; j13 < Datas13.size(); j13++) {
                    this.m_ListRecordName.add((int) R.string.text_energy_testing);
                    this.m_ListFragment.add(new EnergyTestingRecordFragment((EnergyTest) Datas13.get(j13)));
                }
            }
            List<Object> Datas14 = inputDatas(new DailyTestDao(this.m_Context), CustomerSerialNo, Times[i]);
            if (Datas14 != null && Datas14.size() > 0) {
                for (int j14 = 0; j14 < Datas14.size(); j14++) {
                    this.m_ListRecordName.add((int) R.string.text_daily_testing);
                    this.m_ListFragment.add(new DailyTestingRecordFragment((DailyTest) Datas14.get(j14)));
                }
            }
            List<Object> Datas15 = inputDatas(new DigitalMeterTestDao(this.m_Context), CustomerSerialNo, Times[i]);
            if (Datas15 != null && Datas15.size() > 0) {
                for (int j15 = 0; j15 < Datas15.size(); j15++) {
                    this.m_ListRecordName.add((int) R.string.text_digital_meter_testing);
                    this.m_ListFragment.add(new DigitalMeterTestingRecordFragment((DigitalMeterTest) Datas15.get(j15)));
                }
            }
            List<Object> Datas16 = inputDatas(new CTMeasurementDao(this.m_Context), CustomerSerialNo, Times[i]);
            if (Datas16 != null && Datas16.size() > 0) {
                for (int j16 = 0; j16 < Datas16.size(); j16++) {
                    this.m_ListRecordName.add((int) R.string.text_ct_measurement);
                    this.m_ListFragment.add(new CTErrorRecordFragment((CTMeasurement) Datas16.get(j16)));
                }
            }
            List<Object> Datas17 = inputDatas(new CTBurdenDao(this.m_Context), CustomerSerialNo, Times[i]);
            if (Datas17 != null && Datas17.size() > 0) {
                for (int j17 = 0; j17 < Datas17.size(); j17++) {
                    this.m_ListRecordName.add((int) R.string.text_ct_burden);
                    this.m_ListFragment.add(new CTBurdenRecordFragment((CTBurden) Datas17.get(j17)));
                }
            }
            List<Object> Datas18 = inputDatas(new PTBurdenDao(this.m_Context), CustomerSerialNo, Times[i]);
            if (Datas18 != null && Datas18.size() > 0) {
                for (int j18 = 0; j18 < Datas18.size(); j18++) {
                    this.m_ListRecordName.add((int) R.string.text_pt_burden);
                    this.m_ListFragment.add(new PTBurdenRecordFragment((PTBurden) Datas18.get(j18)));
                }
            }
            List<Object> Datas19 = inputDatas(new PTComparisonDao(this.m_Context), CustomerSerialNo, Times[i]);
            if (Datas19 != null && Datas19.size() > 0) {
                for (int j19 = 0; j19 < Datas19.size(); j19++) {
                    this.m_ListRecordName.add((int) R.string.text_pt_comparison);
                    this.m_ListFragment.add(new PTComparisonRecordFragment((PTComparison) Datas19.get(j19)));
                }
            }
            List<Object> Datas20 = inputDatas(new ReadMeterDao(this.m_Context), CustomerSerialNo, Times[i]);
            if (Datas20 != null && Datas20.size() > 0) {
                for (int j20 = 0; j20 < Datas20.size(); j20++) {
                    this.m_ListRecordName.add((int) R.string.text_read_meter);
                    this.m_ListFragment.add(new ReadMeterRecordFragment((ReadMeter) Datas20.get(j20)));
                }
            }
            List<Object> Datas21 = inputDatas(new SignatureDao(this.m_Context), CustomerSerialNo, Times[i]);
            if (Datas21 != null && Datas21.size() > 0) {
                for (int j21 = 0; j21 < Datas21.size(); j21++) {
                    this.m_ListRecordName.add((int) R.string.text_signature);
                    this.m_ListFragment.add(new SignatureRecordFragment((SignatureData) Datas21.get(j21)));
                }
            }
            this.m_ListRecordName.add((int) R.string.text_remark);
            this.m_ListFragment.add(new RemarkRecordFragment(this.m_TestItems.get(i)));
        }
    }

    /* access modifiers changed from: protected */
    @Override // com.clou.rs350.ui.activity.BaseActivity, android.support.v4.app.FragmentActivity
    public void onResume() {
        super.onResume();
    }

    /* access modifiers changed from: protected */
    @Override // com.clou.rs350.ui.activity.BaseActivity, android.support.v4.app.FragmentActivity
    public void onPause() {
        super.onPause();
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private void initView(List<BaseFragment> listFragment) {
        this.m_TabAdapter = new FragmentTabAdapter(this, listFragment, R.id.content_view);
        this.m_TabAdapter.init();
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_back:
                finish();
                return;
            case R.id.btn_delete:
                NormalDialog dialog = new NormalDialog(this.m_Context);
                dialog.setOnClick(new View.OnClickListener() {
                    /* class com.clou.rs350.ui.activity.DataDetailActivity.AnonymousClass2 */

                    public void onClick(View v) {
                        List<DataBaseRecordItem> list = new ArrayList<>();
                        for (int i = 0; i < DataDetailActivity.this.m_TestItems.size(); i++) {
                            list.add(new DataBaseRecordItem(0, ((TestItem) DataDetailActivity.this.m_TestItems.get(i)).TestTime, ((TestItem) DataDetailActivity.this.m_TestItems.get(i)).TestItem, true));
                        }
                        new SearchDao(DataDetailActivity.this.m_Context).deleteRecordBySerialList(((TestItem) DataDetailActivity.this.m_TestItems.get(0)).CustomerSerialNo, list);
                        DataDetailActivity.this.setResult(-1);
                        DataDetailActivity.this.finish();
                    }
                }, null);
                dialog.show();
                dialog.setTitle(R.string.text_delete_one_hint);
                return;
            case R.id.btn_export:
                SelectValueDialog tmpDialog = new SelectValueDialog(this.m_Context, FileUtils.getFileDir(CLApplication.app.getTemplateXmlPath()));
                tmpDialog.setWarning(R.string.text_select_template);
                tmpDialog.show();
                tmpDialog.setOnClick(new View.OnClickListener() {
                    /* class com.clou.rs350.ui.activity.DataDetailActivity.AnonymousClass3 */

                    public void onClick(View v) {
                        BaseDao tmpDao = new CustomerInfoDao(DataDetailActivity.this.m_Context);
                        DataDetailActivity.this.m_ReportDatas.Datas.put(tmpDao.getTableName(), tmpDao.queryReportDataByKeyAndTime(DataDetailActivity.this.m_CustomerInfo.CustomerSr));
                        final String reportPath = (String.valueOf(((TestItem) DataDetailActivity.this.m_TestItems.get(0)).CustomerSerialNo) + "_" + ((TestItem) DataDetailActivity.this.m_TestItems.get(0)).TestTime).replace(" ", PdfObject.NOTHING).replace("/", PdfObject.NOTHING).replace(":", PdfObject.NOTHING);
                        final String templet = v.getTag().toString();
                        new LoadingDialog(DataDetailActivity.this.m_Context, new ILoadCallback() {
                            /* class com.clou.rs350.ui.activity.DataDetailActivity.AnonymousClass3.AnonymousClass1 */

                            @Override // com.clou.rs350.callback.ILoadCallback
                            public Object run() {
                                return new ExportReport(DataDetailActivity.this.m_Context).exportPdf(templet, reportPath, DataDetailActivity.this.m_ReportDatas);
                            }

                            @Override // com.clou.rs350.callback.ILoadCallback
                            public void callback(Object result) {
                                String tmpResult = result.toString();
                                if (tmpResult.equals("-1")) {
                                    HintDialog tmpDialog = new HintDialog(DataDetailActivity.this.m_Context);
                                    tmpDialog.show();
                                    tmpDialog.setTitle(R.string.text_no_template);
                                } else if (!tmpResult.isEmpty()) {
                                    Preferences.putString(Preferences.Key.TEMPLATE, templet);
                                    Intent intent = new Intent(DataDetailActivity.this.m_Context, PDFViewActivity.class);
                                    intent.putExtra("PDFPath", tmpResult);
                                    intent.putExtra("Email", DataDetailActivity.this.m_CustomerInfo.Email);
                                    DataDetailActivity.this.m_Context.startActivity(intent);
                                }
                            }
                        }).show();
                    }
                }, null);
                tmpDialog.setSelectionValue(Preferences.getString(Preferences.Key.TEMPLATE, PdfObject.NOTHING));
                tmpDialog.setTitle(R.string.text_select_template);
                return;
            default:
                return;
        }
    }

    @Override // com.clou.rs350.handler.HandlerSwitchView.IOnSelectCallback
    public void onSelectedChanged(int index) {
    }

    @Override // com.clou.rs350.handler.HandlerSwitchView.IOnSelectCallback
    public void onClickView(View view) {
        onClick(view);
    }
}
