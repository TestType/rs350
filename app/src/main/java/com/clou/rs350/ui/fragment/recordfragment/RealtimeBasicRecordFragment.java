package com.clou.rs350.ui.fragment.recordfragment;

import android.content.res.Resources;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.clou.rs350.R;
import com.clou.rs350.db.model.BasicMeasurement;
import com.clou.rs350.ui.fragment.BaseFragment;
import com.clou.rs350.utils.OtherUtils;
import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.view.annotation.ViewInject;
import com.lidroid.xutils.view.annotation.event.OnClick;

public class RealtimeBasicRecordFragment extends BaseFragment {
    @ViewInject(R.id.f)
    private TextView FTv;
    @ViewInject(R.id.in)
    private TextView INTv;
    @ViewInject(R.id.l1anglei)
    private TextView L1AngleITv;
    @ViewInject(R.id.l1angleui)
    private TextView L1AngleUITv;
    @ViewInject(R.id.l1angleu)
    private TextView L1AngleUTv;
    @ViewInject(R.id.l1cfi)
    private TextView L1CFITv;
    @ViewInject(R.id.l1cfu)
    private TextView L1CFUTv;
    @ViewInject(R.id.txt_il1)
    private TextView L1ITv;
    @ViewInject(R.id.l1pf)
    private TextView L1PFTv;
    @ViewInject(R.id.l1p)
    private TextView L1PTv;
    @ViewInject(R.id.l1q)
    private TextView L1QTv;
    @ViewInject(R.id.l1s)
    private TextView L1STv;
    @ViewInject(R.id.l1thdi)
    private TextView L1THDITv;
    @ViewInject(R.id.l1thdu)
    private TextView L1THDUTv;
    @ViewInject(R.id.l1ull)
    private TextView L1ULLTv;
    @ViewInject(R.id.l1uln)
    private TextView L1ULNTv;
    @ViewInject(R.id.l2anglei)
    private TextView L2AngleITv;
    @ViewInject(R.id.l2angleui)
    private TextView L2AngleUITv;
    @ViewInject(R.id.l2angleu)
    private TextView L2AngleUTv;
    @ViewInject(R.id.l2cfi)
    private TextView L2CFITv;
    @ViewInject(R.id.l2cfu)
    private TextView L2CFUTv;
    @ViewInject(R.id.txt_il2)
    private TextView L2ITv;
    @ViewInject(R.id.l2pf)
    private TextView L2PFTv;
    @ViewInject(R.id.l2p)
    private TextView L2PTv;
    @ViewInject(R.id.l2q)
    private TextView L2QTv;
    @ViewInject(R.id.l2s)
    private TextView L2STv;
    @ViewInject(R.id.l2thdi)
    private TextView L2THDITv;
    @ViewInject(R.id.l2thdu)
    private TextView L2THDUTv;
    @ViewInject(R.id.l2ull)
    private TextView L2ULLTv;
    @ViewInject(R.id.l2uln)
    private TextView L2ULNTv;
    @ViewInject(R.id.l3anglei)
    private TextView L3AngleITv;
    @ViewInject(R.id.l3angleui)
    private TextView L3AngleUITv;
    @ViewInject(R.id.l3angleu)
    private TextView L3AngleUTv;
    @ViewInject(R.id.l3cfi)
    private TextView L3CFITv;
    @ViewInject(R.id.l3cfu)
    private TextView L3CFUTv;
    @ViewInject(R.id.txt_il3)
    private TextView L3ITv;
    @ViewInject(R.id.l3pf)
    private TextView L3PFTv;
    @ViewInject(R.id.l3p)
    private TextView L3PTv;
    @ViewInject(R.id.l3q)
    private TextView L3QTv;
    @ViewInject(R.id.l3s)
    private TextView L3STv;
    @ViewInject(R.id.l3thdi)
    private TextView L3THDITv;
    @ViewInject(R.id.l3thdu)
    private TextView L3THDUTv;
    @ViewInject(R.id.l3ull)
    private TextView L3ULLTv;
    @ViewInject(R.id.l3uln)
    private TextView L3ULNTv;
    @ViewInject(R.id.pseqi)
    private TextView PSEQITv;
    @ViewInject(R.id.psequ)
    private TextView PSEQUTv;
    @ViewInject(R.id.real_basic_f_textview)
    private TextView RbFTv;
    @ViewInject(R.id.real_basic_i_12_textview)
    private TextView RbL1AngleITv;
    @ViewInject(R.id.real_basic_un_13_textview)
    private TextView RbL1AngleUINTv;
    @ViewInject(R.id.real_basic_up_13_textview)
    private TextView RbL1AngleUITv;
    @ViewInject(R.id.real_basic_un_12_textview)
    private TextView RbL1AngleUNTv;
    @ViewInject(R.id.real_basic_up_12_textview)
    private TextView RbL1AngleUTv;
    @ViewInject(R.id.real_basic_i_11_textview)
    private TextView RbL1ITv;
    @ViewInject(R.id.real_basic_p_1_textview)
    private TextView RbL1PFTv;
    @ViewInject(R.id.real_basic_w_11_textview)
    private TextView RbL1PTv;
    @ViewInject(R.id.real_basic_w_12_textview)
    private TextView RbL1QTv;
    @ViewInject(R.id.real_basic_w_13_textview)
    private TextView RbL1STv;
    @ViewInject(R.id.real_basic_un_11_textview)
    private TextView RbL1UNTv;
    @ViewInject(R.id.real_basic_up_11_textview)
    private TextView RbL1UTv;
    @ViewInject(R.id.real_basic_i_22_textview)
    private TextView RbL2AngleITv;
    @ViewInject(R.id.real_basic_un_23_textview)
    private TextView RbL2AngleUINTv;
    @ViewInject(R.id.real_basic_up_23_textview)
    private TextView RbL2AngleUITv;
    @ViewInject(R.id.real_basic_un_22_textview)
    private TextView RbL2AngleUNTv;
    @ViewInject(R.id.real_basic_up_22_textview)
    private TextView RbL2AngleUTv;
    @ViewInject(R.id.real_basic_i_21_textview)
    private TextView RbL2ITv;
    @ViewInject(R.id.real_basic_p_2_textview)
    private TextView RbL2PFTv;
    @ViewInject(R.id.real_basic_w_21_textview)
    private TextView RbL2PTv;
    @ViewInject(R.id.real_basic_w_22_textview)
    private TextView RbL2QTv;
    @ViewInject(R.id.real_basic_w_23_textview)
    private TextView RbL2STv;
    @ViewInject(R.id.real_basic_un_21_textview)
    private TextView RbL2UNTv;
    @ViewInject(R.id.real_basic_up_21_textview)
    private TextView RbL2UTv;
    @ViewInject(R.id.real_basic_i_32_textview)
    private TextView RbL3AngleITv;
    @ViewInject(R.id.real_basic_un_33_textview)
    private TextView RbL3AngleUINTv;
    @ViewInject(R.id.real_basic_up_33_textview)
    private TextView RbL3AngleUITv;
    @ViewInject(R.id.real_basic_un_32_textview)
    private TextView RbL3AngleUNTv;
    @ViewInject(R.id.real_basic_up_32_textview)
    private TextView RbL3AngleUTv;
    @ViewInject(R.id.real_basic_i_31_textview)
    private TextView RbL3ITv;
    @ViewInject(R.id.real_basic_p_3_textview)
    private TextView RbL3PFTv;
    @ViewInject(R.id.real_basic_w_31_textview)
    private TextView RbL3PTv;
    @ViewInject(R.id.real_basic_w_32_textview)
    private TextView RbL3QTv;
    @ViewInject(R.id.real_basic_w_33_textview)
    private TextView RbL3STv;
    @ViewInject(R.id.real_basic_un_31_textview)
    private TextView RbL3UNTv;
    @ViewInject(R.id.real_basic_up_31_textview)
    private TextView RbL3UTv;
    @ViewInject(R.id.real_basic_iii_textview)
    private TextView RbPSeqITv;
    @ViewInject(R.id.real_basic_uuu_textview)
    private TextView RbPSeqUTv;
    @ViewInject(R.id.real_basic_p_4_textview)
    private TextView RbSumPFTv;
    @ViewInject(R.id.real_basic_w_41_textview)
    private TextView RbSumPTv;
    @ViewInject(R.id.real_basic_w_42_textview)
    private TextView RbSumQTv;
    @ViewInject(R.id.real_basic_w_43_textview)
    private TextView RbSumSTv;
    @ViewInject(R.id.sumpf)
    private TextView SumPFTv;
    @ViewInject(R.id.sump)
    private TextView SumPTv;
    @ViewInject(R.id.sumq)
    private TextView SumQTv;
    @ViewInject(R.id.sums)
    private TextView SumSTv;
    @ViewInject(R.id.un)
    private TextView UNTv;
    @ViewInject(R.id.real_basic_title_i_2_textview)
    private TextView i2TitleTv;
    private BasicMeasurement m_MeasureValue;
    int m_Style = 0;
    @ViewInject(R.id.real_basic_p_n_button)
    private Button pnButton;
    @ViewInject(R.id.basic_p_n_linearlayout)
    private LinearLayout pnLinearlayout;
    @ViewInject(R.id.real_basic_p_p_button)
    private Button ppButton;
    @ViewInject(R.id.basic_p_p_linearlayout)
    private LinearLayout ppLinearlayout;
    @ViewInject(R.id.real_basic_title_21_textview)
    private TextView u23TitleTv;
    @ViewInject(R.id.real_basic_title_31_textview)
    private TextView u31TitleTv;

    public RealtimeBasicRecordFragment(BasicMeasurement data) {
        this.m_MeasureValue = data;
    }

    @Override // android.support.v4.app.Fragment
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        this.fragmentView = inflater.inflate(R.layout.fragment_record_realtime_basic, (ViewGroup) null);
        ViewUtils.inject(this, this.fragmentView);
        return this.fragmentView;
    }

    @Override // android.support.v4.app.Fragment, com.clou.rs350.ui.fragment.BaseFragment
    public void onResume() {
        super.onResume();
        showValue();
    }

    @Override // android.support.v4.app.Fragment, com.clou.rs350.ui.fragment.BaseFragment
    public void onPause() {
        super.onPause();
    }

    @OnClick({R.id.btn_style, R.id.real_basic_p_p_button, R.id.real_basic_p_n_button})
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_style:
                this.m_Style++;
                setStyle();
                return;
            case R.id.real_basic_p_p_button:
                setLayout(false);
                return;
            case R.id.real_basic_p_n_button:
                setLayout(true);
                return;
            default:
                return;
        }
    }

    private void setStyle() {
        switch (this.m_Style) {
            case 1:
                findViewById(R.id.realtimebasic).setVisibility(8);
                findViewById(R.id.layout_whole).setVisibility(0);
                return;
            default:
                findViewById(R.id.realtimebasic).setVisibility(0);
                findViewById(R.id.layout_whole).setVisibility(8);
                this.m_Style = 0;
                return;
        }
    }

    private void setLayout(boolean is3P) {
        int i;
        int i2 = 8;
        LinearLayout linearLayout = this.ppLinearlayout;
        if (is3P) {
            i = 0;
        } else {
            i = 8;
        }
        linearLayout.setVisibility(i);
        LinearLayout linearLayout2 = this.pnLinearlayout;
        if (!is3P) {
            i2 = 0;
        }
        linearLayout2.setVisibility(i2);
    }

    private void setTitle(boolean is3P) {
        int i = R.string.text_phase_null;
        this.u23TitleTv.setText(this.m_Context.getResources().getString(is3P ? R.string.text_phase_null : R.string.text_l23));
        this.u31TitleTv.setText(this.m_Context.getResources().getString(is3P ? R.string.text_l32 : R.string.text_l31));
        Resources resources = this.m_Context.getResources();
        if (!is3P) {
            i = R.string.text_l2;
        }
        this.i2TitleTv.setText(resources.getString(i));
    }

    private void showValue() {
        this.L1ULNTv.setText(this.m_MeasureValue.VoltageValueL1.s_Value);
        this.L2ULNTv.setText(this.m_MeasureValue.VoltageValueL2.s_Value);
        this.L3ULNTv.setText(this.m_MeasureValue.VoltageValueL3.s_Value);
        this.L1ULLTv.setText(this.m_MeasureValue.VoltageValueL1L2.s_Value);
        this.L2ULLTv.setText(this.m_MeasureValue.VoltageValueL2L3.s_Value);
        this.L3ULLTv.setText(this.m_MeasureValue.VoltageValueL3L1.s_Value);
        boolean is3P = OtherUtils.is3P(this.m_MeasureValue.Wiring);
        setTitle(is3P);
        if (is3P) {
            this.L1AngleUTv.setText(this.m_MeasureValue.VoltageAngleL1.s_Value);
            this.L2AngleUTv.setText(this.m_MeasureValue.VoltageAngleL2.s_Value);
            this.L3AngleUTv.setText(this.m_MeasureValue.VoltageAngleL3.s_Value);
            this.L1AngleUITv.setText(this.m_MeasureValue.VoltageCurrentAngleL1.s_Value);
            this.L2AngleUITv.setText(this.m_MeasureValue.VoltageCurrentAngleL2.s_Value);
            this.L3AngleUITv.setText(this.m_MeasureValue.VoltageCurrentAngleL3.s_Value);
        } else {
            this.L1AngleUTv.setText(this.m_MeasureValue.VoltageAngleL1L2.s_Value);
            this.L2AngleUTv.setText(this.m_MeasureValue.VoltageAngleL2L3.s_Value);
            this.L3AngleUTv.setText(this.m_MeasureValue.VoltageAngleL3L1.s_Value);
            this.L1AngleUITv.setText(this.m_MeasureValue.AngleU12I1.s_Value);
            this.L2AngleUITv.setText(this.m_MeasureValue.AngleU23I2.s_Value);
            this.L3AngleUITv.setText(this.m_MeasureValue.AngleU31I3.s_Value);
        }
        this.L1THDUTv.setText(this.m_MeasureValue.THDU1.s_Value);
        this.L2THDUTv.setText(this.m_MeasureValue.THDU2.s_Value);
        this.L3THDUTv.setText(this.m_MeasureValue.THDU3.s_Value);
        this.L1CFUTv.setText(this.m_MeasureValue.CFU1.s_Value);
        this.L2CFUTv.setText(this.m_MeasureValue.CFU2.s_Value);
        this.L3CFUTv.setText(this.m_MeasureValue.CFU3.s_Value);
        this.L1ITv.setText(this.m_MeasureValue.CurrentValueL1.s_Value);
        this.L2ITv.setText(this.m_MeasureValue.CurrentValueL2.s_Value);
        this.L3ITv.setText(this.m_MeasureValue.CurrentValueL3.s_Value);
        this.L1AngleITv.setText(this.m_MeasureValue.CurrentAngleL1.s_Value);
        this.L2AngleITv.setText(this.m_MeasureValue.CurrentAngleL2.s_Value);
        this.L3AngleITv.setText(this.m_MeasureValue.CurrentAngleL3.s_Value);
        this.L1THDITv.setText(this.m_MeasureValue.THDI1.s_Value);
        this.L2THDITv.setText(this.m_MeasureValue.THDI2.s_Value);
        this.L3THDITv.setText(this.m_MeasureValue.THDI3.s_Value);
        this.L1CFITv.setText(this.m_MeasureValue.CFI1.s_Value);
        this.L2CFITv.setText(this.m_MeasureValue.CFI2.s_Value);
        this.L3CFITv.setText(this.m_MeasureValue.CFI3.s_Value);
        this.L1PFTv.setText(this.m_MeasureValue.PowerFactorL1.s_Value);
        this.L2PFTv.setText(this.m_MeasureValue.PowerFactorL2.s_Value);
        this.L3PFTv.setText(this.m_MeasureValue.PowerFactorL3.s_Value);
        this.SumPFTv.setText(this.m_MeasureValue.PowerFactorTotal.s_Value);
        this.L1PTv.setText(this.m_MeasureValue.ActivePowerL1.s_Value);
        this.L2PTv.setText(this.m_MeasureValue.ActivePowerL2.s_Value);
        this.L3PTv.setText(this.m_MeasureValue.ActivePowerL3.s_Value);
        this.SumPTv.setText(this.m_MeasureValue.ActivePowerTotal.s_Value);
        this.L1QTv.setText(this.m_MeasureValue.ReactivePowerL1.s_Value);
        this.L2QTv.setText(this.m_MeasureValue.ReactivePowerL2.s_Value);
        this.L3QTv.setText(this.m_MeasureValue.ReactivePowerL3.s_Value);
        this.SumQTv.setText(this.m_MeasureValue.ReactivePowerTotal.s_Value);
        this.L1STv.setText(this.m_MeasureValue.ApparentPowerL1.s_Value);
        this.L2STv.setText(this.m_MeasureValue.ApparentPowerL2.s_Value);
        this.L3STv.setText(this.m_MeasureValue.ApparentPowerL3.s_Value);
        this.SumSTv.setText(this.m_MeasureValue.ApparentPowerTotal.s_Value);
        this.UNTv.setText(this.m_MeasureValue.VoltageLN.s_Value);
        this.INTv.setText(this.m_MeasureValue.CurrentLN.s_Value);
        this.FTv.setText(this.m_MeasureValue.Frequency.s_Value);
        this.PSEQUTv.setText(this.m_MeasureValue.VoltagePhaseSequence);
        this.PSEQITv.setText(this.m_MeasureValue.CurrentPhaseSequence);
        this.RbL1UTv.setText(this.m_MeasureValue.VoltageValueL1L2.s_Value);
        this.RbL2UTv.setText(this.m_MeasureValue.VoltageValueL2L3.s_Value);
        this.RbL3UTv.setText(this.m_MeasureValue.VoltageValueL3L1.s_Value);
        this.RbL1AngleUTv.setText(this.m_MeasureValue.VoltageAngleL1L2.s_Value);
        this.RbL2AngleUTv.setText(this.m_MeasureValue.VoltageAngleL2L3.s_Value);
        this.RbL3AngleUTv.setText(this.m_MeasureValue.VoltageAngleL3L1.s_Value);
        this.RbL1AngleUITv.setText(this.m_MeasureValue.AngleU12I1.s_Value);
        this.RbL2AngleUITv.setText(this.m_MeasureValue.AngleU23I2.s_Value);
        this.RbL3AngleUITv.setText(this.m_MeasureValue.AngleU31I3.s_Value);
        this.RbL1UNTv.setText(this.m_MeasureValue.VoltageValueL1.s_Value);
        this.RbL2UNTv.setText(this.m_MeasureValue.VoltageValueL2.s_Value);
        this.RbL3UNTv.setText(this.m_MeasureValue.VoltageValueL3.s_Value);
        this.RbL1AngleUNTv.setText(this.m_MeasureValue.VoltageAngleL1.s_Value);
        this.RbL2AngleUNTv.setText(this.m_MeasureValue.VoltageAngleL2.s_Value);
        this.RbL3AngleUNTv.setText(this.m_MeasureValue.VoltageAngleL3.s_Value);
        this.RbL1AngleUINTv.setText(this.m_MeasureValue.VoltageCurrentAngleL1.s_Value);
        this.RbL2AngleUINTv.setText(this.m_MeasureValue.VoltageCurrentAngleL2.s_Value);
        this.RbL3AngleUINTv.setText(this.m_MeasureValue.VoltageCurrentAngleL3.s_Value);
        this.RbL1ITv.setText(this.m_MeasureValue.CurrentValueL1.s_Value);
        this.RbL2ITv.setText(this.m_MeasureValue.CurrentValueL2.s_Value);
        this.RbL3ITv.setText(this.m_MeasureValue.CurrentValueL3.s_Value);
        this.RbL1AngleITv.setText(this.m_MeasureValue.CurrentAngleL1.s_Value);
        this.RbL2AngleITv.setText(this.m_MeasureValue.CurrentAngleL2.s_Value);
        this.RbL3AngleITv.setText(this.m_MeasureValue.CurrentAngleL3.s_Value);
        this.RbL1PTv.setText(this.m_MeasureValue.ActivePowerL1.s_Value);
        this.RbL2PTv.setText(this.m_MeasureValue.ActivePowerL2.s_Value);
        this.RbL3PTv.setText(this.m_MeasureValue.ActivePowerL3.s_Value);
        this.RbSumPTv.setText(this.m_MeasureValue.ActivePowerTotal.s_Value);
        this.RbL1QTv.setText(this.m_MeasureValue.ReactivePowerL1.s_Value);
        this.RbL2QTv.setText(this.m_MeasureValue.ReactivePowerL2.s_Value);
        this.RbL3QTv.setText(this.m_MeasureValue.ReactivePowerL3.s_Value);
        this.RbSumQTv.setText(this.m_MeasureValue.ReactivePowerTotal.s_Value);
        this.RbL1STv.setText(this.m_MeasureValue.ApparentPowerL1.s_Value);
        this.RbL2STv.setText(this.m_MeasureValue.ApparentPowerL2.s_Value);
        this.RbL3STv.setText(this.m_MeasureValue.ApparentPowerL3.s_Value);
        this.RbSumSTv.setText(this.m_MeasureValue.ApparentPowerTotal.s_Value);
        this.RbL1PFTv.setText(this.m_MeasureValue.PowerFactorL1.s_Value);
        this.RbL2PFTv.setText(this.m_MeasureValue.PowerFactorL2.s_Value);
        this.RbL3PFTv.setText(this.m_MeasureValue.PowerFactorL3.s_Value);
        this.RbSumPFTv.setText(this.m_MeasureValue.PowerFactorTotal.s_Value);
        this.RbFTv.setText(this.m_MeasureValue.Frequency.s_Value);
        this.RbPSeqUTv.setText(this.m_MeasureValue.VoltagePhaseSequence);
        this.RbPSeqITv.setText(this.m_MeasureValue.CurrentPhaseSequence);
    }

    @Override // android.support.v4.app.Fragment, com.clou.rs350.ui.fragment.BaseFragment
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initView();
    }

    private void initView() {
    }
}
