package com.clou.rs350.ui.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.LinearInterpolator;
import android.widget.ImageView;

import com.clou.rs350.R;
import com.clou.rs350.callback.ILoadCallback;
import com.clou.rs350.task.MyTask;

public class LoadingDialog extends Dialog {
    Context context;
    ILoadCallback m_Callback = null;

    public LoadingDialog(Context context2) {
        super(context2, R.style.dialog);
        this.context = context2;
    }

    public LoadingDialog(Context context2, ILoadCallback callback) {
        super(context2, R.style.dialog);
        this.context = context2;
        this.m_Callback = callback;
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_loading);
        WindowManager.LayoutParams lp = getWindow().getAttributes();
        lp.width = getContext().getResources().getDisplayMetrics().widthPixels;
        lp.height = getContext().getResources().getDisplayMetrics().heightPixels;
        getWindow().setAttributes(lp);
        init();
    }

    /* access modifiers changed from: package-private */
    public void init() {
        Animation anim = AnimationUtils.loadAnimation(this.context, R.anim.rotate_repeat);
        anim.setInterpolator(new LinearInterpolator());
        ((ImageView) findViewById(R.id.img_loading)).setAnimation(anim);
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        return true;
    }

    public void show() {
        if (this.m_Callback != null) {
            new MyTask(new ILoadCallback() {
                /* class com.clou.rs350.ui.dialog.LoadingDialog.AnonymousClass1 */

                @Override // com.clou.rs350.callback.ILoadCallback
                public Object run() {
                    return LoadingDialog.this.m_Callback.run();
                }

                @Override // com.clou.rs350.callback.ILoadCallback
                public void callback(Object result) {
                    LoadingDialog.this.m_Callback.callback(result);
                    LoadingDialog.this.dismiss();
                }
            }).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, new Integer[0]);
        }
        super.show();
    }
}
