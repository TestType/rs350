package com.clou.rs350.ui.fragment.recordfragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.clou.rs350.R;
import com.clou.rs350.db.model.TestItem;
import com.clou.rs350.ui.fragment.BaseFragment;
import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.view.annotation.ViewInject;

public class RemarkRecordFragment extends BaseFragment {
    @ViewInject(R.id.txt_humidity)
    private TextView HumidityTextView;
    @ViewInject(R.id.txt_temperature)
    private TextView TemperatureTextView;
    TestItem m_Data;
    @ViewInject(R.id.txt_designation)
    private TextView txt_Designation;
    @ViewInject(R.id.txt_operator_name)
    private TextView txt_OperatorName;
    @ViewInject(R.id.txt_remark)
    private TextView txt_Remark;

    public RemarkRecordFragment(TestItem data) {
        this.m_Data = data;
    }

    @Override // android.support.v4.app.Fragment
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        this.fragmentView = inflater.inflate(R.layout.fragment_record_remark, (ViewGroup) null);
        ViewUtils.inject(this, this.fragmentView);
        return this.fragmentView;
    }

    @Override // android.support.v4.app.Fragment, com.clou.rs350.ui.fragment.BaseFragment
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initView();
    }

    private void initView() {
    }

    private void showData() {
        this.txt_Remark.setText(this.m_Data.Remark);
        this.txt_OperatorName.setText(this.m_Data.OperatorName);
        this.txt_Designation.setText(this.m_Data.Designation);
        this.TemperatureTextView.setText(this.m_Data.Temperature);
        this.HumidityTextView.setText(this.m_Data.Humidity);
    }

    @Override // android.support.v4.app.Fragment, com.clou.rs350.ui.fragment.BaseFragment
    public void onResume() {
        showData();
        super.onResume();
    }

    /* access modifiers changed from: protected */
    @Override // com.clou.rs350.ui.fragment.BaseFragment
    public void copyData() {
    }
}
