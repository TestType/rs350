package com.clou.rs350.ui.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.clou.rs350.R;

public class ProgressDialog extends Dialog {
    private ProgressBar progressBar;
    private TextView txtTitle;

    public ProgressDialog(Context context) {
        super(context, R.style.dialog);
        requestWindowFeature(1);
        setCanceledOnTouchOutside(false);
        setCancelable(false);
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_progress);
        this.progressBar = (ProgressBar) findViewById(R.id.ss_calibration_progress);
        this.txtTitle = (TextView) findViewById(R.id.dialog_content_textview);
    }

    public void showDialog(int allSecond, String title) {
        showDialog(allSecond);
        this.txtTitle.setText(title);
    }

    @Override // android.app.Dialog
    public void setTitle(int titleId) {
        this.txtTitle.setText(titleId);
    }

    public void showDialog(int allSecond, int resid) {
        showDialog(allSecond);
        this.txtTitle.setText(resid);
    }

    public void showDialog(int allSecond) {
        super.show();
        this.progressBar.setMax(allSecond);
        this.progressBar.setProgress(0);
    }

    public void dismiss() {
        super.dismiss();
    }

    public void setMax(int max) {
        this.progressBar.setMax(max);
    }

    public void setProgress(int value) {
        this.progressBar.setProgress(value);
    }
}
