package com.clou.rs350.ui.fragment.settingfragment;

import android.content.DialogInterface;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.RadioButton;
import android.widget.TextView;

import com.clou.rs350.R;
import com.clou.rs350.device.MeterBaseDevice;
import com.clou.rs350.ui.dialog.NumericDialog;
import com.clou.rs350.ui.fragment.BaseFragment;
import com.clou.rs350.ui.popwindow.ListPopWindow;
import com.itextpdf.text.pdf.PdfObject;
import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.view.annotation.ViewInject;
import com.lidroid.xutils.view.annotation.event.OnClick;

public class ConstantsFragment extends BaseFragment implements View.OnClickListener {
    private static final int REQUEST_GETCONSTANT = 1;
    private static final int REQUEST_SETCONSTANT = 2;
    private NumericDialog Numeric = null;
    @ViewInject(R.id.ss_constant_radio)
    private RadioButton auto;
    @ViewInject(R.id.ss_constant_constant)
    private TextView constant;
    @ViewInject(R.id.ss_constant_current)
    private TextView current;
    @ViewInject(R.id.constant_manual)
    private RadioButton manual;
    private int requestId = 0;
    @ViewInject(R.id.ss_constant_right)
    private TextView right;
    TextWatcher tw_current = new TextWatcher() {
        /* class com.clou.rs350.ui.fragment.settingfragment.ConstantsFragment.AnonymousClass1 */

        public void onTextChanged(CharSequence s, int start, int before, int count) {
            double result = 0.0d;
            if (!ConstantsFragment.this.voltage.getText().toString().trim().equals(PdfObject.NOTHING) && !s.toString().trim().equals(PdfObject.NOTHING)) {
                result = (3600000.0d / ((3.0d * Double.parseDouble(ConstantsFragment.this.voltage.getText().toString())) * Double.parseDouble(s.toString()))) * 50000.0d;
            }
            ConstantsFragment.this.constant.setText(result != 0.0d ? new StringBuilder(String.valueOf((long) result)).toString() : PdfObject.NOTHING);
        }

        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        public void afterTextChanged(Editable s) {
        }
    };
    TextWatcher tw_voltage = new TextWatcher() {
        /* class com.clou.rs350.ui.fragment.settingfragment.ConstantsFragment.AnonymousClass2 */

        public void onTextChanged(CharSequence s, int start, int before, int count) {
            double result = 0.0d;
            if (!ConstantsFragment.this.current.getText().toString().trim().equals(PdfObject.NOTHING) && !s.toString().trim().equals(PdfObject.NOTHING)) {
                result = (3600000.0d / ((3.0d * Double.parseDouble(s.toString())) * Double.parseDouble(ConstantsFragment.this.current.getText().toString()))) * 50000.0d;
            }
            ConstantsFragment.this.constant.setText(result != 0.0d ? new StringBuilder(String.valueOf((long) result)).toString() : PdfObject.NOTHING);
        }

        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        public void afterTextChanged(Editable s) {
        }
    };
    @ViewInject(R.id.ss_constant_voltage)
    private TextView voltage;

    @Override // android.support.v4.app.Fragment, com.clou.rs350.ui.fragment.BaseFragment
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initData();
    }

    @Override // android.support.v4.app.Fragment
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        this.fragmentView = inflater.inflate(R.layout.fragment_constant, (ViewGroup) null);
        ViewUtils.inject(this, this.fragmentView);
        return this.fragmentView;
    }

    @Override // android.support.v4.app.Fragment, com.clou.rs350.ui.fragment.BaseFragment
    public void onResume() {
        super.onResume();
        showConstant(this.messageManager.getMeter());
    }

    public void initData() {
        this.voltage.addTextChangedListener(this.tw_voltage);
        this.current.addTextChangedListener(this.tw_current);
        this.Numeric = new NumericDialog(this.m_Context);
        this.auto.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            /* class com.clou.rs350.ui.fragment.settingfragment.ConstantsFragment.AnonymousClass3 */

            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    ConstantsFragment.this.right.setBackgroundResource(R.drawable.cannotedittext);
                    ConstantsFragment.this.requestId = 1;
                    ConstantsFragment.this.messageManager.setConstant(0);
                    return;
                }
                ConstantsFragment.this.right.setBackgroundResource(R.drawable.canedittext);
                ConstantsFragment.this.setManuConstant();
            }
        });
        this.requestId = 1;
        this.messageManager.getConstant();
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private void setManuConstant() {
        if (!this.right.getText().toString().equals(PdfObject.NOTHING)) {
            long constant2 = (long) Double.valueOf(this.right.getText().toString()).doubleValue();
            this.requestId = 1;
            this.messageManager.setConstant(constant2);
        }
    }

    @OnClick({R.id.ss_constant_voltage, R.id.ss_constant_current, R.id.ss_constant_right})
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ss_constant_voltage:
                ListPopWindow.showListPopwindow(this.m_Context, this.messageManager.getMeter().getVoltageRange_All(), v, null);
                return;
            case R.id.ss_constant_current:
                ListPopWindow.showListPopwindow(this.m_Context, this.messageManager.getMeter().getCurrentRange_All(), v, null);
                return;
            case R.id.ss_constant_constant:
            default:
                return;
            case R.id.ss_constant_right:
                if (!this.auto.isChecked()) {
                    this.requestId = 2;
                    this.Numeric.showMyDialog((TextView) v);
                    this.Numeric.myDialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                        /* class com.clou.rs350.ui.fragment.settingfragment.ConstantsFragment.AnonymousClass4 */

                        public void onDismiss(DialogInterface dialog) {
                            ConstantsFragment.this.setManuConstant();
                            ConstantsFragment.this.Numeric.myDialog.dismiss();
                        }
                    });
                    return;
                }
                return;
        }
    }

    private void showConstant(MeterBaseDevice Device) {
        long constant2 = Device.getConstantValue();
        if (constant2 == 0) {
            this.auto.setChecked(true);
            this.manual.setChecked(false);
            return;
        }
        this.manual.setChecked(true);
        this.auto.setChecked(false);
        this.right.setText(String.valueOf(constant2));
        this.right.setBackgroundResource(R.drawable.canedittext);
    }

    @Override // com.clou.rs350.ui.fragment.BaseFragment, com.clou.rs350.manager.MessageManager.IMessageListener
    public void onReceiveMessage(MeterBaseDevice Device) {
        switch (this.requestId) {
            case 1:
                showConstant(Device);
                return;
            default:
                return;
        }
    }
}
