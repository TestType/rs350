package com.clou.rs350.ui.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.clou.rs350.R;
import com.clou.rs350.db.model.CustomerInfo;

import java.util.ArrayList;
import java.util.List;

public class CustomerSerialNoAdapter extends BaseAdapter {
    private Context mContext;
    private List<CustomerInfo> serialNoData = new ArrayList();
    private int serialNoIndex = 0;

    public CustomerSerialNoAdapter(Context context) {
        this.mContext = context;
    }

    public int getCount() {
        if (this.serialNoData != null) {
            return this.serialNoData.size();
        }
        return 0;
    }

    public Object getItem(int position) {
        return this.serialNoData.get(position);
    }

    public long getItemId(int position) {
        return 0;
    }

    public boolean setSerialNoIndex(int serialNoIndex2) {
        if (this.serialNoIndex == serialNoIndex2) {
            return false;
        }
        this.serialNoIndex = serialNoIndex2;
        notifyDataSetChanged();
        return true;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        View view;
        if (convertView == null) {
            view = View.inflate(this.mContext, R.layout.database_serial_item, null);
        } else {
            view = convertView;
        }
        TextView tv = (TextView) view.findViewById(R.id.tv_serial_title);
        tv.setText(this.serialNoData.get(position).CustomerSr);
        if (this.serialNoIndex == position) {
            tv.setSelected(true);
        } else {
            tv.setSelected(false);
        }
        return view;
    }

    public void refreshData(List<CustomerInfo> serialNoData2) {
        this.serialNoData = serialNoData2;
        this.serialNoIndex = 0;
        notifyDataSetChanged();
    }

    public CustomerInfo getCurrentMeterInfo() {
        if (getCount() > 0) {
            return (CustomerInfo) getItem(this.serialNoIndex);
        }
        return null;
    }

    public int getCurrentMeterIndex() {
        return this.serialNoIndex;
    }
}
