package com.clou.rs350.ui.fragment.transformerfragment;

import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.clou.rs350.R;
import com.clou.rs350.db.model.CTMeasurement;
import com.clou.rs350.db.model.DBDataModel;
import com.clou.rs350.db.model.SiteData;
import com.clou.rs350.device.MeterBaseDevice;
import com.clou.rs350.handler.HandlerSwitchView;
import com.clou.rs350.manager.MessageManager;
import com.clou.rs350.model.ClouData;
import com.clou.rs350.task.ISleepCallback;
import com.clou.rs350.task.SleepTask;
import com.clou.rs350.ui.dialog.HintDialog;
import com.clou.rs350.ui.dialog.LoadingDialog;
import com.clou.rs350.ui.dialog.NumericDialog;
import com.clou.rs350.ui.fragment.BaseFragment;
import com.clou.rs350.utils.OtherUtils;
import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.view.annotation.ViewInject;
import com.lidroid.xutils.view.annotation.event.OnClick;

import java.util.ArrayList;
import java.util.List;

public class CTErrorFragment extends BaseFragment implements View.OnClickListener, HandlerSwitchView.IOnSelectCallback {
    @ViewInject(R.id.txt_accuracyclass)
    private TextView AccuracyclassTv;
    @ViewInject(R.id.txt_ip)
    private TextView IpTv;
    @ViewInject(R.id.txt_is)
    private TextView IsTv;
    @ViewInject(R.id.l1_phaseanage)
    private TextView L1PhaseAnageTv;
    @ViewInject(R.id.l1_primary)
    private TextView L1PrimaryTv;
    @ViewInject(R.id.l1_ratio_error)
    private TextView L1RatioErrorTv;
    @ViewInject(R.id.l1_result)
    private TextView L1ResultTv;
    @ViewInject(R.id.l1_secondary)
    private TextView L1SecondaryTv;
    @ViewInject(R.id.l2_phaseanage)
    private TextView L2PhaseAnageTv;
    @ViewInject(R.id.l2_primary)
    private TextView L2PrimaryTv;
    @ViewInject(R.id.l2_ratio_error)
    private TextView L2RatioErrorTv;
    @ViewInject(R.id.l2_result)
    private TextView L2ResultTv;
    @ViewInject(R.id.l2_secondary)
    private TextView L2SecondaryTv;
    @ViewInject(R.id.l3_phaseanage)
    private TextView L3PhaseAnageTv;
    @ViewInject(R.id.l3_primary)
    private TextView L3PrimaryTv;
    @ViewInject(R.id.l3_ratio_error)
    private TextView L3RatioErrorTv;
    @ViewInject(R.id.l3_result)
    private TextView L3ResultTv;
    @ViewInject(R.id.l3_secondary)
    private TextView L3SecondaryTv;
    @ViewInject(R.id.txt_va)
    private TextView VaTv;
    private int i_Index = 0;
    private int i_Time = 2;
    private CTMeasurement m_CTMeasurement;
    LoadingDialog m_LoadingDialog;
    private HandlerSwitchView switchView;

    public CTErrorFragment(CTMeasurement Data) {
        this.m_CTMeasurement = Data;
    }

    @Override // android.support.v4.app.Fragment
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        this.fragmentView = inflater.inflate(R.layout.fragment_ct_error, (ViewGroup) null);
        ViewUtils.inject(this, this.fragmentView);
        return this.fragmentView;
    }

    @Override // android.support.v4.app.Fragment, com.clou.rs350.ui.fragment.BaseFragment
    public void onResume() {
        super.onResume();
        initData();
        refreshData();
        showData();
    }

    @Override // com.clou.rs350.ui.fragment.BaseFragment
    public void refreshData() {
        SiteData baseInfo = ClouData.getInstance().getSiteData();
        this.IpTv.setText(new StringBuilder(String.valueOf(baseInfo.CTPrimary.d_Value)).toString());
        this.IsTv.setText(new StringBuilder(String.valueOf(baseInfo.CTSecondary.d_Value)).toString());
        this.m_CTMeasurement.setRatedValue(baseInfo.CTPrimary.d_Value, baseInfo.CTSecondary.d_Value);
        this.AccuracyclassTv.setText(baseInfo.CTAccuracyClass);
        this.m_CTMeasurement.Accuracy = OtherUtils.parseDouble(baseInfo.CTAccuracyClass);
        this.VaTv.setText(new StringBuilder(String.valueOf(baseInfo.CTBurden.d_Value)).toString());
    }

    /* access modifiers changed from: protected */
    @Override // com.clou.rs350.ui.fragment.BaseFragment
    public void copyData() {
        SiteData baseInfo = ClouData.getInstance().getSiteData();
        baseInfo.CTPrimary = this.m_CTMeasurement.CurrentValueNominalPrimary;
        baseInfo.CTSecondary = this.m_CTMeasurement.CurrentValueNominalSecondary;
        baseInfo.CTAccuracyClass = this.AccuracyclassTv.getText().toString();
        String TmpValue = this.VaTv.getText().toString();
        baseInfo.CTBurden = new DBDataModel(OtherUtils.parseDouble(TmpValue), String.valueOf(TmpValue) + "VA");
    }

    @Override // android.support.v4.app.Fragment, com.clou.rs350.ui.fragment.BaseFragment
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initView();
    }

    private void initView() {
        List<View> tabViews = new ArrayList<>();
        tabViews.add(findViewById(R.id.btn_l1));
        tabViews.add(findViewById(R.id.btn_l2));
        tabViews.add(findViewById(R.id.btn_l3));
        this.switchView = new HandlerSwitchView(this.m_Context);
        this.switchView.setTabViews(tabViews);
        this.switchView.boundClick();
        this.switchView.setOnTabChangedCallback(this);
        this.switchView.selectView(0);
    }

    @Override // com.clou.rs350.handler.HandlerSwitchView.IOnSelectCallback
    public void onSelectedChanged(int index) {
        this.i_Index = index;
    }

    @Override // com.clou.rs350.handler.HandlerSwitchView.IOnSelectCallback
    public void onClickView(View v) {
        onClick(v);
    }

    @OnClick({R.id.txt_ip, R.id.txt_is, R.id.txt_va, R.id.txt_accuracyclass, R.id.btn_read, R.id.btn_ratio_guessing})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.txt_ip:
            case R.id.txt_is:
            case R.id.txt_va:
            case R.id.txt_accuracyclass:
                clickTextView(view);
                return;
            case R.id.btn_read:
                ReadClick();
                return;
            case R.id.btn_ratio_guessing:
                switch (this.m_CTMeasurement.ratioGuessing()) {
                    case 0:
                        this.IpTv.setText(new StringBuilder(String.valueOf(this.m_CTMeasurement.CurrentValueNominalPrimary.d_Value)).toString());
                        showData();
                        return;
                    case 1:
                        new HintDialog(this.m_Context, (int) R.string.text_read_data).show();
                        return;
                    case 2:
                        new HintDialog(this.m_Context, (int) R.string.text_set_secondary).show();
                        return;
                    default:
                        return;
                }
            default:
                return;
        }
    }

    public void clickTextView(View view) {
        NumericDialog dialog = new NumericDialog(this.m_Context);
        dialog.showMyDialog((TextView) view);
        dialog.setOnDoneClick(new View.OnClickListener() {
            /* class com.clou.rs350.ui.fragment.transformerfragment.CTErrorFragment.AnonymousClass1 */

            public void onClick(View v) {
                double d_Ip = OtherUtils.parseDouble(CTErrorFragment.this.IpTv.getText().toString().trim());
                double d_Is = OtherUtils.parseDouble(CTErrorFragment.this.IsTv.getText().toString().trim());
                CTErrorFragment.this.m_CTMeasurement.Accuracy = OtherUtils.parseDouble(CTErrorFragment.this.AccuracyclassTv.getText().toString().trim());
                CTErrorFragment.this.m_CTMeasurement.setRatedValue(d_Ip, d_Is);
                CTErrorFragment.this.showData();
            }
        });
    }

    @Override // com.clou.rs350.ui.fragment.BaseFragment, com.clou.rs350.manager.MessageManager.IMessageListener
    public void onReceiveMessage(MeterBaseDevice Device) {
        super.onReceiveMessage(Device);
        if (this.i_Time == 1) {
            this.m_CTMeasurement.parseData(Device, this.i_Index);
            showData();
        }
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private void showData() {
        this.L1PrimaryTv.setText(this.m_CTMeasurement.CurrentValuePrimary[0].s_Value);
        this.L2PrimaryTv.setText(this.m_CTMeasurement.CurrentValuePrimary[1].s_Value);
        this.L3PrimaryTv.setText(this.m_CTMeasurement.CurrentValuePrimary[2].s_Value);
        this.L1SecondaryTv.setText(this.m_CTMeasurement.CurrentValueSecondary[0].s_Value);
        this.L2SecondaryTv.setText(this.m_CTMeasurement.CurrentValueSecondary[1].s_Value);
        this.L3SecondaryTv.setText(this.m_CTMeasurement.CurrentValueSecondary[2].s_Value);
        this.L1RatioErrorTv.setText(this.m_CTMeasurement.CurrentValueError[0].s_Value);
        this.L2RatioErrorTv.setText(this.m_CTMeasurement.CurrentValueError[1].s_Value);
        this.L3RatioErrorTv.setText(this.m_CTMeasurement.CurrentValueError[2].s_Value);
        this.L1PhaseAnageTv.setText(this.m_CTMeasurement.CurrentValuePhase[0].s_Value);
        this.L2PhaseAnageTv.setText(this.m_CTMeasurement.CurrentValuePhase[1].s_Value);
        this.L3PhaseAnageTv.setText(this.m_CTMeasurement.CurrentValuePhase[2].s_Value);
        OtherUtils.setText(this.L1ResultTv, this.m_CTMeasurement.Result[0]);
        OtherUtils.setText(this.L2ResultTv, this.m_CTMeasurement.Result[1]);
        OtherUtils.setText(this.L3ResultTv, this.m_CTMeasurement.Result[2]);
    }

    @Override // android.support.v4.app.Fragment, com.clou.rs350.ui.fragment.BaseFragment
    public void onPause() {
        super.onPause();
    }

    public Boolean isReadable() {
        if (OtherUtils.parseDouble(this.AccuracyclassTv.getText().toString().trim()) > 0.0d) {
            return true;
        }
        return false;
    }

    public void setCTChannel() {
        MessageManager.getInstance(this.m_Context).setCTChannel(this.i_Index);
    }

    public void ReadClick() {
        this.i_Time = 1;
        this.messageManager.getData();
        this.m_LoadingDialog = new LoadingDialog(this.m_Context);
        this.m_LoadingDialog.show();
        new SleepTask(1500, new ISleepCallback() {
            /* class com.clou.rs350.ui.fragment.transformerfragment.CTErrorFragment.AnonymousClass2 */

            @Override // com.clou.rs350.task.ISleepCallback
            public void onSleepAfterToDo() {
                CTErrorFragment.this.i_Time = 0;
                if (CTErrorFragment.this.m_LoadingDialog != null) {
                    CTErrorFragment.this.m_LoadingDialog.dismiss();
                }
            }
        }).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, new Integer[0]);
    }

    private void initData() {
        this.messageManager.setFunc_Mstate(65);
    }

    @Override // com.clou.rs350.ui.fragment.BaseFragment
    public int isFinish() {
        if (!this.m_CTMeasurement.isEmpty()) {
            return 0;
        }
        new HintDialog(this.m_Context, (int) R.string.text_do_test).show();
        return 1;
    }
}
