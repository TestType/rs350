package com.clou.rs350.ui.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.clou.rs350.R;
import com.clou.rs350.db.dao.SiteDataManagerDao;
import com.clou.rs350.db.model.SiteData;
import com.clou.rs350.model.ClouData;
import com.clou.rs350.ui.view.infoview.SiteDataView;

public class SiteDataInfoDialog extends Dialog {
    Button btn_Close = null;
    Button btn_Modify = null;
    TextView contentView = null;
    boolean isModefy = false;
    Context m_Context = null;
    SiteData m_Info;
    SiteDataView m_InfoView;
    View m_InfoViewLayout;
    private View.OnClickListener okClick = null;

    public SiteDataInfoDialog(Context context) {
        super(context, R.style.dialog);
        requestWindowFeature(1);
        setCanceledOnTouchOutside(true);
        this.m_Context = context;
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.m_Info = ClouData.getInstance().getSiteData();
        setContentView(R.layout.dialog_site_data);
        this.m_InfoViewLayout = findViewById(R.id.layout_site_data);
        this.m_InfoView = new SiteDataView(this.m_Context, this.m_InfoViewLayout, this.m_Info, 0);
        this.btn_Close = (Button) findViewById(R.id.btn_close);
        this.btn_Close.setOnClickListener(new View.OnClickListener() {
            /* class com.clou.rs350.ui.dialog.SiteDataInfoDialog.AnonymousClass1 */

            public void onClick(View v) {
                if (SiteDataInfoDialog.this.isModefy) {
                    SiteDataInfoDialog.this.StopModify();
                    SiteDataInfoDialog.this.m_InfoView.refreshData(SiteDataInfoDialog.this.m_Info);
                    return;
                }
                SiteDataInfoDialog.this.dismiss();
            }
        });
        this.btn_Modify = (Button) findViewById(R.id.btn_modify);
        this.btn_Modify.setOnClickListener(new View.OnClickListener() {
            /* class com.clou.rs350.ui.dialog.SiteDataInfoDialog.AnonymousClass2 */

            public void onClick(View v) {
                if (SiteDataInfoDialog.this.isModefy) {
                    SiteDataInfoDialog.this.Save();
                } else {
                    SiteDataInfoDialog.this.Modify();
                }
            }
        });
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private void Modify() {
        this.m_InfoView.setFunction(2);
        this.btn_Modify.setText(R.string.text_save);
        this.btn_Close.setText(R.string.text_cancel);
        this.isModefy = true;
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private void Save() {
        StopModify();
        this.m_Info = this.m_InfoView.getInfo();
        if (this.m_Info.isNew) {
            new SiteDataManagerDao(this.m_Context).insertObject(this.m_Info);
            this.m_Info.isNew = false;
            return;
        }
        new SiteDataManagerDao(this.m_Context).updateObjectByKey(this.m_Info.CustomerSr, this.m_Info);
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private void StopModify() {
        this.m_InfoView.setFunction(0);
        this.btn_Modify.setText(R.string.text_modify);
        this.btn_Close.setText(R.string.text_close);
        this.isModefy = false;
    }

    public void setOnClick(View.OnClickListener okClick2) {
        this.okClick = okClick2;
    }
}
