package com.clou.rs350.ui.fragment.sequencefragment;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.hardware.Camera;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.OrientationEventListener;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.clou.rs350.CLApplication;
import com.clou.rs350.R;
import com.clou.rs350.callback.ILoadCallback;
import com.clou.rs350.db.model.sequence.CameraData;
import com.clou.rs350.db.model.sequence.CameraItem;
import com.clou.rs350.task.ISleepCallback;
import com.clou.rs350.task.MyTask;
import com.clou.rs350.task.SleepTask;
import com.clou.rs350.ui.dialog.HintDialog;
import com.clou.rs350.ui.dialog.LoadingDialog;
import com.clou.rs350.ui.fragment.BaseFragment;
import com.clou.rs350.ui.view.CameraSurfacePreview;
import com.clou.rs350.utils.BitmapUtils;
import com.clou.rs350.utils.OtherUtils;
import com.itextpdf.text.pdf.ColumnText;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

public class CameraSequenceFragment extends BaseFragment implements View.OnClickListener, Camera.PictureCallback {
    private String TAG = "Photo";
    private Button btn_Delete;
    private Button btn_Next;
    private Button btn_Previous;
    private ImageView imageView;
    private CameraSurfacePreview mCameraSurPreview;
    private LinearLayout mCaptureButton;
    private LinearLayout mLightButton;
    private CameraData m_Data;
    private int m_Index;
    private LoadingDialog m_LoadingDialog;
    OrientationEventListener m_OrientationListener;
    int m_Orientations;
    private File m_PhotoFile;
    private LinearLayout preview;
    private TextView txt_Index;
    private TextView txt_Total;

    public CameraSequenceFragment(CameraData data) {
        this.m_Data = data;
    }

    @Override // android.support.v4.app.Fragment
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        this.fragmentView = inflater.inflate(R.layout.fragment_sequence_camera, (ViewGroup) null);
        return this.fragmentView;
    }

    @Override // android.support.v4.app.Fragment, com.clou.rs350.ui.fragment.BaseFragment
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        this.m_OrientationListener = new OrientationEventListener(this.m_Context) {
            /* class com.clou.rs350.ui.fragment.sequencefragment.CameraSequenceFragment.AnonymousClass1 */

            public void onOrientationChanged(int orientation) {
                CameraSequenceFragment.this.m_Orientations = orientation;
            }
        };
        initView();
    }

    private void initView() {
        this.preview = (LinearLayout) findViewById(R.id.camera_preview);
        this.mCameraSurPreview = new CameraSurfacePreview(this.m_Context);
        this.preview.addView(this.mCameraSurPreview);
        this.mCaptureButton = (LinearLayout) findViewById(R.id.btn_capture);
        this.mCaptureButton.setOnClickListener(this);
        this.mLightButton = (LinearLayout) findViewById(R.id.btn_light);
        this.mLightButton.setOnClickListener(this);
        this.txt_Index = (TextView) findViewById(R.id.txt_index);
        this.txt_Total = (TextView) findViewById(R.id.txt_total);
        this.btn_Previous = (Button) findViewById(R.id.btn_previous);
        this.btn_Previous.setOnClickListener(this);
        this.btn_Next = (Button) findViewById(R.id.btn_next);
        this.btn_Next.setOnClickListener(this);
        this.btn_Delete = (Button) findViewById(R.id.btn_delete);
        this.btn_Delete.setOnClickListener(this);
        this.imageView = (ImageView) findViewById(R.id.iv_show_photo);
        this.m_LoadingDialog = new LoadingDialog(this.m_Context);
        new SleepTask(1000, new ISleepCallback() {
            /* class com.clou.rs350.ui.fragment.sequencefragment.CameraSequenceFragment.AnonymousClass2 */

            @Override // com.clou.rs350.task.ISleepCallback
            public void onSleepAfterToDo() {
                CameraSequenceFragment.this.mLightButton.setVisibility(CameraSequenceFragment.this.mCameraSurPreview.hasFlash() ? 0 : 4);
            }
        }).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, 0);
    }

    @Override // android.support.v4.app.Fragment, com.clou.rs350.ui.fragment.BaseFragment
    public void onResume() {
        showData();
        if (this.mCameraSurPreview == null) {
            this.preview.removeAllViews();
            this.mCameraSurPreview = new CameraSurfacePreview(this.m_Context);
            this.preview.addView(this.mCameraSurPreview);
        }
        if (this.m_OrientationListener != null) {
            this.m_OrientationListener.enable();
        }
        super.onResume();
    }

    private void showData() {
        this.txt_Index.setText(new StringBuilder(String.valueOf(this.m_Index + 1)).toString());
        while (this.m_Data.Photos.size() <= this.m_Index) {
            this.m_Data.Photos.add(new CameraItem());
        }
        this.txt_Total.setText(new StringBuilder(String.valueOf(this.m_Data.Photos.size())).toString());
        setInitImageView(String.valueOf(CLApplication.app.getImagePath()) + File.separator + this.m_Data.Photos.get(this.m_Index).PhotoPath);
        setButton();
    }

    private void setInitImageView(final String Path) {
        new MyTask(new ILoadCallback() {
            /* class com.clou.rs350.ui.fragment.sequencefragment.CameraSequenceFragment.AnonymousClass3 */

            @Override // com.clou.rs350.callback.ILoadCallback
            public Object run() {
                CameraSequenceFragment.this.m_PhotoFile = new File(Path);
                if (CameraSequenceFragment.this.m_PhotoFile.exists()) {
                    return null;
                }
                System.out.println("init image-->1");
                return null;
            }

            @Override // com.clou.rs350.callback.ILoadCallback
            public void callback(Object result) {
                if (CameraSequenceFragment.this.m_PhotoFile == null) {
                    CameraSequenceFragment.this.imageView.setWillNotDraw(true);
                    return;
                }
                CameraSequenceFragment.this.imageView.setWillNotDraw(false);
                try {
                    CameraSequenceFragment.this.imageView.setImageBitmap(MediaStore.Images.Media.getBitmap(CameraSequenceFragment.this.m_Context.getContentResolver(), Uri.fromFile(CameraSequenceFragment.this.m_PhotoFile)));
                } catch (FileNotFoundException e) {
                    CameraSequenceFragment.this.imageView.setWillNotDraw(true);
                } catch (IOException e2) {
                }
            }
        }).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, new Integer[0]);
    }

    public void onPictureTaken(byte[] data, Camera camera) {
        BitmapFactory.Options opts = new BitmapFactory.Options();
        opts.inSampleSize = 1;
        Bitmap bmp = BitmapFactory.decodeByteArray(data, 0, data.length, opts);
        Matrix matrixs = new Matrix();
        if (this.m_Orientations > 325 || this.m_Orientations <= 45) {
            matrixs.setRotate(90.0f);
        } else if (this.m_Orientations > 45 && this.m_Orientations <= 135) {
            matrixs.setRotate(180.0f);
        } else if (this.m_Orientations <= 135 || this.m_Orientations >= 225) {
            matrixs.setRotate(ColumnText.GLOBAL_SPACE_CHAR_RATIO);
        } else {
            matrixs.setRotate(270.0f);
        }
        float scale = 900.0f / ((float) bmp.getWidth());
        Bitmap bmp2 = BitmapUtils.getScaledBitmap(bmp, scale, scale);
        Bitmap bmp3 = Bitmap.createBitmap(bmp2, 0, 0, bmp2.getWidth(), bmp2.getHeight(), matrixs, true);
        this.m_Data.Photos.get(this.m_Index).PhotoPath = "Tmp_" + this.m_Data.Index + "_" + this.m_Index + ".jpg";
        BitmapUtils.saveBitmapFile(bmp3, String.valueOf(CLApplication.app.getImagePath()) + File.separator + this.m_Data.Photos.get(this.m_Index).PhotoPath);
        this.m_LoadingDialog.dismiss();
        camera.startPreview();
        this.mCaptureButton.setEnabled(true);
        this.mLightButton.setEnabled(true);
        showData();
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_delete:
                if (this.m_PhotoFile != null) {
                    this.m_PhotoFile.delete();
                }
                this.m_Data.Photos.remove(this.m_Index);
                this.m_Index--;
                if (this.m_Index < 0) {
                    this.m_Index = 0;
                }
                showData();
                return;
            case R.id.btn_previous:
                this.m_Index--;
                showData();
                return;
            case R.id.btn_next:
                this.m_Index++;
                showData();
                return;
            case R.id.btn_light:
                this.mCameraSurPreview.lightControl();
                return;
            case R.id.btn_capture:
                this.m_LoadingDialog.show();
                this.mCaptureButton.setEnabled(false);
                this.mLightButton.setEnabled(false);
                this.mCameraSurPreview.takePicture(this);
                return;
            default:
                return;
        }
    }

    private void setButton() {
        boolean z;
        boolean z2 = true;
        Button button = this.btn_Previous;
        if (this.m_Index != 0) {
            z = true;
        } else {
            z = false;
        }
        button.setEnabled(z);
        this.btn_Next.setText(this.m_Index < this.m_Data.Photos.size() + -1 ? R.string.text_next : R.string.text_add);
        Button button2 = this.btn_Delete;
        if (this.m_Data.Photos.size() <= 1) {
            z2 = false;
        }
        button2.setEnabled(z2);
    }

    @Override // android.support.v4.app.Fragment, com.clou.rs350.ui.fragment.BaseFragment
    public void onPause() {
        if (this.mCameraSurPreview != null) {
            this.preview.removeAllViews();
            this.mCameraSurPreview.surfaceDestroyed(null);
            this.mCameraSurPreview = null;
        }
        if (this.m_OrientationListener != null) {
            this.m_OrientationListener.disable();
        }
        super.onPause();
    }

    @Override // com.clou.rs350.ui.fragment.BaseFragment
    public int isFinish() {
        if (this.m_Data.Photos.size() > 0 && !OtherUtils.isEmpty(this.m_Data.Photos.get(0).PhotoPath)) {
            return 0;
        }
        new HintDialog(this.m_Context, (int) R.string.text_take_photo).show();
        return 1;
    }
}
