package com.clou.rs350.ui.view;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.View;

public class MySignalView extends View {
    private static final String TAG = "BatteryView";
    private Context m_Context;
    float m_Height = ((float) getHeight());
    private Paint m_Paint = null;
    float m_Signal = -100.0f;
    float m_Width = ((float) getWidth());

    public MySignalView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.m_Context = context;
        init();
    }

    private void init() {
        this.m_Paint = new Paint();
        this.m_Paint.setAntiAlias(true);
    }

    private float getValue(int resId, float defaultValue) {
        return this.m_Context == null ? defaultValue : this.m_Context.getResources().getDimension(resId);
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        this.m_Width = (float) getWidth();
        this.m_Height = (float) getHeight();
        drawSignal(canvas, this.m_Paint);
    }

    public void setSignal(float values) {
        this.m_Signal = values;
        postInvalidate();
    }

    private void drawSignal(Canvas canvas, Paint paint) {
        int rectangleColor;
        int rectangleColor2;
        float paddingLeft = this.m_Width * 0.02f;
        float paddingTop = this.m_Height * 0.02f;
        float realWidth = this.m_Width - (2.0f * paddingLeft);
        float realHeight = this.m_Height - (2.0f * paddingTop);
        float rectangleWidth = realWidth / 5.0f;
        int rectangleColor3 = -7829368;
        if (this.m_Signal > -90.0f) {
            rectangleColor3 = -1;
        }
        paint.setColor(rectangleColor3);
        canvas.drawRect(paddingLeft, paddingTop + ((2.0f * realWidth) / 3.0f), paddingLeft + rectangleWidth, paddingTop + realHeight, this.m_Paint);
        if (this.m_Signal > -70.0f) {
            rectangleColor = -1;
        } else {
            rectangleColor = -7829368;
        }
        paint.setColor(rectangleColor);
        canvas.drawRect(paddingLeft + (2.0f * rectangleWidth), paddingTop + ((1.0f * realWidth) / 3.0f), paddingLeft + (3.0f * rectangleWidth), paddingTop + realHeight, this.m_Paint);
        if (this.m_Signal > -50.0f) {
            rectangleColor2 = -1;
        } else {
            rectangleColor2 = -7829368;
        }
        paint.setColor(rectangleColor2);
        canvas.drawRect(paddingLeft + (4.0f * rectangleWidth), paddingTop, paddingLeft + (5.0f * rectangleWidth), paddingTop + realHeight, this.m_Paint);
    }
}
