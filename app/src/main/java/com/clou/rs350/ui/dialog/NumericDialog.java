package com.clou.rs350.ui.dialog;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.clou.rs350.R;
import com.clou.rs350.utils.DeviceUtil;
import com.clou.rs350.utils.OtherUtils;
import com.itextpdf.text.pdf.PdfObject;

@TargetApi(11)
public class NumericDialog extends AlertDialog implements View.OnClickListener, DialogInterface.OnDismissListener {
    private boolean ClickOK;
    private String SavedValue;
    private Button b0;
    private Button b1;
    private Button b2;
    private Button b3;
    private Button b4;
    private Button b5;
    private Button b6;
    private Button b7;
    private Button b8;
    private Button b9;
    private Button b_clear;
    private Button b_delete;
    private Button b_done;
    private Button b_dot;
    private Button b_minus;
    public Button[] button;
    public int[] button_id;
    private boolean isFristClick;
    Context m_Activity;
    public boolean m_Dot;
    String m_Input;
    private double m_Max;
    private boolean m_MaxFlag;
    private double m_Min;
    private boolean m_MinFlag;
    private boolean m_Minus;
    private TextView m_TextView;
    public int m_x;
    public int m_y;
    public Dialog myDialog;
    private View.OnClickListener onDoneClick;
    private View textEntryView;
    Drawable tvBackground;
    public int txtLimit;

    public void setMax(double Max) {
        this.m_MaxFlag = true;
        this.m_Max = Max;
    }

    public void setMin(double Min) {
        this.m_MinFlag = true;
        this.m_Min = Min;
    }

    public void setMinus(boolean Flag) {
        this.m_Minus = Flag;
    }

    public void setDot(boolean Flag) {
        this.m_Dot = Flag;
    }

    public NumericDialog(Context context, int theme) {
        super(context, theme);
        this.myDialog = null;
        this.m_x = 0;
        this.m_y = 20;
        this.txtLimit = 0;
        this.button = new Button[]{this.b0, this.b1, this.b2, this.b3, this.b4, this.b5, this.b6, this.b7, this.b8, this.b9, this.b_dot, this.b_delete, this.b_clear, this.b_done, this.b_minus};
        this.button_id = new int[]{R.id.b0, R.id.b1, R.id.b2, R.id.b3, R.id.b4, R.id.b5, R.id.b6, R.id.b7, R.id.b8, R.id.b9, R.id.b_dian, R.id.b_jiantou, R.id.b_clear, R.id.b_done, R.id.b_minus};
        this.m_Dot = true;
        this.isFristClick = true;
        this.SavedValue = PdfObject.NOTHING;
        this.ClickOK = false;
        this.m_Minus = true;
        this.onDoneClick = null;
    }

    public NumericDialog(Context context) {
        super(context, R.style.dialog);
        this.myDialog = null;
        this.m_x = 0;
        this.m_y = 20;
        this.txtLimit = 0;
        this.button = new Button[]{this.b0, this.b1, this.b2, this.b3, this.b4, this.b5, this.b6, this.b7, this.b8, this.b9, this.b_dot, this.b_delete, this.b_clear, this.b_done, this.b_minus};
        this.button_id = new int[]{R.id.b0, R.id.b1, R.id.b2, R.id.b3, R.id.b4, R.id.b5, R.id.b6, R.id.b7, R.id.b8, R.id.b9, R.id.b_dian, R.id.b_jiantou, R.id.b_clear, R.id.b_done, R.id.b_minus};
        this.m_Dot = true;
        this.isFristClick = true;
        this.SavedValue = PdfObject.NOTHING;
        this.ClickOK = false;
        this.m_Minus = true;
        this.onDoneClick = null;
        this.m_Activity = context;
    }

    public NumericDialog(Context context, int x, int y) {
        super(context, R.style.dialog);
        this.myDialog = null;
        this.m_x = 0;
        this.m_y = 20;
        this.txtLimit = 0;
        this.button = new Button[]{this.b0, this.b1, this.b2, this.b3, this.b4, this.b5, this.b6, this.b7, this.b8, this.b9, this.b_dot, this.b_delete, this.b_clear, this.b_done, this.b_minus};
        this.button_id = new int[]{R.id.b0, R.id.b1, R.id.b2, R.id.b3, R.id.b4, R.id.b5, R.id.b6, R.id.b7, R.id.b8, R.id.b9, R.id.b_dian, R.id.b_jiantou, R.id.b_clear, R.id.b_done, R.id.b_minus};
        this.m_Dot = true;
        this.isFristClick = true;
        this.SavedValue = PdfObject.NOTHING;
        this.ClickOK = false;
        this.m_Minus = true;
        this.onDoneClick = null;
        this.m_Activity = context;
        this.m_x = x;
        this.m_y = y;
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_numeric_keyboard);
        showMyDialog(null);
    }

    public void showMyDialog(TextView textView) {
        showMyDialog(textView, true);
    }

    public void showMyDialog(TextView textView, boolean canCanceled) {
        if (textView != null) {
            this.isFristClick = true;
            this.SavedValue = textView.getText().toString();
            this.m_TextView = textView;
            this.tvBackground = textView.getBackground();
            this.m_TextView.setBackgroundResource(R.drawable.tablecanedittext_blanchedalmond);
            this.textEntryView = LayoutInflater.from(this.m_Activity).inflate(R.layout.dialog_numeric_keyboard, (ViewGroup) null);
            this.textEntryView.findFocus();
            this.myDialog = new Dialog(this.m_Activity, R.style.dialog_light);
            this.myDialog.show();
            WindowManager.LayoutParams params = this.myDialog.getWindow().getAttributes();
            params.width = this.m_Activity.getResources().getDimensionPixelSize(R.dimen.dp_180);
            params.height = this.m_Activity.getResources().getDimensionPixelSize(R.dimen.dp_300);
            int[] location = new int[2];
            textView.getLocationOnScreen(location);
            int iScreenWidth = DeviceUtil.getWidthMaxPx((Activity) this.m_Activity);
            int iStartX = (iScreenWidth / 2) - (params.width / 2);
            this.m_x = (location[0] + textView.getWidth()) - iStartX;
            if (this.m_x + params.width > iScreenWidth - iStartX && (this.m_x - params.width) - textView.getWidth() >= 0 - iStartX) {
                this.m_x = (this.m_x - params.width) - textView.getWidth();
            }
            params.x = this.m_x;
            params.y = this.m_y;
            this.myDialog.getWindow().setAttributes(params);
            this.myDialog.getWindow().setContentView(this.textEntryView);
            for (int i = 0; i < this.button.length; i++) {
                this.button[i] = (Button) this.textEntryView.findViewById(this.button_id[i]);
                this.button[i].setOnClickListener(this);
            }
            this.myDialog.setCanceledOnTouchOutside(canCanceled);
            this.myDialog.setCancelable(canCanceled);
            this.myDialog.setOnDismissListener(this);
            this.button[10].setEnabled(this.m_Dot);
            this.button[14].setEnabled(this.m_Minus);
        }
    }

    public void onClick(View view) {
        this.m_Input = this.m_TextView.getText().toString();
        if (view.getId() != this.button_id[13]) {
            if (this.isFristClick) {
                if (view.getId() != this.button_id[11]) {
                    this.m_Input = PdfObject.NOTHING;
                }
                this.isFristClick = false;
            }
            String AddString = PdfObject.NOTHING;
            for (int i = 0; i < this.button_id.length; i++) {
                if (view.getId() == this.button_id[i]) {
                    if (i >= 10) {
                        if (i == 10) {
                            if (!this.m_Dot || PdfObject.NOTHING.equals(this.m_Input)) {
                                return;
                            }
                            if (!this.m_Input.contains(".")) {
                                AddString = ".";
                            }
                        }
                        if (i == 11 && this.m_Input.length() > 0) {
                            this.m_TextView.setText(this.m_Input.substring(0, this.m_Input.length() - 1));
                            this.m_Input = this.m_Input.substring(0, this.m_Input.length() - 1);
                        }
                        if (i == 12) {
                            this.m_TextView.setText(PdfObject.NOTHING);
                            this.m_Input = PdfObject.NOTHING;
                        }
                        if (i == 14 && PdfObject.NOTHING.equals(this.m_Input) && this.m_Minus) {
                            AddString = "-";
                        }
                    } else if (AddString.length() < 10) {
                        AddString = new StringBuilder(String.valueOf(i)).toString();
                    } else {
                        Toast.makeText(this.m_Activity, PdfObject.NOTHING, 1).show();
                    }
                }
            }
            if (this.txtLimit == 0 || this.m_Input.length() < this.txtLimit) {
                this.m_TextView.setText(String.valueOf(this.m_Input) + AddString);
            } else {
                Toast.makeText(this.m_Activity, String.valueOf(this.m_Activity.getResources().getString(R.string.text_text_limit)) + " " + this.txtLimit, 0).show();
            }
        } else if (!this.m_MaxFlag || OtherUtils.isEmpty(this.m_Input) || Double.valueOf(this.m_Input).doubleValue() <= this.m_Max) {
            if (this.m_MinFlag) {
                if (OtherUtils.isEmpty(this.m_Input)) {
                    String tmpString = this.m_Activity.getResources().getString(R.string.text_min_input);
                    if (this.m_Dot) {
                        Toast.makeText(this.m_Activity, String.valueOf(tmpString) + " " + this.m_Min, 0).show();
                        return;
                    } else {
                        Toast.makeText(this.m_Activity, String.valueOf(tmpString) + " " + ((int) this.m_Min), 0).show();
                        return;
                    }
                } else if (Double.valueOf(this.m_Input).doubleValue() < this.m_Min) {
                    String tmpString2 = this.m_Activity.getResources().getString(R.string.text_min_input);
                    if (this.m_Dot) {
                        Toast.makeText(this.m_Activity, String.valueOf(tmpString2) + " " + this.m_Min, 0).show();
                        return;
                    } else {
                        Toast.makeText(this.m_Activity, String.valueOf(tmpString2) + " " + ((int) this.m_Min), 0).show();
                        return;
                    }
                }
            }
            this.ClickOK = true;
            this.myDialog.dismiss();
        } else {
            String tmpString3 = this.m_Activity.getResources().getString(R.string.text_max_input);
            if (this.m_Dot) {
                Toast.makeText(this.m_Activity, String.valueOf(tmpString3) + " " + this.m_Max, 0).show();
            } else {
                Toast.makeText(this.m_Activity, String.valueOf(tmpString3) + " " + ((int) this.m_Max), 0).show();
            }
        }
    }

    public void checksize() {
        this.m_Input = this.m_TextView.getText().toString();
        if (PdfObject.NOTHING.equals(this.m_Input) || this.m_Input == null) {
            this.m_Input = "00";
        } else if (Double.valueOf(this.m_TextView.getText().toString()).doubleValue() == 0.0d) {
            this.m_TextView.setText("0");
            return;
        }
        if (!this.m_Input.equals("00")) {
            this.m_TextView.setText(this.m_Input.replaceAll("^(0+)", PdfObject.NOTHING));
            String edit_ = this.m_TextView.getText().toString();
            if (edit_.substring(0, 1).equals(".")) {
                this.m_TextView.setText("0" + edit_);
            }
        }
        if (this.m_Input.equals("00")) {
            this.m_TextView.setText("0");
        }
        if (this.m_Input.substring(this.m_Input.length() - 1, this.m_Input.length()).equals(".")) {
            this.m_TextView.setText(this.m_Input.substring(0, this.m_Input.length() - 1));
        }
        if (this.m_Input.substring(0, 1).equals(".")) {
            this.m_TextView.setText("0" + this.m_Input);
        }
    }

    public void onDismiss(DialogInterface dialog) {
        if (!this.ClickOK) {
            this.m_TextView.setText(this.SavedValue);
        } else if (this.onDoneClick != null) {
            this.onDoneClick.onClick(this.m_TextView);
        }
        this.m_TextView.setBackgroundDrawable(this.tvBackground);
    }

    public void setOnDoneClick(View.OnClickListener onDoneClick2) {
        this.onDoneClick = onDoneClick2;
    }
}
