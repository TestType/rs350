package com.clou.rs350.ui.popwindow;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.NumberPicker;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.clou.rs350.R;
import com.itextpdf.xmp.XMPError;

import java.util.Calendar;

public class YearSettingPopWindow extends PopupWindow implements View.OnClickListener, CompoundButton.OnCheckedChangeListener {
    TextView TargetView;
    Context m_Context;
    View.OnClickListener m_OnClick;
    NumberPicker np_Year;
    private String[] sYear = new String[XMPError.BADXML];

    public YearSettingPopWindow(Context context) {
        super(context);
        this.m_Context = context;
        initView();
        setTime();
    }

    private void setTime() {
        this.np_Year.setValue(Calendar.getInstance().get(1));
    }

    private void initView() {
        View conentView = ((LayoutInflater) this.m_Context.getSystemService("layout_inflater")).inflate(R.layout.pop_window_year_setting, (ViewGroup) null);
        for (int i = 0; i <= 200; i++) {
            this.sYear[i] = new StringBuilder(String.valueOf(i + 1900)).toString();
        }
        conentView.findViewById(R.id.ok).setOnClickListener(this);
        conentView.findViewById(R.id.cancel).setOnClickListener(this);
        this.np_Year = (NumberPicker) conentView.findViewById(R.id.np_year);
        this.np_Year.setDisplayedValues(this.sYear);
        this.np_Year.setMinValue(1900);
        this.np_Year.setMaxValue(2100);
        setContentView(conentView);
        setWidth(this.m_Context.getResources().getDimensionPixelSize(R.dimen.dp_180));
        setHeight(this.m_Context.getResources().getDimensionPixelSize(R.dimen.dp_150));
        setFocusable(true);
        setOutsideTouchable(true);
        update();
        setBackgroundDrawable(new ColorDrawable(0));
        setAnimationStyle(16973826);
    }

    public void showPopWindow(View currentClick, View.OnClickListener onclick) {
        showPopWindow(currentClick, null, onclick);
    }

    public void showPopWindow(View currentClick, View parentView, View.OnClickListener onclick) {
        this.m_OnClick = onclick;
        this.TargetView = (TextView) currentClick;
        setSoftInputMode(16);
        if (!isShowing()) {
            int[] location = new int[2];
            currentClick.getLocationOnScreen(location);
            if (parentView != null) {
                showAtLocation(parentView, 0, location[0] + ((currentClick.getWidth() - getWidth()) / 2), location[1] + currentClick.getHeight());
            } else {
                showAtLocation(currentClick, 0, location[0] + ((currentClick.getWidth() - getWidth()) / 2), location[1] + currentClick.getHeight());
            }
        } else {
            dismiss();
        }
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ok:
                this.TargetView.setText(new StringBuilder(String.valueOf(this.np_Year.getValue())).toString());
                if (this.m_OnClick != null) {
                    this.m_OnClick.onClick(this.TargetView);
                }
                if (isShowing()) {
                    dismiss();
                    return;
                }
                return;
            case R.id.cancel:
                if (isShowing()) {
                    dismiss();
                    return;
                }
                return;
            default:
                return;
        }
    }

    public void onCheckedChanged(CompoundButton arg0, boolean arg1) {
    }
}
