package com.clou.rs350.ui.popwindow;

import android.app.Activity;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.clou.rs350.R;
import com.clou.rs350.ui.adapter.MyAdapter;
import com.clou.rs350.utils.DeviceUtil;

import java.util.Arrays;

public class ListPopWindow {
    public static void showListPopwindow(Context context, String[] array, View currentClick, View.OnClickListener onclick) {
        showListPopwindow(context, array, currentClick, onclick, 0);
    }

    public static void showListLRPopwindow(Context context, String[] array, View currentClick, View.OnClickListener onclick) {
        showListLRPopwindow(context, array, currentClick, onclick, 0, true, null);
    }

    public static void showListPopwindow(Context context, String[] array, View currentClick, View.OnClickListener onclick, int location) {
        showListPopwindow(context, array, currentClick, onclick, location, true);
    }

    public static void showListPopwindow(Context context, String[] array, View currentClick, View.OnClickListener onclick, int location, boolean showText) {
        showListPopwindow(context, array, currentClick, onclick, location, showText, null);
    }

    public static void showListPopwindow(Context context, String[] array, View currentClick, View.OnClickListener onclick, int location, boolean showText, View parentView) {
        showListPopwindow(context, array, currentClick, onclick, location, showText, parentView, null);
    }

    public static void showListLRPopwindow(Context context, String[] array, View currentClick, View.OnClickListener onclick, int location, boolean showText, View parentView) {
        showListLRPopwindow(context, array, currentClick, onclick, location, showText, parentView, null);
    }

    public static void showListPopwindow(Context context, String[] array, View currentClick, View.OnClickListener onclick, int locationVertical, boolean showText, View parentView, View parentDialog) {
        showListPopwindow(context, array, currentClick, onclick, locationVertical, 2, showText, parentView, parentDialog);
    }

    public static void showListLRPopwindow(Context context, String[] array, View currentClick, View.OnClickListener onclick, int locationHorizontal, boolean showText, View parentView, View parentDialog) {
        showListPopwindow(context, array, currentClick, onclick, 0, locationHorizontal, showText, parentView, parentDialog);
    }

    public static void showListPopwindow(Context context, String[] array, final View currentClick, final View.OnClickListener onclick, int locationVertical, int locationHorizontal, final boolean showText, View parentView, View parentDialog) {
        if (array != null) {
            View contentView = View.inflate(context.getApplicationContext(), R.layout.pop_window_selection, null);
            MyAdapter adapter = new MyAdapter(Arrays.asList(array), context);
            ListView listView = (ListView) contentView.findViewById(R.id.type_items_listview);
            listView.setAdapter((ListAdapter) adapter);
            final TextView currentView = (TextView) currentClick;
            final PopupWindow popWindow = new PopupWindow(contentView, currentView.getWidth() + context.getResources().getDimensionPixelSize(R.dimen.dp_20), context.getResources().getDimensionPixelSize(R.dimen.dp_120));
            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                /* class com.clou.rs350.ui.popwindow.ListPopWindow.AnonymousClass1 */

                @Override // android.widget.AdapterView.OnItemClickListener
                public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                    popWindow.dismiss();
                    TextView tv = (TextView) view.findViewById(R.id.apt_list_textview);
                    if (currentView != null) {
                        if (showText) {
                            currentView.setText(tv.getText());
                        }
                        currentView.setTag(Integer.valueOf(position));
                    }
                    if (onclick != null) {
                        onclick.onClick(currentClick);
                    }
                }
            });
            popWindow.setFocusable(true);
            popWindow.setOutsideTouchable(true);
            popWindow.setBackgroundDrawable(new ColorDrawable(0));
            popWindow.setAnimationStyle(16973826);
            if (popWindow.isShowing() || currentView == null) {
                popWindow.dismiss();
                return;
            }
            int[] poplocation = new int[2];
            currentView.getLocationOnScreen(poplocation);
            int x = poplocation[0];
            int y = poplocation[1];
            switch (locationVertical) {
                case 0:
                    y += currentView.getHeight();
                    break;
                case 1:
                    y -= popWindow.getHeight();
                    break;
            }
            switch (locationHorizontal) {
                case 0:
                    x += currentView.getWidth();
                    break;
                case 1:
                    x -= popWindow.getWidth();
                    break;
            }
            int x2 = x - context.getResources().getDimensionPixelSize(R.dimen.dp_10);
            if (parentDialog != null) {
                x2 -= getMarginLeft(context, parentDialog);
                y -= getMarginTop(context, parentDialog);
            }
            popWindow.setSoftInputMode(16);
            if (parentView != null) {
                popWindow.showAtLocation(parentView, 0, x2, y);
            } else {
                popWindow.showAtLocation(currentClick, 0, x2, y);
            }
        }
    }

    private static int getMarginLeft(Context context, View parentDialog) {
        return (DeviceUtil.getWidthMaxPx((Activity) context) - parentDialog.getLayoutParams().width) / 2;
    }

    private static int getMarginTop(Context context, View parentDialog) {
        return (DeviceUtil.getHeightMaxPx((Activity) context) - parentDialog.getLayoutParams().height) / 2;
    }
}
