package com.clou.rs350.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.clou.rs350.R;
import com.clou.rs350.device.model.ExplainedModel;

public class PowerHarmonicListViewAdapter extends BaseAdapter {
    private Context context;
    private ExplainedModel[] pValues;
    private ExplainedModel[] qValues;
    private ExplainedModel[] sValues;

    public PowerHarmonicListViewAdapter(Context context2) {
        this.context = context2;
    }

    public void refreshData(ExplainedModel[] pValues2, ExplainedModel[] qValues2, ExplainedModel[] sValues2) {
        this.pValues = pValues2;
        this.qValues = qValues2;
        this.sValues = sValues2;
        notifyDataSetChanged();
    }

    public int getCount() {
        if (this.pValues != null) {
            return this.pValues.length;
        }
        return 0;
    }

    public Object getItem(int arg0) {
        return this.pValues[arg0];
    }

    public long getItemId(int arg0) {
        return 0;
    }

    public View getView(int position, View view, ViewGroup arg2) {
        ViewHolder holder;
        if (view == null) {
            view = LayoutInflater.from(this.context).inflate(R.layout.adapter_power_harmonic, (ViewGroup) null);
            holder = new ViewHolder(view);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }
        holder.showData(position + 1, this.pValues[position], this.qValues[position], this.sValues[position]);
        return view;
    }

    class ViewHolder {
        TextView itemTv;
        TextView pTv;
        TextView qTv;
        TextView sTv;

        ViewHolder(View v) {
            this.itemTv = (TextView) v.findViewById(R.id.power_itemText);
            this.pTv = (TextView) v.findViewById(R.id.power_p);
            this.qTv = (TextView) v.findViewById(R.id.power_q);
            this.sTv = (TextView) v.findViewById(R.id.power_s);
        }

        public void showData(int index, ExplainedModel pValues, ExplainedModel qValues, ExplainedModel sValues) {
            this.itemTv.setText(new StringBuilder(String.valueOf(index)).toString());
            this.pTv.setText(pValues == null ? "---" : pValues.getStringValue());
            this.qTv.setText(qValues == null ? "---" : qValues.getStringValue());
            this.sTv.setText(sValues == null ? "---" : sValues.getStringValue());
        }
    }
}
