package com.clou.rs350.ui.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.clou.rs350.R;

public class UpdateDialog extends Dialog implements View.OnClickListener {
    private Button btn_Cancel = null;
    private Button btn_OK = null;
    private View.OnClickListener cancelClick = null;
    private Context m_Context = null;
    private View.OnClickListener okClick = null;
    private TextView txt_Describe = null;
    private TextView txt_Title = null;

    public UpdateDialog(Context context) {
        super(context, R.style.dialog);
        requestWindowFeature(1);
        setCanceledOnTouchOutside(true);
        this.m_Context = context;
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_update);
        this.txt_Title = (TextView) findViewById(R.id.txt_title);
        this.txt_Describe = (TextView) findViewById(R.id.txt_describe);
        this.txt_Describe.setMovementMethod(ScrollingMovementMethod.getInstance());
        this.btn_OK = (Button) findViewById(R.id.btn_ok);
        this.btn_OK.setOnClickListener(this);
        this.btn_Cancel = (Button) findViewById(R.id.btn_cancel);
        this.btn_Cancel.setOnClickListener(this);
    }

    @Override // android.app.Dialog
    public void setTitle(CharSequence title) {
        this.txt_Title.setText(title.toString());
    }

    @Override // android.app.Dialog
    public void setTitle(int titleId) {
        this.txt_Title.setText(titleId);
    }

    public void setDescript(CharSequence descript) {
        this.txt_Describe.setText(descript.toString());
    }

    public void setOkText(CharSequence text) {
        this.btn_OK.setText(text);
    }

    public void setOkText(int id) {
        this.btn_OK.setText(id);
    }

    public void setPositiveButton(int textId, View.OnClickListener listener) {
        this.btn_OK.setText(textId);
        this.okClick = listener;
    }

    public void setNegativeButton(int textId, View.OnClickListener listener) {
        this.btn_Cancel.setText(textId);
        this.cancelClick = listener;
    }

    public void setCancleText(CharSequence text) {
        this.btn_Cancel.setText(text);
    }

    public void onClick(View v) {
        dismiss();
        switch (v.getId()) {
            case R.id.btn_cancel:
                if (this.cancelClick != null) {
                    this.cancelClick.onClick(v);
                }
                dismiss();
                return;
            case R.id.btn_ok:
                if (this.okClick != null) {
                    this.okClick.onClick(v);
                }
                dismiss();
                return;
            default:
                return;
        }
    }

    public void setOnClick(View.OnClickListener onClickListener, View.OnClickListener cancleClick) {
        this.okClick = onClickListener;
        this.cancelClick = cancleClick;
    }
}
