package com.clou.rs350.ui.view.infoview;

import android.content.Context;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.clou.rs350.R;
import com.clou.rs350.db.dao.DigitalMeterTypeDao;
import com.clou.rs350.db.model.DigitalMeterType;
import com.clou.rs350.ui.dialog.HintDialog;
import com.clou.rs350.ui.dialog.NormalDialog;
import com.clou.rs350.ui.dialog.NumericDialog;
import com.clou.rs350.ui.popwindow.ListPopWindow;
import com.clou.rs350.utils.DoubleClick;
import com.clou.rs350.utils.OtherUtils;
import com.itextpdf.text.pdf.PdfObject;

import java.util.List;

public class DigitalMeterTypeView implements View.OnClickListener {
    Button btn_Cancel = null;
    Button btn_Delete = null;
    Button btn_Modify = null;
    Button btn_New = null;
    Button btn_Save = null;
    LinearLayout btn_Select;
    int[] m_Constants = new int[3];
    Context m_Context = null;
    DigitalMeterTypeDao m_Dao;
    String[] m_DataTypes = null;
    int m_Function = 1;
    boolean m_HasSearch = false;
    DigitalMeterType m_Info;
    private DoubleClick selectClick = null;
    TextView txt_ActiveAddress = null;
    TextView txt_ActiveDataType = null;
    TextView txt_ActiveLength = null;
    TextView txt_ActiveMultiple = null;
    TextView txt_ApparentAddress = null;
    TextView txt_ApparentDataType = null;
    TextView txt_ApparentLength = null;
    TextView txt_ApparentMultiple = null;
    TextView txt_ReactiveAddress = null;
    TextView txt_ReactiveDataType = null;
    TextView txt_ReactiveLength = null;
    TextView txt_ReactiveMultiple = null;
    EditText txt_Type;

    public DigitalMeterTypeView(Context context, View view) {
        this.m_Context = context;
        this.m_Dao = new DigitalMeterTypeDao(this.m_Context);
        this.m_DataTypes = this.m_Context.getResources().getStringArray(R.array.digial_meter_data_type);
        this.txt_Type = (EditText) view.findViewById(R.id.txt_type);
        this.txt_Type.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            /* class com.clou.rs350.ui.view.infoview.DigitalMeterTypeView.AnonymousClass1 */

            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if ((actionId == 6 || actionId == 5) && 3 != DigitalMeterTypeView.this.m_Function) {
                    String id = DigitalMeterTypeView.this.txt_Type.getText().toString();
                    DigitalMeterTypeView.this.searchData(id);
                    DigitalMeterTypeView.this.txt_Type.setSelection(id.length());
                }
                Log.i("actionId", new StringBuilder(String.valueOf(actionId)).toString());
                return false;
            }
        });
        this.btn_Select = (LinearLayout) view.findViewById(R.id.btn_select);
        this.btn_Select.setOnClickListener(this);
        showData();
        this.selectClick = new DoubleClick(new DoubleClick.OnDoubleClick() {
            /* class com.clou.rs350.ui.view.infoview.DigitalMeterTypeView.AnonymousClass2 */

            private void searchKey(String condition) {
                List<String> tmpNames = DigitalMeterTypeView.this.m_Dao.queryAllKey(condition);
                ListPopWindow.showListPopwindow(DigitalMeterTypeView.this.m_Context, (String[]) tmpNames.toArray(new String[tmpNames.size()]), DigitalMeterTypeView.this.txt_Type, new View.OnClickListener() {
                    /* class com.clou.rs350.ui.view.infoview.DigitalMeterTypeView.AnonymousClass2.AnonymousClass1 */

                    public void onClick(View v) {
                        DigitalMeterTypeView.this.searchData(((TextView) v).getText().toString());
                    }
                });
            }

            @Override // com.clou.rs350.utils.DoubleClick.OnDoubleClick
            public void doubleClick() {
                searchKey(DigitalMeterTypeView.this.txt_Type.getText().toString());
            }

            @Override // com.clou.rs350.utils.DoubleClick.OnDoubleClick
            public void singleClick() {
                searchKey(PdfObject.NOTHING);
            }
        });
        this.btn_New = (Button) view.findViewById(R.id.btn_new);
        this.btn_New.setOnClickListener(this);
        this.btn_Modify = (Button) view.findViewById(R.id.btn_modify);
        this.btn_Modify.setOnClickListener(this);
        this.btn_Delete = (Button) view.findViewById(R.id.btn_delete);
        this.btn_Delete.setOnClickListener(this);
        this.btn_Save = (Button) view.findViewById(R.id.btn_save);
        this.btn_Save.setOnClickListener(this);
        this.btn_Cancel = (Button) view.findViewById(R.id.btn_cancel);
        this.btn_Cancel.setOnClickListener(this);
        this.txt_ActiveAddress = (TextView) view.findViewById(R.id.txt_active_address);
        this.txt_ActiveAddress.setOnClickListener(this);
        this.txt_ActiveLength = (TextView) view.findViewById(R.id.txt_active_length);
        this.txt_ActiveLength.setOnClickListener(this);
        this.txt_ActiveDataType = (TextView) view.findViewById(R.id.txt_active_data_type);
        this.txt_ActiveDataType.setOnClickListener(this);
        this.txt_ActiveMultiple = (TextView) view.findViewById(R.id.txt_active_multiple);
        this.txt_ActiveMultiple.setOnClickListener(this);
        this.txt_ReactiveAddress = (TextView) view.findViewById(R.id.txt_reactive_address);
        this.txt_ReactiveAddress.setOnClickListener(this);
        this.txt_ReactiveLength = (TextView) view.findViewById(R.id.txt_reactive_length);
        this.txt_ReactiveLength.setOnClickListener(this);
        this.txt_ReactiveDataType = (TextView) view.findViewById(R.id.txt_reactive_data_type);
        this.txt_ReactiveDataType.setOnClickListener(this);
        this.txt_ReactiveMultiple = (TextView) view.findViewById(R.id.txt_reactive_multiple);
        this.txt_ReactiveMultiple.setOnClickListener(this);
        this.txt_ApparentAddress = (TextView) view.findViewById(R.id.txt_apparent_address);
        this.txt_ApparentAddress.setOnClickListener(this);
        this.txt_ApparentLength = (TextView) view.findViewById(R.id.txt_apparent_length);
        this.txt_ApparentLength.setOnClickListener(this);
        this.txt_ApparentDataType = (TextView) view.findViewById(R.id.txt_apparent_data_type);
        this.txt_ApparentDataType.setOnClickListener(this);
        this.txt_ApparentMultiple = (TextView) view.findViewById(R.id.txt_apparent_multiple);
        this.txt_ApparentMultiple.setOnClickListener(this);
        setEditButton(this.m_Function);
    }

    public void setEditButton(int function) {
        boolean z;
        boolean z2;
        boolean z3;
        boolean z4;
        boolean z5;
        boolean z6;
        boolean z7;
        boolean z8;
        boolean z9;
        boolean z10;
        boolean z11;
        boolean z12;
        boolean z13;
        boolean z14;
        boolean z15;
        boolean z16;
        boolean z17;
        boolean z18 = true;
        this.m_Function = function;
        this.txt_Type.setEnabled(this.m_Function == 1 || this.m_Function == 3);
        LinearLayout linearLayout = this.btn_Select;
        if (this.m_Function == 1) {
            z = true;
        } else {
            z = false;
        }
        linearLayout.setEnabled(z);
        Button button = this.btn_New;
        if (this.m_Function == 1) {
            z2 = true;
        } else {
            z2 = false;
        }
        button.setEnabled(z2);
        Button button2 = this.btn_Modify;
        if (this.m_Function == 1) {
            z3 = true;
        } else {
            z3 = false;
        }
        button2.setEnabled(z3);
        Button button3 = this.btn_Delete;
        if (this.m_Function == 1) {
            z4 = true;
        } else {
            z4 = false;
        }
        button3.setEnabled(z4);
        Button button4 = this.btn_Save;
        if (this.m_Function != 1) {
            z5 = true;
        } else {
            z5 = false;
        }
        button4.setEnabled(z5);
        Button button5 = this.btn_Cancel;
        if (this.m_Function != 1) {
            z6 = true;
        } else {
            z6 = false;
        }
        button5.setEnabled(z6);
        TextView textView = this.txt_ActiveAddress;
        if (this.m_Function > 1) {
            z7 = true;
        } else {
            z7 = false;
        }
        textView.setEnabled(z7);
        TextView textView2 = this.txt_ActiveLength;
        if (this.m_Function > 1) {
            z8 = true;
        } else {
            z8 = false;
        }
        textView2.setEnabled(z8);
        TextView textView3 = this.txt_ActiveDataType;
        if (this.m_Function > 1) {
            z9 = true;
        } else {
            z9 = false;
        }
        textView3.setEnabled(z9);
        TextView textView4 = this.txt_ActiveMultiple;
        if (this.m_Function > 1) {
            z10 = true;
        } else {
            z10 = false;
        }
        textView4.setEnabled(z10);
        TextView textView5 = this.txt_ReactiveAddress;
        if (this.m_Function > 1) {
            z11 = true;
        } else {
            z11 = false;
        }
        textView5.setEnabled(z11);
        TextView textView6 = this.txt_ReactiveLength;
        if (this.m_Function > 1) {
            z12 = true;
        } else {
            z12 = false;
        }
        textView6.setEnabled(z12);
        TextView textView7 = this.txt_ReactiveDataType;
        if (this.m_Function > 1) {
            z13 = true;
        } else {
            z13 = false;
        }
        textView7.setEnabled(z13);
        TextView textView8 = this.txt_ReactiveMultiple;
        if (this.m_Function > 1) {
            z14 = true;
        } else {
            z14 = false;
        }
        textView8.setEnabled(z14);
        TextView textView9 = this.txt_ApparentAddress;
        if (this.m_Function > 1) {
            z15 = true;
        } else {
            z15 = false;
        }
        textView9.setEnabled(z15);
        TextView textView10 = this.txt_ApparentLength;
        if (this.m_Function > 1) {
            z16 = true;
        } else {
            z16 = false;
        }
        textView10.setEnabled(z16);
        TextView textView11 = this.txt_ApparentDataType;
        if (this.m_Function > 1) {
            z17 = true;
        } else {
            z17 = false;
        }
        textView11.setEnabled(z17);
        TextView textView12 = this.txt_ApparentMultiple;
        if (this.m_Function <= 1) {
            z18 = false;
        }
        textView12.setEnabled(z18);
    }

    public void refreshData(DigitalMeterType info) {
        this.m_Info = info;
        showData();
    }

    private void showData() {
        if (this.m_Info != null) {
            this.txt_Type.setText(this.m_Info.Type);
            this.txt_ActiveAddress.setText(new StringBuilder(String.valueOf(this.m_Info.DataType[0].Address)).toString());
            this.txt_ActiveLength.setText(new StringBuilder(String.valueOf(this.m_Info.DataType[0].Length)).toString());
            this.txt_ActiveDataType.setText(this.m_DataTypes[this.m_Info.DataType[0].DataType]);
            this.txt_ActiveDataType.setTag(Integer.valueOf(this.m_Info.DataType[0].DataType));
            this.txt_ActiveMultiple.setText(new StringBuilder(String.valueOf(this.m_Info.DataType[0].Multiple)).toString());
            this.txt_ReactiveAddress.setText(new StringBuilder(String.valueOf(this.m_Info.DataType[1].Address)).toString());
            this.txt_ReactiveLength.setText(new StringBuilder(String.valueOf(this.m_Info.DataType[1].Length)).toString());
            this.txt_ReactiveDataType.setText(this.m_DataTypes[this.m_Info.DataType[1].DataType]);
            this.txt_ReactiveDataType.setTag(Integer.valueOf(this.m_Info.DataType[0].DataType));
            this.txt_ReactiveMultiple.setText(new StringBuilder(String.valueOf(this.m_Info.DataType[1].Multiple)).toString());
            this.txt_ApparentAddress.setText(new StringBuilder(String.valueOf(this.m_Info.DataType[2].Address)).toString());
            this.txt_ApparentLength.setText(new StringBuilder(String.valueOf(this.m_Info.DataType[2].Length)).toString());
            this.txt_ApparentDataType.setText(this.m_DataTypes[this.m_Info.DataType[2].DataType]);
            this.txt_ApparentDataType.setTag(Integer.valueOf(this.m_Info.DataType[0].DataType));
            this.txt_ApparentMultiple.setText(new StringBuilder(String.valueOf(this.m_Info.DataType[2].Multiple)).toString());
        }
    }

    private void parseData() {
        this.m_Info.Type = this.txt_Type.getText().toString();
        this.m_Info.DataType[0].Address = OtherUtils.parseInt(this.txt_ActiveAddress.getText().toString());
        this.m_Info.DataType[0].Length = OtherUtils.parseInt(this.txt_ActiveLength.getText().toString());
        this.m_Info.DataType[0].DataType = OtherUtils.convertToInt(this.txt_ActiveDataType.getTag());
        this.m_Info.DataType[0].Multiple = (float) OtherUtils.parseDouble(this.txt_ActiveMultiple.getText().toString());
        this.m_Info.DataType[1].Address = OtherUtils.parseInt(this.txt_ReactiveAddress.getText().toString());
        this.m_Info.DataType[1].Length = OtherUtils.parseInt(this.txt_ReactiveLength.getText().toString());
        this.m_Info.DataType[1].DataType = OtherUtils.convertToInt(this.txt_ReactiveDataType.getTag());
        this.m_Info.DataType[1].Multiple = (float) OtherUtils.parseDouble(this.txt_ReactiveMultiple.getText().toString());
        this.m_Info.DataType[2].Address = OtherUtils.parseInt(this.txt_ApparentAddress.getText().toString());
        this.m_Info.DataType[2].Length = OtherUtils.parseInt(this.txt_ApparentLength.getText().toString());
        this.m_Info.DataType[2].DataType = OtherUtils.convertToInt(this.txt_ApparentDataType.getTag());
        this.m_Info.DataType[2].Multiple = (float) OtherUtils.parseDouble(this.txt_ApparentMultiple.getText().toString());
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private void searchData(String type) {
        this.m_Info = this.m_Dao.queryObjectByType(type);
        if (this.m_Info != null) {
            this.m_HasSearch = true;
        } else {
            this.m_HasSearch = false;
            this.m_Info = new DigitalMeterType();
            this.m_Info.Type = type;
        }
        refreshData(this.m_Info);
    }

    private void Save() {
        String Name = this.txt_Type.getText().toString();
        if (3 != this.m_Function || !OtherUtils.isEmpty(Name)) {
            if (3 != this.m_Function) {
                this.m_Dao.deleteRecordByKey(Name);
            } else if (this.m_Dao.queryObjectByType(Name) != null) {
                showHint(R.string.text_meter_type_exists);
                return;
            }
            parseData();
            this.m_Dao.insertObject(this.m_Info);
            setEditButton(1);
            showHint(R.string.text_save_success);
            return;
        }
        showHint(R.string.text_meter_type_empty);
    }

    public void onClick(View v) {
        NumericDialog numDialog = new NumericDialog(this.m_Context);
        switch (v.getId()) {
            case R.id.btn_delete:
                if (!this.m_HasSearch) {
                    showHint(R.string.text_meter_type_wrong);
                    return;
                } else if (OtherUtils.isEmpty(this.txt_Type.getText().toString())) {
                    showHint(R.string.text_meter_type_empty);
                    return;
                } else {
                    NormalDialog dialog = new NormalDialog(this.m_Context);
                    dialog.setOnClick(new View.OnClickListener() {
                        /* class com.clou.rs350.ui.view.infoview.DigitalMeterTypeView.AnonymousClass3 */

                        public void onClick(View v) {
                            DigitalMeterTypeView.this.m_Dao.deleteRecordByKey(DigitalMeterTypeView.this.txt_Type.getText().toString());
                            DigitalMeterTypeView.this.m_Info = new DigitalMeterType();
                            DigitalMeterTypeView.this.refreshData(DigitalMeterTypeView.this.m_Info);
                        }
                    }, null);
                    dialog.show();
                    dialog.setTitle(R.string.text_meter_type_delete);
                    return;
                }
            case R.id.btn_save:
                Save();
                return;
            case R.id.btn_select:
                this.selectClick.click();
                return;
            case R.id.btn_cancel:
                setEditButton(1);
                refreshData(this.m_Info);
                return;
            case R.id.btn_new:
                setEditButton(3);
                refreshData(new DigitalMeterType());
                return;
            case R.id.btn_modify:
                if (!this.m_HasSearch) {
                    showHint(R.string.text_meter_type_wrong);
                    return;
                } else if (OtherUtils.isEmpty(this.txt_Type.getText().toString())) {
                    showHint(R.string.text_meter_type_empty);
                    return;
                } else {
                    setEditButton(2);
                    return;
                }
            case R.id.txt_active_address:
            case R.id.txt_active_length:
            case R.id.txt_reactive_address:
            case R.id.txt_reactive_length:
            case R.id.txt_apparent_address:
            case R.id.txt_apparent_length:
                numDialog.m_Dot = false;
                numDialog.setMinus(false);
                numDialog.showMyDialog((TextView) v);
                return;
            case R.id.txt_active_data_type:
            case R.id.txt_reactive_data_type:
            case R.id.txt_apparent_data_type:
                ListPopWindow.showListPopwindow(this.m_Context, this.m_DataTypes, v, null, 0, true, null);
                return;
            case R.id.txt_active_multiple:
            case R.id.txt_reactive_multiple:
            case R.id.txt_apparent_multiple:
                numDialog.setMinus(false);
                numDialog.showMyDialog((TextView) v);
                return;
            default:
                numDialog.setMinus(false);
                numDialog.showMyDialog((TextView) v);
                return;
        }
    }

    private void showHint(int title) {
        new HintDialog(this.m_Context, title).show();
    }
}
