package com.clou.rs350.ui.view;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.View;

import com.clou.rs350.R;
import com.itextpdf.text.pdf.ColumnText;

public class MyScanView extends View {
    private static final String TAG = "ScanView";
    private Context m_Context;
    private Paint m_Paint = null;
    float m_Size = ColumnText.GLOBAL_SPACE_CHAR_RATIO;

    public MyScanView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.m_Context = context;
        init();
    }

    private void init() {
        this.m_Paint = new Paint();
        this.m_Paint.setAntiAlias(true);
        this.m_Paint.setStrokeWidth((float) ((((int) getValue(R.dimen.dp_2, 2.0f)) / 2) * 2));
        this.m_Paint.setColor(-1);
    }

    private float getValue(int resId, float defaultValue) {
        return this.m_Context == null ? defaultValue : this.m_Context.getResources().getDimension(resId);
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        float width = (float) getWidth();
        float height = (float) getHeight();
        this.m_Size = width > height ? height * 0.9f : width * 0.9f;
        float x = (width - this.m_Size) / 2.0f;
        float y = (height - this.m_Size) / 2.0f;
        float spaceStart = this.m_Size * 0.2f;
        float spaceStop = this.m_Size * 0.8f;
        canvas.drawLine(x, y, x + spaceStart, y, this.m_Paint);
        canvas.drawLine(x + spaceStop, y, x + this.m_Size, y, this.m_Paint);
        canvas.drawLine(x, y + this.m_Size, x + spaceStart, y + this.m_Size, this.m_Paint);
        canvas.drawLine(x + spaceStop, y + this.m_Size, x + this.m_Size, y + this.m_Size, this.m_Paint);
        canvas.drawLine(x, y, x, y + spaceStart, this.m_Paint);
        canvas.drawLine(x, y + spaceStop, x, y + this.m_Size, this.m_Paint);
        canvas.drawLine(x + this.m_Size, y, x + this.m_Size, y + spaceStart, this.m_Paint);
        canvas.drawLine(x + this.m_Size, y + spaceStop, x + this.m_Size, y + this.m_Size, this.m_Paint);
        canvas.drawLine(x, height / 2.0f, x + this.m_Size, height / 2.0f, this.m_Paint);
    }
}
