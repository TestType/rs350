package com.clou.rs350.ui.view;

import android.content.Context;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.clou.rs350.R;
import com.clou.rs350.db.model.Range;
import com.clou.rs350.manager.MessageManager;
import com.clou.rs350.ui.dialog.HintDialog;
import com.clou.rs350.ui.popwindow.RangePopWindow;
import com.clou.rs350.ui.popwindow.WiringPopWindow;
import com.itextpdf.text.pdf.PdfObject;
import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.view.annotation.ViewInject;
import com.lidroid.xutils.view.annotation.event.OnClick;

public class WiringAndRangeView {
    private String[] OverloadHints;
    @ViewInject(R.id.L1_show)
    private TextView l1TextView;
    @ViewInject(R.id.L2_show)
    private TextView l2TextView;
    @ViewInject(R.id.L3_show)
    private TextView l3TextView;
    Context m_Context;
    private HintDialog m_Dialog;
    private View.OnClickListener m_OverloadClick = null;
    private Range m_Range;
    private boolean m_RangeHasChange = false;
    String[] m_Ranges = {PdfObject.NOTHING, PdfObject.NOTHING, PdfObject.NOTHING};
    @ViewInject(R.id.realtime_range)
    private Button rangeButton;
    String[] sCurrentConnectType;
    String[] sDefinition;
    String[] sVoltageConnectType;
    @ViewInject(R.id.realtime_wiring)
    private Button wiringButton;
    @ViewInject(R.id.txt_wiring)
    private TextView wiringTextView;

    public WiringAndRangeView(Context context, View view) {
        this.m_Context = context;
        this.m_Range = new Range();
        ViewUtils.inject(this, view);
        this.OverloadHints = this.m_Context.getResources().getStringArray(R.array.overloadhint);
        this.sDefinition = this.m_Context.getResources().getStringArray(R.array.wiring_definition);
        this.sVoltageConnectType = this.m_Context.getResources().getStringArray(R.array.voltage_connect_type);
        this.sCurrentConnectType = this.m_Context.getResources().getStringArray(R.array.current_connect_type);
    }

    public void setEnabled(boolean enabled) {
        this.wiringButton.setEnabled(enabled);
        this.rangeButton.setEnabled(enabled);
    }

    @OnClick({R.id.realtime_wiring, R.id.realtime_range})
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.realtime_wiring:
                new WiringPopWindow(this.m_Context).showWiringPopWindow(v, new int[]{this.m_Range.Wiring, this.m_Range.PQFlag, this.m_Range.Definition}, new View.OnClickListener() {
                    /* class com.clou.rs350.ui.view.WiringAndRangeView.AnonymousClass1 */

                    public void onClick(View v) {
                        int[] values = (int[]) v.getTag();
                        MessageManager.getInstance(WiringAndRangeView.this.m_Context).setWiringData(values[0], values[1], values[2]);
                    }
                });
                return;
            case R.id.txt_wiring:
            default:
                return;
            case R.id.realtime_range:
                new RangePopWindow(this.m_Context).showPopWindow(v, this.m_Range, new View.OnClickListener() {
                    /* class com.clou.rs350.ui.view.WiringAndRangeView.AnonymousClass2 */

                    public void onClick(View v) {
                    }
                });
                return;
        }
    }

    public void showData(Range range) {
        this.m_Range = range;
        if (range != null) {
            this.wiringTextView.setText(String.valueOf(range.WiringValue) + "\r\n" + this.sDefinition[range.Definition]);
            String[] Ranges = {PdfObject.NOTHING, PdfObject.NOTHING, PdfObject.NOTHING};
            try {
                String[] Ranges2 = new String[3];
                Ranges2[0] = String.valueOf(this.sVoltageConnectType[range.VoltageConnectionTypeL1.getIntValue()]) + "-" + (range.VoltageRangeL1Index == -1 ? "A-" : "M-") + range.VoltageRangeL1 + "\r\n" + this.sCurrentConnectType[range.CurrentConnectionTypeL1.getIntValue()] + "-" + (range.CurrentRangeL1Index == -1 ? "A-" : "M-") + range.CurrentRangeL1;
                Ranges2[1] = String.valueOf(this.sVoltageConnectType[range.VoltageConnectionTypeL2.getIntValue()]) + "-" + (range.VoltageRangeL2Index == -1 ? "A-" : "M-") + range.VoltageRangeL2 + "\r\n" + this.sCurrentConnectType[range.CurrentConnectionTypeL2.getIntValue()] + "-" + (range.CurrentRangeL2Index == -1 ? "A-" : "M-") + range.CurrentRangeL2;
                Ranges2[2] = String.valueOf(this.sVoltageConnectType[range.VoltageConnectionTypeL3.getIntValue()]) + "-" + (range.VoltageRangeL3Index == -1 ? "A-" : "M-") + range.VoltageRangeL3 + "\r\n" + this.sCurrentConnectType[range.CurrentConnectionTypeL3.getIntValue()] + "-" + (range.CurrentRangeL3Index == -1 ? "A-" : "M-") + range.CurrentRangeL3;
                Ranges = Ranges2;
            } catch (Exception e) {
            }
            this.l1TextView.setText(Ranges[0]);
            this.l2TextView.setText(Ranges[1]);
            this.l3TextView.setText(Ranges[2]);
            if (range.CannotDiscernClamp) {
                if (this.m_Dialog == null) {
                    this.m_Dialog = new HintDialog(this.m_Context);
                }
                if (!this.m_Dialog.isShowing()) {
                    this.m_Dialog = new HintDialog(this.m_Context);
                    this.m_Dialog.show();
                    this.m_Dialog.setTitle(R.string.text_cannot_discern_clamp);
                }
            }
            String OverloadHint = PdfObject.NOTHING;
            boolean[] tmpOverloadFlag = range.OverloadHint;
            if (tmpOverloadFlag != null) {
                for (int i = 0; i < tmpOverloadFlag.length; i++) {
                    if (tmpOverloadFlag[i]) {
                        OverloadHint = String.valueOf(OverloadHint) + this.OverloadHints[i] + "\n";
                    }
                }
            }
            if (!OverloadHint.equals(PdfObject.NOTHING)) {
                String OverloadHint2 = OverloadHint.substring(0, OverloadHint.length() - 1);
                if (this.m_Dialog == null) {
                    this.m_Dialog = new HintDialog(this.m_Context);
                }
                if (!this.m_Dialog.isShowing()) {
                    this.m_Dialog = new HintDialog(this.m_Context);
                    this.m_Dialog.setOnClick(new View.OnClickListener() {
                        /* class com.clou.rs350.ui.view.WiringAndRangeView.AnonymousClass3 */

                        public void onClick(View v) {
                            MessageManager.getInstance(WiringAndRangeView.this.m_Context).cleanOverloadFlag();
                            if (WiringAndRangeView.this.m_OverloadClick != null) {
                                WiringAndRangeView.this.m_OverloadClick.onClick(v);
                            }
                        }
                    });
                    this.m_Dialog.show();
                    this.m_Dialog.setTitle(OverloadHint2);
                }
            }
            for (int i2 = 0; i2 < 3; i2++) {
                if (!this.m_Ranges[i2].equals(Ranges[i2])) {
                    this.m_RangeHasChange = true;
                    this.m_Ranges[i2] = Ranges[i2];
                }
            }
        }
    }

    public boolean getHasRangeChange() {
        boolean result = this.m_RangeHasChange;
        this.m_RangeHasChange = false;
        return result;
    }

    public void setOverloadClick(View.OnClickListener okClick) {
        this.m_OverloadClick = okClick;
    }
}
