package com.clou.rs350.ui.view;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.DashPathEffect;
import android.graphics.Paint;
import android.graphics.PathEffect;
import android.util.AttributeSet;

import com.clou.rs350.R;
import com.clou.rs350.db.model.BaseErrorTest;
import com.clou.rs350.db.model.DBDataModel;
import com.itextpdf.text.pdf.ColumnText;

public class MyErrorGraphView extends MyZoomView {
    private static final String TAG = "MyErrorGraphView";
    private float arrow = ColumnText.GLOBAL_SPACE_CHAR_RATIO;
    boolean isfirst = true;
    private Context m_Context;
    private int m_LineColor = -1;
    private float m_StrokeWidth = 3.0f;
    private BaseErrorTest m_Values = new BaseErrorTest();
    private Paint myPaint = null;
    private PathEffect newEffects = null;
    private PathEffect oldEffects = null;
    private float textSize = ColumnText.GLOBAL_SPACE_CHAR_RATIO;
    private float x = 30.0f;
    private float x_length = ColumnText.GLOBAL_SPACE_CHAR_RATIO;
    private float y = 30.0f;
    private float y_height = ColumnText.GLOBAL_SPACE_CHAR_RATIO;

    public MyErrorGraphView(Context context) {
        super(context);
        this.m_Context = context;
        initData();
    }

    public MyErrorGraphView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        this.m_Context = context;
        initData();
    }

    public MyErrorGraphView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.m_Context = context;
        initData();
    }

    private void initData() {
    }

    @Override // com.clou.rs350.ui.view.MyZoomView
    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        float width = getScaleWidth();
        float height = getScaleHeight();
        this.x = width / 15.0f;
        this.y = height / 15.0f;
        this.y_height = height - (this.y * 2.0f);
        this.x_length = width - (this.x * 2.0f);
        this.arrow = this.y_height / 20.0f;
        if (this.isfirst) {
            this.myPaint = new Paint();
            this.myPaint.setColor(this.m_LineColor);
            this.myPaint.setAntiAlias(true);
            this.myPaint.setTextAlign(Paint.Align.RIGHT);
            this.myPaint.setStrokeWidth(3.0f);
            this.oldEffects = this.myPaint.getPathEffect();
            this.newEffects = new DashPathEffect(new float[]{1.0f, 2.0f, 4.0f, 8.0f}, 1.0f);
            this.isfirst = false;
        }
        this.textSize = this.y_height / 13.0f;
        if (this.textSize > getValue(R.dimen.sp_10, 17.0f)) {
            this.textSize = getValue(R.dimen.sp_10, 17.0f);
        }
        this.myPaint.setTextSize(this.textSize);
        drawLines();
    }

    private void drawLines() {
        double scale;
        this.myPaint.setColor(this.m_LineColor);
        this.myPaint.setPathEffect(this.oldEffects);
        drawLine(this.x, this.y, this.x, this.y_height + this.y, this.myPaint);
        drawLine(this.x, this.y, this.x - (this.arrow / 1.5f), this.arrow + this.y, this.myPaint);
        drawLine(this.x, this.y, (this.arrow / 1.5f) + this.x, this.arrow + this.y, this.myPaint);
        drawLine(this.x, (this.y_height / 2.0f) + this.y, this.x_length + this.x, (this.y_height / 2.0f) + this.y, this.myPaint);
        drawText("0", this.x - 3.0f, this.y + (this.y_height / 2.0f) + (this.textSize / 3.0f), this.myPaint);
        int count = this.m_Values.ErrorCount < this.m_Values.hasErrorCount ? this.m_Values.ErrorCount : this.m_Values.hasErrorCount;
        double max = getMax(this.m_Values.Error, count).doubleValue();
        if (max > this.m_Values.Accuracy) {
            scale = max;
        } else {
            scale = this.m_Values.Accuracy;
        }
        if (this.m_Values.Accuracy != 0.0d) {
            this.myPaint.setColor(-1426063361);
            this.myPaint.setPathEffect(this.newEffects);
            drawText(new StringBuilder(String.valueOf(this.m_Values.Accuracy)).toString(), this.x - 3.0f, (this.textSize / 3.0f) + draw_new_line(new Double[]{Double.valueOf(this.m_Values.Accuracy), Double.valueOf(this.m_Values.Accuracy)}, scale), this.myPaint);
            drawText(new StringBuilder(String.valueOf(-this.m_Values.Accuracy)).toString(), this.x - 3.0f, (this.textSize / 3.0f) + draw_new_line(new Double[]{Double.valueOf(-this.m_Values.Accuracy), Double.valueOf(-this.m_Values.Accuracy)}, scale), this.myPaint);
        }
        this.myPaint.setColor(this.m_LineColor);
        this.myPaint.setPathEffect(this.oldEffects);
        if (count > 0) {
            Double[] error = new Double[count];
            for (int i = 0; i < count; i++) {
                error[i] = Double.valueOf(this.m_Values.Error[i].d_Value);
            }
            draw_new_line(error, scale);
        }
    }

    private float draw_new_line(Double[] y_Doubles, double scale) {
        float stopY = ColumnText.GLOBAL_SPACE_CHAR_RATIO;
        if (!(y_Doubles == null || y_Doubles[0] == null)) {
            Double oldx = Double.valueOf((double) this.x);
            if (0.0d == scale) {
                scale = 1.0d;
            }
            double scale2 = ((double) ((int) (((double) this.y_height) / 2.5d))) / scale;
            float point_length = this.x_length / ((float) (y_Doubles.length - 1));
            for (int i = 0; i < y_Doubles.length - 1; i++) {
                float startX = (float) oldx.doubleValue();
                float startY = (float) ((-(y_Doubles[i].doubleValue() * scale2)) + ((double) (this.y + (this.y_height / 2.0f))));
                float stopX = (float) (oldx.doubleValue() + ((double) point_length));
                stopY = (float) ((-(y_Doubles[i + 1].doubleValue() * scale2)) + ((double) (this.y + (this.y_height / 2.0f))));
                drawLine(startX, startY, stopX, stopY, this.myPaint);
                oldx = Double.valueOf(oldx.doubleValue() + ((double) point_length));
            }
        }
        return stopY;
    }

    private Double getMax(DBDataModel[] array, int count) {
        Double max = Double.valueOf(Math.abs(array[0].d_Value));
        for (int i = 0; i < count; i++) {
            if (Math.abs(array[i].d_Value) > max.doubleValue()) {
                max = Double.valueOf(Math.abs(array[i].d_Value));
            }
        }
        return max;
    }

    public void refreshData(BaseErrorTest data) {
        this.m_Values = data;
        postInvalidate();
    }
}
