package com.clou.rs350.ui.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.clou.rs350.R;
import com.clou.rs350.task.ISleepCallback;
import com.clou.rs350.task.TimerThread;

public class CountdownDialog extends Dialog {
    private Handler handler = new Handler();
    private ProgressBar progressBar;
    private ISleepCallback sleepOver;
    TimerThread sleepTimer = null;
    private TextView txtTitle;

    public CountdownDialog(Context context) {
        super(context, R.style.dialog);
        requestWindowFeature(1);
        setCanceledOnTouchOutside(false);
        setCancelable(false);
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_progress);
        this.progressBar = (ProgressBar) findViewById(R.id.ss_calibration_progress);
        this.txtTitle = (TextView) findViewById(R.id.dialog_content_textview);
    }

    public void showDialog(int allSecond, String title, ISleepCallback sleepOver2) {
        showDialog(allSecond, sleepOver2);
        this.txtTitle.setText(title);
    }

    public void showDialog(int allSecond, int resid, ISleepCallback sleepOver2) {
        showDialog(allSecond, sleepOver2);
        this.txtTitle.setText(resid);
    }

    public void showDialog(int allSecond, ISleepCallback sleepOver2) {
        super.show();
        this.sleepOver = sleepOver2;
        this.progressBar.setMax(allSecond);
        this.progressBar.setProgress(0);
        sleepDo(allSecond);
    }

    public void dismiss() {
        super.dismiss();
        if (this.sleepTimer != null) {
            this.sleepTimer.stopTimer();
        }
    }

    private void sleepDo(int allSecond) {
        if (this.sleepTimer == null || this.sleepTimer.isStop()) {
            this.progressBar.setMax(allSecond);
            this.progressBar.setProgress(0);
            this.sleepTimer = new TimerThread(1000, new ISleepCallback() {
                /* class com.clou.rs350.ui.dialog.CountdownDialog.AnonymousClass1 */

                @Override // com.clou.rs350.task.ISleepCallback
                public void onSleepAfterToDo() {
                    CountdownDialog.this.handler.post(new Runnable() {
                        /* class com.clou.rs350.ui.dialog.CountdownDialog.AnonymousClass1.AnonymousClass1 */

                        public void run() {
                            int progress = CountdownDialog.this.progressBar.getProgress() + 1;
                            if (progress > CountdownDialog.this.progressBar.getMax()) {
                                CountdownDialog.this.sleepTimer.stopTimer();
                                if (CountdownDialog.this.sleepOver != null) {
                                    CountdownDialog.this.sleepOver.onSleepAfterToDo();
                                } else {
                                    CountdownDialog.this.dismiss();
                                }
                            } else {
                                CountdownDialog.this.progressBar.setProgress(progress);
                            }
                        }
                    });
                }
            }, "sleepTimer");
            this.sleepTimer.start();
        }
    }
}
