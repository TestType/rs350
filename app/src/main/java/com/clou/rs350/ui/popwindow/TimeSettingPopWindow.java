package com.clou.rs350.ui.popwindow;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.NumberPicker;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.clou.rs350.R;
import com.clou.rs350.utils.OtherUtils;

import java.util.Calendar;

public class TimeSettingPopWindow extends PopupWindow implements View.OnClickListener, CompoundButton.OnCheckedChangeListener {
    TextView TargetView;
    Context m_Context;
    View.OnClickListener m_OnClick;
    NumberPicker np_Hour;
    NumberPicker np_Minute;
    NumberPicker np_Second;
    private String[] sHour = new String[24];
    private String[] sMinute = new String[60];
    private String[] sSecond = new String[60];

    public TimeSettingPopWindow(Context context) {
        super(context);
        this.m_Context = context;
        initView();
        setTime();
    }

    private void setTime() {
        Calendar cal = Calendar.getInstance();
        this.np_Hour.setValue(cal.get(11));
        this.np_Minute.setValue(cal.get(12));
        this.np_Second.setValue(cal.get(13));
    }

    private void initView() {
        View conentView = ((LayoutInflater) this.m_Context.getSystemService("layout_inflater")).inflate(R.layout.pop_window_time_setting, (ViewGroup) null);
        for (int i = 0; i < 24; i++) {
            this.sHour[i] = new StringBuilder(String.valueOf(i)).toString();
        }
        for (int i2 = 0; i2 < 60; i2++) {
            this.sMinute[i2] = String.format("%1$02d", Integer.valueOf(i2));
            this.sSecond[i2] = String.format("%1$02d", Integer.valueOf(i2));
        }
        conentView.findViewById(R.id.ok).setOnClickListener(this);
        conentView.findViewById(R.id.cancel).setOnClickListener(this);
        this.np_Hour = (NumberPicker) conentView.findViewById(R.id.np_hour);
        this.np_Minute = (NumberPicker) conentView.findViewById(R.id.np_minute);
        this.np_Second = (NumberPicker) conentView.findViewById(R.id.np_second);
        this.np_Hour.setDisplayedValues(this.sHour);
        this.np_Minute.setDisplayedValues(this.sMinute);
        this.np_Second.setDisplayedValues(this.sSecond);
        this.np_Hour.setMinValue(0);
        this.np_Hour.setMaxValue(this.sHour.length - 1);
        this.np_Minute.setMinValue(0);
        this.np_Minute.setMaxValue(this.sMinute.length - 1);
        this.np_Second.setMinValue(0);
        this.np_Second.setMaxValue(this.sSecond.length - 1);
        setContentView(conentView);
        setWidth(this.m_Context.getResources().getDimensionPixelSize(R.dimen.dp_250));
        setHeight(this.m_Context.getResources().getDimensionPixelSize(R.dimen.dp_150));
        setFocusable(true);
        setOutsideTouchable(true);
        update();
        setBackgroundDrawable(new ColorDrawable(0));
        setAnimationStyle(16973826);
    }

    public void showPopWindow(View currentClick, View.OnClickListener onclick) {
        showPopWindow(currentClick, null, onclick);
    }

    public void showPopWindow(View currentClick, View parentView, View.OnClickListener onclick) {
        this.m_OnClick = onclick;
        this.TargetView = (TextView) currentClick;
        String[] tmpString = this.TargetView.getText().toString().split(":");
        if (tmpString.length == 3) {
            this.np_Hour.setValue(OtherUtils.parseInt(tmpString[0]));
            this.np_Minute.setValue(OtherUtils.parseInt(tmpString[1]));
            this.np_Second.setValue(OtherUtils.parseInt(tmpString[2]));
        }
        setSoftInputMode(16);
        if (!isShowing()) {
            int[] location = new int[2];
            currentClick.getLocationOnScreen(location);
            if (parentView != null) {
                showAtLocation(parentView, 0, location[0] + ((currentClick.getWidth() - getWidth()) / 2), location[1] + currentClick.getHeight());
            } else {
                showAtLocation(currentClick, 0, location[0] + ((currentClick.getWidth() - getWidth()) / 2), location[1] + currentClick.getHeight());
            }
        } else {
            dismiss();
        }
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ok:
                this.TargetView.setText(String.valueOf(this.sHour[this.np_Hour.getValue()]) + ":" + this.sMinute[this.np_Minute.getValue()] + ":" + this.sSecond[this.np_Second.getValue()]);
                if (this.m_OnClick != null) {
                    this.m_OnClick.onClick(this.TargetView);
                }
                if (isShowing()) {
                    dismiss();
                    return;
                }
                return;
            case R.id.cancel:
                if (isShowing()) {
                    dismiss();
                    return;
                }
                return;
            default:
                return;
        }
    }

    public void onCheckedChanged(CompoundButton arg0, boolean arg1) {
    }
}
