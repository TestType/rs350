package com.clou.rs350.ui.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.clou.rs350.R;
import com.clou.rs350.callback.PasswordCallback;

public class PasswordDialog extends Dialog implements View.OnClickListener {
    Button cancel = null;
    Context context = null;
    private OnDismissListener dismissListener = null;
    TextView edit_password = null;
    private boolean isCorrect = false;
    String m_Password;
    private NumericDialog numericDialog;
    Button ok = null;
    private PasswordCallback passwordCallback;

    public PasswordDialog(Context context2, String sPassword, PasswordCallback passwordCallback2) {
        super(context2, R.style.dialog);
        requestWindowFeature(1);
        setCanceledOnTouchOutside(true);
        this.context = context2;
        this.passwordCallback = passwordCallback2;
        this.m_Password = sPassword;
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_password);
        this.ok = (Button) findViewById(R.id.btn_ok);
        this.cancel = (Button) findViewById(R.id.btn_cancel);
        this.edit_password = (TextView) findViewById(R.id.edit_password);
        this.ok.setOnClickListener(this);
        this.cancel.setOnClickListener(this);
        this.edit_password.setOnClickListener(this);
        this.numericDialog = new NumericDialog(this.context);
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_cancel:
                dismiss();
                return;
            case R.id.btn_ok:
                if (this.edit_password.getText().toString().equals(this.m_Password)) {
                    this.passwordCallback.onPasswordIsCorrect();
                    this.isCorrect = true;
                    dismiss();
                    return;
                }
                this.passwordCallback.onPasswordIsMistake();
                new HintDialog(this.context, (int) R.string.text_password_wrong).show();
                return;
            case R.id.edit_password:
                this.numericDialog.showMyDialog(this.edit_password);
                return;
            default:
                return;
        }
    }

    public void setOnDismiss(OnDismissListener listener) {
        this.dismissListener = listener;
    }

    public void dismiss() {
        if (!this.isCorrect && this.dismissListener != null) {
            this.dismissListener.onDismiss(this);
        }
        super.dismiss();
    }
}
