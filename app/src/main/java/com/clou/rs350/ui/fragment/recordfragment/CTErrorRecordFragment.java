package com.clou.rs350.ui.fragment.recordfragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.clou.rs350.R;
import com.clou.rs350.db.model.CTMeasurement;
import com.clou.rs350.ui.fragment.BaseFragment;
import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.view.annotation.ViewInject;

public class CTErrorRecordFragment extends BaseFragment {
    @ViewInject(R.id.txt_ip)
    private TextView IpTv;
    @ViewInject(R.id.txt_is)
    private TextView IsTv;
    @ViewInject(R.id.l1_phaseanage)
    private TextView L1PhaseAnageTv;
    @ViewInject(R.id.l1_primary)
    private TextView L1PrimaryTv;
    @ViewInject(R.id.l1_ratio_error)
    private TextView L1RatioErrorTv;
    @ViewInject(R.id.l1_result)
    private TextView L1ResultTv;
    @ViewInject(R.id.l1_secondary)
    private TextView L1SecondaryTv;
    @ViewInject(R.id.l2_phaseanage)
    private TextView L2PhaseAnageTv;
    @ViewInject(R.id.l2_primary)
    private TextView L2PrimaryTv;
    @ViewInject(R.id.l2_ratio_error)
    private TextView L2RatioErrorTv;
    @ViewInject(R.id.l2_result)
    private TextView L2ResultTv;
    @ViewInject(R.id.l2_secondary)
    private TextView L2SecondaryTv;
    @ViewInject(R.id.l3_phaseanage)
    private TextView L3PhaseAnageTv;
    @ViewInject(R.id.l3_primary)
    private TextView L3PrimaryTv;
    @ViewInject(R.id.l3_ratio_error)
    private TextView L3RatioErrorTv;
    @ViewInject(R.id.l3_result)
    private TextView L3ResultTv;
    @ViewInject(R.id.l3_secondary)
    private TextView L3SecondaryTv;
    private CTMeasurement m_CTMeasurement;

    public CTErrorRecordFragment(CTMeasurement data) {
        this.m_CTMeasurement = data;
    }

    @Override // android.support.v4.app.Fragment
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        this.fragmentView = inflater.inflate(R.layout.fragment_record_ct_error, (ViewGroup) null);
        ViewUtils.inject(this, this.fragmentView);
        return this.fragmentView;
    }

    @Override // android.support.v4.app.Fragment, com.clou.rs350.ui.fragment.BaseFragment
    public void onResume() {
        super.onResume();
        showSavedData();
        showData();
    }

    private void showSavedData() {
        this.IpTv.setText(new StringBuilder(String.valueOf(this.m_CTMeasurement.CurrentValueNominalPrimary.s_Value)).toString());
        this.IsTv.setText(new StringBuilder(String.valueOf(this.m_CTMeasurement.CurrentValueNominalSecondary.s_Value)).toString());
    }

    @Override // android.support.v4.app.Fragment, com.clou.rs350.ui.fragment.BaseFragment
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initView();
    }

    private void initView() {
    }

    private void showData() {
        this.L1PrimaryTv.setText(this.m_CTMeasurement.CurrentValuePrimary[0].s_Value);
        this.L2PrimaryTv.setText(this.m_CTMeasurement.CurrentValuePrimary[1].s_Value);
        this.L3PrimaryTv.setText(this.m_CTMeasurement.CurrentValuePrimary[2].s_Value);
        this.L1SecondaryTv.setText(this.m_CTMeasurement.CurrentValueSecondary[0].s_Value);
        this.L2SecondaryTv.setText(this.m_CTMeasurement.CurrentValueSecondary[1].s_Value);
        this.L3SecondaryTv.setText(this.m_CTMeasurement.CurrentValueSecondary[2].s_Value);
        this.L1RatioErrorTv.setText(this.m_CTMeasurement.CurrentValueError[0].s_Value);
        this.L2RatioErrorTv.setText(this.m_CTMeasurement.CurrentValueError[1].s_Value);
        this.L3RatioErrorTv.setText(this.m_CTMeasurement.CurrentValueError[2].s_Value);
        this.L1PhaseAnageTv.setText(this.m_CTMeasurement.CurrentValuePhase[0].s_Value);
        this.L2PhaseAnageTv.setText(this.m_CTMeasurement.CurrentValuePhase[1].s_Value);
        this.L3PhaseAnageTv.setText(this.m_CTMeasurement.CurrentValuePhase[2].s_Value);
    }

    @Override // android.support.v4.app.Fragment, com.clou.rs350.ui.fragment.BaseFragment
    public void onPause() {
        super.onPause();
    }
}
