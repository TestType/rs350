package com.clou.rs350.ui.fragment.databasefragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.clou.rs350.CLApplication;
import com.clou.rs350.R;
import com.clou.rs350.callback.ILoadCallback;
import com.clou.rs350.constants.ConstantsFileName;
import com.clou.rs350.db.dao.MeterTypeDao;
import com.clou.rs350.db.model.MeterType;
import com.clou.rs350.model.ClouData;
import com.clou.rs350.ui.dialog.FieldSettingDialog;
import com.clou.rs350.ui.dialog.HintDialog;
import com.clou.rs350.ui.dialog.LoadingDialog;
import com.clou.rs350.ui.dialog.NormalDialog;
import com.clou.rs350.ui.fragment.BaseFragment;
import com.clou.rs350.ui.view.infoview.MeterTypeView;
import com.clou.rs350.utils.OtherUtils;
import com.lidroid.xutils.ViewUtils;

import java.io.File;

public class MeterTypeFragment extends BaseFragment implements View.OnClickListener {
    Button btn_Cancel = null;
    Button btn_Delete = null;
    Button btn_FieldSetting = null;
    Button btn_Modify = null;
    Button btn_New = null;
    Button btn_Save = null;
    int m_Authority = 0;
    MeterTypeDao m_Dao;
    FieldSettingDialog m_FieldSettingDialog;
    int m_Function = 1;
    MeterType m_Info;
    MeterTypeView m_InfoView;
    View m_ViewLayout;

    @Override // android.support.v4.app.Fragment
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        this.fragmentView = inflater.inflate(R.layout.fragment_meter_type, (ViewGroup) null);
        ViewUtils.inject(this, this.fragmentView);
        return this.fragmentView;
    }

    @Override // android.support.v4.app.Fragment, com.clou.rs350.ui.fragment.BaseFragment
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initView();
    }

    private void initView() {
        this.m_Authority = ClouData.getInstance().getOperatorInfo().Authority;
        this.m_Dao = new MeterTypeDao(this.m_Context);
        this.m_Info = new MeterType();
        this.m_ViewLayout = findViewById(R.id.layout_meter_type);
        this.m_InfoView = new MeterTypeView(this.m_Context, this.m_ViewLayout, this.m_Info, 1);
        findViewById(R.id.btn_export).setOnClickListener(this);
        this.btn_New = (Button) findViewById(R.id.btn_new);
        this.btn_New.setOnClickListener(this);
        this.btn_Modify = (Button) findViewById(R.id.btn_modify);
        this.btn_Modify.setOnClickListener(this);
        this.btn_Delete = (Button) findViewById(R.id.btn_delete);
        this.btn_Delete.setOnClickListener(this);
        this.btn_FieldSetting = (Button) findViewById(R.id.btn_field_setting);
        this.btn_FieldSetting.setOnClickListener(this);
        this.btn_Save = (Button) findViewById(R.id.btn_save);
        this.btn_Save.setOnClickListener(new View.OnClickListener() {
            /* class com.clou.rs350.ui.fragment.databasefragment.MeterTypeFragment.AnonymousClass1 */

            public void onClick(View v) {
                MeterTypeFragment.this.Save();
            }
        });
        this.btn_Cancel = (Button) findViewById(R.id.btn_cancel);
        this.btn_Cancel.setOnClickListener(new View.OnClickListener() {
            /* class com.clou.rs350.ui.fragment.databasefragment.MeterTypeFragment.AnonymousClass2 */

            public void onClick(View v) {
                MeterTypeFragment.this.setEditButton(1);
                MeterTypeFragment.this.refreshData(MeterTypeFragment.this.m_Info);
            }
        });
        setEditButton(1);
        this.m_InfoView.refreshFieldSetting();
    }

    private void parseData() {
        this.m_Info = this.m_InfoView.getInfo();
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private void Save() {
        String Name = this.m_InfoView.getKey();
        if (3 == this.m_Function && OtherUtils.isEmpty(Name)) {
            showHint(R.string.text_meter_type_empty);
        } else if (this.m_InfoView.hasSetParams()) {
            if (3 != this.m_Function) {
                this.m_Dao.deleteRecordByKey(Name);
            } else if (this.m_Dao.queryObjectByType(Name) != null) {
                showHint(R.string.text_meter_type_exists);
                return;
            }
            parseData();
            this.m_Dao.insertObject(this.m_Info);
            setEditButton(1);
            showHint(R.string.text_save_success);
        }
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private void setEditButton(int function) {
        boolean z;
        boolean z2;
        boolean z3;
        int i = 0;
        boolean z4 = true;
        this.m_Function = function;
        this.btn_New.setEnabled(this.m_Function == 1);
        Button button = this.btn_Modify;
        if (this.m_Function != 1 || this.m_Authority == 0 || 2 == this.m_Authority) {
            z = false;
        } else {
            z = true;
        }
        button.setEnabled(z);
        Button button2 = this.btn_Delete;
        if (this.m_Function != 1 || this.m_Authority == 0 || 2 == this.m_Authority) {
            z2 = false;
        } else {
            z2 = true;
        }
        button2.setEnabled(z2);
        Button button3 = this.btn_Save;
        if (this.m_Function != 1) {
            z3 = true;
        } else {
            z3 = false;
        }
        button3.setEnabled(z3);
        Button button4 = this.btn_Cancel;
        if (this.m_Function == 1) {
            z4 = false;
        }
        button4.setEnabled(z4);
        this.m_InfoView.setFunction(this.m_Function);
        Button button5 = this.btn_FieldSetting;
        if (2 >= this.m_Authority) {
            i = 4;
        }
        button5.setVisibility(i);
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private void refreshData(MeterType info) {
        this.m_InfoView.refreshData(info);
    }

    public void onClick(View v) {
        this.m_Info = this.m_InfoView.getInfo();
        switch (v.getId()) {
            case R.id.btn_delete:
                if (!this.m_InfoView.getHasSearch()) {
                    showHint(R.string.text_meter_type_wrong);
                    return;
                } else if (OtherUtils.isEmpty(this.m_InfoView.getKey())) {
                    showHint(R.string.text_meter_type_empty);
                    return;
                } else {
                    NormalDialog dialog = new NormalDialog(this.m_Context);
                    dialog.setOnClick(new View.OnClickListener() {
                        /* class com.clou.rs350.ui.fragment.databasefragment.MeterTypeFragment.AnonymousClass3 */

                        public void onClick(View v) {
                            MeterTypeFragment.this.m_Dao.deleteRecordByKey(MeterTypeFragment.this.m_InfoView.getKey());
                            MeterTypeFragment.this.m_Info = new MeterType();
                            MeterTypeFragment.this.refreshData(MeterTypeFragment.this.m_Info);
                        }
                    }, null);
                    dialog.show();
                    dialog.setTitle(R.string.text_meter_type_delete);
                    return;
                }
            case R.id.btn_export:
                new LoadingDialog(this.m_Context, new ILoadCallback() {
                    /* class com.clou.rs350.ui.fragment.databasefragment.MeterTypeFragment.AnonymousClass4 */

                    @Override // com.clou.rs350.callback.ILoadCallback
                    public Object run() {
                        String exportPath = CLApplication.app.getExportPath();
                        MeterTypeFragment.this.m_Dao.exportDataToCSV(String.valueOf(exportPath) + File.separator + ConstantsFileName.METERTYPECSVNAME);
                        CLApplication.app.refreshPath(exportPath);
                        return null;
                    }

                    @Override // com.clou.rs350.callback.ILoadCallback
                    public void callback(Object result) {
                        new HintDialog(MeterTypeFragment.this.m_Context, (int) R.string.text_export_finish).show();
                    }
                }).show();
                return;
            case R.id.btn_new:
                setEditButton(3);
                refreshData(new MeterType());
                return;
            case R.id.btn_modify:
                if (!this.m_InfoView.getHasSearch()) {
                    showHint(R.string.text_meter_type_wrong);
                    return;
                } else if (OtherUtils.isEmpty(this.m_InfoView.getKey())) {
                    showHint(R.string.text_meter_type_empty);
                    return;
                } else {
                    setEditButton(2);
                    return;
                }
            case R.id.btn_field_setting:
                if (this.m_FieldSettingDialog == null) {
                    this.m_FieldSettingDialog = new FieldSettingDialog(this.m_Context, "MeterType");
                    this.m_FieldSettingDialog.setOnClick(new View.OnClickListener() {
                        /* class com.clou.rs350.ui.fragment.databasefragment.MeterTypeFragment.AnonymousClass5 */

                        public void onClick(View v) {
                        }
                    }, new View.OnClickListener() {
                        /* class com.clou.rs350.ui.fragment.databasefragment.MeterTypeFragment.AnonymousClass6 */

                        public void onClick(View v) {
                            MeterTypeFragment.this.m_InfoView.refreshFieldSetting();
                        }
                    });
                }
                this.m_FieldSettingDialog.show();
                return;
            default:
                return;
        }
    }

    @Override // android.support.v4.app.Fragment, com.clou.rs350.ui.fragment.BaseFragment
    public void onPause() {
        if (1 != this.m_Function) {
            setEditButton(1);
            refreshData(this.m_Info);
        } else {
            this.m_Info = this.m_InfoView.getInfo();
        }
        super.onPause();
    }
}
