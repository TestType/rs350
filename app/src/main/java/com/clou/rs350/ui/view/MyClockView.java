package com.clou.rs350.ui.view;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.View;

import com.clou.rs350.R;
import com.clou.rs350.utils.AngleUtil;
import com.itextpdf.text.pdf.ColumnText;

public class MyClockView extends View {
    protected int CircleBackground = getResources().getColor(R.color.show_title_color);
    protected int CircleLine = -1;
    private double a_radio = 0.0d;
    private float center_x = ColumnText.GLOBAL_SPACE_CHAR_RATIO;
    private float center_y = ColumnText.GLOBAL_SPACE_CHAR_RATIO;
    private int density = 60;
    protected int height = 0;
    protected boolean is3P = false;
    private boolean isfirst = true;
    protected LineModel[] lines = null;
    protected Address[] m_Address = null;
    protected int m_AngleDefinition = 0;
    protected int m_LineMode = 0;
    private float m_Radius = ColumnText.GLOBAL_SPACE_CHAR_RATIO;
    protected float m_StrokeWidth = 3.0f;
    protected int m_VectorDefinition = 0;
    protected int m_VectorType = 0;
    protected Canvas myCanvas = null;
    protected Paint myPaint = null;
    private float tick_mark_long = 10.0f;
    private float tick_mark_short = 5.0f;
    protected int width = 0;

    /* access modifiers changed from: package-private */
    public class LineModel {
        public double angle;
        public int color;
        public double length;
        public String name;
        public float ratio;

        public LineModel(float ratio2, double angle2, double length2, String name2, int color2) {
            this.ratio = ratio2;
            this.angle = angle2;
            this.length = length2;
            this.name = name2;
            this.color = color2;
        }
    }

    /* access modifiers changed from: package-private */
    public class Address {
        public double X;
        public double Y;

        public Address(double X2, double Y2) {
            this.X = X2;
            this.Y = Y2;
        }
    }

    public MyClockView(Context context) {
        super(context);
    }

    public MyClockView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    public MyClockView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (this.width == 0) {
            this.width = getWidth();
        }
        if (this.height == 0) {
            this.height = getHeight();
        }
        this.center_x = (float) (this.width / 2);
        this.center_y = (float) (this.height / 2);
        this.myCanvas = canvas;
        if (this.isfirst) {
            this.myPaint = new Paint();
            this.myPaint.setAntiAlias(true);
            this.myPaint.setTextAlign(Paint.Align.CENTER);
            this.isfirst = false;
            this.myPaint.setStrokeWidth(this.m_StrokeWidth);
        }
        this.m_Radius = ((float) (this.width < this.height ? this.width / 2 : this.height / 2)) * 0.9f;
        this.tick_mark_long = this.m_Radius * 0.1f;
        this.tick_mark_short = this.tick_mark_long * 0.5f;
        this.a_radio = (double) (this.m_Radius * 0.08f);
        drawXY((float) this.width, (float) this.height);
        if (this.lines != null) {
            this.m_Address = new Address[this.lines.length];
            for (int i = 0; i < this.lines.length; i++) {
                this.m_Address[i] = calcAddress(this.lines[i]);
            }
            this.myPaint.setColor(-7829368);
            this.myCanvas.drawLine((float) this.m_Address[0].X, (float) this.m_Address[0].Y, (float) this.m_Address[1].X, (float) this.m_Address[1].Y, this.myPaint);
            this.myCanvas.drawLine((float) this.m_Address[1].X, (float) this.m_Address[1].Y, (float) this.m_Address[2].X, (float) this.m_Address[2].Y, this.myPaint);
            this.myCanvas.drawLine((float) this.m_Address[2].X, (float) this.m_Address[2].Y, (float) this.m_Address[0].X, (float) this.m_Address[0].Y, this.myPaint);
            for (int i2 = 0; i2 < this.lines.length; i2++) {
                drawLine(this.lines[i2], this.m_Address[i2].X, this.m_Address[i2].Y);
            }
        }
    }

    public void postInvalidate(LineModel[] lines2) {
        this.lines = lines2;
        super.postInvalidate();
    }

    private void drawXY(float width2, float height2) {
        this.myPaint.setColor(this.CircleLine);
        this.myCanvas.drawCircle(this.center_x, this.center_y, this.m_Radius, this.myPaint);
        this.myPaint.setColor(this.CircleBackground);
        this.myCanvas.drawCircle(this.center_x, this.center_y, this.m_Radius - this.m_StrokeWidth, this.myPaint);
        this.myPaint.setColor(this.CircleLine);
        float tmpHalfWidth = width2 / 2.0f;
        float tmpHalfHeight = height2 / 2.0f;
        float f = this.m_Radius / 2.4f;
        float tmpTextHeight = this.m_Radius / 6.0f;
        this.myPaint.setTextSize(tmpTextHeight);
        float tmpTextHeight2 = (tmpTextHeight * 2.0f) / 5.0f;
        this.myPaint.setTextAlign(Paint.Align.CENTER);
        this.myCanvas.drawLine(tmpHalfWidth - this.m_Radius, tmpHalfHeight, this.m_Radius + tmpHalfWidth, tmpHalfHeight, this.myPaint);
        this.myCanvas.drawLine(tmpHalfWidth, tmpHalfHeight - this.m_Radius, tmpHalfWidth, tmpHalfHeight + this.m_Radius, this.myPaint);
    }

    private float[] getXY(Double angle, float length) {
        return new float[]{(float) (((double) this.center_x) + (((double) this.m_Radius) * Math.sin((angle.doubleValue() * 3.141592653589793d) / 180.0d))), (float) (((double) this.center_y) + (((double) this.m_Radius) * Math.cos((angle.doubleValue() * 3.141592653589793d) / 180.0d))), (float) (((double) this.center_x) + (((double) (this.m_Radius - length)) * Math.sin((angle.doubleValue() * 3.141592653589793d) / 180.0d))), (float) (((double) this.center_y) + (((double) (this.m_Radius - length)) * Math.cos((angle.doubleValue() * 3.141592653589793d) / 180.0d)))};
    }

    private Address calcAddress(LineModel line) {
        Address arrorAddress = new Address(0.0d, 0.0d);
        arrorAddress.X = ((double) this.center_x) - (((double) (line.ratio * this.m_Radius)) * Math.sin(line.angle * 0.017453292519943295d));
        arrorAddress.Y = ((double) this.center_y) - (((double) (line.ratio * this.m_Radius)) * Math.cos(line.angle * 0.017453292519943295d));
        return arrorAddress;
    }

    public void drawLine(LineModel line, double arrowX, double arrowY) {
        if (line.angle <= 360.0d) {
            AngleUtil.adjustAngle(line.angle + 165.0d);
            double offsetAngle = AngleUtil.adjustAngle(line.angle + 155.0d);
            double arrow_left_x_offect = this.a_radio * Math.sin(0.017453292519943295d * offsetAngle);
            double arrow_left_y_offect = this.a_radio * Math.cos(0.017453292519943295d * offsetAngle);
            double offsetAngle2 = AngleUtil.adjustAngle(line.angle + 205.0d);
            double arrow_left_coordinate_x = arrowX - arrow_left_x_offect;
            double arrow_left_coordinate_y = arrowY - arrow_left_y_offect;
            double arrow_right_coordinate_x = arrowX - (this.a_radio * Math.sin(0.017453292519943295d * offsetAngle2));
            double arrow_right_coordinate_y = arrowY - (this.a_radio * Math.cos(0.017453292519943295d * offsetAngle2));
            double offsetAngle3 = AngleUtil.adjustAngle(line.angle + 225.0d);
            double character_x_coordinate = arrow_right_coordinate_x - (15.0d * Math.sin(0.017453292519943295d * offsetAngle3));
            double character_y_coordinate = (arrow_right_coordinate_y - (15.0d * Math.cos(0.017453292519943295d * offsetAngle3))) + 7.0d;
            this.myPaint.setColor(line.color);
            if (line.length != 0.0d) {
                this.myCanvas.drawLine(this.center_x, this.center_y, (float) arrowX, (float) arrowY, this.myPaint);
                this.myCanvas.drawLine((float) arrowX, (float) arrowY, (float) arrow_left_coordinate_x, (float) arrow_left_coordinate_y, this.myPaint);
                this.myCanvas.drawLine((float) arrowX, (float) arrowY, (float) arrow_right_coordinate_x, (float) arrow_right_coordinate_y, this.myPaint);
                float old_textSize = this.myPaint.getTextSize();
                this.myPaint.setTextSize(20.0f);
                this.myCanvas.drawText(line.name, (float) character_x_coordinate, (float) character_y_coordinate, this.myPaint);
                this.myPaint.setTextSize(old_textSize);
            }
        }
    }
}
