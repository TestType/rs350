package com.clou.rs350.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.clou.rs350.R;
import com.clou.rs350.db.model.ErrorTest;

public class ErrorAdapter extends BaseAdapter {
    private Context m_Context;
    private ErrorTest m_ErrorTest;

    public ErrorAdapter(ErrorTest errorTest, Context context) {
        this.m_ErrorTest = errorTest;
        this.m_Context = context;
    }

    public int getCount() {
        return this.m_ErrorTest.ErrorCount > this.m_ErrorTest.Error.length ? this.m_ErrorTest.Error.length : this.m_ErrorTest.ErrorCount;
    }

    public Object getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return (long) position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = LayoutInflater.from(this.m_Context).inflate(R.layout.adapter_error, (ViewGroup) null);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        holder.show(position);
        return convertView;
    }

    public void refreshData(ErrorTest errorTest) {
        this.m_ErrorTest = errorTest;
        notifyDataSetChanged();
    }

    private class ViewHolder {
        private TextView txt_Energy;
        private TextView txt_Error;
        private TextView txt_Title;

        ViewHolder(View view) {
            this.txt_Title = (TextView) view.findViewById(R.id.txt_title);
            this.txt_Error = (TextView) view.findViewById(R.id.txt_error);
            this.txt_Energy = (TextView) view.findViewById(R.id.txt_energy);
        }

        /* access modifiers changed from: package-private */
        public void show(int position) {
            this.txt_Title.setText(new StringBuilder(String.valueOf(position + 1)).toString());
            this.txt_Error.setText(ErrorAdapter.this.m_ErrorTest.getError(position));
            this.txt_Energy.setText(ErrorAdapter.this.m_ErrorTest.getEnergy(position));
        }
    }
}
