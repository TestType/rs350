package com.clou.rs350.ui.fragment.settingfragment;

import android.content.DialogInterface;
import android.content.res.Resources;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;

import com.clou.rs350.Preferences;
import com.clou.rs350.R;
import com.clou.rs350.callback.PasswordCallback;
import com.clou.rs350.db.model.BasicMeasurement;
import com.clou.rs350.db.model.ConTypeRange;
import com.clou.rs350.db.model.Range;
import com.clou.rs350.device.MeterBaseDevice;
import com.clou.rs350.handler.HandlerSwitchView;
import com.clou.rs350.model.ClouData;
import com.clou.rs350.task.ISleepCallback;
import com.clou.rs350.task.SleepTask;
import com.clou.rs350.task.TimerThread;
import com.clou.rs350.ui.dialog.CountdownDialog;
import com.clou.rs350.ui.dialog.HintDialog;
import com.clou.rs350.ui.dialog.NormalDialog;
import com.clou.rs350.ui.dialog.NumericDialog;
import com.clou.rs350.ui.dialog.PasswordDialog;
import com.clou.rs350.ui.dialog.StandardInfoDialog;
import com.clou.rs350.ui.fragment.BaseFragment;
import com.clou.rs350.ui.popwindow.ListPopWindow;
import com.clou.rs350.ui.view.WiringAndRangeView;
import com.clou.rs350.ui.view.infoview.StandardInfoView;
import com.clou.rs350.utils.OtherUtils;
import com.itextpdf.text.pdf.PdfObject;
import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.view.annotation.ViewInject;
import com.lidroid.xutils.view.annotation.event.OnClick;

import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

public class CalibrationFragment extends BaseFragment implements HandlerSwitchView.IOnSelectCallback {
    private static final int STATE_ADJUST_STATE_DATA = 1;
    private static final int STATE_READ_MEASURE_DATA = 0;
    @ViewInject(R.id.ss_calibration_checkAll)
    private CheckBox allCheckBox;
    @ViewInject(R.id.ss_calibration_start)
    private Button btn_Calibrate;
    @ViewInject(R.id.ss_calibration_dc_offset)
    private Button btn_Calibrate_DC;
    @ViewInject(R.id.btn_next)
    private Button btn_Next;
    @ViewInject(R.id.btn_previous)
    private Button btn_Previous;
    @ViewInject(R.id.btn_start)
    private Button btn_Start;
    private CountdownDialog calibrateDialog = null;
    private CompoundButton.OnCheckedChangeListener checkListener = new CompoundButton.OnCheckedChangeListener() {
        /* class com.clou.rs350.ui.fragment.settingfragment.CalibrationFragment.AnonymousClass1 */

        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
            String unitString;
            String unitString2;
            switch (buttonView.getId()) {
                case R.id.chk_nertral_voltage:
                    if (isChecked) {
                        unitString = "V";
                    } else {
                        unitString = "A";
                    }
                    CalibrationFragment.this.txt_Neutral_Unit.setText(unitString);
                    CalibrationFragment.this.txt_Neutral_Range.setText(PdfObject.NOTHING);
                    break;
                case R.id.chk_calibrate_voltage:
                    CalibrationFragment.this.chk_Voltage1.setChecked(isChecked);
                    CalibrationFragment.this.rangeEdittext.setText(PdfObject.NOTHING);
                    CalibrationFragment.this.connectTypeEditText.setText(PdfObject.NOTHING);
                    if (isChecked) {
                        unitString2 = "V";
                    } else {
                        unitString2 = "A";
                    }
                    CalibrationFragment.this.l1RmsTableTextView.setText(unitString2);
                    CalibrationFragment.this.l2RmsTableTextView.setText(unitString2);
                    CalibrationFragment.this.l3RmsTableTextView.setText(unitString2);
                    CalibrationFragment.this.checkTextView();
                    CalibrationFragment.this.clearStandardValue();
                    break;
                case R.id.chk_calibrate_current:
                case R.id.chk_calibrate_current1:
                    CalibrationFragment.this.chk_Current.setChecked(isChecked);
                    CalibrationFragment.this.chk_Current1.setChecked(isChecked);
                    CalibrationFragment.this.clearStandardValue();
                    break;
                case R.id.ss_calibration_checkAll:
                    if (!isChecked) {
                        CalibrationFragment.this.checkTextView(false, false, false);
                        CalibrationFragment.this.l1CheckBox.setOnCheckedChangeListener(null);
                        CalibrationFragment.this.l2CheckBox.setOnCheckedChangeListener(null);
                        CalibrationFragment.this.l3CheckBox.setOnCheckedChangeListener(null);
                        CalibrationFragment.this.l1CheckBox.setChecked(false);
                        CalibrationFragment.this.l2CheckBox.setChecked(false);
                        CalibrationFragment.this.l3CheckBox.setChecked(false);
                        CalibrationFragment.this.l1CheckBox.setOnCheckedChangeListener(CalibrationFragment.this.checkListener);
                        CalibrationFragment.this.l2CheckBox.setOnCheckedChangeListener(CalibrationFragment.this.checkListener);
                        CalibrationFragment.this.l3CheckBox.setOnCheckedChangeListener(CalibrationFragment.this.checkListener);
                        break;
                    } else {
                        CalibrationFragment.this.checkTextView(true, true, true);
                        CalibrationFragment.this.l1CheckBox.setOnCheckedChangeListener(null);
                        CalibrationFragment.this.l2CheckBox.setOnCheckedChangeListener(null);
                        CalibrationFragment.this.l3CheckBox.setOnCheckedChangeListener(null);
                        CalibrationFragment.this.l1CheckBox.setChecked(true);
                        CalibrationFragment.this.l2CheckBox.setChecked(true);
                        CalibrationFragment.this.l3CheckBox.setChecked(true);
                        CalibrationFragment.this.l1CheckBox.setOnCheckedChangeListener(CalibrationFragment.this.checkListener);
                        CalibrationFragment.this.l2CheckBox.setOnCheckedChangeListener(CalibrationFragment.this.checkListener);
                        CalibrationFragment.this.l3CheckBox.setOnCheckedChangeListener(CalibrationFragment.this.checkListener);
                        break;
                    }
                case R.id.ss_calibration_checkL1:
                    CalibrationFragment.this.checkTextView();
                    break;
                case R.id.ss_calibration_checkL2:
                    CalibrationFragment.this.checkTextView();
                    break;
                case R.id.ss_calibration_checkL3:
                    CalibrationFragment.this.checkTextView();
                    break;
                case R.id.chk_calibrate_voltage1:
                    CalibrationFragment.this.chk_Voltage.setChecked(isChecked);
                    CalibrationFragment.this.txt_ConnectType.setText(PdfObject.NOTHING);
                    break;
            }
            CalibrationFragment.this.setDCbtn();
        }
    };
    @ViewInject(R.id.chk_calibrate_current)
    private RadioButton chk_Current;
    @ViewInject(R.id.chk_calibrate_current1)
    private RadioButton chk_Current1;
    @ViewInject(R.id.chk_nertral_voltage)
    private RadioButton chk_Neutral_Voltage;
    @ViewInject(R.id.chk_calibrate_voltage)
    private RadioButton chk_Voltage;
    @ViewInject(R.id.chk_calibrate_voltage1)
    private RadioButton chk_Voltage1;
    @ViewInject(R.id.ss_calibration_input_type)
    private TextView connectTypeEditText;
    HintDialog dialog;
    @ViewInject(R.id.ss_calibration_har_l1_ang)
    private TextView l1AngleTextView;
    @ViewInject(R.id.ss_calibration_value_2)
    private TextView l1AngleValueTextView;
    @ViewInject(R.id.ss_calibration_checkL1)
    private CheckBox l1CheckBox;
    @ViewInject(R.id.ss_calibration_table_lable1)
    private TextView l1RmsTableTextView;
    @ViewInject(R.id.ss_calibration_har_l1_rms)
    private TextView l1RmsTextView;
    @ViewInject(R.id.ss_calibration_value_1)
    private TextView l1RmsValueTextView;
    @ViewInject(R.id.ss_calibration_har_l2_ang)
    private TextView l2AngleTextView;
    @ViewInject(R.id.ss_calibration_value_4)
    private TextView l2AngleValueTextView;
    @ViewInject(R.id.ss_calibration_checkL2)
    private CheckBox l2CheckBox;
    @ViewInject(R.id.ss_calibration_table_lable2)
    private TextView l2RmsTableTextView;
    @ViewInject(R.id.ss_calibration_har_l2_rms)
    private TextView l2RmsTextView;
    @ViewInject(R.id.ss_calibration_value_3)
    private TextView l2RmsValueTextView;
    @ViewInject(R.id.ss_calibration_har_l3_ang)
    private TextView l3AngleTextView;
    @ViewInject(R.id.ss_calibration_value_6)
    private TextView l3AngleValueTextView;
    @ViewInject(R.id.ss_calibration_checkL3)
    private CheckBox l3CheckBox;
    @ViewInject(R.id.ss_calibration_table_lable3)
    private TextView l3RmsTableTextView;
    @ViewInject(R.id.ss_calibration_har_l3_rms)
    private TextView l3RmsTextView;
    @ViewInject(R.id.ss_calibration_value_5)
    private TextView l3RmsValueTextView;
    @ViewInject(R.id.layout_calibrate_all_point)
    private LinearLayout layout_All_Point;
    @ViewInject(R.id.layout_neutral)
    private LinearLayout layout_Neutral;
    @ViewInject(R.id.layout_normal)
    private LinearLayout layout_Normal;
    @ViewInject(R.id.layout_calibrate_one_point)
    private LinearLayout layout_One_Point;
    @ViewInject(R.id.layout_standard_info)
    private LinearLayout layout_Standard_Info;
    BasicMeasurement m_BasicMeasurement = new BasicMeasurement();
    private CalibrateStep m_Calibrate;
    private boolean m_IsAllPoint = false;
    private StandardInfoView m_StandardInfoView;
    private int m_Type = 0;
    private int operateState = 0;
    @ViewInject(R.id.ss_calibration_input_type1)
    private TextView rangeEdittext;
    private TimerThread readDataThread = null;
    private HandlerSwitchView switchView;
    @ViewInject(R.id.txt_connect_type)
    private TextView txt_ConnectType;
    @ViewInject(R.id.txt_current_range)
    private TextView txt_CurrentRange;
    @ViewInject(R.id.txt_neutral_range)
    private TextView txt_Neutral_Range;
    @ViewInject(R.id.txt_neutral_standard_value)
    private TextView txt_Neutral_Standard;
    @ViewInject(R.id.txt_neutral_standard_unit)
    private TextView txt_Neutral_Unit;
    @ViewInject(R.id.txt_neutral_device_value)
    private TextView txt_Neutral_Value;
    @ViewInject(R.id.txt_voltage_range)
    private TextView txt_VolatageRange;
    @ViewInject(R.id.layout_wiring)
    private View wiringLayout;
    private WiringAndRangeView wiringView;

    /* access modifiers changed from: package-private */
    public class CalibrateStep {
        int ConnectType;
        int CurrentIndex;
        String[] CurrentRange;
        int VoltageIndex;
        String[] VoltageRange;
        boolean isLast = false;
        boolean isStart = false;
        boolean justVoltage = true;

        CalibrateStep() {
        }

        /* access modifiers changed from: package-private */
        public String getVoltageRangeString() {
            return this.VoltageRange[this.VoltageIndex];
        }

        /* access modifiers changed from: package-private */
        public String getCurrentRangeString() {
            if (this.justVoltage) {
                return "---";
            }
            return this.CurrentRange[this.CurrentIndex];
        }

        /* access modifiers changed from: package-private */
        public void setNext() {
            if (this.justVoltage) {
                this.VoltageIndex++;
                if (this.VoltageIndex == this.VoltageRange.length - 1) {
                    this.isLast = true;
                    return;
                }
                return;
            }
            this.CurrentIndex++;
            if (this.CurrentIndex >= this.CurrentRange.length) {
                this.VoltageIndex++;
                this.CurrentIndex = 0;
            }
            if (this.VoltageIndex == this.VoltageRange.length - 1 && this.CurrentIndex == this.CurrentRange.length - 1) {
                this.isLast = true;
            }
        }

        /* access modifiers changed from: package-private */
        public void setPremise() {
            if (this.justVoltage) {
                this.VoltageIndex--;
            } else {
                this.CurrentIndex--;
                if (this.CurrentIndex < 0) {
                    this.VoltageIndex--;
                    this.CurrentIndex = this.CurrentRange.length - 1;
                }
            }
            this.isLast = false;
        }
    }

    @Override // android.support.v4.app.Fragment
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        this.fragmentView = inflater.inflate(R.layout.fragment_calibration, (ViewGroup) null);
        ViewUtils.inject(this, this.fragmentView);
        return this.fragmentView;
    }

    @Override // android.support.v4.app.Fragment, com.clou.rs350.ui.fragment.BaseFragment
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initView();
        initData();
        this.allCheckBox.setOnCheckedChangeListener(this.checkListener);
        this.l1CheckBox.setOnCheckedChangeListener(this.checkListener);
        this.l2CheckBox.setOnCheckedChangeListener(this.checkListener);
        this.l3CheckBox.setOnCheckedChangeListener(this.checkListener);
        this.chk_Voltage.setOnCheckedChangeListener(this.checkListener);
        this.chk_Current.setOnCheckedChangeListener(this.checkListener);
        this.chk_Voltage1.setOnCheckedChangeListener(this.checkListener);
        this.chk_Current1.setOnCheckedChangeListener(this.checkListener);
        this.chk_Neutral_Voltage.setOnCheckedChangeListener(this.checkListener);
    }

    private void initView() {
        List<View> tabViews = new ArrayList<>();
        tabViews.add(findViewById(R.id.btn_one_point));
        tabViews.add(findViewById(R.id.btn_all_point));
        tabViews.add(findViewById(R.id.btn_neutral));
        this.switchView = new HandlerSwitchView(this.m_Context);
        this.switchView.setTabViews(tabViews);
        this.switchView.boundClick();
        this.switchView.setOnTabChangedCallback(this);
        this.switchView.selectView(0);
        this.wiringView = new WiringAndRangeView(this.m_Context, this.wiringLayout);
        this.wiringView.setOverloadClick(new View.OnClickListener() {
            /* class com.clou.rs350.ui.fragment.settingfragment.CalibrationFragment.AnonymousClass2 */

            public void onClick(View v) {
                if (CalibrationFragment.this.m_Calibrate != null && CalibrationFragment.this.m_Calibrate.isStart) {
                    CalibrationFragment.this.SetRange(false, new int[]{CalibrationFragment.this.m_Calibrate.ConnectType, CalibrationFragment.this.m_Calibrate.VoltageIndex + 1, CalibrationFragment.this.m_Calibrate.CurrentIndex + 1, CalibrationFragment.this.m_Calibrate.VoltageIndex + 1, CalibrationFragment.this.m_Calibrate.CurrentIndex + 1, CalibrationFragment.this.m_Calibrate.VoltageIndex + 1, CalibrationFragment.this.m_Calibrate.CurrentIndex + 1});
                }
            }
        });
        this.m_StandardInfoView = new StandardInfoView(this.m_Context, this.layout_Standard_Info);
    }

    @Override // android.support.v4.app.Fragment, com.clou.rs350.ui.fragment.BaseFragment
    public void onResume() {
        super.onResume();
        this.messageManager.SetRatio(0.0d, 0.0d, 0);
        this.messageManager.setDeviceAngleDefinition(false);
        readTestData();
    }

    private void initData() {
        this.m_BasicMeasurement = new BasicMeasurement();
        this.m_Calibrate = new CalibrateStep();
    }

    private void cleanRangeView() {
        this.txt_VolatageRange.setText(PdfObject.NOTHING);
        this.txt_CurrentRange.setText(PdfObject.NOTHING);
        this.rangeEdittext.setText(PdfObject.NOTHING);
    }

    @OnClick({R.id.ss_calibration_input_type, R.id.ss_calibration_input_type1, R.id.ss_calibration_start, R.id.ss_calibration_value_1, R.id.ss_calibration_value_2, R.id.ss_calibration_value_3, R.id.ss_calibration_value_4, R.id.ss_calibration_value_5, R.id.ss_calibration_value_6, R.id.btn_start, R.id.btn_previous, R.id.btn_next, R.id.txt_connect_type, R.id.txt_neutral_range, R.id.txt_neutral_standard_value, R.id.ss_calibration_dc_offset, R.id.ss_reset_adjustment})
    public void onClick(View v) {
        MeterBaseDevice device = this.messageManager.getMeter();
        NumericDialog number = new NumericDialog(this.m_Context);
        String range = this.rangeEdittext.getText().toString().trim();
        switch (v.getId()) {
            case R.id.btn_previous:
                this.m_Calibrate.setPremise();
                GoStep();
                return;
            case R.id.btn_next:
                if (getString(R.string.text_finish) != this.btn_Next.getText().toString()) {
                    this.m_Calibrate.setNext();
                    GoStep();
                    return;
                }
                setButton(false);
                this.m_Calibrate.isStart = false;
                int[] iArr = new int[7];
                iArr[0] = this.m_Calibrate.ConnectType;
                SetRange(false, iArr);
                new StandardInfoDialog(this.m_Context).show();
                return;
            case R.id.txt_neutral_range:
                if (this.chk_Neutral_Voltage.isChecked()) {
                    ListPopWindow.showListPopwindow(this.m_Context, device.getS_Neutral_Voltage_Range(), v, new View.OnClickListener() {
                        /* class com.clou.rs350.ui.fragment.settingfragment.CalibrationFragment.AnonymousClass5 */

                        public void onClick(View v) {
                            CalibrationFragment.this.messageManager.SetNeutralRange(1, Integer.parseInt(v.getTag().toString().trim()), 0);
                        }
                    }, 1);
                } else {
                    ListPopWindow.showListPopwindow(this.m_Context, device.getS_Neutral_Current_Range(), v, new View.OnClickListener() {
                        /* class com.clou.rs350.ui.fragment.settingfragment.CalibrationFragment.AnonymousClass6 */

                        public void onClick(View v) {
                            CalibrationFragment.this.messageManager.SetNeutralRange(2, 0, Integer.parseInt(v.getTag().toString().trim()));
                        }
                    }, 1);
                }
                this.txt_Neutral_Standard.setText(PdfObject.NOTHING);
                return;
            case R.id.txt_neutral_standard_value:
                if (OtherUtils.isEmpty(this.txt_Neutral_Range)) {
                    new HintDialog(this.m_Context, (int) R.string.text_please_select_range).show();
                    return;
                } else {
                    number.showMyDialog((TextView) v);
                    return;
                }
            case R.id.ss_calibration_input_type:
            case R.id.txt_connect_type:
                if (this.chk_Current.isChecked()) {
                    ListPopWindow.showListPopwindow(this.m_Context, device.getS_ConnectType_Current(), v, null);
                } else {
                    ListPopWindow.showListPopwindow(this.m_Context, device.getS_ConnectType_Voltage(), v, null);
                }
                cleanRangeView();
                return;
            case R.id.ss_calibration_input_type1:
                if (TextUtils.isEmpty(this.connectTypeEditText.getText().toString().trim())) {
                    new HintDialog(this.m_Context, (int) R.string.text_please_select_connect).show();
                    return;
                }
                ConTypeRange ctr = new ConTypeRange();
                ctr.connectType = device.getS_ConnectType_Realtime();
                ctr.connectTypeIndex = device.getI_ConnectType_Realtime_Index();
                ctr.voltageArray = device.getVoltageRange();
                ctr.currentArray = device.getCurrentRange();
                int index = ctr.connectTypeIndex[OtherUtils.convertToInt(this.connectTypeEditText.getTag())];
                final int[] values = new int[7];
                values[0] = index;
                if (this.chk_Current.isChecked()) {
                    ListPopWindow.showListPopwindow(this.m_Context, ctr.currentArray.get(index), v, new View.OnClickListener() {
                        /* class com.clou.rs350.ui.fragment.settingfragment.CalibrationFragment.AnonymousClass3 */

                        public void onClick(View v) {
                            int position = Integer.parseInt(v.getTag().toString().trim()) + 1;
                            values[2] = position;
                            values[4] = position;
                            values[6] = position;
                            CalibrationFragment.this.SetRange(false, values);
                            CalibrationFragment.this.clearStandardValue();
                        }
                    }, 1);
                    return;
                } else {
                    ListPopWindow.showListPopwindow(this.m_Context, ctr.voltageArray.get(index), v, new View.OnClickListener() {
                        /* class com.clou.rs350.ui.fragment.settingfragment.CalibrationFragment.AnonymousClass4 */

                        public void onClick(View v) {
                            int position = Integer.parseInt(v.getTag().toString().trim()) + 1;
                            values[1] = position;
                            values[3] = position;
                            values[5] = position;
                            CalibrationFragment.this.SetRange(false, values);
                            CalibrationFragment.this.clearStandardValue();
                        }
                    }, 1);
                    return;
                }
            case R.id.btn_start:
                if (!OtherUtils.isEmpty(this.txt_ConnectType)) {
                    setButton(true);
                    this.m_Calibrate = new CalibrateStep();
                    this.m_Calibrate.justVoltage = this.chk_Voltage1.isChecked();
                    this.m_Calibrate.ConnectType = OtherUtils.convertToInt(this.txt_ConnectType.getTag());
                    if (this.m_Calibrate.justVoltage) {
                        this.m_Calibrate.VoltageRange = device.getVoltageRange().get(this.m_Calibrate.ConnectType);
                    } else {
                        this.m_Calibrate.VoltageRange = device.getVoltageRange().get(0);
                    }
                    this.m_Calibrate.CurrentRange = device.getCurrentRange().get(this.m_Calibrate.ConnectType);
                    this.m_Calibrate.isStart = true;
                    GoStep();
                    return;
                }
                new HintDialog(this.m_Context, (int) R.string.text_please_select_connect).show();
                return;
            case R.id.ss_calibration_value_1:
            case R.id.ss_calibration_value_2:
            case R.id.ss_calibration_value_3:
            case R.id.ss_calibration_value_4:
            case R.id.ss_calibration_value_5:
            case R.id.ss_calibration_value_6:
                if (!TextUtils.isEmpty(range) || this.m_Calibrate.isStart) {
                    number.showMyDialog((TextView) v);
                    return;
                } else {
                    new HintDialog(this.m_Context, (int) R.string.text_please_select_range).show();
                    return;
                }
            case R.id.ss_reset_adjustment:
                String sPassword = new SimpleDateFormat("yyyyMMdd").format(new Date(System.currentTimeMillis())).toString();
                this.messageManager.GetAllowAdjustFlag();
                PasswordDialog passwordDialog = new PasswordDialog(this.m_Context, sPassword, new PasswordCallback() {
                    /* class com.clou.rs350.ui.fragment.settingfragment.CalibrationFragment.AnonymousClass8 */

                    @Override // com.clou.rs350.callback.PasswordCallback
                    public void onPasswordIsMistake() {
                    }

                    @Override // com.clou.rs350.callback.PasswordCallback
                    public void onPasswordIsCorrect() {
                        NormalDialog tmpNormalDialog = new NormalDialog(CalibrationFragment.this.m_Context);
                        tmpNormalDialog.setOnClick(new View.OnClickListener() {
                            /* class com.clou.rs350.ui.fragment.settingfragment.CalibrationFragment.AnonymousClass8.AnonymousClass1 */

                            public void onClick(View v) {
                                CalibrationFragment.this.messageManager.setResetAdjustment(1);
                                new HintDialog(CalibrationFragment.this.m_Context, (int) R.string.text_restart_device).show();
                            }
                        }, null);
                        tmpNormalDialog.show(R.string.text_whether_reset_adjustment);
                    }
                });
                passwordDialog.setOnDismiss(new DialogInterface.OnDismissListener() {
                    /* class com.clou.rs350.ui.fragment.settingfragment.CalibrationFragment.AnonymousClass9 */

                    public void onDismiss(DialogInterface dialog) {
                    }
                });
                passwordDialog.show();
                return;
            case R.id.ss_calibration_dc_offset:
                if (!TextUtils.isEmpty(range) || this.m_Calibrate.isStart) {
                    NormalDialog tmpDialog = new NormalDialog(this.m_Context);
                    tmpDialog.setOnClick(new View.OnClickListener() {
                        /* class com.clou.rs350.ui.fragment.settingfragment.CalibrationFragment.AnonymousClass7 */

                        public void onClick(View v) {
                            CalibrationFragment.this.messageManager.setDCOffsetAdjust(1);
                            CalibrationFragment.this.calibrateDialog = new CountdownDialog(CalibrationFragment.this.m_Context);
                            CalibrationFragment.this.calibrateDialog.showDialog(2, new ISleepCallback() {
                                /* class com.clou.rs350.ui.fragment.settingfragment.CalibrationFragment.AnonymousClass7.AnonymousClass1 */

                                @Override // com.clou.rs350.task.ISleepCallback
                                public void onSleepAfterToDo() {
                                    CalibrationFragment.this.calibrateDialog.dismiss();
                                    new HintDialog(CalibrationFragment.this.m_Context, (int) R.string.text_success).show();
                                }
                            });
                        }
                    }, null);
                    tmpDialog.show();
                    tmpDialog.setTitle(R.string.text_dont_measure_current);
                    return;
                }
                new HintDialog(this.m_Context, (int) R.string.text_please_select_range).show();
                return;
            case R.id.ss_calibration_start:
                startCalibrate();
                return;
            default:
                return;
        }
    }

    private void GoStep() {
        clearStandardValue();
        if (this.m_Calibrate.VoltageIndex == 0 && this.m_Calibrate.CurrentIndex == 0) {
            this.btn_Previous.setEnabled(false);
        } else {
            this.btn_Previous.setEnabled(true);
        }
        if (this.m_Calibrate.isLast) {
            this.btn_Next.setText(R.string.text_finish);
        } else {
            this.btn_Next.setText(R.string.text_next);
        }
        Resources resource = getResources();
        String hintString = String.valueOf(resource.getString(R.string.text_please_set_power)) + "\r\n" + resource.getString(R.string.text_voltage) + ":" + this.m_Calibrate.getVoltageRangeString();
        if (!this.m_Calibrate.justVoltage) {
            hintString = String.valueOf(hintString) + " " + resource.getString(R.string.text_current) + ":" + this.m_Calibrate.getCurrentRangeString();
        }
        int[] iArr = new int[7];
        iArr[0] = this.m_Calibrate.ConnectType;
        SetRange(false, iArr);
        HintDialog hintDialog = new HintDialog(this.m_Context);
        hintDialog.setOnClick(new View.OnClickListener() {
            /* class com.clou.rs350.ui.fragment.settingfragment.CalibrationFragment.AnonymousClass10 */

            public void onClick(View v) {
                CalibrationFragment.this.SetRange(false, new int[]{CalibrationFragment.this.m_Calibrate.ConnectType, CalibrationFragment.this.m_Calibrate.VoltageIndex + 1, CalibrationFragment.this.m_Calibrate.CurrentIndex + 1, CalibrationFragment.this.m_Calibrate.VoltageIndex + 1, CalibrationFragment.this.m_Calibrate.CurrentIndex + 1, CalibrationFragment.this.m_Calibrate.VoltageIndex + 1, CalibrationFragment.this.m_Calibrate.CurrentIndex + 1});
                CalibrationFragment.this.txt_CurrentRange.setText(CalibrationFragment.this.m_Calibrate.getCurrentRangeString());
                CalibrationFragment.this.txt_VolatageRange.setText(CalibrationFragment.this.m_Calibrate.getVoltageRangeString());
            }
        });
        hintDialog.show();
        hintDialog.setTitle(String.valueOf(hintString) + "\r\n" + resource.getString(R.string.text_then));
    }

    private void startCalibrate() {
        if (this.m_Type == 0) {
            if (TextUtils.isEmpty(this.rangeEdittext.getText().toString().trim()) && !this.m_Calibrate.isStart) {
                new HintDialog(this.m_Context, (int) R.string.text_please_select_range).show();
                return;
            }
        } else if (OtherUtils.isEmpty(this.txt_Neutral_Range)) {
            new HintDialog(this.m_Context, (int) R.string.text_please_select_range).show();
            return;
        }
        if (this.m_Type == 0) {
            adjust();
        } else {
            double AdjustValue = OtherUtils.parseDouble(this.txt_Neutral_Standard.getText().toString());
            if (this.chk_Neutral_Voltage.isChecked()) {
                this.messageManager.setNeutralAdjust(AdjustValue, -1.0d);
            } else {
                this.messageManager.setNeutralAdjust(-1.0d, AdjustValue);
            }
        }
        this.operateState = 1;
        this.calibrateDialog = new CountdownDialog(this.m_Context);
        this.calibrateDialog.showDialog(60, new ISleepCallback() {
            /* class com.clou.rs350.ui.fragment.settingfragment.CalibrationFragment.AnonymousClass11 */

            @Override // com.clou.rs350.task.ISleepCallback
            public void onSleepAfterToDo() {
                CalibrationFragment.this.stopAdjust();
                CalibrationFragment.this.showCalibrationResultDialog(R.string.text_failed);
            }
        });
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private void setDCbtn() {
        this.btn_Calibrate_DC.setEnabled(this.m_Type == 0 && this.chk_Current.isChecked());
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private void clearStandardValue() {
        this.l1RmsValueTextView.setText(PdfObject.NOTHING);
        this.l2RmsValueTextView.setText(PdfObject.NOTHING);
        this.l3RmsValueTextView.setText(PdfObject.NOTHING);
        this.l1AngleValueTextView.setText(PdfObject.NOTHING);
        this.l2AngleValueTextView.setText(PdfObject.NOTHING);
        this.l3AngleValueTextView.setText(PdfObject.NOTHING);
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private void SetRange(boolean AutoConnectType, int[] values) {
        MeterBaseDevice device = this.messageManager.getMeter();
        int connectType = device.getI_ConnectType_Realtime_Index()[values[0]];
        this.messageManager.SetRange(AutoConnectType ? 0 : 1, connectType + (connectType << 16) + (connectType << 8), Range.getAuto(new int[]{values[1], values[2], values[3], values[4], values[5], values[6]}), (device.getVoltageRangeIndex()[0] + values[1]) - 1, (device.getVoltageRangeIndex()[0] + values[3]) - 1, (device.getVoltageRangeIndex()[0] + values[5]) - 1, (device.getCurrentRangeIndex()[connectType] + values[2]) - 1, (device.getCurrentRangeIndex()[connectType] + values[4]) - 1, (device.getCurrentRangeIndex()[connectType] + values[6]) - 1);
    }

    private void adjust() {
        this.messageManager.setAdjust(getUaValue(), getUbValue(), getUcValue(), getIaValue(), getIbValue(), getIcValue(), getUaAngle(), getUbAngle(), getUcAngle(), getIaAngle(), getIbAngle(), getIcAngle());
    }

    private void readTestData() {
        int refreshTime = Preferences.getInt(Preferences.Key.REFRESHTIME, 1500);
        if (this.readDataThread == null || this.readDataThread.isStop()) {
            this.readDataThread = new TimerThread((long) refreshTime, new ISleepCallback() {
                /* class com.clou.rs350.ui.fragment.settingfragment.CalibrationFragment.AnonymousClass12 */

                @Override // com.clou.rs350.task.ISleepCallback
                public void onSleepAfterToDo() {
                    if (CalibrationFragment.this.operateState == 0) {
                        CalibrationFragment.this.messageManager.getData();
                    } else {
                        CalibrationFragment.this.messageManager.getAdjustState();
                    }
                    CalibrationFragment.this.messageManager.getRanage();
                }
            }, "readDataThread");
            this.readDataThread.start();
        }
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private void stopAdjust() {
        this.messageManager.setStopAdjust();
        this.operateState = 0;
    }

    @Override // com.clou.rs350.ui.fragment.BaseFragment, com.clou.rs350.manager.MessageManager.IMessageListener
    public void onReceiveMessage(MeterBaseDevice Device) {
        switch (this.operateState) {
            case 0:
                showData(Device);
                break;
            case 1:
                getAdjustState(Device);
                break;
        }
        this.m_BasicMeasurement.parseData(Device);
        this.wiringView.showData(this.m_BasicMeasurement);
        this.m_StandardInfoView.showData();
    }

    private void showData(MeterBaseDevice Device) {
        String Ua = Device.getLNVoltageValue()[0].getStringValue();
        String Ub = Device.getLNVoltageValue()[1].getStringValue();
        String Uc = Device.getLNVoltageValue()[2].getStringValue();
        String Ia = Device.getCurrentValue()[0].getStringValue();
        String Ib = Device.getCurrentValue()[1].getStringValue();
        String Ic = Device.getCurrentValue()[2].getStringValue();
        String sUa = Device.getLNVoltageAngleValue()[0].getStringValue();
        String sUb = Device.getLNVoltageAngleValue()[1].getStringValue();
        String sUc = Device.getLNVoltageAngleValue()[2].getStringValue();
        String sIa = Device.getCurrentAngleValue()[0].getStringValue();
        String sIb = Device.getCurrentAngleValue()[1].getStringValue();
        String sIc = Device.getCurrentAngleValue()[2].getStringValue();
        if (this.chk_Voltage.isChecked()) {
            this.l1RmsTextView.setText(Ua);
            this.l1AngleTextView.setText(sUa);
            this.l2RmsTextView.setText(Ub);
            this.l2AngleTextView.setText(sUb);
            this.l3RmsTextView.setText(Uc);
            this.l3AngleTextView.setText(sUc);
        } else {
            this.l1RmsTextView.setText(Ia);
            this.l1AngleTextView.setText(sIa);
            this.l2RmsTextView.setText(Ib);
            this.l2AngleTextView.setText(sIb);
            this.l3RmsTextView.setText(Ic);
            this.l3AngleTextView.setText(sIc);
        }
        if (this.chk_Neutral_Voltage.isChecked()) {
            this.txt_Neutral_Value.setText(Device.getNeutralValue()[0].getStringValue());
        } else {
            this.txt_Neutral_Value.setText(Device.getNeutralValue()[1].getStringValue());
        }
    }

    private void getAdjustState(MeterBaseDevice Device) {
        if (Device.getAdjustResult()) {
            stopAdjust();
            if (this.m_IsAllPoint) {
                showCalibrationResultDialog(R.string.text_success);
            } else {
                showCalibrationResultDialog(R.string.text_success, new View.OnClickListener() {
                    /* class com.clou.rs350.ui.fragment.settingfragment.CalibrationFragment.AnonymousClass13 */

                    public void onClick(View v) {
                        new StandardInfoDialog(CalibrationFragment.this.m_Context).show();
                    }
                });
            }
        }
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private void showCalibrationResultDialog(final int resid) {
        new SleepTask(500, new ISleepCallback() {
            /* class com.clou.rs350.ui.fragment.settingfragment.CalibrationFragment.AnonymousClass14 */

            @Override // com.clou.rs350.task.ISleepCallback
            public void onSleepAfterToDo() {
                if (CalibrationFragment.this.calibrateDialog != null && CalibrationFragment.this.calibrateDialog.isShowing()) {
                    CalibrationFragment.this.calibrateDialog.dismiss();
                }
                if (CalibrationFragment.this.dialog == null || !CalibrationFragment.this.dialog.isShowing()) {
                    CalibrationFragment.this.dialog = new HintDialog(CalibrationFragment.this.m_Context);
                    CalibrationFragment.this.dialog.show();
                    CalibrationFragment.this.dialog.setTitle(resid);
                }
            }
        }).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, new Integer[0]);
    }

    private void showCalibrationResultDialog(final int resid, final View.OnClickListener okClick) {
        new SleepTask(500, new ISleepCallback() {
            /* class com.clou.rs350.ui.fragment.settingfragment.CalibrationFragment.AnonymousClass15 */

            @Override // com.clou.rs350.task.ISleepCallback
            public void onSleepAfterToDo() {
                if (CalibrationFragment.this.calibrateDialog != null && CalibrationFragment.this.calibrateDialog.isShowing()) {
                    CalibrationFragment.this.calibrateDialog.dismiss();
                }
                if (CalibrationFragment.this.dialog == null || !CalibrationFragment.this.dialog.isShowing()) {
                    CalibrationFragment.this.dialog = new HintDialog(CalibrationFragment.this.m_Context);
                    CalibrationFragment.this.dialog.setOnClick(okClick);
                    CalibrationFragment.this.dialog.show();
                    CalibrationFragment.this.dialog.setTitle(resid);
                }
            }
        }).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, new Integer[0]);
    }

    @Override // android.support.v4.app.Fragment, com.clou.rs350.ui.fragment.BaseFragment
    public void onPause() {
        super.onPause();
        this.messageManager.SetRatio(0.0d, 0.0d, ClouData.getInstance().getSiteData().RatioApply);
        this.messageManager.setDeviceAngleDefinition(true);
        this.messageManager.setFunc_Mstate(0);
        killThread(this.readDataThread);
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private void checkTextView() {
        boolean L1Checked = this.l1CheckBox.isChecked();
        boolean L2Checked = this.l2CheckBox.isChecked();
        boolean L3Checked = this.l3CheckBox.isChecked();
        this.allCheckBox.setOnCheckedChangeListener(null);
        if (!L1Checked || !L2Checked || !L3Checked) {
            this.allCheckBox.setChecked(false);
        } else {
            this.allCheckBox.setChecked(true);
        }
        this.allCheckBox.setOnCheckedChangeListener(this.checkListener);
        checkTextView(L1Checked, L2Checked, L3Checked);
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private void checkTextView(boolean L1Checked, boolean L2Checked, boolean L3Checked) {
        setEnable(this.l1RmsValueTextView, L1Checked);
        setEnable(this.l1AngleValueTextView, L1Checked);
        setEnable(this.l2RmsValueTextView, L2Checked);
        setEnable(this.l2AngleValueTextView, L2Checked);
        setEnable(this.l3RmsValueTextView, L3Checked);
        setEnable(this.l3AngleValueTextView, L3Checked);
    }

    private void setEnable(TextView view, boolean enable) {
        view.setEnabled(enable);
    }

    private double getIcAngle() {
        if (!this.chk_Current.isChecked() || !this.l3CheckBox.isChecked()) {
            return -1.0d;
        }
        String value = this.l3AngleValueTextView.getText().toString().trim();
        if (TextUtils.isEmpty(value)) {
            return -1.0d;
        }
        return OtherUtils.parseDouble(value);
    }

    private double getIbAngle() {
        if (!this.chk_Current.isChecked() || !this.l2CheckBox.isChecked()) {
            return -1.0d;
        }
        String value = this.l2AngleValueTextView.getText().toString().trim();
        if (TextUtils.isEmpty(value)) {
            return -1.0d;
        }
        return OtherUtils.parseDouble(value);
    }

    private double getIaAngle() {
        if (!this.chk_Current.isChecked() || !this.l1CheckBox.isChecked()) {
            return -1.0d;
        }
        String value = this.l1AngleValueTextView.getText().toString().trim();
        if (TextUtils.isEmpty(value)) {
            return -1.0d;
        }
        return OtherUtils.parseDouble(value);
    }

    private double getUcAngle() {
        if (!this.chk_Voltage.isChecked() || !this.l3CheckBox.isChecked()) {
            return -1.0d;
        }
        String value = this.l3AngleValueTextView.getText().toString().trim();
        if (TextUtils.isEmpty(value)) {
            return -1.0d;
        }
        return OtherUtils.parseDouble(value);
    }

    private double getUaAngle() {
        if (!this.chk_Voltage.isChecked() || !this.l1CheckBox.isChecked()) {
            return -1.0d;
        }
        String value = this.l1AngleValueTextView.getText().toString().trim();
        if (TextUtils.isEmpty(value)) {
            return -1.0d;
        }
        return OtherUtils.parseDouble(value);
    }

    private double getUbAngle() {
        if (!this.chk_Voltage.isChecked() || !this.l2CheckBox.isChecked()) {
            return -1.0d;
        }
        String value = this.l2AngleValueTextView.getText().toString().trim();
        if (TextUtils.isEmpty(value)) {
            return -1.0d;
        }
        return OtherUtils.parseDouble(value);
    }

    private double getIcValue() {
        if (!this.chk_Current.isChecked() || !this.l3CheckBox.isChecked()) {
            return -1.0d;
        }
        double dValue = OtherUtils.parseDouble(this.l3RmsValueTextView.getText().toString().trim());
        if (dValue == 0.0d) {
            return -1.0d;
        }
        return dValue;
    }

    private double getIbValue() {
        if (!this.chk_Current.isChecked() || !this.l2CheckBox.isChecked()) {
            return -1.0d;
        }
        double dValue = OtherUtils.parseDouble(this.l2RmsValueTextView.getText().toString().trim());
        if (dValue == 0.0d) {
            return -1.0d;
        }
        return dValue;
    }

    private double getIaValue() {
        if (!this.chk_Current.isChecked() || !this.l1CheckBox.isChecked()) {
            return -1.0d;
        }
        double dValue = OtherUtils.parseDouble(this.l1RmsValueTextView.getText().toString().trim());
        if (dValue == 0.0d) {
            return -1.0d;
        }
        return dValue;
    }

    private double getUcValue() {
        if (!this.chk_Voltage.isChecked() || !this.l3CheckBox.isChecked()) {
            return -1.0d;
        }
        double dValue = OtherUtils.parseDouble(this.l3RmsValueTextView.getText().toString().trim());
        if (dValue == 0.0d) {
            return -1.0d;
        }
        return dValue;
    }

    private double getUbValue() {
        if (!this.chk_Voltage.isChecked() || !this.l2CheckBox.isChecked()) {
            return -1.0d;
        }
        double dValue = OtherUtils.parseDouble(this.l2RmsValueTextView.getText().toString().trim());
        if (dValue == 0.0d) {
            return -1.0d;
        }
        return dValue;
    }

    private double getUaValue() {
        if (!this.chk_Voltage.isChecked() || !this.l1CheckBox.isChecked()) {
            return -1.0d;
        }
        double dValue = OtherUtils.parseDouble(this.l1RmsValueTextView.getText().toString().trim());
        if (dValue == 0.0d) {
            return -1.0d;
        }
        return dValue;
    }

    @Override // android.support.v4.app.Fragment
    public void onDestroy() {
        int[] values = new int[7];
        values[0] = OtherUtils.convertToInt(this.connectTypeEditText.getTag());
        SetRange(Preferences.getBoolean(Preferences.Key.AUTOCONNECTTYPE, true), values);
        this.messageManager.SetNeutralRange(0, 0, 0);
        super.onDestroy();
    }

    private void initAllPoint() {
        this.txt_ConnectType.setText(PdfObject.NOTHING);
        this.txt_VolatageRange.setText(PdfObject.NOTHING);
        this.txt_CurrentRange.setText(PdfObject.NOTHING);
        setButton(false);
        clearStandardValue();
    }

    private void setButton(boolean isStartCalibrate) {
        int i;
        boolean z;
        boolean z2;
        int i2 = 8;
        boolean z3 = false;
        this.btn_Previous.setVisibility(isStartCalibrate ? 0 : 8);
        Button button = this.btn_Next;
        if (isStartCalibrate) {
            i = 0;
        } else {
            i = 8;
        }
        button.setVisibility(i);
        Button button2 = this.btn_Start;
        if (!isStartCalibrate) {
            i2 = 0;
        }
        button2.setVisibility(i2);
        this.btn_Calibrate.setEnabled(isStartCalibrate);
        TextView textView = this.txt_ConnectType;
        if (isStartCalibrate) {
            z = false;
        } else {
            z = true;
        }
        textView.setEnabled(z);
        RadioButton radioButton = this.chk_Current1;
        if (isStartCalibrate) {
            z2 = false;
        } else {
            z2 = true;
        }
        radioButton.setEnabled(z2);
        RadioButton radioButton2 = this.chk_Voltage1;
        if (!isStartCalibrate) {
            z3 = true;
        }
        radioButton2.setEnabled(z3);
    }

    @Override // com.clou.rs350.handler.HandlerSwitchView.IOnSelectCallback
    public void onSelectedChanged(int index) {
        switch (index) {
            case 0:
                this.m_IsAllPoint = false;
                this.layout_One_Point.setVisibility(0);
                this.layout_All_Point.setVisibility(8);
                checkTextView();
                this.btn_Calibrate.setEnabled(true);
                this.m_Calibrate.isStart = false;
                setCalibrateType(0);
                return;
            case 1:
                this.m_IsAllPoint = true;
                this.layout_One_Point.setVisibility(8);
                this.layout_All_Point.setVisibility(0);
                checkTextView(true, true, true);
                initAllPoint();
                setCalibrateType(0);
                return;
            case 2:
                this.m_IsAllPoint = false;
                this.btn_Calibrate.setEnabled(true);
                this.m_Calibrate.isStart = false;
                setCalibrateType(1);
                return;
            case 3:
                this.layout_Standard_Info.setVisibility(0);
                this.layout_Normal.setVisibility(8);
                this.layout_Neutral.setVisibility(8);
                this.btn_Calibrate.setEnabled(false);
                return;
            default:
                return;
        }
    }

    private void setCalibrateType(int type) {
        int i;
        int i2 = 8;
        this.m_Type = type;
        this.layout_Standard_Info.setVisibility(8);
        LinearLayout linearLayout = this.layout_Normal;
        if (type == 0) {
            i = 0;
        } else {
            i = 8;
        }
        linearLayout.setVisibility(i);
        LinearLayout linearLayout2 = this.layout_Neutral;
        if (type != 0) {
            i2 = 0;
        }
        linearLayout2.setVisibility(i2);
        setDCbtn();
    }

    @Override // com.clou.rs350.handler.HandlerSwitchView.IOnSelectCallback
    public void onClickView(View view) {
    }
}
