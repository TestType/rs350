package com.clou.rs350.ui.fragment.errorfragment;

import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.clou.rs350.Preferences;
import com.clou.rs350.R;
import com.clou.rs350.db.model.BasicMeasurement;
import com.clou.rs350.db.model.ErrorTest;
import com.clou.rs350.db.model.SiteData;
import com.clou.rs350.device.MeterBaseDevice;
import com.clou.rs350.model.ClouData;
import com.clou.rs350.task.ISleepCallback;
import com.clou.rs350.task.SleepTask;
import com.clou.rs350.task.TimerThread;
import com.clou.rs350.ui.activity.BaseActivity;
import com.clou.rs350.ui.adapter.ErrorAdapter;
import com.clou.rs350.ui.dialog.HintDialog;
import com.clou.rs350.ui.popwindow.ErrorConfigurePopWindow;
import com.clou.rs350.ui.popwindow.TrxRatioPopWindow;
import com.clou.rs350.ui.view.MyErrorGraphView;
import com.clou.rs350.utils.CountDown;
import com.clou.rs350.utils.OtherUtils;
import com.itextpdf.text.html.HtmlTags;
import com.itextpdf.text.pdf.ColumnText;
import com.itextpdf.text.pdf.PdfObject;
import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.view.annotation.ViewInject;
import com.lidroid.xutils.view.annotation.event.OnClick;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;

public class ErrorTestingFragment extends BaseErrorFragment implements View.OnClickListener, CompoundButton.OnCheckedChangeListener {
    private static final String TAG = "ErrorFragment";
    private boolean autoStop = true;
    @ViewInject(R.id.btn_check)
    private Button checkBtn;
    @ViewInject(R.id.chk_meter2)
    private CheckBox chkMeter2;
    @ViewInject(R.id.chk_meter3)
    private CheckBox chkMeter3;
    @ViewInject(R.id.btn_configure)
    private Button configureBtn;
    @ViewInject(R.id.error_meter_constant1_button)
    private Button constant1Btn;
    @ViewInject(R.id.error_meter_constant2_button)
    private Button constant2Btn;
    @ViewInject(R.id.error_meter_constant3_button)
    private Button constant3Btn;
    @ViewInject(R.id.error_meter_constant_button)
    private Button constantBtn;
    private Button[] constantBtns;
    @ViewInject(R.id.error_meter_constant1_value_edittext)
    private EditText constantValue1Et;
    @ViewInject(R.id.error_meter_constant2_value_edittext)
    private EditText constantValue2Et;
    @ViewInject(R.id.error_meter_constant3_value_edittext)
    private EditText constantValue3Et;
    @ViewInject(R.id.error_meter_constant_value_edittext)
    private EditText constantValueEt;
    private EditText[] constantValueEts;
    @ViewInject(R.id.error_pulses1_cover_view)
    private View cover1View;
    @ViewInject(R.id.error_pulses2_cover_view)
    private View cover2View;
    @ViewInject(R.id.error_pulses3_cover_view)
    private View cover3View;
    private TextView e11Tv;
    private TextView e12Tv;
    private TextView e13Tv;
    private TextView e21Tv;
    private TextView e22Tv;
    private TextView e23Tv;
    private TextView e31Tv;
    private TextView e32Tv;
    private TextView e33Tv;
    private TextView e41Tv;
    private TextView e42Tv;
    private TextView e43Tv;
    private TextView e51Tv;
    private TextView e52Tv;
    private TextView e53Tv;
    private TextView eA1Tv;
    private TextView eA2Tv;
    private TextView eA3Tv;
    @ViewInject(R.id.error_progress_1)
    private ProgressBar error1PBar;
    @ViewInject(R.id.error_progress_2)
    private ProgressBar error2PBar;
    @ViewInject(R.id.error_progress_3)
    private ProgressBar error3PBar;
    @ViewInject(R.id.error_progress)
    private ProgressBar errorPBar;
    private TextView es1Tv;
    private TextView es2Tv;
    private TextView es3Tv;
    Handler handler = new Handler() {
        /* class com.clou.rs350.ui.fragment.errorfragment.ErrorTestingFragment.AnonymousClass1 */

        public void handleMessage(Message msg) {
            if (msg != null) {
                switch (msg.what) {
                    case 0:
                        ErrorTestingFragment.this.refreshBasicData();
                        return;
                    case 1:
                        ErrorTestingFragment.this.refreshPulse();
                        return;
                    default:
                        return;
                }
            }
        }
    };
    private boolean hasStart = false;
    private boolean is1Meter = false;
    private int isCheckK = 0;
    @ViewInject(R.id.list_errors)
    private ListView lstError;
    private BasicMeasurement m_BasicData;
    ErrorAdapter m_ErrorAdapter;
    private int m_ErrorCount = 0;
    private int m_ErrorTestType = 0;
    private ErrorTest[] m_ErrorTests;
    private int m_ErrorTime = 5;
    private int[] m_HasErrors = new int[3];
    private int[] m_HasPulses = new int[3];
    private HintDialog[] m_HintDialog = new HintDialog[3];
    private boolean m_IsBaseTime = false;
    private int m_PQ_Flag = 0;
    private SiteData m_Sitedata;
    private SleepTask[] m_WaitDialogTasks = new SleepTask[3];
    private int m_WaitLimit = 0;
    private int[] measurementType = new int[3];
    @ViewInject(R.id.error_pulses1_edittext)
    private EditText pulsesValue1Et;
    @ViewInject(R.id.error_pulses2_edittext)
    private EditText pulsesValue2Et;
    @ViewInject(R.id.error_pulses3_edittext)
    private EditText pulsesValue3Et;
    @ViewInject(R.id.error_pulses_edittext)
    private TextView pulsesValueEt;
    private EditText[] pulsesValueEts;
    @ViewInject(R.id.btn_txr_ratio)
    private Button ratioBtn;
    private TimerThread readDataThread;
    @ViewInject(R.id.btn_start)
    private Button startBtn;
    @ViewInject(R.id.error_remain_time1_textview)
    private TextView timeValue1Tv;
    @ViewInject(R.id.error_remain_time2_textview)
    private TextView timeValue2Tv;
    @ViewInject(R.id.error_remain_time3_textview)
    private TextView timeValue3Tv;
    @ViewInject(R.id.error_remain_time_textview)
    private TextView timeValueTv;
    private TextView[] timeValueTvs = new TextView[3];
    @ViewInject(R.id.txt_eavg)
    private TextView txt_EAvg;
    @ViewInject(R.id.txt_es)
    private TextView txt_ES;
    @ViewInject(R.id.error_graph)
    private MyErrorGraphView viewError;

    public ErrorTestingFragment(ErrorTest[] Data) {
        this.m_ErrorTests = Data;
        this.m_BasicData = new BasicMeasurement();
        this.m_Sitedata = ClouData.getInstance().getSiteData();
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private void refreshPulse() {
        setPulse(0, this.constantValueEt, this.pulsesValueEt);
        setPulse(0, this.constantValue1Et, this.pulsesValue1Et);
        setPulse(1, this.constantValue2Et, this.pulsesValue2Et);
        setPulse(2, this.constantValue3Et, this.pulsesValue3Et);
    }

    private void setPulse(int index, TextView constantValue, TextView pulseValue) {
        if (!TextUtils.isEmpty(constantValue.getText().toString().trim()) && TextUtils.isEmpty(pulseValue.getText().toString().trim())) {
            this.m_ErrorTests[index].iPulse = getPulses(index);
            pulseValue.setText(new StringBuilder(String.valueOf(this.m_ErrorTests[index].iPulse)).toString());
        }
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private void refreshBasicData() {
        boolean z;
        int i;
        int i2;
        int i3;
        boolean z2;
        boolean z3;
        int i4 = 8;
        if (1 == ClouData.getInstance().getTestMeterCount()) {
            z = true;
        } else {
            z = false;
        }
        this.is1Meter = z;
        View findViewById = findViewById(R.id.layout_1meter);
        if (this.is1Meter) {
            i = 0;
        } else {
            i = 8;
        }
        findViewById.setVisibility(i);
        View findViewById2 = findViewById(R.id.layout_3meter);
        if (!this.is1Meter) {
            i4 = 0;
        }
        findViewById2.setVisibility(i4);
        this.m_ErrorTests[0].iPulseSamplingMethod = 1;
        int tmpValue = ClouData.getInstance().getTestMeterCount();
        ErrorTest errorTest = this.m_ErrorTests[1];
        if ((tmpValue & 2) == 2) {
            i2 = 1;
        } else {
            i2 = 0;
        }
        errorTest.iPulseSamplingMethod = i2;
        ErrorTest errorTest2 = this.m_ErrorTests[2];
        if ((tmpValue & 4) == 4) {
            i3 = 1;
        } else {
            i3 = 0;
        }
        errorTest2.iPulseSamplingMethod = i3;
        this.m_ErrorTests[0].ErrorCount = Preferences.getInt("ErrorCount", 5);
        this.m_ErrorTests[1].ErrorCount = Preferences.getInt("ErrorCount", 5);
        this.m_ErrorTests[2].ErrorCount = Preferences.getInt("ErrorCount", 5);
        this.pulsesValueEt.setText(this.m_ErrorTests[0].Pulses);
        this.pulsesValue1Et.setText(this.m_ErrorTests[0].Pulses);
        this.m_ErrorTests[0].iPulse = OtherUtils.parseInt(this.m_ErrorTests[0].Pulses);
        this.measurementType[0] = this.m_ErrorTests[0].iPulseSamplingMethod;
        this.pulsesValue2Et.setText(this.m_ErrorTests[1].Pulses);
        this.m_ErrorTests[1].iPulse = OtherUtils.parseInt(this.m_ErrorTests[1].Pulses);
        this.measurementType[1] = this.m_ErrorTests[1].iPulseSamplingMethod;
        this.chkMeter2.setChecked(this.measurementType[1] == 1);
        this.pulsesValue3Et.setText(this.m_ErrorTests[2].Pulses);
        this.m_ErrorTests[2].iPulse = OtherUtils.parseInt(this.m_ErrorTests[2].Pulses);
        this.measurementType[2] = this.m_ErrorTests[2].iPulseSamplingMethod;
        CheckBox checkBox = this.chkMeter3;
        if (this.measurementType[2] == 1) {
            z2 = true;
        } else {
            z2 = false;
        }
        checkBox.setChecked(z2);
        this.m_ErrorTestType = Preferences.getInt(Preferences.Key.ERRORTESTMODE, 0);
        this.messageManager.setTestMode(this.m_ErrorTestType);
        this.m_ErrorCount = Preferences.getInt("ErrorCount", 5);
        this.m_IsBaseTime = Preferences.getBoolean(Preferences.Key.ERRORISBASETIME, false);
        if (this.m_IsBaseTime) {
            z3 = false;
        } else {
            z3 = true;
        }
        setPulseTxtEnable(z3);
        this.m_ErrorTime = Preferences.getInt(Preferences.Key.TIMEBASEFORERROR, 5);
        this.m_WaitLimit = Preferences.getInt(Preferences.Key.WAITLIMIT, 3);
        this.autoStop = Preferences.getBoolean(Preferences.Key.ERRORAUTOSTOP, true);
        this.m_ErrorAdapter.refreshData(this.m_ErrorTests[0]);
    }

    private void setPulseTxtEnable(boolean flag) {
        this.pulsesValueEt.setEnabled(flag);
        this.pulsesValue1Et.setEnabled(flag);
        this.pulsesValue2Et.setEnabled(flag);
        this.pulsesValue3Et.setEnabled(flag);
    }

    @Override // android.support.v4.app.Fragment
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        this.fragmentView = inflater.inflate(R.layout.fragment_error_testing, (ViewGroup) null);
        ViewUtils.inject(this, this.fragmentView);
        this.constantValueEts = new EditText[]{this.constantValue1Et, this.constantValue2Et, this.constantValue3Et};
        this.constantBtns = new Button[]{this.constant1Btn, this.constant2Btn, this.constant3Btn};
        this.pulsesValueEts = new EditText[]{this.pulsesValue1Et, this.pulsesValue2Et, this.pulsesValue3Et};
        return this.fragmentView;
    }

    @Override // android.support.v4.app.Fragment, com.clou.rs350.ui.fragment.BaseFragment
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initView();
    }

    private void initView() {
        this.e11Tv = (TextView) findViewById(R.id.error_e11_textview);
        this.e21Tv = (TextView) findViewById(R.id.error_e21_textview);
        this.e31Tv = (TextView) findViewById(R.id.error_e31_textview);
        this.e41Tv = (TextView) findViewById(R.id.error_e41_textview);
        this.e51Tv = (TextView) findViewById(R.id.error_e51_textview);
        this.eA1Tv = (TextView) findViewById(R.id.error_ea1_textview);
        this.es1Tv = (TextView) findViewById(R.id.error_es1_textview);
        this.e12Tv = (TextView) findViewById(R.id.error_e12_textview);
        this.e22Tv = (TextView) findViewById(R.id.error_e22_textview);
        this.e32Tv = (TextView) findViewById(R.id.error_e32_textview);
        this.e42Tv = (TextView) findViewById(R.id.error_e42_textview);
        this.e52Tv = (TextView) findViewById(R.id.error_e52_textview);
        this.eA2Tv = (TextView) findViewById(R.id.error_ea2_textview);
        this.es2Tv = (TextView) findViewById(R.id.error_es2_textview);
        this.e13Tv = (TextView) findViewById(R.id.error_e13_textview);
        this.e23Tv = (TextView) findViewById(R.id.error_e23_textview);
        this.e33Tv = (TextView) findViewById(R.id.error_e33_textview);
        this.e43Tv = (TextView) findViewById(R.id.error_e43_textview);
        this.e53Tv = (TextView) findViewById(R.id.error_e53_textview);
        this.eA3Tv = (TextView) findViewById(R.id.error_ea3_textview);
        this.es3Tv = (TextView) findViewById(R.id.error_es3_textview);
        this.timeValueTvs[0] = this.timeValue1Tv;
        this.timeValueTvs[1] = this.timeValue2Tv;
        this.timeValueTvs[2] = this.timeValue3Tv;
        this.chkMeter2 = (CheckBox) findViewById(R.id.chk_meter2);
        this.chkMeter2.setOnCheckedChangeListener(this);
        this.chkMeter3 = (CheckBox) findViewById(R.id.chk_meter3);
        this.chkMeter3.setOnCheckedChangeListener(this);
        this.m_ErrorAdapter = new ErrorAdapter(this.m_ErrorTests[0], this.m_Context);
        this.lstError.setAdapter((ListAdapter) this.m_ErrorAdapter);
    }

    @Override // android.support.v4.app.Fragment, com.clou.rs350.ui.fragment.BaseFragment
    public void onResume() {
        CountDown cd = new CountDown();
        cd.start("errorfragment onResume");
        super.onResume();
        if (this.messageManager.getMeter().getStartStateValue() == 0) {
            this.messageManager.setFunc_Mstate(33);
        }
        if (1 == ClouData.getInstance().getTestMeterCount()) {
            this.is1Meter = true;
        }
        if (this.hasStart) {
            getDatas();
        }
        updateAllConstantAndUnit();
        updateALLAccuracy();
        setSavedData();
        rememberFunc_Mstate(33);
        cd.stop("errorfragment onResume");
    }

    private void setSavedData() {
        new Thread() {
            /* class com.clou.rs350.ui.fragment.errorfragment.ErrorTestingFragment.AnonymousClass2 */

            public void run() {
                try {
                    Message msg = new Message();
                    msg.what = 0;
                    ErrorTestingFragment.this.handler.sendMessage(msg);
                    sleep(1000);
                    Message msg2 = new Message();
                    msg2.what = 1;
                    ErrorTestingFragment.this.handler.sendMessage(msg2);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }.start();
    }

    private void getDatas() {
        int refreshTime = Preferences.getInt(Preferences.Key.REFRESHTIME, 1000);
        if (this.readDataThread == null || this.readDataThread.isStop()) {
            this.readDataThread = new TimerThread((long) refreshTime, new ISleepCallback() {
                /* class com.clou.rs350.ui.fragment.errorfragment.ErrorTestingFragment.AnonymousClass3 */

                @Override // com.clou.rs350.task.ISleepCallback
                public void onSleepAfterToDo() {
                    if (1 == ErrorTestingFragment.this.isCheckK) {
                        ErrorTestingFragment.this.messageManager.getCheckKFlag();
                    } else {
                        ErrorTestingFragment.this.messageManager.getErrors();
                    }
                }
            }, "Get Data Thread");
            this.readDataThread.start();
        }
    }

    private void setParameters() {
        this.messageManager.StartErrorTest(new int[]{this.m_ErrorCount, this.m_ErrorCount, this.m_ErrorCount}, this.measurementType, new float[]{this.m_ErrorTests[0].iMeterConstant, this.m_ErrorTests[1].iMeterConstant, this.m_ErrorTests[2].iMeterConstant}, new int[]{this.m_ErrorTests[0].iPulse, this.m_ErrorTests[1].iPulse, this.m_ErrorTests[2].iPulse});
    }

    @Override // com.clou.rs350.ui.fragment.BaseFragment, com.clou.rs350.manager.MessageManager.IMessageListener
    public void onReceiveMessage(MeterBaseDevice Device) {
        this.m_BasicData.parseData(Device);
        if (this.hasStart) {
            this.m_ErrorTests[0].parseData(Device, 0);
            this.m_ErrorTests[1].parseData(Device, 1);
            this.m_ErrorTests[2].parseData(Device, 2);
            for (int i = 0; i < this.measurementType.length; i++) {
                if (!(this.measurementType[i] == 0 || this.m_ErrorTests[i].iMeterConstant <= ColumnText.GLOBAL_SPACE_CHAR_RATIO || (this.m_ErrorTests[i].hasErrorCount == this.m_HasErrors[i] && this.m_ErrorTests[i].remainPulses == this.m_HasPulses[i]))) {
                    this.m_HasErrors[i] = this.m_ErrorTests[i].hasErrorCount;
                    this.m_HasPulses[i] = this.m_ErrorTests[i].remainPulses;
                    if (this.m_WaitDialogTasks[i] != null) {
                        if (this.m_HintDialog[i] != null) {
                            this.m_HintDialog[i].dismiss();
                        }
                        this.m_WaitDialogTasks[i].restart();
                    }
                }
            }
        } else if (this.m_IsBaseTime) {
            for (int i2 = 0; i2 < this.measurementType.length; i2++) {
                if (this.measurementType[i2] != 0 && this.m_ErrorTests[i2].iMeterConstant > ColumnText.GLOBAL_SPACE_CHAR_RATIO) {
                    this.pulsesValueEts[i2].setText(new StringBuilder(String.valueOf(getPulses(i2))).toString());
                }
            }
            this.pulsesValueEt.setText(new StringBuilder().append(this.m_ErrorTests[0].iPulse).toString());
        }
        if (1 == this.isCheckK && Device.getCheckKFlagValue() == 0) {
            killThread();
            this.messageManager.getMeterConstant();
            this.isCheckK = 2;
        } else if (5 < this.isCheckK) {
            cancelCheckK();
            if (this.is1Meter) {
                this.m_ErrorTests[0].iMeterConstant = (float) Device.getMeterConstantValues(0).getIntValue();
                refreshConstant();
                this.m_ErrorTests[0].iPulse = getPulses(0);
                this.pulsesValueEt.setText(new StringBuilder().append(this.m_ErrorTests[0].iPulse).toString());
            } else {
                for (int i3 = 0; i3 < 3; i3++) {
                    if (this.measurementType[i3] != 0) {
                        this.m_ErrorTests[i3].iMeterConstant = (float) Device.getMeterConstantValues(i3).getIntValue();
                        refreshConstant(i3);
                        this.m_ErrorTests[i3].iPulse = getPulses(i3);
                        this.pulsesValueEts[i3].setText(new StringBuilder().append(this.m_ErrorTests[i3].iPulse).toString());
                    }
                }
            }
        } else if (1 < this.isCheckK) {
            this.isCheckK++;
        }
        showData(this.m_ErrorTests);
        if (this.messageManager.getMeter().getPQFlagValue() != this.m_PQ_Flag) {
            updateAllConstantAndUnit();
            updateALLAccuracy();
        }
        for (int i4 = 0; i4 < this.measurementType.length; i4++) {
            this.timeValueTvs[i4].setText("---");
        }
        if (this.autoStop) {
            boolean isTestOver = true;
            if (this.is1Meter) {
                this.timeValueTv.setText(getTime(0));
                if (this.m_ErrorTests[0].hasErrorCount < this.m_ErrorCount) {
                    isTestOver = false;
                }
            } else {
                for (int i5 = 0; i5 < this.measurementType.length; i5++) {
                    if (this.measurementType[i5] != 0 && this.m_ErrorTests[i5].iMeterConstant > ColumnText.GLOBAL_SPACE_CHAR_RATIO) {
                        this.timeValueTvs[i5].setText(getTime(i5));
                        if (this.m_ErrorTests[i5].hasErrorCount < this.m_ErrorCount) {
                            isTestOver = false;
                        }
                    }
                }
            }
            if (isTestOver && this.hasStart) {
                stop();
            }
        } else if (this.is1Meter) {
            this.timeValueTv.setText("---");
        } else {
            for (int i6 = 0; i6 < this.measurementType.length; i6++) {
                if (this.measurementType[i6] != 0 && this.m_ErrorTests[i6].iMeterConstant > ColumnText.GLOBAL_SPACE_CHAR_RATIO) {
                    this.timeValueTvs[i6].setText("---");
                }
            }
        }
    }

    @Override // android.support.v4.app.Fragment, com.clou.rs350.ui.fragment.BaseFragment
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override // android.support.v4.app.Fragment, com.clou.rs350.ui.fragment.BaseFragment
    public void onPause() {
        super.onPause();
        killThread();
    }

    /* access modifiers changed from: protected */
    @Override // com.clou.rs350.ui.fragment.BaseFragment
    public void copyData() {
        this.m_PQ_Flag = this.messageManager.getMeter().getPQFlagValue();
        if (this.is1Meter) {
            if (!TextUtils.isEmpty(this.constantValueEt.getText().toString())) {
                this.m_ErrorTests[0].Pulses = this.pulsesValueEt.getText().toString();
                this.m_ErrorTests[0].iPulseSamplingMethod = this.measurementType[0];
            } else {
                this.m_ErrorTests[0].iPulseSamplingMethod = 0;
                this.m_ErrorTests[0].Pulses = PdfObject.NOTHING;
            }
            SetConstant(this.constantValueEt.getText().toString(), this.constantBtn.getText().toString(), 0);
            return;
        }
        if (!TextUtils.isEmpty(this.constantValue1Et.getText().toString())) {
            this.m_ErrorTests[0].Pulses = this.pulsesValue1Et.getText().toString();
            this.m_ErrorTests[0].iPulseSamplingMethod = this.measurementType[0];
        } else {
            this.m_ErrorTests[0].iPulseSamplingMethod = 0;
            this.m_ErrorTests[0].Pulses = PdfObject.NOTHING;
        }
        SetConstant(this.constantValue1Et.getText().toString(), this.constant1Btn.getText().toString(), 0);
        if (!TextUtils.isEmpty(this.constantValue2Et.getText().toString())) {
            this.m_ErrorTests[1].Pulses = this.pulsesValue2Et.getText().toString();
            this.m_ErrorTests[1].iPulseSamplingMethod = this.measurementType[1];
        } else {
            this.m_ErrorTests[1].iPulseSamplingMethod = 0;
            this.m_ErrorTests[1].Pulses = PdfObject.NOTHING;
        }
        SetConstant(this.constantValue2Et.getText().toString(), this.constant2Btn.getText().toString(), 1);
        if (!OtherUtils.isEmpty(this.constantValue3Et)) {
            this.m_ErrorTests[2].Pulses = this.pulsesValue3Et.getText().toString();
            this.m_ErrorTests[2].iPulseSamplingMethod = this.measurementType[2];
        } else {
            this.m_ErrorTests[2].iPulseSamplingMethod = 0;
            this.m_ErrorTests[2].Pulses = PdfObject.NOTHING;
        }
        SetConstant(this.constantValue3Et.getText().toString(), this.constant3Btn.getText().toString(), 2);
    }

    private void SetConstant(String constant, String unit, int meterIndex) {
        switch (this.m_PQ_Flag) {
            case 0:
                if (!TextUtils.isEmpty(ClouData.getInstance().getMeterBaseInfo()[meterIndex].SerialNo) || !TextUtils.isEmpty(constant)) {
                    if (TextUtils.isEmpty(ClouData.getInstance().getMeterBaseInfo()[meterIndex].ActiveConstant)) {
                        ClouData.getInstance().getMeterBaseInfo()[meterIndex].ActiveConstant = String.valueOf(constant) + unit;
                        break;
                    }
                } else {
                    ClouData.getInstance().getMeterBaseInfo()[meterIndex].ActiveConstant = PdfObject.NOTHING;
                    break;
                }
                break;
            case 1:
                if (!TextUtils.isEmpty(ClouData.getInstance().getMeterBaseInfo()[meterIndex].SerialNo) || !TextUtils.isEmpty(constant)) {
                    if (TextUtils.isEmpty(ClouData.getInstance().getMeterBaseInfo()[meterIndex].ReactiveConstant)) {
                        ClouData.getInstance().getMeterBaseInfo()[meterIndex].ReactiveConstant = String.valueOf(constant) + unit;
                        break;
                    }
                } else {
                    ClouData.getInstance().getMeterBaseInfo()[meterIndex].ReactiveConstant = PdfObject.NOTHING;
                    break;
                }
                break;
            case 2:
                if (!TextUtils.isEmpty(ClouData.getInstance().getMeterBaseInfo()[meterIndex].SerialNo) || !TextUtils.isEmpty(constant)) {
                    if (TextUtils.isEmpty(ClouData.getInstance().getMeterBaseInfo()[meterIndex].ApparentConstant)) {
                        ClouData.getInstance().getMeterBaseInfo()[meterIndex].ApparentConstant = String.valueOf(constant) + unit;
                        break;
                    }
                } else {
                    ClouData.getInstance().getMeterBaseInfo()[meterIndex].ApparentConstant = PdfObject.NOTHING;
                    break;
                }
                break;
        }
        this.m_ErrorTests[meterIndex].MeterConstant = constant;
        this.m_ErrorTests[meterIndex].ConstantUnit = unit;
    }

    private void killThread() {
        if (this.readDataThread != null && !this.readDataThread.isStop()) {
            this.readDataThread.stopTimer();
            this.readDataThread.interrupt();
            this.readDataThread = null;
        }
    }

    private void updateAccuracy(int meterIndex) {
        String tmpString = PdfObject.NOTHING;
        switch (this.m_PQ_Flag) {
            case 0:
                tmpString = ClouData.getInstance().getMeterBaseInfo()[meterIndex].ActiveAccuracy.trim();
                break;
            case 1:
                tmpString = ClouData.getInstance().getMeterBaseInfo()[meterIndex].ReactiveAccuracy.trim();
                break;
            case 2:
                tmpString = ClouData.getInstance().getMeterBaseInfo()[meterIndex].ApparentAccuracy.trim();
                break;
        }
        if (!OtherUtils.isEmpty(tmpString)) {
            this.m_ErrorTests[meterIndex].Accuracy = OtherUtils.parseDouble(tmpString);
        }
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private void updateALLAccuracy() {
        updateAccuracy(0);
        updateAccuracy(1);
        updateAccuracy(2);
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private void updateAllConstantAndUnit() {
        this.m_PQ_Flag = this.messageManager.getMeter().getPQFlagValue();
        setConstantsText(0, this.constantValueEt, this.constantBtn);
        updateConstantUnit(this.constantBtn, false);
        setConstantsText(0, this.constantValue1Et, this.constant1Btn);
        setConstantsText(1, this.constantValue2Et, this.constant2Btn);
        setConstantsText(2, this.constantValue3Et, this.constant3Btn);
        updateConstantUnit(this.constant1Btn, false);
        updateConstantUnit(this.constant2Btn, false);
        updateConstantUnit(this.constant3Btn, false);
    }

    private void setConstantsText(int meterIndex, EditText constantValue, Button constantBtn2) {
        String tmpString = PdfObject.NOTHING;
        String value = PdfObject.NOTHING;
        String unit = PdfObject.NOTHING;
        switch (this.m_PQ_Flag) {
            case 0:
                tmpString = ClouData.getInstance().getMeterBaseInfo()[meterIndex].ActiveConstant.trim();
                break;
            case 1:
                tmpString = ClouData.getInstance().getMeterBaseInfo()[meterIndex].ReactiveConstant.trim();
                break;
            case 2:
                tmpString = ClouData.getInstance().getMeterBaseInfo()[meterIndex].ApparentConstant.trim();
                break;
        }
        if (!TextUtils.isEmpty(tmpString)) {
            int index = OtherUtils.getnumberCount(tmpString);
            value = tmpString.substring(0, index);
            unit = tmpString.substring(index);
        }
        if (!TextUtils.isEmpty(this.m_ErrorTests[meterIndex].MeterConstant)) {
            value = this.m_ErrorTests[meterIndex].MeterConstant;
            unit = this.m_ErrorTests[meterIndex].ConstantUnit;
        }
        if (!TextUtils.isEmpty(value)) {
            constantValue.setText(value);
            constantBtn2.setText(unit);
            this.m_ErrorTests[meterIndex].iMeterConstant = getConstantsImp(value, unit);
        }
    }

    @Override // com.clou.rs350.ui.fragment.errorfragment.BaseErrorFragment
    public boolean start() {
        if (!hasSetParams()) {
            showHint(R.string.text_have_not_set_param);
            return false;
        }
        this.messageManager.setFunc_Mstate_Start(33, 1);
        setParameters();
        getDatas();
        setStartButton(1);
        setWaitDialog();
        this.hasStart = true;
        return true;
    }

    private void setWaitDialog() {
        new SleepTask(2000, new ISleepCallback() {
            /* class com.clou.rs350.ui.fragment.errorfragment.ErrorTestingFragment.AnonymousClass4 */

            @Override // com.clou.rs350.task.ISleepCallback
            public void onSleepAfterToDo() {
                if (ErrorTestingFragment.this.hasStart) {
                    long[] waittime = new long[3];
                    for (int i = 0; i < 3; i++) {
                        if (ErrorTestingFragment.this.measurementType[i] != 0 && ErrorTestingFragment.this.m_ErrorTests[i].iMeterConstant > ColumnText.GLOBAL_SPACE_CHAR_RATIO) {
                            waittime[i] = (long) ErrorTestingFragment.this.calculatetimeintvalue((double) ErrorTestingFragment.this.m_ErrorTests[i].iMeterConstant, (double) ErrorTestingFragment.this.m_ErrorTests[i].iPulse, ErrorTestingFragment.this.getPower(), 0, ErrorTestingFragment.this.m_ErrorTests[i].iPulse);
                            waittime[i] = ((waittime[i] / ((long) ErrorTestingFragment.this.m_ErrorCount)) / ((long) (ErrorTestingFragment.this.m_ErrorTestType == 0 ? ErrorTestingFragment.this.m_ErrorTests[i].iPulse : 1))) * 1000;
                            if (((double) waittime[i]) < ((double) Preferences.getInt(Preferences.Key.REFRESHTIME, 1000)) * 1.5d) {
                                waittime[i] = (long) (((double) Preferences.getInt(Preferences.Key.REFRESHTIME, 1000)) * 1.5d);
                            }
                            waittime[i] = waittime[i] * ((long) ErrorTestingFragment.this.m_WaitLimit);
                            ErrorTestingFragment.this.m_HasErrors[i] = 0;
                            ErrorTestingFragment.this.m_HasPulses[i] = ErrorTestingFragment.this.m_ErrorTests[i].iPulse;
                            ErrorTestingFragment.this.setWaitDialog(i, waittime[i]);
                        }
                    }
                }
            }
        }).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, new Integer[0]);
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private void setWaitDialog(final int index, final long waittime) {
        if (this.m_WaitDialogTasks[index] != null) {
            this.m_WaitDialogTasks[index].stop();
        }
        this.m_WaitDialogTasks[index] = new SleepTask(waittime, new ISleepCallback() {
            /* class com.clou.rs350.ui.fragment.errorfragment.ErrorTestingFragment.AnonymousClass5 */

            @Override // com.clou.rs350.task.ISleepCallback
            public void onSleepAfterToDo() {
                if (ErrorTestingFragment.this.hasStart) {
                    ErrorTestingFragment.this.m_HintDialog[index] = new HintDialog(ErrorTestingFragment.this.m_Context);
                    ErrorTestingFragment.this.m_HintDialog[index].show();
                    ErrorTestingFragment.this.m_HintDialog[index].setTitle(String.format(ErrorTestingFragment.this.getResources().getString(R.string.text_no_error), Integer.valueOf(index + 1)));
                    HintDialog hintDialog = ErrorTestingFragment.this.m_HintDialog[index];
                    final long j = waittime;
                    final int i = index;
                    hintDialog.setOnDismiss(new DialogInterface.OnDismissListener() {
                        /* class com.clou.rs350.ui.fragment.errorfragment.ErrorTestingFragment.AnonymousClass5.AnonymousClass1 */

                        public void onDismiss(DialogInterface dialog) {
                            if (j < 3000) {
                                ErrorTestingFragment.this.setWaitDialog(i, j + 3000);
                            } else {
                                ErrorTestingFragment.this.setWaitDialog(i, j);
                            }
                        }
                    });
                }
            }
        });
        this.m_WaitDialogTasks[index].executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, new Integer[0]);
    }

    private void cleanWaitDialog() {
        for (int i = 0; i < 3; i++) {
            if (this.m_WaitDialogTasks[i] != null) {
                this.m_WaitDialogTasks[i].stop();
            }
        }
    }

    @Override // com.clou.rs350.ui.fragment.errorfragment.BaseErrorFragment
    public void doContinue() {
        getDatas();
        showCoverView(0);
        this.hasStart = true;
    }

    @Override // com.clou.rs350.ui.fragment.errorfragment.BaseErrorFragment
    public boolean stop() {
        this.messageManager.setStart(0);
        this.messageManager.SetParameters(new int[3]);
        killThread();
        cleanWaitDialog();
        setStartButton(0);
        this.hasStart = false;
        copyData();
        return true;
    }

    private void setStartButton(int flag) {
        boolean enabled = true;
        this.startBtn.setText(getString(flag == 0 ? R.string.text_start : R.string.text_stop));
        showCoverView(1 == flag ? 0 : 4);
        if (flag != 0) {
            enabled = false;
        }
        this.ratioBtn.setEnabled(enabled);
        this.configureBtn.setEnabled(enabled);
        this.checkBtn.setEnabled(enabled);
        this.constantBtn.setEnabled(enabled);
        this.pulsesValueEt.setEnabled(enabled);
        this.constantValueEt.setEnabled(enabled);
        if (this.m_Context instanceof BaseActivity) {
            ((BaseActivity) this.m_Context).startOrStopChangeViewStatus(flag);
        }
    }

    private void setCheckKButton(int flag) {
        boolean enabled = true;
        this.checkBtn.setText(getString(flag == 0 ? R.string.text_checkk : R.string.text_cancel));
        showCoverView(1 == flag ? 0 : 4);
        if (flag != 0) {
            enabled = false;
        }
        this.ratioBtn.setEnabled(enabled);
        this.configureBtn.setEnabled(enabled);
        this.startBtn.setEnabled(enabled);
        if (this.m_Context instanceof BaseActivity) {
            ((BaseActivity) this.m_Context).startOrStopChangeViewStatus(flag);
        }
    }

    private void showCoverView(int visible) {
        this.cover1View.setVisibility(visible);
        this.cover2View.setVisibility(visible);
        this.cover3View.setVisibility(visible);
        boolean enabled = visible != 0;
        this.constant1Btn.setEnabled(enabled);
        this.constant2Btn.setEnabled(enabled);
        this.constant3Btn.setEnabled(enabled);
        this.constantValue1Et.setEnabled(enabled);
        this.constantValue2Et.setEnabled(enabled);
        this.constantValue3Et.setEnabled(enabled);
        this.chkMeter2.setEnabled(enabled);
        this.chkMeter3.setEnabled(enabled);
        if (!this.m_IsBaseTime) {
            this.pulsesValueEt.setEnabled(enabled);
            this.pulsesValue1Et.setEnabled(enabled);
            this.pulsesValue2Et.setEnabled(enabled);
            this.pulsesValue3Et.setEnabled(enabled);
        }
    }

    private void showData(ErrorTest[] errors) {
        if (this.is1Meter) {
            this.errorPBar.setMax(this.m_ErrorTests[0].iPulse);
            if (this.measurementType[0] != 0) {
                this.errorPBar.setProgress(this.m_ErrorTests[0].iPulse - errors[0].getPulsesProgress());
                this.m_ErrorAdapter.refreshData(errors[0]);
                this.viewError.refreshData(errors[0]);
            } else {
                this.errorPBar.setProgress(this.m_ErrorTests[0].iPulse);
            }
            this.txt_EAvg.setText(errors[0].getErrorAverage(this.m_ErrorCount));
            this.txt_EAvg.setTextColor(OtherUtils.getColor(errors[0].getPass(this.m_ErrorCount)));
            this.txt_ES.setText(errors[0].getErrorStandard(this.m_ErrorCount));
            return;
        }
        this.e11Tv.setText(errors[0].getError(0));
        this.e21Tv.setText(errors[0].getError(1));
        this.e31Tv.setText(errors[0].getError(2));
        this.e41Tv.setText(errors[0].getError(3));
        this.e51Tv.setText(errors[0].getError(4));
        this.eA1Tv.setText(errors[0].getErrorAverage(this.m_ErrorCount));
        this.es1Tv.setText(errors[0].getErrorStandard(this.m_ErrorCount));
        this.e12Tv.setText(errors[1].getError(0));
        this.e22Tv.setText(errors[1].getError(1));
        this.e32Tv.setText(errors[1].getError(2));
        this.e42Tv.setText(errors[1].getError(3));
        this.e52Tv.setText(errors[1].getError(4));
        this.eA2Tv.setText(errors[1].getErrorAverage(this.m_ErrorCount));
        this.es2Tv.setText(errors[1].getErrorStandard(this.m_ErrorCount));
        this.e13Tv.setText(errors[2].getError(0));
        this.e23Tv.setText(errors[2].getError(1));
        this.e33Tv.setText(errors[2].getError(2));
        this.e43Tv.setText(errors[2].getError(3));
        this.e53Tv.setText(errors[2].getError(4));
        this.eA3Tv.setText(errors[2].getErrorAverage(this.m_ErrorCount));
        this.es3Tv.setText(errors[2].getErrorStandard(this.m_ErrorCount));
        this.error1PBar.setMax(this.m_ErrorTests[0].iPulse);
        if (this.measurementType[0] != 0) {
            this.error1PBar.setProgress(this.m_ErrorTests[0].iPulse - errors[0].getPulsesProgress());
        } else {
            this.error1PBar.setProgress(this.m_ErrorTests[0].iPulse);
        }
        this.error2PBar.setMax(this.m_ErrorTests[1].iPulse);
        if (this.measurementType[1] != 0) {
            this.error2PBar.setProgress(this.m_ErrorTests[1].iPulse - errors[1].getPulsesProgress());
        } else {
            this.error2PBar.setProgress(this.m_ErrorTests[1].iPulse);
        }
        this.error3PBar.setMax(this.m_ErrorTests[2].iPulse);
        if (this.measurementType[2] != 0) {
            this.error3PBar.setProgress(this.m_ErrorTests[2].iPulse - errors[2].getPulsesProgress());
        } else {
            this.error3PBar.setProgress(this.m_ErrorTests[2].iPulse);
        }
        this.eA1Tv.setTextColor(OtherUtils.getColor(errors[0].getPass(this.m_ErrorCount)));
        this.eA2Tv.setTextColor(OtherUtils.getColor(errors[1].getPass(this.m_ErrorCount)));
        this.eA3Tv.setTextColor(OtherUtils.getColor(errors[2].getPass(this.m_ErrorCount)));
    }

    @OnClick({R.id.error_meter_constant_button, R.id.error_meter_constant1_button, R.id.error_meter_constant2_button, R.id.error_meter_constant3_button, R.id.error_meter_constant_value_edittext, R.id.error_meter_constant1_value_edittext, R.id.error_meter_constant2_value_edittext, R.id.error_meter_constant3_value_edittext, R.id.error_pulses_edittext, R.id.error_pulses1_edittext, R.id.error_pulses2_edittext, R.id.error_pulses3_edittext, R.id.btn_txr_ratio, R.id.btn_configure, R.id.btn_start, R.id.btn_check})
    public void onClick(final View v) {
        switch (v.getId()) {
            case R.id.btn_start:
                if (!this.hasStart) {
                    start();
                    return;
                } else {
                    stop();
                    return;
                }
            case R.id.error_meter_constant_value_edittext:
                showInputDialog((TextView) ((EditText) v), (View.OnClickListener) new View.OnClickListener() {
                    /* class com.clou.rs350.ui.fragment.errorfragment.ErrorTestingFragment.AnonymousClass7 */

                    public void onClick(View view) {
                        String unit = ErrorTestingFragment.this.constantBtn.getText().toString().trim();
                        String value = ((EditText) v).getText().toString().trim();
                        ErrorTestingFragment.this.m_ErrorTests[0].iMeterConstant = ErrorTestingFragment.this.getConstantsImp(value, unit);
                        ErrorTestingFragment.this.m_ErrorTests[0].iPulse = ErrorTestingFragment.this.getPulses(0);
                        ErrorTestingFragment.this.pulsesValueEt.setText(new StringBuilder().append(ErrorTestingFragment.this.m_ErrorTests[0].iPulse).toString());
                    }
                }, false);
                return;
            case R.id.error_pulses_edittext:
                showInputDialog((TextView) v, (View.OnClickListener) new View.OnClickListener() {
                    /* class com.clou.rs350.ui.fragment.errorfragment.ErrorTestingFragment.AnonymousClass11 */

                    public void onClick(View view) {
                        ErrorTestingFragment.this.m_ErrorTests[0].iPulse = ErrorTestingFragment.this.getValue((TextView) v);
                        ((TextView) v).setText(new StringBuilder(String.valueOf(ErrorTestingFragment.this.m_ErrorTests[0].iPulse)).toString());
                    }
                }, false);
                return;
            case R.id.error_meter_constant1_value_edittext:
                showInputDialog((TextView) ((EditText) v), (View.OnClickListener) new View.OnClickListener() {
                    /* class com.clou.rs350.ui.fragment.errorfragment.ErrorTestingFragment.AnonymousClass8 */

                    public void onClick(View view) {
                        String unit = ErrorTestingFragment.this.constant1Btn.getText().toString().trim();
                        String value = ((EditText) v).getText().toString().trim();
                        ErrorTestingFragment.this.m_ErrorTests[0].iMeterConstant = ErrorTestingFragment.this.getConstantsImp(value, unit);
                        ErrorTestingFragment.this.m_ErrorTests[0].iPulse = ErrorTestingFragment.this.getPulses(0);
                        ErrorTestingFragment.this.pulsesValue1Et.setText(new StringBuilder().append(ErrorTestingFragment.this.m_ErrorTests[0].iPulse).toString());
                    }
                }, false);
                return;
            case R.id.error_pulses1_edittext:
                showInputDialog((TextView) ((EditText) v), (View.OnClickListener) new View.OnClickListener() {
                    /* class com.clou.rs350.ui.fragment.errorfragment.ErrorTestingFragment.AnonymousClass12 */

                    public void onClick(View view) {
                        ErrorTestingFragment.this.m_ErrorTests[0].iPulse = ErrorTestingFragment.this.getValue((EditText) v);
                        ((EditText) v).setText(new StringBuilder(String.valueOf(ErrorTestingFragment.this.m_ErrorTests[0].iPulse)).toString());
                    }
                }, false);
                return;
            case R.id.error_meter_constant2_value_edittext:
                showInputDialog((TextView) ((EditText) v), (View.OnClickListener) new View.OnClickListener() {
                    /* class com.clou.rs350.ui.fragment.errorfragment.ErrorTestingFragment.AnonymousClass9 */

                    public void onClick(View view) {
                        String unit = ErrorTestingFragment.this.constant2Btn.getText().toString().trim();
                        String value = ((EditText) v).getText().toString().trim();
                        ErrorTestingFragment.this.m_ErrorTests[1].iMeterConstant = ErrorTestingFragment.this.getConstantsImp(value, unit);
                        ErrorTestingFragment.this.m_ErrorTests[1].iPulse = ErrorTestingFragment.this.getPulses(1);
                        ErrorTestingFragment.this.pulsesValue2Et.setText(new StringBuilder().append(ErrorTestingFragment.this.m_ErrorTests[1].iPulse).toString());
                    }
                }, false);
                return;
            case R.id.error_pulses2_edittext:
                showInputDialog((TextView) ((EditText) v), (View.OnClickListener) new View.OnClickListener() {
                    /* class com.clou.rs350.ui.fragment.errorfragment.ErrorTestingFragment.AnonymousClass13 */

                    public void onClick(View view) {
                        ErrorTestingFragment.this.m_ErrorTests[1].iPulse = ErrorTestingFragment.this.getValue((EditText) v);
                        ((EditText) v).setText(new StringBuilder(String.valueOf(ErrorTestingFragment.this.m_ErrorTests[1].iPulse)).toString());
                    }
                }, false);
                return;
            case R.id.error_meter_constant3_value_edittext:
                showInputDialog((TextView) ((EditText) v), (View.OnClickListener) new View.OnClickListener() {
                    /* class com.clou.rs350.ui.fragment.errorfragment.ErrorTestingFragment.AnonymousClass10 */

                    public void onClick(View view) {
                        String unit = ErrorTestingFragment.this.constant3Btn.getText().toString().trim();
                        String value = ((EditText) v).getText().toString().trim();
                        ErrorTestingFragment.this.m_ErrorTests[2].iMeterConstant = ErrorTestingFragment.this.getConstantsImp(value, unit);
                        ErrorTestingFragment.this.m_ErrorTests[2].iPulse = ErrorTestingFragment.this.getPulses(2);
                        ErrorTestingFragment.this.pulsesValue3Et.setText(new StringBuilder().append(ErrorTestingFragment.this.m_ErrorTests[2].iPulse).toString());
                    }
                }, false);
                return;
            case R.id.error_pulses3_edittext:
                showInputDialog((TextView) ((EditText) v), (View.OnClickListener) new View.OnClickListener() {
                    /* class com.clou.rs350.ui.fragment.errorfragment.ErrorTestingFragment.AnonymousClass14 */

                    public void onClick(View view) {
                        ErrorTestingFragment.this.m_ErrorTests[2].iPulse = ErrorTestingFragment.this.getValue((EditText) v);
                        ((EditText) v).setText(new StringBuilder(String.valueOf(ErrorTestingFragment.this.m_ErrorTests[2].iPulse)).toString());
                    }
                }, false);
                return;
            case R.id.btn_configure:
                copyData();
                new ErrorConfigurePopWindow(this.m_Context).showPopWindow(v, this.m_PQ_Flag, new View.OnClickListener() {
                    /* class com.clou.rs350.ui.fragment.errorfragment.ErrorTestingFragment.AnonymousClass6 */

                    public void onClick(View v) {
                        ErrorTestingFragment.this.updateAllConstantAndUnit();
                        ErrorTestingFragment.this.refreshBasicData();
                        ErrorTestingFragment.this.refreshPulse();
                        ErrorTestingFragment.this.updateALLAccuracy();
                    }
                });
                return;
            case R.id.btn_txr_ratio:
                new TrxRatioPopWindow(this.m_Context).showRatioPopWindow(v, ClouData.getInstance().getSiteData(), null);
                return;
            case R.id.error_meter_constant_button:
                updateConstantUnit((Button) v, true);
                refreshConstant();
                return;
            case R.id.error_meter_constant1_button:
                updateConstantUnit((Button) v, true);
                refreshConstant(0);
                return;
            case R.id.error_meter_constant2_button:
                updateConstantUnit((Button) v, true);
                refreshConstant(1);
                return;
            case R.id.error_meter_constant3_button:
                updateConstantUnit((Button) v, true);
                refreshConstant(2);
                return;
            case R.id.btn_check:
                if (this.isCheckK == 0) {
                    startCheckK();
                    getDatas();
                    return;
                }
                cancelCheckK();
                killThread();
                return;
            default:
                return;
        }
    }

    private void startCheckK() {
        this.m_ErrorTests[0].iMeterConstant = ColumnText.GLOBAL_SPACE_CHAR_RATIO;
        this.m_ErrorTests[1].iMeterConstant = ColumnText.GLOBAL_SPACE_CHAR_RATIO;
        this.m_ErrorTests[2].iMeterConstant = ColumnText.GLOBAL_SPACE_CHAR_RATIO;
        this.constantValueEt.setText(PdfObject.NOTHING);
        this.constantValue1Et.setText(PdfObject.NOTHING);
        this.constantValue2Et.setText(PdfObject.NOTHING);
        this.constantValue3Et.setText(PdfObject.NOTHING);
        for (int i = 0; i < 3; i++) {
            if (this.measurementType[i] != 0 && this.m_ErrorTests[i].iPulse == 0) {
                this.m_ErrorTests[i].iPulse = 3;
                this.pulsesValueEts[i].setText(new StringBuilder(String.valueOf(this.m_ErrorTests[i].iPulse)).toString());
            }
        }
        this.pulsesValueEt.setText(new StringBuilder(String.valueOf(this.m_ErrorTests[0].iPulse)).toString());
        this.messageManager.SetParameters(this.measurementType, new float[]{ColumnText.GLOBAL_SPACE_CHAR_RATIO, ColumnText.GLOBAL_SPACE_CHAR_RATIO, ColumnText.GLOBAL_SPACE_CHAR_RATIO}, new int[]{this.m_ErrorTests[0].iPulse, this.m_ErrorTests[1].iPulse, this.m_ErrorTests[2].iPulse});
        new SleepTask(1000, new ISleepCallback() {
            /* class com.clou.rs350.ui.fragment.errorfragment.ErrorTestingFragment.AnonymousClass15 */

            @Override // com.clou.rs350.task.ISleepCallback
            public void onSleepAfterToDo() {
                ErrorTestingFragment.this.messageManager.setCheckKFlag(1);
                ErrorTestingFragment.this.isCheckK = 1;
            }
        }).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, new Integer[0]);
        setCheckKButton(1);
    }

    private void cancelCheckK() {
        this.isCheckK = 0;
        this.messageManager.setCheckKFlag(0);
        setCheckKButton(0);
    }

    private void refreshConstant() {
        if (isStartWithImp(this.constantBtn)) {
            this.constantValueEt.setText(new StringBuilder(String.valueOf(this.m_ErrorTests[0].iMeterConstant)).toString());
            return;
        }
        double value = calculateConstant((double) this.m_ErrorTests[0].iMeterConstant);
        DecimalFormat df = new DecimalFormat("#.####");
        DecimalFormatSymbols symbols = new DecimalFormatSymbols();
        symbols.setDecimalSeparator('.');
        df.setDecimalFormatSymbols(symbols);
        this.constantValueEt.setText(df.format(value));
    }

    private void refreshConstant(int index) {
        if (isStartWithImp(this.constantBtns[index])) {
            this.constantValueEts[index].setText(new StringBuilder(String.valueOf(this.m_ErrorTests[index].iMeterConstant)).toString());
            return;
        }
        double value = calculateConstant((double) this.m_ErrorTests[index].iMeterConstant);
        DecimalFormat df = new DecimalFormat("#.####");
        DecimalFormatSymbols symbols = new DecimalFormatSymbols();
        symbols.setDecimalSeparator('.');
        df.setDecimalFormatSymbols(symbols);
        this.constantValueEts[index].setText(df.format(value));
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private float getConstantsImp(String value, String unit) {
        double data = OtherUtils.parseDouble(value);
        if (isStartWithImp(unit)) {
            return (float) data;
        }
        return (float) OtherUtils.convertToInt(Double.valueOf(calculateConstant(data)));
    }

    private boolean isStartWithImp(String text) {
        if (TextUtils.isEmpty(text)) {
            return false;
        }
        return text.startsWith("imp");
    }

    private boolean isStartWithImp(Button constantBtn2) {
        String text = constantBtn2.getText().toString().trim();
        if (TextUtils.isEmpty(text)) {
            return false;
        }
        return text.startsWith("imp");
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private int getValue(TextView tv) {
        String text = tv.getText().toString().trim();
        if (TextUtils.isEmpty(text)) {
            return 0;
        }
        return OtherUtils.convertToInt(Double.valueOf(OtherUtils.parseDouble(text)));
    }

    public double calculateRatio(SiteData Info) {
        double PTRatio = 1.0d;
        double CTRatio = 1.0d;
        if (Info.PTEnable == 0 && !OtherUtils.isEmpty(Info.PTPrimary.s_Value) && !OtherUtils.isEmpty(Info.PTSecondary.s_Value)) {
            PTRatio = (Info.PTPrimary.d_Value * 1000.0d) / Info.PTSecondary.d_Value;
        }
        if (Info.CTEnable == 0 && !OtherUtils.isEmpty(Info.CTPrimary.s_Value) && !OtherUtils.isEmpty(Info.CTSecondary.s_Value)) {
            CTRatio = Info.CTPrimary.d_Value / Info.CTSecondary.d_Value;
        }
        if (1 == Info.Measurement_Position) {
            PTRatio = 1.0d / PTRatio;
            CTRatio = 1.0d / CTRatio;
        }
        return PTRatio * CTRatio;
    }

    private void updateConstantUnit(Button meterConstantBtn, boolean change) {
        String text;
        String text2 = meterConstantBtn.getText().toString().trim();
        String str = PdfObject.NOTHING;
        switch (this.messageManager.getMeter().getPQFlagValue()) {
            case 0:
                str = "W";
                break;
            case 1:
                str = "var";
                break;
            case 2:
                str = "VA";
                break;
        }
        if (change) {
            text = text2.startsWith("imp") ? String.valueOf(str) + "h/imp" : "imp/k" + str + "h";
        } else {
            text = text2.startsWith("imp") ? "imp/k" + str + "h" : String.valueOf(str) + "h/imp";
        }
        meterConstantBtn.setText(text);
    }

    private double calculateConstant(double constant) {
        if (0.0d == constant) {
            return 0.0d;
        }
        return (1.0d / Double.valueOf(constant).doubleValue()) * 1000.0d;
    }

    private int calculatePulse(double constant, double time, double power) {
        if (0.0d == power) {
            return 0;
        }
        if (1 == (this.m_Sitedata.RatioApply & 3)) {
            power /= calculateRatio(this.m_Sitedata);
        } else if (2 == (this.m_Sitedata.RatioApply & 3)) {
            power *= calculateRatio(this.m_Sitedata);
        }
        int pulse = (int) Math.round(Math.abs(((time * constant) * power) / 3600000.0d));
        if (pulse < 1) {
            return 1;
        }
        return pulse;
    }

    private String calculatetime(double constant, double pulse, double power, int hasErrorCount, int remainPulse) {
        if (-1 == remainPulse) {
            return "---";
        }
        if (1 == (this.m_Sitedata.RatioApply & 3)) {
            power /= calculateRatio(this.m_Sitedata);
        } else if (2 == (this.m_Sitedata.RatioApply & 3)) {
            power *= calculateRatio(this.m_Sitedata);
        }
        int time = calculatetimeintvalue(constant, pulse, power, hasErrorCount, remainPulse);
        if (String.valueOf(time).equals("Infinity")) {
            return "0s";
        }
        String TimeString = String.valueOf(time) + HtmlTags.S;
        if (time < 0) {
            return getResources().getString(R.string.text_finish);
        }
        return TimeString;
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private int calculatetimeintvalue(double constant, double pulse, double power, int hasErrorCount, int remainPulse) {
        return (int) Math.round((((((double) ((this.m_ErrorCount - 1) - hasErrorCount)) * pulse) + ((double) remainPulse)) * 3600000.0d) / (Math.abs(power) * constant));
    }

    private String getTime(int index) {
        double power = getPower();
        if (power < 1.0d) {
            return getResources().getString(R.string.text_power_is_low);
        }
        if (this.hasStart) {
            return calculatetime((double) this.m_ErrorTests[index].iMeterConstant, (double) this.m_ErrorTests[index].iPulse, power, this.m_ErrorTests[index].hasErrorCount, this.m_ErrorTests[index].remainPulses);
        }
        return calculatetime((double) this.m_ErrorTests[index].iMeterConstant, (double) this.m_ErrorTests[index].iPulse, power, 0, this.m_ErrorTests[index].iPulse);
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private double getPower() {
        switch (this.m_BasicData.PQFlag) {
            case 0:
                return this.m_BasicData.ActivePowerTotal.d_Value;
            case 1:
                return this.m_BasicData.ReactivePowerTotal.d_Value;
            case 2:
                return this.m_BasicData.ApparentPowerTotal.d_Value;
            default:
                return 0.0d;
        }
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private int getPulses(int index) {
        double power = 0.0d;
        switch (this.m_BasicData.PQFlag) {
            case 0:
                power = this.m_BasicData.ActivePowerTotal.d_Value;
                break;
            case 1:
                power = this.m_BasicData.ReactivePowerTotal.d_Value;
                break;
            case 2:
                power = this.m_BasicData.ApparentPowerTotal.d_Value;
                break;
        }
        int pulse = calculatePulse((double) this.m_ErrorTests[index].iMeterConstant, (double) this.m_ErrorTime, power);
        this.m_ErrorTests[index].iPulse = pulse;
        return pulse;
    }

    @Override // com.clou.rs350.ui.fragment.errorfragment.BaseErrorFragment
    public boolean hasSetParams() {
        boolean meter1IsOK = true;
        boolean meter2IsOK = true;
        boolean meter3IsOK = true;
        if (this.is1Meter) {
            double meterConstant = OtherUtils.parseDouble(OtherUtils.getText(this.constantValueEt));
            long pulsesValue = OtherUtils.parseLong(OtherUtils.getText(this.pulsesValueEt));
            if (meterConstant <= 0.0d || pulsesValue <= 0) {
                meter1IsOK = false;
            } else {
                meter1IsOK = true;
            }
        } else {
            if (this.measurementType[0] != 0) {
                meter1IsOK = OtherUtils.parseDouble(OtherUtils.getText(this.constantValue1Et)) > 0.0d && OtherUtils.parseLong(OtherUtils.getText(this.pulsesValue1Et)) > 0;
            }
            if (this.measurementType[1] != 0) {
                meter2IsOK = OtherUtils.parseDouble(OtherUtils.getText(this.constantValue2Et)) > 0.0d && OtherUtils.parseLong(OtherUtils.getText(this.pulsesValue2Et)) > 0;
            }
            if (this.measurementType[2] != 0) {
                meter3IsOK = OtherUtils.parseDouble(OtherUtils.getText(this.constantValue3Et)) > 0.0d && OtherUtils.parseLong(OtherUtils.getText(this.pulsesValue3Et)) > 0;
            }
        }
        return meter1IsOK & meter2IsOK & meter3IsOK;
    }

    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        switch (buttonView.getId()) {
            case R.id.chk_meter2:
                if (isChecked) {
                    this.measurementType[1] = 1;
                    ClouData.getInstance().setTestMeterCount(ClouData.getInstance().getTestMeterCount() | 2);
                    return;
                }
                this.measurementType[1] = 0;
                ClouData.getInstance().setTestMeterCount(ClouData.getInstance().getTestMeterCount() & -3);
                return;
            case R.id.chk_meter3:
                if (isChecked) {
                    this.measurementType[2] = 1;
                    ClouData.getInstance().setTestMeterCount(ClouData.getInstance().getTestMeterCount() | 4);
                    return;
                }
                this.measurementType[2] = 0;
                ClouData.getInstance().setTestMeterCount(ClouData.getInstance().getTestMeterCount() & -5);
                return;
            default:
                return;
        }
    }

    @Override // com.clou.rs350.ui.fragment.BaseFragment
    public int isFinish() {
        int result = 0;
        for (int i = 0; i < 3; i++) {
            if (this.measurementType[i] != 0 && this.m_ErrorTests[i].isEmpty()) {
                result = 1;
                new HintDialog(this.m_Context, (int) R.string.text_do_test).show();
            }
        }
        return result;
    }
}
