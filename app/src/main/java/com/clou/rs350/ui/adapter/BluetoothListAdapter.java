package com.clou.rs350.ui.adapter;

import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.clou.rs350.R;

import java.util.ArrayList;
import java.util.List;

public class BluetoothListAdapter extends BaseAdapter {
    private Context mContext;
    private List<BluetoothDevice> mDataList;

    public BluetoothListAdapter(List<BluetoothDevice> list, Context context) {
        this.mDataList = list;
        this.mContext = context;
    }

    public int getCount() {
        if (this.mDataList != null) {
            return this.mDataList.size();
        }
        return 0;
    }

    public Object getItem(int position) {
        try {
            if (this.mDataList != null) {
                return this.mDataList.get(position);
            }
            return null;
        } catch (Exception e) {
            return null;
        }
    }

    public long getItemId(int position) {
        return 0;
    }

    public View getView(int position, View view, ViewGroup parent) {
        ViewHolder holder;
        if (view == null) {
            view = LayoutInflater.from(this.mContext).inflate(R.layout.adapter_itemlist, (ViewGroup) null);
            holder = new ViewHolder(view);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }
        holder.show((BluetoothDevice) getItem(position));
        return view;
    }

    public void refreshData(List<BluetoothDevice> list) {
        if (this.mDataList == null) {
            this.mDataList = new ArrayList();
        }
        this.mDataList.clear();
        this.mDataList.addAll(list);
        notifyDataSetChanged();
    }

    public void addBluetoothDevice(BluetoothDevice newDevice) {
        if (this.mDataList == null) {
            this.mDataList = new ArrayList();
        }
        this.mDataList.add(newDevice);
        notifyDataSetChanged();
    }

    class ViewHolder {
        TextView macTextView;
        TextView nameTextView;

        ViewHolder(View view) {
            this.nameTextView = (TextView) view.findViewById(R.id.key_textview);
            this.macTextView = (TextView) view.findViewById(R.id.name_textview);
        }

        /* access modifiers changed from: package-private */
        public void show(BluetoothDevice device) {
            if (device != null) {
                this.nameTextView.setText(device.getName());
                this.macTextView.setText(device.getAddress());
            }
        }
    }
}
