package com.clou.rs350.ui.fragment.recordfragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.clou.rs350.R;
import com.clou.rs350.db.model.ReadMeter;
import com.clou.rs350.device.MeterBaseDevice;
import com.clou.rs350.ui.fragment.BaseFragment;
import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.view.annotation.ViewInject;

public class ReadMeterRecordFragment extends BaseFragment {
    ReadMeter m_Data;
    @ViewInject(R.id.txt_address)
    TextView txt_Address;
    TextView[] txt_Energys;
    @ViewInject(R.id.txt_flat_export_active)
    TextView txt_Flat_Export_Active_Energy;
    @ViewInject(R.id.txt_flat_export_reactive)
    TextView txt_Flat_Export_Reactive_Energy;
    @ViewInject(R.id.txt_flat_import_active)
    TextView txt_Flat_Import_Active_Energy;
    @ViewInject(R.id.txt_flat_import_reactive)
    TextView txt_Flat_Import_Reactive_Energy;
    @ViewInject(R.id.txt_il1)
    TextView txt_IL1;
    @ViewInject(R.id.txt_il2)
    TextView txt_IL2;
    @ViewInject(R.id.txt_il3)
    TextView txt_IL3;
    @ViewInject(R.id.txt_peak_export_active)
    TextView txt_Peak_Export_Active_Energy;
    @ViewInject(R.id.txt_peak_export_reactive)
    TextView txt_Peak_Export_Reactive_Energy;
    @ViewInject(R.id.txt_peak_import_active)
    TextView txt_Peak_Import_Active_Energy;
    @ViewInject(R.id.txt_peak_import_reactive)
    TextView txt_Peak_Import_Reactive_Energy;
    @ViewInject(R.id.txt_psum)
    TextView txt_Psum;
    @ViewInject(R.id.txt_qsum)
    TextView txt_Qsum;
    @ViewInject(R.id.txt_standard_time)
    TextView txt_Standard_Time;
    @ViewInject(R.id.txt_time)
    TextView txt_Time;
    @ViewInject(R.id.txt_time_error)
    TextView txt_Time_Error;
    @ViewInject(R.id.txt_tip_export_active)
    TextView txt_Tip_Export_Active_Energy;
    @ViewInject(R.id.txt_tip_export_reactive)
    TextView txt_Tip_Export_Reactive_Energy;
    @ViewInject(R.id.txt_tip_import_active)
    TextView txt_Tip_Import_Active_Energy;
    @ViewInject(R.id.txt_tip_import_reactive)
    TextView txt_Tip_Import_Reactive_Energy;
    @ViewInject(R.id.txt_total_export_active)
    TextView txt_Total_Export_Active_Energy;
    @ViewInject(R.id.txt_total_export_reactive)
    TextView txt_Total_Export_Reactive_Energy;
    @ViewInject(R.id.txt_total_import_active)
    TextView txt_Total_Import_Active_Energy;
    @ViewInject(R.id.txt_total_import_reactive)
    TextView txt_Total_Import_Reactive_Energy;
    @ViewInject(R.id.txt_ul1)
    TextView txt_UL1;
    @ViewInject(R.id.txt_ul2)
    TextView txt_UL2;
    @ViewInject(R.id.txt_ul3)
    TextView txt_UL3;
    @ViewInject(R.id.txt_valley_export_active)
    TextView txt_Valley_Export_Active_Energy;
    @ViewInject(R.id.txt_valley_export_reactive)
    TextView txt_Valley_Export_Reactive_Energy;
    @ViewInject(R.id.txt_valley_import_active)
    TextView txt_Valley_Import_Active_Energy;
    @ViewInject(R.id.txt_valley_import_reactive)
    TextView txt_Valley_Import_Reactive_Energy;

    public ReadMeterRecordFragment(ReadMeter data) {
        this.m_Data = data;
    }

    @Override // android.support.v4.app.Fragment
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        this.fragmentView = inflater.inflate(R.layout.fragment_record_read_meter, (ViewGroup) null);
        ViewUtils.inject(this, this.fragmentView);
        initView();
        return this.fragmentView;
    }

    private void initView() {
        this.txt_Energys = new TextView[]{this.txt_Total_Import_Active_Energy, this.txt_Tip_Import_Active_Energy, this.txt_Peak_Import_Active_Energy, this.txt_Flat_Import_Active_Energy, this.txt_Valley_Import_Active_Energy, this.txt_Total_Export_Active_Energy, this.txt_Tip_Export_Active_Energy, this.txt_Peak_Export_Active_Energy, this.txt_Flat_Export_Active_Energy, this.txt_Valley_Export_Active_Energy, this.txt_Total_Import_Reactive_Energy, this.txt_Tip_Import_Reactive_Energy, this.txt_Peak_Import_Reactive_Energy, this.txt_Flat_Import_Reactive_Energy, this.txt_Valley_Import_Reactive_Energy, this.txt_Total_Export_Reactive_Energy, this.txt_Tip_Export_Reactive_Energy, this.txt_Peak_Export_Reactive_Energy, this.txt_Flat_Export_Reactive_Energy, this.txt_Valley_Export_Reactive_Energy};
    }

    @Override // android.support.v4.app.Fragment, com.clou.rs350.ui.fragment.BaseFragment
    public void onResume() {
        super.onResume();
        showData();
    }

    @Override // android.support.v4.app.Fragment, com.clou.rs350.ui.fragment.BaseFragment
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initView();
    }

    @Override // com.clou.rs350.ui.fragment.BaseFragment, com.clou.rs350.manager.MessageManager.IMessageListener
    public void onReceiveMessage(MeterBaseDevice Device) {
        super.onReceiveMessage(Device);
    }

    private void showData() {
        this.txt_Address.setText(this.m_Data.Address);
        this.txt_Time.setText(this.m_Data.DateAndTime);
        this.txt_Standard_Time.setText(this.m_Data.StandardTime);
        this.txt_Time_Error.setText(this.m_Data.TimeError);
        this.txt_UL1.setText(this.m_Data.U1);
        this.txt_UL2.setText(this.m_Data.U2);
        this.txt_UL3.setText(this.m_Data.U3);
        this.txt_IL1.setText(this.m_Data.I1);
        this.txt_IL2.setText(this.m_Data.I2);
        this.txt_IL3.setText(this.m_Data.I3);
        this.txt_Psum.setText(this.m_Data.Psum);
        this.txt_Qsum.setText(this.m_Data.Qsum);
        for (int i = 0; i < 20; i++) {
            this.txt_Energys[i].setText(this.m_Data.Energys[i]);
        }
    }

    @Override // android.support.v4.app.Fragment, com.clou.rs350.ui.fragment.BaseFragment
    public void onPause() {
        super.onPause();
    }
}
