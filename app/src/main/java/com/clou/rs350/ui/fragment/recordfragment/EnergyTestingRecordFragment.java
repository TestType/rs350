package com.clou.rs350.ui.fragment.recordfragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.clou.rs350.R;
import com.clou.rs350.db.model.EnergyTest;
import com.clou.rs350.ui.fragment.BaseFragment;
import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.view.annotation.ViewInject;

public class EnergyTestingRecordFragment extends BaseFragment {
    @ViewInject(R.id.active_end)
    private TextView ActiveEndTv;
    @ViewInject(R.id.active_end_unit)
    private TextView ActiveEndUnitTv;
    @ViewInject(R.id.active_error)
    private TextView ActiveErrorTv;
    @ViewInject(R.id.active_initial)
    private TextView ActiveInitialTv;
    @ViewInject(R.id.active_initial_unit)
    private TextView ActiveInitialUnitTv;
    @ViewInject(R.id.active_ref)
    private TextView ActiveRefTv;
    @ViewInject(R.id.apparent_end)
    private TextView ApparentEndTv;
    @ViewInject(R.id.apparent_end_unit)
    private TextView ApparentEndUnitTv;
    @ViewInject(R.id.apparent_error)
    private TextView ApparentErrorTv;
    @ViewInject(R.id.apparent_initial)
    private TextView ApparentInitialTv;
    @ViewInject(R.id.apparent_initial_unit)
    private TextView ApparentInitialUnitTv;
    @ViewInject(R.id.apparent_ref)
    private TextView ApparentRefTv;
    @ViewInject(R.id.reactive_end)
    private TextView ReactiveEndTv;
    @ViewInject(R.id.reactive_end_unit)
    private TextView ReactiveEndUnitTv;
    @ViewInject(R.id.reactive_error)
    private TextView ReactiveErrorTv;
    @ViewInject(R.id.reactive_initial)
    private TextView ReactiveInitialTv;
    @ViewInject(R.id.reactive_initial_unit)
    private TextView ReactiveInitialUnitTv;
    @ViewInject(R.id.reactive_ref)
    private TextView ReactiveRefTv;
    private EnergyTest m_EnergyMeasurement;
    @ViewInject(R.id.txt_meter_sr)
    private TextView txt_Meter_Sr;

    public EnergyTestingRecordFragment(EnergyTest data) {
        this.m_EnergyMeasurement = data;
    }

    @Override // android.support.v4.app.Fragment
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        this.fragmentView = inflater.inflate(R.layout.fragment_record_energy_testing, (ViewGroup) null);
        ViewUtils.inject(this, this.fragmentView);
        return this.fragmentView;
    }

    private void showData() {
        this.txt_Meter_Sr.setText(this.m_EnergyMeasurement.MeterSerialNo);
        refreshInput(this.m_EnergyMeasurement);
        showData(this.m_EnergyMeasurement);
    }

    @Override // android.support.v4.app.Fragment, com.clou.rs350.ui.fragment.BaseFragment
    public void onResume() {
        showData();
        super.onResume();
    }

    @Override // android.support.v4.app.Fragment, com.clou.rs350.ui.fragment.BaseFragment
    public void onPause() {
        super.onPause();
    }

    private void refreshInput(EnergyTest energyMeasurement) {
        this.ActiveInitialTv.setText(new StringBuilder(String.valueOf(energyMeasurement.BeginEnergy[0].s_Value)).toString());
        this.ReactiveInitialTv.setText(new StringBuilder(String.valueOf(energyMeasurement.BeginEnergy[1].s_Value)).toString());
        this.ApparentInitialTv.setText(new StringBuilder(String.valueOf(energyMeasurement.BeginEnergy[2].s_Value)).toString());
        this.ActiveEndTv.setText(new StringBuilder(String.valueOf(energyMeasurement.EndEnergy[0].s_Value)).toString());
        this.ReactiveEndTv.setText(new StringBuilder(String.valueOf(energyMeasurement.EndEnergy[1].s_Value)).toString());
        this.ApparentEndTv.setText(new StringBuilder(String.valueOf(energyMeasurement.EndEnergy[2].s_Value)).toString());
    }

    private void showData(EnergyTest energyMeasurement) {
        this.ActiveRefTv.setText(energyMeasurement.StandardEnergy[0].s_Value);
        this.ReactiveRefTv.setText(energyMeasurement.StandardEnergy[1].s_Value);
        this.ApparentRefTv.setText(energyMeasurement.StandardEnergy[2].s_Value);
        this.ActiveErrorTv.setText(energyMeasurement.Error[0].s_Value);
        this.ReactiveErrorTv.setText(energyMeasurement.Error[1].s_Value);
        this.ApparentErrorTv.setText(energyMeasurement.Error[2].s_Value);
    }
}
