package com.clou.rs350.ui.view;

import android.content.Context;
import androidx.viewpager.widget.ViewPager;
import android.util.AttributeSet;
import android.view.MotionEvent;

public class MyViewPager extends ViewPager {
    private boolean isPagingEnabled;

    public MyViewPager(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public MyViewPager(Context context) {
        super(context);
    }

    public void scrollTo(int x, int y) {
        super.scrollTo(x, y);
    }

    @Override // android.support.v4.view.ViewPager
    public boolean onTouchEvent(MotionEvent ev) {
        if (this.isPagingEnabled) {
            return super.onTouchEvent(ev);
        }
        return false;
    }

    @Override // android.support.v4.view.ViewPager
    public void setCurrentItem(int item, boolean smoothScroll) {
        super.setCurrentItem(item, smoothScroll);
    }

    @Override // android.support.v4.view.ViewPager
    public void setCurrentItem(int item) {
        super.setCurrentItem(item);
    }

    @Override // android.support.v4.view.ViewPager
    public boolean onInterceptTouchEvent(MotionEvent event) {
        if (this.isPagingEnabled) {
            return super.onInterceptTouchEvent(event);
        }
        return false;
    }

    public void setPagingEnabled(boolean b) {
        this.isPagingEnabled = b;
    }
}
