package com.clou.rs350.ui.fragment.sequencefragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.clou.rs350.R;
import com.clou.rs350.db.model.sequence.SignatureData;
import com.clou.rs350.db.model.sequence.SignatureItem;
import com.clou.rs350.ui.dialog.HintDialog;
import com.clou.rs350.ui.fragment.BaseFragment;
import com.clou.rs350.ui.view.MySignatureView;
import com.clou.rs350.utils.OtherUtils;
import com.itextpdf.text.pdf.PdfObject;

public class SignatureSequenceFragment extends BaseFragment implements View.OnClickListener {
    private Button btn_Delete;
    private Button btn_Next;
    private Button btn_Previous;
    SignatureData m_Data;
    private int m_Index = 0;
    MySignatureView m_SignatureView;
    private TextView txt_Index;
    private TextView txt_Name;
    private TextView txt_Total;

    public SignatureSequenceFragment(SignatureData data) {
        this.m_Data = data;
    }

    @Override // android.support.v4.app.Fragment
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        this.fragmentView = inflater.inflate(R.layout.fragment_sequence_signature, (ViewGroup) null);
        return this.fragmentView;
    }

    @Override // android.support.v4.app.Fragment, com.clou.rs350.ui.fragment.BaseFragment
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initView();
    }

    private void initView() {
        this.m_SignatureView = (MySignatureView) findViewById(R.id.v_signature);
        findViewById(R.id.btn_clean).setOnClickListener(this);
        this.btn_Previous = (Button) findViewById(R.id.btn_previous);
        this.btn_Previous.setOnClickListener(this);
        this.btn_Next = (Button) findViewById(R.id.btn_next);
        this.btn_Next.setOnClickListener(this);
        this.btn_Delete = (Button) findViewById(R.id.btn_delete);
        this.btn_Delete.setOnClickListener(this);
        this.txt_Index = (TextView) findViewById(R.id.txt_index);
        this.txt_Total = (TextView) findViewById(R.id.txt_total);
        this.txt_Name = (TextView) findViewById(R.id.txt_name);
        this.m_Data.Title = this.Title;
    }

    @Override // android.support.v4.app.Fragment, com.clou.rs350.ui.fragment.BaseFragment
    public void onResume() {
        showData();
        super.onResume();
    }

    @Override // android.support.v4.app.Fragment, com.clou.rs350.ui.fragment.BaseFragment
    public void onPause() {
        super.onPause();
    }

    /* access modifiers changed from: protected */
    @Override // com.clou.rs350.ui.fragment.BaseFragment
    public void copyData() {
        this.m_Data.Signatures.get(this.m_Index).Index = this.m_Index;
        this.m_Data.Signatures.get(this.m_Index).SignatureBmp = this.m_SignatureView.getSignatureBitmap();
        this.m_Data.Signatures.get(this.m_Index).HasSign = this.m_SignatureView.getHasSign();
        this.m_Data.Signatures.get(this.m_Index).Name = this.txt_Name.getText().toString();
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_delete:
                this.m_Data.Signatures.remove(this.m_Index);
                this.m_Index--;
                if (this.m_Index < 0) {
                    this.m_Index = 0;
                }
                showData();
                return;
            case R.id.btn_previous:
                copyData();
                this.m_Index--;
                showData();
                return;
            case R.id.btn_next:
                copyData();
                this.m_Index++;
                showData();
                return;
            case R.id.btn_clean:
                this.m_SignatureView.clearSignature();
                return;
            default:
                return;
        }
    }

    private void showData() {
        this.txt_Index.setText(new StringBuilder(String.valueOf(this.m_Index + 1)).toString());
        while (this.m_Data.Signatures.size() <= this.m_Index) {
            this.m_Data.Signatures.add(new SignatureItem());
        }
        this.txt_Total.setText(new StringBuilder(String.valueOf(this.m_Data.Signatures.size())).toString());
        this.txt_Name.setText(this.m_Data.Signatures.get(this.m_Index).Name);
        this.m_SignatureView.setSignatureBitmap(this.m_Data.Signatures.get(this.m_Index).SignatureBmp);
        this.m_SignatureView.setHasSign(this.m_Data.Signatures.get(this.m_Index).HasSign);
        setButton();
    }

    private void setButton() {
        boolean z;
        boolean z2 = true;
        Button button = this.btn_Previous;
        if (this.m_Index != 0) {
            z = true;
        } else {
            z = false;
        }
        button.setEnabled(z);
        this.btn_Next.setText(this.m_Index < this.m_Data.Signatures.size() + -1 ? R.string.text_next : R.string.text_add);
        Button button2 = this.btn_Delete;
        if (this.m_Data.Signatures.size() <= 1) {
            z2 = false;
        }
        button2.setEnabled(z2);
    }

    @Override // com.clou.rs350.ui.fragment.BaseFragment
    public int isFinish() {
        int result = 0;
        if (1 == this.m_MustDo) {
            String strHint = PdfObject.NOTHING;
            copyData();
            if (1 == this.m_Data.Signatures.size()) {
                SignatureItem tmpItem = this.m_Data.Signatures.get(0);
                if (OtherUtils.isEmpty(tmpItem.Name)) {
                    result = 1;
                    strHint = String.valueOf(strHint) + this.m_Context.getResources().getString(R.string.text_name) + " " + this.m_Context.getResources().getString(R.string.text_is_empty) + "\r\n";
                }
                if (!tmpItem.HasSign) {
                    result = 1;
                    strHint = String.valueOf(strHint) + this.m_Context.getResources().getString(R.string.text_signature) + " " + this.m_Context.getResources().getString(R.string.text_is_empty) + "\r\n";
                }
                if (1 == result) {
                    new HintDialog(this.m_Context, strHint.substring(0, strHint.length() - 2)).show();
                }
            }
        }
        return result;
    }
}
