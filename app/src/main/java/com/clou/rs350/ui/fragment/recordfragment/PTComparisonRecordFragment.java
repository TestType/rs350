package com.clou.rs350.ui.fragment.recordfragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.clou.rs350.R;
import com.clou.rs350.db.model.PTComparison;
import com.clou.rs350.ui.fragment.BaseFragment;
import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.view.annotation.ViewInject;

public class PTComparisonRecordFragment extends BaseFragment {
    @ViewInject(R.id.l1_delta_angle)
    private TextView Loca1L1AngleTv;
    @ViewInject(R.id.l1_error)
    private TextView Loca1L1ErrorTv;
    @ViewInject(R.id.loca1_l1_phase_angle)
    private TextView Loca1L1PhaseAngleTV;
    @ViewInject(R.id.loca1_l1_primary_voltage)
    private TextView Loca1L1PrimaryVoltageTV;
    @ViewInject(R.id.l1_delta_voltage)
    private TextView Loca1L1Voltage;
    @ViewInject(R.id.l2_delta_angle)
    private TextView Loca1L2AngleTv;
    @ViewInject(R.id.l2_error)
    private TextView Loca1L2ErrorTv;
    @ViewInject(R.id.loca1_l2_phase_angle)
    private TextView Loca1L2PhaseAngleTV;
    @ViewInject(R.id.loca1_l2_primary_voltage)
    private TextView Loca1L2PrimaryVoltageTV;
    @ViewInject(R.id.l2_delta_voltage)
    private TextView Loca1L2Voltage;
    @ViewInject(R.id.l3_delta_angle)
    private TextView Loca1L3AngleTv;
    @ViewInject(R.id.l3_error)
    private TextView Loca1L3ErrorTv;
    @ViewInject(R.id.loca1_l3_phase_angle)
    private TextView Loca1L3PhaseAngleTV;
    @ViewInject(R.id.loca1_l3_primary_voltage)
    private TextView Loca1L3PrimaryVoltageTV;
    @ViewInject(R.id.l3_delta_voltage)
    private TextView Loca1L3Voltage;
    @ViewInject(R.id.loca2_l1_phase_angle)
    private TextView Loca2L1PhaseAngleTV;
    @ViewInject(R.id.loca2_l1_primary_voltage)
    private TextView Loca2L1PrimaryVoltageTV;
    @ViewInject(R.id.loca2_l2_phase_angle)
    private TextView Loca2L2PhaseAngleTV;
    @ViewInject(R.id.loca2_l2_primary_voltage)
    private TextView Loca2L2PrimaryVoltageTV;
    @ViewInject(R.id.loca2_l3_phase_angle)
    private TextView Loca2L3PhaseAngleTV;
    @ViewInject(R.id.loca2_l3_primary_voltage)
    private TextView Loca2L3PrimaryVoltageTV;
    @ViewInject(R.id.txt_time)
    private TextView TimeTv;
    PTComparison m_PTComparison;

    public PTComparisonRecordFragment(PTComparison data) {
        this.m_PTComparison = data;
    }

    @Override // android.support.v4.app.Fragment
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        this.fragmentView = inflater.inflate(R.layout.fragment_record_pt_comparison, (ViewGroup) null);
        ViewUtils.inject(this, this.fragmentView);
        return this.fragmentView;
    }

    @Override // android.support.v4.app.Fragment, com.clou.rs350.ui.fragment.BaseFragment
    public void onResume() {
        super.onResume();
        showData();
    }

    private void showData() {
        this.TimeTv.setText(this.m_PTComparison.Time);
        this.Loca1L1PrimaryVoltageTV.setText(this.m_PTComparison.Location1Voltage[0].s_Value);
        this.Loca1L2PrimaryVoltageTV.setText(this.m_PTComparison.Location1Voltage[1].s_Value);
        this.Loca1L3PrimaryVoltageTV.setText(this.m_PTComparison.Location1Voltage[2].s_Value);
        this.Loca1L1PhaseAngleTV.setText(this.m_PTComparison.Location1Angle[0].s_Value);
        this.Loca1L2PhaseAngleTV.setText(this.m_PTComparison.Location1Angle[1].s_Value);
        this.Loca1L3PhaseAngleTV.setText(this.m_PTComparison.Location1Angle[2].s_Value);
        this.Loca2L1PrimaryVoltageTV.setText(this.m_PTComparison.Location2Voltage[0].s_Value);
        this.Loca2L2PrimaryVoltageTV.setText(this.m_PTComparison.Location2Voltage[1].s_Value);
        this.Loca2L3PrimaryVoltageTV.setText(this.m_PTComparison.Location2Voltage[2].s_Value);
        this.Loca2L1PhaseAngleTV.setText(this.m_PTComparison.Location2Angle[0].s_Value);
        this.Loca2L2PhaseAngleTV.setText(this.m_PTComparison.Location2Angle[1].s_Value);
        this.Loca2L3PhaseAngleTV.setText(this.m_PTComparison.Location2Angle[2].s_Value);
        this.Loca1L1Voltage.setText(this.m_PTComparison.RatioError[0].s_Value);
        this.Loca1L2Voltage.setText(this.m_PTComparison.RatioError[1].s_Value);
        this.Loca1L3Voltage.setText(this.m_PTComparison.RatioError[2].s_Value);
        this.Loca1L1AngleTv.setText(this.m_PTComparison.AngleError[0].s_Value);
        this.Loca1L2AngleTv.setText(this.m_PTComparison.AngleError[1].s_Value);
        this.Loca1L3AngleTv.setText(this.m_PTComparison.AngleError[2].s_Value);
        this.Loca1L1ErrorTv.setText(this.m_PTComparison.VoltageDifferent[0].s_Value);
        this.Loca1L2ErrorTv.setText(this.m_PTComparison.VoltageDifferent[1].s_Value);
        this.Loca1L3ErrorTv.setText(this.m_PTComparison.VoltageDifferent[2].s_Value);
    }
}
