package com.clou.rs350.ui.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.clou.rs350.R;
import com.clou.rs350.callback.ILoadCallback;
import com.clou.rs350.db.dao.FieldSettingDao;
import com.clou.rs350.db.model.sequence.FieldSetting;
import com.clou.rs350.ui.adapter.FieldSettingAdapter;
import com.clou.rs350.utils.FieldSettingUtil;
import com.itextpdf.text.pdf.PdfObject;

public class FieldSettingDialog extends Dialog {
    private Button btn_Close = null;
    private Button btn_Save = null;
    private View.OnClickListener cancleClick = null;
    private ListView lst_FieldSetting;
    private Context m_Context = null;
    private FieldSettingDao m_Dao;
    private FieldSetting m_FieldSetting;
    private FieldSettingAdapter m_FieldSettingAdapter;
    private String m_Title;
    private View.OnClickListener okClick = null;
    private TextView txt_Title = null;

    public FieldSettingDialog(Context context, String Title) {
        super(context, R.style.dialog);
        requestWindowFeature(1);
        setCanceledOnTouchOutside(false);
        this.m_Context = context;
        this.m_Dao = new FieldSettingDao(this.m_Context);
        this.m_Title = Title;
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_field_setting);
        this.btn_Close = (Button) findViewById(R.id.btn_close);
        this.btn_Close.setOnClickListener(new View.OnClickListener() {
            /* class com.clou.rs350.ui.dialog.FieldSettingDialog.AnonymousClass1 */

            public void onClick(View v) {
                if (FieldSettingDialog.this.cancleClick != null) {
                    FieldSettingDialog.this.cancleClick.onClick(v);
                }
                FieldSettingDialog.this.dismiss();
            }
        });
        this.btn_Save = (Button) findViewById(R.id.btn_save);
        this.btn_Save.setOnClickListener(new View.OnClickListener() {
            /* class com.clou.rs350.ui.dialog.FieldSettingDialog.AnonymousClass2 */

            public void onClick(View v) {
                if (FieldSettingDialog.this.okClick != null) {
                    FieldSettingDialog.this.okClick.onClick(v);
                }
                FieldSettingDialog.this.Save();
            }
        });
        this.txt_Title = (TextView) findViewById(R.id.txt_title);
        this.lst_FieldSetting = (ListView) findViewById(R.id.list_field_setting);
        this.m_FieldSettingAdapter = new FieldSettingAdapter(this.m_Context);
        this.lst_FieldSetting.setAdapter((ListAdapter) this.m_FieldSettingAdapter);
        initData();
    }

    private void initData() {
        this.m_FieldSetting = new FieldSettingUtil(this.m_Context).getFieldSetting(this.m_Title);
        this.txt_Title.setText(this.m_FieldSetting.TitleId);
        this.m_FieldSettingAdapter.refresh(this.m_FieldSetting.ItemKeys, this.m_FieldSetting.Items);
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private void parseData() {
        this.m_FieldSetting.Items = this.m_FieldSettingAdapter.getData();
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private int Save() {
        new LoadingDialog(this.m_Context, new ILoadCallback() {
            /* class com.clou.rs350.ui.dialog.FieldSettingDialog.AnonymousClass3 */

            @Override // com.clou.rs350.callback.ILoadCallback
            public Object run() {
                FieldSettingDialog.this.parseData();
                FieldSettingDialog.this.m_Dao.deleteRecordByKey(FieldSettingDialog.this.m_Title);
                return Integer.valueOf(FieldSettingDialog.this.m_Dao.insertObject(FieldSettingDialog.this.m_FieldSetting, PdfObject.NOTHING) ? 0 : 1);
            }

            @Override // com.clou.rs350.callback.ILoadCallback
            public void callback(Object result) {
                if (((Integer) result).intValue() == 0) {
                    new HintDialog(FieldSettingDialog.this.m_Context, (int) R.string.text_save_success).show();
                } else {
                    new HintDialog(FieldSettingDialog.this.m_Context, (int) R.string.text_save_fail).show();
                }
            }
        }).show();
        return 0;
    }

    public void setOnClick(View.OnClickListener okClick2) {
        this.okClick = okClick2;
    }

    public void setOnClick(View.OnClickListener okClick2, View.OnClickListener cancleClick2) {
        this.okClick = okClick2;
        this.cancleClick = cancleClick2;
    }
}
