package com.clou.rs350.ui.fragment.transformerfragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.clou.rs350.Preferences;
import com.clou.rs350.R;
import com.clou.rs350.db.model.DBDataModel;
import com.clou.rs350.db.model.PTBurden;
import com.clou.rs350.db.model.SiteData;
import com.clou.rs350.device.MeterBaseDevice;
import com.clou.rs350.model.ClouData;
import com.clou.rs350.task.ISleepCallback;
import com.clou.rs350.task.TimerThread;
import com.clou.rs350.ui.dialog.NumericDialog;
import com.clou.rs350.ui.fragment.BaseFragment;
import com.clou.rs350.utils.OtherUtils;
import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.view.annotation.ViewInject;
import com.lidroid.xutils.view.annotation.event.OnClick;

public class PTBurdenFragment extends BaseFragment implements View.OnClickListener {
    @ViewInject(R.id.txt_accuracyclass)
    private TextView AccuracyclassTv;
    @ViewInject(R.id.l1)
    private TextView L1ResultTv;
    @ViewInject(R.id.l1vtburdenva)
    private TextView L1VtBurdenVATv;
    @ViewInject(R.id.l1vtburdenw)
    private TextView L1VtBurdenWTv;
    @ViewInject(R.id.l1vtcurrent)
    private TextView L1VtCurrentTv;
    @ViewInject(R.id.l1vtnominal)
    private TextView L1VtNominalTv;
    @ViewInject(R.id.l1vtphaseangle)
    private TextView L1VtPhaseAngleTv;
    @ViewInject(R.id.l1vtsecondaryvoltage)
    private TextView L1VtSecondaryVoltageTv;
    @ViewInject(R.id.l2)
    private TextView L2ResultTv;
    @ViewInject(R.id.l2vtburdenva)
    private TextView L2VtBurdenVATv;
    @ViewInject(R.id.l2vtburdenw)
    private TextView L2VtBurdenWTv;
    @ViewInject(R.id.l2vtcurrent)
    private TextView L2VtCurrentTv;
    @ViewInject(R.id.l2vtnominal)
    private TextView L2VtNominalTv;
    @ViewInject(R.id.l2vtphaseangle)
    private TextView L2VtPhaseAngleTv;
    @ViewInject(R.id.l2vtsecondaryvoltage)
    private TextView L2VtSecondaryVoltageTv;
    @ViewInject(R.id.l3)
    private TextView L3ResultTv;
    @ViewInject(R.id.l3vtburdenva)
    private TextView L3VtBurdenVATv;
    @ViewInject(R.id.l3vtburdenw)
    private TextView L3VtBurdenWTv;
    @ViewInject(R.id.l3vtcurrent)
    private TextView L3VtCurrentTv;
    @ViewInject(R.id.l3vtnominal)
    private TextView L3VtNominalTv;
    @ViewInject(R.id.l3vtphaseangle)
    private TextView L3VtPhaseAngleTv;
    @ViewInject(R.id.l3vtsecondaryvoltage)
    private TextView L3VtSecondaryVoltageTv;
    @ViewInject(R.id.txt_vs)
    private TextView VSTv;
    @ViewInject(R.id.txt_va)
    private TextView VaTv;
    private int m_AngleDefinition;
    private PTBurden m_PTMeasurement;
    private TimerThread readDataThread = null;

    public PTBurdenFragment(PTBurden Data) {
        this.m_PTMeasurement = Data;
    }

    @Override // android.support.v4.app.Fragment
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        this.fragmentView = inflater.inflate(R.layout.fragment_pt_burden, (ViewGroup) null);
        ViewUtils.inject(this, this.fragmentView);
        return this.fragmentView;
    }

    @Override // android.support.v4.app.Fragment, com.clou.rs350.ui.fragment.BaseFragment
    public void onResume() {
        super.onResume();
        this.m_AngleDefinition = Preferences.getInt("AngleDefinition", 0);
        refreshData();
        initData();
    }

    @Override // com.clou.rs350.ui.fragment.BaseFragment
    public void refreshData() {
        SiteData baseInfo = ClouData.getInstance().getSiteData();
        this.VSTv.setText(new StringBuilder(String.valueOf(baseInfo.PTSecondary.d_Value)).toString());
        this.m_PTMeasurement.VoltageValueNormalSecondary = baseInfo.PTSecondary;
        this.m_PTMeasurement.setVAValue(baseInfo.PTBurden.d_Value);
        this.AccuracyclassTv.setText(baseInfo.PTAccuracyClass);
        this.VaTv.setText(new StringBuilder(String.valueOf(baseInfo.PTBurden.d_Value)).toString());
        showData();
    }

    @OnClick({R.id.txt_vs, R.id.txt_va, R.id.txt_accuracyclass})
    public void onClick(View view) {
        NumericDialog nd = new NumericDialog(this.m_Context);
        switch (view.getId()) {
            case R.id.txt_va:
                nd.showMyDialog((TextView) view);
                nd.setOnDoneClick(new View.OnClickListener() {
                    /* class com.clou.rs350.ui.fragment.transformerfragment.PTBurdenFragment.AnonymousClass2 */

                    public void onClick(View v) {
                        PTBurdenFragment.this.m_PTMeasurement.setVAValue(OtherUtils.parseDouble(PTBurdenFragment.this.VaTv.getText().toString().trim()));
                        PTBurdenFragment.this.showData();
                    }
                });
                return;
            case R.id.txt_accuracyclass:
                nd.txtLimit = 6;
                nd.showMyDialog((TextView) view);
                return;
            case R.id.txt_vs:
                nd.txtLimit = 6;
                nd.showMyDialog((TextView) view);
                nd.setOnDoneClick(new View.OnClickListener() {
                    /* class com.clou.rs350.ui.fragment.transformerfragment.PTBurdenFragment.AnonymousClass1 */

                    public void onClick(View v) {
                        String TmpValue = PTBurdenFragment.this.VSTv.getText().toString();
                        PTBurdenFragment.this.m_PTMeasurement.VoltageValueNormalSecondary = new DBDataModel(OtherUtils.parseDouble(TmpValue), String.valueOf(TmpValue) + "V");
                        PTBurdenFragment.this.showData();
                    }
                });
                return;
            default:
                return;
        }
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private void showData() {
        this.L1VtSecondaryVoltageTv.setText(this.m_PTMeasurement.VoltageValue[0].s_Value);
        this.L2VtSecondaryVoltageTv.setText(this.m_PTMeasurement.VoltageValue[1].s_Value);
        this.L3VtSecondaryVoltageTv.setText(this.m_PTMeasurement.VoltageValue[2].s_Value);
        this.L1VtCurrentTv.setText(this.m_PTMeasurement.CurrentValue[0].s_Value);
        this.L2VtCurrentTv.setText(this.m_PTMeasurement.CurrentValue[1].s_Value);
        this.L3VtCurrentTv.setText(this.m_PTMeasurement.CurrentValue[2].s_Value);
        this.L1VtPhaseAngleTv.setText(this.m_PTMeasurement.VoltageCurrentAngle[0].s_Value);
        this.L2VtPhaseAngleTv.setText(this.m_PTMeasurement.VoltageCurrentAngle[1].s_Value);
        this.L3VtPhaseAngleTv.setText(this.m_PTMeasurement.VoltageCurrentAngle[2].s_Value);
        this.L1VtBurdenWTv.setText(this.m_PTMeasurement.BurdenW[0].s_Value);
        this.L2VtBurdenWTv.setText(this.m_PTMeasurement.BurdenW[1].s_Value);
        this.L3VtBurdenWTv.setText(this.m_PTMeasurement.BurdenW[2].s_Value);
        this.L1VtBurdenVATv.setText(this.m_PTMeasurement.BurdenVA[0].s_Value);
        this.L2VtBurdenVATv.setText(this.m_PTMeasurement.BurdenVA[1].s_Value);
        this.L3VtBurdenVATv.setText(this.m_PTMeasurement.BurdenVA[2].s_Value);
        this.L1VtNominalTv.setText(this.m_PTMeasurement.BurdenPersents[0].s_Value);
        this.L2VtNominalTv.setText(this.m_PTMeasurement.BurdenPersents[1].s_Value);
        this.L3VtNominalTv.setText(this.m_PTMeasurement.BurdenPersents[2].s_Value);
        OtherUtils.setText(this.L1ResultTv, this.m_PTMeasurement.Result[0]);
        OtherUtils.setText(this.L2ResultTv, this.m_PTMeasurement.Result[1]);
        OtherUtils.setText(this.L3ResultTv, this.m_PTMeasurement.Result[2]);
    }

    @Override // com.clou.rs350.ui.fragment.BaseFragment, com.clou.rs350.manager.MessageManager.IMessageListener
    public void onReceiveMessage(MeterBaseDevice Device) {
        super.onReceiveMessage(Device);
        this.m_PTMeasurement.parseData(Device, this.m_AngleDefinition);
        showData();
    }

    /* access modifiers changed from: protected */
    @Override // com.clou.rs350.ui.fragment.BaseFragment
    public void copyData() {
        SiteData baseInfo = ClouData.getInstance().getSiteData();
        baseInfo.PTAccuracyClass = this.AccuracyclassTv.getText().toString();
        baseInfo.PTBurden = this.m_PTMeasurement.PTVA;
        baseInfo.PTSecondary = this.m_PTMeasurement.VoltageValueNormalSecondary;
    }

    @Override // android.support.v4.app.Fragment, com.clou.rs350.ui.fragment.BaseFragment
    public void onPause() {
        super.onPause();
        killThread(this.readDataThread);
    }

    private void initData() {
        this.messageManager.setFunc_Mstate(66);
        int refreshTime = Preferences.getInt(Preferences.Key.REFRESHTIME, 1000);
        if (this.readDataThread == null || this.readDataThread.isStop()) {
            this.readDataThread = new TimerThread((long) refreshTime, new ISleepCallback() {
                /* class com.clou.rs350.ui.fragment.transformerfragment.PTBurdenFragment.AnonymousClass3 */

                @Override // com.clou.rs350.task.ISleepCallback
                public void onSleepAfterToDo() {
                    PTBurdenFragment.this.messageManager.getPTBurden();
                }
            }, "Get Data Thread");
            this.readDataThread.start();
        }
    }
}
