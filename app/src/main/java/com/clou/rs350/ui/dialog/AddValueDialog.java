package com.clou.rs350.ui.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.clou.rs350.R;

public class AddValueDialog extends Dialog implements View.OnClickListener {
    private Button btn_Cancel = null;
    private Button btn_Ok = null;
    private View.OnClickListener cancleClick = null;
    private Context context = null;
    private View.OnClickListener okClick = null;
    private TextView txt_ContentView = null;
    private TextView txt_Value = null;

    public AddValueDialog(Context context2) {
        super(context2, R.style.dialog);
        requestWindowFeature(1);
        setCanceledOnTouchOutside(true);
        this.context = context2;
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_add_value);
        this.txt_ContentView = (TextView) findViewById(R.id.txt_title);
        this.txt_ContentView.setMovementMethod(ScrollingMovementMethod.getInstance());
        this.txt_Value = (TextView) findViewById(R.id.txt_value);
        this.btn_Ok = (Button) findViewById(R.id.btn_ok);
        this.btn_Ok.setOnClickListener(this);
        this.btn_Cancel = (Button) findViewById(R.id.btn_cancel);
        this.btn_Cancel.setOnClickListener(this);
    }

    @Override // android.app.Dialog
    public void setTitle(CharSequence title) {
        this.txt_ContentView.setText(title.toString());
    }

    public void setOkText(CharSequence text) {
        this.btn_Ok.setText(text);
    }

    public void setOkText(int id) {
        this.btn_Ok.setText(id);
    }

    public void setCancleText(CharSequence text) {
        this.btn_Cancel.setText(text);
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_cancel:
                if (this.cancleClick != null) {
                    this.cancleClick.onClick(v);
                }
                dismiss();
                return;
            case R.id.btn_ok:
                v.setTag(this.txt_Value.getText().toString());
                if (this.okClick != null) {
                    this.okClick.onClick(v);
                }
                dismiss();
                return;
            default:
                return;
        }
    }

    public void setOnClick(View.OnClickListener onClickListener, View.OnClickListener cancleClick2) {
        this.okClick = onClickListener;
        this.cancleClick = cancleClick2;
    }
}
