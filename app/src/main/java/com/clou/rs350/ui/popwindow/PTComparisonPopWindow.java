package com.clou.rs350.ui.popwindow;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.clou.rs350.R;
import com.clou.rs350.db.model.MeterInfo;
import com.clou.rs350.ui.dialog.NumericDialog;

public class PTComparisonPopWindow extends PopupWindow implements View.OnClickListener {
    Context mContext;
    View.OnClickListener mOnClick;
    MeterInfo m_Info;
    TextView txt_vt_primary_location1;
    TextView txt_vt_primary_location2;
    TextView txt_vt_secondary_location1;
    TextView txt_vt_secondary_location2;

    public PTComparisonPopWindow(Context context) {
        super(context);
        this.mContext = context;
        initView();
    }

    private void initView() {
        View conentView = ((LayoutInflater) this.mContext.getSystemService("layout_inflater")).inflate(R.layout.pop_window_pt_comparison_config, (ViewGroup) null);
        conentView.findViewById(R.id.ok).setOnClickListener(this);
        conentView.findViewById(R.id.cancel).setOnClickListener(this);
        conentView.findViewById(R.id.txt_vt_primary_location1).setOnClickListener(this);
        conentView.findViewById(R.id.txt_vt_primary_location2).setOnClickListener(this);
        conentView.findViewById(R.id.txt_vt_secondary_location1).setOnClickListener(this);
        conentView.findViewById(R.id.txt_vt_secondary_location2).setOnClickListener(this);
        setContentView(conentView);
        setWidth(this.mContext.getResources().getDimensionPixelSize(R.dimen.dp_440));
        setHeight(this.mContext.getResources().getDimensionPixelSize(R.dimen.dp_260));
        setFocusable(true);
        setOutsideTouchable(true);
        update();
        setBackgroundDrawable(new ColorDrawable(0));
        setAnimationStyle(16973826);
    }

    public void showPopWindow(View currentClick, MeterInfo Info, View.OnClickListener onclick) {
        this.mOnClick = onclick;
        this.m_Info = Info;
        if (!isShowing()) {
            int[] location = new int[2];
            currentClick.getLocationOnScreen(location);
            showAtLocation(currentClick, 0, location[0] - getWidth(), location[1]);
            return;
        }
        dismiss();
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ok:
                if (isShowing()) {
                    dismiss();
                    return;
                }
                return;
            case R.id.cancel:
                if (isShowing()) {
                    dismiss();
                    return;
                }
                return;
            case R.id.txt_vt_primary_location1:
            case R.id.txt_vt_secondary_location1:
            case R.id.txt_vt_primary_location2:
            case R.id.txt_vt_secondary_location2:
                NumericDialog nd = new NumericDialog(this.mContext);
                nd.txtLimit = 6;
                nd.showMyDialog((TextView) v);
                return;
            default:
                return;
        }
    }
}
