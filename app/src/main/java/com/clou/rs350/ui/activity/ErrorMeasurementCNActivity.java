package com.clou.rs350.ui.activity;

import android.os.AsyncTask;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.animation.Animation;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.clou.rs350.R;
import com.clou.rs350.callback.ILoadCallback;
import com.clou.rs350.db.dao.BaseTestDao;
import com.clou.rs350.db.dao.DailyTestDao;
import com.clou.rs350.db.dao.DigitalMeterTestDao;
import com.clou.rs350.db.dao.EnergyTestDao;
import com.clou.rs350.db.dao.ErrorTestDao;
import com.clou.rs350.db.model.BasicMeasurement;
import com.clou.rs350.db.model.DailyTest;
import com.clou.rs350.db.model.DigitalMeterTest;
import com.clou.rs350.db.model.EnergyTest;
import com.clou.rs350.db.model.ErrorTest;
import com.clou.rs350.device.MeterBaseDevice;
import com.clou.rs350.handler.HandlerSwitchView;
import com.clou.rs350.model.ClouData;
import com.clou.rs350.task.ISleepCallback;
import com.clou.rs350.task.MyTask;
import com.clou.rs350.task.TimerThread;
import com.clou.rs350.ui.adapter.FragmentTabAdapter;
import com.clou.rs350.ui.dialog.SaveErrorDialog;
import com.clou.rs350.ui.fragment.BaseFragment;
import com.clou.rs350.ui.fragment.errorfragment.DailyTestingFragment;
import com.clou.rs350.ui.fragment.errorfragment.DigitalMeterTestingFragment;
import com.clou.rs350.ui.fragment.errorfragment.EnergyTestingFragment;
import com.clou.rs350.ui.fragment.errorfragment.ErrorTestingFragment;
import com.clou.rs350.ui.view.MeterPopView;
import com.clou.rs350.ui.view.WiringAndRangeView;
import com.clou.rs350.utils.AnimaUtils;
import com.clou.rs350.utils.OtherUtils;
import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.view.annotation.ViewInject;
import com.lidroid.xutils.view.annotation.event.OnClick;

import java.util.ArrayList;
import java.util.List;

public class ErrorMeasurementCNActivity extends BaseActivity implements View.OnClickListener, HandlerSwitchView.IOnSelectCallback {
    @ViewInject(R.id.error_arrow_right_imageview)
    private ImageView arrowRightView;
    @ViewInject(R.id.btn_back)
    private Button backButtom;
    private MeterPopView errorPopView;
    private boolean hasStart = false;
    BasicMeasurement m_BasicValue = new BasicMeasurement();
    @ViewInject(R.id.setting_normal_linearlayout)
    private LinearLayout normalLayout;
    @ViewInject(R.id.layout_pop)
    private View popView;
    @ViewInject(R.id.btn_save)
    private Button saveButtom;
    private HandlerSwitchView switchView;
    private TimerThread timerThread;
    @ViewInject(R.id.title_textview)
    private TextView titleTextView;
    @ViewInject(R.id.layout_wiring)
    private View wiringLayout;
    private WiringAndRangeView wiringView;

    /* access modifiers changed from: protected */
    @Override // com.clou.rs350.ui.activity.BaseActivity, android.support.v4.app.FragmentActivity
    public void onCreate(Bundle savedInstanceState) {
        this.TAG = "ErrorMeasurementActivity";
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_error_measurement_cn);
        setDefaultTitle(R.string.text_error_testing);
        ViewUtils.inject(this);
        initView();
        this.m_TabAdapter = new FragmentTabAdapter(this, getFragments(), R.id.errormeasurementcontent_view);
        this.m_TabAdapter.init();
    }

    private List<BaseFragment> getFragments() {
        List<BaseFragment> list = new ArrayList<>();
        list.add(new ErrorTestingFragment(ClouData.getInstance().getErrorTest()));
        list.add(new EnergyTestingFragment(ClouData.getInstance().getEnergyTest()));
        list.add(new DailyTestingFragment(ClouData.getInstance().getDailyTest()));
        list.add(new DigitalMeterTestingFragment(ClouData.getInstance().getDigitalTest()));
        return list;
    }

    /* access modifiers changed from: protected */
    @Override // com.clou.rs350.ui.activity.BaseActivity, android.support.v4.app.FragmentActivity
    public void onResume() {
        super.onResume();
        startToReadRangeData();
    }

    /* access modifiers changed from: protected */
    @Override // com.clou.rs350.ui.activity.BaseActivity, android.support.v4.app.FragmentActivity
    public void onPause() {
        super.onPause();
        killThread(this.timerThread);
    }

    private void startToReadRangeData() {
        this.messageManager.getStartState();
        if (this.timerThread == null || this.timerThread.isStop()) {
            this.timerThread = new TimerThread(2000, new ISleepCallback() {
                /* class com.clou.rs350.ui.activity.ErrorMeasurementCNActivity.AnonymousClass1 */
                private int count;

                @Override // com.clou.rs350.task.ISleepCallback
                public void onSleepAfterToDo() {
                    switch (this.count) {
                        case 1:
                            ErrorMeasurementCNActivity.this.messageManager.getRanage();
                            break;
                        default:
                            ErrorMeasurementCNActivity.this.messageManager.getData();
                            this.count = 0;
                            break;
                    }
                    this.count++;
                }
            }, "Read Range Thread");
            this.timerThread.start();
        }
    }

    private void initView() {
        int i = 0;
        List<View> tabViews = new ArrayList<>();
        tabViews.add(findViewById(R.id.erroe_layout));
        tabViews.add(findViewById(R.id.energytest_layout));
        tabViews.add(findViewById(R.id.dailytest_layout));
        tabViews.add(findViewById(R.id.digitalmetertest_layout));
        this.switchView = new HandlerSwitchView(this.m_Context);
        this.switchView.setTabViews(tabViews);
        this.switchView.boundClick();
        this.switchView.setOnTabChangedCallback(this);
        this.switchView.selectView(0);
        this.errorPopView = new MeterPopView(this.popView);
        this.wiringView = new WiringAndRangeView(this, this.wiringLayout);
        View findViewById = findViewById(R.id.digitalmetertest_layout);
        if (!ClouData.getInstance().getSettingFunction().DigitalMeterTesting) {
            i = 4;
        }
        findViewById.setVisibility(i);
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private void showMeasurement(BasicMeasurement basicValue) {
        this.errorPopView.showData(basicValue);
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_back:
                finish();
                return;
            case R.id.other_layout:
                this.normalLayout.setVisibility(View.GONE);
                return;
            case R.id.erroe_layout:
                this.m_TabAdapter.checkedIndex(0);
                this.titleTextView.setText(R.string.text_error_testing);
                return;
            case R.id.energytest_layout:
                this.m_TabAdapter.checkedIndex(1);
                this.titleTextView.setText(R.string.text_energy_testing);
                return;
            case R.id.btn_save:
                new SaveErrorDialog(this.m_Context, getString(new int[]{R.string.text_error_testing, R.string.text_energy_testing, R.string.text_daily_testing, R.string.text_digital_meter_testing}[this.m_TabAdapter.getCurrentTab()]), new SaveErrorDialog.ISave() {
                    /* class com.clou.rs350.ui.activity.ErrorMeasurementCNActivity.AnonymousClass2 */

                    @Override // com.clou.rs350.ui.dialog.SaveErrorDialog.ISave
                    public boolean SaveMeasureData(String[] SerialNos, String Time) {
                        boolean Reault = true;
                        String tmpCustomerSr = ClouData.getInstance().getCustomerInfo().CustomerSr;
                        switch (ErrorMeasurementCNActivity.this.m_TabAdapter.getCurrentTab()) {
                            case 0:
                                ErrorTest[] ErrorDatas = ClouData.getInstance().getErrorTest();
                                BaseTestDao tmpDao = new ErrorTestDao(ErrorMeasurementCNActivity.this.m_Context);
                                for (int i = 0; i < 3; i++) {
                                    if (!OtherUtils.isEmpty(SerialNos[i])) {
                                        ErrorDatas[i].CustomerSerialNo = tmpCustomerSr;
                                        ErrorDatas[i].MeterSerialNo = SerialNos[i];
                                        boolean result = tmpDao.insertObject(ErrorDatas[i], Time);
                                        if (result) {
                                            ErrorDatas[i].cleanError();
                                        }
                                        Reault &= result;
                                    }
                                }
                                return Reault;
                            case 1:
                                EnergyTest[] EnergyDatas = ClouData.getInstance().getEnergyTest();
                                BaseTestDao tmpDao2 = new EnergyTestDao(ErrorMeasurementCNActivity.this.m_Context);
                                for (int i2 = 0; i2 < 3; i2++) {
                                    if (!OtherUtils.isEmpty(SerialNos[i2])) {
                                        EnergyDatas[i2].CustomerSerialNo = tmpCustomerSr;
                                        EnergyDatas[i2].MeterSerialNo = SerialNos[i2];
                                        boolean result2 = tmpDao2.insertObject(EnergyDatas[i2], Time);
                                        if (result2) {
                                            EnergyDatas[i2].cleanError();
                                        }
                                        Reault &= result2;
                                    }
                                }
                                return Reault;
                            case 2:
                                DailyTest[] DailyDatas = ClouData.getInstance().getDailyTest();
                                BaseTestDao tmpDao3 = new DailyTestDao(ErrorMeasurementCNActivity.this.m_Context);
                                for (int i3 = 0; i3 < 3; i3++) {
                                    if (!OtherUtils.isEmpty(SerialNos[i3])) {
                                        DailyDatas[i3].CustomerSerialNo = tmpCustomerSr;
                                        DailyDatas[i3].MeterSerialNo = SerialNos[i3];
                                        boolean result3 = tmpDao3.insertObject(DailyDatas[i3], Time);
                                        if (result3) {
                                            DailyDatas[i3].cleanError();
                                        }
                                        Reault &= result3;
                                    }
                                }
                                return Reault;
                            case 3:
                                DigitalMeterTest DigitalMeterDatas = ClouData.getInstance().getDigitalTest();
                                BaseTestDao tmpDao4 = new DigitalMeterTestDao(ErrorMeasurementCNActivity.this.m_Context);
                                if (OtherUtils.isEmpty(SerialNos[0])) {
                                    return true;
                                }
                                DigitalMeterDatas.CustomerSerialNo = tmpCustomerSr;
                                DigitalMeterDatas.MeterSerialNo = SerialNos[0];
                                boolean result4 = tmpDao4.insertObject(DigitalMeterDatas, Time);
                                if (result4) {
                                    DigitalMeterDatas.cleanError();
                                }
                                return true & result4;
                            default:
                                return true;
                        }
                    }
                }, new SaveErrorDialog.IAfterSave() {
                    /* class com.clou.rs350.ui.activity.ErrorMeasurementCNActivity.AnonymousClass3 */

                    @Override // com.clou.rs350.ui.dialog.SaveErrorDialog.IAfterSave
                    public void AfterSave(boolean SaveFlag) {
                        ErrorMeasurementCNActivity.this.m_TabAdapter.getCurrentFragment().refreshData();
                    }
                }).show();
                return;
            case R.id.dailytest_layout:
                this.m_TabAdapter.checkedIndex(2);
                this.titleTextView.setText(R.string.text_daily_testing);
                return;
            case R.id.digitalmetertest_layout:
                this.m_TabAdapter.checkedIndex(3);
                this.titleTextView.setText(R.string.text_digital_meter_testing);
                return;
            default:
                return;
        }
    }

    @Override // com.clou.rs350.ui.activity.BaseActivity
    public void startOrStopChangeViewStatus(int flag) {
        boolean z;
        boolean z2;
        boolean z3;
        boolean z4 = false;
        super.startOrStopChangeViewStatus(flag);
        if (flag == 0) {
            this.switchView.boundClick();
            this.switchView.selectCurrentView();
        } else {
            this.switchView.unBoundClick();
        }
        if (1 == flag) {
            z = true;
        } else {
            z = false;
        }
        this.hasStart = z;
        Button button = this.backButtom;
        if (this.hasStart) {
            z2 = false;
        } else {
            z2 = true;
        }
        button.setEnabled(z2);
        Button button2 = this.saveButtom;
        if (this.hasStart) {
            z3 = false;
        } else {
            z3 = true;
        }
        button2.setEnabled(z3);
        WiringAndRangeView wiringAndRangeView = this.wiringView;
        if (!this.hasStart) {
            z4 = true;
        }
        wiringAndRangeView.setEnabled(z4);
    }

    @Override // com.clou.rs350.ui.activity.BaseActivity, com.clou.rs350.manager.MessageManager.IMessageListener
    public void onReceiveMessage(final MeterBaseDevice Device) {
        new MyTask(new ILoadCallback() {
            /* class com.clou.rs350.ui.activity.ErrorMeasurementCNActivity.AnonymousClass4 */

            @Override // com.clou.rs350.callback.ILoadCallback
            public Object run() {
                ErrorMeasurementCNActivity.this.m_BasicValue.parseData(Device);
                return null;
            }

            @Override // com.clou.rs350.callback.ILoadCallback
            public void callback(Object result) {
                ErrorMeasurementCNActivity.this.wiringView.showData(ErrorMeasurementCNActivity.this.m_BasicValue);
                ErrorMeasurementCNActivity.this.showMeasurement(ErrorMeasurementCNActivity.this.m_BasicValue);
                if (ErrorMeasurementCNActivity.this.wiringView.getHasRangeChange() && 1 == ErrorMeasurementCNActivity.this.m_StartFlag) {
                    ErrorMeasurementCNActivity.this.showHint(R.string.text_range_changed);
                }
            }
        }).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, new Integer[0]);
    }

    @Override // com.clou.rs350.handler.HandlerSwitchView.IOnSelectCallback
    public void onSelectedChanged(int index) {
    }

    @Override // com.clou.rs350.handler.HandlerSwitchView.IOnSelectCallback
    public void onClickView(View view) {
        onClick(view);
    }

    @OnClick({R.id.layout_pop})
    public void popClick(View view) {
        switch (view.getId()) {
            case R.id.layout_pop:
                View contentView = view.findViewById(R.id.error_pop_content_view);
                if (contentView.getVisibility() == View.VISIBLE) {
                    toLeft(contentView, view);
                    return;
                } else {
                    toRight(contentView, view);
                    return;
                }
            default:
                return;
        }
    }

    private void toLeft(final View contentView, View parentView) {
        Animation ani = AnimaUtils.createRightToLeftAnimation(parentView, (float) contentView.getLayoutParams().width);
        ani.setAnimationListener(new Animation.AnimationListener() {
            /* class com.clou.rs350.ui.activity.ErrorMeasurementCNActivity.AnonymousClass5 */

            public void onAnimationStart(Animation animation) {
            }

            public void onAnimationRepeat(Animation animation) {
            }

            public void onAnimationEnd(Animation animation) {
                contentView.setVisibility(View.GONE);
                ErrorMeasurementCNActivity.this.arrowRightView.setVisibility(View.VISIBLE);
            }
        });
        AnimaUtils.startAnimation(ani, parentView);
    }

    private void toRight(final View contentView, View parentView) {
        Animation ani = AnimaUtils.createLeftToRightAnimationNew(parentView, (float) contentView.getLayoutParams().width);
        ani.setAnimationListener(new Animation.AnimationListener() {
            /* class com.clou.rs350.ui.activity.ErrorMeasurementCNActivity.AnonymousClass6 */

            public void onAnimationStart(Animation animation) {
                contentView.setVisibility(View.VISIBLE);
                ErrorMeasurementCNActivity.this.arrowRightView.setVisibility(View.GONE);
            }

            public void onAnimationRepeat(Animation animation) {
            }

            public void onAnimationEnd(Animation animation) {
            }
        });
        AnimaUtils.startAnimation(ani, parentView);
    }

    @Override // android.support.v4.app.FragmentActivity
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode != 4 || !this.hasStart) {
            return super.onKeyDown(keyCode, event);
        }
        return false;
    }
}
