package com.clou.rs350.ui.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.clou.rs350.R;
import com.clou.rs350.db.model.DataBaseRecordItem;
import com.clou.rs350.ui.activity.DataDetailActivity;
import com.itextpdf.text.pdf.PdfObject;

import java.util.ArrayList;
import java.util.List;

public class RecordsAdapter extends BaseAdapter {
    private Context m_Context;
    private String m_CustomerSr = PdfObject.NOTHING;
    private List<DataBaseRecordItem> m_Data = new ArrayList();
    View.OnClickListener m_OnClick;

    public RecordsAdapter(Context context, View.OnClickListener onClick) {
        this.m_Context = context;
        this.m_OnClick = onClick;
    }

    public int getCount() {
        if (this.m_Data != null) {
            return this.m_Data.size();
        }
        return 0;
    }

    public Object getItem(int position) {
        return this.m_Data.get(position);
    }

    public long getItemId(int position) {
        return 0;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = View.inflate(this.m_Context, R.layout.data_record_item, null);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        holder.showValue(position, this.m_Data.get(position));
        return convertView;
    }

    public void refreshData(String customerSr, List<DataBaseRecordItem> records) {
        this.m_CustomerSr = customerSr;
        this.m_Data = records;
        notifyDataSetChanged();
    }

    public void selectIndex(int index) {
        this.m_Data.get(index).setSelect(!this.m_Data.get(index).isSelect());
        notifyDataSetChanged();
    }

    public int isSelectAll() {
        int result = 0;
        for (int i = 0; i < this.m_Data.size(); i++) {
            result = !this.m_Data.get(i).isSelect() ? result - 1 : result + 1;
        }
        if (result == this.m_Data.size()) {
            return 1;
        }
        if (result == (-this.m_Data.size())) {
            return -1;
        }
        return 0;
    }

    public List<DataBaseRecordItem> getList() {
        return this.m_Data;
    }

    public void selectAll(boolean isChecked) {
        for (int i = 0; i < this.m_Data.size(); i++) {
            this.m_Data.get(i).setSelect(isChecked);
        }
        notifyDataSetChanged();
    }

    class ViewHolder {
        CheckBox cb;
        int m_Index;
        TextView m_TestItem;

        ViewHolder(View view) {
            this.cb = (CheckBox) view.findViewById(R.id.checkbox);
            this.cb.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                /* class com.clou.rs350.ui.adapter.RecordsAdapter.ViewHolder.AnonymousClass1 */

                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    System.out.println("oncheckedChanged : " + isChecked + " currentPosition: " + ViewHolder.this.m_Index);
                    ((DataBaseRecordItem) RecordsAdapter.this.m_Data.get(ViewHolder.this.m_Index)).setSelect(isChecked);
                    if (RecordsAdapter.this.m_OnClick != null) {
                        RecordsAdapter.this.m_OnClick.onClick(buttonView);
                    }
                }
            });
            this.m_TestItem = (TextView) view.findViewById(R.id.txt_testitem);
            view.findViewById(R.id.btn_detail).setOnClickListener(new View.OnClickListener() {
                /* class com.clou.rs350.ui.adapter.RecordsAdapter.ViewHolder.AnonymousClass2 */

                public void onClick(View v) {
                    Intent intent = new Intent(RecordsAdapter.this.m_Context, DataDetailActivity.class);
                    intent.putExtra("CustomerSerialNo", RecordsAdapter.this.m_CustomerSr);
                    intent.putExtra("TestTime", new String[]{((DataBaseRecordItem) RecordsAdapter.this.m_Data.get(ViewHolder.this.m_Index)).getDatetime()});
                    ((Activity) RecordsAdapter.this.m_Context).startActivityForResult(intent, 0);
                }
            });
        }

        /* access modifiers changed from: package-private */
        public void showValue(int index, DataBaseRecordItem data) {
            this.m_Index = index;
            this.cb.setText(data.getDatetime());
            this.cb.setChecked(data.isSelect());
            this.m_TestItem.setText(data.getTestItem());
        }
    }
}
