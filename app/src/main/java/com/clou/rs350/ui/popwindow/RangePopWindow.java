package com.clou.rs350.ui.popwindow;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.clou.rs350.Preferences;
import com.clou.rs350.R;
import com.clou.rs350.db.model.Range;
import com.clou.rs350.device.MeterBaseDevice;
import com.clou.rs350.device.model.ExplainedModel;
import com.clou.rs350.manager.MessageManager;
import com.itextpdf.text.pdf.PdfObject;

import java.util.ArrayList;
import java.util.List;

public class RangePopWindow extends PopupWindow implements View.OnClickListener {
    View ConnectTypeLayout;
    private Button btn_Cancel;
    private Button btn_OK;
    CheckBox chk_IAll;
    CheckBox chk_UAll;
    private LinearLayout ll_L1IRange;
    private LinearLayout ll_L1URange;
    private LinearLayout ll_L3IRange;
    private LinearLayout ll_L3URange;
    View.OnClickListener mOnClick;
    boolean m_AutoConnectType = true;
    private Range m_BasicValue;
    Context m_Context;
    private MessageManager m_MessageManager;
    private View parentView;
    private TextView tv_ConnectType;
    private TextView tv_L1IRange;
    private TextView tv_L1URange;
    private TextView tv_L2IRange;
    private TextView tv_L2IRangeLab;
    private TextView tv_L2URange;
    private TextView tv_L2URangeLab;
    private TextView tv_L3IRange;
    private TextView tv_L3URange;

    public RangePopWindow(Context context) {
        super(context);
        this.m_Context = context;
        this.m_MessageManager = MessageManager.getInstance(this.m_Context);
        initView();
    }

    private void initView() {
        int i;
        int i2 = 8;
        View conentView = ((LayoutInflater) this.m_Context.getSystemService("layout_inflater")).inflate(R.layout.pop_window_range, (ViewGroup) null);
        this.ConnectTypeLayout = conentView.findViewById(R.id.layout_connecttype);
        this.chk_UAll = (CheckBox) conentView.findViewById(R.id.chk_U_Range_All_);
        this.chk_IAll = (CheckBox) conentView.findViewById(R.id.chk_I_Range_All_);
        this.m_AutoConnectType = Preferences.getBoolean(Preferences.Key.AUTOCONNECTTYPE, true);
        this.ConnectTypeLayout.setVisibility(this.m_AutoConnectType ? View.GONE : View.VISIBLE);
        CheckBox checkBox = this.chk_UAll;
        if (this.m_AutoConnectType) {
            i = 8;
        } else {
            i = 0;
        }
        checkBox.setVisibility(i);
        CheckBox checkBox2 = this.chk_IAll;
        if (!this.m_AutoConnectType) {
            i2 = 0;
        }
        checkBox2.setVisibility(i2);
        this.tv_ConnectType = (TextView) conentView.findViewById(R.id.txt_ConnectType);
        this.tv_L1URange = (TextView) conentView.findViewById(R.id.txt_L1URange_);
        this.tv_L2URange = (TextView) conentView.findViewById(R.id.txt_L2URange_);
        this.tv_L3URange = (TextView) conentView.findViewById(R.id.txt_L3URange_);
        this.tv_L1IRange = (TextView) conentView.findViewById(R.id.txt_L1IRange_);
        this.tv_L2IRange = (TextView) conentView.findViewById(R.id.txt_L2IRange_);
        this.tv_L3IRange = (TextView) conentView.findViewById(R.id.txt_L3IRange_);
        this.tv_L2URangeLab = (TextView) conentView.findViewById(R.id.lab_L2URange_);
        this.tv_L2IRangeLab = (TextView) conentView.findViewById(R.id.lab_L2IRange_);
        this.ll_L1URange = (LinearLayout) conentView.findViewById(R.id.layout_L1URange_);
        this.ll_L3URange = (LinearLayout) conentView.findViewById(R.id.layout_L3URange_);
        this.ll_L1IRange = (LinearLayout) conentView.findViewById(R.id.layout_L1IRange_);
        this.ll_L3IRange = (LinearLayout) conentView.findViewById(R.id.layout_L3IRange_);
        this.btn_OK = (Button) conentView.findViewById(R.id.ok);
        this.btn_Cancel = (Button) conentView.findViewById(R.id.cancel);
        setContentView(conentView);
        setWidth(this.m_Context.getResources().getDimensionPixelSize(R.dimen.dp_350));
        setHeight(this.m_Context.getResources().getDimensionPixelSize(this.m_AutoConnectType ? R.dimen.dp_200 : R.dimen.dp_230));
        setFocusable(true);
        setOutsideTouchable(true);
        update();
        setBackgroundDrawable(new ColorDrawable(0));
        setAnimationStyle(16973826);
    }

    public void showUIRangeValue() {
        if (!this.m_BasicValue.ConnectionType.equals(PdfObject.NOTHING)) {
            this.tv_ConnectType.setText(this.m_BasicValue.ConnectionType);
            this.tv_ConnectType.setTag(getintConnectType(this.m_BasicValue.ConnectionTypeIndex));
            this.tv_L1URange.setText(this.m_BasicValue.VoltageRangeL1Index == -1 ? "Auto" : this.m_BasicValue.VoltageRangeL1);
            this.tv_L1URange.setTag(Integer.valueOf(this.m_BasicValue.VoltageRangeL1Index + 1));
            this.tv_L2URange.setText(this.m_BasicValue.VoltageRangeL2Index == -1 ? "Auto" : this.m_BasicValue.VoltageRangeL2);
            this.tv_L2URange.setTag(Integer.valueOf(this.m_BasicValue.VoltageRangeL2Index + 1));
            this.tv_L3URange.setText(this.m_BasicValue.VoltageRangeL3Index == -1 ? "Auto" : this.m_BasicValue.VoltageRangeL3);
            this.tv_L3URange.setTag(Integer.valueOf(this.m_BasicValue.VoltageRangeL3Index + 1));
            this.tv_L1IRange.setText(this.m_BasicValue.CurrentRangeL1Index == -1 ? "Auto" : this.m_BasicValue.CurrentRangeL1);
            this.tv_L1IRange.setTag(Integer.valueOf(this.m_BasicValue.CurrentRangeL1Index + 1));
            this.tv_L2IRange.setText(this.m_BasicValue.CurrentRangeL2Index == -1 ? "Auto" : this.m_BasicValue.CurrentRangeL2);
            this.tv_L2IRange.setTag(Integer.valueOf(this.m_BasicValue.CurrentRangeL2Index + 1));
            this.tv_L3IRange.setText(this.m_BasicValue.CurrentRangeL3Index == -1 ? "Auto" : this.m_BasicValue.CurrentRangeL3);
            this.tv_L3IRange.setTag(Integer.valueOf(this.m_BasicValue.CurrentRangeL3Index + 1));
            return;
        }
        int[] index = new int[6];
        this.tv_ConnectType.setText(this.m_MessageManager.getMeter().getS_ConnectType_All()[index[0]]);
        this.tv_ConnectType.setTag(index);
        fillDefaultValue(this.tv_ConnectType, this.tv_L1URange, this.tv_L2URange, this.tv_L3URange, this.tv_L1IRange, this.tv_L2IRange, this.tv_L3IRange);
    }

    private void fillDefaultValue(TextView tv_ConnectType2, TextView tv_L1URange2, TextView tv_L2URange2, TextView tv_L3URange2, TextView tv_L1IRange2, TextView tv_L2IRange2, TextView tv_L3IRange2) {
        tv_L1URange2.setText("Auto");
        tv_L1URange2.setTag(0);
        tv_L2URange2.setText("Auto");
        tv_L2URange2.setTag(0);
        tv_L3URange2.setText("Auto");
        tv_L3URange2.setTag(0);
        tv_L1IRange2.setText("Auto");
        tv_L1IRange2.setTag(0);
        tv_L2IRange2.setText("Auto");
        tv_L2IRange2.setTag(0);
        tv_L3IRange2.setText("Auto");
        tv_L3IRange2.setTag(0);
    }

    public void bundClickEvent(View.OnClickListener callback) {
        this.chk_UAll.setOnClickListener(this);
        this.chk_IAll.setOnClickListener(this);
        this.tv_L1URange.setOnClickListener(this);
        this.tv_L1URange.setTag(R.id.index_id, "U");
        this.tv_L1URange.setTag(R.id.phase_id, 0);
        this.tv_L2URange.setOnClickListener(this);
        this.tv_L2URange.setTag(R.id.index_id, "U");
        this.tv_L2URange.setTag(R.id.phase_id, 1);
        this.tv_L3URange.setOnClickListener(this);
        this.tv_L3URange.setTag(R.id.index_id, "U");
        this.tv_L3URange.setTag(R.id.phase_id, 2);
        this.tv_L1IRange.setOnClickListener(this);
        this.tv_L1IRange.setTag(R.id.index_id, "I");
        this.tv_L1IRange.setTag(R.id.phase_id, 0);
        this.tv_L2IRange.setOnClickListener(this);
        this.tv_L2IRange.setTag(R.id.index_id, "I");
        this.tv_L2IRange.setTag(R.id.phase_id, 1);
        this.tv_L3IRange.setOnClickListener(this);
        this.tv_L3IRange.setTag(R.id.index_id, "I");
        this.tv_L3IRange.setTag(R.id.phase_id, 2);
        this.tv_ConnectType.setOnClickListener(this);
        this.btn_Cancel.setOnClickListener(this);
        this.btn_OK.setOnClickListener(this);
    }

    public void showPopWindow(View currentClick, Range basicValue, View.OnClickListener onclick) {
        this.mOnClick = onclick;
        this.parentView = currentClick;
        this.m_BasicValue = basicValue;
        bundClickEvent(onclick);
        showUIRangeValue();
        if (!isShowing()) {
            int[] location = new int[2];
            currentClick.getLocationOnScreen(location);
            showAtLocation(currentClick, 0, location[0], location[1] + currentClick.getHeight());
            return;
        }
        dismiss();
    }

    public void onClick(View v) {
        String[] temp;
        switch (v.getId()) {
            case R.id.ok:
                SetRange();
                if (this.mOnClick != null) {
                    this.mOnClick.onClick(v);
                }
                if (isShowing()) {
                    dismiss();
                    return;
                }
                return;
            case R.id.cancel:
                if (isShowing()) {
                    dismiss();
                    return;
                }
                return;
            case R.id.txt_ConnectType:
                return;
            case R.id.chk_U_Range_All_:
                if (this.chk_UAll.isChecked()) {
                    this.ll_L1URange.setVisibility(View.INVISIBLE);
                    this.ll_L3URange.setVisibility(View.INVISIBLE);
                    this.tv_L2URangeLab.setText("L:");
                    return;
                }
                this.ll_L1URange.setVisibility(View.VISIBLE);
                this.ll_L3URange.setVisibility(View.VISIBLE);
                this.tv_L2URangeLab.setText("L2:");
                return;
            case R.id.chk_I_Range_All_:
                if (this.chk_IAll.isChecked()) {
                    this.ll_L1IRange.setVisibility(View.INVISIBLE);
                    this.ll_L3IRange.setVisibility(View.INVISIBLE);
                    this.tv_L2IRangeLab.setText("L:");
                    return;
                }
                this.ll_L1IRange.setVisibility(View.VISIBLE);
                this.ll_L3IRange.setVisibility(View.VISIBLE);
                this.tv_L2IRangeLab.setText("L2:");
                return;
            default:
                //new String[1][0] = PdfObject.NOTHING;
                String tag = v.getTag(R.id.index_id).toString().trim();
                if (this.m_AutoConnectType || this.tv_ConnectType.getTag() != null) {
                    int index = Integer.parseInt(v.getTag(R.id.phase_id).toString().trim());
                    MeterBaseDevice device = this.m_MessageManager.getMeter();
                    if ("U".equals(tag)) {
                        temp = device.getVoltageRange().get(this.m_BasicValue.ConnectionTypeIndex[index].getIntValue());
                    } else {
                        temp = device.getCurrentRange().get(this.m_BasicValue.ConnectionTypeIndex[index + 3].getIntValue());
                    }
                    List<String> tempList = new ArrayList<>();
                    tempList.add(0, "Auto");
                    for (String str : temp) {
                        tempList.add(str);
                    }
                    ListPopWindow.showListPopwindow(this.m_Context, (String[]) tempList.toArray(new String[0]), v, null, 0, true, this.parentView);
                    return;
                }
                return;
        }
    }

    private void SetRange() {
        MeterBaseDevice device = this.m_MessageManager.getMeter();
        int[] connectTypes = (int[]) this.tv_ConnectType.getTag();
        int[] values = new int[6];
        if (this.chk_UAll.isChecked()) {
            values[0] = Integer.parseInt(this.tv_L2URange.getTag().toString());
            values[2] = Integer.parseInt(this.tv_L2URange.getTag().toString());
            values[4] = Integer.parseInt(this.tv_L2URange.getTag().toString());
        } else {
            values[0] = Integer.parseInt(this.tv_L1URange.getTag().toString());
            values[2] = Integer.parseInt(this.tv_L2URange.getTag().toString());
            values[4] = Integer.parseInt(this.tv_L3URange.getTag().toString());
        }
        if (this.chk_IAll.isChecked()) {
            values[1] = Integer.parseInt(this.tv_L2IRange.getTag().toString());
            values[3] = Integer.parseInt(this.tv_L2IRange.getTag().toString());
            values[5] = Integer.parseInt(this.tv_L2IRange.getTag().toString());
        } else {
            values[1] = Integer.parseInt(this.tv_L1IRange.getTag().toString());
            values[3] = Integer.parseInt(this.tv_L2IRange.getTag().toString());
            values[5] = Integer.parseInt(this.tv_L3IRange.getTag().toString());
        }
        try {
            this.m_MessageManager.SetRange(this.m_AutoConnectType ? 0 : 1, connectTypes[3] + (connectTypes[4] << 8) + (connectTypes[5] << 16), getAuto(new int[]{values[0], values[1], values[2], values[3], values[4], values[5]}), (device.getVoltageRangeIndex()[connectTypes[0]] + values[0]) - 1, (device.getVoltageRangeIndex()[connectTypes[1]] + values[2]) - 1, (device.getVoltageRangeIndex()[connectTypes[2]] + values[4]) - 1, (device.getCurrentRangeIndex()[connectTypes[3]] + values[1]) - 1, (device.getCurrentRangeIndex()[connectTypes[4]] + values[3]) - 1, (device.getCurrentRangeIndex()[connectTypes[5]] + values[5]) - 1);
        } catch (Exception e) {
        }
    }

    public int getAuto(int[] values) {
        int i;
        int i2;
        int i3 = 0;
        int auto = 0 | (values[0] == 0 ? 0 : 1);
        if (values[1] == 0) {
            i = 0;
        } else {
            i = 2;
        }
        int auto2 = auto | i;
        if (values[2] == 0) {
            i2 = 0;
        } else {
            i2 = 4;
        }
        int auto3 = auto2 | i2 | (values[3] == 0 ? 0 : 8) | (values[4] == 0 ? 0 : 16);
        if (values[5] != 0) {
            i3 = 32;
        }
        return auto3 | i3;
    }

    private int[] getintConnectType(ExplainedModel[] ConnectTypes) {
        int[] tmpValue = new int[ConnectTypes.length];
        for (int i = 0; i < ConnectTypes.length; i++) {
            tmpValue[i] = ConnectTypes[i].getIntValue();
        }
        return tmpValue;
    }
}
