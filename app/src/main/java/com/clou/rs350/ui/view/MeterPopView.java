package com.clou.rs350.ui.view;

import android.view.View;
import android.widget.TextView;

import com.clou.rs350.R;
import com.clou.rs350.db.model.BasicMeasurement;
import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.view.annotation.ViewInject;

public class MeterPopView {
    @ViewInject(R.id.error_text_i1)
    private TextView error_text_i1;
    @ViewInject(R.id.error_text_i2)
    private TextView error_text_i2;
    @ViewInject(R.id.error_text_i3)
    private TextView error_text_i3;
    @ViewInject(R.id.error_text_p)
    private TextView error_text_p;
    @ViewInject(R.id.error_text_q)
    private TextView error_text_q;
    @ViewInject(R.id.error_text_s)
    private TextView error_text_s;
    @ViewInject(R.id.error_text_u1)
    private TextView error_text_u1;
    @ViewInject(R.id.error_text_u2)
    private TextView error_text_u2;
    @ViewInject(R.id.error_text_u3)
    private TextView error_text_u3;

    public MeterPopView(View view) {
        ViewUtils.inject(this, view);
    }

    public void showData(BasicMeasurement bment) {
        this.error_text_u1.setText(bment.VoltageValueL1Show.s_Value);
        this.error_text_u2.setText(bment.VoltageValueL2Show.s_Value);
        this.error_text_u3.setText(bment.VoltageValueL3Show.s_Value);
        this.error_text_i1.setText(bment.CurrentValueL1.s_Value);
        this.error_text_i2.setText(bment.CurrentValueL2.s_Value);
        this.error_text_i3.setText(bment.CurrentValueL3.s_Value);
        this.error_text_p.setText(bment.ActivePowerTotal.s_Value);
        this.error_text_q.setText(bment.ReactivePowerTotal.s_Value);
        this.error_text_s.setText(bment.ApparentPowerTotal.s_Value);
    }
}
