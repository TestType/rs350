package com.clou.rs350.ui.adapter;

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.clou.rs350.R;
import com.clou.rs350.db.model.sequence.SequenceItem;
import com.clou.rs350.ui.popwindow.ListPopWindow;

import java.util.ArrayList;
import java.util.List;

public class SequenceEditAdapter extends BaseAdapter {
    private Context m_Context;
    private List<SequenceItem> m_DataList;
    private int m_SelectIndex;
    private List<String> m_SequenceItemNames;

    public SequenceEditAdapter(List<SequenceItem> list, Context context) {
        this.m_DataList = list;
        this.m_Context = context;
        refreshNames();
    }

    public void setIndex(int index) {
        if (this.m_SelectIndex == index) {
            this.m_SelectIndex = -1;
        } else {
            this.m_SelectIndex = index;
        }
        notifyDataSetChanged();
    }

    public void moveDown() {
        if (-1 != this.m_SelectIndex) {
            SequenceItem tmpSequence = this.m_DataList.get(this.m_SelectIndex);
            if (this.m_SelectIndex + 1 < this.m_DataList.size()) {
                this.m_DataList.remove(this.m_SelectIndex);
                this.m_SelectIndex++;
                this.m_DataList.add(this.m_SelectIndex, tmpSequence);
                notifyDataSetChanged();
            }
        }
    }

    public void moveUp() {
        if (-1 != this.m_SelectIndex) {
            SequenceItem tmpSequence = this.m_DataList.get(this.m_SelectIndex);
            if (this.m_SelectIndex - 1 >= 1) {
                this.m_DataList.remove(this.m_SelectIndex);
                this.m_SelectIndex--;
                this.m_DataList.add(this.m_SelectIndex, tmpSequence);
                notifyDataSetChanged();
            }
        }
    }

    public int getCount() {
        if (this.m_DataList != null) {
            return this.m_DataList.size();
        }
        return 0;
    }

    public Object getItem(int position) {
        try {
            if (this.m_DataList != null) {
                return this.m_DataList.get(position);
            }
            return null;
        } catch (Exception e) {
            return null;
        }
    }

    public long getItemId(int position) {
        return 0;
    }

    public View getView(int position, View view, ViewGroup parent) {
        ViewHolder holder;
        if (view == null) {
            view = LayoutInflater.from(this.m_Context).inflate(R.layout.adapter_sequence_edit_item, (ViewGroup) null);
            holder = new ViewHolder(view);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }
        holder.show(position);
        return view;
    }

    public void refreshData(List<SequenceItem> list) {
        this.m_DataList = new ArrayList();
        this.m_DataList.clear();
        this.m_DataList.addAll(list);
        this.m_SelectIndex = -1;
        refreshNames();
        notifyDataSetChanged();
    }

    public void add() {
        if (this.m_DataList == null) {
            this.m_DataList = new ArrayList();
        }
        this.m_DataList.add(new SequenceItem());
        notifyDataSetChanged();
    }

    public List<SequenceItem> getDatas() {
        return this.m_DataList;
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private void refreshNames() {
        String[] tmpNames = this.m_Context.getResources().getStringArray(R.array.sequence_items);
        this.m_SequenceItemNames = new ArrayList();
        for (String str : tmpNames) {
            this.m_SequenceItemNames.add(str);
        }
        if (this.m_DataList != null) {
            for (int i = 0; i < this.m_DataList.size(); i++) {
                if (this.m_DataList.get(i).ItemFunction.equals(getString(R.string.text_customer_info)) || this.m_DataList.get(i).ItemFunction.equals(getString(R.string.text_site_data)) || this.m_DataList.get(i).ItemFunction.equals(getString(R.string.text_visual)) || this.m_DataList.get(i).ItemFunction.equals(getString(R.string.text_remark))) {
                    this.m_SequenceItemNames.remove(this.m_DataList.get(i).ItemFunction);
                }
            }
        }
    }

    private String getString(int id) {
        return this.m_Context.getResources().getString(id);
    }

    /* access modifiers changed from: package-private */
    public class ViewHolder {
        Button btn_Delete;
        CheckBox chk_Index;
        CheckBox chk_MustDo;
        int m_Index;
        TextView txt_Function;
        TextView txt_Name;

        ViewHolder(View view) {
            this.chk_Index = (CheckBox) view.findViewById(R.id.chk_index);
            this.chk_Index.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                /* class com.clou.rs350.ui.adapter.SequenceEditAdapter.ViewHolder.AnonymousClass1 */

                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    if (isChecked) {
                        SequenceEditAdapter.this.m_SelectIndex = ViewHolder.this.m_Index;
                    } else if (SequenceEditAdapter.this.m_SelectIndex == ViewHolder.this.m_Index) {
                        SequenceEditAdapter.this.m_SelectIndex = -1;
                    }
                    SequenceEditAdapter.this.notifyDataSetChanged();
                }
            });
            this.chk_MustDo = (CheckBox) view.findViewById(R.id.chk_must_do);
            this.chk_MustDo.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                /* class com.clou.rs350.ui.adapter.SequenceEditAdapter.ViewHolder.AnonymousClass2 */

                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    ((SequenceItem) SequenceEditAdapter.this.m_DataList.get(ViewHolder.this.m_Index)).MustDo = isChecked ? 1 : 0;
                }
            });
            this.txt_Name = (TextView) view.findViewById(R.id.txt_sequence_name);
            this.txt_Name.addTextChangedListener(new TextWatcher() {
                /* class com.clou.rs350.ui.adapter.SequenceEditAdapter.ViewHolder.AnonymousClass3 */

                public void onTextChanged(CharSequence s, int start, int before, int count) {
                }

                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                }

                public void afterTextChanged(Editable s) {
                    ((SequenceItem) SequenceEditAdapter.this.m_DataList.get(ViewHolder.this.m_Index)).ItemName = ViewHolder.this.txt_Name.getText().toString();
                }
            });
            this.txt_Function = (TextView) view.findViewById(R.id.txt_sequence_item);
            this.txt_Function.setOnClickListener(new View.OnClickListener() {
                /* class com.clou.rs350.ui.adapter.SequenceEditAdapter.ViewHolder.AnonymousClass4 */

                public void onClick(View v) {
                    ListPopWindow.showListPopwindow(SequenceEditAdapter.this.m_Context, (String[]) SequenceEditAdapter.this.m_SequenceItemNames.toArray(new String[SequenceEditAdapter.this.m_SequenceItemNames.size()]), v, new View.OnClickListener() {
                        /* class com.clou.rs350.ui.adapter.SequenceEditAdapter.ViewHolder.AnonymousClass4.AnonymousClass1 */

                        public void onClick(View v) {
                            ((SequenceItem) SequenceEditAdapter.this.m_DataList.get(ViewHolder.this.m_Index)).ItemFunction = ViewHolder.this.txt_Function.getText().toString();
                            SequenceEditAdapter.this.refreshNames();
                        }
                    });
                }
            });
            this.btn_Delete = (Button) view.findViewById(R.id.btn_delete);
            this.btn_Delete.setOnClickListener(new View.OnClickListener() {
                /* class com.clou.rs350.ui.adapter.SequenceEditAdapter.ViewHolder.AnonymousClass5 */

                public void onClick(View v) {
                    SequenceEditAdapter.this.m_DataList.remove(ViewHolder.this.m_Index);
                    SequenceEditAdapter.this.refreshNames();
                    SequenceEditAdapter.this.notifyDataSetChanged();
                }
            });
        }

        /* access modifiers changed from: package-private */
        public void show(int Index) {
            boolean z;
            boolean z2;
            boolean z3;
            boolean z4;
            boolean z5 = true;
            if (SequenceEditAdapter.this.m_DataList != null) {
                SequenceItem item = (SequenceItem) SequenceEditAdapter.this.m_DataList.get(Index);
                this.m_Index = Index;
                this.chk_Index.setText(new StringBuilder(String.valueOf(this.m_Index + 1)).toString());
                CheckBox checkBox = this.chk_Index;
                if (Index == SequenceEditAdapter.this.m_SelectIndex) {
                    z = true;
                } else {
                    z = false;
                }
                checkBox.setChecked(z);
                CheckBox checkBox2 = this.chk_MustDo;
                if (1 == item.MustDo) {
                    z2 = true;
                } else {
                    z2 = false;
                }
                checkBox2.setChecked(z2);
                this.txt_Name.setText(item.ItemName);
                this.txt_Function.setText(item.ItemFunction);
                CheckBox checkBox3 = this.chk_Index;
                if (Index != 0) {
                    z3 = true;
                } else {
                    z3 = false;
                }
                checkBox3.setEnabled(z3);
                TextView textView = this.txt_Function;
                if (Index != 0) {
                    z4 = true;
                } else {
                    z4 = false;
                }
                textView.setEnabled(z4);
                Button button = this.btn_Delete;
                if (Index == 0) {
                    z5 = false;
                }
                button.setEnabled(z5);
            }
        }
    }
}
