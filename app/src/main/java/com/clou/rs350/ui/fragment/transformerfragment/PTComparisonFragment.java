package com.clou.rs350.ui.fragment.transformerfragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.clou.rs350.R;
import com.clou.rs350.db.model.PTComparison;
import com.clou.rs350.device.MeterBaseDevice;
import com.clou.rs350.manager.MessageManager;
import com.clou.rs350.model.ClouData;
import com.clou.rs350.task.ISleepCallback;
import com.clou.rs350.task.TimerThread;
import com.clou.rs350.ui.fragment.BaseFragment;
import com.clou.rs350.ui.popwindow.PTComparisonPopWindow;
import com.clou.rs350.ui.popwindow.PTConnectionPopWindow;
import com.clou.rs350.utils.OtherUtils;
import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.view.annotation.ViewInject;
import com.lidroid.xutils.view.annotation.event.OnClick;

public class PTComparisonFragment extends BaseFragment implements View.OnClickListener {
    @ViewInject(R.id.txt_connect_state)
    private TextView ConnectStateTv;
    @ViewInject(R.id.l1_delta_angle)
    private TextView Loca1L1AngleTv;
    @ViewInject(R.id.l1_error)
    private TextView Loca1L1ErrorTv;
    @ViewInject(R.id.loca1_l1_phase_angle)
    private TextView Loca1L1PhaseAngleTV;
    @ViewInject(R.id.loca1_l1_primary_voltage)
    private TextView Loca1L1PrimaryVoltageTV;
    @ViewInject(R.id.l1_delta_voltage)
    private TextView Loca1L1Voltage;
    @ViewInject(R.id.l2_delta_angle)
    private TextView Loca1L2AngleTv;
    @ViewInject(R.id.l2_error)
    private TextView Loca1L2ErrorTv;
    @ViewInject(R.id.loca1_l2_phase_angle)
    private TextView Loca1L2PhaseAngleTV;
    @ViewInject(R.id.loca1_l2_primary_voltage)
    private TextView Loca1L2PrimaryVoltageTV;
    @ViewInject(R.id.l2_delta_voltage)
    private TextView Loca1L2Voltage;
    @ViewInject(R.id.l3_delta_angle)
    private TextView Loca1L3AngleTv;
    @ViewInject(R.id.l3_error)
    private TextView Loca1L3ErrorTv;
    @ViewInject(R.id.loca1_l3_phase_angle)
    private TextView Loca1L3PhaseAngleTV;
    @ViewInject(R.id.loca1_l3_primary_voltage)
    private TextView Loca1L3PrimaryVoltageTV;
    @ViewInject(R.id.l3_delta_voltage)
    private TextView Loca1L3Voltage;
    @ViewInject(R.id.loca2_l1_phase_angle)
    private TextView Loca2L1PhaseAngleTV;
    @ViewInject(R.id.loca2_l1_primary_voltage)
    private TextView Loca2L1PrimaryVoltageTV;
    @ViewInject(R.id.loca2_l2_phase_angle)
    private TextView Loca2L2PhaseAngleTV;
    @ViewInject(R.id.loca2_l2_primary_voltage)
    private TextView Loca2L2PrimaryVoltageTV;
    @ViewInject(R.id.loca2_l3_phase_angle)
    private TextView Loca2L3PhaseAngleTV;
    @ViewInject(R.id.loca2_l3_primary_voltage)
    private TextView Loca2L3PrimaryVoltageTV;
    @ViewInject(R.id.txt_location)
    private TextView LocationTv;
    @ViewInject(R.id.btn_start)
    private Button StartBtn;
    @ViewInject(R.id.txt_time)
    private TextView TimeTv;
    private TimerThread deviceStateThread;
    private boolean hasStart = false;
    private int[] m_ChannelFlags;
    PTComparison m_PTComparison;

    public PTComparisonFragment(PTComparison Data) {
        int[] iArr = new int[3];
        iArr[1] = 1;
        this.m_ChannelFlags = iArr;
        this.deviceStateThread = null;
        this.m_PTComparison = Data;
    }

    @Override // android.support.v4.app.Fragment
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        this.fragmentView = inflater.inflate(R.layout.fragment_pt_comparison, (ViewGroup) null);
        ViewUtils.inject(this, this.fragmentView);
        return this.fragmentView;
    }

    @Override // android.support.v4.app.Fragment, com.clou.rs350.ui.fragment.BaseFragment
    public void onResume() {
        super.onResume();
        initData();
        refreshData();
    }

    private void initData() {
        this.messageManager = MessageManager.getInstance(this.m_Context);
        this.messageManager.setFunc_Mstate(64);
        readPTDeviceState();
    }

    public void readPTDeviceState() {
        if (this.deviceStateThread == null || this.deviceStateThread.isStop()) {
            this.deviceStateThread = new TimerThread((long) 1000, new ISleepCallback() {
                /* class com.clou.rs350.ui.fragment.transformerfragment.PTComparisonFragment.AnonymousClass1 */

                @Override // com.clou.rs350.task.ISleepCallback
                public void onSleepAfterToDo() {
                    if (PTComparisonFragment.this.hasStart) {
                        PTComparisonFragment.this.messageManager.getPTValue();
                    } else {
                        PTComparisonFragment.this.messageManager.getPTDeviceState();
                    }
                    PTComparisonFragment.this.messageManager.getStartState();
                }
            }, "isConnectedThread");
            this.deviceStateThread.start();
        }
    }

    @OnClick({R.id.btn_configure, R.id.btn_connect_configure, R.id.btn_start})
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_start:
                if (this.hasStart) {
                    stop();
                    return;
                } else {
                    start();
                    return;
                }
            case R.id.btn_configure:
                new PTComparisonPopWindow(this.m_Context).showPopWindow(v, ClouData.getInstance().getMeterBaseInfo()[0], new View.OnClickListener() {
                    /* class com.clou.rs350.ui.fragment.transformerfragment.PTComparisonFragment.AnonymousClass2 */

                    public void onClick(View v) {
                        double[] values = (double[]) v.getTag();
                        PTComparisonFragment.this.messageManager.SetRatio(values[0], values[1], ClouData.getInstance().getSiteData().RatioApply);
                    }
                });
                return;
            case R.id.btn_connect_configure:
                new PTConnectionPopWindow(this.m_Context).showPopWindow(v, this.m_ChannelFlags);
                return;
            default:
                return;
        }
    }

    @Override // com.clou.rs350.ui.fragment.BaseFragment, com.clou.rs350.manager.MessageManager.IMessageListener
    public void onReceiveMessage(MeterBaseDevice Device) {
        boolean z = true;
        super.onReceiveMessage(Device);
        getFlag(Device);
        boolean z2 = this.hasStart;
        if (1 != Device.getStartStateValue()) {
            z = false;
        }
        if (z ^ z2) {
            if (this.hasStart) {
                stop();
            } else {
                start();
            }
        }
        if (this.hasStart) {
            this.m_PTComparison.parseData(Device);
            refreshData();
        }
    }

    @Override // com.clou.rs350.ui.fragment.BaseFragment
    public void refreshData() {
        this.TimeTv.setText(this.m_PTComparison.Time);
        this.Loca1L1PrimaryVoltageTV.setText(this.m_PTComparison.Location1Voltage[0].s_Value);
        this.Loca1L2PrimaryVoltageTV.setText(this.m_PTComparison.Location1Voltage[1].s_Value);
        this.Loca1L3PrimaryVoltageTV.setText(this.m_PTComparison.Location1Voltage[2].s_Value);
        this.Loca1L1PhaseAngleTV.setText(this.m_PTComparison.Location1Angle[0].s_Value);
        this.Loca1L2PhaseAngleTV.setText(this.m_PTComparison.Location1Angle[1].s_Value);
        this.Loca1L3PhaseAngleTV.setText(this.m_PTComparison.Location1Angle[2].s_Value);
        this.Loca2L1PrimaryVoltageTV.setText(this.m_PTComparison.Location2Voltage[0].s_Value);
        this.Loca2L2PrimaryVoltageTV.setText(this.m_PTComparison.Location2Voltage[1].s_Value);
        this.Loca2L3PrimaryVoltageTV.setText(this.m_PTComparison.Location2Voltage[2].s_Value);
        this.Loca2L1PhaseAngleTV.setText(this.m_PTComparison.Location2Angle[0].s_Value);
        this.Loca2L2PhaseAngleTV.setText(this.m_PTComparison.Location2Angle[1].s_Value);
        this.Loca2L3PhaseAngleTV.setText(this.m_PTComparison.Location2Angle[2].s_Value);
        this.Loca1L1Voltage.setText(this.m_PTComparison.RatioError[0].s_Value);
        this.Loca1L2Voltage.setText(this.m_PTComparison.RatioError[1].s_Value);
        this.Loca1L3Voltage.setText(this.m_PTComparison.RatioError[2].s_Value);
        this.Loca1L1AngleTv.setText(this.m_PTComparison.AngleError[0].s_Value);
        this.Loca1L2AngleTv.setText(this.m_PTComparison.AngleError[1].s_Value);
        this.Loca1L3AngleTv.setText(this.m_PTComparison.AngleError[2].s_Value);
        this.Loca1L1ErrorTv.setText(this.m_PTComparison.VoltageDifferent[0].s_Value);
        this.Loca1L2ErrorTv.setText(this.m_PTComparison.VoltageDifferent[1].s_Value);
        this.Loca1L3ErrorTv.setText(this.m_PTComparison.VoltageDifferent[2].s_Value);
    }

    private void setButton(int flag) {
        this.StartBtn.setText(getString(flag == 0 ? R.string.text_start : R.string.text_stop));
    }

    private void start() {
        this.hasStart = true;
        this.messageManager.setStart(1);
        setButton(1);
    }

    private void stop() {
        this.hasStart = false;
        this.messageManager.setStart(0);
        setButton(0);
    }

    private void getFlag(MeterBaseDevice Device) {
        int ConnectionState;
        boolean z = true;
        this.m_ChannelFlags = Device.getPTConnectStateValue();
        this.LocationTv.setText(getActivity().getResources().getStringArray(R.array.location)[this.m_ChannelFlags[0]]);
        if (1 == this.m_ChannelFlags[0]) {
            this.StartBtn.setVisibility(0);
        } else {
            this.StartBtn.setVisibility(4);
        }
        if (this.m_ChannelFlags[2] == 1) {
            ConnectionState = 0;
        } else {
            ConnectionState = 1;
        }
        this.ConnectStateTv.setText(getActivity().getResources().getStringArray(R.array.connectstate)[ConnectionState]);
        this.ConnectStateTv.setTextColor(OtherUtils.getColor(ConnectionState));
        Button button = this.StartBtn;
        if (ConnectionState != 0) {
            z = false;
        }
        button.setEnabled(z);
    }

    @Override // android.support.v4.app.Fragment, com.clou.rs350.ui.fragment.BaseFragment
    public void onPause() {
        if (this.deviceStateThread != null && !this.deviceStateThread.isStop()) {
            this.deviceStateThread.stopTimer();
            this.deviceStateThread.interrupt();
            this.deviceStateThread = null;
        }
        super.onPause();
    }
}
