package com.clou.rs350.ui.activity;

import android.content.DialogInterface;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.viewpager.widget.ViewPager;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.clou.rs350.R;
import com.clou.rs350.callback.PasswordCallback;
import com.clou.rs350.constants.ConstExtras;
import com.clou.rs350.handler.HandlerSwitchView;
import com.clou.rs350.model.ClouData;
import com.clou.rs350.ui.adapter.FragmentTabAdapter;
import com.clou.rs350.ui.dialog.PasswordDialog;
import com.clou.rs350.ui.fragment.BaseFragment;
import com.clou.rs350.ui.fragment.settingfragment.AboutFragment;
import com.clou.rs350.ui.fragment.settingfragment.BasicSettingFragment;
import com.clou.rs350.ui.fragment.settingfragment.BluetoothFragment;
import com.clou.rs350.ui.fragment.settingfragment.CalibrationFragment;
import com.clou.rs350.ui.fragment.settingfragment.ConstantsFragment;
import com.clou.rs350.ui.fragment.settingfragment.FunctionFragment;
import com.clou.rs350.ui.fragment.settingfragment.StyleFragment;
import com.clou.rs350.version.IUpdateNewVersionCallback;
import com.clou.rs350.version.UpdateUtils;
import com.clou.rs350.version.UpdateVersionManager;
import com.clou.rs350.version.VersionInfo;
import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.view.annotation.ViewInject;
import com.lidroid.xutils.view.annotation.event.OnClick;

import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

public class SystemSettingActivity extends BaseActivity implements View.OnClickListener, HandlerSwitchView.IOnSelectCallback {
    private final String[] HelpString = {"#SettingBasic", "#SettingConstant", "#SettingBluetooth", "#SettingCalibration", "#SettingStyle", "#SettingAbout"};
    @ViewInject(R.id.ss_function)
    private TextView bottomFunction;
    @ViewInject(R.id.setting_normal_linearlayout)
    private LinearLayout bottomViewNormalLinearlayout;
    private IUpdateNewVersionCallback callback = new IUpdateNewVersionCallback() {
        /* class com.clou.rs350.ui.activity.SystemSettingActivity.AnonymousClass1 */

        @Override // com.clou.rs350.version.IUpdateNewVersionCallback
        public void onNewVersionCallback(VersionInfo newVersion) {
            SystemSettingActivity.this.img_system_update.setVisibility(UpdateUtils.isNewVersionExist(newVersion, SystemSettingActivity.this.m_Context) ? View.VISIBLE : View.GONE);
        }
    };
    private int currentIndex = 0;
    private int defaultIndex = 0;
    private final int functionIndex = 6;
    @ViewInject(R.id.red_point_update)
    private ImageView img_system_update;
    private int isGoFunction;
    private HandlerSwitchView switchView;
    @ViewInject(R.id.title_textview)
    private TextView titleTextView;
    private UpdateVersionManager updateVersionManager;

    /* access modifiers changed from: protected */
    @Override // com.clou.rs350.ui.activity.BaseActivity, android.support.v4.app.FragmentActivity
    public void onCreate(Bundle arg0) {
        super.onCreate(arg0);
        setContentView(R.layout.activity_system_setting);
        setDefaultTitle(R.string.text_setting_basic);
        ViewUtils.inject(this);
        initData();
        initView();
        initViewPager();
        checkNewVersion();
    }

    /* access modifiers changed from: protected */
    @Override // com.clou.rs350.ui.activity.BaseActivity, android.support.v4.app.FragmentActivity
    public void onResume() {
        this.messageManager.getConstant();
        super.onResume();
    }

    private void checkNewVersion() {
        this.updateVersionManager = UpdateVersionManager.getInstance(this);
        this.updateVersionManager.getNewVersion(this.callback);
    }

    private void initData() {
        this.defaultIndex = getIntent().getIntExtra(ConstExtras.SETTING_TAG, 0);
    }

    private void initView() {
        List<View> tabViews = new ArrayList<>();
        tabViews.add(findViewById(R.id.ss_basic));
        tabViews.add(findViewById(R.id.ss_constant));
        tabViews.add(findViewById(R.id.ss_bluetooth));
        tabViews.add(findViewById(R.id.ss_calibration));
        tabViews.add(findViewById(R.id.ss_style));
        tabViews.add(findViewById(R.id.ss_function));
        tabViews.add(findViewById(R.id.ss_about));
        this.switchView = new HandlerSwitchView(this.m_Context);
        this.switchView.setTabViews(tabViews);
        this.switchView.boundClick();
        this.switchView.setOnTabChangedCallback(this);
    }

    private void initViewPager() {
        ArrayList<BaseFragment> fragments = new ArrayList<>();
        fragments.add(new BasicSettingFragment());
        fragments.add(new ConstantsFragment());
        fragments.add(new BluetoothFragment());
        fragments.add(new CalibrationFragment());
        fragments.add(new StyleFragment());
        fragments.add(new FunctionFragment());
        fragments.add(new AboutFragment());
        this.m_TabAdapter = new FragmentTabAdapter(this, fragments, R.id.content_view);
        this.m_TabAdapter.init();
        if (this.defaultIndex != 0) {
            this.m_TabAdapter.checkedIndex(this.defaultIndex);
        }
        this.switchView.selectView(this.defaultIndex);
    }

    @OnClick({R.id.ss_back, R.id.ss_style})
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.ss_back:
                finish();
                return;
            default:
                return;
        }
    }

    class MyFragmentPagerAdapter extends FragmentStatePagerAdapter {
        private ArrayList<Fragment> fragmentsList;

        public MyFragmentPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        public MyFragmentPagerAdapter(FragmentManager fm, ArrayList<Fragment> fragments) {
            super(fm);
            this.fragmentsList = fragments;
        }

        @Override // android.support.v4.view.PagerAdapter
        public int getCount() {
            return this.fragmentsList.size();
        }

        @Override // android.support.v4.app.FragmentStatePagerAdapter
        public Fragment getItem(int arg0) {
            return this.fragmentsList.get(arg0);
        }

        @Override // android.support.v4.view.PagerAdapter
        public int getItemPosition(Object object) {
            return super.getItemPosition(object);
        }
    }

    public class MyOnPageChangeListener implements ViewPager.OnPageChangeListener {
        public MyOnPageChangeListener() {
        }

        @Override // android.support.v4.view.ViewPager.OnPageChangeListener
        public void onPageSelected(int arg0) {
            switch (arg0) {
                case 0:
                    SystemSettingActivity.this.currentIndex = 0;
                    SystemSettingActivity.this.setDefaultTitle(R.string.text_setting_basic);
                    return;
                case 1:
                    SystemSettingActivity.this.currentIndex = 1;
                    SystemSettingActivity.this.setDefaultTitle(R.string.text_setting_constant);
                    return;
                case 2:
                    SystemSettingActivity.this.currentIndex = 2;
                    SystemSettingActivity.this.setDefaultTitle(R.string.text_setting_bluetooth);
                    return;
                case 3:
                    SystemSettingActivity.this.currentIndex = 3;
                    SystemSettingActivity.this.setDefaultTitle(R.string.text_setting_adjustment);
                    return;
                case 4:
                    SystemSettingActivity.this.currentIndex = 4;
                    SystemSettingActivity.this.setDefaultTitle(R.string.text_setting_style);
                    return;
                case 5:
                    SystemSettingActivity.this.currentIndex = 5;
                    SystemSettingActivity.this.setDefaultTitle(R.string.text_setting_function);
                    return;
                case 6:
                    SystemSettingActivity.this.currentIndex = 6;
                    SystemSettingActivity.this.setDefaultTitle(R.string.text_setting_about);
                    return;
                default:
                    return;
            }
        }

        @Override // android.support.v4.view.ViewPager.OnPageChangeListener
        public void onPageScrolled(int arg0, float arg1, int arg2) {
        }

        @Override // android.support.v4.view.ViewPager.OnPageChangeListener
        public void onPageScrollStateChanged(int arg0) {
        }
    }

    /* access modifiers changed from: protected */
    @Override // com.clou.rs350.ui.activity.BaseActivity, android.support.v4.app.FragmentActivity
    public void onDestroy() {
        super.onDestroy();
        this.updateVersionManager.removeCallback(this.callback);
    }

    @Override // com.clou.rs350.handler.HandlerSwitchView.IOnSelectCallback
    public void onSelectedChanged(final int index) {
        if (index != 3 || 101 == ClouData.getInstance().getOperatorInfo().Authority) {
            this.m_TabAdapter.checkedIndex(index);
        } else if (this.messageManager.isConnected()) {
            String sPassword = new SimpleDateFormat("yyyyMMdd").format(new Date(System.currentTimeMillis())).toString();
            this.messageManager.GetAllowAdjustFlag();
            PasswordDialog passwordDialog = new PasswordDialog(this, sPassword, new PasswordCallback() {
                /* class com.clou.rs350.ui.activity.SystemSettingActivity.AnonymousClass2 */

                @Override // com.clou.rs350.callback.PasswordCallback
                public void onPasswordIsMistake() {
                }

                @Override // com.clou.rs350.callback.PasswordCallback
                public void onPasswordIsCorrect() {
                    if (SystemSettingActivity.this.messageManager.getMeter().getAllowAdjustFlagValue(SystemSettingActivity.this.m_Context)) {
                        SystemSettingActivity.this.m_TabAdapter.checkedIndex(index);
                        SystemSettingActivity.this.messageManager.CleanAllowAdjustFlag();
                        return;
                    }
                    SystemSettingActivity.this.switchView.selectView(SystemSettingActivity.this.m_TabAdapter.getCurrentTab());
                    SystemSettingActivity.this.showHint(R.string.text_press_adjust_button);
                }
            });
            passwordDialog.setOnDismiss(new DialogInterface.OnDismissListener() {
                /* class com.clou.rs350.ui.activity.SystemSettingActivity.AnonymousClass3 */

                public void onDismiss(DialogInterface dialog) {
                    SystemSettingActivity.this.switchView.selectView(SystemSettingActivity.this.m_TabAdapter.getCurrentTab());
                }
            });
            passwordDialog.show();
        } else {
            showHint(R.string.text_you_need_connect_device);
            this.switchView.selectView(this.m_TabAdapter.getCurrentTab());
        }
    }

    @Override // com.clou.rs350.handler.HandlerSwitchView.IOnSelectCallback
    public void onClickView(View view) {
    }
}
