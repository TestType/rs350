package com.clou.rs350.ui.adapter;

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.clou.rs350.R;
import com.clou.rs350.db.model.sequence.RegisterReadingItem;
import com.itextpdf.text.pdf.PdfObject;

import java.util.List;

public class RegisterItemAdapter extends BaseAdapter {
    private boolean isShow;
    private Context m_Context;
    List<RegisterReadingItem> m_Data;
    private int m_MustDo;

    public RegisterItemAdapter(Context context, int flag) {
        this.m_Context = context;
        this.m_MustDo = flag;
    }

    public void setIsShow(boolean flag) {
        this.isShow = flag;
    }

    public void refresh(List<RegisterReadingItem> data) {
        this.m_Data = data;
        notifyDataSetChanged();
    }

    public int getCount() {
        if (this.m_Data != null) {
            return this.m_Data.size();
        }
        return 0;
    }

    public Object getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return 0;
    }

    public List<RegisterReadingItem> getData() {
        return this.m_Data;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = View.inflate(this.m_Context, R.layout.adapter_register_reading_item, null);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        holder.showValue(position, this.m_Data.get(position));
        return convertView;
    }

    class ViewHolder {
        int m_Index;
        TextView txt_Index;
        TextView txt_Register;
        TextView txt_RegisterValue;

        ViewHolder(View v) {
            this.txt_Index = (TextView) v.findViewById(R.id.txt_index);
            this.txt_Register = (TextView) v.findViewById(R.id.txt_seal_location);
            this.txt_RegisterValue = (TextView) v.findViewById(R.id.txt_serial_number);
            this.txt_RegisterValue.addTextChangedListener(new TextWatcher() {
                /* class com.clou.rs350.ui.adapter.RegisterItemAdapter.ViewHolder.AnonymousClass1 */

                public void onTextChanged(CharSequence s, int start, int before, int count) {
                }

                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                }

                public void afterTextChanged(Editable s) {
                    RegisterItemAdapter.this.m_Data.get(ViewHolder.this.m_Index).RegisterValue = ViewHolder.this.txt_RegisterValue.getText().toString();
                }
            });
            if (RegisterItemAdapter.this.isShow) {
                this.txt_RegisterValue.setBackgroundResource(R.drawable.tableshowtext);
                this.txt_RegisterValue.setTextColor(RegisterItemAdapter.this.m_Context.getResources().getColor(R.color.showtextcolor));
                this.txt_RegisterValue.setEnabled(false);
                this.txt_Register.setBackgroundResource(R.drawable.tableshowtext);
                this.txt_Register.setTextColor(RegisterItemAdapter.this.m_Context.getResources().getColor(R.color.showtextcolor));
                this.txt_Register.setEnabled(false);
            }
        }

        /* access modifiers changed from: package-private */
        public void showValue(int index, RegisterReadingItem data) {
            this.m_Index = index;
            this.txt_Register.setText(data.Register);
            this.txt_RegisterValue.setText(data.RegisterValue);
            if (1 == data.RequiredField && 1 == RegisterItemAdapter.this.m_MustDo) {
                this.txt_Index.setText(String.valueOf(index + 1) + " *");
                this.txt_RegisterValue.setHint(R.string.text_required_field);
                return;
            }
            this.txt_Index.setText(new StringBuilder(String.valueOf(index + 1)).toString());
            this.txt_RegisterValue.setHint(PdfObject.NOTHING);
        }
    }
}
