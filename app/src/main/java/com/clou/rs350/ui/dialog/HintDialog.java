package com.clou.rs350.ui.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.clou.rs350.R;

public class HintDialog extends Dialog {
    Button btn_Ok = null;
    Context context = null;
    private OnDismissListener dismissListener = null;
    private String m_Title = null;
    private int m_TitleId = 0;
    private View.OnClickListener okClick = null;
    TextView txt_Title = null;

    public HintDialog(Context context2) {
        super(context2, R.style.dialog);
        requestWindowFeature(1);
        setCanceledOnTouchOutside(true);
        this.context = context2;
    }

    public HintDialog(Context context2, int txt_Id) {
        super(context2, R.style.dialog);
        requestWindowFeature(1);
        setCanceledOnTouchOutside(true);
        this.context = context2;
        this.m_TitleId = txt_Id;
    }

    public HintDialog(Context context2, String txt_String) {
        super(context2, R.style.dialog);
        requestWindowFeature(1);
        setCanceledOnTouchOutside(true);
        this.context = context2;
        this.m_Title = txt_String;
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_hint);
        this.txt_Title = (TextView) findViewById(R.id.txt_title);
        this.txt_Title.setMovementMethod(ScrollingMovementMethod.getInstance());
        if (this.m_TitleId != 0) {
            this.txt_Title.setText(this.m_TitleId);
        } else if (this.m_Title != null) {
            this.txt_Title.setText(this.m_Title);
        }
        this.btn_Ok = (Button) findViewById(R.id.btn_ok);
        this.btn_Ok.setOnClickListener(new View.OnClickListener() {
            /* class com.clou.rs350.ui.dialog.HintDialog.AnonymousClass1 */

            public void onClick(View v) {
                if (HintDialog.this.okClick != null) {
                    HintDialog.this.okClick.onClick(v);
                }
                HintDialog.this.dismiss();
            }
        });
    }

    public void dismiss() {
        if (this.dismissListener != null) {
            this.dismissListener.onDismiss(this);
        }
        super.dismiss();
    }

    @Override // android.app.Dialog
    public void setTitle(int id) {
        this.txt_Title.setText(this.context.getResources().getText(id));
    }

    @Override // android.app.Dialog
    public void setTitle(CharSequence title) {
        this.txt_Title.setText(title.toString());
    }

    public void setOnClick(View.OnClickListener okClick2) {
        this.okClick = okClick2;
    }

    public void setOnDismiss(OnDismissListener listener) {
        this.dismissListener = listener;
    }
}
