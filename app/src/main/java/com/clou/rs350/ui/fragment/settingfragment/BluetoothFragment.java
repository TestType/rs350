package com.clou.rs350.ui.fragment.settingfragment;

import android.bluetooth.BluetoothDevice;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.clou.rs350.Preferences;
import com.clou.rs350.R;
import com.clou.rs350.connect.bluetooth.manager.BluetoothSearch;
import com.clou.rs350.handler.HandlerSwitchView;
import com.clou.rs350.model.ClouData;
import com.clou.rs350.ui.adapter.BluetoothListAdapter;
import com.clou.rs350.ui.fragment.BaseFragment;
import com.itextpdf.text.pdf.PdfObject;
import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.view.annotation.ViewInject;
import com.lidroid.xutils.view.annotation.event.OnClick;

import java.util.ArrayList;
import java.util.List;

public class BluetoothFragment extends BaseFragment implements View.OnClickListener, AdapterView.OnItemClickListener, HandlerSwitchView.IOnSelectCallback {
    @ViewInject(R.id.tvm_mac)
    private TextView bluetoothMacTv;
    @ViewInject(R.id.tvn_name)
    private TextView bluetoothNameTv;
    @ViewInject(R.id.mylist_blue)
    public ListView deviceListView;
    private boolean hasRefreshList = false;
    private BluetoothSearch.IBlutoothListener listener = new BluetoothSearch.IBlutoothListener() {
        /* class com.clou.rs350.ui.fragment.settingfragment.BluetoothFragment.AnonymousClass1 */

        @Override // com.clou.rs350.connect.bluetooth.manager.BluetoothSearch.IBlutoothListener
        public void onRefreshDeviceList(final List<BluetoothDevice> deviceList) {
            BluetoothFragment.this.m_Context.runOnUiThread(new Runnable() {
                /* class com.clou.rs350.ui.fragment.settingfragment.BluetoothFragment.AnonymousClass1.AnonymousClass1 */

                public void run() {
                    BluetoothFragment.this.mAdapter.refreshData(deviceList);
                }
            });
        }
    };
    private BluetoothListAdapter mAdapter;
    private BluetoothSearch manager;
    @ViewInject(R.id.bluetooth_refresh_view)
    private View refreshTextView;
    private HandlerSwitchView switchView;

    @Override // android.support.v4.app.Fragment, com.clou.rs350.ui.fragment.BaseFragment
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        this.manager = BluetoothSearch.getInstance(this.m_Context);
        initView();
        showSavedDevice();
        this.mAdapter = new BluetoothListAdapter(this.manager.getDeviceList(), this.m_Context);
        this.deviceListView.setAdapter((ListAdapter) this.mAdapter);
        this.deviceListView.setOnItemClickListener(this);
        this.manager.addBlueListener(this.listener);
    }

    private void initView() {
        List<View> tabViews = new ArrayList<>();
        tabViews.add(findViewById(R.id.btn_device));
        tabViews.add(findViewById(R.id.btn_printer));
        this.switchView = new HandlerSwitchView(this.m_Context);
        this.switchView.setTabViews(tabViews);
        this.switchView.boundClick();
        this.switchView.setOnTabChangedCallback(this);
        this.switchView.selectView(0);
    }

    @Override // android.support.v4.app.Fragment
    public void onDestroyView() {
        this.manager.removeBlueListener(this.listener);
        ClouData.getInstance().setCommunicateType(0);
        this.manager.setMac(Preferences.getString(Preferences.Key.BLUETOOTH_MAC));
        if (this.hasRefreshList) {
            this.manager.reSearchDeviceList("手动蓝牙列表-重新搜索");
        }
        super.onDestroyView();
    }

    @Override // android.support.v4.app.Fragment, com.clou.rs350.ui.fragment.BaseFragment
    public void onResume() {
        super.onResume();
        if (this.mAdapter != null) {
            this.mAdapter.notifyDataSetChanged();
        }
    }

    private void showSavedDevice() {
        String name = Preferences.getString(Preferences.Key.BLUETOOTH_NAME);
        String mac = Preferences.getString(Preferences.Key.BLUETOOTH_MAC);
        if (1 == ClouData.getInstance().getCommunicateType()) {
            name = Preferences.getString(Preferences.Key.PRINTER_BLUETOOTH_NAME);
            mac = Preferences.getString(Preferences.Key.PRINTER_BLUETOOTH_MAC);
        }
        this.bluetoothNameTv.setText(name);
        this.bluetoothMacTv.setText(mac);
        this.manager.setMac(mac);
    }

    private void saveDeviceInfo(BluetoothDevice device) {
        if (ClouData.getInstance().getCommunicateType() == 0) {
            Preferences.putString(Preferences.Key.BLUETOOTH_NAME, device.getName());
            Preferences.putString(Preferences.Key.BLUETOOTH_MAC, device.getAddress());
        } else {
            Preferences.putString(Preferences.Key.PRINTER_BLUETOOTH_NAME, device.getName());
            Preferences.putString(Preferences.Key.PRINTER_BLUETOOTH_MAC, device.getAddress());
        }
        this.bluetoothNameTv.setText(device.getName());
        this.bluetoothMacTv.setText(device.getAddress());
    }

    @Override // android.support.v4.app.Fragment
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        this.fragmentView = inflater.inflate(R.layout.fragment_bluetooth, (ViewGroup) null);
        ViewUtils.inject(this, this.fragmentView);
        return this.fragmentView;
    }

    @OnClick({R.id.delete, R.id.bluetooth_refresh_view})
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.delete:
                if (!TextUtils.isEmpty(this.bluetoothMacTv.getText().toString().trim())) {
                    this.manager.setMac(PdfObject.NOTHING);
                    this.manager.disConnectSoket("删除蓝牙列表");
                    this.bluetoothNameTv.setText(PdfObject.NOTHING);
                    this.bluetoothMacTv.setText(PdfObject.NOTHING);
                    if (ClouData.getInstance().getCommunicateType() == 0) {
                        Preferences.removeKey(Preferences.Key.BLUETOOTH_NAME);
                        Preferences.removeKey(Preferences.Key.BLUETOOTH_MAC);
                    } else {
                        Preferences.removeKey(Preferences.Key.PRINTER_BLUETOOTH_NAME);
                        Preferences.removeKey(Preferences.Key.PRINTER_BLUETOOTH_MAC);
                    }
                    showToast(getString(R.string.text_delete_success));
                    return;
                }
                return;
            case R.id.bluetooth_refresh_view:
                this.manager.reSearchDeviceList("手动蓝牙列表-重新搜索");
                this.messageManager.freshDevice();
                if (1 == ClouData.getInstance().getCommunicateType()) {
                    this.hasRefreshList = true;
                    return;
                }
                return;
            default:
                return;
        }
    }

    @Override // android.widget.AdapterView.OnItemClickListener
    public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
        String btoothName = Preferences.getString(Preferences.Key.BLUETOOTH_NAME);
        String btoothMac = Preferences.getString(Preferences.Key.BLUETOOTH_MAC);
        BluetoothDevice device = (BluetoothDevice) this.mAdapter.getItem(position);
        if (device == null) {
            return;
        }
        if (!TextUtils.isEmpty(btoothMac) && !btoothMac.equals(device.getAddress())) {
            showToast(getString(R.string.text_try_to_delete_device).replace("name1", btoothName).replace("name2", new StringBuilder(String.valueOf(device.getName())).toString()));
        } else if (btoothMac.equals(device.getAddress())) {
            showToast(getString(R.string.text_you_have_connected));
        } else {
            saveDeviceInfo(device);
            if (ClouData.getInstance().getCommunicateType() == 0) {
                this.manager.setMac(device.getAddress());
                this.manager.connectDevice(device, true, this.m_Context);
            }
        }
    }

    @Override // com.clou.rs350.handler.HandlerSwitchView.IOnSelectCallback
    public void onSelectedChanged(int index) {
        ClouData.getInstance().setCommunicateType(index);
        showSavedDevice();
    }

    @Override // com.clou.rs350.handler.HandlerSwitchView.IOnSelectCallback
    public void onClickView(View view) {
    }
}
