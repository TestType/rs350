package com.clou.rs350.ui.dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import com.clou.rs350.R;
import com.clou.rs350.ui.popwindow.ListPopWindow;
import com.clou.rs350.utils.OtherUtils;

import java.util.List;

public class SelectValueDialog extends Dialog implements View.OnClickListener {
    private Button btn_Cancel;
    private Button btn_Ok;
    private View.OnClickListener cancleClick;
    private Context m_Context;
    private String[] m_Values;
    private int m_WarningId;
    private View.OnClickListener okClick;
    private TextView txt_ContentView;
    private TextView txt_Value;

    public SelectValueDialog(Context context, List<String> values) {
        this(context, (String[]) values.toArray(new String[values.size()]));
    }

    public SelectValueDialog(Context context, String[] values) {
        super(context, R.style.dialog);
        this.m_Context = null;
        this.txt_ContentView = null;
        this.txt_Value = null;
        this.btn_Ok = null;
        this.btn_Cancel = null;
        this.m_Values = null;
        this.m_WarningId = 0;
        this.okClick = null;
        this.cancleClick = null;
        requestWindowFeature(1);
        setCanceledOnTouchOutside(true);
        this.m_Context = context;
        this.m_Values = values;
    }

    /* access modifiers changed from: protected */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_selection);
        WindowManager.LayoutParams lp = getWindow().getAttributes();
        lp.width = getContext().getResources().getDisplayMetrics().widthPixels;
        lp.height = getContext().getResources().getDisplayMetrics().heightPixels;
        getWindow().setAttributes(lp);
        this.txt_ContentView = (TextView) findViewById(R.id.txt_title);
        this.txt_ContentView.setMovementMethod(ScrollingMovementMethod.getInstance());
        this.txt_Value = (TextView) findViewById(R.id.txt_value);
        this.txt_Value.setOnClickListener(this);
        this.btn_Ok = (Button) findViewById(R.id.btn_ok);
        this.btn_Ok.setOnClickListener(this);
        this.btn_Cancel = (Button) findViewById(R.id.btn_cancel);
        this.btn_Cancel.setOnClickListener(this);
    }

    @Override // android.app.Dialog
    public void setTitle(CharSequence title) {
        this.txt_ContentView.setText(title.toString());
    }

    public void setSelectionValue(String value) {
        this.txt_Value.setText(value);
    }

    public void setWarning(int id) {
        this.m_WarningId = id;
    }

    public void setOkText(CharSequence text) {
        this.btn_Ok.setText(text);
    }

    public void setOkText(int id) {
        this.btn_Ok.setText(id);
    }

    public void setCancleText(CharSequence text) {
        this.btn_Cancel.setText(text);
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_cancel:
                if (this.cancleClick != null) {
                    this.cancleClick.onClick(v);
                }
                dismiss();
                return;
            case R.id.txt_value:
                ListPopWindow.showListPopwindow(this.m_Context, this.m_Values, v, null);
                return;
            case R.id.btn_ok:
                if (!OtherUtils.isEmpty(this.txt_Value)) {
                    v.setTag(this.txt_Value.getText().toString());
                    if (this.okClick != null) {
                        this.okClick.onClick(v);
                    }
                    dismiss();
                    return;
                } else if (this.m_WarningId != 0) {
                    new HintDialog(this.m_Context, this.m_WarningId).show();
                    return;
                } else {
                    return;
                }
            default:
                return;
        }
    }

    public void setOnClick(View.OnClickListener onClickListener, View.OnClickListener cancleClick2) {
        this.okClick = onClickListener;
        this.cancleClick = cancleClick2;
    }
}
