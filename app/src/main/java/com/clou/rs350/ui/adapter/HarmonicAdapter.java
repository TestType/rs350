package com.clou.rs350.ui.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.clou.rs350.R;
import com.clou.rs350.device.model.ExplainedModel;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;

public class HarmonicAdapter extends BaseAdapter {
    private Double[] angles;
    private Double[] contents;
    private Context context;
    private ExplainedModel[] values;

    public HarmonicAdapter(Context context2) {
        this.context = context2;
    }

    public void refresh(ExplainedModel[] values2, Double[] contents2, Double[] angles2) {
        this.values = values2;
        this.contents = contents2;
        this.angles = angles2;
        notifyDataSetChanged();
    }

    public int getCount() {
        if (this.values != null) {
            return this.values.length;
        }
        return 0;
    }

    public Object getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return 0;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = View.inflate(this.context, R.layout.adapter_harmonic_item, null);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        holder.showValue(position + 1, this.values[position], this.contents[position].doubleValue(), this.angles[position].doubleValue());
        return convertView;
    }

    class ViewHolder {
        TextView angleTv;
        TextView contentTv;
        TextView numberTv;
        TextView valueTv;

        ViewHolder(View v) {
            this.numberTv = (TextView) v.findViewById(R.id.realtime_harmonic_table_l1);
            this.valueTv = (TextView) v.findViewById(R.id.realtime_harmonic_table_l2);
            this.contentTv = (TextView) v.findViewById(R.id.realtime_harmonic_table_l3);
            this.angleTv = (TextView) v.findViewById(R.id.realtime_harmonic_table_l4);
        }

        /* access modifiers changed from: package-private */
        public void showValue(int index, ExplainedModel value, double content, double angle) {
            this.numberTv.setText(new StringBuilder(String.valueOf(index)).toString());
            this.valueTv.setText(value.getStringValue());
            DecimalFormat df = new DecimalFormat("0.00");
            DecimalFormatSymbols symbols = new DecimalFormatSymbols();
            symbols.setDecimalSeparator('.');
            df.setDecimalFormatSymbols(symbols);
            this.contentTv.setText(df.format(content));
            DecimalFormat df2 = new DecimalFormat("0.000");
            df2.setDecimalFormatSymbols(symbols);
            this.angleTv.setText(df2.format(angle));
        }
    }

    public void clear() {
        this.values = null;
        this.angles = null;
        notifyDataSetChanged();
    }
}
