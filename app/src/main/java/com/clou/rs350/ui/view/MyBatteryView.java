package com.clou.rs350.ui.view;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.view.View;

import com.clou.rs350.R;

public class MyBatteryView extends View {
    private static final String TAG = "BatteryView";
    private Context m_Context;
    float m_Height = ((float) getHeight());
    private Paint m_Paint = null;
    float m_Power = 10.0f;
    private float m_StrokeWidth = 2.0f;
    float m_Width = ((float) getWidth());

    public MyBatteryView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.m_Context = context;
        init();
    }

    private void init() {
        this.m_Paint = new Paint();
        this.m_Paint.setAntiAlias(true);
        this.m_StrokeWidth = getValue(R.dimen.dp_2, 2.0f);
        this.m_StrokeWidth = (float) ((((int) this.m_StrokeWidth) / 2) * 2);
    }

    private float getValue(int resId, float defaultValue) {
        return this.m_Context == null ? defaultValue : this.m_Context.getResources().getDimension(resId);
    }

    /* access modifiers changed from: protected */
    public void onDraw(Canvas canvas) {
        this.m_Width = (float) getWidth();
        this.m_Height = (float) getHeight();
        drawBattery(canvas, this.m_Paint);
    }

    public void setBattery(float values) {
        this.m_Power = values;
        postInvalidate();
    }

    private void drawBattery(Canvas canvas, Paint paint) {
        float halfStroke = this.m_StrokeWidth / 2.0f;
        float batteryheadheight = this.m_Height * 0.25f;
        float batteryheadwidth = this.m_Width * 0.1f;
        this.m_Paint.setStyle(Paint.Style.FILL);
        this.m_Paint.setStrokeWidth(this.m_StrokeWidth);
        if (this.m_Power < 15.0f) {
            this.m_Paint.setColor(-65536);
        } else if (this.m_Power < 40.0f) {
            this.m_Paint.setColor(-256);
        } else {
            this.m_Paint.setColor(-16711936);
        }
        float width = (this.m_Power / 100.0f) * ((this.m_Width - batteryheadwidth) - this.m_StrokeWidth);
        if (width < this.m_Width * 0.05f) {
            width = this.m_Width * 0.05f;
        }
        canvas.drawRect(this.m_StrokeWidth, this.m_StrokeWidth, width + this.m_StrokeWidth, this.m_Height - this.m_StrokeWidth, this.m_Paint);
        this.m_Paint.setStrokeWidth(this.m_StrokeWidth);
        this.m_Paint.setColor(-1);
        this.m_Paint.setStyle(Paint.Style.STROKE);
        canvas.drawRect(halfStroke, halfStroke, (this.m_Width - batteryheadwidth) - halfStroke, this.m_Height - halfStroke, this.m_Paint);
        this.m_Paint.setStyle(Paint.Style.FILL);
        canvas.drawRect(this.m_Width - batteryheadwidth, batteryheadheight, this.m_Width - halfStroke, this.m_Height - batteryheadheight, this.m_Paint);
    }
}
