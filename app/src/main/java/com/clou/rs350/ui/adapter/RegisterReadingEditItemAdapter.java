package com.clou.rs350.ui.adapter;

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.clou.rs350.R;
import com.clou.rs350.db.model.sequence.RegisterReadingItem;

import java.util.List;

public class RegisterReadingEditItemAdapter extends BaseAdapter {
    private Context context;
    List<RegisterReadingItem> m_Data;
    boolean m_Enable = false;

    public void setEnable(boolean flag) {
        this.m_Enable = flag;
        notifyDataSetChanged();
    }

    public RegisterReadingEditItemAdapter(Context context2) {
        this.context = context2;
    }

    public void refresh(List<RegisterReadingItem> data) {
        this.m_Data = data;
        notifyDataSetChanged();
    }

    public int getCount() {
        if (this.m_Data != null) {
            return this.m_Data.size();
        }
        return 0;
    }

    public Object getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return 0;
    }

    public List<RegisterReadingItem> getDatas() {
        return this.m_Data;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = View.inflate(this.context, R.layout.adapter_scheme_edit_item, null);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        holder.showValue(position, this.m_Data.get(position));
        return convertView;
    }

    class ViewHolder {
        CheckBox chk_Required_Field;
        int m_Index;
        TextView txt_Index;
        TextView txt_Value;

        ViewHolder(View v) {
            this.txt_Index = (TextView) v.findViewById(R.id.txt_index);
            this.txt_Value = (TextView) v.findViewById(R.id.txt_value1);
            this.txt_Value.addTextChangedListener(new TextWatcher() {
                /* class com.clou.rs350.ui.adapter.RegisterReadingEditItemAdapter.ViewHolder.AnonymousClass1 */

                public void onTextChanged(CharSequence s, int start, int before, int count) {
                }

                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                }

                public void afterTextChanged(Editable s) {
                    RegisterReadingEditItemAdapter.this.m_Data.get(ViewHolder.this.m_Index).Register = ViewHolder.this.txt_Value.getText().toString();
                }
            });
            this.chk_Required_Field = (CheckBox) v.findViewById(R.id.chk_required_field);
            this.chk_Required_Field.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                /* class com.clou.rs350.ui.adapter.RegisterReadingEditItemAdapter.ViewHolder.AnonymousClass2 */

                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    RegisterReadingEditItemAdapter.this.m_Data.get(ViewHolder.this.m_Index).RequiredField = isChecked ? 1 : 0;
                }
            });
        }

        /* access modifiers changed from: package-private */
        public void showValue(int index, RegisterReadingItem data) {
            this.m_Index = index;
            this.txt_Index.setText(new StringBuilder(String.valueOf(index + 1)).toString());
            this.txt_Value.setText(data.Register);
            this.txt_Value.setEnabled(RegisterReadingEditItemAdapter.this.m_Enable);
            this.chk_Required_Field.setChecked(1 == RegisterReadingEditItemAdapter.this.m_Data.get(this.m_Index).RequiredField);
            this.chk_Required_Field.setEnabled(RegisterReadingEditItemAdapter.this.m_Enable);
        }
    }
}
