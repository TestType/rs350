package com.clou.rs350.ui.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.clou.rs350.R;
import com.clou.rs350.db.model.sequence.FieldItem;

import java.util.List;
import java.util.Map;

public class FieldSettingAdapter extends BaseAdapter {
    private Context m_Context;
    private Map<String, FieldItem> m_Data;
    private List<String> m_Keys;

    public FieldSettingAdapter(Context context) {
        this.m_Context = context;
    }

    public void refresh(List<String> keys, Map<String, FieldItem> data) {
        this.m_Keys = keys;
        this.m_Data = data;
        notifyDataSetChanged();
    }

    public int getCount() {
        if (this.m_Data != null) {
            return this.m_Data.size();
        }
        return 0;
    }

    public Object getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return 0;
    }

    public Map<String, FieldItem> getData() {
        return this.m_Data;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;
        if (convertView == null) {
            convertView = View.inflate(this.m_Context, R.layout.adapter_field_setting, null);
            holder = new ViewHolder(convertView);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        holder.showValue(position, this.m_Data.get(this.m_Keys.get(position)));
        return convertView;
    }

    class ViewHolder {
        CheckBox chk_Required_Field;
        int m_Index;
        TextView txt_FieldName;

        ViewHolder(View v) {
            this.txt_FieldName = (TextView) v.findViewById(R.id.txt_field_name);
            this.chk_Required_Field = (CheckBox) v.findViewById(R.id.chk_required_field);
            this.chk_Required_Field.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                /* class com.clou.rs350.ui.adapter.FieldSettingAdapter.ViewHolder.AnonymousClass1 */

                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    ((FieldItem) FieldSettingAdapter.this.m_Data.get(FieldSettingAdapter.this.m_Keys.get(ViewHolder.this.m_Index))).RequiredSetting = isChecked ? 1 : 0;
                }
            });
        }

        /* access modifiers changed from: package-private */
        public void showValue(int index, FieldItem data) {
            this.m_Index = index;
            if (data.Id != 0) {
                this.txt_FieldName.setText(data.Id);
            } else {
                this.txt_FieldName.setText(data.Item);
            }
            this.chk_Required_Field.setChecked(1 == ((FieldItem) FieldSettingAdapter.this.m_Data.get(FieldSettingAdapter.this.m_Keys.get(this.m_Index))).RequiredSetting);
        }
    }
}
