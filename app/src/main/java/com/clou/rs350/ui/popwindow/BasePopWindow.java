package com.clou.rs350.ui.popwindow;

import android.content.Context;
import android.view.View;
import android.widget.PopupWindow;

public class BasePopWindow extends PopupWindow {
    Context m_Context;

    public BasePopWindow(Context context) {
        super(context);
        this.m_Context = context;
    }

    public void showAtLocation(View parent, int gravity, int x, int y) {
        super.showAtLocation(parent, gravity, x, y);
    }
}
