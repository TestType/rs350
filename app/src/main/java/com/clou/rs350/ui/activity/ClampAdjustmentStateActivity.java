package com.clou.rs350.ui.activity;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.clou.rs350.R;
import com.clou.rs350.db.model.BasicMeasurement;
import com.clou.rs350.device.MeterBaseDevice;
import com.clou.rs350.handler.HandlerSwitchView;
import com.clou.rs350.task.ISleepCallback;
import com.clou.rs350.task.TimerThread;
import com.clou.rs350.ui.adapter.FragmentTabAdapter;
import com.clou.rs350.ui.fragment.BaseFragment;
import com.clou.rs350.ui.fragment.clampadjustmentfragment.ClampAdjustmentStateFragment;
import com.clou.rs350.ui.view.WiringAndRangeView;
import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.view.annotation.ViewInject;

import java.util.ArrayList;
import java.util.List;

public class ClampAdjustmentStateActivity extends BaseActivity implements View.OnClickListener, HandlerSwitchView.IOnSelectCallback {
    public static boolean isCreated;
    @SuppressLint("NonConstantResourceId")
    @ViewInject(R.id.btn_back)
    private Button backButtom;
    BasicMeasurement m_BasicValue = new BasicMeasurement();
    private boolean m_CNVersion;
    @ViewInject(R.id.setting_normal_linearlayout)
    private LinearLayout normalLayout;
    private HandlerSwitchView switchView;
    private TimerThread timerThread;
    @ViewInject(R.id.title_textview)
    private TextView titleTextView;
    @ViewInject(R.id.layout_wiring)
    private View wiringLayout;
    private WiringAndRangeView wiringView;

    /* access modifiers changed from: protected */
    @Override // com.clou.rs350.ui.activity.BaseActivity, android.support.v4.app.FragmentActivity
    public void onCreate(Bundle savedInstanceState) {
        this.TAG = "RealTimeActivity";
        isCreated = true;
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_clamp_adjustment_state);
        setDefaultTitle(R.string.text_clamp_adjustment_state);
        ViewUtils.inject(this);
        initView();
        this.m_TabAdapter = new FragmentTabAdapter(this, getFragments(), R.id.content_view);
        this.m_TabAdapter.init();
    }

    /* access modifiers changed from: protected */
    @Override // com.clou.rs350.ui.activity.BaseActivity, android.support.v4.app.FragmentActivity
    public void onDestroy() {
        isCreated = false;
        super.onDestroy();
    }

    private List<BaseFragment> getFragments() {
        List<BaseFragment> list = new ArrayList<>();
        list.add(new ClampAdjustmentStateFragment());
        return list;
    }

    /* access modifiers changed from: protected */
    @Override // com.clou.rs350.ui.activity.BaseActivity, android.support.v4.app.FragmentActivity
    public void onResume() {
        super.onResume();
        startToReadRangeData();
    }

    /* access modifiers changed from: protected */
    @Override // com.clou.rs350.ui.activity.BaseActivity, android.support.v4.app.FragmentActivity
    public void onPause() {
        super.onPause();
        killThread(this.timerThread);
    }

    private void startToReadRangeData() {
        if (this.timerThread == null || this.timerThread.isStop()) {
            this.timerThread = new TimerThread(3000, new ISleepCallback() {
                /* class com.clou.rs350.ui.activity.ClampAdjustmentStateActivity.AnonymousClass1 */

                @Override // com.clou.rs350.task.ISleepCallback
                public void onSleepAfterToDo() {
                    ClampAdjustmentStateActivity.this.messageManager.getRanage();
                }
            }, "Read Range Thread");
            this.timerThread.start();
        }
    }

    private void initView() {
        this.wiringView = new WiringAndRangeView(this, this.wiringLayout);
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_back:
                finish();
                return;
            case R.id.other_layout:
                this.normalLayout.setVisibility(View.GONE);
                return;
            default:
                return;
        }
    }

    @Override // com.clou.rs350.ui.activity.BaseActivity, com.clou.rs350.manager.MessageManager.IMessageListener
    public void onReceiveMessage(MeterBaseDevice Device) {
        this.m_BasicValue.parseRangeData(Device);
        this.wiringView.showData(this.m_BasicValue);
    }

    @Override // com.clou.rs350.handler.HandlerSwitchView.IOnSelectCallback
    public void onSelectedChanged(int index) {
    }

    @Override // com.clou.rs350.handler.HandlerSwitchView.IOnSelectCallback
    public void onClickView(View view) {
        onClick(view);
    }

    @Override // com.clou.rs350.ui.activity.BaseActivity
    public void startOrStopChangeViewStatus(int flag) {
        boolean hasStart;
        boolean z;
        boolean z2 = false;
        super.startOrStopChangeViewStatus(flag);
        if (1 == flag) {
            hasStart = true;
        } else {
            hasStart = false;
        }
        Button button = this.backButtom;
        if (hasStart) {
            z = false;
        } else {
            z = true;
        }
        button.setEnabled(z);
        WiringAndRangeView wiringAndRangeView = this.wiringView;
        if (!hasStart) {
            z2 = true;
        }
        wiringAndRangeView.setEnabled(z2);
    }
}
