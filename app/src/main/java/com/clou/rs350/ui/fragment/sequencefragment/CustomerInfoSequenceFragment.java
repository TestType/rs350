package com.clou.rs350.ui.fragment.sequencefragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.clou.rs350.R;
import com.clou.rs350.model.ClouData;
import com.clou.rs350.ui.dialog.HintDialog;
import com.clou.rs350.ui.fragment.BaseFragment;
import com.clou.rs350.ui.view.infoview.CustomerInfoView;

public class CustomerInfoSequenceFragment extends BaseFragment {
    TextView contentView = null;
    CustomerInfoView m_InfoView;
    View m_InfoViewLayout;

    @Override // android.support.v4.app.Fragment
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        this.fragmentView = inflater.inflate(R.layout.fragment_sequence_customer_info, (ViewGroup) null);
        return this.fragmentView;
    }

    @Override // android.support.v4.app.Fragment, com.clou.rs350.ui.fragment.BaseFragment
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initView();
    }

    private void initView() {
        this.m_InfoViewLayout = findViewById(R.id.layout_customer_info);
        this.m_InfoView = new CustomerInfoView(this.m_Context, this.m_InfoViewLayout, ClouData.getInstance().getCustomerInfo(), 4);
        this.m_InfoView.setRequiredField(this.m_MustDo);
    }

    @Override // android.support.v4.app.Fragment, com.clou.rs350.ui.fragment.BaseFragment
    public void onResume() {
        this.m_InfoView.refreshData(ClouData.getInstance().getCustomerInfo());
        super.onResume();
    }

    /* access modifiers changed from: protected */
    @Override // com.clou.rs350.ui.fragment.BaseFragment
    public void copyData() {
        ClouData.getInstance().setCustomerInfo(this.m_InfoView.getInfo());
    }

    @Override // com.clou.rs350.ui.fragment.BaseFragment
    public int isFinish() {
        if (this.m_InfoView.getInfo().CustomerSr.isEmpty()) {
            new HintDialog(this.m_Context, (int) R.string.text_customer_serial_no_is_empty).show();
            return 1;
        } else if (this.m_InfoView.hasSetParams()) {
            return 0;
        } else {
            return 1;
        }
    }
}
