package com.clou.rs350.ui.view.control;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.clou.rs350.R;
import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.view.annotation.ViewInject;

import java.util.ArrayList;
import java.util.List;

public class HorizontalList implements View.OnClickListener {
    Context m_Context;
    @ViewInject(R.id.horizontallayout)
    LinearLayout m_HorizontalList;
    onItemClick m_OnClick;
    int m_Selection;
    List<TextView> m_TextViews;

    public interface onItemClick {
        void onItemClick(int i);
    }

    public HorizontalList(Context context, View view) {
        this.m_Context = context;
        ViewUtils.inject(this, view);
    }

    public void setOnClick(onItemClick onClick) {
        this.m_OnClick = onClick;
    }

    public void setList(List<Integer> titles) {
        this.m_Selection = 0;
        this.m_HorizontalList.removeAllViews();
        this.m_TextViews = new ArrayList();
        for (int i = 0; i < titles.size(); i++) {
            View view = LayoutInflater.from(this.m_Context).inflate(R.layout.adapter_record_item, (ViewGroup) null);
            TextView child = (TextView) view.findViewById(R.id.apt_list_textview);
            LinearLayout layout = (LinearLayout) view.findViewById(R.id.layout_listitem);
            child.setText(titles.get(i).intValue());
            child.setWidth(this.m_Context.getResources().getDimensionPixelSize(R.dimen.dp_80));
            child.setTag(Integer.valueOf(i));
            child.setOnClickListener(this);
            this.m_TextViews.add(child);
            this.m_HorizontalList.addView(view);
            ViewGroup.LayoutParams layoutParams = layout.getLayoutParams();
            layoutParams.height = -1;
            layout.setLayoutParams(layoutParams);
        }
        refreshTextView();
    }

    private void refreshTextView() {
        if (this.m_TextViews != null) {
            int i = 0;
            while (i < this.m_TextViews.size()) {
                this.m_TextViews.get(i).setEnabled(this.m_Selection != i);
                i++;
            }
        }
    }

    public void onClick(View v) {
        int index = ((Number) v.getTag()).intValue();
        this.m_Selection = index;
        refreshTextView();
        if (this.m_OnClick != null) {
            this.m_OnClick.onItemClick(index);
        }
    }
}
