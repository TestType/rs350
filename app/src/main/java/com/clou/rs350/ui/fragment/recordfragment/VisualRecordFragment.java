package com.clou.rs350.ui.fragment.recordfragment;

import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.clou.rs350.R;
import com.clou.rs350.db.model.sequence.VisualData;
import com.clou.rs350.ui.adapter.VisualRecordItemAdapter;
import com.clou.rs350.ui.fragment.BaseFragment;
import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.view.annotation.ViewInject;
import com.lidroid.xutils.view.annotation.event.OnClick;

public class VisualRecordFragment extends BaseFragment implements View.OnClickListener {
    private static final String TAG = "VisualRecordFragment";
    @ViewInject(R.id.list_visual)
    private ListView list_Visual;
    private VisualRecordItemAdapter m_Adapter;
    private VisualData m_Data;
    @ViewInject(R.id.txt_remark)
    private TextView txt_Remark;

    public VisualRecordFragment(VisualData data) {
        this.m_Data = data;
    }

    @Override // android.support.v4.app.Fragment
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        this.fragmentView = inflater.inflate(R.layout.fragment_record_visualinspection, (ViewGroup) null);
        ViewUtils.inject(this, this.fragmentView);
        this.txt_Remark.setMovementMethod(ScrollingMovementMethod.getInstance());
        return this.fragmentView;
    }

    @Override // android.support.v4.app.Fragment, com.clou.rs350.ui.fragment.BaseFragment
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        this.m_Adapter = new VisualRecordItemAdapter(this.m_Context);
        this.list_Visual.setAdapter((ListAdapter) this.m_Adapter);
    }

    @OnClick({})
    public void onClick(View v) {
        v.getId();
    }

    private void showData() {
        this.m_Adapter.refresh(this.m_Data.Visuals);
        this.txt_Remark.setText(this.m_Data.Remark);
    }

    @Override // android.support.v4.app.Fragment, com.clou.rs350.ui.fragment.BaseFragment
    public void onResume() {
        showData();
        super.onResume();
    }

    private void parseData() {
        this.m_Data.Visuals = this.m_Adapter.getData();
        this.m_Data.Remark = this.txt_Remark.getText().toString();
    }

    /* access modifiers changed from: protected */
    @Override // com.clou.rs350.ui.fragment.BaseFragment
    public void copyData() {
        parseData();
    }

    @Override // android.support.v4.app.Fragment, com.clou.rs350.ui.fragment.BaseFragment
    public void onPause() {
        super.onPause();
    }
}
