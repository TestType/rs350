package com.clou.rs350.ui.fragment.settingfragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.clou.rs350.R;
import com.clou.rs350.ui.fragment.BaseFragment;

public class StyleFragment extends BaseFragment implements View.OnClickListener {
    private View view = null;

    public void onClick(View v) {
    }

    @Override // android.support.v4.app.Fragment, com.clou.rs350.ui.fragment.BaseFragment
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override // android.support.v4.app.Fragment
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        this.view = inflater.inflate(R.layout.fragment_style, (ViewGroup) null);
        return this.view;
    }
}
