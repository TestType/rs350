package com.clou.rs350.ui.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.clou.rs350.CLApplication;
import com.clou.rs350.Preferences;
import com.clou.rs350.R;
import com.clou.rs350.callback.ILoadCallback;
import com.clou.rs350.db.dao.sequence.SequenceDao;
import com.clou.rs350.db.model.BasicMeasurement;
import com.clou.rs350.db.model.sequence.SequenceData;
import com.clou.rs350.db.model.sequence.SequenceItem;
import com.clou.rs350.device.MeterBaseDevice;
import com.clou.rs350.handler.HandlerSwitchView;
import com.clou.rs350.model.ClouData;
import com.clou.rs350.report.ExportReport;
import com.clou.rs350.report.LoadExportDataTool;
import com.clou.rs350.task.ISleepCallback;
import com.clou.rs350.task.MyTask;
import com.clou.rs350.task.TimerThread;
import com.clou.rs350.ui.adapter.FragmentTabAdapter;
import com.clou.rs350.ui.adapter.SequenceEditAdapter;
import com.clou.rs350.ui.adapter.SequenceShowAdapter;
import com.clou.rs350.ui.dialog.HintDialog;
import com.clou.rs350.ui.dialog.LoadingDialog;
import com.clou.rs350.ui.dialog.NormalDialog;
import com.clou.rs350.ui.dialog.SaveAsDialog;
import com.clou.rs350.ui.dialog.SelectValueDialog;
import com.clou.rs350.ui.fragment.BaseFragment;
import com.clou.rs350.ui.fragment.errorfragment.DailyTestingFragment;
import com.clou.rs350.ui.fragment.errorfragment.DemandTestingFragment;
import com.clou.rs350.ui.fragment.errorfragment.EnergyTestingFragment;
import com.clou.rs350.ui.fragment.errorfragment.ErrorTestingFragment;
import com.clou.rs350.ui.fragment.readmeterfragment.ReadMeterFragment;
import com.clou.rs350.ui.fragment.realtimefragment.HarmonicFragment;
import com.clou.rs350.ui.fragment.realtimefragment.LTMFragment;
import com.clou.rs350.ui.fragment.realtimefragment.RealtimeBasicFragment;
import com.clou.rs350.ui.fragment.realtimefragment.WaveFragment;
import com.clou.rs350.ui.fragment.realtimefragment.WiringCheckFragment;
import com.clou.rs350.ui.fragment.sequencefragment.CameraSequenceFragment;
import com.clou.rs350.ui.fragment.sequencefragment.CustomerInfoSequenceFragment;
import com.clou.rs350.ui.fragment.sequencefragment.RegisterReadingSequenceFragment;
import com.clou.rs350.ui.fragment.sequencefragment.RemarkSequenceFragment;
import com.clou.rs350.ui.fragment.sequencefragment.SealSequenceFragment;
import com.clou.rs350.ui.fragment.sequencefragment.SignatureSequenceFragment;
import com.clou.rs350.ui.fragment.sequencefragment.SiteDataSequenceFragment;
import com.clou.rs350.ui.fragment.sequencefragment.VisualSequenceFragment;
import com.clou.rs350.ui.fragment.transformerfragment.CTBurdenFragment;
import com.clou.rs350.ui.fragment.transformerfragment.CTErrorFragment;
import com.clou.rs350.ui.fragment.transformerfragment.PTBurdenFragment;
import com.clou.rs350.ui.fragment.transformerfragment.PTComparisonFragment;
import com.clou.rs350.ui.popwindow.ListPopWindow;
import com.clou.rs350.ui.view.WiringAndRangeView;
import com.clou.rs350.utils.FileUtils;
import com.clou.rs350.utils.OtherUtils;
import com.itextpdf.text.pdf.PdfObject;
import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.view.annotation.ViewInject;
import com.lidroid.xutils.view.annotation.event.OnClick;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class TestSequenceActivity extends BaseActivity implements View.OnClickListener {
    @ViewInject(R.id.btn_add)
    private Button btn_Add;
    @ViewInject(R.id.btn_back)
    private Button btn_Back;
    @ViewInject(R.id.btn_delete)
    private Button btn_Delete;
    @ViewInject(R.id.btn_edit)
    private Button btn_Edit;
    @ViewInject(R.id.btn_new)
    private Button btn_New;
    @ViewInject(R.id.btn_next)
    private Button btn_Next;
    @ViewInject(R.id.btn_previous)
    private Button btn_Previous;
    @ViewInject(R.id.btn_run)
    private Button btn_Run;
    @ViewInject(R.id.btn_save_as)
    private Button btn_SaveAs;
    public boolean isRun = false;
    @ViewInject(R.id.layout_sequence_edit)
    private LinearLayout layout_SequenceEdit;
    @ViewInject(R.id.layout_edit)
    private LinearLayout layout_SequenceSelection;
    @ViewInject(R.id.layout_sequence_show)
    private LinearLayout layout_SequenceShow;
    @ViewInject(R.id.layout_test)
    private LinearLayout layout_SequenceTest;
    @ViewInject(R.id.list_sequence_edit)
    private ListView lst_SequenceEdit;
    @ViewInject(R.id.list_sequence_show)
    private ListView lst_SequenceShow;
    int m_Authority = 0;
    BasicMeasurement m_BasicValue = new BasicMeasurement();
    private FunctionIndex m_Indexs;
    private boolean m_IsNew = false;
    private LoadingDialog m_LoadingDialog;
    private SequenceDao m_SequenceDao;
    private SequenceData m_SequenceData;
    private SequenceEditAdapter m_SequenceEditAdapter;
    private SequenceShowAdapter m_SequenceShowAdapter;
    private int m_TestIndex;
    private HandlerSwitchView switchView;
    private TimerThread timerThread;
    @ViewInject(R.id.txt_sequence_name_edit)
    private TextView txt_SequenceEdit;
    @ViewInject(R.id.txt_sequence_name_select)
    private TextView txt_SequenceSelection;
    @ViewInject(R.id.subtitle_textview)
    private TextView txt_Subtitle;
    @ViewInject(R.id.title_textview)
    private TextView txt_Title;
    @ViewInject(R.id.layout_wiring)
    private View wiringLayout;
    private WiringAndRangeView wiringView;

    /* access modifiers changed from: package-private */
    public class FunctionIndex {
        public int BasicMeasurementIndex;
        public int CTBurdenIndex;
        public int CTErrorIndex;
        public int CameraIndex;
        public int DailyIndex;
        public int DemandIndex;
        public int EnergyIndex;
        public int ErrorIndex;
        public int HarmonicIndex;
        public int LTMIndex;
        public int PTBurdenIndex;
        public int PTComparisonIndex;
        public int ReadMeterIndex;
        public int RegisterReadingIndex;
        public int SealIndex;
        public int SignatureIndex;
        public int WaveIndex;
        public int WiringCheckIndex;

        FunctionIndex() {
        }
    }

    /* access modifiers changed from: protected */
    @Override // com.clou.rs350.ui.activity.BaseActivity, android.support.v4.app.FragmentActivity
    public void onCreate(Bundle savedInstanceState) {
        this.TAG = "TestSequenceActivity";
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test_sequence);
        ViewUtils.inject(this);
        this.txt_Title.setText(R.string.text_test_sequence);
        this.m_SequenceDao = new SequenceDao(this.m_Context);
        initData();
        this.wiringView = new WiringAndRangeView(this, this.wiringLayout);
        this.m_SequenceShowAdapter = new SequenceShowAdapter(null, this.m_Context);
        this.lst_SequenceShow.setAdapter((ListAdapter) this.m_SequenceShowAdapter);
        this.m_SequenceEditAdapter = new SequenceEditAdapter(null, this.m_Context);
        this.lst_SequenceEdit.setAdapter((ListAdapter) this.m_SequenceEditAdapter);
        this.btn_Add.setOnClickListener(new View.OnClickListener() {
            /* class com.clou.rs350.ui.activity.TestSequenceActivity.AnonymousClass1 */

            public void onClick(View v) {
                TestSequenceActivity.this.m_SequenceEditAdapter.add();
                TestSequenceActivity.this.lst_SequenceEdit.setSelection(TestSequenceActivity.this.m_SequenceEditAdapter.getCount() - 1);
            }
        });
        setEditButton(false, false);
    }

    private void initData() {
        this.m_Authority = ClouData.getInstance().getOperatorInfo().Authority;
    }

    private void startToReadRangeData() {
        if (this.timerThread == null || this.timerThread.isStop()) {
            this.timerThread = new TimerThread(3000, new ISleepCallback() {
                /* class com.clou.rs350.ui.activity.TestSequenceActivity.AnonymousClass2 */

                @Override // com.clou.rs350.task.ISleepCallback
                public void onSleepAfterToDo() {
                    TestSequenceActivity.this.messageManager.getRanage();
                    TestSequenceActivity.this.messageManager.getData();
                }
            }, "Read Range Thread");
            this.timerThread.start();
        }
    }

    /* access modifiers changed from: protected */
    @Override // com.clou.rs350.ui.activity.BaseActivity, android.support.v4.app.FragmentActivity
    public void onResume() {
        super.onResume();
        startToReadRangeData();
    }

    /* access modifiers changed from: protected */
    @Override // com.clou.rs350.ui.activity.BaseActivity, android.support.v4.app.FragmentActivity
    public void onPause() {
        super.onPause();
        killThread(this.timerThread);
    }

    private BaseFragment getFragment(String Name) {
        if (Name.equals(getString(R.string.text_customer_info))) {
            return new CustomerInfoSequenceFragment();
        }
        if (Name.equals(getString(R.string.text_site_data))) {
            return new SiteDataSequenceFragment();
        }
        if (Name.equals(getString(R.string.text_seal))) {
            BaseFragment tmpFragment = new SealSequenceFragment(ClouData.getInstance().getSealData(this.m_Indexs.SealIndex));
            this.m_Indexs.SealIndex++;
            return tmpFragment;
        } else if (Name.equals(getString(R.string.text_register_reading))) {
            BaseFragment tmpFragment2 = new RegisterReadingSequenceFragment(ClouData.getInstance().getRegisterReadingData(this.m_Indexs.RegisterReadingIndex));
            this.m_Indexs.RegisterReadingIndex++;
            return tmpFragment2;
        } else if (Name.equals(getString(R.string.text_visual))) {
            return new VisualSequenceFragment(ClouData.getInstance().getVisualData());
        } else {
            if (Name.equals(getString(R.string.text_realtime))) {
                BaseFragment tmpFragment3 = new RealtimeBasicFragment(ClouData.getInstance().getBasicMeasurement(this.m_Indexs.BasicMeasurementIndex));
                this.m_Indexs.BasicMeasurementIndex++;
                return tmpFragment3;
            } else if (Name.equals(getString(R.string.text_wave))) {
                BaseFragment tmpFragment4 = new WaveFragment(ClouData.getInstance().getWaveMeasurement(this.m_Indexs.WaveIndex));
                this.m_Indexs.WaveIndex++;
                return tmpFragment4;
            } else if (Name.equals(getString(R.string.text_wiring_check))) {
                BaseFragment tmpFragment5 = new WiringCheckFragment(ClouData.getInstance().getWiringCheckMeasurement(this.m_Indexs.WiringCheckIndex));
                this.m_Indexs.WiringCheckIndex++;
                return tmpFragment5;
            } else if (Name.equals(getString(R.string.text_harmonic))) {
                BaseFragment tmpFragment6 = new HarmonicFragment(ClouData.getInstance().getHarmonicMeasurement(this.m_Indexs.HarmonicIndex));
                this.m_Indexs.HarmonicIndex++;
                return tmpFragment6;
            } else if (Name.equals(getString(R.string.text_ltm))) {
                BaseFragment tmpFragment7 = new LTMFragment(ClouData.getInstance().getLTM(this.m_Indexs.LTMIndex));
                this.m_Indexs.LTMIndex++;
                return tmpFragment7;
            } else if (Name.equals(getString(R.string.text_error_testing))) {
                BaseFragment tmpFragment8 = new ErrorTestingFragment(ClouData.getInstance().getErrorTest(this.m_Indexs.ErrorIndex));
                this.m_Indexs.ErrorIndex++;
                return tmpFragment8;
            } else if (Name.equals(getString(R.string.text_demand_testing))) {
                BaseFragment tmpFragment9 = new DemandTestingFragment(ClouData.getInstance().getDemandTest(this.m_Indexs.DemandIndex));
                this.m_Indexs.DemandIndex++;
                return tmpFragment9;
            } else if (Name.equals(getString(R.string.text_energy_testing))) {
                BaseFragment tmpFragment10 = new EnergyTestingFragment(ClouData.getInstance().getEnergyTest(this.m_Indexs.EnergyIndex));
                this.m_Indexs.EnergyIndex++;
                return tmpFragment10;
            } else if (Name.equals(getString(R.string.text_daily_testing))) {
                BaseFragment tmpFragment11 = new DailyTestingFragment(ClouData.getInstance().getDailyTest(this.m_Indexs.DailyIndex));
                this.m_Indexs.DailyIndex++;
                return tmpFragment11;
            } else if (Name.equals(getString(R.string.text_ct_measurement))) {
                BaseFragment tmpFragment12 = new CTErrorFragment(ClouData.getInstance().getCTMeasurement(this.m_Indexs.CTErrorIndex));
                this.m_Indexs.CTErrorIndex++;
                return tmpFragment12;
            } else if (Name.equals(getString(R.string.text_ct_burden))) {
                BaseFragment tmpFragment13 = new CTBurdenFragment(ClouData.getInstance().getCTBurden(this.m_Indexs.CTBurdenIndex));
                this.m_Indexs.CTBurdenIndex++;
                return tmpFragment13;
            } else if (Name.equals(getString(R.string.text_pt_burden))) {
                BaseFragment tmpFragment14 = new PTBurdenFragment(ClouData.getInstance().getPTBurden(this.m_Indexs.PTBurdenIndex));
                this.m_Indexs.PTBurdenIndex++;
                return tmpFragment14;
            } else if (Name.equals(getString(R.string.text_pt_comparison))) {
                BaseFragment tmpFragment15 = new PTComparisonFragment(ClouData.getInstance().getPTComparison(this.m_Indexs.PTComparisonIndex));
                this.m_Indexs.PTComparisonIndex++;
                return tmpFragment15;
            } else if (Name.equals(getString(R.string.text_read_meter))) {
                BaseFragment tmpFragment16 = new ReadMeterFragment(ClouData.getInstance().getReadmeter(this.m_Indexs.ReadMeterIndex));
                this.m_Indexs.ReadMeterIndex++;
                return tmpFragment16;
            } else if (Name.equals(getString(R.string.text_camera))) {
                BaseFragment tmpFragment17 = new CameraSequenceFragment(ClouData.getInstance().getCameraData(this.m_Indexs.CameraIndex));
                this.m_Indexs.CameraIndex++;
                return tmpFragment17;
            } else if (Name.equals(getString(R.string.text_signature))) {
                BaseFragment tmpFragment18 = new SignatureSequenceFragment(ClouData.getInstance().getSignatureData(this.m_Indexs.SignatureIndex));
                this.m_Indexs.SignatureIndex++;
                return tmpFragment18;
            } else if (Name.equals(getString(R.string.text_remark))) {
                return new RemarkSequenceFragment(ClouData.getInstance().getTestItem());
            } else {
                Name.equals(getString(R.string.text_email));
                return null;
            }
        }
    }

    private boolean initFregments() {
        List<BaseFragment> list = new ArrayList<>();
        this.m_Indexs = new FunctionIndex();
        for (int i = 0; i < this.m_SequenceData.SequenceItems.size(); i++) {
            BaseFragment tmpFragment = getFragment(this.m_SequenceData.SequenceItems.get(i).ItemFunction);
            if (tmpFragment != null) {
                tmpFragment.Title = this.m_SequenceData.SequenceItems.get(i).ItemName;
                tmpFragment.setMustDo(this.m_SequenceData.SequenceItems.get(i).MustDo);
                list.add(tmpFragment);
            }
        }
        if (list.size() <= 0) {
            return false;
        }
        if (this.m_TabAdapter != null) {
            this.m_TabAdapter.cleanFragment();
        }
        this.m_TabAdapter = new FragmentTabAdapter(this, list, R.id.sequencecontent_view);
        this.m_TabAdapter.init();
        this.txt_Subtitle.setText(this.m_TabAdapter.getCurrentFragment().Title);
        return true;
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private void refreshSequenceData(SequenceData data) {
        if (data != null) {
            this.txt_SequenceSelection.setText(data.SequenceName);
            this.m_SequenceShowAdapter.refreshData(data.SequenceItems);
        }
    }

    private void refreshSequenceEdit(SequenceData data) {
        this.txt_SequenceEdit.setText(data.SequenceName);
        this.m_SequenceEditAdapter.refreshData(data.SequenceItems);
    }

    private void setEditButton(boolean Edit, boolean isNew) {
        boolean z;
        boolean z2;
        boolean z3;
        boolean z4;
        int i = 8;
        boolean z5 = false;
        this.layout_SequenceEdit.setVisibility(Edit ? View.VISIBLE : View.GONE);
        LinearLayout linearLayout = this.layout_SequenceShow;
        if (!Edit) {
            i = 0;
        }
        linearLayout.setVisibility(i);
        this.m_IsNew = isNew;
        this.txt_SequenceEdit.setEnabled(isNew);
        Button button = this.btn_New;
        if ((!Edit || !isNew) && 2 < this.m_Authority) {
            z = true;
        } else {
            z = false;
        }
        button.setEnabled(z);
        Button button2 = this.btn_Edit;
        if (Edit || 2 >= this.m_Authority) {
            z2 = false;
        } else {
            z2 = true;
        }
        button2.setEnabled(z2);
        Button button3 = this.btn_SaveAs;
        if (!Edit || !isNew) {
            z3 = true;
        } else {
            z3 = false;
        }
        button3.setEnabled(z3);
        Button button4 = this.btn_Delete;
        if (Edit || 2 >= this.m_Authority) {
            z4 = false;
        } else {
            z4 = true;
        }
        button4.setEnabled(z4);
        Button button5 = this.btn_Run;
        if (!Edit) {
            z5 = true;
        }
        button5.setEnabled(z5);
    }

    private class SaveResult {
        String customerSr;
        int result;
        String testTime;

        private SaveResult() {
        }

        /* synthetic */ SaveResult(TestSequenceActivity testSequenceActivity, SaveResult saveResult) {
            this();
        }
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private void SaveSequence(String Name) {
        if (!this.m_IsNew) {
            this.m_SequenceDao.deleteRecordByKey(Name);
        } else if (this.m_SequenceDao.queryByKey(Name) != null) {
            new HintDialog(this.m_Context, (int) R.string.text_sequence_name_exists).show();
            return;
        }
        this.m_SequenceData = new SequenceData();
        this.m_SequenceData.SequenceName = Name;
        this.m_SequenceData.SequenceItems = this.m_SequenceEditAdapter.getDatas();
        this.m_SequenceData.Language = ClouData.getInstance(this.m_Context).getSettingBasic().Language;
        this.m_SequenceDao.insertObject(this.m_SequenceData);
        setEditButton(false, false);
        refreshSequenceData(this.m_SequenceData);
        showHint(R.string.text_save_success);
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private void runtest() {
        this.m_TestIndex = 0;
        this.isRun = true;
        ClouData.getInstance().getTestItem().TestItem = this.txt_SequenceSelection.getText().toString();
        if (initFregments()) {
            setRuningButton(1);
        } else {
            new HintDialog(this.m_Context, (int) R.string.text_sequence_is_wrong).show();
        }
    }

    @OnClick({R.id.btn_back, R.id.txt_sequence_name_select, R.id.btn_new, R.id.btn_up, R.id.btn_save_as, R.id.btn_down, R.id.btn_edit, R.id.btn_delete, R.id.btn_save_sequence, R.id.btn_cancel, R.id.btn_run, R.id.btn_next, R.id.btn_previous, R.id.btn_save})
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_back:
                if (View.GONE == this.layout_SequenceSelection.getVisibility()) {
                    this.isRun = false;
                    this.m_TabAdapter.cleanFragment();
                    this.txt_Subtitle.setText(PdfObject.NOTHING);
                    setRuningButton(0);
                    return;
                }
                finish();
                return;
            case R.id.btn_delete:
                if (OtherUtils.isEmpty(this.txt_SequenceSelection)) {
                    new HintDialog(this.m_Context, (int) R.string.text_sequence_name_empty).show();
                    return;
                }
                NormalDialog dialog = new NormalDialog(this.m_Context);
                dialog.setOnClick(new View.OnClickListener() {
                    /* class com.clou.rs350.ui.activity.TestSequenceActivity.AnonymousClass3 */

                    public void onClick(View v) {
                        TestSequenceActivity.this.m_SequenceDao.deleteRecordByKey(TestSequenceActivity.this.m_SequenceData.SequenceName);
                        TestSequenceActivity.this.refreshSequenceData(new SequenceData());
                    }
                }, null);
                dialog.show();
                dialog.setTitle(R.string.text_sequence_delete);
                return;
            case R.id.txt_sequence_name_select:
                List<String> tmpNames = this.m_SequenceDao.queryAllKey(ClouData.getInstance(this.m_Context).getSettingBasic().Language);
                ListPopWindow.showListPopwindow(this.m_Context, (String[]) tmpNames.toArray(new String[tmpNames.size()]), v, new View.OnClickListener() {
                    /* class com.clou.rs350.ui.activity.TestSequenceActivity.AnonymousClass5 */

                    public void onClick(View v) {
                        TestSequenceActivity.this.m_SequenceData = (SequenceData) TestSequenceActivity.this.m_SequenceDao.queryByKey(((TextView) v).getText().toString());
                        TestSequenceActivity.this.m_SequenceShowAdapter.refreshData(TestSequenceActivity.this.m_SequenceData.SequenceItems);
                    }
                });
                return;
            case R.id.btn_up:
                this.m_SequenceEditAdapter.moveUp();
                return;
            case R.id.btn_down:
                this.m_SequenceEditAdapter.moveDown();
                return;
            case R.id.btn_save_as:
                SaveAsDialog tmpSaveAsDialog = new SaveAsDialog(this.m_Context, this.m_SequenceDao);
                tmpSaveAsDialog.setWarning(R.string.text_scheme_name_empty);
                tmpSaveAsDialog.setExist(R.string.text_scheme_name_exists);
                tmpSaveAsDialog.setOnClick(new View.OnClickListener() {
                    /* class com.clou.rs350.ui.activity.TestSequenceActivity.AnonymousClass4 */

                    public void onClick(View v) {
                        TestSequenceActivity.this.m_SequenceData.SequenceName = v.getTag().toString();
                        TestSequenceActivity.this.txt_SequenceEdit.setText(TestSequenceActivity.this.m_SequenceData.SequenceName);
                        TestSequenceActivity.this.SaveSequence(TestSequenceActivity.this.m_SequenceData.SequenceName);
                    }
                }, null);
                tmpSaveAsDialog.show();
                tmpSaveAsDialog.setSelectionValue(this.m_SequenceData.SequenceName);
                return;
            case R.id.btn_save_sequence:
                if (OtherUtils.isEmpty(this.txt_SequenceEdit)) {
                    new HintDialog(this.m_Context, (int) R.string.text_sequence_name_empty).show();
                    return;
                } else {
                    SaveSequence(this.txt_SequenceEdit.getText().toString());
                    return;
                }
            case R.id.btn_cancel:
                setEditButton(false, false);
                refreshSequenceData(this.m_SequenceData);
                return;
            case R.id.btn_new:
                setEditButton(true, true);
                SequenceData tmpSequence = new SequenceData();
                tmpSequence.SequenceItems.add(new SequenceItem(0, PdfObject.NOTHING, getString(R.string.text_customer_info), 1));
                refreshSequenceEdit(tmpSequence);
                return;
            case R.id.btn_edit:
                if (OtherUtils.isEmpty(this.txt_SequenceSelection)) {
                    new HintDialog(this.m_Context, (int) R.string.text_sequence_name_empty).show();
                    return;
                }
                setEditButton(true, false);
                refreshSequenceEdit(this.m_SequenceData);
                return;
            case R.id.btn_previous:
                this.m_TestIndex--;
                this.m_TabAdapter.checkedIndex(this.m_TestIndex);
                this.txt_Subtitle.setText(this.m_TabAdapter.getCurrentFragment().Title);
                setTestButton();
                return;
            case R.id.btn_next:
                if (this.m_SequenceData.SequenceItems.get(this.m_TestIndex).MustDo == 0 || this.m_TabAdapter.getCurrentFragment().isFinish() == 0) {
                    if (this.m_TestIndex < this.m_TabAdapter.getCount() - 1) {
                        this.m_TestIndex++;
                        this.m_TabAdapter.checkedIndex(this.m_TestIndex);
                        this.txt_Subtitle.setText(this.m_TabAdapter.getCurrentFragment().Title);
                    } else {
                        this.m_TabAdapter.getCurrentFragment().SaveData();
                        NormalDialog normalDialog = new NormalDialog(this.m_Context);
                        normalDialog.setOnClick(new View.OnClickListener() {
                            /* class com.clou.rs350.ui.activity.TestSequenceActivity.AnonymousClass8 */

                            public void onClick(View v) {
                                if (TestSequenceActivity.this.checkTemplate()) {
                                    TestSequenceActivity.this.save();
                                }
                            }
                        }, null);
                        normalDialog.show();
                        normalDialog.setTitle(R.string.text_do_save);
                    }
                    setTestButton();
                    return;
                }
                return;
            case R.id.btn_run:
                if (this.isRun) {
                    this.isRun = false;
                    return;
                } else if (OtherUtils.isEmpty(this.txt_SequenceSelection)) {
                    new HintDialog(this.m_Context, (int) R.string.text_sequence_name_empty).show();
                    return;
                } else if (OtherUtils.isEmpty(ClouData.getInstance().getCustomerInfo().CustomerSr)) {
                    runtest();
                    return;
                } else {
                    NormalDialog normalDialog2 = new NormalDialog(this.m_Context);
                    normalDialog2.setOnClick(new View.OnClickListener() {
                        /* class com.clou.rs350.ui.activity.TestSequenceActivity.AnonymousClass6 */

                        public void onClick(View v) {
                            ClouData.getInstance().CleanData();
                        }
                    }, null);
                    normalDialog2.setOnDismissListener(new DialogInterface.OnDismissListener() {
                        /* class com.clou.rs350.ui.activity.TestSequenceActivity.AnonymousClass7 */

                        public void onDismiss(DialogInterface dialog) {
                            TestSequenceActivity.this.runtest();
                        }
                    });
                    normalDialog2.show();
                    normalDialog2.setTitle(R.string.text_clean_data);
                    return;
                }
            default:
                return;
        }
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private void save() {
        this.m_LoadingDialog = new LoadingDialog(this.m_Context);
        this.m_LoadingDialog.setCancelable(false);
        this.m_LoadingDialog.show();
        new MyTask(new ILoadCallback() {
            /* class com.clou.rs350.ui.activity.TestSequenceActivity.AnonymousClass9 */

            @Override // com.clou.rs350.callback.ILoadCallback
            public Object run() {
                SaveResult tmpResult = new SaveResult(TestSequenceActivity.this, null);
                tmpResult.result = ClouData.getInstance().saveAll();
                tmpResult.customerSr = ClouData.getInstance().getCustomerInfo().CustomerSr;
                tmpResult.testTime = ClouData.getInstance().getTestItem().TestTime;
                if (tmpResult.result == 0) {
                    ClouData.getInstance().CleanData();
                }
                String reportPath = (String.valueOf(tmpResult.customerSr) + "_" + tmpResult.testTime).replace(" ", PdfObject.NOTHING).replace("/", PdfObject.NOTHING).replace(":", PdfObject.NOTHING);
                String templet = Preferences.getString(Preferences.Key.EXPORTTEMPLATE, PdfObject.NOTHING);
                new ExportReport(TestSequenceActivity.this.m_Context).exportPdf(templet, reportPath, new LoadExportDataTool(TestSequenceActivity.this.m_Context).loadData(tmpResult.customerSr, new String[]{tmpResult.testTime}));
                return tmpResult;
            }

            @Override // com.clou.rs350.callback.ILoadCallback
            public void callback(Object result) {
                int id;
                TestSequenceActivity.this.m_LoadingDialog.dismiss();
                final SaveResult tmpResult = (SaveResult) result;
                switch (tmpResult.result) {
                    case 0:
                        id = R.string.text_save_success;
                        NormalDialog tmpDialog = new NormalDialog(TestSequenceActivity.this.m_Context);
                        tmpDialog.setOnClick(new View.OnClickListener() {
                            /* class com.clou.rs350.ui.activity.TestSequenceActivity.AnonymousClass9.AnonymousClass1 */

                            public void onClick(View v) {
                                Intent intent = new Intent(TestSequenceActivity.this.m_Context, DataDetailActivity.class);
                                intent.putExtra("CustomerSerialNo", tmpResult.customerSr);
                                intent.putExtra("TestTime", new String[]{tmpResult.testTime});
                                TestSequenceActivity.this.startActivityForResult(intent, 0);
                                TestSequenceActivity.this.finish();
                            }
                        }, null);
                        tmpDialog.show();
                        tmpDialog.setTitle(R.string.text_go_view_result);
                        break;
                    case 1:
                        id = R.string.text_customer_serial_no_is_empty;
                        break;
                    default:
                        id = R.string.text_save_fail;
                        break;
                }
                new HintDialog(TestSequenceActivity.this.m_Context, id).show();
            }
        }).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, new Integer[0]);
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private boolean checkTemplate() {
        String templatePath = Preferences.getString(Preferences.Key.EXPORTTEMPLATE, PdfObject.NOTHING);
        File tmpFile = new File(String.valueOf(CLApplication.app.getTemplateXmlPath()) + File.separator + templatePath);
        if (!OtherUtils.isEmpty(templatePath) && tmpFile.exists()) {
            return true;
        }
        SelectValueDialog tmpDialog = new SelectValueDialog(this.m_Context, FileUtils.getFileDir(CLApplication.app.getTemplateXmlPath()));
        tmpDialog.setWarning(R.string.text_select_template);
        tmpDialog.show();
        tmpDialog.setOnClick(new View.OnClickListener() {
            /* class com.clou.rs350.ui.activity.TestSequenceActivity.AnonymousClass10 */

            public void onClick(View v) {
                Preferences.putString(Preferences.Key.EXPORTTEMPLATE, v.getTag().toString());
                TestSequenceActivity.this.save();
            }
        }, null);
        tmpDialog.setTitle(R.string.text_select_template);
        return false;
    }

    private void setRuningButton(int Case) {
        int i;
        int i2;
        int i3;
        boolean z;
        boolean z2;
        boolean z3;
        int i4 = 4;
        boolean z4 = false;
        this.layout_SequenceSelection.setVisibility(Case == 0 ? View.VISIBLE : View.GONE);
        LinearLayout linearLayout = this.layout_SequenceTest;
        if (Case == 0) {
            i = 8;
        } else {
            i = 0;
        }
        linearLayout.setVisibility(i);
        Button button = this.btn_Next;
        if (Case == 0) {
            i2 = 4;
        } else {
            i2 = 0;
        }
        button.setVisibility(i2);
        Button button2 = this.btn_Previous;
        if (Case == 0) {
            i3 = 4;
        } else {
            i3 = 0;
        }
        button2.setVisibility(i3);
        Button button3 = this.btn_Run;
        if (!this.isRun) {
            i4 = 0;
        }
        button3.setVisibility(i4);
        this.btn_Back.setText(View.GONE == this.layout_SequenceSelection.getVisibility() ? R.string.text_back : R.string.text_main);
        Button button4 = this.btn_Back;
        if (2 == Case) {
            z = false;
        } else {
            z = true;
        }
        button4.setEnabled(z);
        Button button5 = this.btn_Next;
        if (2 == Case) {
            z2 = false;
        } else {
            z2 = true;
        }
        button5.setEnabled(z2);
        Button button6 = this.btn_Previous;
        if (2 == Case) {
            z3 = false;
        } else {
            z3 = true;
        }
        button6.setEnabled(z3);
        Button button7 = this.btn_Run;
        if (2 != Case) {
            z4 = true;
        }
        button7.setEnabled(z4);
        if (2 != Case) {
            setTestButton();
        }
    }

    @Override // com.clou.rs350.ui.activity.BaseActivity
    public void startOrStopChangeViewStatus(int flag) {
        int i;
        boolean z = true;
        super.startOrStopChangeViewStatus(flag);
        if (flag == 1) {
            i = 2;
        } else {
            i = 1;
        }
        setRuningButton(i);
        WiringAndRangeView wiringAndRangeView = this.wiringView;
        if (flag != 0) {
            z = false;
        }
        wiringAndRangeView.setEnabled(z);
    }

    private void setTestButton() {
        this.btn_Previous.setEnabled(this.m_TestIndex > 0);
        this.btn_Next.setText(this.m_TestIndex < this.m_TabAdapter.getCount() + -1 ? R.string.text_next : R.string.text_save);
    }

    @Override // com.clou.rs350.ui.activity.BaseActivity, com.clou.rs350.manager.MessageManager.IMessageListener
    public void onReceiveMessage(MeterBaseDevice Device) {
        this.m_BasicValue.parseRangeData(Device);
        this.wiringView.showData(this.m_BasicValue);
    }
}
