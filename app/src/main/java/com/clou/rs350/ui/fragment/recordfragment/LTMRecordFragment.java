package com.clou.rs350.ui.fragment.recordfragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import com.clou.rs350.CLApplication;
import com.clou.rs350.R;
import com.clou.rs350.callback.ILoadCallback;
import com.clou.rs350.db.dao.LTMDao;
import com.clou.rs350.db.model.IntegrationData;
import com.clou.rs350.db.model.LTM;
import com.clou.rs350.handler.HandlerSwitchView;
import com.clou.rs350.model.ClouData;
import com.clou.rs350.ui.dialog.HintDialog;
import com.clou.rs350.ui.dialog.LoadingDialog;
import com.clou.rs350.ui.dialog.NumericDialog;
import com.clou.rs350.ui.fragment.BaseFragment;
import com.clou.rs350.ui.model.LTMData;
import com.clou.rs350.ui.view.LTMGraphView;
import com.clou.rs350.utils.OtherUtils;
import com.itextpdf.text.pdf.PdfObject;
import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.view.annotation.ViewInject;
import com.lidroid.xutils.view.annotation.event.OnClick;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class LTMRecordFragment extends BaseFragment implements View.OnClickListener, HandlerSwitchView.IOnSelectCallback {
    @ViewInject(R.id.f)
    private TextView FTv;
    @ViewInject(R.id.in)
    private TextView INTv;
    @ViewInject(R.id.l1anglei)
    private TextView L1AngleITv;
    @ViewInject(R.id.l1angleui)
    private TextView L1AngleUITv;
    @ViewInject(R.id.l1angleu)
    private TextView L1AngleUTv;
    @ViewInject(R.id.txt_il1)
    private TextView L1ITv;
    @ViewInject(R.id.l1pf)
    private TextView L1PFTv;
    @ViewInject(R.id.l1p)
    private TextView L1PTv;
    @ViewInject(R.id.l1q)
    private TextView L1QTv;
    @ViewInject(R.id.l1s)
    private TextView L1STv;
    @ViewInject(R.id.l1thdi)
    private TextView L1THDITv;
    @ViewInject(R.id.l1thdu)
    private TextView L1THDUTv;
    @ViewInject(R.id.l1ull)
    private TextView L1ULLTv;
    @ViewInject(R.id.l1uln)
    private TextView L1ULNTv;
    @ViewInject(R.id.l2anglei)
    private TextView L2AngleITv;
    @ViewInject(R.id.l2angleui)
    private TextView L2AngleUITv;
    @ViewInject(R.id.l2angleu)
    private TextView L2AngleUTv;
    @ViewInject(R.id.txt_il2)
    private TextView L2ITv;
    @ViewInject(R.id.l2pf)
    private TextView L2PFTv;
    @ViewInject(R.id.l2p)
    private TextView L2PTv;
    @ViewInject(R.id.l2q)
    private TextView L2QTv;
    @ViewInject(R.id.l2s)
    private TextView L2STv;
    @ViewInject(R.id.l2thdi)
    private TextView L2THDITv;
    @ViewInject(R.id.l2thdu)
    private TextView L2THDUTv;
    @ViewInject(R.id.l2ull)
    private TextView L2ULLTv;
    @ViewInject(R.id.l2uln)
    private TextView L2ULNTv;
    @ViewInject(R.id.l3anglei)
    private TextView L3AngleITv;
    @ViewInject(R.id.l3angleui)
    private TextView L3AngleUITv;
    @ViewInject(R.id.l3angleu)
    private TextView L3AngleUTv;
    @ViewInject(R.id.txt_il3)
    private TextView L3ITv;
    @ViewInject(R.id.l3pf)
    private TextView L3PFTv;
    @ViewInject(R.id.l3p)
    private TextView L3PTv;
    @ViewInject(R.id.l3q)
    private TextView L3QTv;
    @ViewInject(R.id.l3s)
    private TextView L3STv;
    @ViewInject(R.id.l3thdi)
    private TextView L3THDITv;
    @ViewInject(R.id.l3thdu)
    private TextView L3THDUTv;
    @ViewInject(R.id.l3ull)
    private TextView L3ULLTv;
    @ViewInject(R.id.l3uln)
    private TextView L3ULNTv;
    @ViewInject(R.id.sumpf)
    private TextView SumPFTv;
    @ViewInject(R.id.sump)
    private TextView SumPTv;
    @ViewInject(R.id.sumq)
    private TextView SumQTv;
    @ViewInject(R.id.sums)
    private TextView SumSTv;
    @ViewInject(R.id.un)
    private TextView UNTv;
    @ViewInject(R.id.btn_configure)
    private Button btn_Configure;
    @ViewInject(R.id.btn_next)
    private Button btn_Next;
    @ViewInject(R.id.btn_previous)
    private Button btn_Previous;
    @ViewInject(R.id.btn_start)
    private Button btn_Start;
    @ViewInject(R.id.btn_switch)
    private Button btn_Switch;
    @ViewInject(R.id.chk_l1)
    private CheckBox chk_L1;
    @ViewInject(R.id.chk_l2)
    private CheckBox chk_L2;
    @ViewInject(R.id.chk_l3)
    private CheckBox chk_L3;
    private CheckBox[] chk_LAll;
    @ViewInject(R.id.chk_lsum)
    private CheckBox chk_LSum;
    @ViewInject(R.id.layout_index)
    private LinearLayout layout_Index;
    @ViewInject(R.id.layout_ltmgraph)
    private LinearLayout layout_graph;
    @ViewInject(R.id.layout_ltmtable)
    private LinearLayout layout_table;
    boolean m_GraphFlag = false;
    private int m_GraphItem = 0;
    private int m_GraphStyle = 0;
    private int m_Index = 0;
    LTM m_LTM;
    private int m_Wiring = 0;
    @ViewInject(R.id.seekbar_progress)
    private SeekBar pSeekBar;
    private HandlerSwitchView switchView;
    @ViewInject(R.id.txt_index)
    private TextView txt_Index;
    @ViewInject(R.id.txt_lnbehind)
    private TextView txt_LnBehind;
    @ViewInject(R.id.txt_lnfront)
    private TextView txt_LnFront;
    @ViewInject(R.id.txt_time_stamp)
    private TextView txt_TimeStamp;
    @ViewInject(R.id.txt_total)
    private TextView txt_Total;
    @ViewInject(R.id.ltmview_graph)
    private LTMGraphView view_Graph;

    public LTMRecordFragment(LTM Data) {
        this.m_LTM = Data;
    }

    @Override // android.support.v4.app.Fragment
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        this.fragmentView = inflater.inflate(R.layout.fragment_record_long_term_measurement, (ViewGroup) null);
        ViewUtils.inject(this, this.fragmentView);
        this.pSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            /* class com.clou.rs350.ui.fragment.recordfragment.LTMRecordFragment.AnonymousClass1 */

            public void onStopTrackingTouch(SeekBar seekBar) {
                LTMRecordFragment.this.showProgressValue(LTMRecordFragment.this.pSeekBar.getProgress());
            }

            public void onStartTrackingTouch(SeekBar seekBar) {
            }

            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
            }
        });
        return this.fragmentView;
    }

    private void initView() {
        List<View> tabViews = new ArrayList<>();
        tabViews.add(findViewById(R.id.btn_uln));
        tabViews.add(findViewById(R.id.btn_ull));
        tabViews.add(findViewById(R.id.btn_i));
        tabViews.add(findViewById(R.id.btn_p));
        tabViews.add(findViewById(R.id.btn_q));
        tabViews.add(findViewById(R.id.btn_s));
        tabViews.add(findViewById(R.id.btn_pf));
        tabViews.add(findViewById(R.id.btn_f));
        tabViews.add(findViewById(R.id.btn_thdu));
        tabViews.add(findViewById(R.id.btn_thdi));
        this.switchView = new HandlerSwitchView(this.m_Context);
        this.switchView.setTabViews(tabViews);
        this.switchView.boundClick();
        this.switchView.setOnTabChangedCallback(this);
        this.switchView.selectView(0);
        this.chk_LAll = new CheckBox[]{this.chk_L1, this.chk_L2, this.chk_L3, this.chk_LSum};
        int[] Colors = ClouData.getInstance().getSettingBasic().PhaseColors;
        this.chk_L1.setTextColor(Colors[0]);
        this.chk_L2.setTextColor(Colors[1]);
        this.chk_L3.setTextColor(Colors[2]);
    }

    @Override // android.support.v4.app.Fragment, com.clou.rs350.ui.fragment.BaseFragment
    public void onResume() {
        super.onResume();
        refreshData();
    }

    @Override // android.support.v4.app.Fragment, com.clou.rs350.ui.fragment.BaseFragment
    public void onPause() {
        super.onPause();
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private void showProgressValue(int progress) {
        boolean z = true;
        this.m_Index = progress;
        this.pSeekBar.setMax(this.m_LTM.IntegrationDatas.size() - 1);
        this.pSeekBar.setProgress(progress);
        this.txt_Index.setText(new StringBuilder(String.valueOf(progress + 1)).toString());
        this.txt_Total.setText(new StringBuilder(String.valueOf(this.m_LTM.IntegrationDatas.size())).toString());
        this.btn_Previous.setEnabled(progress > 0);
        Button button = this.btn_Next;
        if (progress >= this.m_LTM.IntegrationDatas.size() - 1) {
            z = false;
        }
        button.setEnabled(z);
        if (this.m_LTM.IntegrationDatas.size() > 0) {
            showData(this.m_LTM.IntegrationDatas.get(progress));
            return;
        }
        showData(new IntegrationData());
        this.txt_Index.setText("0");
    }

    private void showGraphData() {
        LTMData[] tmpData;
        int[] tmpColors = ClouData.getInstance().getSettingBasic().PhaseColors;
        int[] Colors = {tmpColors[0], tmpColors[1], tmpColors[2], -1};
        switch (this.m_GraphItem) {
            case 1:
                tmpData = new LTMData[]{this.m_LTM.VoltageLTML1L2, this.m_LTM.VoltageLTML2L3, this.m_LTM.VoltageLTML3L1, this.m_LTM.VoltageLTMLN};
                break;
            case 2:
                tmpData = new LTMData[]{this.m_LTM.CurrentLTML1, this.m_LTM.CurrentLTML2, this.m_LTM.CurrentLTML3, this.m_LTM.CurrentLTMLN};
                break;
            case 3:
                tmpData = new LTMData[]{this.m_LTM.ActivePowerLTML1, this.m_LTM.ActivePowerLTML2, this.m_LTM.ActivePowerLTML3, this.m_LTM.ActivePowerLTMTotal};
                break;
            case 4:
                tmpData = new LTMData[]{this.m_LTM.ReactivePowerLTML1, this.m_LTM.ReactivePowerLTML2, this.m_LTM.ReactivePowerLTML3, this.m_LTM.ReactivePowerLTMTotal};
                break;
            case 5:
                tmpData = new LTMData[]{this.m_LTM.ApparentPowerLTML1, this.m_LTM.ApparentPowerLTML2, this.m_LTM.ApparentPowerLTML3, this.m_LTM.ApparentPowerLTMTotal};
                break;
            case 6:
                tmpData = new LTMData[]{this.m_LTM.PowerFactorLTML1, this.m_LTM.PowerFactorLTML2, this.m_LTM.PowerFactorLTML3, this.m_LTM.PowerFactorLTMTotal};
                break;
            case 7:
                tmpData = new LTMData[]{this.m_LTM.VoltageLTML1L2, this.m_LTM.VoltageLTML2, this.m_LTM.VoltageLTML3, this.m_LTM.FrequencyLTM};
                break;
            case 8:
                tmpData = new LTMData[]{this.m_LTM.THDU1LTM, this.m_LTM.THDU2LTM, this.m_LTM.THDU3LTM};
                break;
            case 9:
                tmpData = new LTMData[]{this.m_LTM.THDI1LTM, this.m_LTM.THDI2LTM, this.m_LTM.THDI3LTM};
                break;
            default:
                tmpData = new LTMData[]{this.m_LTM.VoltageLTML1, this.m_LTM.VoltageLTML2, this.m_LTM.VoltageLTML3, this.m_LTM.VoltageLTMLN};
                break;
        }
        List<LTMData> tmpList = new ArrayList<>();
        for (int i = 0; i < this.chk_LAll.length; i++) {
            if (this.chk_LAll[i].isChecked()) {
                tmpData[i].color = Colors[i];
                tmpList.add(tmpData[i]);
            }
        }
        this.view_Graph.refreshData(tmpList, this.m_LTM.Times);
    }

    @Override // com.clou.rs350.ui.fragment.BaseFragment
    public void refreshData() {
        switchTableGraph(this.m_GraphFlag);
        if (this.m_GraphFlag) {
            showGraphData();
        } else {
            showProgressValue(this.m_LTM.IntegrationDatas.size() - 1);
        }
    }

    private void showData(IntegrationData tmpMeasureValue) {
        this.L1ULNTv.setText(tmpMeasureValue.VoltageValueL1.s_Value);
        this.L2ULNTv.setText(tmpMeasureValue.VoltageValueL2.s_Value);
        this.L3ULNTv.setText(tmpMeasureValue.VoltageValueL3.s_Value);
        this.L1ULLTv.setText(tmpMeasureValue.VoltageValueL1L2.s_Value);
        this.L2ULLTv.setText(tmpMeasureValue.VoltageValueL2L3.s_Value);
        this.L3ULLTv.setText(tmpMeasureValue.VoltageValueL3L1.s_Value);
        if (1 != this.m_Wiring) {
            this.L1AngleUTv.setText(tmpMeasureValue.VoltageAngleL1.s_Value);
            this.L2AngleUTv.setText(tmpMeasureValue.VoltageAngleL2.s_Value);
            this.L3AngleUTv.setText(tmpMeasureValue.VoltageAngleL3.s_Value);
            this.L1AngleUITv.setText(tmpMeasureValue.VoltageCurrentAngleL1.s_Value);
            this.L2AngleUITv.setText(tmpMeasureValue.VoltageCurrentAngleL2.s_Value);
            this.L3AngleUITv.setText(tmpMeasureValue.VoltageCurrentAngleL3.s_Value);
        } else {
            this.L1AngleUTv.setText(tmpMeasureValue.VoltageAngleL1L2.s_Value);
            this.L2AngleUTv.setText(tmpMeasureValue.VoltageAngleL2L3.s_Value);
            this.L3AngleUTv.setText(tmpMeasureValue.VoltageAngleL3L1.s_Value);
            this.L1AngleUITv.setText(tmpMeasureValue.AngleU12I1.s_Value);
            this.L2AngleUITv.setText(tmpMeasureValue.AngleU23I2.s_Value);
            this.L3AngleUITv.setText(tmpMeasureValue.AngleU31I3.s_Value);
        }
        this.L1THDUTv.setText(tmpMeasureValue.THDU1.s_Value);
        this.L2THDUTv.setText(tmpMeasureValue.THDU2.s_Value);
        this.L3THDUTv.setText(tmpMeasureValue.THDU3.s_Value);
        this.L1ITv.setText(tmpMeasureValue.CurrentValueL1.s_Value);
        this.L2ITv.setText(tmpMeasureValue.CurrentValueL2.s_Value);
        this.L3ITv.setText(tmpMeasureValue.CurrentValueL3.s_Value);
        this.L1AngleITv.setText(tmpMeasureValue.CurrentAngleL1.s_Value);
        this.L2AngleITv.setText(tmpMeasureValue.CurrentAngleL2.s_Value);
        this.L3AngleITv.setText(tmpMeasureValue.CurrentAngleL3.s_Value);
        this.L1THDITv.setText(tmpMeasureValue.THDI1.s_Value);
        this.L2THDITv.setText(tmpMeasureValue.THDI2.s_Value);
        this.L3THDITv.setText(tmpMeasureValue.THDI3.s_Value);
        this.L1PFTv.setText(tmpMeasureValue.PowerFactorL1.s_Value);
        this.L2PFTv.setText(tmpMeasureValue.PowerFactorL2.s_Value);
        this.L3PFTv.setText(tmpMeasureValue.PowerFactorL3.s_Value);
        this.SumPFTv.setText(tmpMeasureValue.PowerFactorTotal.s_Value);
        this.L1PTv.setText(tmpMeasureValue.ActivePowerL1.s_Value);
        this.L2PTv.setText(tmpMeasureValue.ActivePowerL2.s_Value);
        this.L3PTv.setText(tmpMeasureValue.ActivePowerL3.s_Value);
        this.SumPTv.setText(tmpMeasureValue.ActivePowerTotal.s_Value);
        this.L1QTv.setText(tmpMeasureValue.ReactivePowerL1.s_Value);
        this.L2QTv.setText(tmpMeasureValue.ReactivePowerL2.s_Value);
        this.L3QTv.setText(tmpMeasureValue.ReactivePowerL3.s_Value);
        this.SumQTv.setText(tmpMeasureValue.ReactivePowerTotal.s_Value);
        this.L1STv.setText(tmpMeasureValue.ApparentPowerL1.s_Value);
        this.L2STv.setText(tmpMeasureValue.ApparentPowerL2.s_Value);
        this.L3STv.setText(tmpMeasureValue.ApparentPowerL3.s_Value);
        this.SumSTv.setText(tmpMeasureValue.ApparentPowerTotal.s_Value);
        this.UNTv.setText(tmpMeasureValue.VoltageLN.s_Value);
        this.INTv.setText(tmpMeasureValue.CurrentLN.s_Value);
        this.FTv.setText(tmpMeasureValue.Frequency.s_Value);
        this.txt_TimeStamp.setText(tmpMeasureValue.TimeStamp.s_Value);
    }

    @OnClick({R.id.layout_index, R.id.btn_switch, R.id.chk_l1, R.id.chk_l2, R.id.chk_l3, R.id.chk_lsum, R.id.btn_style, R.id.btn_export, R.id.btn_previous, R.id.btn_next})
    public void onClick(View v) {
        int i = 0;
        boolean z = false;
        switch (v.getId()) {
            case R.id.btn_export:
                new LoadingDialog(this.m_Context, new ILoadCallback() {
                    /* class com.clou.rs350.ui.fragment.recordfragment.LTMRecordFragment.AnonymousClass3 */

                    @Override // com.clou.rs350.callback.ILoadCallback
                    public Object run() {
                        String exportPath = CLApplication.app.getLTMExportPath();
                        new LTMDao(LTMRecordFragment.this.m_Context).exportDataToCSV(LTMRecordFragment.this.m_LTM.SerialNo, LTMRecordFragment.this.m_LTM.SaveTime, String.valueOf(exportPath) + File.separator + ("LTM" + LTMRecordFragment.this.m_LTM.SerialNo + "_" + LTMRecordFragment.this.m_LTM.SaveTime + ".csv").replace(" ", PdfObject.NOTHING).replace("/", PdfObject.NOTHING).replace(":", PdfObject.NOTHING));
                        CLApplication.app.refreshPath(exportPath);
                        return null;
                    }

                    @Override // com.clou.rs350.callback.ILoadCallback
                    public void callback(Object result) {
                        new HintDialog(LTMRecordFragment.this.m_Context, (int) R.string.text_export_finish).show();
                    }
                }).show();
                return;
            case R.id.btn_previous:
                this.m_Index--;
                showProgressValue(this.m_Index);
                return;
            case R.id.btn_next:
                this.m_Index++;
                showProgressValue(this.m_Index);
                return;
            case R.id.btn_style:
                if (this.m_GraphStyle != 1) {
                    i = 1;
                }
                this.m_GraphStyle = i;
                this.view_Graph.setStyle(this.m_GraphStyle);
                return;
            case R.id.layout_index:
                NumericDialog numDialog = new NumericDialog(this.m_Context);
                numDialog.setMax((double) this.m_LTM.IntegrationDatas.size());
                numDialog.setMin(1.0d);
                numDialog.m_Dot = false;
                numDialog.setMinus(false);
                numDialog.setOnDoneClick(new View.OnClickListener() {
                    /* class com.clou.rs350.ui.fragment.recordfragment.LTMRecordFragment.AnonymousClass2 */

                    public void onClick(View v) {
                        if (!OtherUtils.isEmpty(LTMRecordFragment.this.txt_Index)) {
                            LTMRecordFragment.this.m_Index = Integer.parseInt(LTMRecordFragment.this.txt_Index.getText().toString().trim()) - 1;
                            LTMRecordFragment.this.showProgressValue(LTMRecordFragment.this.m_Index);
                        }
                    }
                });
                numDialog.showMyDialog(this.txt_Index);
                return;
            case R.id.chk_l1:
            case R.id.chk_l2:
            case R.id.chk_l3:
            case R.id.chk_lsum:
                showGraphData();
                return;
            case R.id.btn_switch:
                if (!this.m_GraphFlag) {
                    z = true;
                }
                this.m_GraphFlag = z;
                switchTableGraph(this.m_GraphFlag);
                if (this.m_GraphFlag) {
                    showGraphData();
                    return;
                } else {
                    showProgressValue(this.m_LTM.IntegrationDatas.size() - 1);
                    return;
                }
            default:
                return;
        }
    }

    private void switchTableGraph(boolean flag) {
        int i;
        int i2 = 8;
        LinearLayout linearLayout = this.layout_graph;
        if (flag) {
            i = 0;
        } else {
            i = 8;
        }
        linearLayout.setVisibility(i);
        LinearLayout linearLayout2 = this.layout_table;
        if (!flag) {
            i2 = 0;
        }
        linearLayout2.setVisibility(i2);
        this.btn_Switch.setText(flag ? R.string.text_table : R.string.text_graph);
    }

    @Override // android.support.v4.app.Fragment, com.clou.rs350.ui.fragment.BaseFragment
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initView();
    }

    @Override // com.clou.rs350.handler.HandlerSwitchView.IOnSelectCallback
    public void onSelectedChanged(int index) {
        setGraphItem(index);
        this.m_GraphItem = index;
        showGraphData();
    }

    private void setGraphItem(int item) {
        int i;
        boolean z;
        boolean z2;
        boolean z3;
        boolean z4;
        this.txt_LnFront.setText(item > 2 ? R.string.text_phase_sum : R.string.text_l);
        TextView textView = this.txt_LnBehind;
        if (item > 2) {
            i = 4;
        } else {
            i = 0;
        }
        textView.setVisibility(i);
        CheckBox checkBox = this.chk_LSum;
        if (item < 7) {
            z = true;
        } else {
            z = false;
        }
        checkBox.setEnabled(z);
        CheckBox checkBox2 = this.chk_L1;
        if (item != 7) {
            z2 = true;
        } else {
            z2 = false;
        }
        checkBox2.setEnabled(z2);
        CheckBox checkBox3 = this.chk_L2;
        if (item != 7) {
            z3 = true;
        } else {
            z3 = false;
        }
        checkBox3.setEnabled(z3);
        CheckBox checkBox4 = this.chk_L3;
        if (item != 7) {
            z4 = true;
        } else {
            z4 = false;
        }
        checkBox4.setEnabled(z4);
        if (item > 7) {
            this.chk_LSum.setChecked(false);
        }
        if (7 == item) {
            this.chk_L1.setChecked(false);
            this.chk_L2.setChecked(false);
            this.chk_L3.setChecked(false);
            this.chk_LSum.setChecked(true);
        }
        if (7 == this.m_GraphItem) {
            this.chk_L1.setChecked(true);
            this.chk_L2.setChecked(true);
            this.chk_L3.setChecked(true);
        }
    }

    @Override // com.clou.rs350.handler.HandlerSwitchView.IOnSelectCallback
    public void onClickView(View view) {
    }
}
