package com.clou.rs350.ui.popwindow;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.PopupWindow;
import android.widget.TextView;

import com.clou.rs350.R;
import com.clou.rs350.db.model.DBDataModel;
import com.clou.rs350.db.model.SiteData;
import com.clou.rs350.ui.dialog.NumericDialog;
import com.clou.rs350.utils.InfoUtil;
import com.clou.rs350.utils.OtherUtils;

public class TrxRatioPopWindow extends PopupWindow implements View.OnClickListener, CompoundButton.OnCheckedChangeListener {
    CheckBox chk_CTREnable;
    CheckBox chk_DemandTestEnable;
    CheckBox chk_EnergyTestEnable;
    CheckBox chk_ErrorTestEnable;
    CheckBox chk_IPEnable;
    CheckBox chk_Test_at_Primary;
    CheckBox chk_Test_at_Secondary;
    CheckBox chk_VTREnable;
    CheckBox chk_p3;
    CheckBox chk_s3;
    View.OnClickListener mOnClick;
    Context m_Context;
    SiteData m_Info;
    TextView txt_CTR_Primary;
    TextView txt_CTR_Secondary;
    TextView txt_VTR_Primary;
    TextView txt_VTR_Secondary;

    public TrxRatioPopWindow(Context context) {
        super(context);
        this.m_Context = context;
        initView();
    }

    private void initView() {
        View conentView = ((LayoutInflater) this.m_Context.getSystemService("layout_inflater")).inflate(R.layout.pop_window_txrratio, (ViewGroup) null);
        conentView.findViewById(R.id.ok).setOnClickListener(this);
        conentView.findViewById(R.id.cancel).setOnClickListener(this);
        this.txt_VTR_Primary = (TextView) conentView.findViewById(R.id.txt_vtr_primary);
        this.txt_VTR_Primary.setOnClickListener(this);
        this.txt_VTR_Secondary = (TextView) conentView.findViewById(R.id.txt_vtr_secondary);
        this.txt_VTR_Secondary.setOnClickListener(this);
        this.txt_CTR_Primary = (TextView) conentView.findViewById(R.id.txt_ctr_primary);
        this.txt_CTR_Primary.setOnClickListener(this);
        this.txt_CTR_Secondary = (TextView) conentView.findViewById(R.id.txt_ctr_secondary);
        this.txt_CTR_Secondary.setOnClickListener(this);
        this.chk_Test_at_Primary = (CheckBox) conentView.findViewById(R.id.chk_primary);
        this.chk_Test_at_Secondary = (CheckBox) conentView.findViewById(R.id.chk_secondary);
        this.chk_Test_at_Primary.setOnClickListener(this);
        this.chk_Test_at_Secondary.setOnClickListener(this);
        this.chk_VTREnable = (CheckBox) conentView.findViewById(R.id.chk_vtr);
        this.chk_CTREnable = (CheckBox) conentView.findViewById(R.id.chk_ctr);
        this.chk_IPEnable = (CheckBox) conentView.findViewById(R.id.chk_ipenable);
        this.chk_ErrorTestEnable = (CheckBox) conentView.findViewById(R.id.chk_errortestenable);
        this.chk_DemandTestEnable = (CheckBox) conentView.findViewById(R.id.chk_demandtestenable);
        this.chk_EnergyTestEnable = (CheckBox) conentView.findViewById(R.id.chk_energytestenable);
        this.chk_p3 = (CheckBox) conentView.findViewById(R.id.chk_p3);
        this.chk_s3 = (CheckBox) conentView.findViewById(R.id.chk_s3);
        this.chk_p3.setOnCheckedChangeListener(this);
        this.chk_s3.setOnCheckedChangeListener(this);
        setContentView(conentView);
        setWidth(this.m_Context.getResources().getDimensionPixelSize(R.dimen.dp_480));
        setHeight(this.m_Context.getResources().getDimensionPixelSize(R.dimen.dp_200));
        setFocusable(true);
        setOutsideTouchable(true);
        update();
        setBackgroundDrawable(new ColorDrawable(0));
        setAnimationStyle(16973826);
    }

    private void showValue() {
        boolean z;
        boolean z2;
        boolean z3;
        boolean z4;
        boolean z5;
        boolean z6;
        boolean z7 = true;
        this.chk_VTREnable.setChecked(this.m_Info.PTEnable == 0);
        CheckBox checkBox = this.chk_CTREnable;
        if (this.m_Info.CTEnable == 0) {
            z = true;
        } else {
            z = false;
        }
        checkBox.setChecked(z);
        CheckBox checkBox2 = this.chk_Test_at_Primary;
        if (1 == this.m_Info.Measurement_Position) {
            z2 = true;
        } else {
            z2 = false;
        }
        checkBox2.setChecked(z2);
        CheckBox checkBox3 = this.chk_Test_at_Secondary;
        if (this.m_Info.Measurement_Position == 0) {
            z3 = true;
        } else {
            z3 = false;
        }
        checkBox3.setChecked(z3);
        if (this.m_Info.PTPrimary.s_Value.contains("/")) {
            this.chk_p3.setChecked(true);
            this.chk_s3.setChecked(true);
            if (!OtherUtils.isEmpty(this.m_Info.PTPrimary.s_Value)) {
                this.txt_VTR_Primary.setText(this.m_Info.PTPrimary.s_Value.substring(0, this.m_Info.PTPrimary.s_Value.length() - 5));
            }
            if (!OtherUtils.isEmpty(this.m_Info.PTSecondary.s_Value)) {
                this.txt_VTR_Secondary.setText(this.m_Info.PTSecondary.s_Value.substring(0, this.m_Info.PTSecondary.s_Value.length() - 4));
            }
        } else {
            this.chk_p3.setChecked(false);
            this.chk_s3.setChecked(false);
            if (!OtherUtils.isEmpty(this.m_Info.PTPrimary.s_Value)) {
                this.txt_VTR_Primary.setText(this.m_Info.PTPrimary.s_Value.substring(0, this.m_Info.PTPrimary.s_Value.length() - 2));
            }
            if (!OtherUtils.isEmpty(this.m_Info.PTSecondary.s_Value)) {
                this.txt_VTR_Secondary.setText(this.m_Info.PTSecondary.s_Value.substring(0, this.m_Info.PTSecondary.s_Value.length() - 1));
            }
        }
        if (!OtherUtils.isEmpty(this.m_Info.CTPrimary.s_Value)) {
            this.txt_CTR_Primary.setText(this.m_Info.CTPrimary.s_Value.substring(0, this.m_Info.CTPrimary.s_Value.length() - 1));
        }
        if (!OtherUtils.isEmpty(this.m_Info.CTSecondary.s_Value)) {
            this.txt_CTR_Secondary.setText(this.m_Info.CTSecondary.s_Value.substring(0, this.m_Info.CTSecondary.s_Value.length() - 1));
        }
        CheckBox checkBox4 = this.chk_IPEnable;
        if ((this.m_Info.RatioApply & 1) == 1) {
            z4 = true;
        } else {
            z4 = false;
        }
        checkBox4.setChecked(z4);
        CheckBox checkBox5 = this.chk_ErrorTestEnable;
        if ((this.m_Info.RatioApply & 2) == 2) {
            z5 = true;
        } else {
            z5 = false;
        }
        checkBox5.setChecked(z5);
        CheckBox checkBox6 = this.chk_DemandTestEnable;
        if ((this.m_Info.RatioApply & 4) == 4) {
            z6 = true;
        } else {
            z6 = false;
        }
        checkBox6.setChecked(z6);
        CheckBox checkBox7 = this.chk_EnergyTestEnable;
        if ((this.m_Info.RatioApply & 8) != 8) {
            z7 = false;
        }
        checkBox7.setChecked(z7);
    }

    private void praseValue() {
        int i;
        int i2;
        int i3;
        int i4;
        int i5 = 1;
        int i6 = 0;
        this.m_Info.PTEnable = this.chk_VTREnable.isChecked() ? 0 : 1;
        SiteData siteData = this.m_Info;
        if (this.chk_CTREnable.isChecked()) {
            i = 0;
        } else {
            i = 1;
        }
        siteData.CTEnable = i;
        SiteData siteData2 = this.m_Info;
        if (this.chk_Test_at_Secondary.isChecked()) {
            i2 = 0;
        } else {
            i2 = 1;
        }
        siteData2.Measurement_Position = i2;
        this.m_Info.PTPrimary.s_Value = String.valueOf(this.txt_VTR_Primary.getText().toString()) + "kV";
        this.m_Info.PTSecondary.s_Value = String.valueOf(this.txt_VTR_Secondary.getText().toString()) + "V";
        if (this.chk_p3.isChecked()) {
            DBDataModel dBDataModel = this.m_Info.PTPrimary;
            dBDataModel.s_Value = String.valueOf(dBDataModel.s_Value) + "/√3";
            DBDataModel dBDataModel2 = this.m_Info.PTSecondary;
            dBDataModel2.s_Value = String.valueOf(dBDataModel2.s_Value) + "/√3";
        }
        this.m_Info.PTPrimary = InfoUtil.parseVoltageRatio(this.m_Info.PTPrimary.s_Value, true);
        this.m_Info.PTSecondary = InfoUtil.parseVoltageRatio(this.m_Info.PTSecondary.s_Value, false);
        this.m_Info.CTPrimary.s_Value = String.valueOf(this.txt_CTR_Primary.getText().toString()) + "A";
        this.m_Info.CTSecondary.s_Value = String.valueOf(this.txt_CTR_Secondary.getText().toString()) + "A";
        this.m_Info.CTPrimary = InfoUtil.parseCurrentRatio(this.m_Info.CTPrimary.s_Value);
        this.m_Info.CTSecondary = InfoUtil.parseCurrentRatio(this.m_Info.CTSecondary.s_Value);
        this.m_Info.RatioApply = 0;
        SiteData siteData3 = this.m_Info;
        int i7 = siteData3.RatioApply;
        if (!this.chk_IPEnable.isChecked()) {
            i5 = 0;
        }
        siteData3.RatioApply = i5 | i7;
        SiteData siteData4 = this.m_Info;
        int i8 = siteData4.RatioApply;
        if (this.chk_ErrorTestEnable.isChecked()) {
            i3 = 2;
        } else {
            i3 = 0;
        }
        siteData4.RatioApply = i3 | i8;
        SiteData siteData5 = this.m_Info;
        int i9 = siteData5.RatioApply;
        if (this.chk_DemandTestEnable.isChecked()) {
            i4 = 4;
        } else {
            i4 = 0;
        }
        siteData5.RatioApply = i4 | i9;
        SiteData siteData6 = this.m_Info;
        int i10 = siteData6.RatioApply;
        if (this.chk_EnergyTestEnable.isChecked()) {
            i6 = 8;
        }
        siteData6.RatioApply = i6 | i10;
    }

    public void showRatioPopWindow(View currentClick, SiteData Info, View.OnClickListener onclick) {
        this.mOnClick = onclick;
        this.m_Info = Info;
        showValue();
        if (!isShowing()) {
            int[] location = new int[2];
            currentClick.getLocationOnScreen(location);
            showAtLocation(currentClick, 0, location[0] - getWidth(), location[1]);
            return;
        }
        dismiss();
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ok:
                praseValue();
                InfoUtil.setRatio(this.m_Info, this.m_Context);
                if (this.mOnClick != null) {
                    this.mOnClick.onClick(v);
                }
                if (isShowing()) {
                    dismiss();
                    return;
                }
                return;
            case R.id.cancel:
                if (isShowing()) {
                    dismiss();
                    return;
                }
                return;
            case R.id.chk_primary:
                this.chk_Test_at_Primary.setChecked(true);
                this.chk_Test_at_Secondary.setChecked(false);
                return;
            case R.id.chk_secondary:
                this.chk_Test_at_Primary.setChecked(false);
                this.chk_Test_at_Secondary.setChecked(true);
                return;
            case R.id.txt_vtr_primary:
            case R.id.txt_vtr_secondary:
            case R.id.txt_ctr_primary:
            case R.id.txt_ctr_secondary:
                NumericDialog nd = new NumericDialog(this.m_Context);
                nd.txtLimit = 6;
                nd.showMyDialog((TextView) v);
                return;
            default:
                return;
        }
    }

    public void onCheckedChanged(CompoundButton v, boolean isChecked) {
        switch (v.getId()) {
            case R.id.chk_vtr:
            case R.id.txt_vtr_primary:
            case R.id.txt_vtr_secondary:
            default:
                return;
            case R.id.chk_p3:
            case R.id.chk_s3:
                this.chk_p3.setChecked(isChecked);
                this.chk_s3.setChecked(isChecked);
                return;
        }
    }
}
