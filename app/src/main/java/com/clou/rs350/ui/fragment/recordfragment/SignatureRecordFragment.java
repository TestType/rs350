package com.clou.rs350.ui.fragment.recordfragment;

import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.clou.rs350.CLApplication;
import com.clou.rs350.R;
import com.clou.rs350.callback.ILoadCallback;
import com.clou.rs350.db.model.sequence.SignatureData;
import com.clou.rs350.task.MyTask;
import com.clou.rs350.ui.fragment.BaseFragment;

import java.io.File;

public class SignatureRecordFragment extends BaseFragment implements View.OnClickListener {
    private Button btn_Next;
    private Button btn_Previous;
    private ImageView imageView;
    SignatureData m_Data;
    private File m_ImageFile;
    private int m_Index;
    private TextView txt_Index;
    private TextView txt_Name;
    private TextView txt_Title;
    private TextView txt_Total;

    public SignatureRecordFragment(SignatureData data) {
        this.m_Data = data;
    }

    @Override // android.support.v4.app.Fragment
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        this.fragmentView = inflater.inflate(R.layout.fragment_record_signature, (ViewGroup) null);
        return this.fragmentView;
    }

    @Override // android.support.v4.app.Fragment, com.clou.rs350.ui.fragment.BaseFragment
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initView();
    }

    private void initView() {
        this.txt_Title = (TextView) findViewById(R.id.txt_title);
        this.imageView = (ImageView) findViewById(R.id.v_signature);
        this.txt_Index = (TextView) findViewById(R.id.txt_index);
        this.txt_Total = (TextView) findViewById(R.id.txt_total);
        this.btn_Previous = (Button) findViewById(R.id.btn_previous);
        this.txt_Name = (TextView) findViewById(R.id.txt_name);
        this.btn_Previous.setOnClickListener(this);
        this.btn_Next = (Button) findViewById(R.id.btn_next);
        this.btn_Next.setOnClickListener(this);
    }

    @Override // android.support.v4.app.Fragment, com.clou.rs350.ui.fragment.BaseFragment
    public void onResume() {
        showData();
        super.onResume();
    }

    private void setInitImageView(final String Path) {
        new MyTask(new ILoadCallback() {
            /* class com.clou.rs350.ui.fragment.recordfragment.SignatureRecordFragment.AnonymousClass1 */

            @Override // com.clou.rs350.callback.ILoadCallback
            public Object run() {
                SignatureRecordFragment.this.m_ImageFile = new File(Path);
                if (SignatureRecordFragment.this.m_ImageFile.exists()) {
                    return null;
                }
                System.out.println("init image-->1");
                return null;
            }

            @Override // com.clou.rs350.callback.ILoadCallback
            public void callback(Object result) {
                if (SignatureRecordFragment.this.m_ImageFile == null) {
                    SignatureRecordFragment.this.imageView.setImageURI(null);
                } else {
                    SignatureRecordFragment.this.imageView.setImageURI(Uri.fromFile(SignatureRecordFragment.this.m_ImageFile));
                }
            }
        }).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, new Integer[0]);
    }

    @Override // android.support.v4.app.Fragment, com.clou.rs350.ui.fragment.BaseFragment
    public void onPause() {
        super.onPause();
    }

    /* access modifiers changed from: protected */
    @Override // com.clou.rs350.ui.fragment.BaseFragment
    public void copyData() {
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_previous:
                this.m_Index--;
                showData();
                return;
            case R.id.btn_next:
                this.m_Index++;
                showData();
                return;
            default:
                return;
        }
    }

    private void setButton() {
        boolean z;
        boolean z2 = true;
        Button button = this.btn_Previous;
        if (this.m_Index != 0) {
            z = true;
        } else {
            z = false;
        }
        button.setEnabled(z);
        Button button2 = this.btn_Next;
        if (this.m_Index >= this.m_Data.Signatures.size() - 1) {
            z2 = false;
        }
        button2.setEnabled(z2);
    }

    private void showData() {
        this.txt_Index.setText(new StringBuilder(String.valueOf(this.m_Index + 1)).toString());
        this.txt_Total.setText(new StringBuilder(String.valueOf(this.m_Data.Signatures.size())).toString());
        this.txt_Name.setText(this.m_Data.Signatures.get(this.m_Index).Name);
        this.txt_Title.setText(this.m_Data.Title);
        setInitImageView(String.valueOf(CLApplication.app.getImagePath()) + File.separator + this.m_Data.Signatures.get(this.m_Index).SignaturePath);
        setButton();
    }
}
