package com.clou.rs350.ui.view.infoview;

import android.content.Context;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;

import com.clou.rs350.R;
import com.clou.rs350.db.dao.CustomerInfoDao;
import com.clou.rs350.db.dao.MeterTypeDao;
import com.clou.rs350.db.dao.SiteDataManagerDao;
import com.clou.rs350.db.model.DBDataModel;
import com.clou.rs350.db.model.MeterType;
import com.clou.rs350.db.model.SiteData;
import com.clou.rs350.db.model.sequence.FieldItem;
import com.clou.rs350.db.model.sequence.FieldSetting;
import com.clou.rs350.ui.dialog.HintDialog;
import com.clou.rs350.ui.dialog.MeterTypeInfoDialog;
import com.clou.rs350.ui.dialog.NumericDialog;
import com.clou.rs350.ui.popwindow.ListPopWindow;
import com.clou.rs350.ui.popwindow.YearSettingPopWindow;
import com.clou.rs350.utils.DoubleClick;
import com.clou.rs350.utils.FieldSettingUtil;
import com.clou.rs350.utils.InfoUtil;
import com.clou.rs350.utils.OtherUtils;
import com.itextpdf.text.pdf.PdfObject;

import java.util.ArrayList;
import java.util.List;

import google.zxing.camera.CaptureDialog;

public class SiteDataView implements View.OnClickListener, CompoundButton.OnCheckedChangeListener {
    View btn_Meter1_Scan;
    View btn_Meter1_Type_Select;
    View btn_Meter2_Scan;
    View btn_Meter2_Type_Select;
    View btn_Meter3_Scan;
    View btn_Meter3_Type_Select;
    LinearLayout btn_Scan;
    LinearLayout btn_Select;
    RadioButton[] chk_CTR;
    RadioButton[] chk_CTRType;
    RadioButton[] chk_Measurement_At;
    RadioButton chk_Observed_kVA;
    RadioButton chk_Observed_kW;
    RadioButton[] chk_PTR;
    CheckBox[] chk_RatioApply;
    RadioButton chk_Sanctioned_kVA;
    RadioButton chk_Sanctioned_kW;
    CheckBox[] chk_Squ3;
    RadioButton[] chk_WiringTypes;
    private DoubleClick customerSelectClick = null;
    View layout_CTR;
    View layout_CTREnable;
    View layout_Meter2;
    View layout_Meter3;
    View layout_PTR;
    View layout_PTREnable;
    View layout_RatioApply;
    Context m_Context;
    CustomerInfoDao m_CustomerDao;
    FieldSetting m_FieldSetting;
    int m_Function;
    boolean m_HasSearch = false;
    SiteData m_Info;
    int m_RequiredField = 1;
    SiteDataManagerDao m_SiteDataDao;
    List<TextView[]> txtList;
    TextView txt_CTAccuracy;
    TextView txt_CTBurden;
    TextView txt_CTManufacturer;
    TextView txt_CTPrimary_Installed;
    TextView txt_CTPrimary_Meter;
    TextView txt_CTSecondary_Installed;
    TextView txt_CTSecondary_Meter;
    EditText txt_CustomerSrSelect;
    TextView txt_Meter1_CertificationYear;
    TextView txt_Meter1_ProductionYear;
    TextView txt_Meter1_Sr;
    EditText txt_Meter1_Type;
    TextView txt_Meter2_CertificationYear;
    TextView txt_Meter2_ProductionYear;
    TextView txt_Meter2_Sr;
    EditText txt_Meter2_Type;
    TextView txt_Meter3_CertificationYear;
    TextView txt_Meter3_ProductionYear;
    TextView txt_Meter3_Sr;
    EditText txt_Meter3_Type;
    TextView txt_Meter_Count;
    TextView txt_MultiplyingFactor;
    TextView txt_ObservedLoad;
    TextView txt_PTAccuracy;
    TextView txt_PTBurden;
    TextView txt_PTManufacturer;
    TextView txt_PTPrimary_Installed;
    TextView txt_PTPrimary_Meter;
    TextView txt_PTSecondary_Installed;
    TextView txt_PTSecondary_Meter;
    TextView txt_SanctionedLoad;
    private DoubleClick type1SelectClick = null;
    private DoubleClick type2SelectClick = null;
    private DoubleClick type3SelectClick = null;
    View view_ModifyEnable;

    public SiteDataView(Context context, View view, SiteData info, int Function) {
        if (info != null) {
            this.m_Info = info;
        } else {
            this.m_Info = new SiteData();
        }
        this.m_Function = Function;
        this.m_Context = context;
        this.m_CustomerDao = new CustomerInfoDao(this.m_Context);
        this.m_SiteDataDao = new SiteDataManagerDao(this.m_Context);
        this.txt_CustomerSrSelect = (EditText) view.findViewById(R.id.txt_customer_sr_select);
        this.txt_CustomerSrSelect.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            /* class com.clou.rs350.ui.view.infoview.SiteDataView.AnonymousClass1 */

            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if ((actionId == 6 || actionId == 5) && 3 != SiteDataView.this.m_Function) {
                    String id = SiteDataView.this.txt_CustomerSrSelect.getText().toString();
                    SiteDataView.this.searchData(id);
                    SiteDataView.this.txt_CustomerSrSelect.setSelection(id.length());
                }
                Log.i("actionId", new StringBuilder(String.valueOf(actionId)).toString());
                return false;
            }
        });
        this.btn_Select = (LinearLayout) view.findViewById(R.id.btn_select);
        this.btn_Select.setOnClickListener(this);
        this.btn_Scan = (LinearLayout) view.findViewById(R.id.btn_scan);
        this.btn_Scan.setOnClickListener(this);
        this.view_ModifyEnable = view.findViewById(R.id.txt_enable);
        if (2 != Function) {
            this.view_ModifyEnable.setVisibility(View.GONE);
        }
        this.txt_Meter_Count = (TextView) view.findViewById(R.id.txt_meter_count);
        this.txt_Meter_Count.setOnClickListener(this);
        this.txt_Meter1_Sr = (TextView) view.findViewById(R.id.txt_meter1_sr);
        this.btn_Meter1_Scan = view.findViewById(R.id.btn_scan_meter1);
        this.btn_Meter1_Scan.setOnClickListener(this);
        this.txt_Meter1_Type = (EditText) view.findViewById(R.id.txt_meter1_type);
        this.txt_Meter1_Type.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            /* class com.clou.rs350.ui.view.infoview.SiteDataView.AnonymousClass2 */

            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == 6 || actionId == 5) {
                    String id = SiteDataView.this.txt_Meter1_Type.getText().toString();
                    MeterType tmpType = new MeterTypeDao(SiteDataView.this.m_Context).queryObjectByType(v.getText().toString());
                    if (tmpType != null) {
                        SiteDataView.this.showMeterTypeData(tmpType);
                    }
                    SiteDataView.this.txt_Meter1_Type.setSelection(id.length());
                }
                Log.i("actionId", new StringBuilder(String.valueOf(actionId)).toString());
                return false;
            }
        });
        this.txt_Meter1_ProductionYear = (TextView) view.findViewById(R.id.txt_meter1_production);
        this.txt_Meter1_ProductionYear.setOnClickListener(this);
        this.txt_Meter1_CertificationYear = (TextView) view.findViewById(R.id.txt_meter1_certification);
        this.txt_Meter1_CertificationYear.setOnClickListener(this);
        this.layout_Meter2 = view.findViewById(R.id.layout_meter2);
        this.txt_Meter2_Sr = (TextView) view.findViewById(R.id.txt_meter2_sr);
        this.btn_Meter2_Scan = view.findViewById(R.id.btn_scan_meter2);
        this.btn_Meter2_Scan.setOnClickListener(this);
        this.txt_Meter2_Type = (EditText) view.findViewById(R.id.txt_meter2_type);
        this.txt_Meter2_Type.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            /* class com.clou.rs350.ui.view.infoview.SiteDataView.AnonymousClass3 */

            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == 6 || actionId == 5) {
                    String id = SiteDataView.this.txt_Meter2_Type.getText().toString();
                    MeterType tmpType = new MeterTypeDao(SiteDataView.this.m_Context).queryObjectByType(v.getText().toString());
                    if (tmpType != null) {
                        SiteDataView.this.showMeterTypeData(tmpType);
                    }
                    SiteDataView.this.txt_Meter2_Type.setSelection(id.length());
                }
                Log.i("actionId", new StringBuilder(String.valueOf(actionId)).toString());
                return false;
            }
        });
        this.txt_Meter2_ProductionYear = (TextView) view.findViewById(R.id.txt_meter2_production);
        this.txt_Meter2_ProductionYear.setOnClickListener(this);
        this.txt_Meter2_CertificationYear = (TextView) view.findViewById(R.id.txt_meter2_certification);
        this.txt_Meter2_CertificationYear.setOnClickListener(this);
        this.layout_Meter3 = view.findViewById(R.id.layout_meter3);
        this.txt_Meter3_Sr = (TextView) view.findViewById(R.id.txt_meter3_sr);
        this.btn_Meter3_Scan = view.findViewById(R.id.btn_scan_meter3);
        this.btn_Meter3_Scan.setOnClickListener(this);
        this.txt_Meter3_Type = (EditText) view.findViewById(R.id.txt_meter3_type);
        this.txt_Meter3_Type.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            /* class com.clou.rs350.ui.view.infoview.SiteDataView.AnonymousClass4 */

            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == 6 || actionId == 5) {
                    String id = SiteDataView.this.txt_Meter3_Type.getText().toString();
                    MeterType tmpType = new MeterTypeDao(SiteDataView.this.m_Context).queryObjectByType(v.getText().toString());
                    if (tmpType != null) {
                        SiteDataView.this.showMeterTypeData(tmpType);
                    }
                    SiteDataView.this.txt_Meter3_Type.setSelection(id.length());
                }
                Log.i("actionId", new StringBuilder(String.valueOf(actionId)).toString());
                return false;
            }
        });
        this.txt_Meter3_ProductionYear = (TextView) view.findViewById(R.id.txt_meter3_production);
        this.txt_Meter3_ProductionYear.setOnClickListener(this);
        this.txt_Meter3_CertificationYear = (TextView) view.findViewById(R.id.txt_meter3_certification);
        this.txt_Meter3_CertificationYear.setOnClickListener(this);
        this.btn_Meter1_Type_Select = view.findViewById(R.id.btn_select_type1);
        this.btn_Meter1_Type_Select.setOnClickListener(this);
        this.btn_Meter2_Type_Select = view.findViewById(R.id.btn_select_type2);
        this.btn_Meter2_Type_Select.setOnClickListener(this);
        this.btn_Meter3_Type_Select = view.findViewById(R.id.btn_select_type3);
        this.btn_Meter3_Type_Select.setOnClickListener(this);
        this.chk_WiringTypes = new RadioButton[]{(RadioButton) view.findViewById(R.id.chk_wiring1), (RadioButton) view.findViewById(R.id.chk_wiring6), (RadioButton) view.findViewById(R.id.chk_wiring2), (RadioButton) view.findViewById(R.id.chk_wiring3), (RadioButton) view.findViewById(R.id.chk_wiring4), (RadioButton) view.findViewById(R.id.chk_wiring5)};
        for (int i = 0; i < this.chk_WiringTypes.length; i++) {
            this.chk_WiringTypes[i].setOnCheckedChangeListener(this);
        }
        this.layout_CTREnable = view.findViewById(R.id.layout_ctr_enable);
        this.chk_CTR = new RadioButton[]{(RadioButton) view.findViewById(R.id.chk_ctr_yes), (RadioButton) view.findViewById(R.id.chk_ctr_no), (RadioButton) view.findViewById(R.id.chk_ctr_null)};
        this.chk_CTR[0].setOnCheckedChangeListener(this);
        this.chk_CTR[1].setOnCheckedChangeListener(this);
        this.chk_CTR[2].setOnCheckedChangeListener(this);
        this.layout_CTR = view.findViewById(R.id.layout_ctr);
        this.chk_CTRType = new RadioButton[]{(RadioButton) view.findViewById(R.id.chk_ctr_ht), (RadioButton) view.findViewById(R.id.chk_ctr_lt)};
        this.txt_CTPrimary_Meter = (TextView) view.findViewById(R.id.txt_ct_primary_meter);
        this.txt_CTPrimary_Meter.setOnClickListener(this);
        this.txt_CTSecondary_Meter = (TextView) view.findViewById(R.id.txt_ct_secondary_meter);
        this.txt_CTSecondary_Meter.setOnClickListener(this);
        this.txt_CTPrimary_Installed = (TextView) view.findViewById(R.id.txt_ct_primary_installed);
        this.txt_CTPrimary_Installed.setOnClickListener(this);
        this.txt_CTSecondary_Installed = (TextView) view.findViewById(R.id.txt_ct_secondary_installed);
        this.txt_CTSecondary_Installed.setOnClickListener(this);
        this.txt_CTAccuracy = (TextView) view.findViewById(R.id.txt_ct_accuracy);
        this.txt_CTAccuracy.setOnClickListener(this);
        this.txt_CTBurden = (TextView) view.findViewById(R.id.txt_ct_burden);
        this.txt_CTBurden.setOnClickListener(this);
        this.txt_CTManufacturer = (TextView) view.findViewById(R.id.txt_ct_manufacturer);
        this.layout_PTREnable = view.findViewById(R.id.layout_ptr_enable);
        this.chk_PTR = new RadioButton[]{(RadioButton) view.findViewById(R.id.chk_ptr_yes), (RadioButton) view.findViewById(R.id.chk_ptr_no), (RadioButton) view.findViewById(R.id.chk_ptr_null)};
        this.chk_PTR[0].setOnCheckedChangeListener(this);
        this.chk_PTR[1].setOnCheckedChangeListener(this);
        this.chk_PTR[2].setOnCheckedChangeListener(this);
        this.layout_PTR = view.findViewById(R.id.layout_ptr);
        this.txt_PTPrimary_Meter = (TextView) view.findViewById(R.id.txt_pt_primary_meter);
        this.txt_PTPrimary_Meter.setOnClickListener(this);
        this.txt_PTSecondary_Meter = (TextView) view.findViewById(R.id.txt_pt_secondary_meter);
        this.txt_PTSecondary_Meter.setOnClickListener(this);
        this.txt_PTPrimary_Installed = (TextView) view.findViewById(R.id.txt_pt_primary_installed);
        this.txt_PTPrimary_Installed.setOnClickListener(this);
        this.txt_PTSecondary_Installed = (TextView) view.findViewById(R.id.txt_pt_secondary_installed);
        this.txt_PTSecondary_Installed.setOnClickListener(this);
        this.txt_PTAccuracy = (TextView) view.findViewById(R.id.txt_pt_accuracy);
        this.txt_PTAccuracy.setOnClickListener(this);
        this.txt_PTBurden = (TextView) view.findViewById(R.id.txt_pt_burden);
        this.txt_PTBurden.setOnClickListener(this);
        this.txt_PTManufacturer = (TextView) view.findViewById(R.id.txt_pt_manufacturer);
        this.chk_Squ3 = new CheckBox[]{(CheckBox) view.findViewById(R.id.chk_p3), (CheckBox) view.findViewById(R.id.chk_s3)};
        this.chk_Squ3[0].setOnCheckedChangeListener(this);
        this.chk_Squ3[1].setOnCheckedChangeListener(this);
        this.chk_Measurement_At = new RadioButton[]{(RadioButton) view.findViewById(R.id.chk_measurement_at_secondary), (RadioButton) view.findViewById(R.id.chk_measurement_at_primary)};
        this.chk_RatioApply = new CheckBox[]{(CheckBox) view.findViewById(R.id.chk_ipenable), (CheckBox) view.findViewById(R.id.chk_errortestenable), (CheckBox) view.findViewById(R.id.chk_demandtestenable), (CheckBox) view.findViewById(R.id.chk_energytestenable)};
        this.layout_RatioApply = view.findViewById(R.id.layout_ratio_apply);
        this.txt_MultiplyingFactor = (TextView) view.findViewById(R.id.txt_multiplying_factor);
        this.txt_MultiplyingFactor.setOnClickListener(this);
        this.txt_SanctionedLoad = (TextView) view.findViewById(R.id.txt_sanctioned_load);
        this.txt_SanctionedLoad.setOnClickListener(this);
        this.txt_ObservedLoad = (TextView) view.findViewById(R.id.txt_observed_load);
        this.txt_ObservedLoad.setOnClickListener(this);
        this.chk_Sanctioned_kW = (RadioButton) view.findViewById(R.id.chk_sanctioned_kw);
        this.chk_Sanctioned_kW.setOnCheckedChangeListener(this);
        this.chk_Sanctioned_kVA = (RadioButton) view.findViewById(R.id.chk_sanctioned_kva);
        this.chk_Sanctioned_kVA.setOnCheckedChangeListener(this);
        this.chk_Observed_kW = (RadioButton) view.findViewById(R.id.chk_observed_kw);
        this.chk_Observed_kW.setOnCheckedChangeListener(this);
        this.chk_Observed_kVA = (RadioButton) view.findViewById(R.id.chk_observed_kva);
        this.chk_Observed_kVA.setOnCheckedChangeListener(this);
        setFunction(this.m_Function);
        initClick();
        initTxtList();
        showData();
    }

    private void initTxtList() {
        this.txtList = new ArrayList();
        this.txtList.add(new TextView[]{this.txt_Meter1_Sr, this.txt_Meter2_Sr, this.txt_Meter3_Sr});
        this.txtList.add(new TextView[]{this.txt_Meter1_Type, this.txt_Meter2_Type, this.txt_Meter3_Type});
        this.txtList.add(new TextView[]{this.txt_Meter1_ProductionYear, this.txt_Meter2_ProductionYear, this.txt_Meter3_ProductionYear});
        this.txtList.add(new TextView[]{this.txt_Meter1_CertificationYear, this.txt_Meter2_CertificationYear, this.txt_Meter3_CertificationYear});
        this.txtList.add(new TextView[]{this.txt_SanctionedLoad});
        this.txtList.add(new TextView[]{this.txt_ObservedLoad});
    }

    public void refreshFieldSetting() {
        this.m_FieldSetting = new FieldSettingUtil(this.m_Context).getFieldSetting(FieldSettingUtil.SITEDATA);
        for (int i = 0; i < this.txtList.size(); i++) {
            FieldItem tmpItem = this.m_FieldSetting.Items.get(this.m_FieldSetting.ItemKeys.get(i));
            TextView[] tmpTxtView = this.txtList.get(i);
            for (int k = 0; k < tmpTxtView.length; k++) {
                if (1 == tmpItem.RequiredSetting && 1 == this.m_RequiredField) {
                    tmpTxtView[k].setHint(R.string.text_required_field);
                } else {
                    tmpTxtView[k].setHint(PdfObject.NOTHING);
                }
            }
        }
    }

    public void setRequiredField(int flag) {
        this.m_RequiredField = flag;
        refreshFieldSetting();
    }

    private DoubleClick.OnDoubleClick initMeterTypeOnclick(final TextView v) {
        return new DoubleClick.OnDoubleClick() {
            /* class com.clou.rs350.ui.view.infoview.SiteDataView.AnonymousClass5 */

            private void searchKey(String condition) {
                final TextView tmpTextView = v;
                List<String> listMeterType = new MeterTypeDao(SiteDataView.this.m_Context).queryAllKey(condition);
                listMeterType.add(SiteDataView.this.m_Context.getResources().getString(R.string.text_new));
                final String[] sMeterTypes = (String[]) listMeterType.toArray(new String[listMeterType.size()]);
                ListPopWindow.showListPopwindow(SiteDataView.this.m_Context, sMeterTypes, v, new View.OnClickListener() {
                    /* class com.clou.rs350.ui.view.infoview.SiteDataView.AnonymousClass5.AnonymousClass1 */

                    public void onClick(View v) {
                        if (((Number) v.getTag()).intValue() == sMeterTypes.length - 1) {
                            MeterTypeInfoDialog infoDialog = new MeterTypeInfoDialog(SiteDataView.this.m_Context, new MeterType(), true);
                            final TextView textView = tmpTextView;
                            final TextView textView2 = tmpTextView;

                            infoDialog.setOnClick(new View.OnClickListener() {
                                /* class com.clou.rs350.ui.view.infoview.SiteDataView.AnonymousClass5.AnonymousClass1.AnonymousClass1 */

                                public void onClick(View v) {
                                    MeterType tmpType = (MeterType) v.getTag();
                                    textView.setText(tmpType.Type);
                                    SiteDataView.this.showMeterTypeData(tmpType);
                                }
                            }, new View.OnClickListener() {
                                /* class com.clou.rs350.ui.view.infoview.SiteDataView.AnonymousClass5.AnonymousClass1.AnonymousClass2 */

                                public void onClick(View v) {
                                    textView2.setText(PdfObject.NOTHING);
                                }
                            });
                            infoDialog.show();
                            return;
                        }
                        SiteDataView.this.showMeterTypeData(new MeterTypeDao(SiteDataView.this.m_Context).queryObjectByType(((TextView) v).getText().toString()));
                    }
                });
            }

            @Override // com.clou.rs350.utils.DoubleClick.OnDoubleClick
            public void doubleClick() {
                searchKey(v.getText().toString());
            }

            @Override // com.clou.rs350.utils.DoubleClick.OnDoubleClick
            public void singleClick() {
                searchKey(PdfObject.NOTHING);
            }
        };
    }

    private void initClick() {
        this.customerSelectClick = new DoubleClick(new DoubleClick.OnDoubleClick() {
            /* class com.clou.rs350.ui.view.infoview.SiteDataView.AnonymousClass6 */

            private void searchKey(String condition) {
                List<String> tmpSerials = new CustomerInfoDao(SiteDataView.this.m_Context).queryAllKey(condition);
                ListPopWindow.showListPopwindow(SiteDataView.this.m_Context, (String[]) tmpSerials.toArray(new String[tmpSerials.size()]), SiteDataView.this.txt_CustomerSrSelect, new View.OnClickListener() {
                    /* class com.clou.rs350.ui.view.infoview.SiteDataView.AnonymousClass6.AnonymousClass1 */

                    public void onClick(View v) {
                        SiteDataView.this.searchData(SiteDataView.this.txt_CustomerSrSelect.getText().toString());
                    }
                });
            }

            @Override // com.clou.rs350.utils.DoubleClick.OnDoubleClick
            public void doubleClick() {
                searchKey(SiteDataView.this.txt_CustomerSrSelect.getText().toString());
            }

            @Override // com.clou.rs350.utils.DoubleClick.OnDoubleClick
            public void singleClick() {
                searchKey(PdfObject.NOTHING);
            }
        });
        this.type1SelectClick = new DoubleClick(initMeterTypeOnclick(this.txt_Meter1_Type));
        this.type2SelectClick = new DoubleClick(initMeterTypeOnclick(this.txt_Meter2_Type));
        this.type3SelectClick = new DoubleClick(initMeterTypeOnclick(this.txt_Meter3_Type));
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private void showMeterTypeData(MeterType type) {
        this.m_Info.Wiring = type.Wiring;
        this.m_Info.WiringType = this.chk_WiringTypes[this.m_Info.Wiring].getText().toString();
        this.chk_WiringTypes[this.m_Info.Wiring].setChecked(true);
    }

    private void showData() {
        int i = 8;
        this.txt_CustomerSrSelect.setText(this.m_Info.CustomerSr);
        showMeterTypeData(this.m_Info);
        showMeter(this.m_Info.Meter_Count);
        this.txt_Meter1_Sr.setText(this.m_Info.MeterInfo[0].SerialNo);
        this.txt_Meter1_Type.setText(this.m_Info.MeterInfo[0].Type);
        this.txt_Meter1_ProductionYear.setText(this.m_Info.MeterInfo[0].ProductionYear);
        this.txt_Meter1_CertificationYear.setText(this.m_Info.Meter_CertificationYear[0]);
        this.txt_Meter2_Sr.setText(this.m_Info.MeterInfo[1].SerialNo);
        this.txt_Meter2_Type.setText(this.m_Info.MeterInfo[1].Type);
        this.txt_Meter2_ProductionYear.setText(this.m_Info.MeterInfo[1].ProductionYear);
        this.txt_Meter2_CertificationYear.setText(this.m_Info.Meter_CertificationYear[1]);
        this.txt_Meter3_Sr.setText(this.m_Info.MeterInfo[2].SerialNo);
        this.txt_Meter3_Type.setText(this.m_Info.MeterInfo[2].Type);
        this.txt_Meter3_ProductionYear.setText(this.m_Info.MeterInfo[2].ProductionYear);
        this.txt_Meter3_CertificationYear.setText(this.m_Info.Meter_CertificationYear[2]);
        this.chk_CTR[this.m_Info.CTEnable].setChecked(true);
        this.layout_CTR.setVisibility(this.m_Info.CTEnable != 2 ? View.VISIBLE : View.GONE);
        int i2 = 0;
        while (true) {
            if (i2 >= this.chk_CTRType.length) {
                break;
            } else if (this.chk_CTRType[i2].getText().equals(this.m_Info.CTType)) {
                this.chk_CTRType[i2].setChecked(true);
                break;
            } else {
                i2++;
            }
        }
        this.txt_CTPrimary_Meter.setText(new StringBuilder(String.valueOf(this.m_Info.CTPrimary.d_Value)).toString());
        this.txt_CTSecondary_Meter.setText(new StringBuilder(String.valueOf(this.m_Info.CTSecondary.d_Value)).toString());
        this.txt_CTPrimary_Installed.setText(new StringBuilder(String.valueOf(this.m_Info.CTPrimary_Installed.d_Value)).toString());
        this.txt_CTSecondary_Installed.setText(new StringBuilder(String.valueOf(this.m_Info.CTSecondary_Installed.d_Value)).toString());
        this.txt_CTAccuracy.setText(this.m_Info.CTAccuracyClass);
        this.txt_CTBurden.setText(new StringBuilder(String.valueOf(this.m_Info.CTBurden.d_Value)).toString());
        this.txt_CTManufacturer.setText(this.m_Info.CTManufacturer);
        this.chk_PTR[this.m_Info.PTEnable].setChecked(true);
        View view = this.layout_PTR;
        if (this.m_Info.PTEnable != 2) {
            i = 0;
        }
        view.setVisibility(i);
        this.txt_PTPrimary_Meter.setText(new StringBuilder(String.valueOf(this.m_Info.PTPrimary.d_Value)).toString());
        this.txt_PTSecondary_Meter.setText(new StringBuilder(String.valueOf(this.m_Info.PTSecondary.d_Value)).toString());
        this.txt_PTPrimary_Installed.setText(new StringBuilder(String.valueOf(this.m_Info.PTPrimary_Installed.d_Value)).toString());
        this.txt_PTSecondary_Installed.setText(new StringBuilder(String.valueOf(this.m_Info.PTSecondary_Installed.d_Value)).toString());
        this.txt_PTAccuracy.setText(this.m_Info.PTAccuracyClass);
        this.txt_PTBurden.setText(new StringBuilder(String.valueOf(this.m_Info.PTBurden.d_Value)).toString());
        this.txt_PTManufacturer.setText(this.m_Info.PTManufacturer);
        if (this.m_Info.PTPrimary.s_Value.contains("/")) {
            this.chk_Squ3[0].setChecked(true);
            this.chk_Squ3[1].setChecked(true);
        }
        this.chk_Measurement_At[this.m_Info.Measurement_Position].setChecked(true);
        for (int i3 = 0; i3 < this.chk_RatioApply.length; i3++) {
            int flag = 1 << i3;
            if (flag == (this.m_Info.RatioApply & flag)) {
                this.chk_RatioApply[i3].setChecked(true);
            } else {
                this.chk_RatioApply[i3].setChecked(false);
            }
        }
        this.txt_MultiplyingFactor.setText(this.m_Info.MultiplyingFactor);
        this.txt_SanctionedLoad.setText(this.m_Info.SanctionedLoad.replace("kW", PdfObject.NOTHING).replace("kVA", PdfObject.NOTHING));
        this.txt_ObservedLoad.setText(this.m_Info.ObservedLoad.replace("kW", PdfObject.NOTHING).replace("kVA", PdfObject.NOTHING));
        refreshFieldSetting();
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private void showMeter(int meterCount) {
        this.txt_Meter_Count.setText(new StringBuilder(String.valueOf(meterCount)).toString());
        switch (meterCount) {
            case 1:
                this.layout_Meter2.setVisibility(View.GONE);
                this.layout_Meter3.setVisibility(View.GONE);
                return;
            case 2:
                this.layout_Meter2.setVisibility(View.VISIBLE);
                this.layout_Meter3.setVisibility(View.GONE);
                return;
            case 3:
                this.layout_Meter2.setVisibility(View.VISIBLE);
                this.layout_Meter3.setVisibility(View.VISIBLE);
                return;
            default:
                return;
        }
    }

    public SiteData getInfo() {
        parseData();
        return this.m_Info;
    }

    public void refreshData(SiteData info) {
        this.m_Info = info;
        showData();
    }

    public String getKey() {
        return this.txt_CustomerSrSelect.getText().toString();
    }

    public boolean getHasSearch() {
        return this.m_HasSearch;
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private void searchData(String serialNo) {
        if (this.m_CustomerDao.queryObjectBySerialNo(serialNo) != null) {
            this.m_HasSearch = true;
            this.m_Info = (SiteData) this.m_SiteDataDao.queryObjectBySerialNo(serialNo);
            if (this.m_Info == null) {
                this.m_Info = new SiteData();
                this.m_Info.CustomerSr = serialNo;
            }
        } else {
            this.m_HasSearch = false;
            this.m_Info = new SiteData();
            this.m_Info.CustomerSr = serialNo;
        }
        refreshData(this.m_Info);
    }

    public void setFunction(int function) {
        boolean z;
        boolean z2;
        boolean z3;
        boolean z4;
        boolean z5;
        boolean z6;
        boolean z7;
        boolean z8;
        boolean z9 = true;
        this.m_Function = function;
        this.view_ModifyEnable.setVisibility(function < 2 ? View.VISIBLE : View.GONE);
        View view = this.btn_Meter1_Scan;
        if (function > 1) {
            z = true;
        } else {
            z = false;
        }
        view.setEnabled(z);
        View view2 = this.btn_Meter2_Scan;
        if (function > 1) {
            z2 = true;
        } else {
            z2 = false;
        }
        view2.setEnabled(z2);
        View view3 = this.btn_Meter3_Scan;
        if (function > 1) {
            z3 = true;
        } else {
            z3 = false;
        }
        view3.setEnabled(z3);
        View view4 = this.btn_Meter1_Type_Select;
        if (function > 1) {
            z4 = true;
        } else {
            z4 = false;
        }
        view4.setEnabled(z4);
        View view5 = this.btn_Meter2_Type_Select;
        if (function > 1) {
            z5 = true;
        } else {
            z5 = false;
        }
        view5.setEnabled(z5);
        View view6 = this.btn_Meter3_Type_Select;
        if (function > 1) {
            z6 = true;
        } else {
            z6 = false;
        }
        view6.setEnabled(z6);
        EditText editText = this.txt_CustomerSrSelect;
        if (function == 1 || function == 3) {
            z7 = true;
        } else {
            z7 = false;
        }
        editText.setEnabled(z7);
        LinearLayout linearLayout = this.btn_Select;
        if (function == 1) {
            z8 = true;
        } else {
            z8 = false;
        }
        linearLayout.setEnabled(z8);
        LinearLayout linearLayout2 = this.btn_Scan;
        if (function != 1) {
            z9 = false;
        }
        linearLayout2.setEnabled(z9);
    }

    private void parseData() {
        this.m_Info.CustomerSr = this.txt_CustomerSrSelect.getText().toString();
        this.m_Info.Meter_Count = OtherUtils.parseInt(this.txt_Meter_Count.getText().toString());
        this.m_Info.MeterInfo[0].SerialNo = this.txt_Meter1_Sr.getText().toString();
        this.m_Info.MeterInfo[0].Type = this.txt_Meter1_Type.getText().toString();
        this.m_Info.MeterInfo[0].ProductionYear = this.txt_Meter1_ProductionYear.getText().toString();
        this.m_Info.Meter_CertificationYear[0] = this.txt_Meter1_CertificationYear.getText().toString();
        if (1 < this.m_Info.Meter_Count) {
            this.m_Info.MeterInfo[1].SerialNo = this.txt_Meter2_Sr.getText().toString();
            this.m_Info.MeterInfo[1].Type = this.txt_Meter2_Type.getText().toString();
            this.m_Info.MeterInfo[1].ProductionYear = this.txt_Meter2_ProductionYear.getText().toString();
            this.m_Info.Meter_CertificationYear[1] = this.txt_Meter2_CertificationYear.getText().toString();
        } else {
            this.m_Info.MeterInfo[1].SerialNo = PdfObject.NOTHING;
            this.m_Info.MeterInfo[1].Type = PdfObject.NOTHING;
            this.m_Info.MeterInfo[1].ProductionYear = PdfObject.NOTHING;
            this.m_Info.Meter_CertificationYear[1] = PdfObject.NOTHING;
        }
        if (2 < this.m_Info.Meter_Count) {
            this.m_Info.MeterInfo[2].SerialNo = this.txt_Meter3_Sr.getText().toString();
            this.m_Info.MeterInfo[2].Type = this.txt_Meter3_Type.getText().toString();
            this.m_Info.MeterInfo[2].ProductionYear = this.txt_Meter3_ProductionYear.getText().toString();
            this.m_Info.Meter_CertificationYear[2] = this.txt_Meter3_CertificationYear.getText().toString();
        } else {
            this.m_Info.MeterInfo[2].SerialNo = PdfObject.NOTHING;
            this.m_Info.MeterInfo[2].Type = PdfObject.NOTHING;
            this.m_Info.MeterInfo[2].ProductionYear = PdfObject.NOTHING;
            this.m_Info.Meter_CertificationYear[2] = PdfObject.NOTHING;
        }
        int i = 0;
        while (true) {
            if (i >= this.chk_WiringTypes.length) {
                break;
            } else if (this.chk_WiringTypes[i].isChecked()) {
                this.m_Info.WiringType = this.chk_WiringTypes[i].getText().toString();
                this.m_Info.Wiring = i;
                break;
            } else {
                i++;
            }
        }
        int i2 = 0;
        while (true) {
            if (i2 >= this.chk_CTR.length) {
                break;
            } else if (this.chk_CTR[i2].isChecked()) {
                this.m_Info.CTEnable = i2;
                break;
            } else {
                i2++;
            }
        }
        int i3 = 0;
        while (true) {
            if (i3 >= this.chk_CTRType.length) {
                break;
            } else if (this.chk_CTRType[i3].isChecked()) {
                this.m_Info.CTType = this.chk_CTRType[i3].getText().toString();
                break;
            } else {
                i3++;
            }
        }
        this.m_Info.CTPrimary.s_Value = String.valueOf(this.txt_CTPrimary_Meter.getText().toString()) + "A";
        this.m_Info.CTSecondary.s_Value = String.valueOf(this.txt_CTSecondary_Meter.getText().toString()) + "A";
        this.m_Info.CTPrimary = InfoUtil.parseCurrentRatio(this.m_Info.CTPrimary.s_Value);
        this.m_Info.CTSecondary = InfoUtil.parseCurrentRatio(this.m_Info.CTSecondary.s_Value);
        this.m_Info.CTPrimary_Installed.s_Value = String.valueOf(this.txt_CTPrimary_Installed.getText().toString()) + "A";
        this.m_Info.CTSecondary_Installed.s_Value = String.valueOf(this.txt_CTSecondary_Installed.getText().toString()) + "A";
        this.m_Info.CTAccuracyClass = this.txt_CTAccuracy.getText().toString();
        this.m_Info.CTBurden = InfoUtil.parseBurden(String.valueOf(this.txt_CTBurden.getText().toString()) + "VA");
        this.m_Info.CTManufacturer = this.txt_CTManufacturer.getText().toString();
        int i4 = 0;
        while (true) {
            if (i4 >= this.chk_PTR.length) {
                break;
            } else if (this.chk_PTR[i4].isChecked()) {
                this.m_Info.PTEnable = i4;
                break;
            } else {
                i4++;
            }
        }
        this.m_Info.PTPrimary.s_Value = String.valueOf(this.txt_PTPrimary_Meter.getText().toString()) + "kV";
        this.m_Info.PTSecondary.s_Value = String.valueOf(this.txt_PTSecondary_Meter.getText().toString()) + "V";
        if (this.chk_Squ3[0].isChecked()) {
            DBDataModel dBDataModel = this.m_Info.PTPrimary;
            dBDataModel.s_Value = String.valueOf(dBDataModel.s_Value) + "/√3";
            DBDataModel dBDataModel2 = this.m_Info.PTSecondary;
            dBDataModel2.s_Value = String.valueOf(dBDataModel2.s_Value) + "/√3";
        }
        this.m_Info.PTPrimary = InfoUtil.parseVoltageRatio(this.m_Info.PTPrimary.s_Value, true);
        this.m_Info.PTSecondary = InfoUtil.parseVoltageRatio(this.m_Info.PTSecondary.s_Value, false);
        this.m_Info.PTPrimary_Installed.s_Value = String.valueOf(this.txt_PTPrimary_Installed.getText().toString()) + "kV";
        this.m_Info.PTSecondary_Installed.s_Value = String.valueOf(this.txt_PTSecondary_Installed.getText().toString()) + "V";
        this.m_Info.PTAccuracyClass = this.txt_PTAccuracy.getText().toString();
        this.m_Info.PTBurden = InfoUtil.parseBurden(String.valueOf(this.txt_PTBurden.getText().toString()) + "VA");
        this.m_Info.PTManufacturer = this.txt_PTManufacturer.getText().toString();
        int i5 = 0;
        while (true) {
            if (i5 >= this.chk_Measurement_At.length) {
                break;
            } else if (this.chk_Measurement_At[i5].isChecked()) {
                this.m_Info.Measurement_Position = i5;
                break;
            } else {
                i5++;
            }
        }
        this.m_Info.RatioApply = 0;
        if (this.m_Info.CTEnable == 0 || this.m_Info.PTEnable == 0) {
            for (int i6 = 0; i6 < this.chk_RatioApply.length; i6++) {
                if (this.chk_RatioApply[i6].isChecked()) {
                    this.m_Info.RatioApply |= 1 << i6;
                }
            }
        }
        this.m_Info.MultiplyingFactor = this.txt_MultiplyingFactor.getText().toString();
        if (this.chk_Sanctioned_kW.isChecked()) {
            this.m_Info.SanctionedLoad = String.valueOf(this.txt_SanctionedLoad.getText().toString()) + "kW";
            this.m_Info.ObservedLoad = String.valueOf(this.txt_ObservedLoad.getText().toString()) + "kW";
            return;
        }
        this.m_Info.SanctionedLoad = String.valueOf(this.txt_SanctionedLoad.getText().toString()) + "kVA";
        this.m_Info.ObservedLoad = String.valueOf(this.txt_ObservedLoad.getText().toString()) + "kVA";
    }

    public boolean hasSetParams() {
        boolean result = true;
        String strHint = PdfObject.NOTHING;
        if (1 == this.m_RequiredField) {
            int Meter_Count = OtherUtils.parseInt(this.txt_Meter_Count.getText().toString());
            for (int i = 0; i < this.txtList.size(); i++) {
                FieldItem tmpItem = this.m_FieldSetting.Items.get(this.m_FieldSetting.ItemKeys.get(i));
                if (1 == tmpItem.RequiredSetting) {
                    boolean tmpResult = true;
                    TextView[] tmpTxtView = this.txtList.get(i);
                    int k = 0;
                    while (true) {
                        if (k >= tmpTxtView.length || k >= Meter_Count) {
                            break;
                        } else if (OtherUtils.isEmpty(tmpTxtView[k])) {
                            tmpResult = false;
                            break;
                        } else {
                            k++;
                        }
                    }
                    if (!tmpResult) {
                        result = false;
                        strHint = String.valueOf(strHint) + this.m_Context.getResources().getString(tmpItem.Id) + " " + this.m_Context.getResources().getString(R.string.text_is_empty) + "\r\n";
                    }
                }
            }
            if (!result) {
                new HintDialog(this.m_Context, strHint.substring(0, strHint.length() - 2)).show();
            }
        }
        return result;
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_select:
                this.customerSelectClick.click();
                return;
            case R.id.btn_scan:
                CaptureDialog scanDialog = new CaptureDialog(this.m_Context);
                scanDialog.setScanResult(new CaptureDialog.ScanResult() {
                    /* class com.clou.rs350.ui.view.infoview.SiteDataView.AnonymousClass8 */

                    @Override // google.zxing.camera.CaptureDialog.ScanResult
                    public void getScanResult(String result) {
                        SiteDataView.this.txt_CustomerSrSelect.setText(result);
                        SiteDataView.this.searchData(SiteDataView.this.txt_CustomerSrSelect.getText().toString());
                    }
                });
                scanDialog.show();
                return;
            case R.id.txt_meter_count:
                NumericDialog numDialog = new NumericDialog(this.m_Context);
                numDialog.setMax(3.0d);
                numDialog.setMin(1.0d);
                numDialog.setMinus(false);
                numDialog.setOnDoneClick(new View.OnClickListener() {
                    /* class com.clou.rs350.ui.view.infoview.SiteDataView.AnonymousClass7 */

                    public void onClick(View v) {
                        SiteDataView.this.m_Info.Meter_Count = OtherUtils.parseInt(((TextView) v).getText().toString());
                        SiteDataView.this.showMeter(SiteDataView.this.m_Info.Meter_Count);
                    }
                });
                numDialog.showMyDialog((TextView) v);
                return;
            case R.id.btn_scan_meter1:
                new CaptureDialog(this.m_Context).show(this.txt_Meter1_Sr);
                return;
            case R.id.btn_select_type1:
                this.type1SelectClick.click();
                return;
            case R.id.txt_meter1_production:
            case R.id.txt_meter1_certification:
            case R.id.txt_meter2_production:
            case R.id.txt_meter2_certification:
            case R.id.txt_meter3_production:
            case R.id.txt_meter3_certification:
                new YearSettingPopWindow(this.m_Context).showPopWindow(v, null);
                return;
            case R.id.btn_scan_meter2:
                new CaptureDialog(this.m_Context).show(this.txt_Meter2_Sr);
                return;
            case R.id.btn_select_type2:
                this.type2SelectClick.click();
                return;
            case R.id.btn_scan_meter3:
                new CaptureDialog(this.m_Context).show(this.txt_Meter3_Sr);
                return;
            case R.id.btn_select_type3:
                this.type3SelectClick.click();
                return;
            default:
                NumericDialog numDialog2 = new NumericDialog(this.m_Context);
                numDialog2.setMinus(false);
                numDialog2.showMyDialog((TextView) v);
                return;
        }
    }

    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        switch (buttonView.getId()) {
            case R.id.chk_p3:
            case R.id.chk_s3:
                this.chk_Squ3[0].setChecked(isChecked);
                this.chk_Squ3[1].setChecked(isChecked);
                break;
            case R.id.chk_wiring1:
            case R.id.chk_wiring6:
            case R.id.chk_wiring2:
                if (isChecked) {
                    this.layout_CTREnable.setVisibility(View.GONE);
                    this.layout_PTREnable.setVisibility(View.GONE);
                    this.chk_CTR[2].setChecked(true);
                    this.chk_PTR[2].setChecked(true);
                    break;
                }
                break;
            case R.id.chk_wiring3:
                if (isChecked) {
                    this.layout_CTREnable.setVisibility(View.VISIBLE);
                    this.layout_PTREnable.setVisibility(View.GONE);
                    this.chk_PTR[2].setChecked(true);
                    break;
                }
                break;
            case R.id.chk_wiring4:
            case R.id.chk_wiring5:
                if (isChecked) {
                    this.layout_CTREnable.setVisibility(View.VISIBLE);
                    this.layout_PTREnable.setVisibility(View.VISIBLE);
                    break;
                }
                break;
            case R.id.chk_ptr_yes:
            case R.id.chk_ptr_no:
                if (isChecked) {
                    this.layout_PTR.setVisibility(View.VISIBLE);
                    break;
                }
                break;
            case R.id.chk_ptr_null:
                if (isChecked) {
                    this.layout_PTR.setVisibility(View.GONE);
                    break;
                }
                break;
            case R.id.chk_ctr_yes:
            case R.id.chk_ctr_no:
                if (isChecked) {
                    this.layout_CTR.setVisibility(View.VISIBLE);
                    break;
                }
                break;
            case R.id.chk_ctr_null:
                if (isChecked) {
                    this.layout_CTR.setVisibility(View.GONE);
                    break;
                }
                break;
            case R.id.chk_sanctioned_kw:
            case R.id.chk_observed_kw:
                if (isChecked) {
                    this.chk_Sanctioned_kW.setChecked(true);
                    this.chk_Observed_kW.setChecked(true);
                    break;
                }
                break;
            case R.id.chk_sanctioned_kva:
            case R.id.chk_observed_kva:
                if (isChecked) {
                    this.chk_Sanctioned_kVA.setChecked(true);
                    this.chk_Observed_kVA.setChecked(true);
                    break;
                }
                break;
        }
        if (this.chk_CTR[0].isChecked() || this.chk_PTR[0].isChecked()) {
            this.layout_RatioApply.setVisibility(View.VISIBLE);
        } else {
            this.layout_RatioApply.setVisibility(View.GONE);
        }
    }
}
