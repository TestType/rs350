package com.clou.rs350.ui.view.infoview;

import android.content.Context;
import android.location.GpsSatellite;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;

import com.clou.rs350.R;
import com.clou.rs350.db.dao.CustomerInfoDao;
import com.clou.rs350.db.model.CustomerInfo;
import com.clou.rs350.db.model.sequence.FieldItem;
import com.clou.rs350.db.model.sequence.FieldSetting;
import com.clou.rs350.ui.dialog.HintDialog;
import com.clou.rs350.ui.popwindow.ListPopWindow;
import com.clou.rs350.utils.DoubleClick;
import com.clou.rs350.utils.FieldSettingUtil;
import com.clou.rs350.utils.OtherUtils;
import com.itextpdf.text.pdf.ColumnText;
import com.itextpdf.text.pdf.PdfObject;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import google.zxing.camera.CaptureDialog;

public class CustomerInfoView implements View.OnClickListener {
    View btn_GetLocation;
    LinearLayout btn_Scan;
    LinearLayout btn_Select;
    RadioButton[] chk_ConnectionType;
    int[] m_Constants = new int[3];
    Context m_Context;
    CustomerInfoDao m_Dao;
    FieldSetting m_FieldSetting;
    int m_Function;
    boolean m_HasSearch = false;
    CustomerInfo m_Info;
    int m_RequiredField = 1;
    private DoubleClick selectClick = null;
    List<TextView[]> txtList;
    TextView txt_AddStreet;
    TextView txt_City;
    TextView txt_ContactName;
    TextView txt_ContactNr;
    TextView txt_CustomerName;
    EditText txt_CustomerSrSelect;
    TextView txt_District;
    TextView txt_Email;
    TextView txt_HouseNr;
    TextView txt_Latitude;
    TextView txt_Longitude;
    TextView txt_Pin;
    TextView txt_State;
    View view_ModifyEnable;

    public CustomerInfoView(Context context, View view, CustomerInfo info, int Function) {
        this.m_Info = info;
        this.m_Function = Function;
        this.m_Context = context;
        this.m_Dao = new CustomerInfoDao(this.m_Context);
        this.txt_CustomerSrSelect = (EditText) view.findViewById(R.id.txt_customer_sr_select);
        this.txt_CustomerSrSelect.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            /* class com.clou.rs350.ui.view.infoview.CustomerInfoView.AnonymousClass1 */

            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if ((actionId == 6 || actionId == 5) && 3 != CustomerInfoView.this.m_Function) {
                    String id = CustomerInfoView.this.txt_CustomerSrSelect.getText().toString();
                    CustomerInfoView.this.searchData(id);
                    CustomerInfoView.this.txt_CustomerSrSelect.setSelection(id.length());
                }
                Log.i("actionId", new StringBuilder(String.valueOf(actionId)).toString());
                return false;
            }
        });
        this.btn_Select = (LinearLayout) view.findViewById(R.id.btn_select);
        this.btn_Select.setOnClickListener(this);
        this.btn_Scan = (LinearLayout) view.findViewById(R.id.btn_scan);
        this.btn_Scan.setOnClickListener(this);
        this.view_ModifyEnable = view.findViewById(R.id.txt_enable);
        this.txt_CustomerName = (TextView) view.findViewById(R.id.txt_customer_name);
        this.txt_AddStreet = (TextView) view.findViewById(R.id.txt_add_street);
        this.txt_HouseNr = (TextView) view.findViewById(R.id.txt_house_nr);
        this.txt_City = (TextView) view.findViewById(R.id.txt_city);
        this.txt_District = (TextView) view.findViewById(R.id.txt_district);
        this.txt_Pin = (TextView) view.findViewById(R.id.txt_pin);
        this.txt_State = (TextView) view.findViewById(R.id.txt_state);
        this.txt_ContactName = (TextView) view.findViewById(R.id.txt_contact_name);
        this.txt_Email = (TextView) view.findViewById(R.id.txt_email);
        this.txt_ContactNr = (TextView) view.findViewById(R.id.txt_contact_nr);
        this.btn_GetLocation = view.findViewById(R.id.btn_getlocation);
        this.btn_GetLocation.setOnClickListener(this);
        this.txt_Longitude = (TextView) view.findViewById(R.id.txt_longitude);
        this.txt_Latitude = (TextView) view.findViewById(R.id.txt_latitude);
        this.chk_ConnectionType = new RadioButton[]{(RadioButton) view.findViewById(R.id.chk_connectiontype1), (RadioButton) view.findViewById(R.id.chk_connectiontype2), (RadioButton) view.findViewById(R.id.chk_connectiontype3), (RadioButton) view.findViewById(R.id.chk_connectiontype4)};
        if (2 != Function) {
            this.view_ModifyEnable.setVisibility(View.GONE);
            this.btn_GetLocation.setEnabled(true);
        }
        setFunction(this.m_Function);
        this.selectClick = new DoubleClick(new DoubleClick.OnDoubleClick() {
            /* class com.clou.rs350.ui.view.infoview.CustomerInfoView.AnonymousClass2 */

            private void searchKey(String condition) {
                List<String> tmpSerials = CustomerInfoView.this.m_Dao.queryAllKey(condition);
                ListPopWindow.showListPopwindow(CustomerInfoView.this.m_Context, (String[]) tmpSerials.toArray(new String[tmpSerials.size()]), CustomerInfoView.this.txt_CustomerSrSelect, new View.OnClickListener() {
                    /* class com.clou.rs350.ui.view.infoview.CustomerInfoView.AnonymousClass2.AnonymousClass1 */

                    public void onClick(View v) {
                        CustomerInfoView.this.searchData(CustomerInfoView.this.txt_CustomerSrSelect.getText().toString());
                    }
                });
            }

            @Override // com.clou.rs350.utils.DoubleClick.OnDoubleClick
            public void doubleClick() {
                searchKey(CustomerInfoView.this.txt_CustomerSrSelect.getText().toString());
            }

            @Override // com.clou.rs350.utils.DoubleClick.OnDoubleClick
            public void singleClick() {
                searchKey(PdfObject.NOTHING);
            }
        });
        initTxtList();
        showData();
    }

    private void initTxtList() {
        this.txtList = new ArrayList();
        this.txtList.add(new TextView[]{this.txt_CustomerName});
        this.txtList.add(new TextView[]{this.txt_AddStreet});
        this.txtList.add(new TextView[]{this.txt_HouseNr});
        this.txtList.add(new TextView[]{this.txt_State});
        this.txtList.add(new TextView[]{this.txt_City});
        this.txtList.add(new TextView[]{this.txt_District});
        this.txtList.add(new TextView[]{this.txt_Pin});
        this.txtList.add(new TextView[]{this.txt_ContactName});
        this.txtList.add(new TextView[]{this.txt_Email});
        this.txtList.add(new TextView[]{this.txt_ContactNr});
        this.txtList.add(new TextView[]{this.txt_Longitude, this.txt_Latitude});
    }

    public void refreshData(CustomerInfo info) {
        this.m_Info = info;
        showData();
    }

    public void refreshFieldSetting() {
        this.m_FieldSetting = new FieldSettingUtil(this.m_Context).getFieldSetting(FieldSettingUtil.CUSTOMERINFO);
        for (int i = 0; i < this.txtList.size(); i++) {
            FieldItem tmpItem = this.m_FieldSetting.Items.get(this.m_FieldSetting.ItemKeys.get(i));
            TextView[] tmpTxtView = this.txtList.get(i);
            for (int k = 0; k < tmpTxtView.length; k++) {
                if (1 == tmpItem.RequiredSetting && 1 == this.m_RequiredField) {
                    tmpTxtView[k].setHint(R.string.text_required_field);
                } else {
                    tmpTxtView[k].setHint(PdfObject.NOTHING);
                }
            }
        }
    }

    public void setRequiredField(int flag) {
        this.m_RequiredField = flag;
        refreshFieldSetting();
    }

    public String getKey() {
        return this.txt_CustomerSrSelect.getText().toString();
    }

    public boolean getHasSearch() {
        return this.m_HasSearch;
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private void searchData(String serialNo) {
        this.m_Info = this.m_Dao.queryObjectBySerialNo(serialNo);
        if (this.m_Info != null) {
            this.m_HasSearch = true;
        } else {
            this.m_HasSearch = false;
            this.m_Info = new CustomerInfo();
            this.m_Info.CustomerSr = serialNo;
        }
        refreshData(this.m_Info);
    }

    private void showData() {
        if (this.m_Info != null) {
            this.txt_CustomerSrSelect.setText(this.m_Info.CustomerSr);
            this.txt_CustomerName.setText(this.m_Info.CustomerName);
            this.txt_AddStreet.setText(this.m_Info.AddStreet);
            this.txt_HouseNr.setText(this.m_Info.HouseNumber);
            this.txt_City.setText(this.m_Info.City);
            this.txt_District.setText(this.m_Info.District);
            this.txt_Pin.setText(this.m_Info.Pin);
            this.txt_State.setText(this.m_Info.State);
            this.txt_ContactName.setText(this.m_Info.ContactPersonName);
            this.txt_Email.setText(this.m_Info.Email);
            this.txt_ContactNr.setText(this.m_Info.ConstactPhoneNumber);
            this.txt_Longitude.setText(this.m_Info.Longitude);
            this.txt_Latitude.setText(this.m_Info.Latitude);
            int i = 0;
            while (true) {
                if (i >= this.chk_ConnectionType.length) {
                    break;
                } else if (this.chk_ConnectionType[i].getText().equals(this.m_Info.TypeofConnection)) {
                    this.chk_ConnectionType[i].setChecked(true);
                    break;
                } else {
                    i++;
                }
            }
        }
        refreshFieldSetting();
    }

    public void setFunction(int function) {
        boolean z;
        boolean z2;
        boolean z3;
        boolean z4 = false;
        this.m_Function = function;
        this.view_ModifyEnable.setVisibility(function < 2 ? View.VISIBLE : View.GONE);
        View view = this.btn_GetLocation;
        if (function > 1) {
            z = true;
        } else {
            z = false;
        }
        view.setEnabled(z);
        EditText editText = this.txt_CustomerSrSelect;
        if (function == 1 || function == 3 || function == 4) {
            z2 = true;
        } else {
            z2 = false;
        }
        editText.setEnabled(z2);
        LinearLayout linearLayout = this.btn_Select;
        if (function == 1 || function == 4) {
            z3 = true;
        } else {
            z3 = false;
        }
        linearLayout.setEnabled(z3);
        LinearLayout linearLayout2 = this.btn_Scan;
        if (function == 1 || function == 3 || function == 4) {
            z4 = true;
        }
        linearLayout2.setEnabled(z4);
    }

    public CustomerInfo getInfo() {
        parseData();
        return this.m_Info;
    }

    private void parseData() {
        this.m_Info.CustomerSr = this.txt_CustomerSrSelect.getText().toString();
        this.m_Info.CustomerName = this.txt_CustomerName.getText().toString();
        this.m_Info.AddStreet = this.txt_AddStreet.getText().toString();
        this.m_Info.HouseNumber = this.txt_HouseNr.getText().toString();
        this.m_Info.City = this.txt_City.getText().toString();
        this.m_Info.District = this.txt_District.getText().toString();
        this.m_Info.Pin = this.txt_Pin.getText().toString();
        this.m_Info.State = this.txt_State.getText().toString();
        this.m_Info.ContactPersonName = this.txt_ContactName.getText().toString();
        this.m_Info.Email = this.txt_Email.getText().toString();
        this.m_Info.ConstactPhoneNumber = this.txt_ContactNr.getText().toString();
        this.m_Info.Longitude = this.txt_Longitude.getText().toString();
        this.m_Info.Latitude = this.txt_Latitude.getText().toString();
        for (int i = 0; i < this.chk_ConnectionType.length; i++) {
            if (this.chk_ConnectionType[i].isChecked()) {
                this.m_Info.TypeofConnection = this.chk_ConnectionType[i].getText().toString();
                return;
            }
        }
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_select:
                this.selectClick.click();
                return;
            case R.id.btn_scan:
                CaptureDialog tmpDialog = new CaptureDialog(this.m_Context);
                tmpDialog.setScanResult(new CaptureDialog.ScanResult() {
                    /* class com.clou.rs350.ui.view.infoview.CustomerInfoView.AnonymousClass3 */

                    @Override // google.zxing.camera.CaptureDialog.ScanResult
                    public void getScanResult(String result) {
                        CustomerInfoView.this.txt_CustomerSrSelect.setText(result);
                        CustomerInfoView.this.searchData(CustomerInfoView.this.txt_CustomerSrSelect.getText().toString());
                    }
                });
                tmpDialog.show();
                return;
            case R.id.btn_getlocation:
                getLocation();
                return;
            default:
                return;
        }
    }

    private void getLocation() {
        String provider;
        LocationManager locationManager = (LocationManager) this.m_Context.getSystemService("location");
        List<String> list = locationManager.getProviders(true);
        Iterator<GpsSatellite> iterable = locationManager.getGpsStatus(null).getSatellites().iterator();
        if (list.contains("gps") && iterable.hasNext()) {
            provider = "gps";
        } else if (list.contains("network")) {
            provider = "network";
        } else {
            new HintDialog(this.m_Context, (int) R.string.text_check_gps).show();
            return;
        }
        Location location = locationManager.getLastKnownLocation(provider);
        if (location != null) {
            this.txt_Longitude.setText(new StringBuilder(String.valueOf(location.getLongitude())).toString());
            this.txt_Latitude.setText(new StringBuilder(String.valueOf(location.getLatitude())).toString());
        }
        locationManager.requestLocationUpdates(provider, 2000, ColumnText.GLOBAL_SPACE_CHAR_RATIO, new LocationListener() {
            /* class com.clou.rs350.ui.view.infoview.CustomerInfoView.AnonymousClass4 */

            public void onStatusChanged(String provider, int status, Bundle extras) {
            }

            public void onProviderEnabled(String provider) {
            }

            public void onProviderDisabled(String provider) {
            }

            public void onLocationChanged(Location location) {
                if (location != null) {
                    CustomerInfoView.this.txt_Longitude.setText(new StringBuilder(String.valueOf(location.getLongitude())).toString());
                    CustomerInfoView.this.txt_Latitude.setText(new StringBuilder(String.valueOf(location.getLatitude())).toString());
                }
            }
        });
    }

    public boolean hasSetParams() {
        boolean result = true;
        String strHint = PdfObject.NOTHING;
        if (1 == this.m_RequiredField) {
            for (int i = 0; i < this.txtList.size(); i++) {
                FieldItem tmpItem = this.m_FieldSetting.Items.get(this.m_FieldSetting.ItemKeys.get(i));
                if (1 == tmpItem.RequiredSetting) {
                    boolean tmpResult = true;
                    TextView[] tmpTxtView = this.txtList.get(i);
                    int k = 0;
                    while (true) {
                        if (k >= tmpTxtView.length) {
                            break;
                        } else if (OtherUtils.isEmpty(tmpTxtView[k])) {
                            tmpResult = false;
                            break;
                        } else {
                            k++;
                        }
                    }
                    if (!tmpResult) {
                        result = false;
                        strHint = strHint + this.m_Context.getResources().getString(tmpItem.Id) + " " + this.m_Context.getResources().getString(R.string.text_is_empty) + "\r\n";
                    }
                }
            }
            if (!result) {
                new HintDialog(this.m_Context, strHint.substring(0, strHint.length() - 2)).show();
            }
        }
        return result;
    }
}
