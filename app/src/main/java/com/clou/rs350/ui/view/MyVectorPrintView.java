package com.clou.rs350.ui.view;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.util.AttributeSet;

import com.clou.rs350.R;
import com.clou.rs350.model.ClouData;
import com.clou.rs350.utils.AngleUtil;
import com.itextpdf.text.pdf.PdfObject;

public class MyVectorPrintView extends MyClockView {
    public static final float CURRENTLINELENGTH = 0.6f;
    public static final float VOLTAGELINELENGTH = 0.88f;
    private int[] Colors;
    private int m_AngleDefinition;
    private int m_VectorDefinition;

    public MyVectorPrintView(Context context) {
        super(context);
        this.m_VectorDefinition = 0;
        this.m_AngleDefinition = 0;
        this.m_VectorDefinition = ClouData.getInstance().getSettingBasic().VectorDefinition;
        this.m_AngleDefinition = ClouData.getInstance().getSettingBasic().AngleDefinition;
        this.Colors = ClouData.getInstance().getSettingBasic().PhaseColors;
    }

    public MyVectorPrintView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        this.m_VectorDefinition = 0;
        this.m_AngleDefinition = 0;
        this.m_VectorDefinition = ClouData.getInstance().getSettingBasic().VectorDefinition;
        this.m_AngleDefinition = ClouData.getInstance().getSettingBasic().AngleDefinition;
        this.Colors = ClouData.getInstance().getSettingBasic().PhaseColors;
    }

    public MyVectorPrintView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.m_VectorDefinition = 0;
        this.m_AngleDefinition = 0;
        this.m_VectorDefinition = ClouData.getInstance().getSettingBasic().VectorDefinition;
        this.m_AngleDefinition = ClouData.getInstance().getSettingBasic().AngleDefinition;
        this.Colors = ClouData.getInstance().getSettingBasic().PhaseColors;
    }

    public MyVectorPrintView(Context context, int angleDefinition) {
        super(context);
        this.m_VectorDefinition = 0;
        this.m_AngleDefinition = 0;
        this.m_VectorDefinition = (angleDefinition >> 1) & 1;
        this.m_AngleDefinition = angleDefinition & 1;
        this.Colors = ClouData.getInstance().getSettingBasic().PhaseColors;
    }

    private int getColor(int resColorId) {
        Context context = getContext();
        if (context != null) {
            return context.getResources().getColor(resColorId);
        }
        return 0;
    }

    private String getTypeStr(int type) {
        Context context = getContext();
        if (context == null) {
            return PdfObject.NOTHING;
        }
        switch (type) {
            case 1:
                return context.getResources().getString(R.string.text_1);
            case 2:
                return context.getResources().getString(R.string.text_2);
            default:
                return context.getResources().getString(R.string.text_3);
        }
    }

    private String[] getStr(int[] key) {
        Context context = getContext();
        String[] strs = new String[key.length];
        if (context == null) {
            return null;
        }
        for (int i = 0; i < key.length; i++) {
            strs[i] = key[i] == 0 ? PdfObject.NOTHING : context.getResources().getString(key[i]);
        }
        return strs;
    }

    private double getMax(double[] Values) {
        double max = 0.0d;
        for (int i = 0; i < Values.length; i++) {
            if (Values[i] > max) {
                max = Values[i];
            }
        }
        return max;
    }

    public void postInvalidate(Double[] valDouble) {
        double voltageMax = getMax(new double[]{valDouble[0].doubleValue(), valDouble[2].doubleValue(), valDouble[4].doubleValue()});
        double currentMax = getMax(new double[]{valDouble[6].doubleValue(), valDouble[8].doubleValue(), valDouble[10].doubleValue()});
        if (1 == this.m_AngleDefinition) {
            for (int i = 0; i < 6; i++) {
                valDouble[(i * 2) + 1] = Double.valueOf(AngleUtil.adjustAngle(360.0d - valDouble[(i * 2) + 1].doubleValue()));
            }
        }
        if (1 == this.m_VectorDefinition) {
            for (int i2 = 0; i2 < 6; i2++) {
                valDouble[(i2 * 2) + 1] = Double.valueOf(AngleUtil.adjustAngle(valDouble[(i2 * 2) + 1].doubleValue() + 270.0d));
            }
        }
        if (1 == this.m_LineMode && 1 == this.m_VectorType) {
            postInvalidate(valDouble, getStr(new int[]{R.string.text_u12, R.string.text_u23, R.string.text_u32, R.string.text_i1, R.string.text_i2, R.string.text_i3}), new int[]{this.Colors[0], this.Colors[1], this.Colors[2], this.Colors[3], this.Colors[4], this.Colors[5]}, new float[]{(float) ((valDouble[0].doubleValue() / voltageMax) * 0.8799999952316284d), (float) ((valDouble[2].doubleValue() / voltageMax) * 0.8799999952316284d), (float) ((valDouble[4].doubleValue() / voltageMax) * 0.8799999952316284d), (float) ((valDouble[6].doubleValue() / currentMax) * 0.6000000238418579d), (float) ((valDouble[8].doubleValue() / currentMax) * 0.6000000238418579d), (float) ((valDouble[10].doubleValue() / currentMax) * 0.6000000238418579d)});
            return;
        }
        postInvalidate(valDouble, getStr(new int[]{R.string.text_u1, R.string.text_u2, R.string.text_u3, R.string.text_i1, R.string.text_i2, R.string.text_i3}), new int[]{this.Colors[0], this.Colors[1], this.Colors[2], this.Colors[3], this.Colors[4], this.Colors[5]}, new float[]{(float) ((valDouble[0].doubleValue() / voltageMax) * 0.8799999952316284d), (float) ((valDouble[2].doubleValue() / voltageMax) * 0.8799999952316284d), (float) ((valDouble[4].doubleValue() / voltageMax) * 0.8799999952316284d), (float) ((valDouble[6].doubleValue() / currentMax) * 0.6000000238418579d), (float) ((valDouble[8].doubleValue() / currentMax) * 0.6000000238418579d), (float) ((valDouble[10].doubleValue() / currentMax) * 0.6000000238418579d)});
    }

    public void postInvalidate(Double[] valDouble, int angleDefiniation) {
        this.m_VectorDefinition = (angleDefiniation >> 1) & 1;
        this.m_AngleDefinition = angleDefiniation & 1;
        postInvalidate(valDouble);
    }

    public void postInvalidate(Double[] valDouble, String[] name) {
        postInvalidate(valDouble, name, new int[]{this.Colors[0], this.Colors[1], this.Colors[2], this.Colors[3], this.Colors[4], this.Colors[5]}, new float[]{0.88f, 0.88f, 0.88f, 0.6f, 0.6f, 0.6f});
        super.postInvalidate();
    }

    public void postInvalidate(Double[] valDouble, int[] phase, int lineMode) {
        String[] tmp_VoltageName;
        int[] iArr = new int[5];
        iArr[1] = R.string.text_i1;
        iArr[2] = R.string.text_i2;
        iArr[3] = R.string.text_i3;
        String[] tmp_CurrnetName = getStr(iArr);
        int[] tmp_Color = {getColor(R.color.transparent), getColor(R.color.color_l1), getColor(R.color.color_l2), getColor(R.color.color_l3), getColor(R.color.color_l4)};
        if (lineMode == 1) {
            int[] iArr2 = new int[5];
            iArr2[1] = R.string.text_u12;
            iArr2[3] = R.string.text_u32;
            tmp_VoltageName = getStr(iArr2);
            phase[1] = 0;
        } else {
            int[] iArr3 = new int[5];
            iArr3[1] = R.string.text_u1;
            iArr3[2] = R.string.text_u2;
            iArr3[3] = R.string.text_u3;
            tmp_VoltageName = getStr(iArr3);
        }
        postInvalidate(valDouble, new String[]{tmp_VoltageName[phase[0]], tmp_VoltageName[phase[1]], tmp_VoltageName[phase[2]], tmp_CurrnetName[phase[3]], tmp_CurrnetName[phase[4]], tmp_CurrnetName[phase[5]]}, new int[]{tmp_Color[phase[0]], tmp_Color[phase[1]], tmp_Color[phase[2]], tmp_Color[phase[3]], tmp_Color[phase[4]], tmp_Color[phase[5]]}, new float[]{0.88f, 0.88f, 0.88f, 0.6f, 0.6f, 0.6f});
    }

    public void postInvalidate(Double[] valDouble, String[] name, int[] color, float[] ratio) {
        this.lines = new LineModel[name.length];
        for (int i = 0; i < this.lines.length; i++) {
            this.lines[i] = new LineModel(ratio[i], valDouble[(i * 2) + 1].doubleValue(), valDouble[i * 2].doubleValue(), name[i], color[i]);
        }
    }

    public void postInvalidatePT(Double[] valDouble, Double[] angle, String[] name) {
        this.lines = new LineModel[name.length];
        int[] color = {this.Colors[0], this.Colors[1], this.Colors[2], this.Colors[3], this.Colors[4], this.Colors[5]};
        float[] ratio = {0.9f, 0.9f, 0.9f, 0.7f, 0.7f, 0.7f};
        for (int i = 0; i < name.length; i++) {
            this.lines[i] = new LineModel(ratio[i], angle[i].doubleValue(), valDouble[i].doubleValue(), name[i], color[i]);
        }
        super.postInvalidate();
    }

    public Bitmap getBitmap(Double[] valDouble, int angleDefiniation, int lineMode, int vectorType) {
        this.CircleLine = -16777216;
        this.CircleBackground = -1;
        this.m_LineMode = lineMode;
        this.m_VectorType = vectorType;
        this.width = 380;
        this.height = 380;
        this.m_StrokeWidth = 2.0f;
        Bitmap mBitmap = Bitmap.createBitmap(this.width, this.height, Bitmap.Config.ARGB_8888);
        mBitmap.eraseColor(-1);
        Canvas mCanvas = new Canvas(mBitmap);
        postInvalidate(valDouble, angleDefiniation);
        onDraw(mCanvas);
        return mBitmap;
    }
}
