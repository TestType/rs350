package com.clou.rs350.ui.fragment.realtimefragment;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.TextView;

import com.clou.rs350.CLApplication;
import com.clou.rs350.Preferences;
import com.clou.rs350.R;
import com.clou.rs350.callback.ILoadCallback;
import com.clou.rs350.constants.Constants;
import com.clou.rs350.db.model.WiringCheckMeasurement;
import com.clou.rs350.device.MeterBaseDevice;
import com.clou.rs350.handler.HandlerSwitchView;
import com.clou.rs350.model.ClouData;
import com.clou.rs350.task.ISleepCallback;
import com.clou.rs350.task.MyTask;
import com.clou.rs350.task.TimerThread;
import com.clou.rs350.ui.dialog.LimitDialog;
import com.clou.rs350.ui.fragment.BaseFragment;
import com.clou.rs350.ui.popwindow.ListPopWindow;
import com.clou.rs350.ui.view.MyPowerQuadrantView;
import com.clou.rs350.ui.view.MyVectorView;
import com.clou.rs350.utils.AngleUtil;
import com.clou.rs350.utils.FileUtils;
import com.clou.rs350.utils.OtherUtils;
import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.view.annotation.ViewInject;
import com.lidroid.xutils.view.annotation.event.OnClick;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

public class WiringCheckCNFragment extends BaseFragment implements View.OnClickListener, HandlerSwitchView.IOnSelectCallback {
    @ViewInject(R.id.angle1)
    private TextView Angle1;
    @ViewInject(R.id.angle2)
    private TextView Angle2;
    @ViewInject(R.id.angle3)
    private TextView Angle3;
    @ViewInject(R.id.angle4)
    private TextView Angle4;
    @ViewInject(R.id.txt_correction_factor)
    private TextView CorrectionFactorTv;
    @ViewInject(R.id.i1)
    private TextView I1;
    @ViewInject(R.id.i2)
    private TextView I2;
    @ViewInject(R.id.i3)
    private TextView I3;
    @ViewInject(R.id.i4)
    private TextView I4;
    @ViewInject(R.id.i)
    private TextView ITv;
    @ViewInject(R.id.l1anglei)
    private TextView L1AngleITv;
    @ViewInject(R.id.l1angleui)
    private TextView L1AngleUITv;
    @ViewInject(R.id.l1angleu)
    private TextView L1AngleUTv;
    @ViewInject(R.id.l1currentbalance)
    private TextView L1CurrentBalanceTv;
    @ViewInject(R.id.l1currentdirection)
    private TextView L1CurrentDirectionTv;
    @ViewInject(R.id.l1currentsymmetry)
    private TextView L1CurrentSymmetryTv;
    @ViewInject(R.id.l1currentthd)
    private TextView L1CurrentThdTv;
    @ViewInject(R.id.l1current)
    private TextView L1CurrentTv;
    @ViewInject(R.id.txt_il1)
    private TextView L1ITv;
    @ViewInject(R.id.l1p)
    private TextView L1PTv;
    @ViewInject(R.id.l1phasesequencevoltage)
    private TextView L1PhaseSequenceVoltageTv;
    @ViewInject(R.id.l1q)
    private TextView L1QTv;
    @ViewInject(R.id.txt_ul1)
    private TextView L1UTv;
    @ViewInject(R.id.l1voltagebalance)
    private TextView L1VoltageBalanceTv;
    @ViewInject(R.id.l1voltagesymmetry)
    private TextView L1VoltageSymmetryTv;
    @ViewInject(R.id.l1voltagethd)
    private TextView L1VoltageThdTv;
    @ViewInject(R.id.l1voltage)
    private TextView L1VoltageTv;
    @ViewInject(R.id.l2anglei)
    private TextView L2AngleITv;
    @ViewInject(R.id.l2angleui)
    private TextView L2AngleUITv;
    @ViewInject(R.id.l2angleu)
    private TextView L2AngleUTv;
    @ViewInject(R.id.l2currentbalance)
    private TextView L2CurrentBalanceTv;
    @ViewInject(R.id.l2currentdirection)
    private TextView L2CurrentDirectionTv;
    @ViewInject(R.id.l2currentsymmetry)
    private TextView L2CurrentSymmetryTv;
    @ViewInject(R.id.l2currentthd)
    private TextView L2CurrentThdTv;
    @ViewInject(R.id.l2current)
    private TextView L2CurrentTv;
    @ViewInject(R.id.txt_il2)
    private TextView L2ITv;
    @ViewInject(R.id.l2p)
    private TextView L2PTv;
    @ViewInject(R.id.l1phasesequencecurrent)
    private TextView L2PhaseSequenceCurrentTv;
    @ViewInject(R.id.l2q)
    private TextView L2QTv;
    @ViewInject(R.id.txt_ul2)
    private TextView L2UTv;
    @ViewInject(R.id.l2voltagebalance)
    private TextView L2VoltageBalanceTv;
    @ViewInject(R.id.l2voltagesymmetry)
    private TextView L2VoltageSymmetryTv;
    @ViewInject(R.id.l2voltagethd)
    private TextView L2VoltageThdTv;
    @ViewInject(R.id.l2voltage)
    private TextView L2VoltageTv;
    @ViewInject(R.id.l3anglei)
    private TextView L3AngleITv;
    @ViewInject(R.id.l3angleui)
    private TextView L3AngleUITv;
    @ViewInject(R.id.l3angleu)
    private TextView L3AngleUTv;
    @ViewInject(R.id.l3currentbalance)
    private TextView L3CurrentBalanceTv;
    @ViewInject(R.id.l3currentdirection)
    private TextView L3CurrentDirectionTv;
    @ViewInject(R.id.l3currentsymmetry)
    private TextView L3CurrentSymmetryTv;
    @ViewInject(R.id.l3currentthd)
    private TextView L3CurrentThdTv;
    @ViewInject(R.id.l3current)
    private TextView L3CurrentTv;
    @ViewInject(R.id.txt_il3)
    private TextView L3ITv;
    @ViewInject(R.id.l3p)
    private TextView L3PTv;
    @ViewInject(R.id.l3q)
    private TextView L3QTv;
    @ViewInject(R.id.txt_ul3)
    private TextView L3UTv;
    @ViewInject(R.id.l3voltagebalance)
    private TextView L3VoltageBalanceTv;
    @ViewInject(R.id.l3voltagesymmetry)
    private TextView L3VoltageSymmetryTv;
    @ViewInject(R.id.l3voltagethd)
    private TextView L3VoltageThdTv;
    @ViewInject(R.id.l3voltage)
    private TextView L3VoltageTv;
    @ViewInject(R.id.p1)
    private TextView P1;
    @ViewInject(R.id.p2)
    private TextView P2;
    @ViewInject(R.id.p3)
    private TextView P3;
    @ViewInject(R.id.p4)
    private TextView P4;
    @ViewInject(R.id.pf1)
    private TextView PF1;
    @ViewInject(R.id.pf2)
    private TextView PF2;
    @ViewInject(R.id.pf3)
    private TextView PF3;
    @ViewInject(R.id.pf4)
    private TextView PF4;
    @ViewInject(R.id.powerquadrant_view1)
    private MyPowerQuadrantView PowerQuadrantView1;
    @ViewInject(R.id.powerquadrant_view2)
    private MyPowerQuadrantView PowerQuadrantView2;
    @ViewInject(R.id.powerquadrant_view3)
    private MyPowerQuadrantView PowerQuadrantView3;
    @ViewInject(R.id.powerquadrant_view4)
    private MyPowerQuadrantView PowerQuadrantView4;
    @ViewInject(R.id.q1)
    private TextView Q1;
    @ViewInject(R.id.q2)
    private TextView Q2;
    @ViewInject(R.id.q3)
    private TextView Q3;
    @ViewInject(R.id.q4)
    private TextView Q4;
    @ViewInject(R.id.tvremark)
    private TextView RemarkTv;
    @ViewInject(R.id.u1)
    private TextView U1;
    @ViewInject(R.id.u2)
    private TextView U2;
    @ViewInject(R.id.u3)
    private TextView U3;
    @ViewInject(R.id.u4)
    private TextView U4;
    @ViewInject(R.id.u)
    private TextView UTv;
    @ViewInject(R.id.btn_vector_definition)
    private Button btn_VectorDefinition;
    int delay = 0;
    private int m_AngleDefinition;
    private int m_VectorDefinition;
    private int m_VectorType;
    private String[] m_WiringCheckCos;
    WiringCheckMeasurement m_WiringCheckMeasurement;
    private String[] m_strVectorDefinition;
    private TimerThread readDataThread = null;
    private HandlerSwitchView switchView;
    @ViewInject(R.id.txt_cos)
    private TextView txt_Cos;
    @ViewInject(R.id.realtime_vector_clockview)
    private MyVectorView vectorView;

    public WiringCheckCNFragment(WiringCheckMeasurement Data) {
        this.m_WiringCheckMeasurement = Data;
    }

    @Override // android.support.v4.app.Fragment
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        this.fragmentView = inflater.inflate(R.layout.fragment_wiring_check_cn, (ViewGroup) null);
        ViewUtils.inject(this, this.fragmentView);
        this.m_strVectorDefinition = getResources().getStringArray(R.array.vectordefinition);
        this.m_WiringCheckCos = getResources().getStringArray(R.array.wiring_check_cos);
        return this.fragmentView;
    }

    @Override // android.support.v4.app.Fragment, com.clou.rs350.ui.fragment.BaseFragment
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initView();
    }

    private void initView() {
        List<View> tabViews = new ArrayList<>();
        tabViews.add(findViewById(R.id.btn_wiring));
        tabViews.add(findViewById(R.id.btn_powerquadrant));
        tabViews.add(findViewById(R.id.btn_analysis));
        tabViews.add(findViewById(R.id.btn_simulator));
        this.switchView = new HandlerSwitchView(this.m_Context);
        this.switchView.setTabViews(tabViews);
        this.switchView.boundClick();
        this.switchView.setOnTabChangedCallback(this);
        this.switchView.selectView(0);
        this.RemarkTv.setMovementMethod(ScrollingMovementMethod.getInstance());
        WebView webview = (WebView) findViewById(R.id.webview_simulator);
        webview.loadUrl("file://" + CLApplication.app.getSimulatorPath() + "/cas.html");
        webview.getSettings().setJavaScriptEnabled(true);
        webview.getSettings().setLayoutAlgorithm(WebSettings.LayoutAlgorithm.SINGLE_COLUMN);
        webview.getSettings().setLoadWithOverviewMode(true);
        webview.getSettings().setSupportZoom(true);
        webview.getSettings().setUseWideViewPort(true);
        webview.getSettings().setBuiltInZoomControls(true);
    }

    @Override // android.support.v4.app.Fragment, com.clou.rs350.ui.fragment.BaseFragment
    public void onResume() {
        super.onResume();
        readTestData();
        this.m_AngleDefinition = Preferences.getInt("AngleDefinition", 0);
        this.m_VectorDefinition = Preferences.getInt(Preferences.Key.VECTORDEFINITION, 0);
        this.m_VectorType = Preferences.getInt(Preferences.Key.VECTORTYPE, 0);
        this.messageManager.setFunc_Mstate(49);
        this.btn_VectorDefinition.setText(this.m_strVectorDefinition[this.m_VectorDefinition]);
        refreshData();
    }

    @Override // android.support.v4.app.Fragment, com.clou.rs350.ui.fragment.BaseFragment
    public void onPause() {
        killThread(this.readDataThread);
        this.messageManager.setFunc_Mstate(0);
        super.onPause();
    }

    @Override // com.clou.rs350.ui.fragment.BaseFragment
    public void refreshData() {
        this.m_WiringCheckMeasurement.setLimits();
    }

    private void readTestData() {
        if (this.readDataThread == null || this.readDataThread.isStop()) {
            this.readDataThread = new TimerThread((long) (2000 / 2), new ISleepCallback() {
                /* class com.clou.rs350.ui.fragment.realtimefragment.WiringCheckCNFragment.AnonymousClass1 */
                int flag = 0;

                @Override // com.clou.rs350.task.ISleepCallback
                public void onSleepAfterToDo() {
                    if (this.flag % 2 == 0) {
                        WiringCheckCNFragment.this.messageManager.getData();
                    } else {
                        WiringCheckCNFragment.this.messageManager.getWiringCheck();
                    }
                    this.flag++;
                }
            }, "readDataThread");
            this.readDataThread.start();
        }
    }

    @OnClick({R.id.btn_limit_setting, R.id.btn_vector_definition, R.id.txt_cos})
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_limit_setting:
                new LimitDialog(this.m_Context, new View.OnClickListener() {
                    /* class com.clou.rs350.ui.fragment.realtimefragment.WiringCheckCNFragment.AnonymousClass2 */

                    public void onClick(View v) {
                        WiringCheckCNFragment.this.refreshData();
                    }
                }).show();
                return;
            case R.id.btn_vector_definition:
                this.m_VectorDefinition = (this.m_VectorDefinition + 1) % 2;
                Preferences.putInt(Preferences.Key.VECTORDEFINITION, this.m_VectorDefinition);
                this.vectorView.setVectorDefinition(this.m_VectorDefinition);
                this.PowerQuadrantView1.setVectorDefinition(this.m_VectorDefinition);
                this.PowerQuadrantView2.setVectorDefinition(this.m_VectorDefinition);
                this.PowerQuadrantView3.setVectorDefinition(this.m_VectorDefinition);
                this.PowerQuadrantView4.setVectorDefinition(this.m_VectorDefinition);
                this.btn_VectorDefinition.setText(this.m_strVectorDefinition[this.m_VectorDefinition]);
                ClouData.getInstance().getSettingBasic().VectorDefinition = this.m_VectorDefinition;
                this.messageManager.setDeviceAngleDefinition(true);
                return;
            case R.id.txt_cos:
                ListPopWindow.showListPopwindow(this.m_Context, this.m_WiringCheckCos, v, new View.OnClickListener() {
                    /* class com.clou.rs350.ui.fragment.realtimefragment.WiringCheckCNFragment.AnonymousClass3 */

                    public void onClick(View v) {
                        WiringCheckCNFragment.this.messageManager.setWiringCheckPowerFactor(Integer.parseInt(v.getTag().toString()));
                    }
                });
                return;
            default:
                return;
        }
    }

    public boolean hasPreferredApplication(Context context, Intent intent) {
        return !"android".equals(context.getPackageManager().resolveActivity(intent, 65536).activityInfo.packageName);
    }

    @Override // com.clou.rs350.ui.fragment.BaseFragment, com.clou.rs350.manager.MessageManager.IMessageListener
    public void onReceiveMessage(final MeterBaseDevice Device) {
        super.onReceiveMessage(Device);
        new MyTask(new ILoadCallback() {
            /* class com.clou.rs350.ui.fragment.realtimefragment.WiringCheckCNFragment.AnonymousClass4 */

            @Override // com.clou.rs350.callback.ILoadCallback
            public Object run() {
                WiringCheckCNFragment.this.m_WiringCheckMeasurement.parseData(Device, WiringCheckCNFragment.this.m_AngleDefinition);
                return null;
            }

            @Override // com.clou.rs350.callback.ILoadCallback
            public void callback(Object result) {
                WiringCheckCNFragment.this.L1UTv.setText(WiringCheckCNFragment.this.m_WiringCheckMeasurement.VoltageValueL1Show.s_Value);
                WiringCheckCNFragment.this.L2UTv.setText(WiringCheckCNFragment.this.m_WiringCheckMeasurement.VoltageValueL2Show.s_Value);
                WiringCheckCNFragment.this.L3UTv.setText(WiringCheckCNFragment.this.m_WiringCheckMeasurement.VoltageValueL3Show.s_Value);
                WiringCheckCNFragment.this.L1AngleUTv.setText(WiringCheckCNFragment.this.m_WiringCheckMeasurement.VoltageAngleL1Show.s_Value);
                WiringCheckCNFragment.this.L2AngleUTv.setText(WiringCheckCNFragment.this.m_WiringCheckMeasurement.VoltageAngleL2Show.s_Value);
                WiringCheckCNFragment.this.L3AngleUTv.setText(WiringCheckCNFragment.this.m_WiringCheckMeasurement.VoltageAngleL3Show.s_Value);
                WiringCheckCNFragment.this.L1AngleUITv.setText(WiringCheckCNFragment.this.m_WiringCheckMeasurement.VoltageCurrentAngleL1Show.s_Value);
                WiringCheckCNFragment.this.L2AngleUITv.setText(WiringCheckCNFragment.this.m_WiringCheckMeasurement.VoltageCurrentAngleL2Show.s_Value);
                WiringCheckCNFragment.this.L3AngleUITv.setText(WiringCheckCNFragment.this.m_WiringCheckMeasurement.VoltageCurrentAngleL3Show.s_Value);
                WiringCheckCNFragment.this.U1.setText(WiringCheckCNFragment.this.m_WiringCheckMeasurement.VoltageValueL1Show.s_Value);
                WiringCheckCNFragment.this.U2.setText(WiringCheckCNFragment.this.m_WiringCheckMeasurement.VoltageValueL2Show.s_Value);
                WiringCheckCNFragment.this.U3.setText(WiringCheckCNFragment.this.m_WiringCheckMeasurement.VoltageValueL3Show.s_Value);
                WiringCheckCNFragment.this.Angle1.setText(WiringCheckCNFragment.this.m_WiringCheckMeasurement.VoltageCurrentAngleL1Show.s_Value);
                WiringCheckCNFragment.this.Angle2.setText(WiringCheckCNFragment.this.m_WiringCheckMeasurement.VoltageCurrentAngleL2Show.s_Value);
                WiringCheckCNFragment.this.Angle3.setText(WiringCheckCNFragment.this.m_WiringCheckMeasurement.VoltageCurrentAngleL3Show.s_Value);
                WiringCheckCNFragment.this.L1ITv.setText(WiringCheckCNFragment.this.m_WiringCheckMeasurement.CurrentValueL1.s_Value);
                WiringCheckCNFragment.this.L2ITv.setText(WiringCheckCNFragment.this.m_WiringCheckMeasurement.CurrentValueL2.s_Value);
                WiringCheckCNFragment.this.L3ITv.setText(WiringCheckCNFragment.this.m_WiringCheckMeasurement.CurrentValueL3.s_Value);
                WiringCheckCNFragment.this.L1AngleITv.setText(WiringCheckCNFragment.this.m_WiringCheckMeasurement.CurrentAngleL1.s_Value);
                WiringCheckCNFragment.this.L2AngleITv.setText(WiringCheckCNFragment.this.m_WiringCheckMeasurement.CurrentAngleL2.s_Value);
                WiringCheckCNFragment.this.L3AngleITv.setText(WiringCheckCNFragment.this.m_WiringCheckMeasurement.CurrentAngleL3.s_Value);
                WiringCheckCNFragment.this.L1PTv.setText(WiringCheckCNFragment.this.m_WiringCheckMeasurement.ActivePowerL1.s_Value);
                WiringCheckCNFragment.this.L2PTv.setText(WiringCheckCNFragment.this.m_WiringCheckMeasurement.ActivePowerL2.s_Value);
                WiringCheckCNFragment.this.L3PTv.setText(WiringCheckCNFragment.this.m_WiringCheckMeasurement.ActivePowerL3.s_Value);
                WiringCheckCNFragment.this.L1QTv.setText(WiringCheckCNFragment.this.m_WiringCheckMeasurement.ReactivePowerL1.s_Value);
                WiringCheckCNFragment.this.L2QTv.setText(WiringCheckCNFragment.this.m_WiringCheckMeasurement.ReactivePowerL2.s_Value);
                WiringCheckCNFragment.this.L3QTv.setText(WiringCheckCNFragment.this.m_WiringCheckMeasurement.ReactivePowerL3.s_Value);
                WiringCheckCNFragment.this.UTv.setText(WiringCheckCNFragment.this.m_WiringCheckMeasurement.VoltagePhaseSequence);
                WiringCheckCNFragment.this.ITv.setText(WiringCheckCNFragment.this.m_WiringCheckMeasurement.CurrentPhaseSequence);
                WiringCheckCNFragment.this.RemarkTv.setText(WiringCheckCNFragment.this.m_WiringCheckMeasurement.Remark);
                WiringCheckCNFragment.this.CorrectionFactorTv.setText(WiringCheckCNFragment.this.m_WiringCheckMeasurement.CorrectionFactor.s_Value);
                WiringCheckCNFragment.this.txt_Cos.setText(WiringCheckCNFragment.this.m_WiringCheckCos[WiringCheckCNFragment.this.m_WiringCheckMeasurement.WiringCheckCos]);
                WiringCheckCNFragment.this.ShowVector(WiringCheckCNFragment.this.m_WiringCheckMeasurement);
                WiringCheckCNFragment.this.ShowPowerQuadrant(WiringCheckCNFragment.this.m_WiringCheckMeasurement);
                WiringCheckCNFragment.this.L1PhaseSequenceVoltageTv.setText(WiringCheckCNFragment.this.m_WiringCheckMeasurement.VoltagePhaseSequence);
                WiringCheckCNFragment.this.L2PhaseSequenceCurrentTv.setText(WiringCheckCNFragment.this.m_WiringCheckMeasurement.CurrentPhaseSequence);
                OtherUtils.setText(WiringCheckCNFragment.this.L1VoltageThdTv, WiringCheckCNFragment.this.m_WiringCheckMeasurement.SiteHarmonic[0]);
                OtherUtils.setText(WiringCheckCNFragment.this.L2VoltageThdTv, WiringCheckCNFragment.this.m_WiringCheckMeasurement.SiteHarmonic[1]);
                OtherUtils.setText(WiringCheckCNFragment.this.L3VoltageThdTv, WiringCheckCNFragment.this.m_WiringCheckMeasurement.SiteHarmonic[2]);
                OtherUtils.setText(WiringCheckCNFragment.this.L1CurrentThdTv, WiringCheckCNFragment.this.m_WiringCheckMeasurement.SiteHarmonic[3]);
                OtherUtils.setText(WiringCheckCNFragment.this.L2CurrentThdTv, WiringCheckCNFragment.this.m_WiringCheckMeasurement.SiteHarmonic[4]);
                OtherUtils.setText(WiringCheckCNFragment.this.L3CurrentThdTv, WiringCheckCNFragment.this.m_WiringCheckMeasurement.SiteHarmonic[5]);
                OtherUtils.setText(WiringCheckCNFragment.this.L1VoltageTv, WiringCheckCNFragment.this.m_WiringCheckMeasurement.SiteState[0]);
                OtherUtils.setText(WiringCheckCNFragment.this.L2VoltageTv, WiringCheckCNFragment.this.m_WiringCheckMeasurement.SiteState[1]);
                OtherUtils.setText(WiringCheckCNFragment.this.L3VoltageTv, WiringCheckCNFragment.this.m_WiringCheckMeasurement.SiteState[2]);
                OtherUtils.setText(WiringCheckCNFragment.this.L1CurrentTv, WiringCheckCNFragment.this.m_WiringCheckMeasurement.SiteState[3]);
                OtherUtils.setText(WiringCheckCNFragment.this.L2CurrentTv, WiringCheckCNFragment.this.m_WiringCheckMeasurement.SiteState[4]);
                OtherUtils.setText(WiringCheckCNFragment.this.L3CurrentTv, WiringCheckCNFragment.this.m_WiringCheckMeasurement.SiteState[5]);
                OtherUtils.setText(WiringCheckCNFragment.this.L1CurrentDirectionTv, WiringCheckCNFragment.this.m_WiringCheckMeasurement.SiteCurrentDirection[0]);
                OtherUtils.setText(WiringCheckCNFragment.this.L2CurrentDirectionTv, WiringCheckCNFragment.this.m_WiringCheckMeasurement.SiteCurrentDirection[1]);
                OtherUtils.setText(WiringCheckCNFragment.this.L3CurrentDirectionTv, WiringCheckCNFragment.this.m_WiringCheckMeasurement.SiteCurrentDirection[2]);
                OtherUtils.setText(WiringCheckCNFragment.this.L1VoltageBalanceTv, WiringCheckCNFragment.this.m_WiringCheckMeasurement.SiteVoltageBalance[0]);
                OtherUtils.setText(WiringCheckCNFragment.this.L2VoltageBalanceTv, WiringCheckCNFragment.this.m_WiringCheckMeasurement.SiteVoltageBalance[1]);
                OtherUtils.setText(WiringCheckCNFragment.this.L3VoltageBalanceTv, WiringCheckCNFragment.this.m_WiringCheckMeasurement.SiteVoltageBalance[2]);
                OtherUtils.setText(WiringCheckCNFragment.this.L1VoltageSymmetryTv, WiringCheckCNFragment.this.m_WiringCheckMeasurement.SiteVoltageSymmetry[0]);
                OtherUtils.setText(WiringCheckCNFragment.this.L2VoltageSymmetryTv, WiringCheckCNFragment.this.m_WiringCheckMeasurement.SiteVoltageSymmetry[1]);
                OtherUtils.setText(WiringCheckCNFragment.this.L3VoltageSymmetryTv, WiringCheckCNFragment.this.m_WiringCheckMeasurement.SiteVoltageSymmetry[2]);
                OtherUtils.setText(WiringCheckCNFragment.this.L1CurrentBalanceTv, WiringCheckCNFragment.this.m_WiringCheckMeasurement.SiteCurrentBalance[0]);
                OtherUtils.setText(WiringCheckCNFragment.this.L2CurrentBalanceTv, WiringCheckCNFragment.this.m_WiringCheckMeasurement.SiteCurrentBalance[1]);
                OtherUtils.setText(WiringCheckCNFragment.this.L3CurrentBalanceTv, WiringCheckCNFragment.this.m_WiringCheckMeasurement.SiteCurrentBalance[2]);
                OtherUtils.setText(WiringCheckCNFragment.this.L1CurrentSymmetryTv, WiringCheckCNFragment.this.m_WiringCheckMeasurement.SiteCurrentSymmetry[0]);
                OtherUtils.setText(WiringCheckCNFragment.this.L2CurrentSymmetryTv, WiringCheckCNFragment.this.m_WiringCheckMeasurement.SiteCurrentSymmetry[1]);
                OtherUtils.setText(WiringCheckCNFragment.this.L3CurrentSymmetryTv, WiringCheckCNFragment.this.m_WiringCheckMeasurement.SiteCurrentSymmetry[2]);
                WiringCheckCNFragment.this.I1.setText(WiringCheckCNFragment.this.m_WiringCheckMeasurement.CurrentValueL1.s_Value);
                WiringCheckCNFragment.this.I2.setText(WiringCheckCNFragment.this.m_WiringCheckMeasurement.CurrentValueL2.s_Value);
                WiringCheckCNFragment.this.I3.setText(WiringCheckCNFragment.this.m_WiringCheckMeasurement.CurrentValueL3.s_Value);
                WiringCheckCNFragment.this.PF1.setText(WiringCheckCNFragment.this.m_WiringCheckMeasurement.PowerFactorL1.s_Value);
                WiringCheckCNFragment.this.PF2.setText(WiringCheckCNFragment.this.m_WiringCheckMeasurement.PowerFactorL2.s_Value);
                WiringCheckCNFragment.this.PF3.setText(WiringCheckCNFragment.this.m_WiringCheckMeasurement.PowerFactorL3.s_Value);
                WiringCheckCNFragment.this.PF4.setText(WiringCheckCNFragment.this.m_WiringCheckMeasurement.PowerFactorTotal.s_Value);
                WiringCheckCNFragment.this.P1.setText(WiringCheckCNFragment.this.m_WiringCheckMeasurement.ActivePowerL1.s_Value);
                WiringCheckCNFragment.this.P2.setText(WiringCheckCNFragment.this.m_WiringCheckMeasurement.ActivePowerL2.s_Value);
                WiringCheckCNFragment.this.P3.setText(WiringCheckCNFragment.this.m_WiringCheckMeasurement.ActivePowerL3.s_Value);
                WiringCheckCNFragment.this.P4.setText(WiringCheckCNFragment.this.m_WiringCheckMeasurement.ActivePowerTotal.s_Value);
                WiringCheckCNFragment.this.Q1.setText(WiringCheckCNFragment.this.m_WiringCheckMeasurement.ReactivePowerL1.s_Value);
                WiringCheckCNFragment.this.Q2.setText(WiringCheckCNFragment.this.m_WiringCheckMeasurement.ReactivePowerL2.s_Value);
                WiringCheckCNFragment.this.Q3.setText(WiringCheckCNFragment.this.m_WiringCheckMeasurement.ReactivePowerL3.s_Value);
                WiringCheckCNFragment.this.Q4.setText(WiringCheckCNFragment.this.m_WiringCheckMeasurement.ReactivePowerTotal.s_Value);
                WiringCheckCNFragment.this.SaveTotxt();
            }
        }).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, new Integer[0]);
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private void SaveTotxt() {
        if (this.delay > 5) {
            this.delay = 0;
            new String();
            FileUtils.saveContentToFile(String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf(String.valueOf("function readRS(){importfile(\"") + new SimpleDateFormat(Constants.TIMEFORMAT).format(Long.valueOf(System.currentTimeMillis())) + ",") + this.m_WiringCheckMeasurement.VoltageValueL1.s_Value + ",") + this.m_WiringCheckMeasurement.VoltageValueL2.s_Value + ",") + this.m_WiringCheckMeasurement.VoltageValueL3.s_Value + ",") + this.m_WiringCheckMeasurement.CurrentValueL1.s_Value + ",") + this.m_WiringCheckMeasurement.CurrentValueL2.s_Value + ",") + this.m_WiringCheckMeasurement.CurrentValueL3.s_Value + ",") + this.m_WiringCheckMeasurement.VoltageAngleL1.s_Value + ",") + this.m_WiringCheckMeasurement.VoltageAngleL2.s_Value + ",") + this.m_WiringCheckMeasurement.VoltageAngleL3.s_Value + ",") + this.m_WiringCheckMeasurement.CurrentAngleL1.s_Value + ",") + this.m_WiringCheckMeasurement.CurrentAngleL2.s_Value + ",") + this.m_WiringCheckMeasurement.CurrentAngleL3.s_Value) + "\");}", String.valueOf(CLApplication.app.getSimulatorPath()) + "/readRS.js", this.m_Context);
        }
        this.delay++;
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private void ShowVector(WiringCheckMeasurement value) {
        this.vectorView.postInvalidate(new Double[]{Double.valueOf(value.VoltageValueL1Vector.d_Value), Double.valueOf(AngleUtil.adjustAngle(value.VoltageAngleL1Vector.d_Value)), Double.valueOf(value.VoltageValueL2Vector.d_Value), Double.valueOf(AngleUtil.adjustAngle(value.VoltageAngleL2Vector.d_Value)), Double.valueOf(value.VoltageValueL3Vector.d_Value), Double.valueOf(AngleUtil.adjustAngle(value.VoltageAngleL3Vector.d_Value)), Double.valueOf(value.CurrentValueL1.d_Value), Double.valueOf(AngleUtil.adjustAngle(value.CurrentAngleL1.d_Value)), Double.valueOf(value.CurrentValueL2.d_Value), Double.valueOf(AngleUtil.adjustAngle(value.CurrentAngleL2.d_Value)), Double.valueOf(value.CurrentValueL3.d_Value), Double.valueOf(AngleUtil.adjustAngle(value.CurrentAngleL3.d_Value))}, value.Wiring);
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private void ShowPowerQuadrant(WiringCheckMeasurement value) {
        this.PowerQuadrantView1.postInvalidate(value.ActivePowerL1.d_Value, value.ReactivePowerL1.d_Value, value.ApparentPowerL1.d_Value);
        this.PowerQuadrantView2.postInvalidate(value.ActivePowerL2.d_Value, value.ReactivePowerL2.d_Value, value.ApparentPowerL2.d_Value);
        this.PowerQuadrantView3.postInvalidate(value.ActivePowerL3.d_Value, value.ReactivePowerL3.d_Value, value.ApparentPowerL3.d_Value);
        this.PowerQuadrantView4.postInvalidate(value.ActivePowerTotal.d_Value, value.ReactivePowerTotal.d_Value, value.ApparentPowerTotal.d_Value);
    }

    @Override // com.clou.rs350.handler.HandlerSwitchView.IOnSelectCallback
    public void onSelectedChanged(int index) {
        setView(index);
    }

    @Override // com.clou.rs350.handler.HandlerSwitchView.IOnSelectCallback
    public void onClickView(View v) {
    }

    private void setView(int index) {
        int i;
        int i2;
        int i3;
        int i4 = 0;
        View findViewById = findViewById(R.id.connection_check_wiringcheckonline);
        if (index == 0) {
            i = 0;
        } else {
            i = 8;
        }
        findViewById.setVisibility(i);
        View findViewById2 = findViewById(R.id.connection_check_powerquadrant);
        if (index == 1) {
            i2 = 0;
        } else {
            i2 = 8;
        }
        findViewById2.setVisibility(i2);
        View findViewById3 = findViewById(R.id.connection_check_analysis);
        if (index == 2) {
            i3 = 0;
        } else {
            i3 = 8;
        }
        findViewById3.setVisibility(i3);
        View findViewById4 = findViewById(R.id.connection_check_simulator);
        if (index != 3) {
            i4 = 8;
        }
        findViewById4.setVisibility(i4);
    }
}
