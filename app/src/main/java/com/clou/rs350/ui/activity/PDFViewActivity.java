package com.clou.rs350.ui.activity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.clou.rs350.R;
import com.itextpdf.text.pdf.PdfObject;
import com.joanzapata.pdfview.PDFView;
import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.view.annotation.ViewInject;

import java.io.File;

public class PDFViewActivity extends BaseActivity implements View.OnClickListener {
    @ViewInject(R.id.btn_back)
    private Button m_BackButtom;
    private String m_EmailAddress = PdfObject.NOTHING;
    private String m_PDFPath = PdfObject.NOTHING;
    @ViewInject(R.id.v_pdfview)
    private PDFView m_PDFView;
    @ViewInject(R.id.title_textview)
    private TextView m_TitleTextView;

    /* access modifiers changed from: protected */
    @Override // com.clou.rs350.ui.activity.BaseActivity, android.support.v4.app.FragmentActivity
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pdfview);
        setDefaultTitle(R.string.text_pdf_view);
        ViewUtils.inject(this);
        initData();
    }

    private void initData() {
        Intent intent = getIntent();
        this.m_PDFPath = intent.getStringExtra("PDFPath");
        this.m_EmailAddress = intent.getStringExtra("Email");
        this.m_PDFView.fromFile(new File(this.m_PDFPath)).defaultPage(1).onPageChange(null).load();
    }

    /* access modifiers changed from: protected */
    @Override // com.clou.rs350.ui.activity.BaseActivity, android.support.v4.app.FragmentActivity
    public void onResume() {
        super.onResume();
    }

    /* access modifiers changed from: protected */
    @Override // com.clou.rs350.ui.activity.BaseActivity, android.support.v4.app.FragmentActivity
    public void onPause() {
        super.onPause();
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_back:
                finish();
                return;
            case R.id.btn_email:
                Intent email = new Intent("android.intent.action.SEND");
                email.setType("application/octet-stream");
                String[] emailReciver = {this.m_EmailAddress};
                String emailTitle = getString(R.string.text_email_tital);
                String emailContent = getString(R.string.text_email_content);
                email.putExtra("android.intent.extra.EMAIL", emailReciver);
                email.putExtra("android.intent.extra.SUBJECT", emailTitle);
                email.putExtra("android.intent.extra.TEXT", emailContent);
                email.putExtra("android.intent.extra.STREAM", Uri.fromFile(new File(this.m_PDFPath)));
                startActivity(Intent.createChooser(email, getString(R.string.text_select_email_software)));
                return;
            default:
                return;
        }
    }
}
