package com.clou.rs350.ui.popwindow;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.PopupWindow;
import android.widget.RadioButton;
import android.widget.TextView;

import com.clou.rs350.Preferences;
import com.clou.rs350.R;
import com.clou.rs350.model.ClouData;
import com.clou.rs350.ui.dialog.NumericDialog;
import com.clou.rs350.utils.OtherUtils;

public class DailyConfigurePopWindow extends PopupWindow implements View.OnClickListener, CompoundButton.OnCheckedChangeListener {
    RadioButton chk_AutoStop;
    CheckBox chk_IsBaseTime;
    RadioButton chk_ManualStop;
    CheckBox chk_Meter2Enable;
    CheckBox chk_Meter3Enable;
    Context mContext;
    View.OnClickListener mOnClick;
    int testMeterCount;
    TextView txt_ErrorCount;
    TextView txt_ErrorTime;
    TextView txt_WaitLimit;

    public DailyConfigurePopWindow(Context context) {
        super(context);
        this.mContext = context;
        initView();
    }

    private void initView() {
        View conentView = ((LayoutInflater) this.mContext.getSystemService("layout_inflater")).inflate(R.layout.pop_window_dailyconfigure, (ViewGroup) null);
        conentView.findViewById(R.id.ok).setOnClickListener(this);
        conentView.findViewById(R.id.cancel).setOnClickListener(this);
        this.txt_ErrorCount = (TextView) conentView.findViewById(R.id.txt_error_count);
        this.txt_ErrorCount.setOnClickListener(this);
        this.txt_ErrorTime = (TextView) conentView.findViewById(R.id.txt_time_basic);
        this.txt_ErrorTime.setOnClickListener(this);
        this.txt_WaitLimit = (TextView) conentView.findViewById(R.id.txt_wait_limit);
        this.txt_WaitLimit.setOnClickListener(this);
        this.chk_Meter2Enable = (CheckBox) conentView.findViewById(R.id.chk_meter2);
        this.chk_Meter3Enable = (CheckBox) conentView.findViewById(R.id.chk_meter3);
        this.chk_IsBaseTime = (CheckBox) conentView.findViewById(R.id.chk_is_base_time);
        this.chk_AutoStop = (RadioButton) conentView.findViewById(R.id.chk_auto_stop);
        this.chk_ManualStop = (RadioButton) conentView.findViewById(R.id.chk_manual_stop);
        setContentView(conentView);
        setWidth(this.mContext.getResources().getDimensionPixelSize(R.dimen.dp_450));
        setHeight(this.mContext.getResources().getDimensionPixelSize(R.dimen.dp_240));
        setFocusable(true);
        setOutsideTouchable(true);
        update();
        setBackgroundDrawable(new ColorDrawable(0));
        setAnimationStyle(16973826);
    }

    private void showValue() {
        boolean z;
        this.chk_Meter2Enable.setChecked((this.testMeterCount & 2) == 2);
        CheckBox checkBox = this.chk_Meter3Enable;
        if ((this.testMeterCount & 4) == 4) {
            z = true;
        } else {
            z = false;
        }
        checkBox.setChecked(z);
        this.txt_ErrorCount.setText(new StringBuilder(String.valueOf(Preferences.getInt("ErrorCount", 5))).toString());
        this.txt_ErrorTime.setText(new StringBuilder(String.valueOf(Preferences.getInt(Preferences.Key.TIMEBASEFORERROR, 5))).toString());
        this.chk_IsBaseTime.setChecked(Preferences.getBoolean(Preferences.Key.ERRORISBASETIME, false));
        this.txt_WaitLimit.setText(new StringBuilder(String.valueOf(Preferences.getInt(Preferences.Key.WAITLIMIT, 3))).toString());
        if (Preferences.getBoolean(Preferences.Key.ERRORAUTOSTOP, true)) {
            this.chk_AutoStop.setChecked(true);
        } else {
            this.chk_ManualStop.setChecked(true);
        }
    }

    private void praseValue() {
        int i = 0;
        int i2 = (this.chk_Meter2Enable.isChecked() ? 2 : 0) | 1;
        if (this.chk_Meter3Enable.isChecked()) {
            i = 4;
        }
        this.testMeterCount = i2 | i;
        ClouData.getInstance().setTestMeterCount(this.testMeterCount);
        Preferences.putInt("ErrorCount", OtherUtils.getIntValue(this.txt_ErrorCount));
        Preferences.putBoolean(Preferences.Key.ERRORISBASETIME, this.chk_IsBaseTime.isChecked());
        Preferences.putInt(Preferences.Key.TIMEBASEFORERROR, OtherUtils.getIntValue(this.txt_ErrorTime));
        Preferences.putInt(Preferences.Key.WAITLIMIT, OtherUtils.getIntValue(this.txt_WaitLimit));
        Preferences.putBoolean(Preferences.Key.ERRORAUTOSTOP, this.chk_AutoStop.isChecked());
    }

    public void showPopWindow(View currentClick, View.OnClickListener onclick) {
        this.mOnClick = onclick;
        this.testMeterCount = ClouData.getInstance().getTestMeterCount();
        showValue();
        if (!isShowing()) {
            int[] location = new int[2];
            currentClick.getLocationOnScreen(location);
            showAtLocation(currentClick, 0, location[0] - getWidth(), location[1]);
            return;
        }
        dismiss();
    }

    public void onClick(View v) {
        NumericDialog nd = new NumericDialog(this.mContext);
        switch (v.getId()) {
            case R.id.txt_error_count:
                nd.setMax(10.0d);
                nd.setMin(3.0d);
                nd.setMinus(false);
                nd.setDot(false);
                nd.showMyDialog((TextView) v);
                return;
            case R.id.chk_is_base_time:
            default:
                return;
            case R.id.txt_time_basic:
                nd.txtLimit = 3;
                nd.setMin(1.0d);
                nd.setDot(false);
                nd.setMinus(false);
                nd.showMyDialog((TextView) v);
                return;
            case R.id.txt_wait_limit:
                nd.txtLimit = 3;
                nd.setMin(2.0d);
                nd.setDot(false);
                nd.setMinus(false);
                nd.showMyDialog((TextView) v);
                return;
            case R.id.ok:
                praseValue();
                if (this.mOnClick != null) {
                    this.mOnClick.onClick(v);
                }
                if (isShowing()) {
                    dismiss();
                    return;
                }
                return;
            case R.id.cancel:
                if (isShowing()) {
                    dismiss();
                    return;
                }
                return;
        }
    }

    public void onCheckedChanged(CompoundButton v, boolean isChecked) {
    }
}
