package com.clou.rs350.ui.fragment.errorfragment;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.clou.rs350.Preferences;
import com.clou.rs350.R;
import com.clou.rs350.db.model.DBDataModel;
import com.clou.rs350.db.model.DemandTest;
import com.clou.rs350.device.MeterBaseDevice;
import com.clou.rs350.manager.MessageManager;
import com.clou.rs350.model.ClouData;
import com.clou.rs350.task.ISleepCallback;
import com.clou.rs350.task.TimerThread;
import com.clou.rs350.ui.activity.BaseActivity;
import com.clou.rs350.ui.dialog.HintDialog;
import com.clou.rs350.ui.popwindow.DemandConfigurePopWindow;
import com.clou.rs350.ui.popwindow.TrxRatioPopWindow;
import com.clou.rs350.utils.CountDown;
import com.clou.rs350.utils.OtherUtils;
import com.itextpdf.text.pdf.PdfObject;
import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.view.annotation.ViewInject;
import com.lidroid.xutils.view.annotation.event.OnClick;

public class DemandTestingFragment extends BaseErrorFragment implements CompoundButton.OnCheckedChangeListener {
    @ViewInject(R.id.meter1_demand)
    private TextView Meter1DemandTv;
    @ViewInject(R.id.meter1_error)
    private TextView Meter1ErrorTv;
    @ViewInject(R.id.meter1_ref)
    private TextView Meter1RefTv;
    @ViewInject(R.id.meter1_demand_unit)
    private TextView Meter1UnitTv;
    @ViewInject(R.id.meter2_demand)
    private TextView Meter2DemandTv;
    @ViewInject(R.id.meter2_error)
    private TextView Meter2ErrorTv;
    @ViewInject(R.id.meter2_ref)
    private TextView Meter2RefTv;
    @ViewInject(R.id.meter2_demand_unit)
    private TextView Meter2UnitTv;
    @ViewInject(R.id.meter3_demand)
    private TextView Meter3DemandTv;
    @ViewInject(R.id.meter3_error)
    private TextView Meter3ErrorTv;
    @ViewInject(R.id.meter3_ref)
    private TextView Meter3RefTv;
    @ViewInject(R.id.meter3_demand_unit)
    private TextView Meter3UnitTv;
    private TextView[] MeterDemandTvs;
    private TextView[] MeterErrorTvs;
    private TextView[] MeterRefTvs;
    @ViewInject(R.id.remainingtime_text)
    private TextView RemainingTimeTv;
    @ViewInject(R.id.testtime_edit)
    private TextView TestingTimeTv;
    @ViewInject(R.id.chk_meter2)
    private CheckBox chkMeter2;
    @ViewInject(R.id.chk_meter3)
    private CheckBox chkMeter3;
    @ViewInject(R.id.btn_configure)
    private Button configureBtn;
    private boolean hasStart = false;
    private DemandTest[] m_DemandTest;
    private int m_PQFlag = 0;
    private int m_TestType = 0;
    @ViewInject(R.id.progress)
    private ProgressBar progressBar;
    @ViewInject(R.id.btn_txr_ratio)
    private Button ratioBtn;
    private TimerThread readDataThread;
    @ViewInject(R.id.btn_start)
    private Button startBtn;
    private boolean[] testMeter;

    public DemandTestingFragment(DemandTest[] Data) {
        boolean[] zArr = new boolean[3];
        zArr[0] = true;
        this.testMeter = zArr;
        this.m_DemandTest = Data;
    }

    @Override // android.support.v4.app.Fragment
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        this.fragmentView = inflater.inflate(R.layout.fragment_demand_testing, (ViewGroup) null);
        ViewUtils.inject(this, this.fragmentView);
        return this.fragmentView;
    }

    @Override // android.support.v4.app.Fragment, com.clou.rs350.ui.fragment.BaseFragment
    public void onActivityCreated(Bundle savedInstanceState) {
        new CountDown();
        super.onActivityCreated(savedInstanceState);
        initView();
    }

    private void initView() {
        this.chkMeter2.setOnCheckedChangeListener(this);
        this.chkMeter3.setOnCheckedChangeListener(this);
        this.MeterDemandTvs = new TextView[]{this.Meter1DemandTv, this.Meter2DemandTv, this.Meter3DemandTv};
        this.MeterRefTvs = new TextView[]{this.Meter1RefTv, this.Meter2RefTv, this.Meter3RefTv};
        this.MeterErrorTvs = new TextView[]{this.Meter1ErrorTv, this.Meter2ErrorTv, this.Meter3ErrorTv};
    }

    @Override // android.support.v4.app.Fragment, com.clou.rs350.ui.fragment.BaseFragment
    public void onResume() {
        refreshData();
        initData();
        super.onResume();
    }

    @Override // android.support.v4.app.Fragment, com.clou.rs350.ui.fragment.BaseFragment
    public void onPause() {
        killThread();
        super.onPause();
    }

    private void initData() {
        if (this.messageManager.getMeter().getStartStateValue() == 0) {
            this.messageManager.setFunc_Mstate(39);
        }
        int refreshTime = Preferences.getInt(Preferences.Key.REFRESHTIME, 1000);
        if (this.readDataThread == null || this.readDataThread.isStop()) {
            this.readDataThread = new TimerThread((long) refreshTime, new ISleepCallback() {
                /* class com.clou.rs350.ui.fragment.errorfragment.DemandTestingFragment.AnonymousClass1 */

                @Override // com.clou.rs350.task.ISleepCallback
                public void onSleepAfterToDo() {
                    if (DemandTestingFragment.this.hasStart) {
                        DemandTestingFragment.this.messageManager.getDemand();
                    }
                    if (1 == DemandTestingFragment.this.m_TestType) {
                        DemandTestingFragment.this.messageManager.getStartState();
                    }
                }
            }, "Get Data Thread");
            this.readDataThread.start();
        }
    }

    private void killThread() {
        if (this.readDataThread != null && !this.readDataThread.isStop()) {
            this.readDataThread.stopTimer();
            this.readDataThread.interrupt();
            this.readDataThread = null;
        }
    }

    @Override // com.clou.rs350.ui.fragment.BaseFragment
    public void refreshData() {
        this.m_TestType = Preferences.getInt(Preferences.Key.DEMANDTESTMODE, 0);
        this.messageManager.setTestMode(this.m_TestType);
        updateALLAccuracy();
        this.chkMeter2.setChecked(this.testMeter[1]);
        this.chkMeter3.setChecked(this.testMeter[2]);
        refreshUnit(this.m_PQFlag);
        int tmpValue = ClouData.getInstance().getTestMeterCount();
        this.TestingTimeTv.setText(new StringBuilder(String.valueOf(this.m_DemandTest[0].TestTime)).toString());
        for (int i = 0; i < 3; i++) {
            this.testMeter[i] = false;
            if ((tmpValue & 1) == 1) {
                this.testMeter[i] = true;
                this.MeterDemandTvs[i].setText(new StringBuilder(String.valueOf(this.m_DemandTest[i].EndPower[this.m_PQFlag].d_Value)).toString());
                this.MeterRefTvs[i].setText(new StringBuilder(String.valueOf(this.m_DemandTest[i].StandardPower[this.m_PQFlag].s_Value)).toString());
                this.m_DemandTest[i].calculateError(this.m_PQFlag);
                refreshError(i);
            }
            tmpValue >>= 1;
        }
    }

    private void refreshUnit(int PQFlag) {
        int tmpUnit = this.m_DemandTest[0].Unit;
        String sUnit = OtherUtils.parseUnitByWiring(PQFlag, "W");
        switch (tmpUnit) {
            case 1:
                sUnit = "k" + sUnit;
                break;
            case 2:
                sUnit = "M" + sUnit;
                break;
        }
        this.Meter1UnitTv.setText(sUnit);
        this.Meter2UnitTv.setText(sUnit);
        this.Meter3UnitTv.setText(sUnit);
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private void refreshError(int index) {
        this.MeterErrorTvs[index].setText(this.m_DemandTest[index].Error[this.m_PQFlag].s_Value);
        this.MeterErrorTvs[index].setTextColor(OtherUtils.getColor((int) this.m_DemandTest[index].Result[this.m_PQFlag].l_Value));
    }

    @OnClick({R.id.btn_configure, R.id.btn_txr_ratio, R.id.btn_start, R.id.testtime_edit, R.id.meter1_demand, R.id.meter2_demand, R.id.meter3_demand})
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_start:
                if (!this.hasStart) {
                    start();
                    return;
                } else {
                    stop();
                    return;
                }
            case R.id.btn_configure:
                new DemandConfigurePopWindow(this.m_Context).showPopWindow(v, MessageManager.getInstance().getMeter().getPQFlagValue(), this.m_DemandTest, new View.OnClickListener() {
                    /* class com.clou.rs350.ui.fragment.errorfragment.DemandTestingFragment.AnonymousClass2 */

                    public void onClick(View v) {
                        DemandTestingFragment.this.refreshData();
                    }
                });
                return;
            case R.id.testtime_edit:
                showInputDialog((TextView) v, new View.OnClickListener() {
                    /* class com.clou.rs350.ui.fragment.errorfragment.DemandTestingFragment.AnonymousClass3 */

                    public void onClick(View view) {
                        DemandTestingFragment.this.m_DemandTest[0].TestTime = OtherUtils.parseLong(((TextView) view).getText().toString().trim());
                    }
                }, 1.0d, 60.0d);
                return;
            case R.id.meter1_demand:
                showInputDialog((TextView) v, (View.OnClickListener) new View.OnClickListener() {
                    /* class com.clou.rs350.ui.fragment.errorfragment.DemandTestingFragment.AnonymousClass4 */

                    public void onClick(View view) {
                        DemandTestingFragment.this.m_DemandTest[0].setPowerValue(DemandTestingFragment.this.m_PQFlag, OtherUtils.parseDouble(((TextView) view).getText().toString().trim()));
                        DemandTestingFragment.this.refreshError(0);
                    }
                }, false);
                return;
            case R.id.meter2_demand:
                showInputDialog((TextView) v, (View.OnClickListener) new View.OnClickListener() {
                    /* class com.clou.rs350.ui.fragment.errorfragment.DemandTestingFragment.AnonymousClass5 */

                    public void onClick(View view) {
                        DemandTestingFragment.this.m_DemandTest[1].setPowerValue(DemandTestingFragment.this.m_PQFlag, OtherUtils.parseDouble(((TextView) view).getText().toString().trim()));
                        DemandTestingFragment.this.refreshError(1);
                    }
                }, false);
                return;
            case R.id.meter3_demand:
                showInputDialog((TextView) v, (View.OnClickListener) new View.OnClickListener() {
                    /* class com.clou.rs350.ui.fragment.errorfragment.DemandTestingFragment.AnonymousClass6 */

                    public void onClick(View view) {
                        DemandTestingFragment.this.m_DemandTest[2].setPowerValue(DemandTestingFragment.this.m_PQFlag, OtherUtils.parseDouble(((TextView) view).getText().toString().trim()));
                        DemandTestingFragment.this.refreshError(2);
                    }
                }, false);
                return;
            case R.id.btn_txr_ratio:
                new TrxRatioPopWindow(this.m_Context).showRatioPopWindow(v, ClouData.getInstance().getSiteData(), null);
                return;
            default:
                return;
        }
    }

    private void setButton(int flag) {
        this.startBtn.setText(getString(flag == 0 ? R.string.text_start : R.string.text_stop));
        boolean enabled = flag == 0;
        this.ratioBtn.setEnabled(enabled);
        this.configureBtn.setEnabled(enabled);
        this.TestingTimeTv.setEnabled(enabled);
        if (this.m_Context instanceof BaseActivity) {
            ((BaseActivity) this.m_Context).startOrStopChangeViewStatus(flag);
        }
    }

    @Override // com.clou.rs350.ui.fragment.errorfragment.BaseErrorFragment
    public boolean start() {
        this.messageManager.setFunc_Mstate_Start(39, 1);
        cleanError();
        this.m_DemandTest[0].TestStartTime = System.currentTimeMillis();
        setButton(1);
        this.hasStart = true;
        return true;
    }

    private void cleanError() {
        for (int i = 0; i < 3; i++) {
            if (this.testMeter[i]) {
                this.m_DemandTest[i].Error[this.m_PQFlag] = new DBDataModel();
                this.m_DemandTest[i].Result[this.m_PQFlag] = new DBDataModel(-1L, PdfObject.NOTHING);
            }
        }
    }

    private void updateAccuracy(int meterIndex) {
        String tmpString = PdfObject.NOTHING;
        switch (this.m_PQFlag) {
            case 0:
                tmpString = ClouData.getInstance().getMeterBaseInfo()[meterIndex].ActiveAccuracy.trim();
                break;
            case 1:
                tmpString = ClouData.getInstance().getMeterBaseInfo()[meterIndex].ReactiveAccuracy.trim();
                break;
            case 2:
                tmpString = ClouData.getInstance().getMeterBaseInfo()[meterIndex].ApparentAccuracy.trim();
                break;
        }
        if (!OtherUtils.isEmpty(tmpString)) {
            this.m_DemandTest[meterIndex].Accuracy[this.m_PQFlag] = OtherUtils.parseDouble(tmpString);
        }
    }

    private void updateALLAccuracy() {
        updateAccuracy(0);
        updateAccuracy(1);
        updateAccuracy(2);
    }

    @Override // com.clou.rs350.ui.fragment.errorfragment.BaseErrorFragment
    public void doContinue() {
        this.hasStart = true;
    }

    @Override // com.clou.rs350.ui.fragment.errorfragment.BaseErrorFragment
    public boolean stop() {
        setButton(0);
        this.messageManager.setStart(0);
        this.messageManager.getDemand();
        this.hasStart = false;
        return true;
    }

    @Override // com.clou.rs350.ui.fragment.BaseFragment, com.clou.rs350.manager.MessageManager.IMessageListener
    public void onReceiveMessage(MeterBaseDevice Device) {
        boolean z = true;
        int tmpPQ_Flag = Device.getPQFlagValue();
        if (this.m_PQFlag != tmpPQ_Flag) {
            this.m_PQFlag = tmpPQ_Flag;
            refreshUnit(this.m_PQFlag);
            updateALLAccuracy();
            showData(this.m_DemandTest);
        }
        for (int i = 0; i < 3; i++) {
            if (this.testMeter[i]) {
                this.m_DemandTest[i].parseData(Device);
            }
        }
        showData(this.m_DemandTest);
        boolean z2 = this.hasStart;
        if (1 != Device.getStartStateValue()) {
            z = false;
        }
        if (!(z ^ z2)) {
            return;
        }
        if (this.hasStart) {
            stop();
        } else {
            start();
        }
    }

    private void showData(DemandTest[] demandMeasurement) {
        if (this.hasStart) {
            String remainTime = demandMeasurement[0].getRemainTime().trim();
            this.RemainingTimeTv.setText(demandMeasurement[0].getRemainTime());
            this.progressBar.setMax(((int) demandMeasurement[0].TestTime) * 60000);
            this.progressBar.setProgress((int) demandMeasurement[0].getRunningTimeMs());
            if (TextUtils.isEmpty(remainTime)) {
                stop();
                this.progressBar.setProgress(this.progressBar.getMax());
                return;
            }
        }
        for (int i = 0; i < 3; i++) {
            if (this.testMeter[i]) {
                this.MeterRefTvs[i].setText(new StringBuilder(String.valueOf(demandMeasurement[i].StandardPower[this.m_PQFlag].s_Value)).toString());
            }
        }
    }

    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        switch (buttonView.getId()) {
            case R.id.chk_meter2:
                if (isChecked) {
                    ClouData.getInstance().setTestMeterCount(ClouData.getInstance().getTestMeterCount() | 2);
                    this.testMeter[1] = true;
                    return;
                }
                ClouData.getInstance().setTestMeterCount(ClouData.getInstance().getTestMeterCount() & -3);
                this.testMeter[1] = false;
                return;
            case R.id.chk_meter3:
                if (isChecked) {
                    ClouData.getInstance().setTestMeterCount(ClouData.getInstance().getTestMeterCount() | 4);
                    this.testMeter[2] = true;
                    return;
                }
                ClouData.getInstance().setTestMeterCount(ClouData.getInstance().getTestMeterCount() & -5);
                this.testMeter[2] = false;
                return;
            default:
                return;
        }
    }

    @Override // com.clou.rs350.ui.fragment.BaseFragment
    public int isFinish() {
        int result = 0;
        for (int i = 0; i < 3; i++) {
            if (this.testMeter[i] && this.m_DemandTest[i].isEmpty()) {
                result = 1;
                new HintDialog(this.m_Context, (int) R.string.text_do_test).show();
            }
        }
        return result;
    }
}
