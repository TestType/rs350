package com.clou.rs350.ui.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.clou.rs350.R;
import com.clou.rs350.db.dao.CTBurdenDao;
import com.clou.rs350.db.dao.CTMeasurementDao;
import com.clou.rs350.db.dao.PTBurdenDao;
import com.clou.rs350.db.dao.PTComparisonDao;
import com.clou.rs350.db.model.BasicMeasurement;
import com.clou.rs350.db.model.CTBurden;
import com.clou.rs350.db.model.CTMeasurement;
import com.clou.rs350.db.model.PTBurden;
import com.clou.rs350.db.model.PTComparison;
import com.clou.rs350.device.MeterBaseDevice;
import com.clou.rs350.handler.HandlerSwitchView;
import com.clou.rs350.model.ClouData;
import com.clou.rs350.task.ISleepCallback;
import com.clou.rs350.task.TimerThread;
import com.clou.rs350.ui.adapter.FragmentTabAdapter;
import com.clou.rs350.ui.dialog.SaveDialog;
import com.clou.rs350.ui.fragment.BaseFragment;
import com.clou.rs350.ui.fragment.transformerfragment.CTBurdenFragment;
import com.clou.rs350.ui.fragment.transformerfragment.CTErrorFragment;
import com.clou.rs350.ui.fragment.transformerfragment.PTBurdenFragment;
import com.clou.rs350.ui.fragment.transformerfragment.PTComparisonFragment;
import com.clou.rs350.ui.view.WiringAndRangeView;
import com.lidroid.xutils.ViewUtils;
import com.lidroid.xutils.view.annotation.ViewInject;

import java.util.ArrayList;
import java.util.List;

public class TransformerTestActivity extends BaseActivity implements View.OnClickListener, HandlerSwitchView.IOnSelectCallback {
    BasicMeasurement m_BasicValue = new BasicMeasurement();
    @ViewInject(R.id.setting_normal_linearlayout)
    private LinearLayout normalLayout;
    private HandlerSwitchView switchView;
    private TimerThread timerThread;
    @ViewInject(R.id.title_textview)
    private TextView titleTextView;
    @ViewInject(R.id.layout_wiring)
    private View wiringLayout;
    private WiringAndRangeView wiringView;

    /* access modifiers changed from: protected */
    @Override // com.clou.rs350.ui.activity.BaseActivity, android.support.v4.app.FragmentActivity
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_instrument_transformer_testing);
        setDefaultTitle(R.string.text_instrument_errorofct);
        ViewUtils.inject(this);
        initView();
        this.m_TabAdapter = new FragmentTabAdapter(this, getFragments(), R.id.transformertestcontent_view);
        this.m_TabAdapter.init();
    }

    private List<BaseFragment> getFragments() {
        List<BaseFragment> list = new ArrayList<>();
        list.add(new CTErrorFragment(ClouData.getInstance().getCTMeasurement()));
        list.add(new CTBurdenFragment(ClouData.getInstance().getCTBurden()));
        list.add(new PTBurdenFragment(ClouData.getInstance().getPTBurden()));
        list.add(new PTComparisonFragment(ClouData.getInstance().getPTComparison()));
        return list;
    }

    /* access modifiers changed from: protected */
    @Override // com.clou.rs350.ui.activity.BaseActivity, android.support.v4.app.FragmentActivity
    public void onResume() {
        super.onResume();
        initData();
    }

    /* access modifiers changed from: protected */
    @Override // com.clou.rs350.ui.activity.BaseActivity, android.support.v4.app.FragmentActivity
    public void onPause() {
        super.onPause();
        this.messageManager.SetRatio(0.0d, 0.0d, ClouData.getInstance().getSiteData().RatioApply);
        killThread(this.timerThread);
    }

    /* access modifiers changed from: protected */
    @Override // com.clou.rs350.ui.activity.BaseActivity, android.support.v4.app.FragmentActivity
    public void onDestroy() {
        this.messageManager.setFunc_Mstate(0);
        super.onDestroy();
    }

    private void initData() {
        this.messageManager.SetRatio(0.0d, 0.0d, 0);
        if (this.timerThread == null || this.timerThread.isStop()) {
            this.timerThread = new TimerThread(3000, new ISleepCallback() {
                /* class com.clou.rs350.ui.activity.TransformerTestActivity.AnonymousClass1 */

                @Override // com.clou.rs350.task.ISleepCallback
                public void onSleepAfterToDo() {
                    TransformerTestActivity.this.messageManager.getRanage();
                }
            }, "Read Range Thread");
            this.timerThread.start();
        }
    }

    private void initView() {
        List<View> tabViews = new ArrayList<>();
        tabViews.add(findViewById(R.id.errorofct_layout));
        tabViews.add(findViewById(R.id.vatestingofct_layout));
        tabViews.add(findViewById(R.id.vatestingofvt_layout));
        tabViews.add(findViewById(R.id.vtcomparison_layout));
        this.switchView = new HandlerSwitchView(this.m_Context);
        this.switchView.setTabViews(tabViews);
        this.switchView.boundClick();
        this.switchView.setOnTabChangedCallback(this);
        this.switchView.selectView(0);
        this.wiringView = new WiringAndRangeView(this, this.wiringLayout);
    }

    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_back:
                finish();
                return;
            case R.id.other_layout:
                this.normalLayout.setVisibility(View.GONE);
                return;
            case R.id.btn_save:
                new SaveDialog(this.m_Context, getString(new int[]{R.string.text_ct_measurement, R.string.text_ct_burden, R.string.text_pt_burden, R.string.text_pt_comparison}[this.m_TabAdapter.getCurrentTab()]), new SaveDialog.ISave() {
                    /* class com.clou.rs350.ui.activity.TransformerTestActivity.AnonymousClass2 */

                    @Override // com.clou.rs350.ui.dialog.SaveDialog.ISave
                    public boolean SaveMeasureData(String SerialNo, String Time) {
                        boolean Result = false;
                        switch (TransformerTestActivity.this.m_TabAdapter.getCurrentTab()) {
                            case 0:
                                CTMeasurement CTErrorData = ClouData.getInstance().getCTMeasurement();
                                CTErrorData.SerialNo = SerialNo;
                                Result = new CTMeasurementDao(TransformerTestActivity.this.m_Context).insertObject(CTErrorData, Time);
                                if (Result) {
                                    CTErrorData.cleanData();
                                    break;
                                }
                                break;
                            case 1:
                                CTBurden CTBurdenData = ClouData.getInstance().getCTBurden();
                                CTBurdenData.SerialNo = SerialNo;
                                Result = new CTBurdenDao(TransformerTestActivity.this.m_Context).insertObject(CTBurdenData, Time);
                                if (Result) {
                                    CTBurdenData.cleanData();
                                    break;
                                }
                                break;
                            case 2:
                                PTBurden PTBurdenData = ClouData.getInstance().getPTBurden();
                                PTBurdenData.SerialNo = SerialNo;
                                Result = new PTBurdenDao(TransformerTestActivity.this.m_Context).insertObject(PTBurdenData, Time);
                                if (Result) {
                                    PTBurdenData.cleanData();
                                    break;
                                }
                                break;
                            case 3:
                                PTComparison PTCumparisonData = ClouData.getInstance().getPTComparison();
                                PTCumparisonData.SerialNo = SerialNo;
                                Result = new PTComparisonDao(TransformerTestActivity.this.m_Context).insertObject(PTCumparisonData, Time);
                                if (Result) {
                                    PTCumparisonData.cleanData();
                                    break;
                                }
                                break;
                        }
                        return Result;
                    }
                }, new SaveDialog.IAfterSave() {
                    /* class com.clou.rs350.ui.activity.TransformerTestActivity.AnonymousClass3 */

                    @Override // com.clou.rs350.ui.dialog.SaveDialog.IAfterSave
                    public void AfterSave(boolean SaveFlag) {
                        TransformerTestActivity.this.m_TabAdapter.getCurrentFragment().refreshData();
                    }
                }).show();
                return;
            case R.id.errorofct_layout:
                this.m_TabAdapter.checkedIndex(0);
                this.titleTextView.setText(R.string.text_instrument_errorofct);
                return;
            case R.id.vatestingofct_layout:
                this.m_TabAdapter.checkedIndex(1);
                this.titleTextView.setText(R.string.text_instrument_vaofct);
                return;
            case R.id.vatestingofvt_layout:
                this.m_TabAdapter.checkedIndex(2);
                this.titleTextView.setText(R.string.text_instrument_vaofpt);
                return;
            case R.id.vtcomparison_layout:
                this.m_TabAdapter.checkedIndex(3);
                this.titleTextView.setText(R.string.text_instrument_ptcomparison);
                return;
            default:
                return;
        }
    }

    @Override // com.clou.rs350.ui.activity.BaseActivity, com.clou.rs350.manager.MessageManager.IMessageListener
    public void onReceiveMessage(MeterBaseDevice Device) {
        this.m_BasicValue.parseRangeData(Device);
        this.wiringView.showData(this.m_BasicValue);
    }

    @Override // com.clou.rs350.handler.HandlerSwitchView.IOnSelectCallback
    public void onSelectedChanged(int index) {
    }

    @Override // com.clou.rs350.handler.HandlerSwitchView.IOnSelectCallback
    public void onClickView(View view) {
        onClick(view);
    }
}
