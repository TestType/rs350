package com.clou.rs350.model;

import android.content.Context;
import com.clou.rs350.CLApplication;
import com.clou.rs350.constants.Constants;
import com.clou.rs350.db.dao.BasicMeasurementDao;
import com.clou.rs350.db.dao.CTBurdenDao;
import com.clou.rs350.db.dao.CTMeasurementDao;
import com.clou.rs350.db.dao.CameraDao;
import com.clou.rs350.db.dao.CustomerInfoDao;
import com.clou.rs350.db.dao.DemandTestDao;
import com.clou.rs350.db.dao.DigitalMeterTestDao;
import com.clou.rs350.db.dao.EnergyTestDao;
import com.clou.rs350.db.dao.ErrorTestDao;
import com.clou.rs350.db.dao.HarmonicMeasurementDao;
import com.clou.rs350.db.dao.LTMDao;
import com.clou.rs350.db.dao.PTBurdenDao;
import com.clou.rs350.db.dao.PTComparisonDao;
import com.clou.rs350.db.dao.RegisterReadingDao;
import com.clou.rs350.db.dao.SealDao;
import com.clou.rs350.db.dao.SignatureDao;
import com.clou.rs350.db.dao.SiteDataManagerDao;
import com.clou.rs350.db.dao.SiteDataTestDao;
import com.clou.rs350.db.dao.TestItemDao;
import com.clou.rs350.db.dao.VisualDao;
import com.clou.rs350.db.dao.WaveMeasurementDao;
import com.clou.rs350.db.dao.WiringCheckDao;
import com.clou.rs350.db.model.BasicMeasurement;
import com.clou.rs350.db.model.CTBurden;
import com.clou.rs350.db.model.CTMeasurement;
import com.clou.rs350.db.model.CustomerInfo;
import com.clou.rs350.db.model.DailyTest;
import com.clou.rs350.db.model.DemandTest;
import com.clou.rs350.db.model.DigitalMeterTest;
import com.clou.rs350.db.model.EnergyTest;
import com.clou.rs350.db.model.ErrorTest;
import com.clou.rs350.db.model.HarmonicMeasurement;
import com.clou.rs350.db.model.LTM;
import com.clou.rs350.db.model.MeterInfo;
import com.clou.rs350.db.model.OperatorInfo;
import com.clou.rs350.db.model.PTBurden;
import com.clou.rs350.db.model.PTComparison;
import com.clou.rs350.db.model.ReadMeter;
import com.clou.rs350.db.model.SiteData;
import com.clou.rs350.db.model.StandardInfo;
import com.clou.rs350.db.model.TestItem;
import com.clou.rs350.db.model.WaveMeasurement;
import com.clou.rs350.db.model.WiringCheckMeasurement;
import com.clou.rs350.db.model.sequence.CameraData;
import com.clou.rs350.db.model.sequence.RegisterReadingData;
import com.clou.rs350.db.model.sequence.SealData;
import com.clou.rs350.db.model.sequence.SignatureData;
import com.clou.rs350.db.model.sequence.VisualData;
import com.clou.rs350.utils.OtherUtils;
import com.clou.rs350.version.VersionInfos;
import com.itextpdf.text.pdf.PdfObject;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.io.StreamCorruptedException;
import java.lang.reflect.Array;
import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ClouData implements Serializable {
    private static ClouData clouData = null;
    private static final long serialVersionUID = 238690782683930647L;
    private List<BasicMeasurement> basicMeasurement;
    private List<CTBurden> cTBurden;
    private List<CTMeasurement> cTMeasurement;
    private List<CameraData> cameraDatas;
    private int communicateType = 0;
    private CustomerInfo customerinfo;
    private List<DailyTest[]> dailyTest;
    private List<DemandTest[]> demandTest;
    private List<DigitalMeterTest> digitalMeterTest;
    private List<EnergyTest[]> energyTest;
    private List<ErrorTest[]> errorTest;
    private List<HarmonicMeasurement> harmonicMeasurement;
    private List<LTM> integrationMeasurement;
    private Context m_Context;
    private boolean m_IsDemo = false;
    private OperatorInfo operatorInfo;
    private List<PTBurden> pTBurden;
    private List<PTComparison> ptComparison;
    private List<ReadMeter> readMeter;
    private List<RegisterReadingData> registerReadingDatas;
    private String registerReadingScheme = PdfObject.NOTHING;
    private List<SealData> sealDatas;
    private String sealScheme = PdfObject.NOTHING;
    private SettingBasic settingBasic;
    private SettingFunction settingFunction;
    private List<SignatureData> signatureData;
    private SiteData siteData;
    private StandardInfo standardInfo;
    private TestItem testItem;
    private int testMeterCount = 1;
    private VersionInfos versionInfo;
    private VisualData visualData;
    private List<WaveMeasurement> waveMeasurement;
    private List<WiringCheckMeasurement> wiringCheck;

    public static ClouData getInstance(Context context) {
        if (clouData == null) {
            clouData = new ClouData(context);
        }
        return clouData;
    }

    public static ClouData getInstance() {
        return clouData;
    }

    private ClouData(Context context) {
        this.m_Context = context;
        initData();
    }

    public void CleanData() {
        this.customerinfo = null;
        this.testItem = null;
        this.basicMeasurement = null;
        this.harmonicMeasurement = null;
        this.waveMeasurement = null;
        this.integrationMeasurement = null;
        this.errorTest = null;
        this.energyTest = null;
        this.demandTest = null;
        this.dailyTest = null;
        this.digitalMeterTest = null;
        this.wiringCheck = null;
        this.cTMeasurement = null;
        this.cTBurden = null;
        this.pTBurden = null;
        this.ptComparison = null;
        this.siteData = null;
        this.visualData = null;
        this.cameraDatas = null;
        this.sealDatas = null;
        this.registerReadingDatas = null;
        this.signatureData = null;
        this.sealScheme = PdfObject.NOTHING;
    }

    private void initData() {
        this.settingBasic = new SettingBasic(this.m_Context);
        this.settingBasic.loadSetting();
        this.settingFunction = new SettingFunction(this.m_Context);
        this.settingFunction.loadSetting();
        this.standardInfo = new StandardInfo(this.m_Context);
        this.standardInfo.loadInfo();
    }

    public void setIsDemo(boolean flag) {
        this.m_IsDemo = flag;
    }

    public boolean isDemo() {
        return this.m_IsDemo;
    }

    public void setTestMeterCount(int count) {
        this.testMeterCount = count;
    }

    public int getTestMeterCount() {
        return this.testMeterCount;
    }

    public void setCommunicateType(int type) {
        this.communicateType = type;
    }

    public int getCommunicateType() {
        return this.communicateType;
    }

    public void setSeriolNo(String serioNo, int meterIndex) {
    }

    public SettingBasic getSettingBasic() {
        return this.settingBasic;
    }

    public SettingFunction getSettingFunction() {
        return this.settingFunction;
    }

    public void setVersionInfo(VersionInfos info) {
        this.versionInfo = info;
    }

    public VersionInfos getVersionInfo() {
        return this.versionInfo;
    }

    public void setStandardInfo(StandardInfo info) {
        this.standardInfo = info;
    }

    public StandardInfo getStandardInfo() {
        return this.standardInfo;
    }

    public OperatorInfo getOperatorInfo() {
        if (this.operatorInfo == null) {
            this.operatorInfo = new OperatorInfo();
        }
        return this.operatorInfo;
    }

    public void setOperatorInfo(OperatorInfo info) {
        this.operatorInfo = info;
    }

    public CustomerInfo getCustomerInfo() {
        if (this.customerinfo == null) {
            this.customerinfo = new CustomerInfo();
        }
        return this.customerinfo;
    }

    public void setCustomerInfo(CustomerInfo info) {
        this.customerinfo = info;
    }

    public TestItem getTestItem() {
        if (this.testItem == null) {
            this.testItem = new TestItem();
        }
        return this.testItem;
    }

    public MeterInfo[] getMeterBaseInfo() {
        if (this.siteData == null) {
            this.siteData = new SiteData();
        }
        return this.siteData.MeterInfo;
    }

    public void setMeterBaseInfo(MeterInfo[] infos) {
        this.siteData.MeterInfo = infos;
    }

    public void setMeterBaseInfo(MeterInfo info, int index) {
        if (this.siteData == null) {
            this.siteData = new SiteData();
        }
        this.siteData.MeterInfo[index] = info;
    }

    public SiteData getSiteData() {
        if (this.siteData == null) {
            this.siteData = new SiteData();
        }
        return this.siteData;
    }

    public void setSiteData(SiteData data) {
        this.siteData = data;
    }

    public VisualData getVisualData() {
        if (this.visualData == null) {
            this.visualData = new VisualData();
        }
        return this.visualData;
    }

    public BasicMeasurement getBasicMeasurement() {
        return getBasicMeasurement(0);
    }

    public BasicMeasurement getBasicMeasurement(int index) {
        if (this.basicMeasurement == null) {
            this.basicMeasurement = new ArrayList();
        }
        while (this.basicMeasurement.size() <= index) {
            this.basicMeasurement.add(new BasicMeasurement());
        }
        return this.basicMeasurement.get(index);
    }

    public WiringCheckMeasurement getWiringCheckMeasurement() {
        return getWiringCheckMeasurement(0);
    }

    public WiringCheckMeasurement getWiringCheckMeasurement(int index) {
        if (this.wiringCheck == null) {
            this.wiringCheck = new ArrayList();
        }
        while (this.wiringCheck.size() <= index) {
            this.wiringCheck.add(new WiringCheckMeasurement());
        }
        return this.wiringCheck.get(index);
    }

    public WaveMeasurement getWaveMeasurement() {
        return getWaveMeasurement(0);
    }

    public WaveMeasurement getWaveMeasurement(int index) {
        if (this.waveMeasurement == null) {
            this.waveMeasurement = new ArrayList();
        }
        while (this.waveMeasurement.size() <= index) {
            this.waveMeasurement.add(new WaveMeasurement());
        }
        return this.waveMeasurement.get(index);
    }

    public HarmonicMeasurement getHarmonicMeasurement() {
        return getHarmonicMeasurement(0);
    }

    public HarmonicMeasurement getHarmonicMeasurement(int index) {
        if (this.harmonicMeasurement == null) {
            this.harmonicMeasurement = new ArrayList();
        }
        while (this.harmonicMeasurement.size() <= index) {
            this.harmonicMeasurement.add(new HarmonicMeasurement());
        }
        return this.harmonicMeasurement.get(index);
    }

    public LTM getLTM() {
        return getLTM(0);
    }

    public LTM getLTM(int index) {
        if (this.integrationMeasurement == null) {
            this.integrationMeasurement = new ArrayList();
        }
        while (this.integrationMeasurement.size() <= index) {
            this.integrationMeasurement.add(new LTM());
        }
        return this.integrationMeasurement.get(index);
    }

    public ErrorTest[] getErrorTest() {
        return getErrorTest(0);
    }

    public ErrorTest[] getErrorTest(int index) {
        if (this.errorTest == null) {
            this.errorTest = new ArrayList();
        }
        while (this.errorTest.size() <= index) {
            this.errorTest.add(new ErrorTest[]{new ErrorTest(0), new ErrorTest(1), new ErrorTest(2)});
        }
        return this.errorTest.get(index);
    }

    public DigitalMeterTest getDigitalTest() {
        return getDigitalTest(0);
    }

    public DigitalMeterTest getDigitalTest(int index) {
        if (this.digitalMeterTest == null) {
            this.digitalMeterTest = new ArrayList();
        }
        while (this.digitalMeterTest.size() <= index) {
            this.digitalMeterTest.add(new DigitalMeterTest());
        }
        return this.digitalMeterTest.get(index);
    }

    public EnergyTest[] getEnergyTest() {
        return getEnergyTest(0);
    }

    public EnergyTest[] getEnergyTest(int index) {
        if (this.energyTest == null) {
            this.energyTest = new ArrayList();
        }
        while (this.energyTest.size() <= index) {
            this.energyTest.add(new EnergyTest[]{new EnergyTest(0), new EnergyTest(1), new EnergyTest(2)});
        }
        return this.energyTest.get(index);
    }

    public DemandTest[] getDemandTest() {
        return getDemandTest(0);
    }

    public DemandTest[] getDemandTest(int index) {
        if (this.demandTest == null) {
            this.demandTest = new ArrayList();
        }
        while (this.demandTest.size() <= index) {
            this.demandTest.add(new DemandTest[]{new DemandTest(0), new DemandTest(1), new DemandTest(2)});
        }
        return this.demandTest.get(index);
    }

    public DailyTest[] getDailyTest() {
        return getDailyTest(0);
    }

    public DailyTest[] getDailyTest(int index) {
        if (this.dailyTest == null) {
            this.dailyTest = new ArrayList();
        }
        while (this.dailyTest.size() <= index) {
            this.dailyTest.add(new DailyTest[]{new DailyTest(0), new DailyTest(1), new DailyTest(2)});
        }
        return this.dailyTest.get(index);
    }

    public CTMeasurement getCTMeasurement() {
        return getCTMeasurement(0);
    }

    public CTMeasurement getCTMeasurement(int index) {
        if (this.cTMeasurement == null) {
            this.cTMeasurement = new ArrayList();
        }
        while (this.cTMeasurement.size() <= index) {
            this.cTMeasurement.add(new CTMeasurement());
        }
        return this.cTMeasurement.get(index);
    }

    public CTBurden getCTBurden() {
        return getCTBurden(0);
    }

    public CTBurden getCTBurden(int index) {
        if (this.cTBurden == null) {
            this.cTBurden = new ArrayList();
        }
        while (this.cTBurden.size() <= index) {
            this.cTBurden.add(new CTBurden());
        }
        return this.cTBurden.get(index);
    }

    public PTBurden getPTBurden() {
        return getPTBurden(0);
    }

    public PTBurden getPTBurden(int index) {
        if (this.pTBurden == null) {
            this.pTBurden = new ArrayList();
        }
        while (this.pTBurden.size() <= index) {
            this.pTBurden.add(new PTBurden());
        }
        return this.pTBurden.get(index);
    }

    public PTComparison getPTComparison() {
        return getPTComparison(0);
    }

    public PTComparison getPTComparison(int index) {
        if (this.ptComparison == null) {
            this.ptComparison = new ArrayList();
        }
        while (this.ptComparison.size() <= index) {
            this.ptComparison.add(new PTComparison());
        }
        return this.ptComparison.get(index);
    }

    public ReadMeter getReadMeter() {
        return getReadmeter(0);
    }

    public ReadMeter getReadmeter(int index) {
        if (this.readMeter == null) {
            this.readMeter = new ArrayList();
        }
        while (this.readMeter.size() <= index) {
            this.readMeter.add(new ReadMeter());
        }
        return this.readMeter.get(index);
    }

    public CameraData getCameraData(int index) {
        if (this.cameraDatas == null) {
            this.cameraDatas = new ArrayList();
        }
        while (this.cameraDatas.size() <= index) {
            this.cameraDatas.add(new CameraData(this.cameraDatas.size()));
        }
        return this.cameraDatas.get(index);
    }

    public SealData getSealData(int index) {
        if (this.sealDatas == null) {
            this.sealDatas = new ArrayList();
        }
        while (this.sealDatas.size() <= index) {
            this.sealDatas.add(new SealData(this.sealDatas.size()));
        }
        return this.sealDatas.get(index);
    }

    public RegisterReadingData getRegisterReadingData(int index) {
        if (this.registerReadingDatas == null) {
            this.registerReadingDatas = new ArrayList();
        }
        while (this.registerReadingDatas.size() <= index) {
            this.registerReadingDatas.add(new RegisterReadingData(this.registerReadingDatas.size()));
        }
        return this.registerReadingDatas.get(index);
    }

    public SignatureData getSignatureData(int index) {
        if (this.signatureData == null) {
            this.signatureData = new ArrayList();
        }
        while (this.signatureData.size() <= index) {
            this.signatureData.add(new SignatureData(this.signatureData.size()));
        }
        return this.signatureData.get(index);
    }

    public int saveAll() {
        if (this.customerinfo == null) {
            return 1;
        }
        if (OtherUtils.isEmpty(this.customerinfo.CustomerSr)) {
            return 1;
        }
        getMeterBaseInfo();
        if (OtherUtils.isEmpty(this.siteData.MeterInfo[0].SerialNo)) {
            this.siteData.MeterInfo[0].CustomerSerialNo = this.customerinfo.CustomerSr;
            this.siteData.MeterInfo[0].SerialNo = this.customerinfo.CustomerSr;
        }
        CustomerInfoDao tmpCustomerDao = new CustomerInfoDao(this.m_Context);
        if (!tmpCustomerDao.queryIfExistByKey(this.customerinfo.CustomerSr)) {
            tmpCustomerDao.insertObject(this.customerinfo);
        } else {
            tmpCustomerDao.updateObjectByKey(this.customerinfo.CustomerSr, this.customerinfo);
        }
        SiteDataManagerDao tmpSiteDataDao = new SiteDataManagerDao(this.m_Context);
        if (this.siteData != null) {
            if (!tmpSiteDataDao.queryIfExistBySerialNo(this.customerinfo.CustomerSr)) {
                tmpSiteDataDao.insertObject(this.siteData);
            } else {
                tmpSiteDataDao.updateObjectByKey(this.customerinfo.CustomerSr, this.siteData);
            }
        }
        String Time = new SimpleDateFormat(Constants.TIMEFORMAT).format(new Date(System.currentTimeMillis())).toString();
        this.testItem = getTestItem();
        this.testItem.TestTime = Time;
        this.testItem.CustomerSerialNo = this.customerinfo.CustomerSr;
        if (this.siteData != null) {
            for (int i = 0; i < this.siteData.Meter_Count; i++) {
                if (this.siteData.MeterInfo[i] != null) {
                    this.testItem.MeterSerialNo[i] = this.siteData.MeterInfo[i].SerialNo;
                }
            }
        }
        this.testItem.parseData();
        new TestItemDao(this.m_Context).insertObject(this.testItem, Time);
        if (this.siteData != null) {
            new SiteDataTestDao(this.m_Context).insertObject(this.siteData, Time);
        }
        if (this.sealDatas != null) {
            for (int i2 = 0; i2 < this.sealDatas.size(); i2++) {
                this.sealDatas.get(i2).SerialNo = this.customerinfo.CustomerSr;
                new SealDao(this.m_Context).insertObject(this.sealDatas.get(i2), Time);
            }
        }
        if (this.registerReadingDatas != null) {
            for (int i3 = 0; i3 < this.registerReadingDatas.size(); i3++) {
                this.registerReadingDatas.get(i3).SerialNo = this.customerinfo.CustomerSr;
                new RegisterReadingDao(this.m_Context).insertObject(this.registerReadingDatas.get(i3), Time);
            }
        }
        if (this.visualData != null) {
            this.visualData.SerialNo = this.customerinfo.CustomerSr;
            new VisualDao(this.m_Context).insertObject(this.visualData, Time);
        }
        if (this.basicMeasurement != null) {
            for (int i4 = 0; i4 < this.basicMeasurement.size(); i4++) {
                this.basicMeasurement.get(i4).CustomerSerialNo = this.customerinfo.CustomerSr;
                new BasicMeasurementDao(this.m_Context).insertObject(this.basicMeasurement.get(i4), Time);
            }
        }
        if (this.waveMeasurement != null) {
            for (int i5 = 0; i5 < this.waveMeasurement.size(); i5++) {
                this.waveMeasurement.get(i5).SerialNo = this.customerinfo.CustomerSr;
                this.waveMeasurement.get(i5).buildStringData();
                new WaveMeasurementDao(this.m_Context).insertObject(this.waveMeasurement.get(i5), Time);
            }
        }
        if (this.wiringCheck != null) {
            for (int i6 = 0; i6 < this.wiringCheck.size(); i6++) {
                this.wiringCheck.get(i6).CustomerSerialNo = this.customerinfo.CustomerSr;
                new WiringCheckDao(this.m_Context).insertObject(this.wiringCheck.get(i6), Time);
            }
        }
        if (this.harmonicMeasurement != null) {
            for (int i7 = 0; i7 < this.harmonicMeasurement.size(); i7++) {
                this.harmonicMeasurement.get(i7).SerialNo = this.customerinfo.CustomerSr;
                this.harmonicMeasurement.get(i7).buildStringData();
                new HarmonicMeasurementDao(this.m_Context).insertObject(this.harmonicMeasurement.get(i7), Time);
            }
        }
        if (this.integrationMeasurement != null) {
            for (int i8 = 0; i8 < this.integrationMeasurement.size(); i8++) {
                this.integrationMeasurement.get(i8).SerialNo = this.customerinfo.CustomerSr;
                new LTMDao(this.m_Context).insertObject(this.integrationMeasurement.get(i8), Time);
            }
        }
        if (this.siteData != null) {
            for (int j = 0; j < this.siteData.Meter_Count; j++) {
                if (this.errorTest != null) {
                    for (int i9 = 0; i9 < this.errorTest.size(); i9++) {
                        this.errorTest.get(i9)[j].CustomerSerialNo = this.customerinfo.CustomerSr;
                        this.errorTest.get(i9)[j].MeterSerialNo = this.siteData.MeterInfo[j].SerialNo;
                        new ErrorTestDao(this.m_Context).insertObject(this.errorTest.get(i9)[j], Time);
                    }
                }
                if (this.demandTest != null) {
                    for (int i10 = 0; i10 < this.demandTest.size(); i10++) {
                        this.demandTest.get(i10)[j].CustomerSerialNo = this.customerinfo.CustomerSr;
                        this.demandTest.get(i10)[j].MeterSerialNo = this.siteData.MeterInfo[j].SerialNo;
                        new DemandTestDao(this.m_Context).insertObject(this.demandTest.get(i10)[j], Time);
                    }
                }
                if (this.energyTest != null) {
                    for (int i11 = 0; i11 < this.energyTest.size(); i11++) {
                        this.energyTest.get(i11)[j].CustomerSerialNo = this.customerinfo.CustomerSr;
                        this.energyTest.get(i11)[j].MeterSerialNo = this.siteData.MeterInfo[j].SerialNo;
                        new EnergyTestDao(this.m_Context).insertObject(this.energyTest.get(i11)[j], Time);
                    }
                }
                if (this.dailyTest != null) {
                    for (int i12 = 0; i12 < this.dailyTest.size(); i12++) {
                        this.dailyTest.get(i12)[j].CustomerSerialNo = this.customerinfo.CustomerSr;
                        this.dailyTest.get(i12)[j].MeterSerialNo = this.siteData.MeterInfo[j].SerialNo;
                        new DemandTestDao(this.m_Context).insertObject(this.dailyTest.get(i12)[j], Time);
                    }
                }
            }
        } else {
            if (this.errorTest != null) {
                for (int i13 = 0; i13 < this.errorTest.size(); i13++) {
                    this.errorTest.get(i13)[0].CustomerSerialNo = this.customerinfo.CustomerSr;
                    this.errorTest.get(i13)[0].MeterSerialNo = this.siteData.MeterInfo[0].SerialNo;
                    new ErrorTestDao(this.m_Context).insertObject(this.errorTest.get(i13)[0], Time);
                }
            }
            if (this.demandTest != null) {
                for (int i14 = 0; i14 < this.demandTest.size(); i14++) {
                    this.demandTest.get(i14)[0].CustomerSerialNo = this.customerinfo.CustomerSr;
                    this.demandTest.get(i14)[0].MeterSerialNo = this.siteData.MeterInfo[0].SerialNo;
                    new DemandTestDao(this.m_Context).insertObject(this.demandTest.get(i14)[0], Time);
                }
            }
            if (this.energyTest != null) {
                for (int i15 = 0; i15 < this.energyTest.size(); i15++) {
                    this.energyTest.get(i15)[0].CustomerSerialNo = this.customerinfo.CustomerSr;
                    this.energyTest.get(i15)[0].MeterSerialNo = this.siteData.MeterInfo[0].SerialNo;
                    new EnergyTestDao(this.m_Context).insertObject(this.energyTest.get(i15)[0], Time);
                }
            }
            if (this.dailyTest != null) {
                for (int i16 = 0; i16 < this.dailyTest.size(); i16++) {
                    this.dailyTest.get(i16)[0].CustomerSerialNo = this.customerinfo.CustomerSr;
                    this.dailyTest.get(i16)[0].MeterSerialNo = this.siteData.MeterInfo[0].SerialNo;
                    new DemandTestDao(this.m_Context).insertObject(this.dailyTest.get(i16)[0], Time);
                }
            }
        }
        if (this.digitalMeterTest != null) {
            for (int i17 = 0; i17 < this.digitalMeterTest.size(); i17++) {
                this.digitalMeterTest.get(i17).CustomerSerialNo = this.customerinfo.CustomerSr;
                new DigitalMeterTestDao(this.m_Context).insertObject(this.digitalMeterTest.get(i17), Time);
            }
        }
        if (this.cTMeasurement != null) {
            for (int i18 = 0; i18 < this.cTMeasurement.size(); i18++) {
                this.cTMeasurement.get(i18).SerialNo = this.customerinfo.CustomerSr;
                new CTMeasurementDao(this.m_Context).insertObject(this.cTMeasurement.get(i18), Time);
            }
        }
        if (this.cTBurden != null) {
            for (int i19 = 0; i19 < this.cTBurden.size(); i19++) {
                this.cTBurden.get(i19).SerialNo = this.customerinfo.CustomerSr;
                new CTBurdenDao(this.m_Context).insertObject(this.cTBurden.get(i19), Time);
            }
        }
        if (this.pTBurden != null) {
            for (int i20 = 0; i20 < this.pTBurden.size(); i20++) {
                this.pTBurden.get(i20).SerialNo = this.customerinfo.CustomerSr;
                new PTBurdenDao(this.m_Context).insertObject(this.pTBurden.get(i20), Time);
            }
        }
        if (this.ptComparison != null) {
            for (int i21 = 0; i21 < this.ptComparison.size(); i21++) {
                this.ptComparison.get(i21).SerialNo = this.customerinfo.CustomerSr;
                new PTComparisonDao(this.m_Context).insertObject(this.ptComparison.get(i21), Time);
            }
        }
        if (this.cameraDatas != null) {
            for (int i22 = 0; i22 < this.cameraDatas.size(); i22++) {
                this.cameraDatas.get(i22).SerialNo = this.customerinfo.CustomerSr;
                new CameraDao(this.m_Context).insertObject(this.cameraDatas.get(i22), Time);
            }
        }
        if (this.signatureData == null) {
            return 0;
        }
        for (int i23 = 0; i23 < this.signatureData.size(); i23++) {
            this.signatureData.get(i23).SerialNo = this.customerinfo.CustomerSr;
            new SignatureDao(this.m_Context).insertObject(this.signatureData.get(i23), Time);
        }
        return 0;
    }

    public String getSealScheme() {
        return this.sealScheme;
    }

    public void setSealScheme(String data) {
        this.sealScheme = data;
    }

    public String getRegisterReadingScheme() {
        return this.registerReadingScheme;
    }

    public void setRegisterReadingScheme(String data) {
        this.registerReadingScheme = data;
    }

    public void saveSerialization() {
        saveSerializationItem(this.customerinfo, "/CustomerInfo.obj");
        saveSerializationItem(this.testItem, "/TestItem.obj");
        if (this.basicMeasurement != null) {
            BasicMeasurement[] tmpBasic = new BasicMeasurement[this.basicMeasurement.size()];
            this.basicMeasurement.toArray(tmpBasic);
            saveSerializationItem(tmpBasic, "/BasicMeasurement.obj");
        }
        if (this.harmonicMeasurement != null) {
            HarmonicMeasurement[] tmpHarmonic = new HarmonicMeasurement[this.harmonicMeasurement.size()];
            this.harmonicMeasurement.toArray(tmpHarmonic);
            saveSerializationItem(tmpHarmonic, "/HarmonicMeasurement.obj");
        }
        if (this.wiringCheck != null) {
            WiringCheckMeasurement[] tmpWiringCheck = new WiringCheckMeasurement[this.wiringCheck.size()];
            this.wiringCheck.toArray(tmpWiringCheck);
            saveSerializationItem(tmpWiringCheck, "/WiringCheck.obj");
        }
        if (this.waveMeasurement != null) {
            WaveMeasurement[] tmpWave = new WaveMeasurement[this.waveMeasurement.size()];
            this.waveMeasurement.toArray(tmpWave);
            saveSerializationItem(tmpWave, "/WaveMeasurement.obj");
        }
        if (this.integrationMeasurement != null) {
            LTM[] tmpLTM = new LTM[this.integrationMeasurement.size()];
            this.integrationMeasurement.toArray(tmpLTM);
            saveSerializationItem(tmpLTM, "/LTM.obj");
        }
        if (this.errorTest != null) {
            ErrorTest[][] tmpError = (ErrorTest[][]) Array.newInstance(ErrorTest.class, this.errorTest.size(), 3);
            this.errorTest.toArray(tmpError);
            saveSerializationItem(tmpError, "/ErrorTest.obj");
        }
        if (this.energyTest != null) {
            EnergyTest[][] tmpEnergy = (EnergyTest[][]) Array.newInstance(EnergyTest.class, this.energyTest.size(), 3);
            this.energyTest.toArray(tmpEnergy);
            saveSerializationItem(tmpEnergy, "/EnergyTest.obj");
        }
        if (this.demandTest != null) {
            DemandTest[][] tmpDemand = (DemandTest[][]) Array.newInstance(DemandTest.class, this.demandTest.size(), 3);
            this.demandTest.toArray(tmpDemand);
            saveSerializationItem(tmpDemand, "/DemandTest.obj");
        }
        if (this.dailyTest != null) {
            DailyTest[][] tmpDaily = (DailyTest[][]) Array.newInstance(DailyTest.class, this.dailyTest.size(), 3);
            this.dailyTest.toArray(tmpDaily);
            saveSerializationItem(tmpDaily, "/DailyTest.obj");
        }
        if (this.digitalMeterTest != null) {
            DigitalMeterTest[] tmpDigitalMeterTest = new DigitalMeterTest[this.digitalMeterTest.size()];
            this.digitalMeterTest.toArray(tmpDigitalMeterTest);
            saveSerializationItem(tmpDigitalMeterTest, "/DigitalMeterTest.obj");
        }
        if (this.cTMeasurement != null) {
            CTMeasurement[] tmpCTTest = new CTMeasurement[this.cTMeasurement.size()];
            this.cTMeasurement.toArray(tmpCTTest);
            saveSerializationItem(tmpCTTest, "/CTMeasurement.obj");
        }
        if (this.cTBurden != null) {
            CTBurden[] tmpCTBurden = new CTBurden[this.cTBurden.size()];
            this.cTBurden.toArray(tmpCTBurden);
            saveSerializationItem(tmpCTBurden, "/CTBurden.obj");
        }
        if (this.pTBurden != null) {
            PTBurden[] tmpPTBurden = new PTBurden[this.pTBurden.size()];
            this.pTBurden.toArray(tmpPTBurden);
            saveSerializationItem(tmpPTBurden, "/PTBurden.obj");
        }
        if (this.ptComparison != null) {
            PTComparison[] tmpPTTest = new PTComparison[this.ptComparison.size()];
            this.ptComparison.toArray(tmpPTTest);
            saveSerializationItem(tmpPTTest, "/PTComparison.obj");
        }
        if (this.readMeter != null) {
            ReadMeter[] tmpReadMeter = new ReadMeter[this.readMeter.size()];
            this.readMeter.toArray(tmpReadMeter);
            saveSerializationItem(tmpReadMeter, "/ReadMeter.obj");
        }
        saveSerializationItem(this.siteData, "/SiteData.obj");
        saveSerializationItem(this.visualData, "/VisualData.obj");
        if (this.cameraDatas != null) {
            CameraData[] tmpCamera = new CameraData[this.cameraDatas.size()];
            this.cameraDatas.toArray(tmpCamera);
            saveSerializationItem(tmpCamera, "/CameraDatas.obj");
        }
        if (this.sealDatas != null) {
            SealData[] tmpSeal = new SealData[this.sealDatas.size()];
            this.sealDatas.toArray(tmpSeal);
            saveSerializationItem(tmpSeal, "/SealDatas.obj");
        }
        if (this.registerReadingDatas != null) {
            RegisterReadingData[] tmpRegisterReading = new RegisterReadingData[this.registerReadingDatas.size()];
            this.registerReadingDatas.toArray(tmpRegisterReading);
            saveSerializationItem(tmpRegisterReading, "/RegisterReadingDatas.obj");
        }
        if (this.signatureData != null) {
            SignatureData[] tmpSignatureData = new SignatureData[this.signatureData.size()];
            this.signatureData.toArray(tmpSignatureData);
            saveSerializationItem(tmpSignatureData, "/SignatureData.obj");
        }
    }

    public void saveSerializationItem(Object obj, String dataName) {
        if (obj != null) {
            try {
                ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(CLApplication.app.getBackupPath() + dataName));
                out.writeObject(obj);
                out.close();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e2) {
                e2.printStackTrace();
            }
        }
    }

    public void cleanDir(String pPath) {
        File dir = new File(pPath);
        if (dir != null && dir.exists() && dir.isDirectory()) {
            File[] listFiles = dir.listFiles();
            for (File file : listFiles) {
                if (file.isFile()) {
                    file.delete();
                }
            }
        }
    }

    public void loadDeserialization() {
        VisualData visualData2 = null;
        Object data = loadDeserializationItem("/CustomerInfo.obj");
        this.customerinfo = data == null ? null : (CustomerInfo) data;
        Object data2 = loadDeserializationItem("/TestItem.obj");
        this.testItem = data2 == null ? null : (TestItem) data2;
        Object data3 = loadDeserializationItem("/BasicMeasurement.obj");
        if (data3 != null) {
            this.basicMeasurement = Arrays.asList((BasicMeasurement[]) data3);
        }
        Object data4 = loadDeserializationItem("/HarmonicMeasurement.obj");
        if (data4 != null) {
            this.harmonicMeasurement = Arrays.asList((HarmonicMeasurement[]) data4);
        }
        Object data5 = loadDeserializationItem("/WaveMeasurement.obj");
        if (data5 != null) {
            this.waveMeasurement = Arrays.asList((WaveMeasurement[]) data5);
        }
        Object data6 = loadDeserializationItem("/LTM.obj");
        if (data6 != null) {
            this.integrationMeasurement = Arrays.asList((LTM[]) data6);
        }
        Object data7 = loadDeserializationItem("/ErrorTest.obj");
        if (data7 != null) {
            this.errorTest = Arrays.asList((ErrorTest[][]) data7);
        }
        Object data8 = loadDeserializationItem("/EnergyTest.obj");
        if (data8 != null) {
            this.energyTest = Arrays.asList((EnergyTest[][]) data8);
        }
        Object data9 = loadDeserializationItem("/DemandTest.obj");
        if (data9 != null) {
            this.demandTest = Arrays.asList((DemandTest[][]) data9);
        }
        Object data10 = loadDeserializationItem("/DailyTest.obj");
        if (data10 != null) {
            this.dailyTest = Arrays.asList((DailyTest[][]) data10);
        }
        Object data11 = loadDeserializationItem("/DigitalMeterTest.obj");
        if (data11 != null) {
            this.digitalMeterTest = Arrays.asList((DigitalMeterTest[]) data11);
        }
        Object data12 = loadDeserializationItem("/WiringCheck.obj");
        if (data12 != null) {
            this.wiringCheck = Arrays.asList((WiringCheckMeasurement[]) data12);
        }
        Object data13 = loadDeserializationItem("/CTMeasurement.obj");
        if (data13 != null) {
            this.cTMeasurement = Arrays.asList((CTMeasurement[]) data13);
        }
        Object data14 = loadDeserializationItem("/CTBurden.obj");
        if (data14 != null) {
            this.cTBurden = Arrays.asList((CTBurden[]) data14);
        }
        Object data15 = loadDeserializationItem("/PTBurden.obj");
        if (data15 != null) {
            this.pTBurden = Arrays.asList((PTBurden[]) data15);
        }
        Object data16 = loadDeserializationItem("/PTComparison.obj");
        if (data16 != null) {
            this.ptComparison = Arrays.asList((PTComparison[]) data16);
        }
        Object data17 = loadDeserializationItem("/ReadMeter.obj");
        if (data17 != null) {
            this.readMeter = Arrays.asList((ReadMeter[]) data17);
        }
        Object data18 = loadDeserializationItem("/SiteData.obj");
        this.siteData = data18 == null ? null : (SiteData) data18;
        Object data19 = loadDeserializationItem("/VisualData.obj");
        if (data19 != null) {
            visualData2 = (VisualData) data19;
        }
        this.visualData = visualData2;
        Object data20 = loadDeserializationItem("/CameraDatas.obj");
        if (data20 != null) {
            this.cameraDatas = Arrays.asList((CameraData[]) data20);
        }
        Object data21 = loadDeserializationItem("/SealDatas.obj");
        if (data21 != null) {
            this.sealDatas = Arrays.asList((SealData[]) data21);
        }
        Object data22 = loadDeserializationItem("/RegisterReadingDatas.obj");
        if (data22 != null) {
            this.registerReadingDatas = Arrays.asList((RegisterReadingData[]) data22);
        }
        Object data23 = loadDeserializationItem("/SignatureData.obj");
        if (data23 != null) {
            this.signatureData = Arrays.asList((SignatureData[]) data23);
        }
        cleanDir(CLApplication.app.getBackupPath());
    }

    public Object loadDeserializationItem(String pPath) {
        Object obj = null;
        try {
            ObjectInputStream in = new ObjectInputStream(new FileInputStream(String.valueOf(CLApplication.app.getBackupPath()) + pPath));
            obj = in.readObject();
            in.close();
            return obj;
        } catch (StreamCorruptedException e) {
            e.printStackTrace();
            return obj;
        } catch (FileNotFoundException e2) {
            e2.printStackTrace();
            return obj;
        } catch (IOException e3) {
            e3.printStackTrace();
            return obj;
        } catch (ClassNotFoundException e4) {
            e4.printStackTrace();
            return obj;
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:23:0x0032  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static <T> boolean saveListObject(List<T> list, File file) {
        boolean result = false;
        Object[] array = list.toArray();
        Throwable th = null;

        if (list != null)
        {
            try
            {
                ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(file));
                out.writeObject(array);
                out.flush();
                result = true;
                if (out != null) {
                    out.close();
                }
            } catch (Throwable th2) {
                th.printStackTrace();
            }
        }
        return result;
    }


    /* JADX WARNING: Removed duplicated region for block: B:20:0x0031  */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x0038  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static <E> List<E> LoadObjectForList(File file) {
        List<E> asList = null;
        try {
            ObjectInputStream out = new ObjectInputStream(new FileInputStream(file));
            Object[] object = (Object[]) out.readObject();
            if (object != null) {
                asList = (List<E>) Arrays.asList(object);
                out.close();
            }else{
                out.close();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return asList;
    }

}
