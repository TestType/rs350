package com.clou.rs350.model;

import android.content.Context;

import com.clou.rs350.Preferences;
import com.clou.rs350.R;
import com.itextpdf.text.pdf.PdfContentParser;
import com.itextpdf.text.pdf.PdfObject;

public class SettingBasic {
    public int AngleDefinition;
    public boolean AutoDiscernConnect;
    public int DeviceModel;
    public int DoubleClickDelay;
    public int ErrorDigit;
    public String ExportTemplate;
    public int HarmonicDefinition;
    public int Language;
    public int[] PhaseColors = new int[6];
    public int ProgressBarTime;
    public int RefreshTime;
    public boolean ResetFlag;
    public int SSType;
    public String UpdateServer;
    public int VectorDefinition;
    public int VectorType;
    private Context m_Context;

    public SettingBasic(Context context) {
        this.m_Context = context;
    }

    public void loadSetting() {
        this.ResetFlag = Preferences.getBoolean(Preferences.Key.RESETFLAG, true);
        this.AngleDefinition = Preferences.getInt("AngleDefinition", 0);
        this.RefreshTime = Preferences.getInt(Preferences.Key.REFRESHTIME, 1000);
        this.DoubleClickDelay = Preferences.getInt(Preferences.Key.DOUBLECLICKDELAY, PdfContentParser.COMMAND_TYPE);
        this.ErrorDigit = Preferences.getInt(Preferences.Key.ERRORDIGIT, 3);
        this.ProgressBarTime = Preferences.getInt(Preferences.Key.PROGRESSBARTIME, 45);
        this.SSType = Preferences.getInt(Preferences.Key.SSTYPE, 0);
        this.DeviceModel = Preferences.getInt(Preferences.Key.DEVICEMODEL, 0);
        this.HarmonicDefinition = Preferences.getInt(Preferences.Key.HARMONICDEFINITION, 0);
        this.VectorDefinition = Preferences.getInt(Preferences.Key.VECTORDEFINITION, 0);
        this.VectorType = Preferences.getInt(Preferences.Key.VECTORTYPE, 0);
        this.AutoDiscernConnect = Preferences.getBoolean(Preferences.Key.AUTOCONNECTTYPE, true);
        this.Language = Preferences.getInt("language", 0);
        this.ExportTemplate = Preferences.getString(Preferences.Key.EXPORTTEMPLATE, PdfObject.NOTHING);
        this.UpdateServer = Preferences.getString(Preferences.Key.UPDATESERVER, "http://58.251.74.100:8806");
        String[] PhaseColorKeys = {Preferences.Key.PHASECOLORU1, Preferences.Key.PHASECOLORU2, Preferences.Key.PHASECOLORU3, Preferences.Key.PHASECOLORI1, Preferences.Key.PHASECOLORI2, Preferences.Key.PHASECOLORI3};
        int[] ColorIds = {R.color.color_l1, R.color.color_l2, R.color.color_l3, R.color.color_l1, R.color.color_l2, R.color.color_l3};
        if (this.ResetFlag) {
            Preferences.getBoolean(Preferences.Key.RESETFLAG, false);
            for (int i = 0; i < this.PhaseColors.length; i++) {
                Preferences.putInt(PhaseColorKeys[i], getColor(ColorIds[i]));
            }
        }
        for (int i2 = 0; i2 < this.PhaseColors.length; i2++) {
            this.PhaseColors[i2] = Preferences.getInt(PhaseColorKeys[i2], getColor(ColorIds[i2]));
        }
    }

    private int getColor(int id) {
        return this.m_Context.getResources().getColor(id);
    }

    public void saveSetting() {
        Preferences.putInt("AngleDefinition", this.AngleDefinition);
        Preferences.putInt(Preferences.Key.VECTORDEFINITION, this.VectorDefinition);
        Preferences.putInt(Preferences.Key.VECTORTYPE, this.VectorType);
        Preferences.putInt(Preferences.Key.REFRESHTIME, this.RefreshTime);
        Preferences.putInt(Preferences.Key.DOUBLECLICKDELAY, this.DoubleClickDelay);
        Preferences.putInt(Preferences.Key.ERRORDIGIT, this.ErrorDigit);
        Preferences.putInt(Preferences.Key.PROGRESSBARTIME, this.ProgressBarTime);
        Preferences.putInt(Preferences.Key.SSTYPE, this.SSType);
        Preferences.putInt(Preferences.Key.DEVICEMODEL, this.DeviceModel);
        Preferences.putInt(Preferences.Key.HARMONICDEFINITION, this.HarmonicDefinition);
        Preferences.putBoolean(Preferences.Key.AUTOCONNECTTYPE, this.AutoDiscernConnect);
        Preferences.putInt("language", this.Language);
        Preferences.putString(Preferences.Key.EXPORTTEMPLATE, this.ExportTemplate);
        Preferences.putString(Preferences.Key.UPDATESERVER, this.UpdateServer);
        Preferences.getBoolean(Preferences.Key.RESETFLAG, this.ResetFlag);
        String[] PhaseColorKeys = {Preferences.Key.PHASECOLORU1, Preferences.Key.PHASECOLORU2, Preferences.Key.PHASECOLORU3, Preferences.Key.PHASECOLORI1, Preferences.Key.PHASECOLORI2, Preferences.Key.PHASECOLORI3};
        for (int i = 0; i < this.PhaseColors.length; i++) {
            Preferences.putInt(PhaseColorKeys[i], this.PhaseColors[i]);
        }
    }

    public String getDeviceModelText(Context context) {
        return context.getResources().getStringArray(R.array.device_model)[this.DeviceModel];
    }

    public String getArrayText(Context context, int id, int index) {
        return context.getResources().getStringArray(id)[index];
    }
}
