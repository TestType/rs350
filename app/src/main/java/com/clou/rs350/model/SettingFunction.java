package com.clou.rs350.model;

import android.content.Context;
import com.clou.rs350.Preferences;

public class SettingFunction {
    public boolean CNVersion;
    public boolean DigitalMeterTesting;
    public boolean TestSequence;
    private Context m_Context;

    public SettingFunction(Context context) {
        this.m_Context = context;
    }

    public void loadSetting() {
        this.CNVersion = Preferences.getBoolean(Preferences.Key.CNVERSION, false);
        this.DigitalMeterTesting = Preferences.getBoolean(Preferences.Key.DIGITALMETERTESTING, false);
        this.TestSequence = Preferences.getBoolean(Preferences.Key.TESTSEQUENCE, false);
    }

    public void saveSetting() {
        Preferences.putBoolean(Preferences.Key.CNVERSION, this.CNVersion);
        Preferences.putBoolean(Preferences.Key.DIGITALMETERTESTING, this.DigitalMeterTesting);
        Preferences.putBoolean(Preferences.Key.TESTSEQUENCE, this.TestSequence);
    }
}
