package com.clou.rs350.callback;

public interface PasswordCallback {
    void onPasswordIsCorrect();

    void onPasswordIsMistake();
}
