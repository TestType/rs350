package com.clou.rs350.callback;

import android.view.View;

public class PopClick implements View.OnClickListener {
    protected int clickIndex;
    protected boolean isSelectAll;
    protected int meterIndex;

    public PopClick(int index) {
        this.meterIndex = index;
    }

    public void setSelectAll(boolean isSelectAll2) {
        this.isSelectAll = isSelectAll2;
    }

    public void setSelectIndex(int index) {
        this.clickIndex = index;
    }

    public void onClick(View v) {
    }
}
