package com.clou.rs350.callback;

public interface ILoadCallback {
    void callback(Object obj);

    Object run();
}
