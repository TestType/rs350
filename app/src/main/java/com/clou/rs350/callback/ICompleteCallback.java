package com.clou.rs350.callback;

public interface ICompleteCallback {
    void onComplete();
}
