package com.clou.rs350.connect.bluetooth.socket;

import android.bluetooth.BluetoothSocket;
import android.util.Log;
import com.clou.rs350.constants.ConstTag;
import com.clou.rs350.utils.OtherUtils;
import java.io.IOException;

public class MsgSender {
    private String TAG = ConstTag.TAG_SOCKET;
    private BluetoothSocket socket;

    public MsgSender(BluetoothSocket bluetoothSocket) {
        this.socket = bluetoothSocket;
    }

    public void sendMsgInThread(final byte[] msg, final String method) {
        new Thread() {
            /* class com.clou.rs350.connect.bluetooth.socket.MsgSender.AnonymousClass1 */

            public void run() {
                try {
                    MsgSender.this.socket.getOutputStream().write(msg);
                    Log.i(MsgSender.this.TAG, "send single msg -- method:" + method + "   " + OtherUtils.bytes2HexString(msg, "-"));
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }.start();
    }

    public boolean sendMsg(byte[] msg, String method) {
        try {
            this.socket.getOutputStream().write(msg);
            Log.i(this.TAG, "send msg -- method:" + method + "  " + OtherUtils.bytes2HexString(msg, "-"));
            return true;
        } catch (IOException e) {
            e.printStackTrace();
            Log.i(this.TAG, "send msg -- method:" + method + " 发送失败-Socket异常");
            return false;
        }
    }

    public void close() {
        if (this.socket != null) {
            try {
                this.socket.close();
            } catch (IOException e) {
            }
        }
    }
}
