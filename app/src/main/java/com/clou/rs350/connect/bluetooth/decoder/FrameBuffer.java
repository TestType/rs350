package com.clou.rs350.connect.bluetooth.decoder;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class FrameBuffer {
    private byte[] buffer;
    private List<Decoder> decoders = new ArrayList();

    public void addDecoderIfAbsent(Decoder decoder) {
        if (!this.decoders.contains(decoder)) {
            this.decoders.add(decoder);
        }
    }

    public synchronized List<DecoderData<byte[]>> addFrame(byte[] data) {
        List<DecoderData<byte[]>> decData;
        byte[] tmp;
        decData = new ArrayList<>();
        if (this.buffer == null) {
            tmp = new byte[data.length];
            System.arraycopy(data, 0, tmp, 0, data.length);
        } else {
            tmp = new byte[(this.buffer.length + data.length)];
            System.arraycopy(this.buffer, 0, tmp, 0, this.buffer.length);
            System.arraycopy(data, 0, tmp, this.buffer.length, data.length);
        }
        for (Decoder dec : this.decoders) {
            List<int[]> drawPos = new ArrayList<>();
            int i = 0;
            while (i < tmp.length) {
                int pos = dec.checkFrame(tmp, i, tmp.length - i);
                if (pos > 0) {
                    decData.add(new DecoderData<>(dec, Arrays.copyOfRange(tmp, i, pos)));
                    drawPos.add(new int[]{i, pos});
                    i = pos;
                }
                i++;
            }
            tmp = getRemainData(drawPos, tmp);
        }
        this.buffer = tmp;
        return decData;
    }

    private byte[] getRemainData(List<int[]> completeFrameIndexs, byte[] tempdata) {
        if (completeFrameIndexs == null || completeFrameIndexs.isEmpty()) {
            return tempdata;
        }
        List<byte[]> datas = new ArrayList<>();
        int start = 0;
        for (int[] indexs : completeFrameIndexs) {
            int end = indexs[0];
            if (start < end) {
                datas.add(Arrays.copyOfRange(tempdata, start, end));
            }
            start = indexs[1];
        }
        if (start < tempdata.length) {
            datas.add(Arrays.copyOfRange(tempdata, start, tempdata.length));
        }
        if (datas.isEmpty()) {
            return null;
        }
        int len = 0;
        for (byte[] data : datas) {
            len += data.length;
        }
        byte[] remainData = new byte[len];
        int pos = 0;
        for (byte[] data2 : datas) {
            System.arraycopy(data2, 0, remainData, pos, data2.length);
            pos += data2.length;
        }
        return remainData;
    }
}
