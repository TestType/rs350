package com.clou.rs350.connect.bluetooth.decoder;

public interface Decoder {
    int checkFrame(byte[] bArr, int i, int i2);
}
