package com.clou.rs350.connect.bluetooth.socket;

import android.os.Handler;
import android.os.Looper;
import java.util.ArrayList;
import java.util.List;

public class ThreadHandler extends Handler {
    public int messageCount;
    List<Runnable> runableList;
    public int sendedCount;

    public ThreadHandler(Looper looper) {
        super(looper);
        this.runableList = null;
        this.messageCount = 0;
        this.sendedCount = 0;
        this.runableList = new ArrayList();
    }

    public boolean postRunnable(Runnable runnable) {
        this.runableList.add(runnable);
        return post(runnable);
    }

    public boolean removeAllRunnable() {
        if (this.runableList.size() <= 5) {
            return true;
        }
        int flag = 5;
        int i = this.runableList.size() - 1;
        while (i > 0) {
            removeCallbacks(this.runableList.get(i));
            int i2 = i + 1;
            flag++;
            if (flag > 5) {
                return true;
            }
            i = i2 - 1;
        }
        return true;
    }

    public void addMessageCount(int size) {
        this.messageCount += size;
    }

    public int getUnsendCount() {
        return this.messageCount;
    }
}
