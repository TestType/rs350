package com.clou.rs350.connect.bluetooth;

import android.bluetooth.BluetoothSocket;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import com.clou.rs350.connect.BaseConnect;
import com.clou.rs350.connect.bluetooth.manager.BluetoothConnection;
import com.clou.rs350.connect.bluetooth.manager.BluetoothSearch;
import com.clou.rs350.connect.bluetooth.socket.MsgClient;
import com.clou.rs350.connect.callback.ISendMsgCallback;
import com.clou.rs350.connect.callback.ISocketReceiveListener;
import com.clou.rs350.constants.ConstTag;
import com.clou.rs350.model.ClouData;
import com.clou.rs350.task.ISleepCallback;
import com.clou.rs350.task.SleepTask;
import com.clou.rs350.utils.BroadCastUtil;
import java.util.List;

public class BluetoothConnect extends BaseConnect implements BluetoothConnection.IBluetoothConnectCallback, ISocketReceiveListener {
    private String TAG = ConstTag.TAG_SOCKET;
    private MsgClient msgClient;

    public BluetoothConnect(Context context) {
        super(context);
        BluetoothSearch.getInstance(context).setConnectCallback(this);
    }

    @Override // com.clou.rs350.connect.BaseConnect
    public void notifyToSendNextMsg() {
        if (this.msgClient != null) {
            this.msgClient.notifyToSendNextMsg();
        }
    }

    @Override // com.clou.rs350.connect.BaseConnect
    public void removeAllMessage() {
        if (this.msgClient != null) {
            this.msgClient.removeAllMessage();
        }
    }

    @Override // com.clou.rs350.connect.BaseConnect
    public void sendMsg(List<byte[]> byteList, String method) {
        sendMsg(byteList, false, method);
    }

    @Override // com.clou.rs350.connect.BaseConnect
    public void sendMsg(List<byte[]> byteList, boolean sendFront, String method) {
        sendMsg(byteList, sendFront, true, method);
    }

    @Override // com.clou.rs350.connect.BaseConnect
    public void sendMsg(List<byte[]> byteList, boolean sendFront, boolean isReadDataRequest, String method) {
        if (this.msgClient == null) {
            return;
        }
        if (!isConnected()) {
            Log.i(this.TAG, "is disconnect, can not to : " + method);
        } else if (!isBusy()) {
            this.msgClient.sendMsg(byteList, sendFront, method, isReadDataRequest, new ISendMsgCallback() {
                /* class com.clou.rs350.connect.bluetooth.BluetoothConnect.AnonymousClass1 */

                @Override // com.clou.rs350.connect.callback.ISendMsgCallback
                public void onSendFailed() {
                    BluetoothConnect.this.onSocketDisConnect();
                }
            });
        } else {
            Log.i(this.TAG, "is busy, can not to : " + method);
        }
    }

    @Override // com.clou.rs350.connect.BaseConnect
    public void sendMsgInThread(byte[] bytes, String method) {
        if (this.msgClient == null) {
            return;
        }
        if (isConnected()) {
            this.msgClient.sendMsgInThread(bytes, method);
        } else {
            Log.i(this.TAG, "is disconnect, can not to : " + method);
        }
    }

    @Override // com.clou.rs350.connect.bluetooth.manager.BluetoothConnection.IBluetoothConnectCallback
    public void onBluetoothConnecting(String deviceName) {
        Log.i(this.TAG, "socket connecting " + deviceName);
        onSocketConnecting();
    }

    @Override // com.clou.rs350.connect.bluetooth.manager.BluetoothConnection.IBluetoothConnectCallback
    public void onBluetoothSocketConnected(String deviceName, BluetoothSocket socket) {
        Log.i(this.TAG, "socket connected " + deviceName);
        BroadCastUtil.send(BroadCastUtil.Action.ACTION_SOCKET_CONNECTED);
        if (this.msgClient != null) {
            this.msgClient.release();
            this.msgClient = null;
        }
        this.msgClient = new MsgClient(socket);
        this.msgClient.setOnReceiveMessageListener(this);
        if (ClouData.getInstance().getSettingBasic().DeviceModel == 0) {
            onSocketConnected();
            return;
        }
        new SleepTask(10000, new ISleepCallback() {
            /* class com.clou.rs350.connect.bluetooth.BluetoothConnect.AnonymousClass2 */

            @Override // com.clou.rs350.task.ISleepCallback
            public void onSleepAfterToDo() {
                if (!BluetoothConnect.this.isConnected()) {
                    BluetoothSearch.getInstance(BluetoothConnect.this.mContext).reSearchDeviceList("10秒后设备未通讯-重新搜索蓝牙");
                }
            }
        }).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, 0);
    }

    @Override // com.clou.rs350.connect.bluetooth.manager.BluetoothConnection.IBluetoothConnectCallback
    public void onBluetoothDisconnect(String deviceName, String tagMessage) {
        if (this.msgClient != null) {
            this.msgClient.release();
        }
        this.connectStatus = 0;
        Log.i(this.TAG, "蓝牙断开连接----tag:" + tagMessage);
        BroadCastUtil.send(BroadCastUtil.Action.ACTION_DISCONNECT);
    }

    @Override // com.clou.rs350.connect.BaseConnect
    public void onSocketConnecting() {
        super.onSocketConnecting();
    }

    @Override // com.clou.rs350.connect.BaseConnect
    public void onSocketConnected() {
        super.onSocketConnected();
    }

    @Override // com.clou.rs350.connect.BaseConnect
    public void onSocketDisConnect() {
        super.onSocketDisConnect();
        BluetoothSearch.getInstance(this.mContext).reSearchDeviceList("Message sending failed-active disconnection-search for Bluetooth again");
    }

    @Override // com.clou.rs350.connect.callback.ISocketReceiveListener
    public void onReceiveMessage(byte[] data) {
        if (this.messageReceivelistener != null) {
            this.messageReceivelistener.onReceiveMessage(data);
        }
    }

    @Override // com.clou.rs350.connect.BaseConnect
    public void sendAndReceive(byte[] sendBuffer, int timeout, String method) {
        if (this.msgClient == null) {
            return;
        }
        if (isConnected()) {
            this.msgClient.sendMsgInThread(sendBuffer, method);
            try {
                Thread.sleep(10);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        } else {
            Log.i(this.TAG, "is disconnect, can not to : " + method);
        }
    }
}
