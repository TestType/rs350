package com.clou.rs350.connect.bluetooth.socket;

import android.bluetooth.BluetoothSocket;
import android.util.Log;
import com.clou.rs350.connect.bluetooth.decoder.FrameBuffer;
import com.clou.rs350.connect.callback.ISocketReceiveListener;
import com.clou.rs350.constants.ConstTag;
import com.clou.rs350.utils.OtherUtils;
import com.itextpdf.text.pdf.PdfObject;
import java.io.IOException;
import java.io.InputStream;

public class MsgReceiver {
    private String TAG = ConstTag.TAG_SOCKET;
    private FrameBuffer frmBuffer = new FrameBuffer();
    private ISocketReceiveListener receiveListener;
    private ConnectThread receiveThread = null;
    private BluetoothSocket socket;

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private boolean isATCommand(String command) {
        if (command != null) {
            return command.equals("41-54-2b-4c-43-4e-54-3f-0d-0a");
        }
        return false;
    }

    public MsgReceiver(BluetoothSocket bluetoothSocket) {
        this.socket = bluetoothSocket;
    }

    public byte[] recvData(long timeout) {
        byte[] data = null;
        try {
            long begTime = System.currentTimeMillis();
            while (System.currentTimeMillis() - begTime < timeout) {
                data = recvData();
                if (data != null) {
                    return data;
                }
            }
            return data;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    private byte[] recvData() {
        try {
            InputStream inuptStream = this.socket.getInputStream();
            int avial = inuptStream.available();
            if (avial <= 0) {
                return null;
            }
            byte[] data = new byte[avial];
            inuptStream.read(data);
            return data;
        } catch (Exception e) {
            return null;
        }
    }

    public void startListenMsg() {
        this.receiveThread = new ConnectThread(PdfObject.NOTHING);
        this.receiveThread.start();
    }

    class ConnectThread extends Thread {
        private byte[] buffer = new byte[65536];
        private int count = 0;
        private int index = 0;
        private boolean isStop = false;

        public ConnectThread(String name) {
            Log.i(MsgReceiver.this.TAG, "ConnectThread: " + name + " is startd");
        }

        public boolean isStop() {
            return this.isStop;
        }

        public void cancel() {
            this.isStop = true;
        }

        public void run() {
            while (!this.isStop) {
                byte[] data = MsgReceiver.this.recvData(25);
                if (data != null && data.length > 0) {
                    System.arraycopy(data, 0, this.buffer, this.index, data.length);
                    this.index += data.length;
                    this.count = 0;
                } else if (this.index != 0) {
                    if (this.count < 2) {
                        this.count++;
                    } else {
                        byte[] result = new byte[this.index];
                        System.arraycopy(this.buffer, 0, result, 0, this.index);
                        String sBuffer = OtherUtils.bytes2HexString(result, "-");
                        if (MsgReceiver.this.receiveListener != null && !MsgReceiver.this.isATCommand(sBuffer)) {
                            Log.i(MsgReceiver.this.TAG, "received message--: " + sBuffer);
                            MsgReceiver.this.receiveListener.onReceiveMessage(result);
                        }
                        this.index = 0;
                    }
                }
            }
        }
    }

    public void setOnReceiveMessageListener(ISocketReceiveListener receiveListener2) {
        this.receiveListener = receiveListener2;
    }

    public void close() {
        if (this.receiveThread != null && !this.receiveThread.isStop()) {
            this.receiveThread.cancel();
            this.receiveThread = null;
        }
        if (this.socket != null) {
            try {
                this.socket.close();
            } catch (IOException e) {
            }
        }
    }
}
