package com.clou.rs350.connect.bluetooth.decoder;

public class DecoderData<T> {
    private T data;
    private Decoder decoder;

    public DecoderData() {
    }

    public DecoderData(Decoder decoder2, T data2) {
        this.decoder = decoder2;
        this.data = data2;
    }

    public Decoder getDecoder() {
        return this.decoder;
    }

    public void setDecoder(Decoder decoder2) {
        this.decoder = decoder2;
    }

    public T getData() {
        return this.data;
    }

    public void setData(T data2) {
        this.data = data2;
    }
}
