package com.clou.rs350.connect.callback;

public interface ISendMsgCallback {
    void onSendFailed();
}
