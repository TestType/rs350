package com.clou.rs350.connect;

import android.content.Context;
import com.clou.rs350.Preferences;
import com.clou.rs350.connect.callback.ISocketCallback;
import com.clou.rs350.connect.callback.ISocketReceiveListener;
import com.clou.rs350.utils.BroadCastUtil;
import java.util.List;

public abstract class BaseConnect {
    public static final int STATUS_CONNECTED = 2;
    public static final int STATUS_CONNECTING = 1;
    public static final int STATUS_DISCONNECT = 0;
    protected int connectStatus;
    private boolean isBusy;
    protected Context mContext;
    protected ISocketReceiveListener messageReceivelistener;
    private ISocketCallback socketCallback;

    public abstract void notifyToSendNextMsg();

    public abstract void removeAllMessage();

    public abstract void sendAndReceive(byte[] bArr, int i, String str);

    public abstract void sendMsg(List<byte[]> list, String str);

    public abstract void sendMsg(List<byte[]> list, boolean z, String str);

    public abstract void sendMsg(List<byte[]> list, boolean z, boolean z2, String str);

    public abstract void sendMsgInThread(byte[] bArr, String str);

    protected BaseConnect(Context context) {
        this.mContext = context;
    }

    public void setOnSocketCallback(ISocketCallback socketCallback2) {
        this.socketCallback = socketCallback2;
    }

    /* access modifiers changed from: protected */
    public void onSocketConnecting() {
        if (this.socketCallback != null) {
            this.socketCallback.onSocketConnecting();
        }
        this.connectStatus = 1;
        BroadCastUtil.send(BroadCastUtil.Action.ACTION_CONNECTING);
    }

    public void onSocketConnected() {
        if (this.socketCallback != null) {
            this.socketCallback.onSocketConnected();
        }
        this.connectStatus = 2;
        BroadCastUtil.send(BroadCastUtil.Action.ACTION_SERVER_CONNECTED);
    }

    /* access modifiers changed from: protected */
    public void onSocketDisConnect() {
        if (this.socketCallback != null) {
            this.socketCallback.onSocketDisConnected();
        }
        Preferences.putBoolean(Preferences.Key.CONNECTEDBEFORE, true);
        this.connectStatus = 0;
        BroadCastUtil.send(BroadCastUtil.Action.ACTION_DISCONNECT);
    }

    public void setOnReceiveMessageListener(ISocketReceiveListener listener) {
        this.messageReceivelistener = listener;
    }

    /* access modifiers changed from: protected */
    public ISocketReceiveListener getReceiveMessageListener() {
        return this.messageReceivelistener;
    }

    public boolean isConnected() {
        return 2 == this.connectStatus;
    }

    public boolean isBusy() {
        return this.isBusy;
    }

    public void setBusy(boolean isBusy2) {
        this.isBusy = isBusy2;
    }
}
