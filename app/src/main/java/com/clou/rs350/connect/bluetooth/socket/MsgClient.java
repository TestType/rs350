package com.clou.rs350.connect.bluetooth.socket;

import android.bluetooth.BluetoothSocket;
import android.os.HandlerThread;
import android.util.Log;
import com.clou.rs350.Preferences;
import com.clou.rs350.connect.callback.ISendMsgCallback;
import com.clou.rs350.connect.callback.ISocketReceiveListener;
import com.clou.rs350.constants.Constants;
import java.util.List;

public class MsgClient {
    private static final String TAG = "RS350_socket";
    private static final int TotalResendCount = 10;
    private BluetoothSocket bluetoothSocket;
    private HandlerThread handlerThread = null;
    private boolean hasReceivedMsg = false;
    private ThreadHandler messageHandler = null;
    private MsgReceiver receiver;
    private Object sendMsgLock = new Object();
    private MsgSender sender;

    public MsgClient(BluetoothSocket bluetoothSocket2) {
        this.bluetoothSocket = bluetoothSocket2;
        this.sender = new MsgSender(bluetoothSocket2);
        this.receiver = new MsgReceiver(bluetoothSocket2);
        this.receiver.startListenMsg();
        this.handlerThread = new HandlerThread("handler thread");
        this.handlerThread.start();
        this.messageHandler = new ThreadHandler(this.handlerThread.getLooper());
    }

    public void removeAllMessage() {
        this.messageHandler.removeAllRunnable();
    }

    public void sendMsgInThread(byte[] msg, String method) {
        if (msg != null && msg.length != 0) {
            if (this.sender != null) {
                this.sender.sendMsgInThread(msg, method);
            } else {
                Log.i("RS350_socket", "sender is null in sendMsgInThread");
            }
        }
    }

    public synchronized void sendMsg(List<byte[]> msgList, String method) {
        sendMsg(msgList, method, null);
    }

    public synchronized void sendMsg(List<byte[]> msgList, String method, ISendMsgCallback sendCallback) {
        sendMsg(msgList, false, method, sendCallback);
    }

    public synchronized void sendMsg(List<byte[]> msgList, boolean toFront, String method, ISendMsgCallback sendCallback) {
        sendMsg(msgList, toFront, method, true, sendCallback);
    }

    public synchronized void sendMsg(final List<byte[]> msgList, boolean toFront, final String method, final boolean isReadDataRequest, final ISendMsgCallback sendCallback) {
        if (msgList != null) {
            Runnable runnable = new Runnable() {
                /* class com.clou.rs350.connect.bluetooth.socket.MsgClient.AnonymousClass1 */

                /* JADX WARNING: Code restructure failed: missing block: B:21:0x0045, code lost:
                    if (r4 != 10) goto L_0x00f4;
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:23:0x004d, code lost:
                    if (r12.this$0.hasReceivedMsg != false) goto L_0x00f4;
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:24:0x004f, code lost:
                    android.util.Log.i("RS350_socket", "连续10条报文发送失败----掉线");
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:26:0x0058, code lost:
                    if (r12 == null) goto L_0x0013;
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:27:0x005a, code lost:
                    r12.onSendFailed();
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:43:0x00f4, code lost:
                    if (r5 != false) goto L_0x0056;
                 */
                /* JADX WARNING: Code restructure failed: missing block: B:44:0x00f6, code lost:
                    android.util.Log.i("RS350_socket", "报文发送失败--Socket断开--掉线");
                 */
                /* Code decompiled incorrectly, please refer to instructions dump. */
                public void run() {
                    synchronized (MsgClient.this.sendMsgLock) {
                        int i = 0;
                        while (true) {
                            if (i >= msgList.size()) {
                                break;
                            }
                            byte[] msg = (byte[]) msgList.get(i);
                            if (!(msg == null || msg.length == 0)) {
                                int reSendCount = 0;
                                boolean sendSuccess = false;
                                while (reSendCount < 10) {
                                    if (MsgClient.this.sender != null) {
                                        Log.i("RS350_socket", "send current:" + (i + 1) + "  total:" + msgList.size());
                                        sendSuccess = MsgClient.this.sender.sendMsg(msg, method);
                                        Log.i("RS350_socket", "send ReSendCount:" + (reSendCount + 1) + "  TotalResendCount:" + 10);
                                        if (!sendSuccess) {
                                            break;
                                        }
                                        MsgClient.this.hasReceivedMsg = false;
                                        reSendCount++;
                                        try {
                                            MsgClient.this.sendMsgLock.wait((long) Preferences.getInt(Preferences.Key.BASIC_SOCKET_TIMEOUT, Constants.DEFAULT_SOCKET_TIMEOUT));
                                            if (MsgClient.this.hasReceivedMsg) {
                                                break;
                                            }
                                        } catch (InterruptedException e) {
                                            e.printStackTrace();
                                        }
                                    } else {
                                        Log.i("RS350_socket", "sender is null in sendMsg");
                                        return;
                                    }
                                }
                                if ((reSendCount != 10 || MsgClient.this.hasReceivedMsg) && sendSuccess) {
                                }
                            }
                            i++;
                        }
                        if (isReadDataRequest) {
                            MsgClient.this.messageHandler.addMessageCount(-1);
                        }
                    }
                }
            };
            if (toFront) {
                this.messageHandler.postAtFrontOfQueue(runnable);
            } else {
                if (isReadDataRequest) {
                    if (this.messageHandler.getUnsendCount() > 2) {
                        Log.i("RS350_socket", "队列等待消息有3条以上，当前 ：" + method + "被丢弃");
                    } else {
                        this.messageHandler.addMessageCount(1);
                    }
                }
                this.messageHandler.postRunnable(runnable);
            }
        }
    }

    public void setOnReceiveMessageListener(ISocketReceiveListener listener) {
        this.receiver.setOnReceiveMessageListener(listener);
    }

    public void release() {
        if (this.sender != null) {
            this.sender.close();
            this.sender = null;
        }
        if (this.receiver != null) {
            this.receiver.close();
            this.receiver = null;
        }
    }

    public void notifyToSendNextMsg() {
        this.hasReceivedMsg = true;
        synchronized (this.sendMsgLock) {
            this.sendMsgLock.notifyAll();
        }
    }
}
