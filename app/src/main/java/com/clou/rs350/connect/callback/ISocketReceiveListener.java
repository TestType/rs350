package com.clou.rs350.connect.callback;

public interface ISocketReceiveListener {
    void onReceiveMessage(byte[] bArr);
}
