package com.clou.rs350.connect.callback;

public interface ISocketCallback {
    void onSocketConnected();

    void onSocketConnecting();

    void onSocketDisConnected();
}
