package com.clou.rs350.connect.bluetooth.manager;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.AsyncTask;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Toast;
import com.clou.rs350.connect.bluetooth.manager.BluetoothConnection;
import com.clou.rs350.task.ISleepCallback;
import com.clou.rs350.task.SleepTask;
import com.clou.rs350.utils.BroadCastUtil;
import com.itextpdf.text.pdf.PdfObject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class BluetoothSearch {
    public static final String TAG = "RS350_bluetooth";
    private static BluetoothSearch instance;
    private boolean fristSearch = true;
    private boolean isSearching;
    private BluetoothAdapter mBluetoothAdapter;
    private BroadcastReceiver mBluetoothReceiver = new BroadcastReceiver() {
        /* class com.clou.rs350.connect.bluetooth.manager.BluetoothSearch.AnonymousClass1 */
        long sleepTime = 10000;

        public void onReceive(Context context, Intent intent) {
            boolean isNoDevice;
            boolean isNotContainsDevice;
            String action = intent.getAction();
            Log.i("BluetoothBroadcastReceiver", "BluetoothBroadcastReceiver:" + action);
            if ("android.bluetooth.adapter.action.DISCOVERY_FINISHED".equals(action)) {
                BluetoothSearch.this.cancleDiscovery();
                if (TextUtils.isEmpty(BluetoothSearch.this.mMac)) {
                    BroadCastUtil.send(BroadCastUtil.Action.ACTION_SEARCH_COMPLETE);
                    return;
                }
                if (BluetoothSearch.this.mDeviceList == null || BluetoothSearch.this.mDeviceList.size() == 0) {
                    isNoDevice = true;
                } else {
                    isNoDevice = false;
                }
                if (TextUtils.isEmpty(BluetoothSearch.this.mMac) || BluetoothSearch.this.mDeviceMap.containsKey(BluetoothSearch.this.mMac)) {
                    isNotContainsDevice = false;
                } else {
                    isNotContainsDevice = true;
                }
                if (isNoDevice || isNotContainsDevice) {
                    BroadCastUtil.send(BroadCastUtil.Action.ACTION_NO_DEVICE);
                    BluetoothSearch.this.sleepAfterToResearch(this.sleepTime);
                    this.sleepTime *= 2;
                    if (this.sleepTime > 60000) {
                        this.sleepTime = 60000;
                        return;
                    }
                    return;
                }
                this.sleepTime = 10000;
            } else if ("android.bluetooth.device.action.FOUND".equals(action)) {
                BluetoothDevice device = (BluetoothDevice) intent.getParcelableExtra("android.bluetooth.device.extra.DEVICE");
                if (device != null) {
                    Log.i("RS350_bluetooth", "found device name:" + device.getName() + " address:" + device.getAddress());
                    BluetoothSearch.this.doFoundDevice(device);
                    BluetoothSearch.this.tryToConnectDevice(device);
                }
            } else if ("android.bluetooth.device.action.NAME_CHANGED".equals(action)) {
                Log.i("RS350_bluetooth", "found device name.");
                BluetoothSearch.this.notifyToRefreshDeviceList();
            }
        }
    };
    private boolean mBluetoothState;
    private List<IBlutoothListener> mBlutoothListener;
    private BluetoothConnection mConnection;
    private Context mContext;
    private List<BluetoothDevice> mDeviceList;
    private Map<String, BluetoothDevice> mDeviceMap;
    private String mMac = PdfObject.NOTHING;
    SleepTask reSearchThread = null;

    public interface IBlutoothListener {
        void onRefreshDeviceList(List<BluetoothDevice> list);
    }

    public static BluetoothSearch getInstance(Context context) {
        if (instance == null) {
            instance = new BluetoothSearch(context);
        }
        return instance;
    }

    public static BluetoothSearch getInstance() {
        return instance;
    }

    private BluetoothSearch(Context context) {
        this.mContext = context;
        this.mConnection = new BluetoothConnection(this.mContext);
        this.mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        this.mDeviceMap = new HashMap();
        this.mDeviceList = new ArrayList();
        this.mBlutoothListener = new ArrayList();
        if (this.fristSearch) {
            this.mBluetoothState = this.mBluetoothAdapter.isEnabled();
            this.fristSearch = false;
        }
        if (this.mBluetoothAdapter != null && !this.mBluetoothAdapter.isEnabled() && !this.mBluetoothAdapter.enable()) {
            BroadCastUtil.send(BroadCastUtil.Action.ACTION_BLUETOOTH_UNAVAILABLE);
        }
    }

    public List<BluetoothDevice> getDeviceList() {
        List<BluetoothDevice> deviceList = new ArrayList<>();
        deviceList.addAll(this.mDeviceMap.values());
        return deviceList;
    }

    public BluetoothConnection getBluetoothConnection() {
        return this.mConnection;
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private void tryToConnectDevice(BluetoothDevice device) {
        if (!TextUtils.isEmpty(this.mMac) && device.getAddress().equals(this.mMac) && this.mConnection.getConnect_status() == 0) {
            Log.i("RS350_bluetooth", " connecting <" + device.getAddress() + "> in service ");
            connectDevice(device);
        }
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private void doFoundDevice(BluetoothDevice device) {
        if (!this.mDeviceMap.containsKey(device.getAddress())) {
            this.mDeviceMap.put(device.getAddress(), device);
            if (this.mDeviceList.contains(device)) {
                this.mDeviceList.remove(device);
            }
            this.mDeviceList.add(device);
        }
        notifyToRefreshDeviceList();
    }

    private void onStartSearch() {
        BroadCastUtil.send(BroadCastUtil.Action.ACTION_SEARCHING);
    }

    private void onSearchFinished() {
        if (this.mDeviceList == null || this.mDeviceList.size() == 0) {
            BroadCastUtil.send(BroadCastUtil.Action.ACTION_NO_DEVICE);
        } else {
            BroadCastUtil.send(BroadCastUtil.Action.ACTION_SEARCH_COMPLETE);
        }
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private void notifyToRefreshDeviceList() {
        if (this.mBlutoothListener != null) {
            for (IBlutoothListener listener : this.mBlutoothListener) {
                listener.onRefreshDeviceList(this.mDeviceList);
            }
        }
    }

    public void setMac(String mac) {
        this.mMac = mac;
    }

    public void searchDeviceList() {
        Log.i("RS350_bluetooth", "searching bluetooth list in service");
        startDiscovery("searchDeviceList");
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private void sleepAfterToResearch(long sleepMillions) {
        Log.i("RS350_bluetooth", "sleep time :" + sleepMillions + " after research.");
        if (this.reSearchThread == null) {
            this.reSearchThread = new SleepTask(sleepMillions, new ISleepCallback() {
                /* class com.clou.rs350.connect.bluetooth.manager.BluetoothSearch.AnonymousClass2 */

                @Override // com.clou.rs350.task.ISleepCallback
                public void onSleepAfterToDo() {
                    BluetoothSearch.this.reSearchDeviceList("没有搜到蓝牙或者没有搜到指定的蓝牙-重新搜索");
                    BluetoothSearch.this.reSearchThread = null;
                }
            });
            this.reSearchThread.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, 0);
        }
    }

    public void reSearchDeviceList(String tagMessage) {
        this.mConnection.disConnectSoket(tagMessage);
        startDiscovery("reSearchDeviceList");
    }

    private void startDiscovery(String from) {
        onStartSearch();
        openBluetooth();
        this.isSearching = true;
        if (!this.mBluetoothAdapter.isDiscovering()) {
            this.mDeviceMap.clear();
            this.mDeviceList.clear();
            notifyToRefreshDeviceList();
            if (!this.mBluetoothAdapter.startDiscovery()) {
                reSearchDeviceList("start discovery again");
            }
        } else {
            Set<BluetoothDevice> devices = this.mBluetoothAdapter.getBondedDevices();
            for (int i = 0; i < devices.size(); i++) {
                doFoundDevice(devices.iterator().next());
            }
        }
        Log.i("RS350_bluetooth", "searching bluetooth list from " + from);
    }

    public void disConnectSoket(String tagMessage) {
        this.mConnection.disConnectSoket(tagMessage);
    }

    public void closeSocket(String tagMessage) {
        this.mConnection.disConnectSoket(tagMessage);
        if (!this.mBluetoothState) {
            Log.i("RS350_bluetooth", "Close bluetooth.");
            this.mBluetoothAdapter.disable();
        }
    }

    public void cancleDiscovery() {
        if (this.isSearching) {
            boolean cancledDiscovery = this.mBluetoothAdapter.cancelDiscovery();
            Log.i("RS350_bluetooth", "Bluetooth is cancled to discovery  result :" + cancledDiscovery);
            if (cancledDiscovery) {
                this.isSearching = false;
            }
        }
    }

    public void connectDevice(BluetoothDevice device) {
        connectDevice(device, false, this.mContext);
    }

    public void connectDevice(BluetoothDevice device, boolean showDialog, Context context) {
        BluetoothDevice connectDevice = this.mConnection.getConnectDevice();
        int connectStatu = this.mConnection.getConnect_status();
        if (connectDevice == null || connectStatu == 0) {
            cancleDiscovery();
            this.mConnection.connect(device, showDialog, context);
        }
    }

    public void registBroadcastReceiver() {
        IntentFilter filter = new IntentFilter();
        filter.addAction("android.bluetooth.adapter.action.DISCOVERY_FINISHED");
        filter.addAction("android.bluetooth.device.action.FOUND");
        filter.addAction("android.bluetooth.device.action.NAME_CHANGED");
        this.mContext.registerReceiver(this.mBluetoothReceiver, filter);
    }

    public void unRegistBroadcastReceiver() {
        this.mContext.unregisterReceiver(this.mBluetoothReceiver);
    }

    public void addBlueListener(IBlutoothListener listener) {
        if (!this.mBlutoothListener.contains(listener)) {
            this.mBlutoothListener.add(listener);
        }
    }

    public void setConnectCallback(BluetoothConnection.IBluetoothConnectCallback connectCallback) {
        this.mConnection.setConnectCallback(connectCallback);
    }

    public void removeBlueListener(IBlutoothListener listener) {
        if (this.mBlutoothListener.contains(listener)) {
            this.mBlutoothListener.remove(listener);
        }
    }

    public void getConnectedDevice() {
        this.mConnection.getConnectedDevice();
    }

    public void cancelGetConnectedDevice() {
        this.mConnection.cancelGetConnectedDevice();
    }

    public void connectException() {
        Toast.makeText(this.mContext, "The device maybe connected by other device or device is unavailable.", 1).show();
    }

    public void closeBluetooth() {
        if (this.mBluetoothAdapter.isEnabled()) {
            this.mBluetoothAdapter.disable();
        }
    }

    public void openBluetooth() {
        if (!this.mBluetoothAdapter.isEnabled()) {
            this.mBluetoothAdapter.enable();
        }
    }

    public void restartBluetooth() {
        closeBluetooth();
        openBluetooth();
        Log.i("RS350_bluetooth", "Bluetooth restart.");
    }
}
