package com.clou.rs350.connect.bluetooth.decoder;

public interface HeaderDecoder extends Decoder {
    byte[] getHeader();
}
