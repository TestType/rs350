package com.clou.rs350.connect.bluetooth.manager;

import android.annotation.SuppressLint;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothServerSocket;
import android.bluetooth.BluetoothSocket;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Build;
import android.util.Log;
import com.clou.rs350.callback.ILoadCallback;
import com.clou.rs350.task.MyTask;
import java.util.UUID;

@SuppressLint({"NewApi"})
public class BluetoothConnection {
    private static final String TAG = "RS350_bluetooth";
    private IBluetoothConnectCallback connectCallback;
    private MyTask connectTask;
    private int connect_status = 0;
    BluetoothServerSocket mBluetoothServerSocket = null;
    private Context mContext;
    private BluetoothDevice mCurConnectDevice;
    private boolean mIsConnected = false;

    public interface IBluetoothConnectCallback {
        void onBluetoothConnecting(String str);

        void onBluetoothDisconnect(String str, String str2);

        void onBluetoothSocketConnected(String str, BluetoothSocket bluetoothSocket);
    }

    public BluetoothConnection(Context context) {
        this.mContext = context;
    }

    public BluetoothDevice getConnectDevice() {
        return this.mCurConnectDevice;
    }

    public void connect(BluetoothDevice device) {
        connect(device, false, this.mContext);
    }

    public void connect(BluetoothDevice device, boolean showDialog, Context context) {
        this.mContext = context;
        this.mCurConnectDevice = device;
        disConnectSoket("连接新的蓝牙，断开老的蓝牙");
        connectInTask();
    }

    private void connectInTask() {
        if (this.connectTask != null && !this.connectTask.isCancelled()) {
            this.connectTask.cancel(true);
        }
        this.connectTask = new MyTask(new ILoadCallback() {
            /* class com.clou.rs350.connect.bluetooth.manager.BluetoothConnection.AnonymousClass1 */

            @Override // com.clou.rs350.callback.ILoadCallback
            public Object run() {
                BluetoothSocket bluetoothSocket;
                try {
                    int sdk = Build.VERSION.SDK_INT;
                    if (sdk >= 10) {
                        bluetoothSocket = BluetoothConnection.this.mCurConnectDevice.createInsecureRfcommSocketToServiceRecord(UUID.fromString("00001101-0000-1000-8000-00805F9B34FB"));
                    } else {
                        bluetoothSocket = BluetoothConnection.this.mCurConnectDevice.createRfcommSocketToServiceRecord(UUID.fromString("00001101-0000-1000-8000-00805F9B34FB"));
                    }
                    if (bluetoothSocket == null) {
                        return null;
                    }
                    BluetoothConnection.this.connectCallback.onBluetoothConnecting(BluetoothConnection.this.mCurConnectDevice.getName());
                    if (sdk >= 18) {
                        BluetoothConnection.this.mCurConnectDevice.connectGatt(BluetoothConnection.this.mContext, false, new BluetoothGattCallback() {
                            /* class com.clou.rs350.connect.bluetooth.manager.BluetoothConnection.AnonymousClass1.AnonymousClass1 */
                        });
                    }
                    bluetoothSocket.connect();
                    return bluetoothSocket;
                } catch (Exception e) {
                    return null;
                }
            }

            @Override // com.clou.rs350.callback.ILoadCallback
            public void callback(Object result) {
                BluetoothSocket socket = result != null ? (BluetoothSocket) result : null;
                if (socket != null) {
                    BluetoothConnection.this.setConnectState(2);
                    BluetoothConnection.this.connectCallback.onBluetoothSocketConnected(BluetoothConnection.this.mCurConnectDevice.getName(), socket);
                    Log.i("RS350_bluetooth", "蓝牙连接成功-通知与设备建立连接");
                    return;
                }
                BluetoothConnection.this.setConnectState(0);
                BluetoothSearch.getInstance().reSearchDeviceList("蓝牙连接报错，重新搜索...");
            }
        });
        this.connectTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, 0);
    }

    public void cancelGetConnectedDevice() {
        this.mIsConnected = false;
        if (this.mBluetoothServerSocket != null) {
            try {
                this.mBluetoothServerSocket.close();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void getConnectedDevice() {
        this.mIsConnected = true;
        new Thread(new Runnable() {
            /* class com.clou.rs350.connect.bluetooth.manager.BluetoothConnection.AnonymousClass2 */

            public void run() {
                while (BluetoothConnection.this.mIsConnected) {
                    try {
                        BluetoothConnection.this.mBluetoothServerSocket = BluetoothAdapter.getDefaultAdapter().listenUsingInsecureRfcommWithServiceRecord("btspp", UUID.fromString("00001101-0000-1000-8000-00805F9B34FB"));
                        BluetoothSocket mBluetoothSocket = BluetoothConnection.this.mBluetoothServerSocket.accept();
                        if (mBluetoothSocket != null) {
                            BluetoothConnection.this.mBluetoothServerSocket.close();
                            BluetoothConnection.this.mIsConnected = false;
                            BluetoothConnection.this.connectCallback.onBluetoothSocketConnected("GetConnectedSocket", mBluetoothSocket);
                            return;
                        }
                        Thread.sleep(20);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        }).start();
    }

    public boolean disConnectSoket(String tagMessage) {
        if (this.mCurConnectDevice == null) {
            return true;
        }
        this.connectCallback.onBluetoothDisconnect(this.mCurConnectDevice.getName(), tagMessage);
        return true;
    }

    public int getConnect_status() {
        return this.connect_status;
    }

    public void setConnectState(int state) {
    }

    public void setConnectCallback(IBluetoothConnectCallback callback) {
        this.connectCallback = callback;
    }
}
