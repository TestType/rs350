package com.clou.rs350.manager;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import com.clou.rs350.CLApplication;
import com.clou.rs350.db.dao.BasicMeasurementDao;
import com.clou.rs350.db.dao.CTBurdenDao;
import com.clou.rs350.db.dao.CTMeasurementDao;
import com.clou.rs350.db.dao.CameraDao;
import com.clou.rs350.db.dao.CustomerInfoDao;
import com.clou.rs350.db.dao.DailyTestDao;
import com.clou.rs350.db.dao.DemandTestDao;
import com.clou.rs350.db.dao.DigitalMeterTestDao;
import com.clou.rs350.db.dao.DigitalMeterTypeDao;
import com.clou.rs350.db.dao.EnergyTestDao;
import com.clou.rs350.db.dao.ErrorTestDao;
import com.clou.rs350.db.dao.FieldSettingDao;
import com.clou.rs350.db.dao.HarmonicMeasurementDao;
import com.clou.rs350.db.dao.LTMDao;
import com.clou.rs350.db.dao.MeterTypeDao;
import com.clou.rs350.db.dao.OperatorInfoDao;
import com.clou.rs350.db.dao.PTBurdenDao;
import com.clou.rs350.db.dao.PTComparisonDao;
import com.clou.rs350.db.dao.ReadMeterDao;
import com.clou.rs350.db.dao.RegisterReadingDao;
import com.clou.rs350.db.dao.SealDao;
import com.clou.rs350.db.dao.SignatureDao;
import com.clou.rs350.db.dao.SiteDataManagerDao;
import com.clou.rs350.db.dao.SiteDataTestDao;
import com.clou.rs350.db.dao.StandardBaseInfoDao;
import com.clou.rs350.db.dao.TestItemDao;
import com.clou.rs350.db.dao.VisualDao;
import com.clou.rs350.db.dao.WaveMeasurementDao;
import com.clou.rs350.db.dao.WiringCheckDao;
import com.clou.rs350.db.dao.sequence.RegisterReadingSchemeDao;
import com.clou.rs350.db.dao.sequence.SealSchemeDao;
import com.clou.rs350.db.dao.sequence.SequenceDao;
import com.clou.rs350.db.dao.sequence.VisualSchemeDao;
import java.io.File;

public class DatabaseManager {
    private static final String DB_NAME = (CLApplication.app.getRootPath() + File.separator + "RS350.db");
    private static final int DB_VERSION = 30;
    public static DatabaseHelper openDataBaseHelper = null;

    public static class DatabaseHelper extends SQLiteOpenHelper {
        private Context context;
        private SQLiteDatabase database = null;

        public SQLiteDatabase getDatabase() {
            return this.database;
        }

        public synchronized SQLiteDatabase getWritableDatabase() {
            if (this.database == null) {
                this.database = super.getWritableDatabase();
            }
            return this.database;
        }

        public synchronized SQLiteDatabase getReadableDatabase() {
            this.database = super.getReadableDatabase();
            return this.database;
        }

        DatabaseHelper(Context context2) {
            super(context2, DatabaseManager.DB_NAME, (SQLiteDatabase.CursorFactory) null, 28);
            this.context = context2;
        }

        public void CloseDatabase() {
            if (this.database != null && this.database.isOpen()) {
                this.database.close();
                this.database = null;
            }
        }

        public void onCreate(SQLiteDatabase db) {
            new OperatorInfoDao(this.context).createTable(db);
            StandardBaseInfoDao.createTable(db);
            new CustomerInfoDao(this.context).createTable(db);
            new SiteDataManagerDao(this.context).createTable(db);
            new MeterTypeDao(this.context).createTable(db);
            new DigitalMeterTypeDao(this.context).createTable(db);
            new TestItemDao(this.context).createTable(db);
            new BasicMeasurementDao(this.context).createTable(db);
            new WiringCheckDao(this.context).createTable(db);
            new WaveMeasurementDao(this.context).createTable(db);
            new HarmonicMeasurementDao(this.context).createTable(db);
            new LTMDao(this.context).createTable(db);
            new ErrorTestDao(this.context).createTable(db);
            new EnergyTestDao(this.context).createTable(db);
            new DemandTestDao(this.context).createTable(db);
            new DailyTestDao(this.context).createTable(db);
            new DigitalMeterTestDao(this.context).createTable(db);
            new CTMeasurementDao(this.context).createTable(db);
            new CTBurdenDao(this.context).createTable(db);
            new PTComparisonDao(this.context).createTable(db);
            new PTBurdenDao(this.context).createTable(db);
            new ReadMeterDao(this.context).createTable(db);
            new SequenceDao(this.context).createTable(db);
            new VisualSchemeDao(this.context).createTable(db);
            new SealSchemeDao(this.context).createTable(db);
            new RegisterReadingSchemeDao(this.context).createTable(db);
            new SealDao(this.context).createTable(db);
            new SiteDataTestDao(this.context).createTable(db);
            new VisualDao(this.context).createTable(db);
            new CameraDao(this.context).createTable(db);
            new RegisterReadingDao(this.context).createTable(db);
            new SignatureDao(this.context).createTable(db);
            new FieldSettingDao(this.context).createTable(db);
        }

        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            new OperatorInfoDao(this.context).upgradeTable(db, oldVersion, newVersion);
            new CustomerInfoDao(this.context).upgradeTable(db, oldVersion, newVersion);
            new SiteDataManagerDao(this.context).upgradeTable(db, oldVersion, newVersion);
            new MeterTypeDao(this.context).upgradeTable(db, oldVersion, newVersion);
            new DigitalMeterTypeDao(this.context).upgradeTable(db, oldVersion, newVersion);
            new TestItemDao(this.context).upgradeTable(db, oldVersion, newVersion);
            new BasicMeasurementDao(this.context).upgradeTable(db, oldVersion, newVersion);
            new WiringCheckDao(this.context).upgradeTable(db, oldVersion, newVersion);
            new WaveMeasurementDao(this.context).upgradeTable(db, oldVersion, newVersion);
            new HarmonicMeasurementDao(this.context).upgradeTable(db, oldVersion, newVersion);
            new LTMDao(this.context).upgradeTable(db, oldVersion, newVersion);
            new ErrorTestDao(this.context).upgradeTable(db, oldVersion, newVersion);
            new EnergyTestDao(this.context).upgradeTable(db, oldVersion, newVersion);
            new DemandTestDao(this.context).upgradeTable(db, oldVersion, newVersion);
            new DailyTestDao(this.context).upgradeTable(db, oldVersion, newVersion);
            new DigitalMeterTestDao(this.context).upgradeTable(db, oldVersion, newVersion);
            new CTMeasurementDao(this.context).upgradeTable(db, oldVersion, newVersion);
            new CTBurdenDao(this.context).upgradeTable(db, oldVersion, newVersion);
            new PTComparisonDao(this.context).upgradeTable(db, oldVersion, newVersion);
            new PTBurdenDao(this.context).upgradeTable(db, oldVersion, newVersion);
            new ReadMeterDao(this.context).upgradeTable(db, oldVersion, newVersion);
            new SequenceDao(this.context).upgradeTable(db, oldVersion, newVersion);
            new VisualSchemeDao(this.context).upgradeTable(db, oldVersion, newVersion);
            new SealSchemeDao(this.context).upgradeTable(db, oldVersion, newVersion);
            new RegisterReadingSchemeDao(this.context).upgradeTable(db, oldVersion, newVersion);
            new SealDao(this.context).upgradeTable(db, oldVersion, newVersion);
            new SiteDataTestDao(this.context).upgradeTable(db, oldVersion, newVersion);
            new VisualDao(this.context).upgradeTable(db, oldVersion, newVersion);
            new CameraDao(this.context).upgradeTable(db, oldVersion, newVersion);
            new RegisterReadingDao(this.context).upgradeTable(db, oldVersion, newVersion);
            new SignatureDao(this.context).upgradeTable(db, oldVersion, newVersion);
            new FieldSettingDao(this.context).upgradeTable(db, oldVersion, newVersion);
        }
    }

    public static synchronized DatabaseHelper getDataBaseHelper(Context context) {
        DatabaseHelper databaseHelper;
        synchronized (DatabaseManager.class) {
            if (openDataBaseHelper == null) {
                openDataBaseHelper = new DatabaseHelper(context.getApplicationContext());
            }
            databaseHelper = openDataBaseHelper;
        }
        return databaseHelper;
    }

    public static SQLiteDatabase getWriteableDatabase(Context context) {
        return getDataBaseHelper(context).getWritableDatabase();
    }

    public static SQLiteDatabase getReadableDatabase(Context context) {
        return getDataBaseHelper(context).getReadableDatabase();
    }
}
