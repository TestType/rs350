package com.clou.rs350.manager;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Resources;
import android.util.Log;

import com.clou.rs350.Preferences;
import com.clou.rs350.R;
import com.clou.rs350.model.ClouData;
import com.clou.rs350.ui.view.StatusBarView;
import com.clou.rs350.utils.BroadCastUtil;
import com.itextpdf.text.pdf.PdfObject;

import java.util.ArrayList;
import java.util.List;

public class StatusBarManager {
    private static final String TAG = "StatusBarManager";
    private static StatusBarManager manager = new StatusBarManager();
    BatteryReceiver batteryReceiver = new BatteryReceiver();
    private String connectState = PdfObject.NOTHING;
    private BroadcastReceiver connectStateReceiver = new BroadcastReceiver() {
        /* class com.clou.rs350.manager.StatusBarManager.AnonymousClass1 */

        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            Resources res = context.getResources();
            Log.i(StatusBarManager.TAG, action);
            if (BroadCastUtil.Action.ACTION_CONNECTING.equals(action)) {
                StatusBarManager.this.connectState = res.getString(R.string.text_connecting);
            } else if (BroadCastUtil.Action.ACTION_SOCKET_CONNECTED.equals(action)) {
                if (ClouData.getInstance().getSettingBasic().DeviceModel == 0) {
                    MessageManager.getInstance(context).ReadBasicInfo();
                }
            } else if (BroadCastUtil.Action.ACTION_SERVER_CONNECTED.equals(action)) {
                StatusBarManager.this.connectState = res.getString(R.string.text_connected);
            } else if (BroadCastUtil.Action.ACTION_DISCONNECT.equals(action)) {
                StatusBarManager.this.connectState = res.getString(R.string.text_disconnect);
                if (Preferences.getBoolean(Preferences.Key.CONNECTEDBEFORE, false)) {
                    Preferences.putBoolean(Preferences.Key.CONNECTEDBEFORE, false);
                }
            } else if (BroadCastUtil.Action.ACTION_SEARCHING.equals(action)) {
                StatusBarManager.this.connectState = res.getString(R.string.text_searching);
            } else if (BroadCastUtil.Action.ACTION_NO_DEVICE.equals(action)) {
                StatusBarManager.this.connectState = res.getString(R.string.text_no_device);
            } else if (BroadCastUtil.Action.ACTION_SEARCH_COMPLETE.equals(action)) {
                if (StatusBarManager.this.connectState.equals("Searching")) {
                    StatusBarManager.this.connectState = res.getString(R.string.text_search_complete);
                }
            } else if (BroadCastUtil.Action.ACTION_BLUETOOTH_UNAVAILABLE.equals(action)) {
                StatusBarManager.this.connectState = res.getString(R.string.text_bluetooth_unavailable);
            }
            for (StatusBarView view : StatusBarManager.this.list) {
                view.setConnectState(StatusBarManager.this.connectState);
                if (!BroadCastUtil.Action.ACTION_SERVER_CONNECTED.equals(action)) {
                    view.setSignal(-100.0f);
                    StatusBarManager.this.currentDeviceBattery = 0;
                    view.setDeviceBattery(0, StatusBarManager.this.m_DeviceBatteryState);
                }
            }
        }
    };
    private int currentDeviceBattery;
    private int currentTabBattery;
    IntentFilter intentFilter = new IntentFilter("android.intent.action.BATTERY_CHANGED");
    private boolean isRegisted;
    private List<StatusBarView> list = new ArrayList();
    private Context m_Context;
    private int m_DeviceBatteryState = 0;
    private int m_TabletBatteryState = 0;
    private int m_Viscous = -1;
    protected MessageManager messageManager;

    private StatusBarManager() {
    }

    public void setContext(Context context) {
        this.m_Context = context;
    }

    public static StatusBarManager getInstance() {
        return manager;
    }

    public void registBroadcastReceiver() {
        registerSystemBatteryReceiver();
        registBluetoothReceiver();
    }

    public void unRegistBroadcastReceiver() {
        unregisterSystemBatteryReceiver();
        unRegistBluetoothReceiver();
    }

    public void addStatusView(StatusBarView view) {
        if (!this.list.contains(view)) {
            this.list.add(view);
        }
        view.setDeviceBattery(this.currentDeviceBattery, this.m_DeviceBatteryState);
        view.setTabBattery(this.currentTabBattery, this.m_TabletBatteryState);
        view.setConnectState(this.connectState);
    }

    public void removeStatusView(StatusBarView view) {
        if (this.list.contains(view)) {
            this.list.remove(view);
        }
    }

    private void registerSystemBatteryReceiver() {
        System.out.println("registerSystemBatteryReceiver");
        if (this.m_Context != null) {
            this.m_Context.registerReceiver(this.batteryReceiver, this.intentFilter);
        }
    }

    private void unregisterSystemBatteryReceiver() {
        System.out.println("unregisterSystemBatteryReceiver");
        this.m_Context.unregisterReceiver(this.batteryReceiver);
    }

    public void setDeviceBattery(int level, int scale, int status) {
        System.out.println("Device Power----->电量为" + ((level * 100) / scale) + "%");
        this.currentDeviceBattery = (level * 100) / scale;
        if (this.currentDeviceBattery >= this.m_Viscous + 15 || this.currentDeviceBattery <= 0) {
            this.m_DeviceBatteryState = 0;
            this.m_Viscous = -1;
        } else {
            if (this.m_DeviceBatteryState == 0) {
                showHint(R.string.text_device_battery_low);
            }
            this.m_DeviceBatteryState = 1;
            this.m_Viscous = 1;
        }
        for (StatusBarView view : this.list) {
            view.setDeviceBattery(this.currentDeviceBattery, this.m_DeviceBatteryState);
        }
    }

    private void showHint(int id) {
        for (StatusBarView view : this.list) {
            view.showHint(id);
        }
    }

    public void setTabBattery(int level, int scale, int status) {
        System.out.println("Tablet Power----->电量为" + ((level * 100) / scale) + "%");
        this.currentTabBattery = (level * 100) / scale;
        for (StatusBarView view : this.list) {
            view.setTabBattery(this.currentTabBattery, this.m_TabletBatteryState);
        }
    }

    /* access modifiers changed from: package-private */
    public class BatteryReceiver extends BroadcastReceiver {
        BatteryReceiver() {
        }

        public void onReceive(Context context, Intent intent) {
            if ("android.intent.action.BATTERY_CHANGED".equals(intent.getAction())) {
                StatusBarManager.this.setTabBattery(intent.getIntExtra("level", 0), intent.getIntExtra("scale", 100), intent.getIntExtra("status", 4));
            }
        }
    }

    private void registBluetoothReceiver() {
        IntentFilter filter = new IntentFilter();
        filter.addAction(BroadCastUtil.Action.ACTION_SEARCHING);
        filter.addAction(BroadCastUtil.Action.ACTION_NO_DEVICE);
        filter.addAction(BroadCastUtil.Action.ACTION_SEARCH_COMPLETE);
        filter.addAction(BroadCastUtil.Action.ACTION_CONNECTING);
        filter.addAction(BroadCastUtil.Action.ACTION_SOCKET_CONNECTED);
        filter.addAction(BroadCastUtil.Action.ACTION_SERVER_CONNECTED);
        filter.addAction(BroadCastUtil.Action.ACTION_DISCONNECT);
        filter.addAction(BroadCastUtil.Action.ACTION_BLUETOOTH_UNAVAILABLE);
        this.m_Context.registerReceiver(this.connectStateReceiver, filter);
    }

    private void unRegistBluetoothReceiver() {
        this.m_Context.unregisterReceiver(this.connectStateReceiver);
    }

    private void ClearDictionary() {
        this.messageManager.freshDevice();
        this.messageManager.freshMessage();
    }
}
