package com.clou.rs350.manager;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

import com.clou.rs350.Preferences;
import com.clou.rs350.R;
import com.clou.rs350.connect.BaseConnect;
import com.clou.rs350.connect.bluetooth.BluetoothConnect;
import com.clou.rs350.connect.callback.ISocketReceiveListener;
import com.clou.rs350.db.model.DigitalMeterDataType;
import com.clou.rs350.db.model.StandardInfo;
import com.clou.rs350.device.CL3122C;
import com.clou.rs350.device.CL3122E;
import com.clou.rs350.device.CL3122_60A;
import com.clou.rs350.device.EmulatorDevice;
import com.clou.rs350.device.MeterBaseDevice;
import com.clou.rs350.device.RS350;
import com.clou.rs350.device.model.Baudrate;
import com.clou.rs350.device.model.ConnectByteModel;
import com.clou.rs350.model.ClouData;
import com.clou.rs350.model.SettingFunction;
import com.clou.rs350.task.ISleepCallback;
import com.clou.rs350.task.SleepTask;
import com.clou.rs350.utils.BroadCastUtil;
import com.clou.rs350.utils.CountDown;
import com.clou.rs350.version.UpdateVersionManager;

import java.util.ArrayList;
import java.util.List;

public class MessageManager implements ISocketReceiveListener {
    private static MessageManager messageManager;
    private BaseConnect baseConnect;
    private MeterBaseDevice baseDevice;

    @SuppressLint({"HandlerLeak"})
    private Handler handler = new Handler() {
        /* class com.clou.rs350.manager.MessageManager.AnonymousClass1 */

        public void handleMessage(Message msg) {
            if (msg != null) {
                switch (msg.what) {
                    case 1:
                        MessageManager.this.notifyUItoRefreshData(((byte[]) msg.obj));
                        return;
                    case 2:
                        MessageManager.this.notifyUItoRefreshData(((MeterBaseDevice) msg.obj));
                        return;
                    default:
                        return;
                }
            }
        }
    };
    private boolean isFirstLaunch = true;
    private Context m_Context;
    private List<IMessageListener> messageListenerList;

    public interface IMessageListener {
        void onReceiveMessage(int i);

        void onReceiveMessage(MeterBaseDevice meterBaseDevice);

        void onReceiveMessage(String str);

        void onReceiveMessage(byte[] bArr);
    }

    private MessageManager(Context context) {
        this.m_Context = context;
        this.messageListenerList = new ArrayList();
        this.baseConnect = new BluetoothConnect(context);
        this.baseConnect.setOnReceiveMessageListener(this);
        freshDevice();
    }

    public static MessageManager getInstance(Context context) {
        if (messageManager == null) {
            messageManager = new MessageManager(context);
        }
        return messageManager;
    }

    public static MessageManager getInstance() {
        return messageManager;
    }

    public void getVersion() {
        this.baseConnect.sendMsg(this.baseDevice.getVersion(), true, "GetVersion");
    }

    public void getSerialNumber() {
        this.baseConnect.sendMsg(this.baseDevice.getSerialNumber(), true, "GetSerialNumber");
    }

    public void getHardwareVersion() {
        this.baseConnect.sendMsg(this.baseDevice.getHardwareVersion(), true, "GetHardwareVersion");
    }

    public int getData() {
        List<byte[]> bytes = this.baseDevice.getMeasureBasicData();
        this.baseConnect.sendMsg(bytes, "getData");
        if (bytes == null) {
            return 0;
        }
        return bytes.size();
    }

    public int getSpecialData() {
        List<byte[]> bytes = this.baseDevice.getMeasureSpecialData();
        this.baseConnect.sendMsg(bytes, "getSpecialData");
        if (bytes == null) {
            return 0;
        }
        return bytes.size();
    }

    public int SetCalculateType(int apparentPower, int Harmonic) {
        List<byte[]> bytes = this.baseDevice.setCalculateType(apparentPower, Harmonic);
        this.baseConnect.sendMsg(bytes, true, "SetCalculateType");
        if (bytes == null) {
            return 0;
        }
        return bytes.size();
    }

    public void getWiringCheck() {
        this.baseConnect.sendMsg(this.baseDevice.getWiringCheck(), "getWiringCheck");
    }

    public void setWiringCheckPowerFactor(int powerFactor) {
        this.baseConnect.sendMsg(this.baseDevice.setPowerFactor(powerFactor), false, false, "setPowerFactor");
    }

    public void setTestMode(int flag) {
        this.baseConnect.sendMsg(this.baseDevice.setTestMode(flag), true, "setTestMode");
    }

    public void setSimulationValue(double ua, double ub, double uc, double uaAngle, double ubAngle, double ucAngle, double ia, double ib, double ic, double iaAngle, double ibAngle, double icAngle) {
        this.baseConnect.sendMsg(this.baseDevice.setSimulationValue(ua, ub, uc, uaAngle, ubAngle, ucAngle, ia, ib, ic, iaAngle, ibAngle, icAngle), false, false, "setSimulationValue");
    }

    public void setCTChannel(int Channel) {
        this.baseConnect.sendMsg(this.baseDevice.setCTChannel(Channel), "setCTChannel");
    }

    public void getCTValue() {
        this.baseConnect.sendMsg(this.baseDevice.getCTValue(), "getCTValue");
    }

    public void getPTValue() {
        this.baseConnect.sendMsg(this.baseDevice.getPTError(), "getPTValue");
    }

    public void getPTBurden() {
        this.baseConnect.sendMsg(this.baseDevice.getPTBurden(), "getPTBurden");
    }

    public void getPTDeviceState() {
        this.baseConnect.sendMsg(this.baseDevice.getPTDeviceState(), "getPTDeviceState");
    }

    public void getDeviceBattery() {
        this.baseConnect.sendMsg(this.baseDevice.getBattery(), "getDeviceBattery");
    }

    public void setPTAutomaticCheck(int check) {
        this.baseConnect.sendMsg(this.baseDevice.setAutomaticCheck(check), false, false, "setAutomaticCheck");
    }

    public void setPTSettingFlag(int PTSettingFlag) {
        this.baseConnect.sendMsg(this.baseDevice.setPTSettingFlag(PTSettingFlag), "setPTSettingFlag");
    }

    public void getPTAutomaticCheckState() {
        this.baseConnect.sendMsg(this.baseDevice.getAutomaticCheckState(), "getPTAutomaticCheckState");
    }

    public void cleanPTAutomaticCheckState() {
        this.baseConnect.sendMsg(this.baseDevice.cleanAutomaticCheckState(), false, false, "cleanPTAutomaticCheckState");
    }

    public void setIntegrationParameter(int Interval, int Period) {
        this.baseConnect.sendMsg(this.baseDevice.setIntegrationParameter(Interval, Period), true, false, "setIntegrationParameter ");
    }

    public void StartLTM(int Interval, int Period) {
        List<byte[]> bytes = this.baseDevice.setIntegrationParameter(Interval, Period);
        bytes.addAll(this.baseDevice.setFunc_Mstate_Start(67, 1));
        this.baseConnect.sendMsg(bytes, true, false, "StartLTM ");
    }

    public void getIntegrationData() {
        this.baseConnect.sendMsg(this.baseDevice.getIntegrationData(), "getIntegrationData");
    }

    public void cleanIntegrationFlag() {
        this.baseConnect.sendMsg(this.baseDevice.cleanIntegrationFlag(), "cleanIntegrationFlag");
    }

    public void getRanage() {
        this.baseConnect.sendMsg(this.baseDevice.getParameter(), "getRanage");
    }

    public void setWiringData(int wiring, int Pluse) {
        List<byte[]> bytes = this.baseDevice.setWiringData(wiring, Pluse);
        bytes.addAll(this.baseDevice.getParameter());
        this.baseConnect.sendMsg(bytes, true, false, "SetWiringData");
    }

    public void setWiringData(int wiring, int Pluse, int Definition) {
        List<byte[]> bytes = this.baseDevice.setWiringData(wiring, Pluse, Definition);
        bytes.addAll(this.baseDevice.getParameter());
        this.baseConnect.sendMsg(bytes, true, false, "SetWiringData");
    }

    public void cleanOverloadFlag() {
        this.baseConnect.sendMsg(this.baseDevice.cleanOverload(), true, false, "cleanOverloadFlag");
    }

    public void SetRange(int AutoConnectType, int ConnectType, int Auto, int UaRange, int UbRange, int UcRange, int IaRange, int IbRange, int IcRange) {
        List<byte[]> bytes = this.baseDevice.setRange(AutoConnectType, ConnectType, Auto, (double) UaRange, (double) UbRange, (double) UcRange, (double) IaRange, (double) IbRange, (double) IcRange);
        bytes.addAll(this.baseDevice.getParameter());
        this.baseConnect.sendMsg(bytes, false, false, "setRange");
    }

    public void SetNeutralRange(int AutoRange, int UnRange, int InRange) {
        this.baseConnect.sendMsg(this.baseDevice.setNeutralRange(AutoRange, UnRange, InRange), false, false, "setNeutralRange");
    }

    public void SetParameter(int Wiring, int Pulse, int ConnectType, int Auto, int UaRange, int UbRange, int UcRange, int IaRange, int IbRange, int IcRange) {
        List<byte[]> bytes = this.baseDevice.setParameter(Wiring, Pulse, Auto, UaRange, UbRange, UcRange, IaRange, IbRange, IcRange);
        bytes.addAll(this.baseDevice.getParameter());
        this.baseConnect.sendMsg(bytes, true, "SetParameter");
    }

    public void SetRatio(double PTRatio, double CTRatio, int RatioApply) {
        this.baseConnect.sendMsg(this.baseDevice.setPTCTRatio(PTRatio, CTRatio, RatioApply), true, "SetRatio");
    }

    public int getUIWave() {
        List<byte[]> bytes = this.baseDevice.getUIWave();
        this.baseConnect.sendMsg(bytes, "getUIWave");
        if (bytes == null) {
            return 0;
        }
        return bytes.size();
    }

    public int getPQWave() {
        List<byte[]> bytes = this.baseDevice.getPQWave();
        this.baseConnect.sendMsg(bytes, "getPQWave");
        if (bytes == null) {
            return 0;
        }
        return bytes.size();
    }

    public void getHarmonic(String type) {
        List<byte[]> bytes = this.baseDevice.getHarmonic(type);
        bytes.addAll(this.baseDevice.getTHD());
        this.baseConnect.sendMsg(bytes, "getHarmonic " + type);
    }

    public void getHarmonic(int type) {
        List<byte[]> bytes = this.baseDevice.getHarmonic(type);
        bytes.addAll(this.baseDevice.getTHD());
        this.baseConnect.sendMsg(bytes, "getHarmonic " + type);
    }

    public void getHarmonicValue(int type) {
        this.baseConnect.sendMsg(this.baseDevice.getHarmonicValues(type), "getHarmonicValues " + type);
    }

    public void getHarmonicValue(int phase, int type) {
        this.baseConnect.sendMsg(this.baseDevice.getHarmonicValues(phase, type), "getHarmonicValues " + type);
    }

    public void getHarmonicContent(int type) {
        this.baseConnect.sendMsg(this.baseDevice.getHarmonicContents(type), "getHarmonicContents " + type);
    }

    public void getHarmonicAngle(int type) {
        this.baseConnect.sendMsg(this.baseDevice.getHarmonicAngles(type), "getHarmonicAngles " + type);
    }

    public void getHarmonicTHD() {
        this.baseConnect.sendMsg(this.baseDevice.getTHD(), "getTHD ");
    }

    public int getHarmonic() {
        List<byte[]> bytes = this.baseDevice.getHarmonic();
        this.baseConnect.sendMsg(bytes, "getHarmonic");
        if (bytes == null) {
            return 0;
        }
        return bytes.size();
    }

    public void setStart(int start) {
        this.baseConnect.sendMsg(this.baseDevice.setStart(start), true, "setStart : " + start);
    }

    public void getStartState() {
        this.baseConnect.sendMsg(this.baseDevice.getStartState(), "getStartState");
    }

    public void getStartStateAndFunc_Mstate() {
        this.baseConnect.sendMsg(this.baseDevice.getStartStateAndFunc_Mstate(), true, "getStartStateAndFunc_Mstate");
    }

    public void setCheckKFlag(int flag) {
        this.baseConnect.sendMsg(this.baseDevice.setCheckKFlag(flag), true, "setCheckKFlag ");
    }

    public void getCheckKFlag() {
        this.baseConnect.sendMsg(this.baseDevice.getCheckKFlag(), "getCheckKFlag ");
    }

    public void getMeterConstant() {
        this.baseConnect.sendMsg(this.baseDevice.getMeterConstant(), "getMeterConstant ");
    }

    public void getErrors() {
        this.baseConnect.sendMsg(this.baseDevice.getErrors(), "getErrors ");
    }

    public void getEnergy() {
        this.baseConnect.sendMsg(this.baseDevice.getEnergy(), "getEnergy");
    }

    public void getDemand() {
        this.baseConnect.sendMsg(this.baseDevice.getDemand(), "getDemand");
    }

    public void setFunc_Mstate(int state) {
        this.baseConnect.sendMsg(this.baseDevice.setFunc_Mstate(state), true, false, "setFunc_Mstate ");
    }

    public void getFunc_Mstate() {
        this.baseConnect.sendMsg(this.baseDevice.getFunc_Mstate(), false, false, "getFunc_Mstate ");
    }

    public void setFunc_Mstate_Start(int state, int start) {
        this.baseConnect.sendMsg(this.baseDevice.setFunc_Mstate_Start(state, start), true, "setFunc_Mstate_Start ");
    }

    public void SetParameters(int[] measurementType) {
        this.baseConnect.sendMsg(this.baseDevice.setErrorParameters(measurementType), true, false, "SetParameters ");
    }

    public void SetParameters(float[] constants, int[] pulses) {
        this.baseConnect.sendMsg(this.baseDevice.setErrorParameters(constants, pulses), true, false, "SetParameters ");
    }

    public void SetParameters(int[] measurementType, float[] constants, int[] pulses) {
        this.baseConnect.sendMsg(this.baseDevice.setErrorParameters(measurementType, constants, pulses), true, false, "SetParameters ");
    }

    public void SetParameters(int[] errorCount, int[] measurementType, float[] constants, int[] pulses) {
        this.baseConnect.sendMsg(this.baseDevice.setErrorParameters(errorCount, measurementType, constants, pulses), true, "SetParameters ");
    }

    public void StartErrorTest(int[] errorCount, int[] measurementType, float[] constants, int[] pulses) {
        List<byte[]> bytes = this.baseDevice.setErrorParameters(errorCount, measurementType, constants, pulses);
        bytes.addAll(this.baseDevice.setFunc_Mstate_Start(33, 1));
        this.baseConnect.sendMsg(bytes, true, "StartErrorTest ");
    }

    public void SetDailyParameters(int[] measurementType, int[] constants, int[] pulses) {
        this.baseConnect.sendMsg(this.baseDevice.setDailyParameters(measurementType, constants, pulses), false, false, "SetParameters ");
    }

    public void setDigitalParameter(int baudrate, int dataBits, int parity, int stopBits, int address, DigitalMeterDataType[] dataType, int pt, int ct) {
        this.baseConnect.sendMsg(this.baseDevice.setDigitalMeterParameters(baudrate, dataBits, parity, stopBits, address, dataType, pt, ct), true, "setDigitalParameter ");
    }

    public void getDigitalError() {
        this.baseConnect.sendMsg(this.baseDevice.getDigitalMeterErrors(), "getDigitalError ");
    }

    public void setDigitalStartFlag(int flag) {
        this.baseConnect.sendMsg(this.baseDevice.setDigitalMeterTestStartFlag(flag), true, "setDigitalStartFlag");
    }

    public void SetPTAddress(int location, int channel) {
        this.baseConnect.sendMsg(this.baseDevice.setPTAddress(location, channel), false, false, "SetAddress ");
    }

    public void setPTConnect() {
        this.baseConnect.sendMsg(this.baseDevice.setPTConnect(1), false, false, "setConnect ");
    }

    public void setAdjust(double UaValue, double UbValue, double UcValue, double IaValue, double IbValue, double IcValue, double UaAngle, double UbAngle, double UcAngle, double IaAngle, double IbAngle, double IcAngle) {
        this.baseConnect.sendMsg(this.baseDevice.setAdjust(UaValue, UbValue, UcValue, IaValue, IbValue, IcValue, UaAngle, UbAngle, UcAngle, IaAngle, IbAngle, IcAngle), true, "adjust");
    }

    public void setNeutralAdjust(double UnValue, double InValue) {
        this.baseConnect.sendMsg(this.baseDevice.setAdjustNeutral(UnValue, InValue), true, "setAdjustNeutral");
    }

    public void setDCOffsetAdjust(int flag) {
        this.baseConnect.sendMsg(this.baseDevice.setDCAdjustFlag(flag), true, "setDCOffsetAdjust");
    }

    public void setResetAdjustment(int flag) {
        this.baseConnect.sendMsg(this.baseDevice.setResetAdjustmentFlag(flag), true, "setResetAdjustment");
    }

    public void getAdjustState() {
        this.baseConnect.sendMsg(this.baseDevice.getAdjustState(), "getAdjustState");
    }

    public void setStopAdjust() {
        this.baseConnect.sendMsg(this.baseDevice.setStopAdjust(), true, "stopAdjust");
    }

    public void SetAdjustFlag(int flag) {
        this.baseConnect.sendMsg(this.baseDevice.setAdjustFlag(flag), false, false, "SetAdjustFlag");
    }

    public void GetAdjustFlag() {
        this.baseConnect.sendMsg(this.baseDevice.getAdjustFlag(), false, false, "GetAdjustFlag");
    }

    public void GetAllowAdjustFlag() {
        this.baseConnect.sendMsg(this.baseDevice.getAllowAdjustFlag(), false, false, "getAllowAdjustFlag");
    }

    public void CleanAllowAdjustFlag() {
        this.baseConnect.sendMsg(this.baseDevice.cleanAllowAdjustFlag(), false, false, "cleanAllowAdjustFlag");
    }

    public void GetClampAdjustmentFlag() {
        this.baseConnect.sendMsg(this.baseDevice.getClampAdjustmentFlag(), false, false, "getClampAdjustmentFlag");
    }

    public void SetStandardInfo(StandardInfo info) {
        this.baseConnect.sendMsg(this.baseDevice.setStandardInfo(info), true, "SetStandardInfo");
    }

    public void getStandardInfo() {
        this.baseConnect.sendMsg(this.baseDevice.getStandardInfo(), false, false, "getStandardInfo");
    }

    private void AnsOK() {
        this.baseConnect.sendMsgInThread(this.baseDevice.AnsOK(), "AnsOK");
    }

    private void AnsErr() {
        this.baseConnect.sendMsgInThread(this.baseDevice.AnsErr(), "AnsErr");
    }

    public void setConstant(long constant) {
        this.baseConnect.sendMsg(this.baseDevice.setConstant(constant), true, "setConstant");
    }

    public void getConstant() {
        this.baseConnect.sendMsg(this.baseDevice.getConstant(), "getConstant");
    }

    public void setBaudrate(int comPort, int baudrate, int databits, int parity, int stopbits) {
        this.baseConnect.sendMsg(this.baseDevice.setBaudrate(comPort, baudrate, databits, parity, stopbits), "setBaudrate");
    }

    public void setBaudrate(int comPort, Baudrate baudrate) {
        this.baseConnect.sendMsg(this.baseDevice.setBaudrate(comPort, (int) baudrate.baudrate.l_Value, (int) baudrate.dataBits.l_Value, (int) baudrate.parityBits.l_Value, (int) baudrate.stopBits.l_Value), "setBaudrate");
    }

    public void sendBuffer(int comPort, byte[] buffer) {
        List<byte[]> bytes = new ArrayList<>();
        bytes.add(buffer);
        this.baseConnect.sendMsgInThread(this.baseDevice.sendBuffer(comPort, bytes).get(0), "readMeter");
    }

    public void sendBuffer(byte[] buffer) {
        this.baseConnect.sendMsgInThread(buffer, "sendBuffer");
    }

    public ConnectByteModel getReceiveModel() {
        return this.baseDevice.getReceiveModel();
    }

    public void clearReceiveBuffer() {
        this.baseDevice.clearReceiveBuffer();
    }

    public boolean isConnected() {
        return this.baseConnect.isConnected();
    }

    public boolean isBusy() {
        return this.baseConnect.isBusy();
    }

    public void addMessageListener(IMessageListener messageListener) {
        if (this.messageListenerList != null && messageListener != null && !this.messageListenerList.contains(messageListener)) {
            this.messageListenerList.add(messageListener);
        }
    }

    public boolean removeMessageListener(IMessageListener messageListener) {
        if (this.messageListenerList == null || messageListener == null || !this.messageListenerList.contains(messageListener)) {
            return false;
        }
        return this.messageListenerList.remove(messageListener);
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private synchronized void notifyUItoRefreshData(MeterBaseDevice Device) {
        if (this.messageListenerList != null) {
            for (IMessageListener listener : this.messageListenerList) {
                listener.onReceiveMessage(Device);
            }
        }
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private synchronized void notifyUItoRefreshData(byte[] buffer) {
        if (this.messageListenerList != null) {
            for (IMessageListener listener : this.messageListenerList) {
                listener.onReceiveMessage(buffer);
            }
        }
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private synchronized void notifyUItoRefreshData(int strId) {
        if (this.messageListenerList != null) {
            for (IMessageListener listener : this.messageListenerList) {
                listener.onReceiveMessage(strId);
            }
        }
    }

    private synchronized void notifyUItoRefreshData(String str) {
        if (this.messageListenerList != null) {
            for (IMessageListener listener : this.messageListenerList) {
                listener.onReceiveMessage(str);
            }
        }
    }

    private void sendMessage(byte[] data) {
        Message msg = new Message();
        msg.what = 1;
        msg.obj = data;
        this.handler.sendMessage(msg);
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private void sendMessage(MeterBaseDevice Device) {
        Message msg = new Message();
        msg.what = 2;
        msg.obj = Device;
        this.handler.sendMessage(msg);
    }

    @Override // com.clou.rs350.connect.callback.ISocketReceiveListener
    public void onReceiveMessage(byte[] data) {
        if (data != null) {
            CountDown cd = new CountDown();
            cd.start("-------UnPacket-------");
            int result = this.baseDevice.UnPacket(data);
            cd.stop("-------UnPacket-------");
            Log.i("MessageManager", "unpacket result: " + result);
            switch (result) {
                case 2:
                    sendMessage(this.baseDevice.getReceiveModel().Buffer);
                    break;
                case 16:
                    AnsOK();
                    this.baseConnect.setBusy(false);
                    Log.i("MessageManager", "receive OK");
                    break;
                case 17:
                    AnsOK();
                    this.baseConnect.setBusy(false);
                    sendMessage(this.baseDevice);
                    break;
                case 129:
                    this.baseConnect.setBusy(true);
                    Log.i("MessageManager", "receive busy");
                    break;
                case 131:
                    break;
                default:
                    this.baseConnect.notifyToSendNextMsg();
                    sendMessage(this.baseDevice);
                    break;
            }
            if (!this.baseConnect.isConnected()) {
                this.baseConnect.onSocketConnected();
                ReadBasicInfo();
            }
        }
    }

    public void ReadBasicInfo() {
        getVersion();
        getSerialNumber();
        getStandardInfo();
        getDeviceBattery();
        getStartStateAndFunc_Mstate();
        readStandardSerialNoFirmwareVersion();
        setSettings();
    }

    private void setSettings() {
        SetCalculateType(Preferences.getInt(Preferences.Key.SSTYPE, 0), 0);
    }

    private void readStandardSerialNoFirmwareVersion() {
        new SleepTask(3000, new ISleepCallback() {
            /* class com.clou.rs350.manager.MessageManager.AnonymousClass2 */

            @Override // com.clou.rs350.task.ISleepCallback
            public void onSleepAfterToDo() {
                boolean z = true;
                try {
                    ClouData.getInstance().getStandardInfo().setFirmwareVersion(MessageManager.messageManager.getMeter().getVersionValue());
                    ClouData.getInstance().getStandardInfo().setStandardSerialNo(MessageManager.messageManager.getMeter().getSerialNumberValue());
                    ClouData.getInstance().setStandardInfo(MessageManager.messageManager.getMeter().getStandardInfoValue(ClouData.getInstance().getStandardInfo()));
                    ClouData.getInstance().getStandardInfo().saveInfo();
                    SettingFunction settingFunction = ClouData.getInstance().getSettingFunction();
                    if (1 != (ClouData.getInstance().getStandardInfo().Function & 1)) {
                        z = false;
                    }
                    settingFunction.TestSequence = z;
                    ClouData.getInstance().getSettingFunction().saveSetting();
                    if (ClouData.getInstance().getStandardInfo().Calib_Valid_Date.getTime() < System.currentTimeMillis()) {
                        MessageManager.this.notifyUItoRefreshData(R.string.text_calibration_expired);
                    }
                    BroadCastUtil.send(UpdateVersionManager.NEW_VERSION_ACTION, "version", ClouData.getInstance().getVersionInfo().Versions.get((String.valueOf(ClouData.getInstance().getStandardInfo().MainBoardVersion.trim()) + "," + ClouData.getInstance().getStandardInfo().MeasureBoardVersion.trim() + "," + ClouData.getInstance().getStandardInfo().NeutralBoardVersion.trim()).replace("_", "-")));
                    MessageManager.this.sendMessage(MessageManager.this.baseDevice);
                } catch (Exception e) {
                }
            }
        }).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, 0);
    }

    public void GetPowerQualityUnbalance() {
        this.baseConnect.sendMsg(this.baseDevice.getPowerQualityUnbalance(), false, true, "GetPowerQualityUnbalance");
    }

    public void GetPowerQualityStability() {
        this.baseConnect.sendMsg(this.baseDevice.getPowerQualityStability(), false, true, "GetPowerQualityStability");
    }

    public void SetPowerQualityStabilityParams(int StbMode, int PhaseStabTestTime, int PhaseStabSampleTime) {
        this.baseConnect.sendMsg(this.baseDevice.setPowerQualityStabilityParams(StbMode, PhaseStabTestTime, PhaseStabSampleTime), true, false, "SetPowerQualityStabilityParams");
    }

    public MeterBaseDevice getMeter() {
        return this.baseDevice;
    }

    public void freshDevice() {
        if (ClouData.getInstance().isDemo()) {
            this.baseDevice = new EmulatorDevice();
        } else {
            int deviceModel = ClouData.getInstance().getSettingBasic().DeviceModel;
            if (deviceModel == 0) {
                this.baseDevice = new RS350();
            } else if (deviceModel == 1) {
                this.baseDevice = new CL3122E();
            } else if (2 == deviceModel) {
                this.baseDevice = new CL3122C();
            } else {
                this.baseDevice = new CL3122_60A();
            }
        }
        setDeviceAngleDefinition(true);
    }

    public void setDeviceAngleDefinition(boolean isSaved) {
        if (isSaved) {
            this.baseDevice.setAngleDefinition(ClouData.getInstance().getSettingBasic().AngleDefinition, ClouData.getInstance().getSettingBasic().VectorDefinition, ClouData.getInstance().getSettingBasic().VectorType);
        } else {
            this.baseDevice.setAngleDefinition(0, 0, 0);
        }
    }

    public boolean removeAllMessage() {
        this.baseConnect.removeAllMessage();
        return true;
    }

    public void freshMessage() {
    }
}
