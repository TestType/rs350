package com.clou.rs350.report;

import com.itextpdf.text.pdf.PdfObject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class XmlModel {
    public Map<String, String> Condition = new HashMap();
    public List<Object> Datas = new ArrayList();
    public String Type = PdfObject.NOTHING;
}
