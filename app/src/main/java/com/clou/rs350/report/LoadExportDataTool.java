package com.clou.rs350.report;

import android.content.Context;
import com.clou.rs350.db.dao.BaseDao;
import com.clou.rs350.db.dao.BasicMeasurementDao;
import com.clou.rs350.db.dao.CTBurdenDao;
import com.clou.rs350.db.dao.CTMeasurementDao;
import com.clou.rs350.db.dao.CameraDao;
import com.clou.rs350.db.dao.CustomerInfoDao;
import com.clou.rs350.db.dao.DailyTestDao;
import com.clou.rs350.db.dao.DemandTestDao;
import com.clou.rs350.db.dao.DigitalMeterTestDao;
import com.clou.rs350.db.dao.EnergyTestDao;
import com.clou.rs350.db.dao.ErrorTestDao;
import com.clou.rs350.db.dao.HarmonicMeasurementDao;
import com.clou.rs350.db.dao.PTBurdenDao;
import com.clou.rs350.db.dao.PTComparisonDao;
import com.clou.rs350.db.dao.ReadMeterDao;
import com.clou.rs350.db.dao.RegisterReadingDao;
import com.clou.rs350.db.dao.SealDao;
import com.clou.rs350.db.dao.SignatureDao;
import com.clou.rs350.db.dao.SiteDataTestDao;
import com.clou.rs350.db.dao.TestItemDao;
import com.clou.rs350.db.dao.VisualDao;
import com.clou.rs350.db.dao.WaveMeasurementDao;
import com.clou.rs350.db.dao.WiringCheckDao;
import com.clou.rs350.db.model.report.DataItem;
import com.clou.rs350.db.model.report.ReportDatas;
import java.util.List;

public class LoadExportDataTool {
    Context m_Context;
    ReportDatas m_ReportDatas;

    public LoadExportDataTool(Context context) {
        this.m_Context = context;
    }

    private void inputData(BaseDao dao, String key1, String key2) {
        List<DataItem> tmpData = dao.queryReportDataByKeyAndTime(key1, key2);
        if (tmpData == null) {
            return;
        }
        if (this.m_ReportDatas.Datas.containsKey(dao.getTableName())) {
            this.m_ReportDatas.Datas.get(dao.getTableName()).addAll(tmpData);
        } else {
            this.m_ReportDatas.Datas.put(dao.getTableName(), tmpData);
        }
    }

    private void inputDatas(BaseDao dao, String key1, String key2) {
        inputDatas(dao, key1, key2, true);
    }

    private void inputDatas(BaseDao dao, String key1, String key2, boolean setReportModel) {
        List<DataItem> tmpData;
        if (setReportModel && (tmpData = dao.queryReportDataByKeyAndTime(key1, key2)) != null) {
            if (this.m_ReportDatas.Datas.containsKey(dao.getTableName())) {
                this.m_ReportDatas.Datas.get(dao.getTableName()).addAll(tmpData);
            } else {
                this.m_ReportDatas.Datas.put(dao.getTableName(), tmpData);
            }
        }
    }

    public ReportDatas loadData(String CustomerSerialNo, String[] Times) {
        this.m_ReportDatas = new ReportDatas();
        BaseDao tmpDao = new CustomerInfoDao(this.m_Context);
        this.m_ReportDatas.Datas.put(tmpDao.getTableName(), tmpDao.queryReportDataByKeyAndTime(CustomerSerialNo));
        for (int i = 0; i < Times.length; i++) {
            inputData(new TestItemDao(this.m_Context), CustomerSerialNo, Times[i]);
            inputDatas(new SiteDataTestDao(this.m_Context), CustomerSerialNo, Times[i]);
            inputDatas(new CameraDao(this.m_Context), CustomerSerialNo, Times[i]);
            inputDatas(new VisualDao(this.m_Context), CustomerSerialNo, Times[i]);
            inputDatas(new SealDao(this.m_Context), CustomerSerialNo, Times[i]);
            inputDatas(new RegisterReadingDao(this.m_Context), CustomerSerialNo, Times[i]);
            inputDatas(new BasicMeasurementDao(this.m_Context), CustomerSerialNo, Times[i]);
            inputDatas(new WaveMeasurementDao(this.m_Context), CustomerSerialNo, Times[i]);
            inputDatas(new WiringCheckDao(this.m_Context), CustomerSerialNo, Times[i]);
            inputDatas(new HarmonicMeasurementDao(this.m_Context), CustomerSerialNo, Times[i]);
            inputDatas(new ErrorTestDao(this.m_Context), CustomerSerialNo, Times[i]);
            inputDatas(new DemandTestDao(this.m_Context), CustomerSerialNo, Times[i]);
            inputDatas(new EnergyTestDao(this.m_Context), CustomerSerialNo, Times[i]);
            inputDatas(new DailyTestDao(this.m_Context), CustomerSerialNo, Times[i]);
            inputDatas(new DigitalMeterTestDao(this.m_Context), CustomerSerialNo, Times[i]);
            inputDatas(new CTMeasurementDao(this.m_Context), CustomerSerialNo, Times[i]);
            inputDatas(new CTBurdenDao(this.m_Context), CustomerSerialNo, Times[i]);
            inputDatas(new PTBurdenDao(this.m_Context), CustomerSerialNo, Times[i]);
            inputDatas(new PTComparisonDao(this.m_Context), CustomerSerialNo, Times[i]);
            inputDatas(new ReadMeterDao(this.m_Context), CustomerSerialNo, Times[i]);
            inputDatas(new SignatureDao(this.m_Context), CustomerSerialNo, Times[i]);
        }
        return this.m_ReportDatas;
    }
}
