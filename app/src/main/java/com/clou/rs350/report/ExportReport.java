package com.clou.rs350.report;

import android.content.Context;
import android.media.MediaScannerConnection;
import android.net.Uri;
import com.clou.rs350.CLApplication;
import com.clou.rs350.db.dao.TestItemDao;
import com.clou.rs350.db.dao.WaveMeasurementDao;
import com.clou.rs350.db.model.WaveValue;
import com.clou.rs350.db.model.report.DataItem;
import com.clou.rs350.db.model.report.ReportDatas;
import com.clou.rs350.model.ClouData;
import com.clou.rs350.ui.view.MyVectorPrintView;
import com.clou.rs350.ui.view.MyWaveView;
import com.clou.rs350.utils.BitmapUtils;
import com.clou.rs350.utils.DataProcessing;
import com.clou.rs350.utils.OtherUtils;
import com.itextpdf.text.BadElementException;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.ColumnText;
import com.itextpdf.text.pdf.PdfContentByte;
import com.itextpdf.text.pdf.PdfObject;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfPageEventHelper;
import com.itextpdf.text.pdf.PdfReader;
import com.itextpdf.text.pdf.PdfStamper;
import com.itextpdf.text.pdf.PdfTemplate;
import com.itextpdf.text.pdf.PdfWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class ExportReport {
    static final float IMAGESCALE = 50.0f;
    static final int MARGINBOTTOM = 30;
    static final int MARGINLEFT = 50;
    static final int MARGINRIGHT = 50;
    static final int MARGINTOP = 30;
    static final int ROWHIGHT = 18;
    static final float TEXTPADDINGLEFT = 5.0f;
    int AngleDefinition = 0;
    int VectorType = 0;
    boolean hasHeaderFooter = false;
    BaseFont m_BaseFont = null;
    Context m_Context;
    public List<PdfTemplate> m_PageTotals = new ArrayList();
    float m_TextSize = 13.0f;
    String tmpPicturePath = (String.valueOf(CLApplication.app.getImagePath()) + File.separator + "TmpPicture.jpg");

    public ExportReport(Context context) {
        this.m_Context = context;
    }

    public class PdfReportHeaderFooter extends PdfPageEventHelper {
        ReportDatas m_Datas;
        private float m_HeaderFooterMarginContent = ExportReport.TEXTPADDINGLEFT;
        XmlModel m_Templet;

        public PdfReportHeaderFooter(XmlModel templet, ReportDatas datas) {
            this.m_Templet = templet;
            this.m_Datas = datas;
        }

        @Override // com.itextpdf.text.pdf.PdfPageEventHelper, com.itextpdf.text.pdf.PdfPageEvent
        public void onOpenDocument(PdfWriter writer, Document document) {
        }

        @Override // com.itextpdf.text.pdf.PdfPageEventHelper, com.itextpdf.text.pdf.PdfPageEvent
        public void onStartPage(PdfWriter writer, Document document) {
        }

        @Override // com.itextpdf.text.pdf.PdfPageEventHelper, com.itextpdf.text.pdf.PdfPageEvent
        public void onEndPage(PdfWriter writer, Document doc) {
            PdfPTable m_HeaderTable = null;
            PdfPTable m_FooterTable = null;
            PdfContentByte cb = writer.getDirectContent();
            for (int i = 0; i < this.m_Templet.Datas.size(); i++) {
                String name = ((XmlModel) this.m_Templet.Datas.get(i)).Type;
                if (name.equals("Header")) {
                    m_HeaderTable = ExportReport.this.getHeaderFooterTable(writer, name, (XmlModel) this.m_Templet.Datas.get(i), this.m_Datas);
                    m_HeaderTable.setTotalWidth(doc.right() - doc.left());
                    m_HeaderTable.writeSelectedRows(0, -1, ExportReport.IMAGESCALE, doc.top() + m_HeaderTable.getTotalHeight() + this.m_HeaderFooterMarginContent, cb);
                } else if (name.equals("Footer")) {
                    m_FooterTable = ExportReport.this.getHeaderFooterTable(writer, name, (XmlModel) this.m_Templet.Datas.get(i), this.m_Datas);
                    m_FooterTable.setTotalWidth(doc.right() - doc.left());
                    m_FooterTable.writeSelectedRows(0, -1, ExportReport.IMAGESCALE, doc.bottom() + m_FooterTable.getTotalHeight() + this.m_HeaderFooterMarginContent, cb);
                }
            }
            float topMargin = 30.0f;
            float buttomMargin = 30.0f;
            if (m_HeaderTable != null) {
                topMargin = 30.0f + m_HeaderTable.getTotalHeight() + this.m_HeaderFooterMarginContent;
            }
            if (m_FooterTable != null) {
                buttomMargin = 30.0f + m_FooterTable.getTotalHeight() + this.m_HeaderFooterMarginContent;
            }
            doc.setMargins(ExportReport.IMAGESCALE, ExportReport.IMAGESCALE, topMargin, buttomMargin);
        }

        @Override // com.itextpdf.text.pdf.PdfPageEventHelper, com.itextpdf.text.pdf.PdfPageEvent
        public void onCloseDocument(PdfWriter writer, Document document) {
            Font tmpFont = new Font(ExportReport.this.m_BaseFont, ExportReport.this.m_TextSize, 0);
            if (ExportReport.this.m_PageTotals != null) {
                for (int i = 0; i < ExportReport.this.m_PageTotals.size(); i++) {
                    ColumnText.showTextAligned(ExportReport.this.m_PageTotals.get(i), 1, new Phrase(String.valueOf(writer.getPageNumber() - 2), tmpFont), ExportReport.this.m_PageTotals.get(i).getWidth() / 2.0f, ColumnText.GLOBAL_SPACE_CHAR_RATIO, ColumnText.GLOBAL_SPACE_CHAR_RATIO);
                }
            }
        }
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private PdfPTable getHeaderFooterTable(PdfWriter writer, String strEnd, XmlModel parser, ReportDatas datas) {
        PdfPTable tmpTable = new PdfPTable(1);
        for (int i = 0; i < parser.Datas.size(); i++) {
            if (((XmlModel) parser.Datas.get(i)).Type.equals("Row")) {
                getRow(writer, tmpTable, parser.Datas.size(), (XmlModel) parser.Datas.get(i), datas);
            } else {
                tmpTable.addCell(getCell(writer, (XmlModel) parser.Datas.get(i), datas));
            }
        }
        return tmpTable;
    }

    private void setHeaderFooter(PdfWriter writer, Document doc, XmlModel parser, String templetPath, ReportDatas datas) {
        this.hasHeaderFooter = true;
        writer.setPageEvent(new PdfReportHeaderFooter(parser, datas));
        try {
            doc.add(new Paragraph(" "));
            doc.newPage();
            doc.add(new Paragraph(" "));
            doc.newPage();
        } catch (DocumentException e) {
            e.printStackTrace();
        }
    }

    private PdfPTable getPage(XmlModel parser, PdfWriter writer) {
        List<PdfPCell> cells = new ArrayList<>();
        List<Float> withs = new ArrayList<>();
        for (int i = 0; i < parser.Datas.size(); i++) {
            String name = ((XmlModel) parser.Datas.get(i)).Type;
            PdfPCell tmpCell = null;
            if (name.equals("String")) {
                String tmpString = getString((XmlModel) parser.Datas.get(i));
                tmpCell = new PdfPCell(getParagraph(tmpString));
                withs.add(Float.valueOf(((float) tmpString.length()) * 10.0f));
            } else if (name.equals("PageIndex")) {
                String pageIndex = PdfObject.NOTHING;
                if (writer != null) {
                    if (this.hasHeaderFooter) {
                        pageIndex = new StringBuilder(String.valueOf(writer.getPageNumber() - 2)).toString();
                    } else {
                        pageIndex = new StringBuilder(String.valueOf(writer.getPageNumber())).toString();
                    }
                }
                withs.add(Float.valueOf(15.0f));
                tmpCell = new PdfPCell(getParagraph(pageIndex));
            } else if (name.equals("PageTotal")) {
                if (writer != null) {
                    PdfTemplate pageTotal = writer.getDirectContent().createTemplate(15.0f, this.m_TextSize);
                    try {
                        tmpCell = new PdfPCell(Image.getInstance(pageTotal));
                    } catch (BadElementException e) {
                        e.printStackTrace();
                        tmpCell = new PdfPCell(getParagraph(PdfObject.NOTHING));
                    }
                    this.m_PageTotals.add(pageTotal);
                } else {
                    tmpCell = new PdfPCell(getParagraph(PdfObject.NOTHING));
                }
                withs.add(Float.valueOf(15.0f));
            }
            if (tmpCell != null) {
                tmpCell.setBorder(0);
                tmpCell.setHorizontalAlignment(1);
                tmpCell.setVerticalAlignment(1);
                tmpCell.setMinimumHeight(18.0f);
                tmpCell.setPadding(ColumnText.GLOBAL_SPACE_CHAR_RATIO);
                cells.add(tmpCell);
            }
        }
        PdfPTable tmpTable = new PdfPTable(cells.size());
        float[] fWidth = new float[withs.size()];
        for (int i2 = 0; i2 < fWidth.length; i2++) {
            fWidth[i2] = withs.get(i2).floatValue();
        }
        try {
            tmpTable.setTotalWidth(fWidth);
        } catch (DocumentException e2) {
            e2.printStackTrace();
        }
        tmpTable.setLockedWidth(true);
        for (int i3 = 0; i3 < cells.size(); i3++) {
            tmpTable.getDefaultCell().setMinimumHeight(18.0f);
            tmpTable.addCell(cells.get(i3));
        }
        return tmpTable;
    }

    private String getString(XmlModel parser) {
        String value;
        if (parser.Datas.size() > 0) {
            value = (String) parser.Datas.get(0);
        } else {
            value = PdfObject.NOTHING;
        }
        return value.replace("\n", PdfObject.NOTHING).trim();
    }

    private List<List<String[]>> getCondition(String strCondition) {
        String[] condition_1;
        String[] condition_0 = strCondition.split(",");
        List<List<String[]>> conditions_0 = new ArrayList<>();
        for (String str : condition_0) {
            List<String[]> conditions_1 = new ArrayList<>();
            for (String str2 : str.trim().split("\\|")) {
                String[] tmpCondition = str2.trim().split("=");
                for (int k = 0; k < tmpCondition.length; k++) {
                    tmpCondition[k] = tmpCondition[k].trim();
                }
                conditions_1.add(tmpCondition);
            }
            conditions_0.add(conditions_1);
        }
        return conditions_0;
    }

    private boolean contrastData(List<List<String[]>> condition_0, DataItem data) {
        boolean result_0 = false;
        try {
            if (condition_0.size() <= 0) {
                return true;
            }
            for (int i = 0; i < condition_0.size(); i++) {
                List<String[]> condition_1 = condition_0.get(i);
                boolean result_1 = false;
                int j = 0;
                while (true) {
                    if (j >= condition_1.size()) {
                        break;
                    } else if (data.DataMap.get(condition_1.get(j)[0]).equals(condition_1.get(j)[1])) {
                        result_1 = true;
                        break;
                    } else {
                        j++;
                    }
                }
                if (!result_1) {
                    return false;
                }
                result_0 = true;
            }
            return result_0;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    private int getAlign(XmlModel parser) {
        if (!parser.Condition.containsKey("Align")) {
            return -1;
        }
        String tmpAlign = parser.Condition.get("Align").toUpperCase();
        if (tmpAlign.equals("CENTER")) {
            return 1;
        }
        if (tmpAlign.equals("LEFT")) {
            return 0;
        }
        if (tmpAlign.equals("RIGHT")) {
            return 2;
        }
        return -1;
    }

    private String getValue(XmlModel parser, DataItem dataList) {
        try {
            return dataList.DataMap.get(((String) parser.Datas.get(0)).replace("\n", PdfObject.NOTHING).replace(" ", PdfObject.NOTHING));
        } catch (Exception e) {
            return PdfObject.NOTHING;
        }
    }

    private String getValue(XmlModel parser, ReportDatas datas) {
        String value = PdfObject.NOTHING;
        if (!parser.Condition.containsKey("DataTable")) {
            return PdfObject.NOTHING;
        }
        String tmpDataTable = parser.Condition.get("DataTable");
        if (datas.Datas.containsKey(tmpDataTable)) {
            List<DataItem> dataList = datas.Datas.get(tmpDataTable);
            if (dataList != null) {
                if (parser.Condition.containsKey("Condition")) {
                    List<List<String[]>> conditions = getCondition(parser.Condition.get("Condition"));
                    int i = 0;
                    while (true) {
                        if (i >= dataList.size()) {
                            break;
                        }
                        try {
                            if (contrastData(conditions, dataList.get(i))) {
                                value = dataList.get(i).DataMap.get(((String) parser.Datas.get(0)).replace("\n", PdfObject.NOTHING).replace(" ", PdfObject.NOTHING));
                                break;
                            }
                            i++;
                        } catch (Exception e) {
                        }
                    }
                } else {
                    value = dataList.get(0).DataMap.get(((String) parser.Datas.get(0)).replace("\n", PdfObject.NOTHING).replace(" ", PdfObject.NOTHING));
                }
            } else {
                return PdfObject.NOTHING;
            }
        }
        return value;
    }

    private Image getPicture(XmlModel parser, DataItem datas) {
        try {
            return Image.getInstance(String.valueOf(CLApplication.app.getImagePath()) + File.separator + getValue(parser, datas));
        } catch (BadElementException e) {
            e.printStackTrace();
        } catch (MalformedURLException e2) {
            e2.printStackTrace();
        } catch (IOException e3) {
            e3.printStackTrace();
        }
        return null;
    }

    private Image getPicture(XmlModel parser, ReportDatas datas) {
        try {
            return Image.getInstance(String.valueOf(CLApplication.app.getImagePath()) + File.separator + getValue(parser, datas));
        } catch (BadElementException e) {
            e.printStackTrace();
        } catch (MalformedURLException e2) {
            e2.printStackTrace();
        } catch (IOException e3) {
            e3.printStackTrace();
        }
        return null;
    }

    private Image getVector(XmlModel parser, DataItem data) {
        if (data == null) {
            return null;
        }
        try {
            MyVectorPrintView tmpView = new MyVectorPrintView(this.m_Context);
            Double[] angles = {Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d), Double.valueOf(0.0d)};
            if (data.DataMap.containsKey("VoltageVectorAngleL1")) {
                String[] tmpValues = {"VoltageVectorValueL1", "VoltageVectorAngleL1", "VoltageVectorValueL2", "VoltageVectorAngleL2", "VoltageVectorValueL3", "VoltageVectorAngleL3", "CurrentValueL1", "CurrentAngleL1", "CurrentValueL2", "CurrentAngleL2", "CurrentValueL3", "CurrentAngleL3"};
                for (int i = 0; i < 12; i++) {
                    String tmpMeasureValue = data.DataMap.get(tmpValues[i]).replace("V", PdfObject.NOTHING).replace("A", PdfObject.NOTHING).replace("m", PdfObject.NOTHING).replace("k", PdfObject.NOTHING).replace("°", PdfObject.NOTHING);
                    if (!tmpMeasureValue.equals("---")) {
                        angles[i] = Double.valueOf(OtherUtils.parseDouble(tmpMeasureValue));
                    } else {
                        angles[i] = Double.valueOf(0.0d);
                    }
                }
            }
            BitmapUtils.saveBitmapFile(tmpView.getBitmap(angles, this.AngleDefinition, OtherUtils.parseInt(data.DataMap.get("IntWiringType")), this.VectorType), this.tmpPicturePath);
            Image tmpPic = Image.getInstance(this.tmpPicturePath);
            tmpPic.scalePercent(IMAGESCALE);
            return tmpPic;
        } catch (BadElementException e) {
            e.printStackTrace();
            return null;
        } catch (MalformedURLException e2) {
            e2.printStackTrace();
            return null;
        } catch (IOException e3) {
            e3.printStackTrace();
            return null;
        }
    }

    private Image getVector(XmlModel parser, ReportDatas datas) {
        Image tmpPic = null;
        DataItem tmpData = null;
        if (!parser.Condition.containsKey("DataTable")) {
            return null;
        }
        String tmpDataTable = parser.Condition.get("DataTable");
        if (datas.Datas.containsKey(tmpDataTable)) {
            List<DataItem> dataList = datas.Datas.get(tmpDataTable);
            if (parser.Condition.containsKey("Condition")) {
                List<List<String[]>> conditions = getCondition(parser.Condition.get("Condition"));
                int i = 0;
                while (true) {
                    if (i >= dataList.size()) {
                        break;
                    }
                    try {
                        if (contrastData(conditions, dataList.get(i))) {
                            tmpData = dataList.get(i);
                            break;
                        }
                        i++;
                    } catch (Exception e) {
                    }
                }
            } else {
                tmpData = dataList.get(0);
            }
        }
        if (tmpData != null) {
            tmpPic = getVector((XmlModel) null, tmpData);
        }
        return tmpPic;
    }

    private Image getWave(XmlModel parser, DataItem data) {
        if (data == null) {
            return null;
        }
        int type = 0;
        try {
            MyWaveView tmpView = new MyWaveView(this.m_Context);
            WaveValue[] wave = {new WaveValue(), new WaveValue(), new WaveValue(), new WaveValue(), new WaveValue(), new WaveValue()};
            if (data.DataMap.containsKey(WaveMeasurementDao.TableColumns.VoltageWaveformL1)) {
                String[] tmpItems = getString(parser).split(",");
                String[] tmpValues = {WaveMeasurementDao.TableColumns.VoltageWaveformL1, WaveMeasurementDao.TableColumns.VoltageWaveformL1Max, WaveMeasurementDao.TableColumns.VoltageWaveformL1Min, WaveMeasurementDao.TableColumns.VoltageWaveformL2, WaveMeasurementDao.TableColumns.VoltageWaveformL2Max, WaveMeasurementDao.TableColumns.VoltageWaveformL2Min, WaveMeasurementDao.TableColumns.VoltageWaveformL3, WaveMeasurementDao.TableColumns.VoltageWaveformL3Max, WaveMeasurementDao.TableColumns.VoltageWaveformL3Min, WaveMeasurementDao.TableColumns.CurrentWaveformL1, WaveMeasurementDao.TableColumns.CurrentWaveformL1Max, WaveMeasurementDao.TableColumns.CurrentWaveformL1Min, WaveMeasurementDao.TableColumns.CurrentWaveformL2, WaveMeasurementDao.TableColumns.CurrentWaveformL2Max, WaveMeasurementDao.TableColumns.CurrentWaveformL2Min, WaveMeasurementDao.TableColumns.CurrentWaveformL3, WaveMeasurementDao.TableColumns.CurrentWaveformL3Max, WaveMeasurementDao.TableColumns.CurrentWaveformL3Min, WaveMeasurementDao.TableColumns.PWaveformL1, WaveMeasurementDao.TableColumns.PWaveformL1Max, WaveMeasurementDao.TableColumns.PWaveformL1Min, WaveMeasurementDao.TableColumns.PWaveformL2, WaveMeasurementDao.TableColumns.PWaveformL2Max, WaveMeasurementDao.TableColumns.PWaveformL2Min, WaveMeasurementDao.TableColumns.PWaveformL3, WaveMeasurementDao.TableColumns.PWaveformL3Max, WaveMeasurementDao.TableColumns.PWaveformL3Min, WaveMeasurementDao.TableColumns.QWaveformL1, WaveMeasurementDao.TableColumns.QWaveformL1Max, WaveMeasurementDao.TableColumns.QWaveformL1Min, WaveMeasurementDao.TableColumns.QWaveformL2, WaveMeasurementDao.TableColumns.QWaveformL2Max, WaveMeasurementDao.TableColumns.QWaveformL2Min, WaveMeasurementDao.TableColumns.QWaveformL3, WaveMeasurementDao.TableColumns.QWaveformL3Max, WaveMeasurementDao.TableColumns.QWaveformL3Min};
                String[] tmpUnit = {"V", "V", "V", "A", "A", "A", "W", "W", "W", "var", "var", "var"};
                int index = 0;
                for (int i = 0; i < tmpItems.length; i++) {
                    if (tmpItems[i].equals("UL1")) {
                        type = 0;
                        index = 0;
                    } else if (tmpItems[i].equals("UL2")) {
                        type = 0;
                        index = 1;
                    } else if (tmpItems[i].equals("UL3")) {
                        type = 0;
                        index = 2;
                    } else if (tmpItems[i].equals("IL1")) {
                        type = 0;
                        index = 3;
                    } else if (tmpItems[i].equals("IL2")) {
                        type = 0;
                        index = 4;
                    } else if (tmpItems[i].equals("IL3")) {
                        type = 0;
                        index = 5;
                    } else if (tmpItems[i].equals("PL1")) {
                        type = 1;
                        index = 6;
                    } else if (tmpItems[i].equals("PL2")) {
                        type = 1;
                        index = 7;
                    } else if (tmpItems[i].equals("PL3")) {
                        type = 1;
                        index = 8;
                    } else if (tmpItems[i].equals("QL1")) {
                        type = 1;
                        index = 9;
                    } else if (tmpItems[i].equals("QL2")) {
                        type = 1;
                        index = 10;
                    } else if (tmpItems[i].equals("QL3")) {
                        type = 1;
                        index = 11;
                    }
                    wave[index - (type * 6)] = new WaveValue(DataProcessing.parseData(data.DataMap.get(tmpValues[(index * 3) + 1]), tmpUnit[index]), DataProcessing.parseData(data.DataMap.get(tmpValues[(index * 3) + 2]), tmpUnit[index]), DataProcessing.parseDouble(data.DataMap.get(tmpValues[index * 3]), "\\|"));
                }
            }
            BitmapUtils.saveBitmapFile(tmpView.getBitmap(type, wave[0], wave[3], wave[1], wave[4], wave[2], wave[5]), this.tmpPicturePath);
            Image tmpPic = Image.getInstance(this.tmpPicturePath);
            tmpPic.scalePercent(IMAGESCALE);
            return tmpPic;
        } catch (BadElementException e) {
            e.printStackTrace();
            return null;
        } catch (MalformedURLException e2) {
            e2.printStackTrace();
            return null;
        } catch (IOException e3) {
            e3.printStackTrace();
            return null;
        }
    }

    private Image getWave(XmlModel parser, ReportDatas datas) {
        Image tmpPic = null;
        DataItem tmpData = null;
        if (!parser.Condition.containsKey("DataTable")) {
            return null;
        }
        String tmpDataTable = parser.Condition.get("DataTable");
        if (datas.Datas.containsKey(tmpDataTable)) {
            List<DataItem> dataList = datas.Datas.get(tmpDataTable);
            if (parser.Condition.containsKey("Condition")) {
                List<List<String[]>> conditions = getCondition(parser.Condition.get("Condition"));
                int i = 0;
                while (true) {
                    if (i >= dataList.size()) {
                        break;
                    }
                    try {
                        if (contrastData(conditions, dataList.get(i))) {
                            tmpData = dataList.get(i);
                            break;
                        }
                        i++;
                    } catch (Exception e) {
                    }
                }
            } else {
                tmpData = dataList.get(0);
            }
        }
        if (tmpData != null) {
            tmpPic = getWave(parser, tmpData);
        }
        return tmpPic;
    }

    private List<DataItem> getDataList(DataItem datas, String strSplit) {
        String tmpValue;
        List<DataItem> tmpList = new ArrayList<>();
        int max = 0;
        Map<String, String[]> tmpItem = new HashMap<>();
        Set<String> tmpKey = datas.DataMap.keySet();
        if (strSplit.equals("|")) {
            strSplit = "\\|";
        }
        for (String key : tmpKey) {
            String[] tmpValues = datas.DataMap.get(key).split(strSplit);
            if (max < tmpValues.length) {
                max = tmpValues.length;
            }
            tmpItem.put(key, tmpValues);
        }
        for (int i = 0; i < max; i++) {
            DataItem tmpDataItem = new DataItem();
            tmpDataItem.DataMap.put("Index", new StringBuilder(String.valueOf(i + 1)).toString());
            for (String key2 : tmpKey) {
                if (i < tmpItem.get(key2).length) {
                    tmpValue = tmpItem.get(key2)[i];
                } else {
                    tmpValue = tmpItem.get(key2)[tmpItem.get(key2).length - 1];
                }
                tmpDataItem.DataMap.put(key2, tmpValue);
            }
            tmpList.add(tmpDataItem);
        }
        return tmpList;
    }

    private void loopData(Document doc, XmlModel parser, ReportDatas datas) {
        if (parser.Condition.containsKey("DataTable")) {
            String tmpDataTable = parser.Condition.get("DataTable");
            List<List<String[]>> conditions = new ArrayList<>();
            if (parser.Condition.containsKey("Condition")) {
                conditions = getCondition(parser.Condition.get("Condition"));
            }
            if (datas.Datas.containsKey(tmpDataTable)) {
                List<DataItem> dataList = datas.Datas.get(tmpDataTable);
                for (int i = 0; i < dataList.size(); i++) {
                    try {
                        if (contrastData(conditions, dataList.get(i))) {
                            for (int j = 0; j < parser.Datas.size(); j++) {
                                PdfPTable tmpTable = new PdfPTable(1);
                                tmpTable.setWidthPercentage(100.0f);
                                PdfPCell tmpCell = getCell((PdfWriter) null, (XmlModel) parser.Datas.get(j), dataList.get(i));
                                if (tmpCell != null) {
                                    tmpCell.setBorder(0);
                                    tmpTable.addCell(tmpCell);
                                    doc.add(tmpTable);
                                }
                            }
                        }
                    } catch (Exception e) {
                    }
                }
            }
        }
    }

    private PdfPTable getTable(String column) {
        PdfPTable tmpTable;
        if (column.contains(",")) {
            String[] tmpStr = column.split(",");
            float[] widths = new float[tmpStr.length];
            for (int i = 0; i < widths.length; i++) {
                widths[i] = (float) OtherUtils.parseDouble(tmpStr[i]);
            }
            tmpTable = new PdfPTable(widths);
        } else {
            int tableColumn = OtherUtils.parseInt(column);
            if (tableColumn == 0) {
                tableColumn = 1;
            }
            tmpTable = new PdfPTable(Math.abs(tableColumn));
        }
        tmpTable.setWidthPercentage(100.0f);
        return tmpTable;
    }

    private void insertTable(PdfPTable table, String column, XmlModel parser, DataItem data, boolean newColumn) {
        PdfPTable tmpTable = table;
        if (newColumn) {
            tmpTable = getTable(column);
        }
        int tableColumn = tmpTable.getNumberOfColumns();
        for (int j = 0; j < tableColumn; j++) {
            PdfPCell tmpCell = getCell((PdfWriter) null, (XmlModel) parser.Datas.get(j), data);
            if (tmpCell != null) {
                tmpCell.setUseAscender(true);
                tmpCell.setVerticalAlignment(5);
                tmpCell.setMinimumHeight(18.0f);
                tmpTable.addCell(tmpCell);
            }
        }
        if (newColumn) {
            PdfPCell tmpCell2 = new PdfPCell(tmpTable);
            tmpCell2.setPadding(ColumnText.GLOBAL_SPACE_CHAR_RATIO);
            table.addCell(tmpCell2);
        }
    }

    private void loopTable(PdfPTable table, String column, XmlModel parser, ReportDatas datas) {
        boolean newColumn = false;
        if (parser.Condition.containsKey("DataTable")) {
            String tmpDataTable = parser.Condition.get("DataTable");
            List<List<String[]>> conditions = new ArrayList<>();
            if (parser.Condition.containsKey("Condition")) {
                conditions = getCondition(parser.Condition.get("Condition"));
            }
            int tableColumn = table.getNumberOfColumns();
            if (parser.Condition.containsKey("Column")) {
                column = parser.Condition.get("Column");
                tableColumn = getTable(column).getNumberOfColumns();
                newColumn = true;
            }
            try {
                for (int ItemIndex = parser.Datas.size(); ItemIndex < tableColumn; ItemIndex++) {
                    parser.Datas.add(new XmlModel());
                }
            } catch (Exception e) {
            }
            if (datas.Datas.containsKey(tmpDataTable)) {
                List<DataItem> dataList = datas.Datas.get(tmpDataTable);
                for (int i = 0; i < dataList.size(); i++) {
                    try {
                        if (contrastData(conditions, dataList.get(i))) {
                            if (parser.Condition.containsKey("Split")) {
                                List<DataItem> tmpList = getDataList(dataList.get(i), parser.Condition.get("Split"));
                                for (int k = 0; k < tmpList.size(); k++) {
                                    insertTable(table, column, parser, tmpList.get(k), newColumn);
                                }
                            } else {
                                insertTable(table, column, parser, dataList.get(i), newColumn);
                            }
                        }
                    } catch (Exception e2) {
                    }
                }
            }
        }
    }

    private PdfPCell getCell(PdfWriter writer, XmlModel parser, DataItem datas) {
        PdfPCell tmpCell = null;
        String name = parser.Type;
        int tmpAlign = getAlign(parser);
        if (name.equals("Table")) {
            tmpCell = new PdfPCell(getTable(writer, parser, datas));
            tmpCell.setPadding(ColumnText.GLOBAL_SPACE_CHAR_RATIO);
        } else if (name.equals("String")) {
            tmpCell = new PdfPCell(getParagraph(getString(parser), parser));
            tmpCell.setPaddingLeft(TEXTPADDINGLEFT);
        } else if (name.equals("Value")) {
            tmpCell = new PdfPCell(getParagraph(getValue(parser, datas), parser));
            tmpCell.setPaddingLeft(TEXTPADDINGLEFT);
        } else if (name.equals("Picture")) {
            Image tmpPic = getPicture(parser, datas);
            if (tmpPic != null) {
                tmpPic.scalePercent(IMAGESCALE);
                tmpCell = new PdfPCell(tmpPic);
                tmpCell.setPadding(2.0f);
                tmpCell.setHorizontalAlignment(1);
            } else {
                tmpCell = new PdfPCell(getParagraph(PdfObject.NOTHING));
            }
        } else if (name.equals("Page")) {
            tmpCell = new PdfPCell(getPage(parser, writer));
            tmpCell.setPadding(ColumnText.GLOBAL_SPACE_CHAR_RATIO);
        } else if (name.equals("Logo")) {
            Image tmpPic2 = null;
            try {
                tmpPic2 = Image.getInstance(String.valueOf(CLApplication.app.getTemplateLogoPath()) + File.separator + getString(parser));
                tmpPic2.scalePercent(2200.0f / tmpPic2.getHeight());
            } catch (BadElementException e) {
                e.printStackTrace();
            } catch (IOException e2) {
                e2.printStackTrace();
            }
            if (tmpPic2 != null) {
                tmpCell = new PdfPCell(tmpPic2);
                tmpCell.setPadding(2.0f);
            } else {
                tmpCell = new PdfPCell(getParagraph(PdfObject.NOTHING));
            }
        } else if (name.equals("Vector")) {
            Image tmpPic3 = getVector(parser, datas);
            if (tmpPic3 != null) {
                tmpCell = new PdfPCell(tmpPic3);
                tmpCell.setPadding(2.0f);
                tmpCell.setHorizontalAlignment(1);
                tmpCell.setVerticalAlignment(1);
            } else {
                tmpCell = new PdfPCell(getParagraph(PdfObject.NOTHING));
            }
        } else if (name.equals("Wave")) {
            Image tmpPic4 = getWave(parser, datas);
            if (tmpPic4 != null) {
                tmpCell = new PdfPCell(tmpPic4);
                tmpCell.setPadding(2.0f);
                tmpCell.setHorizontalAlignment(1);
                tmpCell.setVerticalAlignment(1);
            } else {
                tmpCell = new PdfPCell(getParagraph(PdfObject.NOTHING));
            }
        }
        if (tmpAlign != -1) {
            tmpCell.setHorizontalAlignment(tmpAlign);
        }
        return tmpCell;
    }

    private PdfPCell getCell(PdfWriter writer, XmlModel parser, ReportDatas datas) {
        PdfPCell tmpCell = null;
        String name = parser.Type;
        int tmpAlign = getAlign(parser);
        if (name.equals("If") && getResult(parser, datas)) {
            for (int i = 0; i < parser.Datas.size(); i++) {
                getCell(writer, (XmlModel) parser.Datas.get(i), datas);
            }
        } else if (name.equals("Table")) {
            tmpCell = new PdfPCell(getTable(writer, parser, datas));
            tmpCell.setPadding(ColumnText.GLOBAL_SPACE_CHAR_RATIO);
        } else if (name.equals("String")) {
            tmpCell = new PdfPCell(getParagraph(getString(parser), parser));
            tmpCell.setPaddingLeft(TEXTPADDINGLEFT);
        } else if (name.equals("Value")) {
            tmpCell = new PdfPCell(getParagraph(getValue(parser, datas), parser));
            tmpCell.setPaddingLeft(TEXTPADDINGLEFT);
        } else if (name.equals("Picture")) {
            Image tmpPic = getPicture(parser, datas);
            if (tmpPic != null) {
                tmpPic.scalePercent(IMAGESCALE);
                tmpCell = new PdfPCell(tmpPic);
                tmpCell.setPadding(2.0f);
                tmpCell.setHorizontalAlignment(1);
            } else {
                tmpCell = new PdfPCell(getParagraph(PdfObject.NOTHING));
            }
        } else if (name.equals("Page")) {
            tmpCell = new PdfPCell(getPage(parser, writer));
            tmpCell.setPadding(ColumnText.GLOBAL_SPACE_CHAR_RATIO);
        } else if (name.equals("Logo")) {
            Image tmpPic2 = null;
            try {
                tmpPic2 = Image.getInstance(String.valueOf(CLApplication.app.getTemplateLogoPath()) + File.separator + getString(parser));
                tmpPic2.scalePercent(2200.0f / tmpPic2.getHeight());
            } catch (BadElementException e) {
                e.printStackTrace();
            } catch (IOException e2) {
                e2.printStackTrace();
            }
            if (tmpPic2 != null) {
                tmpCell = new PdfPCell(tmpPic2);
                tmpCell.setPadding(2.0f);
            } else {
                tmpCell = new PdfPCell(getParagraph(PdfObject.NOTHING));
            }
        } else if (name.equals("Vector")) {
            Image tmpPic3 = getVector(parser, datas);
            if (tmpPic3 != null) {
                tmpCell = new PdfPCell(tmpPic3);
                tmpCell.setPadding(2.0f);
                tmpCell.setHorizontalAlignment(1);
                tmpCell.setVerticalAlignment(1);
            } else {
                tmpCell = new PdfPCell(getParagraph(PdfObject.NOTHING));
            }
        } else if (name.equals("Wave")) {
            Image tmpPic4 = getWave(parser, datas);
            if (tmpPic4 != null) {
                tmpCell = new PdfPCell(tmpPic4);
                tmpCell.setPadding(2.0f);
                tmpCell.setHorizontalAlignment(1);
                tmpCell.setVerticalAlignment(1);
            } else {
                tmpCell = new PdfPCell(getParagraph(PdfObject.NOTHING));
            }
        } else if (name.equals("NewLine")) {
            tmpCell = new PdfPCell(getParagraph(PdfObject.NOTHING));
        }
        if (tmpAlign != -1) {
            tmpCell.setHorizontalAlignment(tmpAlign);
        }
        return tmpCell;
    }

    private void getRow(PdfWriter writer, PdfPTable table, int columns, XmlModel parser, DataItem datas) {
        for (int ItemIndex = parser.Datas.size(); ItemIndex < columns; ItemIndex++) {
            parser.Datas.add(new XmlModel());
        }
        for (int i = 0; i < columns; i++) {
            PdfPCell tmpCell = getCell(writer, (XmlModel) parser.Datas.get(i), datas);
            if (tmpCell != null) {
                tmpCell.setUseAscender(true);
                tmpCell.setMinimumHeight(18.0f);
                tmpCell.setVerticalAlignment(5);
                table.addCell(tmpCell);
            } else {
                table.addCell(PdfObject.NOTHING);
            }
        }
    }

    private void getRow(PdfWriter writer, PdfPTable table, int columns, XmlModel parser, ReportDatas datas) {
        for (int ItemIndex = parser.Datas.size(); ItemIndex < columns; ItemIndex++) {
            parser.Datas.add(new XmlModel());
        }
        for (int i = 0; i < columns; i++) {
            PdfPCell tmpCell = getCell(writer, (XmlModel) parser.Datas.get(i), datas);
            if (tmpCell != null) {
                tmpCell.setUseAscender(true);
                tmpCell.setMinimumHeight(18.0f);
                tmpCell.setVerticalAlignment(5);
                table.addCell(tmpCell);
            } else {
                table.addCell(PdfObject.NOTHING);
            }
        }
    }

    private PdfPTable getTable(PdfWriter writer, XmlModel parser, DataItem datas) {
        int tableColumn;
        PdfPTable tmpTable;
        int columnIndex = 0;
        String column = parser.Condition.get("Column");
        if (column.contains(",")) {
            String[] tmpStr = column.split(",");
            tableColumn = tmpStr.length;
            float[] widths = new float[tableColumn];
            for (int i = 0; i < widths.length; i++) {
                widths[i] = (float) OtherUtils.parseDouble(tmpStr[i]);
            }
            tmpTable = new PdfPTable(widths);
        } else {
            tableColumn = OtherUtils.parseInt(column);
            if (tableColumn == 0) {
                tableColumn = 1;
            }
            tmpTable = new PdfPTable(Math.abs(tableColumn));
        }
        tmpTable.setWidthPercentage(100.0f);
        for (int i2 = 0; i2 < parser.Datas.size(); i2++) {
            String name = ((XmlModel) parser.Datas.get(i2)).Type;
            if (columnIndex < tableColumn) {
                columnIndex++;
                PdfPCell tmpCell = null;
                if (name.equals("Row")) {
                    getRow(writer, tmpTable, tableColumn, (XmlModel) parser.Datas.get(i2), datas);
                    columnIndex = 0;
                } else {
                    tmpCell = getCell(writer, (XmlModel) parser.Datas.get(i2), datas);
                }
                if (tmpCell != null) {
                    tmpCell.setUseAscender(true);
                    tmpCell.setMinimumHeight(18.0f);
                    tmpCell.setVerticalAlignment(5);
                    tmpTable.addCell(tmpCell);
                }
            }
        }
        return tmpTable;
    }

    private PdfPTable getTable(PdfWriter writer, XmlModel parser, ReportDatas datas) {
        int tableColumn;
        PdfPTable tmpTable;
        int columnIndex = 0;
        String column = parser.Condition.get("Column");
        if (column.contains(",")) {
            String[] tmpStr = column.split(",");
            tableColumn = tmpStr.length;
            float[] widths = new float[tableColumn];
            for (int i = 0; i < widths.length; i++) {
                widths[i] = (float) OtherUtils.parseDouble(tmpStr[i]);
            }
            tmpTable = new PdfPTable(widths);
        } else {
            tableColumn = OtherUtils.parseInt(column);
            if (tableColumn == 0) {
                tableColumn = 1;
            }
            tmpTable = new PdfPTable(Math.abs(tableColumn));
        }
        tmpTable.setWidthPercentage(100.0f);
        for (int i2 = 0; i2 < parser.Datas.size(); i2++) {
            String name = ((XmlModel) parser.Datas.get(i2)).Type;
            if (columnIndex < tableColumn) {
                columnIndex++;
                PdfPCell tmpCell = null;
                if (name.equals("Row")) {
                    getRow(writer, tmpTable, tableColumn, (XmlModel) parser.Datas.get(i2), datas);
                    columnIndex = 0;
                } else if (name.equals("Loop")) {
                    loopTable(tmpTable, column, (XmlModel) parser.Datas.get(i2), datas);
                    columnIndex = 0;
                } else {
                    tmpCell = getCell(writer, (XmlModel) parser.Datas.get(i2), datas);
                }
                if (tmpCell != null) {
                    tmpCell.setUseAscender(true);
                    tmpCell.setMinimumHeight(18.0f);
                    tmpCell.setVerticalAlignment(5);
                    tmpTable.addCell(tmpCell);
                }
            }
        }
        return tmpTable;
    }

    public Paragraph getParagraph(String str) {
        Paragraph tmpParagraph = new Paragraph(str, new Font(this.m_BaseFont, this.m_TextSize, 0));
        tmpParagraph.setSpacingBefore(TEXTPADDINGLEFT);
        tmpParagraph.setSpacingAfter(TEXTPADDINGLEFT);
        return tmpParagraph;
    }

    public Paragraph getParagraph(String str, XmlModel parser) {
        Paragraph tmpParagraph = new Paragraph(str, getFont(parser));
        tmpParagraph.setSpacingBefore(TEXTPADDINGLEFT);
        tmpParagraph.setSpacingAfter(TEXTPADDINGLEFT);
        return tmpParagraph;
    }

    private Font getFont(XmlModel parser) {
        int iStyle = 0;
        float fSize = this.m_TextSize;
        if (parser.Condition.containsKey("Style")) {
            String[] tmpStyle = parser.Condition.get("Style").split(",");
            for (int i = 0; i < tmpStyle.length; i++) {
                if (tmpStyle[i].toUpperCase().equals("BOLD")) {
                    iStyle |= 1;
                } else if (tmpStyle[i].toUpperCase().equals("ITALIC")) {
                    iStyle |= 2;
                } else if (tmpStyle[i].toUpperCase().equals(Chunk.UNDERLINE)) {
                    iStyle |= 4;
                }
            }
        }
        if (parser.Condition.containsKey("Size")) {
            fSize = (float) OtherUtils.parseDouble(parser.Condition.get("Size"));
        }
        Font tmpFont = new Font(this.m_BaseFont, fSize, iStyle);
        if (parser.Condition.containsKey("Color")) {
            String[] tmpColor = parser.Condition.get("Color").split(",");
            tmpFont.setColor(OtherUtils.parseInt(tmpColor[0]), OtherUtils.parseInt(tmpColor[1]), OtherUtils.parseInt(tmpColor[2]));
        }
        return tmpFont;
    }

    private void setBaseFont() {
        try {
            if (ClouData.getInstance().getSettingFunction().CNVersion) {
                this.m_BaseFont = BaseFont.createFont("assets/simhei.ttf", BaseFont.IDENTITY_H, false);
                this.m_TextSize = 12.0f;
                return;
            }
            this.m_BaseFont = BaseFont.createFont("assets/CALIBRI.TTF", BaseFont.IDENTITY_H, false);
        } catch (DocumentException | IOException e) {
        }
    }

    private boolean getResult(XmlModel parser, ReportDatas datas) {
        if (!parser.Condition.containsKey("DataTable")) {
            return true;
        }
        String tmpDataTable = parser.Condition.get("DataTable");
        if (!datas.Datas.containsKey(tmpDataTable)) {
            return false;
        }
        if (!parser.Condition.containsKey("Condition")) {
            return true;
        }
        List<DataItem> dataList = datas.Datas.get(tmpDataTable);
        if (dataList == null) {
            return false;
        }
        List<List<String[]>> conditions = getCondition(parser.Condition.get("Condition"));
        for (int i = 0; i < dataList.size(); i++) {
            try {
                if (contrastData(conditions, dataList.get(i))) {
                    return true;
                }
            } catch (Exception e) {
            }
        }
        return false;
    }

    private void getCell(Document doc, PdfWriter writer, XmlModel parser, ReportDatas datas) {
        String name = parser.Type;
        if (name.equals("NewPage")) {
            doc.newPage();
        } else if (name.equals("Loop")) {
            loopData(doc, parser, datas);
        } else if (!name.equals("If") || !getResult(parser, datas)) {
            PdfPTable tmpTable = new PdfPTable(1);
            tmpTable.setWidthPercentage(100.0f);
            int tmpAlign = getAlign(parser);
            PdfPCell tmpCell = getCell(writer, parser, datas);
            if (tmpCell != null) {
                tmpCell.setBorder(0);
                tmpCell.setMinimumHeight(18.0f);
                tmpCell.setVerticalAlignment(1);
                tmpCell.setHorizontalAlignment(0);
                if (tmpAlign != -1) {
                    tmpCell.setHorizontalAlignment(tmpAlign);
                }
                tmpTable.addCell(tmpCell);
                try {
                    doc.add(tmpTable);
                } catch (DocumentException e) {
                    e.printStackTrace();
                }
            }
        } else {
            for (int i = 0; i < parser.Datas.size(); i++) {
                getCell(doc, writer, (XmlModel) parser.Datas.get(i), datas);
            }
        }
    }

    public String exportPdf(String templet, String pdfName, ReportDatas datas) {
        String templetPath = String.valueOf(CLApplication.app.getTemplateXmlPath()) + File.separator + templet;
        if (!pdfName.contains(".pdf")) {
            pdfName = String.valueOf(pdfName) + ".pdf";
        }
        String reportPath = String.valueOf(CLApplication.app.getReportPath()) + File.separator + pdfName;
        if (!new File(templetPath).exists()) {
            return "-1";
        }
        boolean result = false;
        try {
            XmlModel tmpModel = new ExpleanXml().getElement(templetPath);
            setBaseFont();
            String tableName = new TestItemDao(this.m_Context).getTableName();
            this.VectorType = ClouData.getInstance().getSettingBasic().VectorType;
            if (datas.Datas.containsKey(tableName)) {
                this.AngleDefinition = OtherUtils.parseInt(datas.Datas.get(tableName).get(0).DataMap.get("AngleDefinition"));
            }
            FileOutputStream fos = new FileOutputStream(reportPath);
            Document doc = new Document(PageSize.A4, IMAGESCALE, IMAGESCALE, 30.0f, 30.0f);
            PdfWriter writer = PdfWriter.getInstance(doc, fos);
            doc.open();
            doc.newPage();
            if (((XmlModel) tmpModel.Datas.get(0)).Type.equals("Templet")) {
                tmpModel = (XmlModel) tmpModel.Datas.get(0);
            }
            for (int i = 0; i < tmpModel.Datas.size(); i++) {
                if (((XmlModel) tmpModel.Datas.get(i)).Type.equals("HeaderFooter")) {
                    setHeaderFooter(writer, doc, (XmlModel) tmpModel.Datas.get(i), templetPath, datas);
                    this.hasHeaderFooter = true;
                } else {
                    getCell(doc, writer, (XmlModel) tmpModel.Datas.get(i), datas);
                }
            }
            int pageCount = writer.getPageNumber();
            doc.close();
            fos.flush();
            fos.close();
            if (this.hasHeaderFooter) {
                PdfReader reader = new PdfReader(reportPath);
                int pageCount2 = pageCount - 2;
                String pages = PdfObject.NOTHING;
                for (int i2 = 1; i2 <= pageCount2; i2++) {
                    pages = String.valueOf(pages) + "," + (i2 + 2);
                }
                reader.selectPages(pages.substring(1));
                String tmpPDFPath = String.valueOf(CLApplication.app.getReportPath()) + File.separator + "TmpPDF.pdf";
                new PdfStamper(reader, new FileOutputStream(tmpPDFPath)).close();
                reader.close();
                File tmpPDF = new File(reportPath);
                if (tmpPDF.exists()) {
                    tmpPDF.delete();
                }
                new File(tmpPDFPath).renameTo(new File(reportPath));
            }
            File tmpImg = new File(this.tmpPicturePath);
            if (tmpImg.exists()) {
                tmpImg.delete();
            }
            result = true;
        } catch (FileNotFoundException e1) {
            e1.printStackTrace();
        } catch (DocumentException e) {
            e.printStackTrace();
        } catch (IOException e2) {
            e2.printStackTrace();
        }
        if (!result) {
            return PdfObject.NOTHING;
        }
        MediaScannerConnection.scanFile(this.m_Context, new String[]{CLApplication.app.getReportPath()}, null, new MediaScannerConnection.OnScanCompletedListener() {
            /* class com.clou.rs350.report.ExportReport.AnonymousClass1 */

            public void onScanCompleted(String path, Uri uri) {
            }
        });
        return reportPath;
    }
}
