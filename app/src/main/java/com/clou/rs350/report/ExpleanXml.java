package com.clou.rs350.report;

import android.util.Xml;
import com.clou.rs350.utils.OtherUtils;
import com.itextpdf.text.xml.xmp.XmpWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

public class ExpleanXml {
    private XmlPullParser getXmlPullParser(String path) {
        FileInputStream fis = null;
        try {
            fis = new FileInputStream(new File(path));
        } catch (FileNotFoundException e1) {
            e1.printStackTrace();
        }
        XmlPullParser parser = Xml.newPullParser();
        try {
            parser.setInput(fis, XmpWriter.UTF8);
        } catch (XmlPullParserException e) {
            e.printStackTrace();
        }
        return parser;
    }

    public XmlModel getElement(String path) {
        return getElement(getXmlPullParser(path));
    }

    public XmlModel getElement(XmlPullParser parser) {
        XmlModel templet = new XmlModel();
        try {
            int eventType = parser.getEventType();
            while (eventType != 1) {
                switch (eventType) {
                    case 2:
                        templet.Datas.add(getElement(parser, parser.getName()));
                        break;
                    case 4:
                        templet.Datas.add(removeReturn(parser.getText().trim()).trim());
                        break;
                }
                eventType = parser.next();
            }
        } catch (XmlPullParserException e) {
            e.printStackTrace();
        } catch (IOException e2) {
            e2.printStackTrace();
        }
        return templet;
    }

    private String removeReturn(String value) {
        while (true) {
            if (!value.startsWith("\r") && !value.startsWith("\n")) {
                break;
            }
            value = value.substring(1);
        }
        while (true) {
            if (!value.endsWith("\r") && !value.endsWith("\n")) {
                return value;
            }
            value = value.substring(0, value.length() - 1);
        }
    }

    private XmlModel getElement(XmlPullParser parser, String elementName) {
        XmlModel element = new XmlModel();
        element.Type = elementName;
        for (int i = 0; i < parser.getAttributeCount(); i++) {
            element.Condition.put(parser.getAttributeName(i), parser.getAttributeValue(i));
        }
        while (true) {
            try {
                int eventType = parser.next();
                if (eventType != 3 || !parser.getName().equals(elementName)) {
                    switch (eventType) {
                        case 2:
                            element.Datas.add(getElement(parser, parser.getName()));
                            break;
                        case 4:
                            String tmpText = removeReturn(parser.getText().trim()).trim();
                            if (!OtherUtils.isEmpty(tmpText)) {
                                element.Datas.add(tmpText);
                                break;
                            } else {
                                break;
                            }
                    }
                } else {
                    return element;
                }
            } catch (XmlPullParserException e) {
                e.printStackTrace();
            } catch (IOException e2) {
                e2.printStackTrace();
            }
        }
    }
}
