package com.clou.rs350.utils;

import android.util.Log;

public class CountDown {
    private static final String TAG = "CountDown";
    private long time = 0;

    public void start(String msg) {
        Log.i(TAG, String.valueOf(msg) + " start....");
        this.time = System.currentTimeMillis();
    }

    public void stop(String msg) {
        Log.i(TAG, String.valueOf(msg) + " stop...used time:" + (System.currentTimeMillis() - this.time));
    }
}
