package com.clou.rs350.utils;

import android.graphics.Color;

public class ColorUtils {
    public static int[] decodeColor(int color) {
        return new int[]{(color >> 24) & 255, (color >> 16) & 255, (color >> 8) & 255, color & 255};
    }

    public static int changeColorAlpha(int color, int alpha) {
        int[] argb = decodeColor(color);
        return Color.argb(alpha, argb[1], argb[2], argb[3]);
    }

    public static int addColor(int color, int step) {
        int[] argb = decodeColor(color);
        int r = argb[1] + step;
        int g = argb[2] + step;
        int b = argb[3] + step;
        if (r >= 255) {
            r = 255;
        }
        if (g >= 255) {
            g = 255;
        }
        if (b >= 255) {
            b = 255;
        }
        return Color.argb(argb[0], r, g, b);
    }
}
