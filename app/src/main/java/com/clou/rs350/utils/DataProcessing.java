package com.clou.rs350.utils;

import com.clou.rs350.db.model.DBDataModel;
import com.itextpdf.text.pdf.PdfObject;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.regex.Pattern;

public class DataProcessing {
    public static String RoundValue(Double Value) {
        DecimalFormat df = null;
        int dian = new StringBuilder(String.valueOf(Math.abs(Value.doubleValue()))).toString().indexOf(".");
        if (dian == 1) {
            df = new DecimalFormat("0.00000");
        } else if (dian == 2) {
            df = new DecimalFormat("0.0000");
        } else if (dian == 3) {
            df = new DecimalFormat("0.000");
        } else if (dian == 4) {
            df = new DecimalFormat("0.00");
        } else if (dian == 5) {
            df = new DecimalFormat("0.0");
        } else if (dian == 6) {
            df = new DecimalFormat("000000");
        }
        DecimalFormatSymbols symbols = new DecimalFormatSymbols();
        symbols.setDecimalSeparator('.');
        df.setDecimalFormatSymbols(symbols);
        return df.format(Value);
    }

    public static Double RoundValueDouble(Double Value, int pointIndex) {
        DecimalFormat df = null;
        if (pointIndex == 5) {
            df = new DecimalFormat("0.00000");
        } else if (pointIndex == 4) {
            df = new DecimalFormat("0.0000");
        } else if (pointIndex == 3) {
            df = new DecimalFormat("0.000");
        } else if (pointIndex == 2) {
            df = new DecimalFormat("0.00");
        } else if (pointIndex == 1) {
            df = new DecimalFormat("0.0");
        } else if (pointIndex == 0) {
            df = new DecimalFormat("000000");
        }
        DecimalFormatSymbols symbols = new DecimalFormatSymbols();
        symbols.setDecimalSeparator('.');
        df.setDecimalFormatSymbols(symbols);
        return Double.valueOf(Double.parseDouble(df.format(Value)));
    }

    public static String RoundValueString(Double Value, int pointIndex) {
        DecimalFormat df = null;
        if (pointIndex == 5) {
            df = new DecimalFormat("0.00000");
        } else if (pointIndex == 4) {
            df = new DecimalFormat("0.0000");
        } else if (pointIndex == 3) {
            df = new DecimalFormat("0.000");
        } else if (pointIndex == 2) {
            df = new DecimalFormat("0.00");
        } else if (pointIndex == 1) {
            df = new DecimalFormat("0.0");
        } else if (pointIndex == 0) {
            df = new DecimalFormat("000000");
        }
        DecimalFormatSymbols symbols = new DecimalFormatSymbols();
        symbols.setDecimalSeparator('.');
        df.setDecimalFormatSymbols(symbols);
        return df.format(Value);
    }

    public static DBDataModel parseData(String Value, String Unit) {
        if (OtherUtils.isEmpty(Value) || OtherUtils.isEmpty(Unit)) {
            return new DBDataModel();
        }
        double multiple = 1.0d;
        String tmpValue = Value.replace(Unit, PdfObject.NOTHING);
        if (OtherUtils.isEmpty(tmpValue)) {
            return new DBDataModel();
        }
        if (tmpValue.contains("M")) {
            multiple = 1000000.0d;
            tmpValue = tmpValue.replace("M", PdfObject.NOTHING);
        }
        if (tmpValue.contains("k")) {
            multiple = 1000.0d;
            tmpValue = tmpValue.replace("k", PdfObject.NOTHING);
        }
        if (tmpValue.contains("m")) {
            multiple = 0.001d;
            tmpValue = tmpValue.replace("m", PdfObject.NOTHING);
        }
        return new DBDataModel(OtherUtils.parseDouble(tmpValue) * multiple, Value);
    }

    public static Double[] parseDouble(String data, String strSplit) {
        if (OtherUtils.isEmpty(data) || OtherUtils.isEmpty(strSplit)) {
            return null;
        }
        String[] stringArray = data.split(strSplit);
        Double[] dataArray = new Double[stringArray.length];
        for (int i = 0; i < stringArray.length; i++) {
            dataArray[i] = Double.valueOf(OtherUtils.parseDouble(stringArray[i]));
        }
        return dataArray;
    }

    public static boolean isNumeric(String str) {
        if (!Pattern.compile("^[-+]?[0-9]+(.[0-9]+)?$").matcher(str).matches()) {
            return false;
        }
        return true;
    }
}
