package com.clou.rs350.utils;

import com.clou.rs350.constants.Constants;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class TimeTag {
    public static final int TIMEUNIT_DAY = 3;
    public static final int TIMEUNIT_HOUR = 2;
    public static final int TIMEUNIT_MINUTE = 1;
    public static final int TIMEUNIT_MONTH = 5;
    public static final int TIMEUNIT_SECOND = 0;
    public static final int TIMEUNIT_WEEK = 4;
    public static final int TIMEUNIT_YEAR = 6;
    private Calendar cal = Calendar.getInstance();
    public int day = 0;
    public int hour = 0;
    public int minute = 0;
    public int month = 0;
    public int second = 0;
    public int year = 0;

    public TimeTag() {
    }

    public TimeTag(long millis) {
        setTime(millis);
    }

    public TimeTag(int year2, int month2, int day2, int hour2, int minute2, int second2) {
        this.year = year2;
        this.month = month2;
        this.day = day2;
        this.hour = hour2;
        this.minute = minute2;
        this.second = second2;
        setCalendar();
    }

    public TimeTag(Calendar cal2) {
        setTime(cal2.getTimeInMillis());
    }

    public static Calendar strAllFieldCalendar(String strTime) {
        SimpleDateFormat sdf = new SimpleDateFormat(Constants.TIMEFORMAT);
        new Date();
        try {
            Date date = sdf.parse(strTime);
            Calendar cal2 = Calendar.getInstance();
            cal2.setTime(date);
            return cal2;
        } catch (ParseException e) {
            return null;
        }
    }

    public static Calendar strTimeFieldCalendar(String strTime) {
        Calendar cal2 = Calendar.getInstance();
        return strAllFieldCalendar(String.valueOf(String.valueOf(cal2.get(1)) + "-" + cal2.get(2) + "-" + cal2.get(5) + " ") + strTime);
    }

    public static Calendar setMinCalendar(Calendar cal2) {
        cal2.set(11, 0);
        cal2.set(12, 0);
        cal2.set(13, 0);
        cal2.set(14, 0);
        return cal2;
    }

    public static Calendar setMaxCalendar(Calendar cal2) {
        cal2.set(11, 23);
        cal2.set(12, 59);
        cal2.set(13, 59);
        return cal2;
    }

    public void setTime(int year2, int month2, int day2, int hour2, int minute2) {
        if (year2 < 2000) {
            this.year = year2 + 2000;
        } else {
            this.year = year2;
        }
        this.month = month2;
        this.day = day2;
        this.hour = hour2;
        this.minute = minute2;
        this.second = 0;
        setCalendar();
    }

    public void setTime(long millis) {
        this.cal.setTimeInMillis(millis);
        this.year = this.cal.get(1);
        this.month = this.cal.get(2) + 1;
        this.day = this.cal.get(5);
        this.hour = this.cal.get(11);
        this.minute = this.cal.get(12);
        this.second = this.cal.get(13);
    }

    public void setCalendar() {
        this.cal.set(1, this.year);
        this.cal.set(2, this.month - 1);
        this.cal.set(5, this.day);
        this.cal.set(11, this.hour);
        this.cal.set(12, this.minute);
        this.cal.set(13, this.second);
        this.cal.set(14, 0);
    }

    public void add(int field, int amount) {
        switch (field) {
            case 0:
                this.cal.add(13, amount);
                break;
            case 1:
                this.cal.add(12, amount);
                break;
            case 2:
                this.cal.add(11, amount);
                break;
            case 3:
                this.cal.add(5, amount);
                break;
            case 4:
                this.cal.add(5, amount * 7);
                break;
            case 5:
                this.cal.add(2, amount);
                break;
            case 6:
                this.cal.add(1, amount);
                break;
        }
        setTime(this.cal.getTimeInMillis());
    }

    public String toString() {
        int dbYear = this.year;
        if (dbYear < 1000) {
            dbYear += 2000;
        }
        String yearStr = String.valueOf(this.year);
        String monthStr = String.valueOf(this.month);
        String dayStr = String.valueOf(this.day);
        String hourStr = String.valueOf(this.hour);
        String minuteStr = String.valueOf(this.minute);
        String secondStr = String.valueOf(this.second);
        if (yearStr.length() < 2) {
            String yearStr2 = "0" + yearStr;
        }
        if (monthStr.length() < 2) {
            monthStr = "0" + monthStr;
        }
        if (dayStr.length() < 2) {
            dayStr = "0" + dayStr;
        }
        if (hourStr.length() < 2) {
            hourStr = "0" + hourStr;
        }
        if (minuteStr.length() < 2) {
            minuteStr = "0" + minuteStr;
        }
        if (secondStr.length() < 2) {
            secondStr = "0" + secondStr;
        }
        return new StringBuffer().append(dbYear).append("-").append(monthStr).append("-").append(dayStr).append(" ").append(hourStr).append(":").append(minuteStr).append(":").append(secondStr).toString();
    }

    public String toDateString() {
        String year2 = new StringBuilder().append(this.year).toString();
        String month2 = this.month < 10 ? "0" + this.month : new StringBuilder().append(this.month).toString();
        String day2 = this.day < 10 ? "0" + this.day : new StringBuilder().append(this.day).toString();
        StringBuffer sb = new StringBuffer().append(year2);
        sb.append("-").append(month2).append("-").append(day2);
        return sb.toString();
    }

    public int getYear() {
        return this.year;
    }

    public void setYear(int year2) {
        this.year = year2;
        setCalendar();
    }

    public int getMonth() {
        return this.month;
    }

    public void setMonth(int month2) {
        this.month = month2;
        setCalendar();
    }

    public int getDay() {
        return this.day;
    }

    public void setDay(int day2) {
        this.day = day2;
        setCalendar();
    }

    public int getHour() {
        return this.hour;
    }

    public void setHour(int hour2) {
        this.hour = hour2;
        setCalendar();
    }

    public int getMinute() {
        return this.minute;
    }

    public void setMinute(int minute2) {
        this.minute = minute2;
        setCalendar();
    }

    public int getSecond() {
        return this.second;
    }

    public void setSecond(int second2) {
        this.second = second2;
        setCalendar();
    }

    public Calendar getCalendar() {
        Calendar cal2 = Calendar.getInstance();
        cal2.setTimeInMillis(this.cal.getTimeInMillis());
        return cal2;
    }

    public TimeTag clone() {
        return new TimeTag(this.cal.getTimeInMillis());
    }

    public String toDbString() {
        int dbYear = this.year;
        if (this.year < 1000) {
            dbYear += 2000;
        }
        NumberFormat f = NumberFormat.getInstance();
        f.setMinimumIntegerDigits(2);
        return String.valueOf(dbYear) + "-" + f.format((long) this.month) + "-" + f.format((long) this.day) + " " + f.format((long) this.hour) + ":" + f.format((long) this.minute) + ":" + f.format((long) this.second);
    }
}
