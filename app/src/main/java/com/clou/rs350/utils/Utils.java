package com.clou.rs350.utils;

import com.itextpdf.text.pdf.PdfObject;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

public class Utils {
    private static final TimeTag NOWTIME = new TimeTag(System.currentTimeMillis());

    public static void sleep(long millis) {
        try {
            Thread.sleep(millis);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public static TimeTag getNowTime() {
        NOWTIME.setTime(System.currentTimeMillis());
        return NOWTIME;
    }

    public static <T extends Number> T getNumFromStr(String data, Class<T> cls) {
        try {
            Method m = cls.getMethod("valueOf", String.class);
            if (m != null) {
                return (T) ((Number) m.invoke(null, data));
            }
        } catch (Exception e) {
        }
        return null;
    }

    public static boolean equalsDeclaredField(Object a, Object b) {
        if (a == null && a == b) {
            return true;
        }
        if (a == null || b == null) {
            return false;
        }
        if (a.getClass() != b.getClass()) {
            return false;
        }
        Field[] flds = a.getClass().getDeclaredFields();
        for (Field f : flds) {
            f.setAccessible(true);
            try {
                Object vala = f.get(a);
                Object valb = f.get(b);
                if (vala != null) {
                    if (!vala.equals(valb)) {
                        return false;
                    }
                } else if (vala != valb) {
                    return false;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return true;
    }

    public static boolean checkIsNum(String... strs) {
        for (String str : strs) {
            if (str == null || PdfObject.NOTHING.equals(str.trim())) {
                return false;
            }
            for (char c : str.toCharArray()) {
                if ("0123456789".indexOf(c) < 0) {
                    return false;
                }
            }
        }
        return true;
    }

    public static byte[] joinBytes(byte[]... frms) {
        int pos = 0;
        int len = 0;
        for (byte[] f : frms) {
            len += f.length;
        }
        byte[] frame = new byte[len];
        for (byte[] f2 : frms) {
            System.arraycopy(f2, 0, frame, pos, f2.length);
            pos += f2.length;
        }
        return frame;
    }

    public static byte[] joinBytes1(byte[]... bs) {
        if (bs == null || bs.length <= 0) {
            return null;
        }
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        for (byte[] b : bs) {
            try {
                bos.write(b);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return bos.toByteArray();
    }

    public static boolean checkIpFormat(String ip) {
        if (ip == null) {
            return false;
        }
        return ip.matches("^(1\\d{2}|2[0-4]\\d|25[0-5]|[1-9]\\d|[1-9])\\.(1\\d{2}|2[0-4]\\d|25[0-5]|[1-9]\\d|\\d)\\.(1\\d{2}|2[0-4]\\d|25[0-5]|[1-9]\\d|\\d)\\.(1\\d{2}|2[0-4]\\d|25[0-5]|[1-9]\\d|\\d)$");
    }

    public static Double caculateString(String calc, Double... numbers) {
        int numIdx;
        char[] chs = calc.toCharArray();
        boolean hasVar = false;
        String cons = PdfObject.NOTHING;
        List<Object> calst = new ArrayList<>();
        int i = 0;
        int numIdx2 = 0;
        while (i < chs.length) {
            try {
                char c = chs[i];
                if (checkIsNum(new StringBuilder(String.valueOf(c)).toString()) || c == '+' || c == '-' || c == '*' || c == '/' || c == '.' || c == '(' || c == ')') {
                    if (hasVar) {
                        numIdx = numIdx2 + 1;
                        calst.add(numbers[numIdx2]);
                        hasVar = false;
                    } else {
                        numIdx = numIdx2;
                    }
                    if (checkIsNum(new StringBuilder(String.valueOf(c)).toString()) || c == '.') {
                        cons = String.valueOf(cons) + c;
                        if (i == chs.length - 1 && !PdfObject.NOTHING.equals(cons)) {
                            calst.add(getNumFromStr(cons, Double.class));
                        }
                    } else {
                        if (!PdfObject.NOTHING.equals(cons)) {
                            calst.add(getNumFromStr(cons, Double.class));
                            cons = PdfObject.NOTHING;
                        }
                        calst.add(new StringBuilder(String.valueOf(c)).toString());
                    }
                } else {
                    hasVar = true;
                    if (i == chs.length - 1) {
                        numIdx = numIdx2 + 1;
                        try {
                            calst.add(numbers[numIdx2]);
                        } catch (Exception e) {
                            e.printStackTrace();
                            return null;
                        }
                    } else {
                        numIdx = numIdx2;
                    }
                }
                i++;
                numIdx2 = numIdx;
            } catch (Exception e2) {
                e2.printStackTrace();
                return null;
            }
        }
        List<Object> res = calst;
        System.currentTimeMillis();
        do {
            res = calculate(res);
        } while (res.indexOf("(") > -1);
        if (res.size() > 1) {
            res = calculate(res);
        }
        if (res.isEmpty() || res.size() != 1) {
            return null;
        }
        return (Double) res.get(0);
    }

    public static void main(String[] args) {
    }

    private static List<Object> calculate(List<Object> input) {
        int leftC = -1;
        int rightC = input.indexOf(")");
        for (int i = 0; i < input.size(); i++) {
            if ("(".equals(input.get(i)) && i < rightC) {
                leftC = i;
            } else if (i > rightC) {
                break;
            }
        }
        List<Object> head = null;
        List<Object> tail = null;
        if (leftC > -1 && rightC > -1) {
            head = input.subList(0, leftC);
            tail = input.subList(rightC + 1, input.size());
            input = new ArrayList<>(input.subList(leftC + 1, rightC));
        }
        while (true) {
            if (input.indexOf("*") <= -1 && input.indexOf("/") <= -1) {
                break;
            }
            int firMul = input.indexOf("*");
            int firDiv = input.indexOf("/");
            boolean isMul = false;
            if (firMul <= -1 || firDiv <= -1) {
                if (firMul > -1) {
                    isMul = true;
                }
            } else if (firMul < firDiv) {
                isMul = true;
            }
            if (isMul) {
                input.set(firMul, Double.valueOf(((Double) input.get(firMul - 1)).doubleValue() * ((Double) input.get(firMul + 1)).doubleValue()));
                input.remove(firMul + 1);
                input.remove(firMul - 1);
            } else {
                input.set(firDiv, Double.valueOf(((Double) input.get(firDiv - 1)).doubleValue() / ((Double) input.get(firDiv + 1)).doubleValue()));
                input.remove(firDiv + 1);
                input.remove(firDiv - 1);
            }
        }
        while (true) {
            if (input.indexOf("+") <= -1 && input.indexOf("-") <= -1) {
                break;
            }
            int firPlus = input.indexOf("+");
            int firMin = input.indexOf("-");
            boolean isPlus = false;
            if (firPlus <= -1 || firMin <= -1) {
                if (firPlus > -1) {
                    isPlus = true;
                }
            } else if (firPlus < firMin) {
                isPlus = true;
            }
            if (isPlus) {
                input.set(firPlus, Double.valueOf(((Double) input.get(firPlus - 1)).doubleValue() + ((Double) input.get(firPlus + 1)).doubleValue()));
                input.remove(firPlus + 1);
                input.remove(firPlus - 1);
            } else {
                Double num2 = (Double) input.get(firMin + 1);
                if (firMin == 0) {
                    input.set(firMin, Double.valueOf(-num2.doubleValue()));
                    input.remove(firMin + 1);
                } else {
                    Object num1 = input.get(firMin - 1);
                    if (num1 instanceof Double) {
                        input.set(firMin, Double.valueOf(((Double) num1).doubleValue() - num2.doubleValue()));
                        input.remove(firMin + 1);
                        input.remove(firMin - 1);
                    } else {
                        input.set(firMin, Double.valueOf(-num2.doubleValue()));
                        input.remove(firMin + 1);
                    }
                }
            }
        }
        List<Object> result = new ArrayList<>();
        if (head != null) {
            result.addAll(head);
        }
        result.addAll(input);
        if (tail != null) {
            result.addAll(tail);
        }
        return result;
    }

    public static TimeTag getCurrentTime() {
        return new TimeTag(System.currentTimeMillis());
    }
}
