package com.clou.rs350.utils;

import android.content.Context;
import com.clou.rs350.db.model.DBDataModel;
import com.clou.rs350.db.model.SiteData;
import com.clou.rs350.manager.MessageManager;
import com.itextpdf.text.pdf.PdfObject;

public class InfoUtil {
    public static DBDataModel parseVoltageRatio(String strRatio, boolean isPrimary) {
        String tmpString;
        if (OtherUtils.isEmpty(strRatio)) {
            return new DBDataModel(1L, PdfObject.NOTHING);
        }
        if (strRatio.contains("/")) {
            tmpString = strRatio.substring(0, strRatio.length() - (isPrimary ? 5 : 4));
        } else {
            tmpString = strRatio.substring(0, strRatio.length() - (isPrimary ? 2 : 1));
        }
        return new DBDataModel(OtherUtils.parseDouble(tmpString), strRatio);
    }

    public static DBDataModel parseCurrentRatio(String strRatio) {
        if (OtherUtils.isEmpty(strRatio)) {
            return new DBDataModel(1L, PdfObject.NOTHING);
        }
        return new DBDataModel(OtherUtils.parseDouble(strRatio.substring(0, strRatio.length() - 1)), strRatio);
    }

    public static void setRatio(SiteData Info, Context context) {
        double PTRatio = 1.0d;
        double CTRatio = 1.0d;
        if (Info.PTEnable == 0 && !OtherUtils.isEmpty(Info.PTPrimary.s_Value) && !OtherUtils.isEmpty(Info.PTSecondary.s_Value)) {
            PTRatio = (Info.PTPrimary.d_Value * 1000.0d) / Info.PTSecondary.d_Value;
        }
        if (Info.CTEnable == 0 && !OtherUtils.isEmpty(Info.CTPrimary.s_Value) && !OtherUtils.isEmpty(Info.CTSecondary.s_Value)) {
            CTRatio = Info.CTPrimary.d_Value / Info.CTSecondary.d_Value;
        }
        if (1 == Info.Measurement_Position) {
            PTRatio = 1.0d / PTRatio;
            CTRatio = 1.0d / CTRatio;
        }
        MessageManager.getInstance(context).SetRatio(PTRatio, CTRatio, Info.RatioApply);
    }

    public static DBDataModel parseBurden(String strBurden) {
        if (OtherUtils.isEmpty(strBurden)) {
            return new DBDataModel();
        }
        return new DBDataModel(OtherUtils.parseDouble(strBurden.replace("VA", PdfObject.NOTHING)), strBurden);
    }
}
