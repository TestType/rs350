package com.clou.rs350.utils;

import android.content.Context;
import android.media.MediaPlayer;
import android.media.SoundPool;

public class MediaUtil {
    public static void playSound(final int soundId, Context context) {
        final SoundPool sundpool = new SoundPool(16, 3, 0);
        sundpool.setOnLoadCompleteListener(new SoundPool.OnLoadCompleteListener() {
            /* class com.clou.rs350.utils.MediaUtil.AnonymousClass1 */

            public void onLoadComplete(SoundPool soundPool, int sampleId, int status) {
                sundpool.play(soundId, 1.0f, 1.0f, 0, 0, 1.0f);
            }
        });
        sundpool.load(context, soundId, 1);
    }

    public static void playMusic(int resSoundId, Context context) {
        MediaPlayer mediaPlayer = MediaPlayer.create(context, resSoundId);
        mediaPlayer.setLooping(false);
        mediaPlayer.seekTo(0);
        mediaPlayer.setVolume(0.5f, 0.5f);
        mediaPlayer.start();
    }
}
