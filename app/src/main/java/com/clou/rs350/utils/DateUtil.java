package com.clou.rs350.utils;

import android.annotation.SuppressLint;
import android.util.Log;
import com.clou.rs350.constants.Constants;
import com.itextpdf.text.html.HtmlTags;
import com.itextpdf.text.pdf.PdfObject;
import java.text.ParseException;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;
import java.util.Random;

@SuppressLint({"SimpleDateFormat"})
public class DateUtil {
    public static Date getNowDate() {
        Date currentTime = new Date();
        SimpleDateFormat formatter = new SimpleDateFormat(Constants.TIMEFORMAT);
        return formatter.parse(formatter.format(currentTime), new ParsePosition(8));
    }

    public static Date getNowDateShort() {
        Date currentTime = new Date();
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        return formatter.parse(formatter.format(currentTime), new ParsePosition(8));
    }

    public static String getStringDate() {
        return new SimpleDateFormat(Constants.TIMEFORMAT).format(new Date());
    }

    public static String getStringDate_Ms() {
        return new SimpleDateFormat("yyyy-MM-dd kk:mm:ss:ms").format(new Date());
    }

    public static String getStringDateShort() {
        return new SimpleDateFormat("yyyy-MM-dd").format(new Date());
    }

    public static String getTimeShort() {
        return new SimpleDateFormat("kk:mm:ss").format(new Date());
    }

    public static String getTimeShort(Date date) {
        return new SimpleDateFormat("kk:mm:ss").format(date);
    }

    public static Date strToDateLong(String strDate) {
        return new SimpleDateFormat(Constants.TIMEFORMAT).parse(strDate, new ParsePosition(0));
    }

    public static String dateToStrLong(Date dateDate) {
        return new SimpleDateFormat(Constants.TIMEFORMAT).format(dateDate);
    }

    public static String dateToStr(Date dateDate) {
        return new SimpleDateFormat("yyyy-MM-dd").format(dateDate);
    }

    public static Date strToDate(String strDate) {
        return new SimpleDateFormat("yyyy-MM-dd").parse(strDate, new ParsePosition(0));
    }

    public static Date getNow() {
        return new Date();
    }

    public static Date getLastDate(long day) {
        return new Date(new Date().getTime() - (86400000 * day));
    }

    public static String getStringToday() {
        return new SimpleDateFormat("yyyyMMdd HHmmss").format(new Date());
    }

    public static String getHour() {
        return new SimpleDateFormat(Constants.TIMEFORMAT).format(new Date()).substring(11, 13);
    }

    public static String getTime() {
        return new SimpleDateFormat(Constants.TIMEFORMAT).format(new Date()).substring(14, 16);
    }

    public static String getUserDate(String sformat) {
        return new SimpleDateFormat(sformat).format(new Date());
    }

    public static String getTwoHour(String st1, String st2) {
        String[] kk = st1.split(":");
        String[] jj = st2.split(":");
        double y = Double.parseDouble(kk[0]) + (Double.parseDouble(kk[1]) / 60.0d);
        double u = Double.parseDouble(jj[0]) + (Double.parseDouble(jj[1]) / 60.0d);
        if (y - u > 0.0d) {
            return new StringBuilder(String.valueOf((y - u) * 60.0d)).toString();
        }
        return new StringBuilder(String.valueOf((y - u) * 60.0d * -1.0d)).toString();
    }

    public static String getTwoDay(String sj1, String sj2) {
        SimpleDateFormat myFormatter = new SimpleDateFormat("yyyy-MM-dd");
        try {
            return new StringBuilder(String.valueOf((myFormatter.parse(sj1).getTime() - myFormatter.parse(sj2).getTime()) / 86400000)).toString();
        } catch (Exception e) {
            return PdfObject.NOTHING;
        }
    }

    public static String getPreTime(String sj1, String jj) {
        SimpleDateFormat format = new SimpleDateFormat(Constants.TIMEFORMAT);
        try {
            Date date1 = format.parse(sj1);
            date1.setTime(((date1.getTime() / 1000) + ((long) (Integer.parseInt(jj) * 60))) * 1000);
            return format.format(date1);
        } catch (Exception e) {
            return PdfObject.NOTHING;
        }
    }

    public static String getNextDay(String nowdate, int delay) {
        try {
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
            Date d = strToDate(nowdate);
            d.setTime(((d.getTime() / 1000) + ((long) (delay * 24 * 60 * 60))) * 1000);
            return format.format(d);
        } catch (Exception e) {
            return PdfObject.NOTHING;
        }
    }

    public static String getMonthAndDay(String nowdate) {
        try {
            new SimpleDateFormat("yyyy-MM-dd");
            Date d = strToDate(nowdate);
            return String.valueOf(d.getMonth() + 1) + "月" + d.getDate() + "日";
        } catch (Exception e) {
            return PdfObject.NOTHING;
        }
    }

    public static String getYearMonthDay(String nowdate) {
        try {
            new SimpleDateFormat("yyyy-MM-dd");
            Date d = strToDate(nowdate);
            return String.valueOf(d.getYear()) + "年" + (d.getMonth() + 1) + "月" + d.getDate() + "日";
        } catch (Exception e) {
            return PdfObject.NOTHING;
        }
    }

    public static boolean isLeapYear(String ddate) {
        Date d = strToDate(ddate);
        GregorianCalendar gc = (GregorianCalendar) Calendar.getInstance();
        gc.setTime(d);
        int year = gc.get(1);
        if (year % Constants.DEFAULT_SOCKET_TIMEOUT == 0) {
            return true;
        }
        if (year % 4 == 0) {
            return year % 100 != 0;
        }
        return false;
    }

    public static String getEDate(String str) {
        String[] k = new SimpleDateFormat("yyyy-MM-dd").parse(str, new ParsePosition(0)).toString().split(" ");
        return String.valueOf(k[2]) + k[1].toUpperCase() + k[5].substring(2, 4);
    }

    public static String getEndDateOfMonth(String dat) {
        String str = dat.substring(0, 8);
        int mon = Integer.parseInt(dat.substring(5, 7));
        if (mon == 1 || mon == 3 || mon == 5 || mon == 7 || mon == 8 || mon == 10 || mon == 12) {
            return String.valueOf(str) + "31";
        }
        if (mon == 4 || mon == 6 || mon == 9 || mon == 11) {
            return String.valueOf(str) + "30";
        }
        if (isLeapYear(dat)) {
            return String.valueOf(str) + "29";
        }
        return String.valueOf(str) + "28";
    }

    /* JADX WARNING: Removed duplicated region for block: B:9:0x003c A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static boolean isSameWeekDates(Date date1, Date date2) {
        Calendar cal1 = Calendar.getInstance();
        Calendar cal2 = Calendar.getInstance();
        cal1.setTime(date1);
        cal2.setTime(date2);
        int subYear = cal1.get(1) - cal2.get(1);
        if (subYear == 0) {
            if (cal1.get(3) == cal2.get(3)) {
                return true;
            }
        } else if (1 == subYear && 11 == cal2.get(2)) {
            if (cal1.get(3) == cal2.get(3)) {
                return true;
            }
        } else if (-1 == subYear && 11 == cal1.get(2) && cal1.get(3) == cal2.get(3)) {
            return true;
        } else {
            return false;
        }
        return false;
    }


    public static String getSeqWeek() {
        Calendar c = Calendar.getInstance(Locale.CHINA);
        String week = Integer.toString(c.get(3));
        if (week.length() == 1) {
            week = "0" + week;
        }
        return c.get(1) + week;
    }

    public static String getWeek(String sdate, String num) {
        Date dd = strToDate(sdate);
        Calendar c = Calendar.getInstance();
        c.setTime(dd);
        if (num.equals("1")) {
            c.set(7, 2);
        } else if (num.equals("2")) {
            c.set(7, 3);
        } else if (num.equals("3")) {
            c.set(7, 4);
        } else if (num.equals("4")) {
            c.set(7, 5);
        } else if (num.equals("5")) {
            c.set(7, 6);
        } else if (num.equals("6")) {
            c.set(7, 7);
        } else if (num.equals("0")) {
            c.set(7, 1);
        }
        return new SimpleDateFormat("yyyy-MM-dd").format(c.getTime());
    }

    public static String getWeek(String sdate) {
        Date date = strToDate(sdate);
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        return new SimpleDateFormat("EEEE").format(c.getTime());
    }

    public static String getWeekStr(String sdate) {
        String str = getWeek(sdate);
        if ("1".equals(str)) {
            return "星期日";
        }
        if ("2".equals(str)) {
            return "星期一";
        }
        if ("3".equals(str)) {
            return "星期二";
        }
        if ("4".equals(str)) {
            return "星期三";
        }
        if ("5".equals(str)) {
            return "星期四";
        }
        if ("6".equals(str)) {
            return "星期五";
        }
        if ("7".equals(str)) {
            return "星期六";
        }
        return str;
    }

    public static long getDays(String date1, String date2) {
        if (date1 == null || date1.equals(PdfObject.NOTHING) || date2 == null || date2.equals(PdfObject.NOTHING)) {
            return 0;
        }
        SimpleDateFormat myFormatter = new SimpleDateFormat("yyyy-MM-dd");
        Date date = null;
        Date mydate = null;
        try {
            date = myFormatter.parse(date1);
            mydate = myFormatter.parse(date2);
        } catch (Exception e) {
        }
        return (date.getTime() - mydate.getTime()) / 86400000;
    }

    public static String getTimeDifferential(String date1, String date2) {
        if (date1 == null || date1.equals(PdfObject.NOTHING) || date2 == null || date2.equals(PdfObject.NOTHING)) {
            return "0s";
        }
        SimpleDateFormat myFormatter = new SimpleDateFormat(Constants.TIMEFORMAT);
        try {
            double seconds = (double) ((myFormatter.parse(date1).getTime() - myFormatter.parse(date2).getTime()) / 1000);
            double hou = Math.floor(seconds / 3600.0d);
            return String.valueOf(hou) + "h" + Math.floor((seconds / 60.0d) % 60.0d) + "m" + (seconds % 3600.0d) + HtmlTags.S;
        } catch (Exception e) {
            return "0s";
        }
    }

    public static String getTimeDifferential(String date1) {
        String date2 = getStringDate();
        if (date1 == null || date1.equals(PdfObject.NOTHING) || date2 == null || date2.equals(PdfObject.NOTHING)) {
            return "0s";
        }
        SimpleDateFormat myFormatter = new SimpleDateFormat(Constants.TIMEFORMAT);
        try {
            double seconds = (double) ((myFormatter.parse(date1).getTime() - myFormatter.parse(date2).getTime()) / 1000);
            double hou = Math.floor(seconds / 3600.0d);
            return String.valueOf(hou) + ":" + Math.floor((seconds / 60.0d) % 60.0d) + ":" + (seconds % 3600.0d);
        } catch (Exception e) {
            return "0s";
        }
    }

    public static long getTimeDifferentialLong(String oldDate) {
        String date2 = getStringDate_Ms();
        if (oldDate == null || oldDate.equals(PdfObject.NOTHING) || date2 == null || date2.equals(PdfObject.NOTHING)) {
            return 0;
        }
        SimpleDateFormat myFormatter = new SimpleDateFormat("yyyy-MM-dd kk:mm:ss:ms");
        try {
            Date oldD = myFormatter.parse(oldDate);
            Date mydate = myFormatter.parse(date2);
            if (mydate.getTime() < oldD.getTime()) {
                Log.i(PdfObject.NOTHING, PdfObject.NOTHING);
            }
            return mydate.getTime() - oldD.getTime();
        } catch (Exception e) {
            return 0;
        }
    }

    public static String getNowMonth(String sdate) {
        String sdate2 = String.valueOf(sdate.substring(0, 8)) + "01";
        Date date = strToDate(sdate2);
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        return getNextDay(sdate2, 1 - c.get(7));
    }

    public static String getNo(int k) {
        return String.valueOf(getUserDate("yyyyMMddhhmmss")) + getRandom(k);
    }

    public static String getRandom(int i) {
        Random jjj = new Random();
        if (i == 0) {
            return PdfObject.NOTHING;
        }
        String jj = PdfObject.NOTHING;
        for (int k = 0; k < i; k++) {
            jj = String.valueOf(jj) + jjj.nextInt(9);
        }
        return jj;
    }

    public static boolean RightDate(String date) {
        SimpleDateFormat sdf;
        new SimpleDateFormat(Constants.TIMEFORMAT);
        if (date == null) {
            return false;
        }
        if (date.length() > 10) {
            sdf = new SimpleDateFormat(Constants.TIMEFORMAT);
        } else {
            sdf = new SimpleDateFormat("yyyy-MM-dd");
        }
        try {
            sdf.parse(date);
            return true;
        } catch (ParseException e) {
            return false;
        }
    }

    public static String getStringDateMonth(String sdate, String nd, String yf, String rq, String format) {
        String dateString = new SimpleDateFormat("yyyy-MM-dd").format(new Date());
        String s_nd = dateString.substring(0, 4);
        String s_yf = dateString.substring(5, 7);
        String s_rq = dateString.substring(8, 10);
        String sreturn = PdfObject.NOTHING;
        if (sdate == null || sdate.equals(PdfObject.NOTHING)) {
            if (nd.equals("1")) {
                sreturn = s_nd;
                if (format.equals("1")) {
                    sreturn = String.valueOf(sreturn) + "年";
                } else if (format.equals("2")) {
                    sreturn = String.valueOf(sreturn) + "-";
                } else if (format.equals("3")) {
                    sreturn = String.valueOf(sreturn) + "/";
                } else if (format.equals("4")) {
                    sreturn = String.valueOf(sreturn) + ".";
                }
            }
            if (yf.equals("1")) {
                sreturn = String.valueOf(sreturn) + s_yf;
                if (format.equals("1")) {
                    sreturn = String.valueOf(sreturn) + "月";
                } else if (format.equals("2")) {
                    sreturn = String.valueOf(sreturn) + "-";
                } else if (format.equals("3")) {
                    sreturn = String.valueOf(sreturn) + "/";
                } else if (format.equals("4")) {
                    sreturn = String.valueOf(sreturn) + ".";
                }
            }
            if (!rq.equals("1")) {
                return sreturn;
            }
            String sreturn2 = String.valueOf(sreturn) + s_rq;
            return format.equals("1") ? String.valueOf(sreturn2) + "日" : sreturn2;
        }
        String s_nd2 = sdate.substring(0, 4);
        String s_yf2 = sdate.substring(5, 7);
        String s_rq2 = sdate.substring(8, 10);
        if (nd.equals("1")) {
            sreturn = s_nd2;
            if (format.equals("1")) {
                sreturn = String.valueOf(sreturn) + "年";
            } else if (format.equals("2")) {
                sreturn = String.valueOf(sreturn) + "-";
            } else if (format.equals("3")) {
                sreturn = String.valueOf(sreturn) + "/";
            } else if (format.equals("5")) {
                sreturn = String.valueOf(sreturn) + ".";
            }
        }
        if (yf.equals("1")) {
            sreturn = String.valueOf(sreturn) + s_yf2;
            if (format.equals("1")) {
                sreturn = String.valueOf(sreturn) + "月";
            } else if (format.equals("2")) {
                sreturn = String.valueOf(sreturn) + "-";
            } else if (format.equals("3")) {
                sreturn = String.valueOf(sreturn) + "/";
            } else if (format.equals("5")) {
                sreturn = String.valueOf(sreturn) + ".";
            }
        }
        if (!rq.equals("1")) {
            return sreturn;
        }
        String sreturn3 = String.valueOf(sreturn) + s_rq2;
        if (format.equals("1")) {
            return String.valueOf(sreturn3) + "日";
        }
        return sreturn3;
    }

    public static String getNextMonthDay(String sdate, int m) {
        String smonth;
        String sdate2 = getOKDate(sdate);
        int year = Integer.parseInt(sdate2.substring(0, 4));
        int month = Integer.parseInt(sdate2.substring(5, 7)) + m;
        if (month < 0) {
            month += 12;
            year--;
        } else if (month > 12) {
            month -= 12;
            year++;
        }
        if (month < 10) {
            smonth = "0" + month;
        } else {
            smonth = new StringBuilder().append(month).toString();
        }
        return String.valueOf(year) + "-" + smonth + "-10";
    }

    public static String getOKDate(String sdate) {
        if (sdate == null || sdate.equals(PdfObject.NOTHING)) {
            return getStringDateShort();
        }
        if (!StrUtil.isDate(sdate)) {
            sdate = getStringDateShort();
        }
        String sdate2 = StrUtil.Replace(sdate, "/", "-");
        if (sdate2.length() == 8) {
            sdate2 = String.valueOf(sdate2.substring(0, 4)) + "-" + sdate2.substring(4, 6) + "-" + sdate2.substring(6, 8);
        }
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        return formatter.format(formatter.parse(sdate2, new ParsePosition(0)));
    }

    public static boolean isRightBirthday(String birthday) {
        if (birthday == null) {
            return false;
        }
        String[] d = birthday.split("-");
        if (3 < d.length) {
            return false;
        }
        return d[0].length() == 4 && d[1].length() == 2 && d[2].length() == 2;
    }

    public static void convertBirthday(String birthday) {
        String[] d = birthday.split("-");
        if (d[1].length() == 1) {
            d[1] = "0" + d[1];
        }
        if (d[2].length() == 1) {
            d[1] = "0" + d[1];
        }
        String birthday2 = String.valueOf(d[0]) + "-" + d[1] + "-" + d[2];
    }

    public static String getStrTime(long duration) {
        Object valueOf;
        if (duration < 59) {
            StringBuilder sb = new StringBuilder("00:00:");
            if (duration < 10) {
                valueOf = "0" + duration;
            } else {
                valueOf = Long.valueOf(duration);
            }
            return sb.append(valueOf).toString();
        }
        long minute = duration / 60;
        long second = duration % 60;
        if (minute < 59) {
            return "00:" + (minute < 10 ? "0" + minute : Long.valueOf(minute)) + ":" + (second < 10 ? "0" + second : Long.valueOf(second));
        }
        long hour = minute / 60;
        long minute2 = minute % 60;
        return String.valueOf(hour) + ":" + (minute2 < 10 ? "0" + minute2 : Long.valueOf(minute2)) + ":" + (second < 10 ? "0" + second : Long.valueOf(second));
    }

    public static Date getLastMothDate() {
        return getLastDate((long) (new Date().getDate() + 1));
    }
}
