package com.clou.rs350.utils;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.net.Uri;
import android.text.TextUtils;
import android.widget.TextView;

import com.clou.rs350.CLApplication;
import com.clou.rs350.R;
import com.clou.rs350.db.model.DBDataModel;
import com.clou.rs350.device.constants.CLTConstant;
import com.itextpdf.text.pdf.PdfObject;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.text.pdf.codec.JBIG2SegmentReader;
import com.itextpdf.text.xml.xmp.XmpWriter;
import com.itextpdf.xmp.XMPError;

import java.io.File;
import java.io.UnsupportedEncodingException;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;

public class OtherUtils {
    private static long time = 0;

    public static boolean is3P(String sWiring) {
        String[] sWirings = CLApplication.app.getResources().getStringArray(R.array.wiring);
        int Wiring = 0;
        int i = 0;
        while (true) {
            if (i >= sWirings.length) {
                break;
            } else if (sWiring.equals(sWirings[i])) {
                Wiring = i;
                break;
            } else {
                i++;
            }
        }
        if (1 == Wiring) {
            return true;
        }
        return false;
    }

    public static String getString(int resId) {
        return CLApplication.app.getResources().getString(resId);
    }

    public static int getColor(double accuracy, double error) {
        int Result = -1;
        if (0.0d != accuracy) {
            if (Math.abs(error) > Math.abs(accuracy)) {
                Result = 1;
            } else {
                Result = 0;
            }
        }
        return getColor(Result);
    }

    public static void setText(TextView view, DBDataModel data) {
        view.setText(data.s_Value);
        view.setTextColor(getColor((int) data.l_Value));
    }

    public static void setResult(TextView view, double accuracy, DBDataModel error) {
        int Result = (int) getErrorResult(accuracy, error).l_Value;
        switch (Result) {
            case -1:
                view.setText("---");
                break;
            case 0:
                view.setText(R.string.text_pass);
                break;
            case 1:
                view.setText(R.string.text_fail);
                break;
        }
        view.setTextColor(getColor(Result));
    }

    public static DBDataModel getErrorResult(double accuracy, DBDataModel error) {
        DBDataModel Result = new DBDataModel(-1L, PdfObject.NOTHING);
        if (!error.s_Value.equals("---") && 0.0d != accuracy) {
            if (Math.abs(error.d_Value) > Math.abs(accuracy)) {
                Result.l_Value = 1;
            } else {
                Result.l_Value = 0;
            }
        }
        Result.s_Value = getErrorResult((int) Result.l_Value);
        return Result;
    }

    public static String getErrorResult(int flag) {
        switch (flag) {
            case 0:
                return getString(R.string.text_pass);
            case 1:
                return getString(R.string.text_fail);
            default:
                return PdfObject.NOTHING;
        }
    }

    public static int getColor(double accuracy, DBDataModel error) {
        return getColor((int) getErrorResult(accuracy, error).l_Value);
    }

    public static int getColor(int Result) {
        Resources res = CLApplication.app.getResources();
        switch (Result) {
            case -1:
                return res.getColor(R.color.showtextcolor);
            case 0:
                return res.getColor(R.color.PassTextColor);
            default:
                return res.getColor(R.color.FalseTextColor);
        }
    }

    public static DBDataModel getResultValue(String strValue, int id, Context context) {
        int result = -1;
        if (strValue == null) {
            return new DBDataModel((long) -1, PdfObject.NOTHING);
        }
        String[] strValues = context.getResources().getStringArray(id);
        if (strValue.equals(strValues[0])) {
            result = 0;
        } else if (strValue.equals(strValues[1])) {
            result = 1;
        }
        return new DBDataModel((long) result, strValue);
    }

    public static String toHexString(byte[] bytes) {
        if (bytes == null) {
            return null;
        }
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < bytes.length; i++) {
            sb.append(String.valueOf(Integer.toHexString(bytes[i])) + " ");
        }
        return sb.toString();
    }

    public static String bytes2HexString(byte[] frame, String sep) {
        StringBuffer sb = new StringBuffer();
        for (byte b : frame) {
            String val = Integer.toHexString(b & 255);
            if (val.length() < 2) {
                val = "0" + val;
            }
            sb.append(val);
            sb.append(sep);
        }
        String f = sb.toString();
        if (f.length() <= 0 || f.lastIndexOf(sep) != f.length() - 1) {
            return f;
        }
        return f.substring(0, f.lastIndexOf(sep));
    }

    public static void openUrlBySystemBrowser(String url, Context conext) {
        Intent intent = new Intent();
        intent.setAction("android.intent.action.VIEW");
        intent.setData(Uri.parse(url));
        conext.startActivity(intent);
    }

    public static void openFileBySystemBrowser(String file, String path, Context conext) {
        new Intent();
        Intent intent = new Intent("android.intent.action.VIEW", Uri.withAppendedPath(Uri.fromFile(new File(file)), path));
        intent.setClassName("com.android.browser", "com.android.browser.BrowserActivity");
        conext.startActivity(intent);
    }

    private static String copyFile(String assetsName) {
        String saveFile = String.valueOf(CLApplication.app.getDownloadPath()) + File.separator + assetsName;
        File file = new File(saveFile);
        file.getParentFile().mkdirs();
        if (!file.exists()) {
            FileUtils.copyFileToSdcard(assetsName, saveFile, CLApplication.app);
        }
        return saveFile;
    }

    public static int getnumberCount(String strValue) {
        int count = strValue.length() - 1;
        while (count >= 0 && strValue.charAt(count) != '0' && strValue.charAt(count) != '1' && strValue.charAt(count) != '2' && strValue.charAt(count) != '3' && strValue.charAt(count) != '4' && strValue.charAt(count) != '5' && strValue.charAt(count) != '6' && strValue.charAt(count) != '7' && strValue.charAt(count) != '8' && strValue.charAt(count) != '9') {
            count--;
        }
        return count + 1;
    }

    public static String[] doubleArrayToStringArray(double[] array) {
        if (array == null || array.length == 0) {
            return null;
        }
        String[] strArray = new String[array.length];
        for (int i = 0; i < array.length; i++) {
            strArray[i] = String.valueOf(array[i]);
        }
        return strArray;
    }

    public static int parseInt(String value) {
        try {
            return Integer.parseInt(value);
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    public static int parseInt(String value, int defaultValue) {
        try {
            return Integer.parseInt(value);
        } catch (Exception e) {
            e.printStackTrace();
            return defaultValue;
        }
    }

    public static double parseDouble(String value) {
        try {
            return Double.parseDouble(value);
        } catch (Exception e) {
            e.printStackTrace();
            return 0.0d;
        }
    }

    public static double parseDouble(TextView valueTv) {
        try {
            return Double.parseDouble(valueTv.getText().toString().trim());
        } catch (Exception e) {
            e.printStackTrace();
            return 0.0d;
        }
    }

    public static double parseDouble(String value, String unit) {
        if (TextUtils.isEmpty(value) || "---".equals(value)) {
            return 0.0d;
        }
        return parseDouble(value.replace(unit, PdfObject.NOTHING));
    }

    public static long parseLong(String value) {
        try {
            return Long.parseLong(value);
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    public static byte[] parseObjectArray(Object[] data) {
        if (data == null || data.length == 0) {
            return null;
        }
        byte[] array = new byte[data.length];
        for (int i = 0; i < data.length; i++) {
            try {
                array[i] = Byte.parseByte(data[i].toString());
            } catch (Exception e) {
                return null;
            }
        }
        return array;
    }

    public static String parseString(byte[] data) {
        try {
            return new String(data, XmpWriter.UTF8);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static int convertToInt(Object data) {
        try {
            return ((Number) data).intValue();
        } catch (Exception e) {
            return 0;
        }
    }

    public static int convertToUint(Object data) {
        try {
            return ((Number) data).intValue() & 1073741823;
        } catch (Exception e) {
            return 0;
        }
    }

    public static double convertToDouble(Object data) {
        try {
            return ((Number) data).doubleValue();
        } catch (Exception e) {
            return 0.0d;
        }
    }

    public static long convertToLong(Object data) {
        try {
            return ((Number) data).longValue();
        } catch (Exception e) {
            return 0;
        }
    }

    public static int[] parseIntArray(Object[] array) {
        if (array == null) {
            return null;
        }
        int[] intArray = new int[array.length];
        for (int i = 0; i < array.length; i++) {
            intArray[i] = convertToInt(array[i]);
        }
        return intArray;
    }

    public static Double[] parseDoubleArray(Object[] array) {
        if (array == null) {
            return null;
        }
        Double[] doubleArray = new Double[array.length];
        for (int i = 0; i < array.length; i++) {
            doubleArray[i] = Double.valueOf(convertToDouble(array[i]));
        }
        return doubleArray;
    }

    public static Double[] parseDoubleArrayRemove0(Object[] array, int dot) {
        if (array == null && array.length > 2) {
            return null;
        }
        Double[] doubleArray = new Double[(array.length - 1)];
        for (int i = 1; i < array.length; i++) {
            doubleArray[i - 1] = Double.valueOf(convertToDouble(array[i]));
            doubleArray[i - 1] = DataProcessing.RoundValueDouble(doubleArray[i - 1], dot);
        }
        return doubleArray;
    }

    public static boolean is3P(int wiring) {
        return wiring != 1;
    }

    public static String decimalFormat(float value, int length) {
        DecimalFormat df = new DecimalFormat("0.00");
        DecimalFormatSymbols symbols = new DecimalFormatSymbols();
        symbols.setDecimalSeparator('.');
        df.setDecimalFormatSymbols(symbols);
        return df.format((double) value);
    }

    public static String parseWiring(int wiring, int pQFlag) {
        Resources res = CLApplication.app.getResources();
        String[] w_head = res.getStringArray(R.array.wiring_head);
        String[] w_tail = res.getStringArray(R.array.wiring_tail);
        StringBuffer sb = new StringBuffer();
        sb.append(w_head[wiring]);
        sb.append(w_tail[pQFlag]);
        return sb.toString();
    }

    public static String getTime(long seconds) {
        int hou = (int) (seconds / 3600);
        int min = (int) ((seconds / 60) % 60);
        int sec = (int) (seconds % 60);
        String houStr = hou > 9 ? new StringBuilder(String.valueOf(hou)).toString() : "0" + hou;
        return String.valueOf(houStr) + ":" + (min > 9 ? new StringBuilder(String.valueOf(min)).toString() : "0" + min) + ":" + (sec > 9 ? new StringBuilder(String.valueOf(sec)).toString() : "0" + sec);
    }

    public static String parseUnitByWiring(int pqFlag, String unit) {
        String str = PdfObject.NOTHING;
        switch (pqFlag) {
            case 0:
                str = "W";
                break;
            case 1:
                str = "var";
                break;
            case 2:
                str = "VA";
                break;
        }
        return unit.replace("W", str);
    }

    public static boolean isEmpty(TextView tv) {
        if (tv == null) {
            return true;
        }
        return TextUtils.isEmpty(tv.getText().toString().trim());
    }

    public static boolean isEmpty(String text) {
        return TextUtils.isEmpty(text);
    }

    public static String getText(TextView textView) {
        return textView != null ? textView.getText().toString().trim() : PdfObject.NOTHING;
    }

    public static int getIntValue(TextView textView) {
        try {
            return Integer.parseInt(textView.getText().toString().trim());
        } catch (Exception e) {
            return 0;
        }
    }

    public static String parsePhaseSequence(int UaPhaseSequence, int UbPhaseSequence, int UcPhaseSequence) {
        return String.valueOf(String.valueOf(String.valueOf(PdfObject.NOTHING) + parsePhaseSequence(UaPhaseSequence)) + " | " + parsePhaseSequence(UbPhaseSequence)) + " | " + parsePhaseSequence(UcPhaseSequence);
    }

    private static String parsePhaseSequence(int phaseSequenceValue) {
        String PhaseSequence = PdfObject.NOTHING;
        Resources res = CLApplication.app.getResources();
        switch (phaseSequenceValue) {
            case 1:
                return String.valueOf(PhaseSequence) + res.getString(R.string.text_1);
            case 2:
                return String.valueOf(PhaseSequence) + res.getString(R.string.text_2);
            case 4:
                return String.valueOf(PhaseSequence) + res.getString(R.string.text_3);
            case 8:
                PhaseSequence = "-";
                return String.valueOf(PhaseSequence) + res.getString(R.string.text_1);
            case 16:
                PhaseSequence = "-";
                return String.valueOf(PhaseSequence) + res.getString(R.string.text_2);
            case 32:
                PhaseSequence = "-";
                return String.valueOf(PhaseSequence) + res.getString(R.string.text_3);
            default:
                return " ";
        }
    }

    public static DBDataModel parseRound(Object value) {
        DBDataModel model = new DBDataModel();
        try {
            model.d_Value = convertToDouble(value);
            model.s_Value = DataProcessing.RoundValue(Double.valueOf(model.d_Value));
        } catch (Exception e) {
        }
        return model;
    }

    public static DBDataModel parseRound(Object value, int pointIndex) {
        DBDataModel model = new DBDataModel();
        try {
            model.d_Value = convertToDouble(value);
            model.s_Value = DataProcessing.RoundValueString(Double.valueOf(model.d_Value), pointIndex);
        } catch (Exception e) {
        }
        return model;
    }

    public static int parseHexInt(String sHex) {
        if (TextUtils.isEmpty(sHex)) {
            return -1;
        }
        int iHex = 0;
        int i = 0;
        while (i < 8 && i < sHex.length()) {
            iHex = (iHex << 4) + parseHexCharToInt(sHex.charAt(i));
            i++;
        }
        return iHex;
    }

    public static String parseHexString(int intHex) {
        char[] cHex = {'0', '1', PdfWriter.VERSION_1_2, PdfWriter.VERSION_1_3, PdfWriter.VERSION_1_4, PdfWriter.VERSION_1_5, PdfWriter.VERSION_1_6, PdfWriter.VERSION_1_7, '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};
        String sHex = PdfObject.NOTHING;
        for (int i = 0; i < 8; i++) {
            sHex = String.valueOf(cHex[intHex & 15]) + sHex;
            intHex >>= 4;
        }
        return sHex;
    }

    private static int parseHexCharToInt(char charHex) {
        switch (charHex) {
            case JBIG2SegmentReader.PAGE_INFORMATION /*{ENCODED_INT: 48}*/:
                return 0;
            case '1':
                return 1;
            case '2':
                return 2;
            case JBIG2SegmentReader.END_OF_FILE /*{ENCODED_INT: 51}*/:
                return 3;
            case JBIG2SegmentReader.PROFILES /*{ENCODED_INT: 52}*/:
                return 4;
            case '5':
                return 5;
            case '6':
                return 6;
            case '7':
                return 7;
            case '8':
                return 8;
            case '9':
                return 9;
            case CLTConstant.Func_Mstate_41:
            case 'a':
                return 10;
            case CLTConstant.Func_Mstate_42:
            case 'b':
                return 11;
            case CLTConstant.Func_Mstate_43:
            case 'c':
                return 12;
            case 'D':
            case 'd':
                return 13;
            case 'E':
            case 'e':
                return 14;
            case 'F':
            case XMPError.BADXPATH /*{ENCODED_INT: 102}*/:
                return 15;
            default:
                return 0;
        }
    }

    public static boolean isDoubleClick() {
        if (System.currentTimeMillis() - time > 1500) {
            time = System.currentTimeMillis();
            return false;
        }
        time = System.currentTimeMillis();
        return true;
    }
}
