package com.clou.rs350.utils;

import android.content.Context;
import android.content.Intent;
import com.clou.rs350.CLApplication;
import java.io.Serializable;

public class BroadCastUtil {
    private static final String ACTION = "com.clou.rs350";

    public static final class Action {
        public static final String ACTION_BLUETOOTH_UNAVAILABLE = "com.clou.rs350.bluetooth.unavailable";
        public static final String ACTION_CONNECTING = "com.clou.rs350.connecting";
        public static final String ACTION_DISCONNECT = "com.clou.rs350.disconnect";
        public static final String ACTION_NO_DEVICE = "com.clou.rs350.no.device";
        public static final String ACTION_SEARCHING = "com.clou.rs350.searching";
        public static final String ACTION_SEARCH_COMPLETE = "com.clou.rs350.search.complete";
        public static final String ACTION_SERVER_CONNECTED = "com.clou.rs350.server.connected";
        public static final String ACTION_SOCKET_CONNECTED = "com.clou.rs350.socket.connected";
    }

    public static void send(String action) {
        CLApplication.app.sendBroadcast(new Intent(action));
    }

    public static void send(String action, String extraName, Serializable extraObj) {
        Context context = CLApplication.app;
        Intent broadcastIntent = new Intent();
        broadcastIntent.setAction(action);
        broadcastIntent.putExtra(extraName, extraObj);
        context.sendBroadcast(broadcastIntent);
    }
}
