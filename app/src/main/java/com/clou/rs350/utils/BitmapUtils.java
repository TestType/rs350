package com.clou.rs350.utils;

import android.annotation.SuppressLint;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import java.io.File;
import java.io.FileOutputStream;

@SuppressLint({"SimpleDateFormat"})
public class BitmapUtils {
    public static Bitmap getScaledBitmap(Bitmap b, float sx, float sy) {
        Matrix matrix = new Matrix();
        matrix.postScale(sx, sy);
        if (b != null) {
            return Bitmap.createBitmap(b, 0, 0, b.getWidth(), b.getHeight(), matrix, true);
        }
        return null;
    }

    public static String saveBitmapFile(Bitmap b, String path) {
        Exception e;
        try {
            File file = new File(path);
            if (file.exists()) {
                file.delete();
            }
            file.createNewFile();
            FileOutputStream fos = new FileOutputStream(file);
            if (fos != null) {
                try {
                    b.compress(Bitmap.CompressFormat.JPEG, 100, fos);
                    fos.close();
                } catch (Exception e2) {
                    e = e2;
                    e.printStackTrace();
                    return path;
                }
            }
        } catch (Exception e3) {
            e = e3;
            e.printStackTrace();
            return path;
        }
        return path;
    }
}
