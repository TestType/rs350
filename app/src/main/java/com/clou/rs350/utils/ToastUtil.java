package com.clou.rs350.utils;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.clou.rs350.R;

public class ToastUtil {
    public static void show(Context context, String info) {
        Toast.makeText(context, info, 1).show();
    }

    public static void show(Context context, int info) {
        Toast.makeText(context, info, 1).show();
    }

    public static void showMyToast(Context context, String info) {
        View view = LayoutInflater.from(context).inflate(R.layout.mytoast, (ViewGroup) null, false);
        Toast toast = new Toast(context);
        toast.setGravity(16, 0, 0);
        toast.setDuration(1);
        toast.setView(view);
        toast.show();
    }
}
