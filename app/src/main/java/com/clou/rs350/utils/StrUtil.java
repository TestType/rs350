package com.clou.rs350.utils;

import com.itextpdf.text.pdf.ColumnText;
import com.itextpdf.text.pdf.PdfBoolean;
import com.itextpdf.text.pdf.PdfObject;
import java.lang.Character;
import java.util.Date;
import java.util.regex.Pattern;

public class StrUtil {
    private static long lastClickTime;

    public static boolean isFastDoubleClick() {
        long time = System.currentTimeMillis();
        long timeD = time - lastClickTime;
        if (0 < timeD && timeD < 700) {
            return true;
        }
        lastClickTime = time;
        return false;
    }

    public static boolean isDate(String sdate) {
        if (!sdate.contains("年") || !sdate.contains("月") || !sdate.contains("日")) {
            return false;
        }
        int indexY = sdate.indexOf("年");
        int indexM = sdate.indexOf("月");
        int indexD = sdate.indexOf("日");
        if (!isNumber(sdate.substring(0, indexY))) {
            return false;
        }
        String monthStr = sdate.substring(indexY + 1, indexM);
        if (!isNumber(monthStr) || Integer.valueOf(monthStr).intValue() > 12 || !isNumber(sdate.substring(indexM + 1, indexD)) || Integer.valueOf(monthStr).intValue() > 31) {
            return false;
        }
        return true;
    }

    public static final String format(int x) {
        String s = new StringBuilder().append(x).toString();
        if (s.length() == 1) {
            return "0" + s;
        }
        return s;
    }

    public static boolean isLocation(String location) {
        if (isNull(location)) {
            return false;
        }
        String[] lc = location.split(",");
        return lc.length == 2 && lc[0].startsWith("(") && lc[1].endsWith(")");
    }

    public static String Replace(String sdate, String string, String string2) {
        return null;
    }

    public static String strTrim(String str) {
        if (!isNull(str)) {
            return str.trim();
        }
        return null;
    }

    public static boolean isNull(String str) {
        if (str != null && !PdfObject.NOTHING.equals(str.trim()) && !"NULL".equals(str.trim().toUpperCase())) {
            return false;
        }
        return true;
    }

    public static boolean isNumber(String num) {
        for (char c : num.toCharArray()) {
            if (!"0123456789".contains(new Character(c).toString())) {
                return false;
            }
        }
        return true;
    }

    public static boolean isZimu(char c) {
        return "abcdefghijklmnopqrstuvwxyz".contains(new Character(c).toString().toLowerCase());
    }

    public static boolean isEmail(String email) {
        return Pattern.compile("^([a-zA-Z0-9_\\.\\-])+\\@(([a-zA-Z0-9\\-])+\\.)+([a-zA-Z0-9]{2,4})+$").matcher(email).matches();
    }

    public static boolean isPhoneNum(String mobilesNo) {
        if (mobilesNo == null || mobilesNo.length() != 11 || !mobilesNo.startsWith("1")) {
            return false;
        }
        return true;
    }

    public static String handlePhone(String phone) {
        return phone.replace("+86", PdfObject.NOTHING).replace(" ", PdfObject.NOTHING).replace("-", PdfObject.NOTHING);
    }

    public static boolean isFixNumber(String fixNumber) {
        if (isNumber(fixNumber) && !fixNumber.startsWith("00")) {
            return true;
        }
        return false;
    }

    public static String getCallTimeFormat(String time) {
        if (isNull(time) || time.equals("Unanswered")) {
            return time;
        }
        StringBuffer sb = new StringBuffer();
        if (time.contains("-")) {
            time = time.replace("-", PdfObject.NOTHING);
        }
        String[] times = time.split(":");
        int hours = Integer.parseInt(times[0]);
        int minutes = Integer.parseInt(times[1]);
        int seconds = Integer.parseInt(times[2]);
        if (hours > 0 && minutes == 0 && seconds > 0) {
            minutes++;
            seconds = 0;
        }
        if (hours > 0) {
            sb.append(String.valueOf(hours) + " hours ");
        }
        if (minutes > 0) {
            sb.append(String.valueOf(minutes) + " minutes ");
        }
        if (seconds > 0) {
            sb.append(String.valueOf(seconds) + " seconds ");
        }
        return sb.toString();
    }

    public static boolean isYesterday(Date date) {
        Date today = new Date();
        if (date.getYear() == today.getYear() && date.getMonth() == today.getMonth() && today.getDay() - date.getDay() == 1) {
            return true;
        }
        return false;
    }

    public static int strlength(String value) {
        int valueLength = 0;
        for (int i = 0; i < value.length(); i++) {
            if (value.substring(i, i + 1).matches("[一-龥]")) {
                valueLength += 2;
            } else {
                valueLength++;
            }
        }
        return valueLength;
    }

    public static int getIntBool(String bool) {
        String bool2 = bool.toLowerCase();
        if (PdfBoolean.TRUE.equals(bool2)) {
            return 1;
        }
        if (PdfBoolean.FALSE.equals(bool2)) {
            return 0;
        }
        return Integer.parseInt(bool2);
    }

    public static boolean getBoolInt(String bool) {
        String bool2 = bool.toLowerCase();
        if ("1".equals(bool2)) {
            return true;
        }
        if ("0".equals(bool2)) {
            return false;
        }
        return Boolean.parseBoolean(bool2);
    }

    public static String getSex(String sex) {
        if (sex == null || !sex.toUpperCase().equals("F")) {
            return "男";
        }
        return "女";
    }

    public static boolean checkChar(char c) {
        Character.UnicodeBlock ub = Character.UnicodeBlock.of(c);
        if (ub == Character.UnicodeBlock.CJK_UNIFIED_IDEOGRAPHS || ub == Character.UnicodeBlock.CJK_COMPATIBILITY_IDEOGRAPHS || ub == Character.UnicodeBlock.CJK_UNIFIED_IDEOGRAPHS_EXTENSION_A || ub == Character.UnicodeBlock.CJK_UNIFIED_IDEOGRAPHS_EXTENSION_B || ub == Character.UnicodeBlock.CJK_SYMBOLS_AND_PUNCTUATION || ub == Character.UnicodeBlock.HALFWIDTH_AND_FULLWIDTH_FORMS || ub == Character.UnicodeBlock.GENERAL_PUNCTUATION) {
            return true;
        }
        return false;
    }

    public static boolean checkChinese(String strName) {
        char[] ch;
        for (char c : strName.toCharArray()) {
            if (checkChar(c)) {
                return true;
            }
        }
        return false;
    }

    public static boolean isMessyCode(String strName) {
        char[] ch = Pattern.compile("\\s*|\t*|\r*|\n*").matcher(strName).replaceAll(PdfObject.NOTHING).replaceAll("\\p{P}", PdfObject.NOTHING).trim().toCharArray();
        float chLength = (float) ch.length;
        float count = ColumnText.GLOBAL_SPACE_CHAR_RATIO;
        for (char c : ch) {
            if (!Character.isLetterOrDigit(c) && !checkChar(c)) {
                count += 1.0f;
            }
        }
        if (((double) (count / chLength)) > 0.4d) {
            return true;
        }
        return false;
    }
}
