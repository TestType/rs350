package com.clou.rs350.utils;

import com.itextpdf.text.pdf.ColumnText;
import com.itextpdf.text.pdf.PdfObject;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;

public class AngleUtil {
    public static double[] GetAngle(int wiring, String cos, boolean negativePhaseSequence) {
        double[] angle = {0.0d, 120.0d, 240.0d, 0.0d, 120.0d, 240.0d};
        switch (wiring) {
            case 2:
                angle[0] = 0.0d;
                angle[1] = 0.0d;
                angle[2] = 300.0d;
                angle[3] = 30.0d;
                angle[4] = 0.0d;
                angle[5] = 270.0d;
                break;
        }
        int sin = 1;
        if (cos.contains("C") || cos.contains("c")) {
            sin = -1;
            cos = cos.substring(0, cos.length() - 1);
        }
        if (cos.contains("L") || cos.contains("l")) {
            cos = cos.substring(0, cos.length() - 1);
        }
        if (cos.equals(PdfObject.NOTHING) || cos.equals("-")) {
            cos = "0";
        }
        double angleOffset = (Math.acos(Double.parseDouble(cos)) / 3.141592653589793d) * 180.0d * ((double) sin);
        angle[3] = angle[3] + angleOffset;
        angle[4] = angle[4] + angleOffset;
        angle[5] = angle[5] + angleOffset;
        if (negativePhaseSequence) {
            angle[0] = 360.0d - angle[0];
            angle[1] = 360.0d - angle[1];
            angle[2] = 360.0d - angle[2];
            angle[3] = 360.0d - angle[3];
            angle[4] = 360.0d - angle[4];
            angle[5] = 360.0d - angle[5];
        }
        angle[0] = adjustAngle(angle[0]);
        angle[1] = adjustAngle(angle[1]);
        angle[2] = adjustAngle(angle[2]);
        angle[3] = adjustAngle(angle[3]);
        angle[4] = adjustAngle(angle[4]);
        angle[5] = adjustAngle(angle[5]);
        return angle;
    }

    public static String getCos(int wiring, double standardAngle, double realAngle, boolean negativePhaseSequence) {
        double angle = adjustAngle(realAngle - standardAngle);
        double d_cos = Math.cos((angle / 180.0d) * 3.141592653589793d);
        DecimalFormat df = new DecimalFormat("#.####");
        DecimalFormatSymbols symbols = new DecimalFormatSymbols();
        symbols.setDecimalSeparator('.');
        df.setDecimalFormatSymbols(symbols);
        return String.valueOf(df.format(d_cos)) + (angle > 180.0d ? "C" : "L");
    }

    public static double adjustAngle(double angle) {
        return adjustAngle(angle, 0);
    }

    public static double adjustAngle(double angle, int type) {
        float min = ColumnText.GLOBAL_SPACE_CHAR_RATIO;
        float max = 360.0f;
        switch (type) {
            case 1:
                min = -180.0f;
                max = 180.0f;
                break;
        }
        while (angle < ((double) min)) {
            angle += 360.0d;
        }
        while (angle >= ((double) max)) {
            angle -= 360.0d;
        }
        return angle;
    }

    public static double getAngle(double angle, int angleDefinition) {
        if (angleDefinition != 0) {
            angle = 360.0d - angle;
        }
        return adjustAngle(angle);
    }

    public static double getNegativeAngle(double angle) {
        return adjustAngle(angle + 180.0d);
    }
}
