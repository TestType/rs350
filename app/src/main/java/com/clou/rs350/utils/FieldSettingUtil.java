package com.clou.rs350.utils;

import android.content.Context;

import com.clou.rs350.R;
import com.clou.rs350.db.dao.FieldSettingDao;
import com.clou.rs350.db.model.sequence.FieldItem;
import com.clou.rs350.db.model.sequence.FieldSetting;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class FieldSettingUtil {
    public static final String ACCURACY = "Accuracy";
    public static final String CITY = "City";
    public static final String CONTACTNAME = "ContactName";
    public static final String CONTACTNO = "ContactNo";
    public static final String CUSTOMERINFO = "CustomerInfo";
    public static final String CUSTOMERSRNAME = "CustomerName";
    public static final String DISTRICT = "District";
    public static final String EMAIL = "Email";
    public static final String GPS = "GPS";
    public static final String HOUSENO = "HouseNo";
    public static final String IB = "Ib";
    public static final String IMAX = "IMax";
    public static final String MANUFACTURER = "Manufacturer";
    public static final String METERCERTIFICATIONYEAR = "MeterCertificationYear";
    public static final String METERPRODUCTIONYEAR = "MeterProductionYear";
    public static final String METERSR = "MeterSr";
    public static final String METERTYPE = "MeterType";
    public static final String NOMINALFREQUENCY = "NominalFrequency";
    public static final String NOMINALVOLTAGE = "NominalVoltage";
    public static final String OBSERVEDLOAD = "ObservedLoad";
    public static final String PINCODE = "Pincode";
    public static final String SANCTIONEDLOAD = "SanctionedLoad";
    public static final String SITEDATA = "SiteData";
    public static final String STATE = "State";
    public static final String STREET = "Street";
    Context m_Context;
    Map<String, Names> m_Names;

    public FieldSettingUtil(Context context) {
        this.m_Context = context;
        initNames();
    }

    /* access modifiers changed from: package-private */
    public class NameID {
        int Id;
        String Name;

        NameID(int id, String name) {
            this.Id = id;
            this.Name = name;
        }
    }

    /* access modifiers changed from: package-private */
    public class Names {
        List<NameID> Names;
        int TitleId;

        Names(int id, List<NameID> names) {
            this.TitleId = id;
            this.Names = names;
        }
    }

    /* access modifiers changed from: package-private */
    public void initNames() {
        this.m_Names = new HashMap();
        List<NameID> tmpCustomerInfo = new ArrayList<>();
        tmpCustomerInfo.add(new NameID(R.string.text_customer_name, "CustomerName"));
        tmpCustomerInfo.add(new NameID(R.string.text_add_street, STREET));
        tmpCustomerInfo.add(new NameID(R.string.text_house_nr, HOUSENO));
        tmpCustomerInfo.add(new NameID(R.string.text_state, "State"));
        tmpCustomerInfo.add(new NameID(R.string.text_city, "City"));
        tmpCustomerInfo.add(new NameID(R.string.text_district, "District"));
        tmpCustomerInfo.add(new NameID(R.string.text_pin, PINCODE));
        tmpCustomerInfo.add(new NameID(R.string.text_contact_name, CONTACTNAME));
        tmpCustomerInfo.add(new NameID(R.string.text_email, "Email"));
        tmpCustomerInfo.add(new NameID(R.string.text_contact_nr, CONTACTNO));
        tmpCustomerInfo.add(new NameID(R.string.text_gps, GPS));
        this.m_Names.put(CUSTOMERINFO, new Names(R.string.text_customer_info, tmpCustomerInfo));
        List<NameID> tmpSiteData = new ArrayList<>();
        tmpSiteData.add(new NameID(R.string.text_meter_sr, METERSR));
        tmpSiteData.add(new NameID(R.string.text_meter_type, "MeterType"));
        tmpSiteData.add(new NameID(R.string.text_meter_production, METERPRODUCTIONYEAR));
        tmpSiteData.add(new NameID(R.string.text_meter_certification, METERCERTIFICATIONYEAR));
        tmpSiteData.add(new NameID(R.string.text_sanctioned_load, "SanctionedLoad"));
        tmpSiteData.add(new NameID(R.string.text_observed_load, "ObservedLoad"));
        this.m_Names.put(SITEDATA, new Names(R.string.text_site_data, tmpSiteData));
        List<NameID> tmpMeterType = new ArrayList<>();
        tmpMeterType.add(new NameID(R.string.text_manufacturer, "Manufacturer"));
        tmpMeterType.add(new NameID(R.string.text_basic_current, IB));
        tmpMeterType.add(new NameID(R.string.text_maximum_current, IMAX));
        tmpMeterType.add(new NameID(R.string.text_nominal_voltage, "NominalVoltage"));
        tmpMeterType.add(new NameID(R.string.text_nominal_frequency, "NominalFrequency"));
        tmpMeterType.add(new NameID(R.string.text_accuracy, "Accuracy"));
        this.m_Names.put("MeterType", new Names(R.string.text_meter_type, tmpMeterType));
    }

    public FieldSetting getFieldSetting(String Element) {
        FieldSetting tmpData = (FieldSetting) new FieldSettingDao(this.m_Context).queryObjectByKey(Element);
        if (tmpData == null) {
            tmpData = new FieldSetting();
            tmpData.Items = new HashMap();
        }
        Names tmpNames = this.m_Names.get(Element);
        tmpData.Element = Element;
        tmpData.TitleId = tmpNames.TitleId;
        tmpData.ItemKeys = new ArrayList();
        for (int i = 0; i < tmpNames.Names.size(); i++) {
            NameID tmpName = tmpNames.Names.get(i);
            tmpData.ItemKeys.add(tmpName.Name);
            if (tmpData.Items.containsKey(tmpName.Name)) {
                tmpData.Items.get(tmpName.Name).Id = tmpName.Id;
            } else {
                tmpData.Items.put(tmpName.Name, new FieldItem(tmpName.Name, tmpName.Id, 0));
            }
        }
        return tmpData;
    }
}
