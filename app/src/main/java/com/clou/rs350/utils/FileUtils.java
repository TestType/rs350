package com.clou.rs350.utils;

import android.content.Context;
import android.os.Environment;
import com.itextpdf.text.pdf.PdfObject;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

public class FileUtils {
    public static List<String> getFileDir(String filePath) {
        try {
            List<String> items = new ArrayList<>();
            File[] files = new File(filePath).listFiles();
            if (files == null) {
                return items;
            }
            for (File file : files) {
                items.add(file.getName());
            }
            return items;
        } catch (Exception ex) {
            ex.printStackTrace();
            return null;
        }
    }

    public static List<String> openFileList(String file, Context context) {
        try {
            List<String> files = new ArrayList<>();
            BufferedReader br = new BufferedReader(new InputStreamReader(context.getAssets().open(new File(file).getPath())));
            while (true) {
                String path = br.readLine();
                if (path == null) {
                    return files;
                }
                files.add(path);
            }
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static String copyFileToSdcard(String assetsName, String outFileName, Context context) {
        try {
            OutputStream myOutput = new FileOutputStream(outFileName);
            InputStream myInput = context.getAssets().open(assetsName);
            byte[] buffer = new byte[1024];
            for (int length = myInput.read(buffer); length > 0; length = myInput.read(buffer)) {
                myOutput.write(buffer, 0, length);
            }
            myOutput.flush();
            myInput.close();
            myOutput.close();
            return PdfObject.NOTHING;
        } catch (IOException e) {
            e.printStackTrace();
            return PdfObject.NOTHING;
        }
    }

    public static String saveContentToFile(String content, String outFileName, Context context) {
        IOException e;
        try {
            OutputStream myOutput = new FileOutputStream(outFileName);
            InputStream myInput = new ByteArrayInputStream(content.getBytes());
            try {
                byte[] buffer = new byte[1024];
                for (int length = myInput.read(buffer); length > 0; length = myInput.read(buffer)) {
                    myOutput.write(buffer, 0, length);
                }
                myOutput.flush();
                myInput.close();
                myOutput.close();
                return outFileName;
            } catch (IOException e2) {
                e = e2;
                e.printStackTrace();
                return PdfObject.NOTHING;
            }
        } catch (IOException e3) {
            e = e3;
            e.printStackTrace();
            return PdfObject.NOTHING;
        }
    }

    public static boolean hasSdcard() {
        if (Environment.getExternalStorageState().equals("mounted")) {
            return true;
        }
        return false;
    }

    public static String getSdcardRootPath() {
        if (hasSdcard()) {
            return Environment.getExternalStorageDirectory().getPath();
        }
        return null;
    }

    public static String getCacheRootPath(Context context) {
        return context.getCacheDir().getAbsolutePath();
    }
}
