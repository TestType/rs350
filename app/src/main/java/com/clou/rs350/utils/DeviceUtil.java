package com.clou.rs350.utils;

import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.os.Build;
import android.telephony.TelephonyManager;
import android.util.DisplayMetrics;
import android.util.Log;

public class DeviceUtil {
    private static final String TAG = "DeviceUtil";

    public static String getInfo(Context ctx) {
        return ((TelephonyManager) ctx.getSystemService("phone")).getDeviceId();
    }

    public static String getOsVersion() {
        return Build.VERSION.RELEASE;
    }

    public static int getVersion(Context ctx) {
        PackageInfo pinfo = null;
        try {
            pinfo = ctx.getPackageManager().getPackageInfo(ctx.getPackageName(), 16384);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return pinfo.versionCode;
    }

    public static String getVersionName(Context ctx) {
        PackageInfo pinfo = null;
        try {
            pinfo = ctx.getPackageManager().getPackageInfo(ctx.getPackageName(), 16384);
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
        return pinfo.versionName;
    }

    public static int getWidthMaxDp(Activity activity) {
        DisplayMetrics dm = new DisplayMetrics();
        activity.getWindowManager().getDefaultDisplay().getMetrics(dm);
        int wMaxDP = (dm.widthPixels * 160) / dm.densityDpi;
        Log.i(TAG, "wMaxDP:" + wMaxDP);
        return wMaxDP;
    }

    public static int getWidthMaxPx(Activity activity) {
        DisplayMetrics dm = new DisplayMetrics();
        activity.getWindowManager().getDefaultDisplay().getMetrics(dm);
        return dm.widthPixels;
    }

    public static int getHeightMaxDp(Activity activity) {
        DisplayMetrics dm = new DisplayMetrics();
        activity.getWindowManager().getDefaultDisplay().getMetrics(dm);
        int wMaxDP = (dm.heightPixels * 160) / dm.densityDpi;
        Log.i(TAG, "wMaxDP:" + wMaxDP);
        return wMaxDP;
    }

    public static int getHeightMaxPx(Activity activity) {
        DisplayMetrics dm = new DisplayMetrics();
        activity.getWindowManager().getDefaultDisplay().getMetrics(dm);
        return dm.heightPixels;
    }

    public static int getWidthMinDp(Activity context) {
        Configuration config = context.getResources().getConfiguration();
        int screenWidth = config.screenWidthDp;
        int screenHeight = config.screenHeightDp;
        int smallestScreenWidthDp = config.smallestScreenWidthDp;
        Log.i(TAG, "screenWidth:" + screenWidth + " screenHeight:" + screenHeight);
        Log.i(TAG, "smallestScreenWidthDp:" + smallestScreenWidthDp);
        return config.smallestScreenWidthDp;
    }
}
