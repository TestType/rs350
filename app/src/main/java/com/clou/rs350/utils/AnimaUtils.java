package com.clou.rs350.utils;

import android.content.Context;
import android.graphics.PointF;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.AnimationUtils;
import android.view.animation.ScaleAnimation;
import android.view.animation.TranslateAnimation;
import com.itextpdf.text.pdf.ColumnText;

public class AnimaUtils {
    public static Animation createRightToLeftAnimation(View view, float translate) {
        Animation anim = new TranslateAnimation(view.getX(), view.getX() - translate, view.getPivotY(), view.getPivotY());
        anim.setInterpolator(new AccelerateInterpolator());
        anim.setDuration(500);
        AnimationSet set = new AnimationSet(true);
        Animation scalAni = new ScaleAnimation(1.0f, ColumnText.GLOBAL_SPACE_CHAR_RATIO, 1.0f, ColumnText.GLOBAL_SPACE_CHAR_RATIO, 1, ColumnText.GLOBAL_SPACE_CHAR_RATIO, 1, 0.5f);
        scalAni.setDuration(500);
        set.addAnimation(scalAni);
        return set;
    }

    public static Animation createLeftToRightAnimationNew(View view, float translate) {
        Animation anim = new TranslateAnimation(view.getX() - translate, view.getX(), view.getPivotY(), view.getPivotY() - ((float) view.getLayoutParams().height));
        anim.setInterpolator(new AccelerateDecelerateInterpolator());
        anim.setDuration(500);
        AnimationSet set = new AnimationSet(true);
        Animation scalAni = new ScaleAnimation(ColumnText.GLOBAL_SPACE_CHAR_RATIO, 1.0f, ColumnText.GLOBAL_SPACE_CHAR_RATIO, 1.0f, 1, 1.0f, 1, 1.0f);
        scalAni.setDuration(500);
        set.addAnimation(scalAni);
        return set;
    }

    public static void startAnimation(int aimation, View view, Context context) {
        startAnimation(AnimationUtils.loadAnimation(context, aimation), view);
    }

    public static void startAnimation(Animation animation, View view) {
        if (animation != null && view != null) {
            view.startAnimation(animation);
        }
    }

    public static void dragViewVertical(final View view) {
        if (view != null) {
            view.setOnTouchListener(new View.OnTouchListener() {
                /* class com.clou.rs350.utils.AnimaUtils.AnonymousClass1 */
                int mode = 0;
                float startX = ColumnText.GLOBAL_SPACE_CHAR_RATIO;
                float startY = ColumnText.GLOBAL_SPACE_CHAR_RATIO;

                public boolean onTouch(View v, MotionEvent event) {
                    switch (event.getAction()) {
                        case 0:
                        case 5:
                            if (event.getPointerCount() != 1) {
                                this.startX = event.getX(0);
                                this.startY = event.getY(0);
                                this.mode = 1;
                                break;
                            }
                            break;
                        case 1:
                        case 6:
                            this.mode = 0;
                            break;
                        case 2:
                            if (this.mode == 1) {
                                AnimaUtils.doMove(event, view, new PointF(this.startX, this.startY));
                                break;
                            }
                            break;
                    }
                    return false;
                }
            });
        }
    }

    /* access modifiers changed from: private */
    public static void doMove(MotionEvent event, View parentView, PointF start) {
        PointF stop = new PointF(event.getX(), event.getY());
        boolean hasMoved = false;
        float rectLeftX = parentView.getX();
        float rectTopY = parentView.getY();
        float width = (float) parentView.getWidth();
        float trans_X = stop.x - start.x;
        if (Math.abs(trans_X) - 2.0f > ColumnText.GLOBAL_SPACE_CHAR_RATIO) {
            if (trans_X / Math.abs(trans_X) > ColumnText.GLOBAL_SPACE_CHAR_RATIO) {
                rectLeftX += trans_X;
            } else {
                rectLeftX += trans_X;
                float width2 = (float) parentView.getWidth();
            }
            hasMoved = true;
        }
        float height = (float) parentView.getHeight();
        float trans_Y = stop.y - start.y;
        if (Math.abs(trans_Y) - 2.0f > ColumnText.GLOBAL_SPACE_CHAR_RATIO) {
            if (trans_Y / Math.abs(trans_Y) > ColumnText.GLOBAL_SPACE_CHAR_RATIO) {
                rectTopY += trans_Y;
            } else {
                rectTopY += trans_Y;
            }
            hasMoved = true;
        }
        if (hasMoved) {
            parentView.setX(rectLeftX);
            parentView.setY(rectTopY);
        }
    }
}
