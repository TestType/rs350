package com.clou.rs350.utils;

import android.os.AsyncTask;
import com.clou.rs350.model.ClouData;
import com.clou.rs350.task.ISleepCallback;
import com.clou.rs350.task.SleepTask;
import com.itextpdf.text.pdf.PdfContentParser;

public class DoubleClick {
    private OnDoubleClick m_Click;
    private SleepTask m_ClickDelay;
    private int m_ClickTime;
    private int m_Delay;

    public interface OnDoubleClick {
        void doubleClick();

        void singleClick();
    }

    public DoubleClick() {
        this.m_ClickDelay = null;
        this.m_ClickTime = 0;
        this.m_Delay = PdfContentParser.COMMAND_TYPE;
        this.m_Delay = ClouData.getInstance().getSettingBasic().DoubleClickDelay;
    }

    public DoubleClick(OnDoubleClick click) {
        this();
        this.m_Click = click;
    }

    public void click() {
        if (this.m_ClickDelay == null || this.m_ClickDelay.getStatus() == AsyncTask.Status.FINISHED) {
            this.m_ClickDelay = new SleepTask((long) this.m_Delay, new ISleepCallback() {
                /* class com.clou.rs350.utils.DoubleClick.AnonymousClass1 */

                @Override // com.clou.rs350.task.ISleepCallback
                public void onSleepAfterToDo() {
                    if (DoubleClick.this.m_Click != null) {
                        if (2 > DoubleClick.this.m_ClickTime) {
                            DoubleClick.this.m_Click.singleClick();
                        } else {
                            DoubleClick.this.m_Click.doubleClick();
                        }
                    }
                    DoubleClick.this.m_ClickTime = 0;
                }
            });
            this.m_ClickDelay.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, 0);
        }
        this.m_ClickTime++;
    }

    public void setClick(OnDoubleClick click) {
        this.m_Click = click;
    }
}
