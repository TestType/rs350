package com.clou.rs350.utils;

import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.util.DisplayMetrics;
import com.clou.rs350.Preferences;
import java.util.Locale;

public class LanguageUtil {

    public static void changeLanguage(Context context) {
        Resources resources = context.getResources();
        DisplayMetrics dm = resources.getDisplayMetrics();
        Configuration config = resources.getConfiguration();
        switch (Preferences.getInt("language", 0)) {
            case 0:
                config.locale = Locale.ENGLISH;
                break;
            case 1:
                config.locale = Locale.GERMAN;
                break;
            case 2:
                config.locale = Locale.CHINESE;
                break;
            case 3:
                config.locale = new Locale("ru");
                break;
            case 4:
                config.locale = Locale.ITALIAN;
                break;
            case 5:
                config.locale = new Locale("hu");
                break;
            case 6:
                config.locale = new Locale("es");
                break;
            default:
                config.locale = Locale.ENGLISH;
                break;
        }
        resources.updateConfiguration(config, dm);
    }

    /*public static void changeLanguageBothAndSystem(int type, Context context) {
        Locale locale;
        if (type == 1) {
            try {
                locale = Locale.CHINESE;
            } catch (Exception e) {
                e.printStackTrace();
                return;
            }
        } else {
            locale = Locale.ENGLISH;
        }
        Class clzIActMag = Class.forName("android.app.IActivityManager");
        Class clzActMagNative = Class.forName("android.app.ActivityManagerNative");
        Object objIActMag = clzActMagNative.getDeclaredMethod("getDefault", new Class[0]).invoke(clzActMagNative, new Object[0]);
        Configuration config = (Configuration) clzIActMag.getDeclaredMethod("getConfiguration", new Class[0]).invoke(objIActMag, new Object[0]);
        config.locale = locale;
        clzIActMag.getDeclaredMethod("updateConfiguration", Configuration.class).invoke(objIActMag, config);
    }*/
}
