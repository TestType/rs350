package com.clou.rs350.util;

import android.app.Activity;
import android.view.View;

public class SystemUiHiderBase extends SystemUiHider {
    private boolean mVisible = true;

    protected SystemUiHiderBase(Activity activity, View anchorView, int flags) {
        super(activity, anchorView, flags);
    }

    @Override // com.clou.rs350.util.SystemUiHider
    public void setup() {
        if ((this.mFlags & 1) == 0) {
            this.mActivity.getWindow().setFlags(768, 768);
        }
    }

    @Override // com.clou.rs350.util.SystemUiHider
    public boolean isVisible() {
        return this.mVisible;
    }

    @Override // com.clou.rs350.util.SystemUiHider
    public void hide() {
        if ((this.mFlags & 2) != 0) {
            this.mActivity.getWindow().setFlags(1024, 1024);
        }
        this.mOnVisibilityChangeListener.onVisibilityChange(false);
        this.mVisible = false;
    }

    @Override // com.clou.rs350.util.SystemUiHider
    public void show() {
        if ((this.mFlags & 2) != 0) {
            this.mActivity.getWindow().setFlags(0, 1024);
        }
        this.mOnVisibilityChangeListener.onVisibilityChange(true);
        this.mVisible = true;
    }
}
