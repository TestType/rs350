package com.clou.rs350.util;

import android.annotation.TargetApi;
import android.app.Activity;
import android.os.Build;
import android.view.View;
import com.itextpdf.text.pdf.codec.TIFFConstants;

@TargetApi(11)
public class SystemUiHiderHoneycomb extends SystemUiHiderBase {
    private int mHideFlags = 1;
    private int mShowFlags = 0;
    private View.OnSystemUiVisibilityChangeListener mSystemUiVisibilityChangeListener = new View.OnSystemUiVisibilityChangeListener() {
        /* class com.clou.rs350.util.SystemUiHiderHoneycomb.AnonymousClass1 */

        public void onSystemUiVisibilityChange(int vis) {
            if ((SystemUiHiderHoneycomb.this.mTestFlags & vis) != 0) {
                if (Build.VERSION.SDK_INT < 16) {
                    SystemUiHiderHoneycomb.this.mActivity.getActionBar().hide();
                    SystemUiHiderHoneycomb.this.mActivity.getWindow().setFlags(1024, 1024);
                }
                SystemUiHiderHoneycomb.this.mOnVisibilityChangeListener.onVisibilityChange(false);
                SystemUiHiderHoneycomb.this.mVisible = false;
                return;
            }
            SystemUiHiderHoneycomb.this.mAnchorView.setSystemUiVisibility(SystemUiHiderHoneycomb.this.mShowFlags);
            if (Build.VERSION.SDK_INT < 16) {
                SystemUiHiderHoneycomb.this.mActivity.getActionBar().show();
                SystemUiHiderHoneycomb.this.mActivity.getWindow().setFlags(0, 1024);
            }
            SystemUiHiderHoneycomb.this.mOnVisibilityChangeListener.onVisibilityChange(true);
            SystemUiHiderHoneycomb.this.mVisible = true;
        }
    };
    private int mTestFlags = 1;
    private boolean mVisible = true;

    protected SystemUiHiderHoneycomb(Activity activity, View anchorView, int flags) {
        super(activity, anchorView, flags);
        if ((this.mFlags & 2) != 0) {
            this.mShowFlags |= 1024;
            this.mHideFlags |= 1028;
        }
        if ((this.mFlags & 6) != 0) {
            this.mShowFlags |= 512;
            this.mHideFlags |= TIFFConstants.TIFFTAG_JPEGIFBYTECOUNT;
            this.mTestFlags |= 2;
        }
    }

    @Override // com.clou.rs350.util.SystemUiHider, com.clou.rs350.util.SystemUiHiderBase
    public void setup() {
        this.mAnchorView.setOnSystemUiVisibilityChangeListener(this.mSystemUiVisibilityChangeListener);
    }

    @Override // com.clou.rs350.util.SystemUiHider, com.clou.rs350.util.SystemUiHiderBase
    public void hide() {
        this.mAnchorView.setSystemUiVisibility(this.mHideFlags);
    }

    @Override // com.clou.rs350.util.SystemUiHider, com.clou.rs350.util.SystemUiHiderBase
    public void show() {
        this.mAnchorView.setSystemUiVisibility(this.mShowFlags);
    }

    @Override // com.clou.rs350.util.SystemUiHider, com.clou.rs350.util.SystemUiHiderBase
    public boolean isVisible() {
        return this.mVisible;
    }
}
