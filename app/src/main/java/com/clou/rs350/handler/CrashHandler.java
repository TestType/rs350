package com.clou.rs350.handler;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Looper;
import android.os.Process;
import android.os.SystemClock;
import android.util.Log;
import android.widget.Toast;

import com.clou.rs350.CLApplication;
import com.clou.rs350.R;
import com.clou.rs350.model.ClouData;

import java.io.File;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.lang.reflect.Field;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class CrashHandler implements Thread.UncaughtExceptionHandler {
    public static String TAG = "MyCrash";
    private static CrashHandler instance = new CrashHandler();
    private DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss");
    private Map<String, String> infos = new HashMap();
    private Context mContext;
    private Thread.UncaughtExceptionHandler mDefaultHandler;

    private CrashHandler() {
    }

    public static CrashHandler getInstance() {
        return instance;
    }

    public void init(Context context) {
        this.mContext = context;
        this.mDefaultHandler = Thread.getDefaultUncaughtExceptionHandler();
        Thread.setDefaultUncaughtExceptionHandler(this);
        autoClear(5);
    }

    public void uncaughtException(Thread thread, Throwable ex) {
        if (handleException(ex) || this.mDefaultHandler == null) {
            SystemClock.sleep(2000);
            Process.killProcess(Process.myPid());
            System.exit(1);
            return;
        }
        this.mDefaultHandler.uncaughtException(thread, ex);
    }

    private boolean handleException(Throwable ex) {
        if (ex == null) {
            return false;
        }
        try {
            new Thread() {
                /* class com.clou.rs350.handler.CrashHandler.AnonymousClass1 */

                public void run() {
                    Looper.prepare();
                    Toast.makeText(CrashHandler.this.mContext, (int) R.string.text_crash_info, Toast.LENGTH_LONG).show();
                    Looper.loop();
                }
            }.start();
            collectDeviceInfo(this.mContext);
            saveCrashInfoFile(ex);
            ClouData.getInstance().saveSerialization();
            SystemClock.sleep(3000);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return true;
    }

    public void collectDeviceInfo(Context ctx) {
        try {
            PackageInfo pi = ctx.getPackageManager().getPackageInfo(ctx.getPackageName(), PackageManager.GET_ACTIVITIES);
            if (pi != null) {
                String versionName = new StringBuilder(String.valueOf(pi.versionName)).toString();
                String versionCode = new StringBuilder(String.valueOf(pi.versionCode)).toString();
                this.infos.put("versionName", versionName);
                this.infos.put("versionCode", versionCode);
            }
        } catch (PackageManager.NameNotFoundException e) {
            Log.e(TAG, "an error occured when collect package info", e);
        }
        Field[] fields = Build.class.getDeclaredFields();
        for (Field field : fields) {
            try {
                field.setAccessible(true);
                this.infos.put(field.getName(), field.get(null).toString());
            } catch (Exception e2) {
                Log.e(TAG, "an error occured when collect crash info", e2);
            }
        }
    }

    private String saveCrashInfoFile(Throwable ex) throws Exception {
        StringBuffer sb = new StringBuffer();
        try {
            sb.append("\r\n" + new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date()) + "\n");
            for (Map.Entry<String, String> entry : this.infos.entrySet()) {
                sb.append(String.valueOf(entry.getKey()) + "=" + entry.getValue() + "\n");
            }
            Writer writer = new StringWriter();
            PrintWriter printWriter = new PrintWriter(writer);
            ex.printStackTrace(printWriter);
            for (Throwable cause = ex.getCause(); cause != null; cause = cause.getCause()) {
                cause.printStackTrace(printWriter);
            }
            printWriter.flush();
            printWriter.close();
            sb.append(writer.toString());
            return writeFile(sb.toString());
        } catch (Exception e) {
            Log.e(TAG, "an error occured while writing file...", e);
            sb.append("an error occured while writing file...\r\n");
            writeFile(sb.toString());
            return null;
        }
    }

    private String writeFile(String sb) throws Exception {
        String fileName = "crash-" + this.formatter.format(new Date()) + ".log";
        String path = CLApplication.app.getLogPath();
        FileOutputStream fos = new FileOutputStream(String.valueOf(path) + File.separator + fileName, true);
        fos.write(sb.getBytes());
        fos.flush();
        fos.close();
        CLApplication.app.refreshPath(path);
        return fileName;
    }

    public static void setTag(String tag) {
        TAG = tag;
    }

    public void autoClear(int autoClearDay) {
    }
}
