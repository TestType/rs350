package com.clou.rs350.handler;

import android.content.Context;
import android.view.View;

import com.clou.rs350.R;

import java.util.List;

public class HandlerSwitchView implements View.OnClickListener {
    private boolean changeBackground = true;
    private Context context;
    private View currentView;
    private int m_Index = 0;
    private IOnSelectCallback onTableChangedCallback;
    private List<View> tabViews;

    public interface IOnSelectCallback {
        void onClickView(View view);

        void onSelectedChanged(int i);
    }

    public HandlerSwitchView(Context context2) {
        this.context = context2;
    }

    public void setTabViews(List<View> tabViews2) {
        if (tabViews2 != null && tabViews2.size() > 0) {
            this.tabViews = tabViews2;
        }
    }

    public void setChangeBackground(boolean flag) {
        this.changeBackground = flag;
    }

    public int getIndex() {
        return this.m_Index;
    }

    public void onClick(View v) {
        selectView(v);
        int index = Integer.parseInt(v.getTag(R.id.index_id).toString());
        if (index != this.m_Index) {
            this.m_Index = index;
            if (this.onTableChangedCallback != null) {
                this.onTableChangedCallback.onSelectedChanged(index);
                this.onTableChangedCallback.onClickView(v);
            }
        }
    }

    public void selectCurrentView() {
        try {
            selectView(this.currentView);
        } catch (IndexOutOfBoundsException e) {
        }
    }

    public void selectView(int index) {
        try {
            this.m_Index = index;
            selectView(this.tabViews.get(index));
        } catch (IndexOutOfBoundsException e) {
        }
    }

    public void selectView(View v) {
        if (this.changeBackground) {
            if (this.currentView != null) {
                this.currentView.setEnabled(true);
            }
            v.setEnabled(false);
        }
        this.currentView = v;
    }

    public void boundClick() {
        if (this.tabViews != null) {
            for (int i = 0; i < this.tabViews.size(); i++) {
                this.tabViews.get(i).setTag(R.id.index_id, Integer.valueOf(i));
                this.tabViews.get(i).setOnClickListener(this);
                setButtonEnabled(this.tabViews.get(i), true);
            }
        }
    }

    public void setButtonEnabled(View button, boolean enable) {
        button.setEnabled(enable);
    }

    public void unBoundClick() {
        if (this.tabViews != null) {
            for (int i = 0; i < this.tabViews.size(); i++) {
                this.tabViews.get(i).setOnClickListener(null);
                setButtonEnabled(this.tabViews.get(i), false);
            }
        }
    }

    public void setOnTabChangedCallback(IOnSelectCallback onTableChangedCallback2) {
        this.onTableChangedCallback = onTableChangedCallback2;
    }
}
