package com.clou.rs350;

import android.content.Context;
import android.content.SharedPreferences;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.util.Log;
import com.itextpdf.text.pdf.ColumnText;
import com.itextpdf.text.pdf.PdfObject;
import java.util.Random;
import java.util.UUID;

public class Preferences {

    public static final class Key {
        public static final String ADDRESS = "address";
        public static final String ANGLEDEFINITION = "AngleDefinition";
        public static final String AUTOCONNECTTYPE = "autoconnecttype";
        public static final String BASIC_SOCKET_TIMEOUT = "basic_socket_timeout";
        public static final String BAUDRATE = "baudrate";
        public static final String BLUETOOTH_MAC = "bluetooth_mac";
        public static final String BLUETOOTH_NAME = "bluetooth_name";
        public static final String CNVERSION = "cnversion";
        public static final String CONNECTEDBEFORE = "connectedbefore";
        public static final String CURRENTHARMONICLIMIT = "currentharmoniclimit";
        public static final String CURRENTSYMMETRY = "currentsymmetry";
        public static final String CURRENTUNBALANCE = "currentunbalance";
        public static final String DEMANDTESTMODE = "DemandTestMode";
        public static final String DEVICEMODEL = "DeviceModel";
        public static final String DIGITALMETERTESTING = "digitalmetertesting";
        public static final String DOUBLECLICKDELAY = "DoubleClickDelay";
        public static final String ENERGYTESTMODE = "EnergyTestMode";
        public static final String ERRORAUTOSTOP = "ErrorAutoStop";
        public static final String ERRORCOUNT = "ErrorCount";
        public static final String ERRORDIGIT = "ErrorDigit";
        public static final String ERRORISBASETIME = "ErrorIsBaseTime";
        public static final String ERRORTESTMODE = "ErrorTestMode";
        public static final String EXPORTTEMPLATE = "exporttemplate";
        public static final String FIRMWAREVERSION = "firmwareversion";
        public static final String HARMONICDEFINITION = "HarmonicDefinition";
        public static final String HAS_CRASH = "has_crash";
        public static final String HAS_UUID = "has_uuid";
        public static final String ISHOST = "ishost";
        public static final String IS_FIRST_LUNCHAPP = "is_first_lunchapp";
        public static final String IS_FIRST_READMETER = "is_first_readmeter";
        public static final String LANGUAGE = "language";
        public static final String MAINBOARDVERSION = "mainboardversion";
        public static final String MEASUREBOARDVERSION = "measureboardversion";
        public static final String NEUTRALBOARDVERSION = "neutralboardversion";
        public static final String OPERATOR_ID = "operator_id";
        public static final String PHASECOLORI1 = "PhaseColorI1";
        public static final String PHASECOLORI2 = "PhaseColorI2";
        public static final String PHASECOLORI3 = "PhaseColorI3";
        public static final String PHASECOLORU1 = "PhaseColorU1";
        public static final String PHASECOLORU2 = "PhaseColorU2";
        public static final String PHASECOLORU3 = "PhaseColorU3";
        public static final String PRINTER_BLUETOOTH_MAC = "printer_bluetooth_mac";
        public static final String PRINTER_BLUETOOTH_NAME = "printer_bluetooth_name";
        public static final String PROGRESSBARTIME = "ProgressBarTime";
        public static final String PROTOCOL = "protocol";
        public static final String PT_CHANNEL = "pt_channel";
        public static final String REALBASICSTYLE = "realbasicstyle";
        public static final String REALHARMONICSTYLE = "realharmonicstyle";
        public static final String REALWAVESTYLE = "realwavestyle";
        public static final String REFRESHTIME = "RefreshTime";
        public static final String RESETFLAG = "ResetFlag";
        public static final String ReturnScreen = "ReturnScreen";
        public static final String SETTING_GROUP_NAME = "setting_group_name";
        public static final String SHOW_GROUP_NAME = "show_group_name";
        public static final String SOUND = "Sound";
        public static final String SSTYPE = "SSType";
        public static final String TEMPLATE = "template";
        public static final String TESTSEQUENCE = "testsequence";
        public static final String TIMEBASEFORERROR = "TimeBaseForError";
        public static final String UPDATESERVER = "updateserver";
        public static final String UUID = "uuid";
        public static final String VECTORDEFINITION = "VectorDefinition";
        public static final String VECTORTYPE = "VectorType";
        public static final String VERSIONBEFORE = "version_before";
        public static final String VIBRATION = "Vibration";
        public static final String VOLTAGEHARMONICLIMIT = "voltageharmoniclimit";
        public static final String VOLTAGELOWERLIMIT = "voltagelowerlimit";
        public static final String VOLTAGENOMINAL = "voltagenominal";
        public static final String VOLTAGESYMMETRY = "voltagesymmetry";
        public static final String VOLTAGEUNBALANCE = "voltageunbalance";
        public static final String VOLTAGEUPPERLIMIT = "voltageupperlimit";
        public static final String WAITLIMIT = "WaitLimit";
    }

    public static int getInt(String key) {
        return CLApplication.mSharedPreferences.getInt(key, 0);
    }

    public static int getInt(String key, int defaultValue) {
        if (CLApplication.mSharedPreferences != null) {
            return CLApplication.mSharedPreferences.getInt(key, defaultValue);
        }
        return defaultValue;
    }

    public static void putInt(String key, int value) {
        SharedPreferences.Editor editor = CLApplication.mSharedPreferences.edit();
        editor.putInt(key, value);
        editor.commit();
    }

    public static float getfloat(String key) {
        return CLApplication.mSharedPreferences.getFloat(key, ColumnText.GLOBAL_SPACE_CHAR_RATIO);
    }

    public static float getFloat(String key, float defaultValue) {
        if (CLApplication.mSharedPreferences != null) {
            return CLApplication.mSharedPreferences.getFloat(key, defaultValue);
        }
        return defaultValue;
    }

    public static void putFloat(String key, float value) {
        SharedPreferences.Editor editor = CLApplication.mSharedPreferences.edit();
        editor.putFloat(key, value);
        editor.commit();
    }

    public static void putString(String key, String value) {
        SharedPreferences.Editor editor = CLApplication.mSharedPreferences.edit();
        editor.putString(key, value);
        editor.commit();
    }

    public static String getString(String key) {
        return CLApplication.mSharedPreferences.getString(key, PdfObject.NOTHING);
    }

    public static String getString(String key, String defaultValue) {
        return CLApplication.mSharedPreferences.getString(key, defaultValue);
    }

    public static void putBoolean(String key, boolean value) {
        SharedPreferences.Editor editor = CLApplication.mSharedPreferences.edit();
        editor.putBoolean(key, value);
        editor.commit();
    }

    public static boolean getBoolean(String key) {
        return CLApplication.mSharedPreferences.getBoolean(key, false);
    }

    public static boolean getBoolean(String key, boolean defaultValue) {
        return CLApplication.mSharedPreferences.getBoolean(key, defaultValue);
    }

    public static void removeKey(String key) {
        SharedPreferences.Editor editor = CLApplication.mSharedPreferences.edit();
        editor.remove(key);
        editor.commit();
    }

    public static boolean hasKey(String key) {
        return CLApplication.mSharedPreferences.contains(key);
    }

    private static void saveUUID() {
        String uuid = buildUUID(CLApplication.app);
        Log.i("Preference", "uuid－－－－－－－" + uuid);
        SharedPreferences.Editor editor = CLApplication.mSharedPreferences.edit();
        editor.putString(Key.UUID, uuid);
        editor.putBoolean(Key.HAS_UUID, true);
        editor.commit();
    }

    public static String getUUID() {
        if (hasUUID()) {
            return CLApplication.mSharedPreferences.getString(Key.UUID, PdfObject.NOTHING);
        }
        saveUUID();
        return CLApplication.mSharedPreferences.getString(Key.UUID, PdfObject.NOTHING);
    }

    private static String buildUUID(Context context) {
        String deviceId = ((TelephonyManager) context.getSystemService("phone")).getDeviceId();
        String androidId = Settings.Secure.getString(context.getContentResolver(), "android_id");
        if ((androidId == null || androidId.length() <= 0) && (deviceId == null || deviceId.length() <= 0)) {
            return null;
        }
        if (androidId == null) {
            androidId = new StringBuilder(String.valueOf(new Random().nextInt(10000000) + 999999)).toString();
        }
        if (deviceId == null) {
            deviceId = new StringBuilder(String.valueOf(new Random().nextInt(10000000) + 999999)).toString();
        }
        return new UUID((long) androidId.hashCode(), ((long) deviceId.hashCode()) << 32).toString();
    }

    public static boolean hasUUID() {
        return CLApplication.mSharedPreferences.getBoolean(Key.HAS_UUID, false);
    }
}
