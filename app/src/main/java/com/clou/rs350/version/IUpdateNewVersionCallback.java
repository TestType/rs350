package com.clou.rs350.version;

public interface IUpdateNewVersionCallback {
    void onNewVersionCallback(VersionInfo versionInfo);
}
