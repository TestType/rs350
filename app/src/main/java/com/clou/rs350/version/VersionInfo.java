package com.clou.rs350.version;

import android.content.Context;
import com.clou.rs350.utils.OtherUtils;
import java.io.Serializable;

public class VersionInfo implements Serializable {
    private static final long serialVersionUID = 1;
    private String apkUrl;
    private String firmwareDescript;
    private String firmwareUrl;
    private String firmwareVersion;
    private String versionCode;
    private String versionDescript;
    private String versionName;

    public int getVersionCode() {
        try {
            return OtherUtils.parseInt(this.versionCode);
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    public void setVersionCode(String versionCode2) {
        this.versionCode = versionCode2;
    }

    public String getVersionName() {
        return this.versionName;
    }

    public void setVersionName(String versionName2) {
        this.versionName = versionName2;
    }

    public String getVersionDescript() {
        return this.versionDescript;
    }

    public void setVersionDescript(String versionDescript2) {
        this.versionDescript = versionDescript2;
    }

    public String getFirmwareDescript() {
        return this.firmwareDescript;
    }

    public void setFirmwareDescript(String firmwareDescript2) {
        this.firmwareDescript = firmwareDescript2;
    }

    public String getApkUrl() {
        return this.apkUrl;
    }

    public void setApkUrl(String apkUrl2) {
        this.apkUrl = apkUrl2;
    }

    public String getFirmwareUrl() {
        return this.firmwareUrl;
    }

    public void setFirmwareUrl(String firmwareUrl2) {
        this.firmwareUrl = firmwareUrl2;
    }

    public String getFirmwareVersion() {
        return this.firmwareVersion;
    }

    public void setFirmwareVersion(String hardwareVersion) {
        this.firmwareVersion = hardwareVersion;
    }

    public String toString() {
        return "Version [apkVersion=" + this.versionCode + ", hardwareVersion=" + this.firmwareVersion + ", apkUrl=" + this.apkUrl + "]";
    }

    public boolean IsNewHardwareVersionExist() {
        return UpdateUtils.isHardwareNewVersionExist(this);
    }

    public boolean isApkNewVersionExist(Context context) {
        return UpdateUtils.isApkNewVersionExist(context, this);
    }
}
