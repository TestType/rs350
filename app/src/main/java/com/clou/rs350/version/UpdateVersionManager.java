package com.clou.rs350.version;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import java.util.ArrayList;
import java.util.List;

public class UpdateVersionManager {
    public static final String NEW_APK_VERSION_KEY = "com.clou.clservice.ui.add.apk.key";
    public static final String NEW_HARDWARE_VERSION_KEY = "com.clou.clservice.ui.add.hardware.key";
    public static final String NEW_VERSION_ACTION = "com.clou.clservice.ui.add.version";
    private static UpdateVersionManager updateVersionManager;
    private List<IUpdateNewVersionCallback> callbackList = new ArrayList();
    private Context mContext;
    private VersionInfo newVersion;
    private UpdateInfoReceiver updateInfoReceiver;

    private UpdateVersionManager(Context context) {
        this.mContext = context;
    }

    public static UpdateVersionManager getInstance(Context context) {
        if (updateVersionManager == null) {
            updateVersionManager = new UpdateVersionManager(context);
        }
        return updateVersionManager;
    }

    public void checkVersion() {
        //this.mContext.startService(new Intent("com.clou.rs350.service.checkVersion"));
        Intent bi = new Intent("com.clou.rs350.service.checkVersion");
        bi.setPackage("com.clou.rs350");
    }

    public void registerNewVersionExistBroadcast() {
        try {
            IntentFilter intentFilter = new IntentFilter(NEW_VERSION_ACTION);
            if (this.updateInfoReceiver == null) {
                this.updateInfoReceiver = new UpdateInfoReceiver();
            }
            this.mContext.registerReceiver(this.updateInfoReceiver, intentFilter);
        } catch (Exception e) {
        }
    }

    public void unregisterNewVersionExistBroadcast() {
        try {
            if (this.updateInfoReceiver != null) {
                this.mContext.unregisterReceiver(this.updateInfoReceiver);
                this.updateInfoReceiver = null;
            }
        } catch (Exception e) {
        }
    }

    public void removeCallback(IUpdateNewVersionCallback callback) {
        if (this.callbackList.contains(callback)) {
            this.callbackList.remove(callback);
        }
    }

    public void getNewVersion(IUpdateNewVersionCallback callback) {
        if (this.newVersion != null) {
            callback.onNewVersionCallback(this.newVersion);
        }
        if (!this.callbackList.contains(callback)) {
            this.callbackList.add(callback);
        }
    }

    /* access modifiers changed from: package-private */
    public class UpdateInfoReceiver extends BroadcastReceiver {
        UpdateInfoReceiver() {
        }

        public void onReceive(Context context, Intent intent) {
            if (UpdateVersionManager.NEW_VERSION_ACTION.equals(intent.getAction())) {
                if (intent.hasExtra("version")) {
                    UpdateVersionManager.this.newVersion = (VersionInfo) intent.getSerializableExtra("version");
                }
                for (IUpdateNewVersionCallback callback : UpdateVersionManager.this.callbackList) {
                    callback.onNewVersionCallback(UpdateVersionManager.this.newVersion);
                }
            }
        }
    }
}
