package com.clou.rs350.version;

import android.util.Xml;
import com.clou.rs350.report.ExpleanXml;
import com.clou.rs350.report.XmlModel;
import com.itextpdf.text.Annotation;
import java.io.IOException;
import java.io.InputStream;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

public class VersionInfoParser {
    public static VersionInfos parseVersionInfo(InputStream is) throws XmlPullParserException, IOException {
        XmlPullParser parser = Xml.newPullParser();
        parser.setInput(is, "utf-8");
        XmlModel tmpModel = new ExpleanXml().getElement(parser);
        VersionInfos info = new VersionInfos();
        XmlModel tmpVersionsModel = getVersionsModel(tmpModel);
        for (int i = 0; i < tmpVersionsModel.Datas.size(); i++) {
            XmlModel tmpVersionModel = (XmlModel) tmpVersionsModel.Datas.get(i);
            info.Versions.put(tmpVersionModel.Condition.get("hardware_version"), getVersion(tmpVersionModel));
        }
        return info;
    }

    private static XmlModel getVersionsModel(XmlModel tmpModel) {
        XmlModel tmpVersion = new XmlModel();
        if ("versions".equals(tmpModel.Type)) {
            return tmpModel;
        }
        for (int i = 0; i < tmpModel.Datas.size(); i++) {
            tmpVersion = getVersionsModel((XmlModel) tmpModel.Datas.get(i));
        }
        return tmpVersion;
    }

    private static VersionInfo getVersion(XmlModel tmpModels) {
        VersionInfo tmpVersion = new VersionInfo();
        for (int i = 0; i < tmpModels.Datas.size(); i++) {
            XmlModel tmpModel = (XmlModel) tmpModels.Datas.get(i);
            String name = tmpModel.Type;
            if ("code".equals(name)) {
                tmpVersion.setVersionCode((String) tmpModel.Datas.get(0));
            } else if ("name".equals(name)) {
                tmpVersion.setVersionName((String) tmpModel.Datas.get(0));
            } else if ("descript".equals(name)) {
                tmpVersion.setVersionDescript((String) tmpModel.Datas.get(0));
            } else if ("firmware_version".equals(name)) {
                tmpVersion.setFirmwareVersion((String) tmpModel.Datas.get(0));
            } else if (Annotation.URL.equals(name)) {
                tmpVersion.setApkUrl((String) tmpModel.Datas.get(0));
            } else if ("firmwareurl".equals(name)) {
                tmpVersion.setFirmwareUrl((String) tmpModel.Datas.get(0));
            } else if ("firmwaredescript".equals(name)) {
                tmpVersion.setFirmwareDescript((String) tmpModel.Datas.get(0));
            }
        }
        return tmpVersion;
    }
}
