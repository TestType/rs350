package com.clou.rs350.version;

import android.app.IntentService;
import android.content.Intent;
import com.clou.rs350.model.ClouData;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import org.xmlpull.v1.XmlPullParserException;

public class CheckVersionUpdateSerivce extends IntentService {
    public CheckVersionUpdateSerivce() {
        this("CheckVersionUpdateSerivce");
    }

    public CheckVersionUpdateSerivce(String name) {
        super(name);
    }

    /* access modifiers changed from: protected */
    public void onHandleIntent(Intent intent) {
        VersionInfos newVersionInfo = null;
        try {
            HttpURLConnection conn = (HttpURLConnection) new URL(String.valueOf(ClouData.getInstance().getSettingBasic().UpdateServer) + UpdateConfigs.UPDATE_XML_URL).openConnection();
            conn.setRequestMethod("GET");
            conn.setConnectTimeout(30000);
            if (conn.getResponseCode() == 200) {
                System.out.println("200");
                newVersionInfo = VersionInfoParser.parseVersionInfo(conn.getInputStream());
            }
            if (newVersionInfo != null) {
                ClouData.getInstance().setVersionInfo(newVersionInfo);
                Intent broadcastIntent = new Intent();
                broadcastIntent.setAction(UpdateVersionManager.NEW_VERSION_ACTION);
                broadcastIntent.putExtra("version", newVersionInfo.Versions.get(String.valueOf(ClouData.getInstance().getStandardInfo().MainBoardVersion.trim()) + "," + ClouData.getInstance().getStandardInfo().MeasureBoardVersion.trim() + "," + ClouData.getInstance().getStandardInfo().NeutralBoardVersion.trim()));
                sendBroadcast(broadcastIntent);
            }
        } catch (MalformedURLException e) {
            System.out.println("201");
            e.printStackTrace();
            if (0 != 0) {
                ClouData.getInstance().setVersionInfo(null);
                Intent broadcastIntent2 = new Intent();
                broadcastIntent2.setAction(UpdateVersionManager.NEW_VERSION_ACTION);
                broadcastIntent2.putExtra("version", newVersionInfo.Versions.get(String.valueOf(ClouData.getInstance().getStandardInfo().MainBoardVersion.trim()) + "," + ClouData.getInstance().getStandardInfo().MeasureBoardVersion.trim() + "," + ClouData.getInstance().getStandardInfo().NeutralBoardVersion.trim()));
                sendBroadcast(broadcastIntent2);
            }
        } catch (ProtocolException e2) {
            System.out.println("202");
            e2.printStackTrace();
            if (0 != 0) {
                ClouData.getInstance().setVersionInfo(null);
                Intent broadcastIntent3 = new Intent();
                broadcastIntent3.setAction(UpdateVersionManager.NEW_VERSION_ACTION);
                broadcastIntent3.putExtra("version", newVersionInfo.Versions.get(String.valueOf(ClouData.getInstance().getStandardInfo().MainBoardVersion.trim()) + "," + ClouData.getInstance().getStandardInfo().MeasureBoardVersion.trim() + "," + ClouData.getInstance().getStandardInfo().NeutralBoardVersion.trim()));
                sendBroadcast(broadcastIntent3);
            }
        } catch (IOException e3) {
            System.out.println("203");
            e3.printStackTrace();
            if (0 != 0) {
                ClouData.getInstance().setVersionInfo(null);
                Intent broadcastIntent4 = new Intent();
                broadcastIntent4.setAction(UpdateVersionManager.NEW_VERSION_ACTION);
                broadcastIntent4.putExtra("version", newVersionInfo.Versions.get(String.valueOf(ClouData.getInstance().getStandardInfo().MainBoardVersion.trim()) + "," + ClouData.getInstance().getStandardInfo().MeasureBoardVersion.trim() + "," + ClouData.getInstance().getStandardInfo().NeutralBoardVersion.trim()));
                sendBroadcast(broadcastIntent4);
            }
        } catch (XmlPullParserException e4) {
            System.out.println("204");
            e4.printStackTrace();
            if (0 != 0) {
                ClouData.getInstance().setVersionInfo(null);
                Intent broadcastIntent5 = new Intent();
                broadcastIntent5.setAction(UpdateVersionManager.NEW_VERSION_ACTION);
                broadcastIntent5.putExtra("version", newVersionInfo.Versions.get(String.valueOf(ClouData.getInstance().getStandardInfo().MainBoardVersion.trim()) + "," + ClouData.getInstance().getStandardInfo().MeasureBoardVersion.trim() + "," + ClouData.getInstance().getStandardInfo().NeutralBoardVersion.trim()));
                sendBroadcast(broadcastIntent5);
            }
        } catch (Throwable th) {
            if (0 != 0) {
                ClouData.getInstance().setVersionInfo(null);
                Intent broadcastIntent6 = new Intent();
                broadcastIntent6.setAction(UpdateVersionManager.NEW_VERSION_ACTION);
                broadcastIntent6.putExtra("version", newVersionInfo.Versions.get(String.valueOf(ClouData.getInstance().getStandardInfo().MainBoardVersion.trim()) + "," + ClouData.getInstance().getStandardInfo().MeasureBoardVersion.trim() + "," + ClouData.getInstance().getStandardInfo().NeutralBoardVersion.trim()));
                sendBroadcast(broadcastIntent6);
            }
            throw th;
        }
    }
}
