package com.clou.rs350.version;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import com.clou.rs350.model.ClouData;
import com.clou.rs350.ui.dialog.ProgressDialog;
import com.clou.rs350.utils.OtherUtils;
import com.itextpdf.text.pdf.PdfObject;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class UpdateUtils {
    public static boolean isApkNewVersionExist(Context context, VersionInfo info) {
        return info != null && getAppVersionFromPM(context) < info.getVersionCode();
    }

    public static boolean isHardwareNewVersionExist(VersionInfo info) {
        boolean isNewVersionExist = false;
        if (info != null) {
            String tmpFirmwareVersion = ClouData.getInstance().getStandardInfo().FirmwareVersion.replace("V", PdfObject.NOTHING).replace("R", PdfObject.NOTHING).replace("C", PdfObject.NOTHING);
            String tmpServerVersion = info.getFirmwareVersion().replace("V", PdfObject.NOTHING).replace("R", PdfObject.NOTHING).replace("C", PdfObject.NOTHING);
            if (!OtherUtils.isEmpty(tmpFirmwareVersion) && !OtherUtils.isEmpty(tmpServerVersion)) {
                String[] tmpFirmwareVersions = tmpFirmwareVersion.split("-");
                String[] tmpServerVersions = tmpServerVersion.split("-");
                int count = tmpFirmwareVersions.length < tmpServerVersions.length ? tmpFirmwareVersions.length : tmpServerVersions.length;
                for (int i = 0; i < count; i++) {
                    isNewVersionExist = Float.parseFloat(tmpFirmwareVersions[i]) < Float.parseFloat(tmpServerVersions[i]);
                    if (isNewVersionExist) {
                        break;
                    }
                }
            }
        }
        return isNewVersionExist;
    }

    public static boolean isNewVersionExist(VersionInfo version, Context context) {
        return isApkNewVersionExist(context, version) || isHardwareNewVersionExist(version);
    }

    public static int getAppVersionFromPM(Context context) {
        try {
            return context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
            return 1;
        }
    }

    public static File download(String fileurl, String path, ProgressDialog pd) {
        boolean isQuit = false;
        try {
            HttpURLConnection conn = (HttpURLConnection) new URL(fileurl).openConnection();
            conn.setRequestMethod("GET");
            conn.setConnectTimeout(5000);
            int code = conn.getResponseCode();
            System.out.println("download--->code : " + code);
            if (code != 200) {
                return null;
            }
            pd.setMax(conn.getContentLength());
            InputStream is = conn.getInputStream();
            File file = new File(path);
            FileOutputStream fos = new FileOutputStream(file);
            byte[] buffer = new byte[51200];
            int total = 0;
            while (true) {
                int len = is.read(buffer);
                if (len == -1) {
                    break;
                } else if (!pd.isShowing()) {
                    System.out.println("download break");
                    isQuit = true;
                    break;
                } else {
                    fos.write(buffer, 0, len);
                    total += len;
                    pd.setProgress(total);
                }
            }
            is.close();
            fos.close();
            if (isQuit) {
                return null;
            }
            return file;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static void installApplication(Context context, File apkfile) {
        Intent intent = new Intent();
        intent.setAction("android.intent.action.VIEW");
        intent.addCategory("android.intent.category.DEFAULT");
        intent.setDataAndType(Uri.fromFile(apkfile), "application/vnd.android.package-archive");
        context.startActivity(intent);
    }
}
