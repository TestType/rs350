package com.clou.rs350.service;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.util.Log;
import com.clou.rs350.CLApplication;
import com.clou.rs350.Preferences;
import com.clou.rs350.connect.bluetooth.manager.BluetoothSearch;
import com.clou.rs350.constants.ConstantsFileName;
import com.clou.rs350.db.dao.BaseDao;
import com.clou.rs350.db.dao.CustomerInfoDao;
import com.clou.rs350.db.dao.FieldSettingDao;
import com.clou.rs350.db.dao.MeterTypeDao;
import com.clou.rs350.db.dao.OperatorInfoDao;
import com.clou.rs350.db.dao.SiteDataManagerDao;
import com.clou.rs350.db.dao.sequence.RegisterReadingSchemeDao;
import com.clou.rs350.db.dao.sequence.SealSchemeDao;
import com.clou.rs350.db.dao.sequence.SequenceDao;
import com.clou.rs350.db.dao.sequence.VisualSchemeDao;
import com.clou.rs350.device.MeterBaseDevice;
import com.clou.rs350.manager.MessageManager;
import com.clou.rs350.manager.StatusBarManager;
import com.clou.rs350.model.ClouData;
import com.clou.rs350.task.ISleepCallback;
import com.clou.rs350.task.TimerThread;
import com.clou.rs350.ui.activity.ClampAdjustmentStateActivity;
import java.io.File;

public class CLService extends Service implements MessageManager.IMessageListener {
    private String TAG = "CLService";
    private MyBinder binder = new MyBinder();
    private TimerThread getBattery;
    private BluetoothSearch mBlutoothManager;
    private String mNetworkManager;
    private Context m_Context;
    private TimerThread m_DemoThread;
    private String m_ImportPath;
    protected MessageManager messageManager;
    private StatusBarManager statusBarManager;

    public void onCreate() {
        super.onCreate();
        this.m_Context = this;
        this.m_ImportPath = CLApplication.app.getImportPath();
        initStatuBarView();
        initBluetooth();
        initGetBattery();
    }

    private void initGetBattery() {
        this.messageManager = MessageManager.getInstance(this);
        if (this.messageManager != null) {
            this.messageManager.addMessageListener(this);
        }
        this.getBattery = new TimerThread(10000, new ISleepCallback() {
            /* class com.clou.rs350.service.CLService.AnonymousClass1 */
            int index;

            @Override // com.clou.rs350.task.ISleepCallback
            public void onSleepAfterToDo() {
                if (this.index == 0) {
                    CLService.this.messageManager.getDeviceBattery();
                    CLService.this.messageManager.getFunc_Mstate();
                }
                this.index++;
                this.index %= 6;
                CLService.this.checkImport(new OperatorInfoDao(CLService.this.m_Context), ConstantsFileName.OPERATORINFOCSVNAME);
                CLService.this.checkImport(new CustomerInfoDao(CLService.this.m_Context), ConstantsFileName.CUSTOMERINFOCSVNAME);
                CLService.this.checkImport(new SiteDataManagerDao(CLService.this.m_Context), ConstantsFileName.SITEDATACSVNAME);
                CLService.this.checkImport(new MeterTypeDao(CLService.this.m_Context), ConstantsFileName.METERTYPECSVNAME);
                CLService.this.checkImport(new SequenceDao(CLService.this.m_Context), ConstantsFileName.SEQUENCESCHEMECSVNAME);
                CLService.this.checkImport(new SealSchemeDao(CLService.this.m_Context), ConstantsFileName.SEALSCHEMECSVNAME);
                CLService.this.checkImport(new VisualSchemeDao(CLService.this.m_Context), ConstantsFileName.VISUALSCHEMECSVNAME);
                CLService.this.checkImport(new RegisterReadingSchemeDao(CLService.this.m_Context), ConstantsFileName.REGISTERREADINGSCHEMECSVNAME);
                CLService.this.checkImport(new FieldSettingDao(CLService.this.m_Context), ConstantsFileName.FIELDSETTINGCSVNAME);
            }
        }, "getBattery");
        this.getBattery.start();
        this.m_DemoThread = new TimerThread(1000, new ISleepCallback() {
            /* class com.clou.rs350.service.CLService.AnonymousClass2 */

            @Override // com.clou.rs350.task.ISleepCallback
            public void onSleepAfterToDo() {
                if (ClouData.getInstance().isDemo()) {
                    CLService.this.messageManager.onReceiveMessage(new byte[1]);
                }
            }
        }, "m_DemoThread");
        this.m_DemoThread.start();
    }

    /* access modifiers changed from: private */
    /* access modifiers changed from: public */
    private void checkImport(BaseDao dao, String fileName) {
        File importFile = new File(String.valueOf(this.m_ImportPath) + File.separator + fileName);
        if (importFile.exists()) {
            dao.importDataFromCSV(importFile.getPath());
            importFile.delete();
            CLApplication.app.refreshPath(this.m_ImportPath);
        }
    }

    private void initStatuBarView() {
        this.statusBarManager = StatusBarManager.getInstance();
        this.statusBarManager.setContext(this);
        this.statusBarManager.registBroadcastReceiver();
    }

    private void initBluetooth() {
        this.mBlutoothManager = BluetoothSearch.getInstance(this);
        this.mBlutoothManager.registBroadcastReceiver();
        this.mBlutoothManager.setMac(Preferences.getString(Preferences.Key.BLUETOOTH_MAC));
        this.mBlutoothManager.searchDeviceList();
    }

    public void onDestroy() {
        super.onDestroy();
        Log.i(this.TAG, String.valueOf(this.TAG) + " onDestroy.");
        this.getBattery.stopTimer();
        this.m_DemoThread.stopTimer();
        this.mBlutoothManager.unRegistBroadcastReceiver();
        this.mBlutoothManager.cancleDiscovery();
        this.mBlutoothManager.closeSocket("关闭服务-断开蓝牙");
        this.statusBarManager.unRegistBroadcastReceiver();
        if (this.messageManager != null) {
            this.messageManager.removeMessageListener(this);
        }
        System.exit(0);
    }

    public class MyBinder extends Binder {
        public MyBinder() {
        }

        public CLService getService() {
            return CLService.this;
        }
    }

    public IBinder onBind(Intent intent) {
        Log.i(this.TAG, String.valueOf(this.TAG) + " -> onBind, Thread: " + Thread.currentThread().getName());
        return this.binder;
    }

    public boolean onUnbind(Intent intent) {
        Log.i(this.TAG, String.valueOf(this.TAG) + " -> onUnbind, from:" + intent.getStringExtra("from"));
        return false;
    }

    public int onStartCommand(Intent intent, int flags, int startId) {
        Log.i(this.TAG, String.valueOf(this.TAG) + " -> onStartCommand, startId: " + startId + ", Thread: " + Thread.currentThread().getName());
        return START_NOT_STICKY;
    }

    @Override // com.clou.rs350.manager.MessageManager.IMessageListener
    public void onReceiveMessage(MeterBaseDevice Device) {
        this.statusBarManager.setDeviceBattery(Device.getBatteryValue(), 100, 0);
        if (152 == Device.getFunctionValue() && !ClampAdjustmentStateActivity.isCreated) {
            Intent dialogIntent = new Intent(getBaseContext(), ClampAdjustmentStateActivity.class);
            dialogIntent.addFlags(268435456);
            getApplication().startActivity(dialogIntent);
        }
    }

    @Override // com.clou.rs350.manager.MessageManager.IMessageListener
    public void onReceiveMessage(byte[] buffer) {
    }

    @Override // com.clou.rs350.manager.MessageManager.IMessageListener
    public void onReceiveMessage(int strId) {
    }

    @Override // com.clou.rs350.manager.MessageManager.IMessageListener
    public void onReceiveMessage(String str) {
    }
}
