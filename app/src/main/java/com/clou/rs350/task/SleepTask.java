package com.clou.rs350.task;

import android.os.AsyncTask;
import android.util.Log;

public class SleepTask extends AsyncTask<Integer, Void, Object> {
    private ISleepCallback callback;
    long i = 0;
    boolean m_Stop = false;
    private long sleepMillis;

    public SleepTask(long sleepMillis2, ISleepCallback callback2) {
        this.sleepMillis = sleepMillis2;
        this.callback = callback2;
    }

    /* access modifiers changed from: protected */
    public Integer doInBackground(Integer... params) {
        sleep(this.sleepMillis);
        return 0;
    }

    public void restart() {
        this.i = 0;
    }

    public void stop() {
        this.m_Stop = true;
    }

    private void sleep(long millis) {
        try {
            Log.i("SleepTask", "Sleep" + millis);
            long count = millis / 100;
            this.i = 0;
            while (this.i < count && !this.m_Stop) {
                Thread.sleep(100);
                this.i++;
            }
            Log.i("SleepTask", "Sleep stop");
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    /* access modifiers changed from: protected */
    @Override // android.os.AsyncTask
    public void onPostExecute(Object result) {
        if (!this.m_Stop && this.callback != null) {
            this.callback.onSleepAfterToDo();
        }
    }
}
