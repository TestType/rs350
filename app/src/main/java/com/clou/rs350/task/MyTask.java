package com.clou.rs350.task;

import android.os.AsyncTask;
import com.clou.rs350.callback.ILoadCallback;

public class MyTask extends AsyncTask<Integer, Void, Object> {
    private ILoadCallback callback;
    public boolean isDestoryed;

    public MyTask(ILoadCallback callback2) {
        this.callback = callback2;
    }

    /* access modifiers changed from: protected */
    public Object doInBackground(Integer... params) {
        if (this.callback != null) {
            return this.callback.run();
        }
        return null;
    }

    /* access modifiers changed from: protected */
    @Override // android.os.AsyncTask
    public void onPostExecute(Object result) {
        if (this.callback != null) {
            this.callback.callback(result);
        }
        this.isDestoryed = true;
    }
}
