package com.clou.rs350.task;

import android.util.Log;
import com.itextpdf.text.pdf.PdfObject;

public class TimerThread extends Thread {
    private ISleepCallback callback;
    private long compareTime;
    private boolean isStop = false;
    private long sleepSecond;
    private String tag = PdfObject.NOTHING;
    private String threadName = "TimerThread";

    public TimerThread(long sleepSecond2, ISleepCallback callback2, String threadName2) {
        this.sleepSecond = sleepSecond2;
        this.callback = callback2;
        this.threadName = threadName2;
        this.tag = "start";
    }

    public void run() {
        this.compareTime = System.currentTimeMillis();
        while (!this.isStop) {
            try {
                if (this.compareTime <= System.currentTimeMillis()) {
                    if (this.callback != null) {
                        new Thread(new Runnable() {
                            /* class com.clou.rs350.task.TimerThread.AnonymousClass1 */

                            public void run() {
                                TimerThread.this.callback.onSleepAfterToDo();
                            }
                        }).start();
                    }
                    this.compareTime += this.sleepSecond;
                }
                Thread.sleep(50);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        Log.i("TimerThread", "thread:" + this.threadName + " isStop:" + this.isStop + " tag:" + this.tag);
    }

    public boolean isStop() {
        return this.isStop;
    }

    public void stopTimer() {
        this.isStop = true;
        this.tag = "stop";
    }
}
